/*
********************************************************************************
*                                      				Realtek's 5828 Project
*
*											Layer: 			
*										Module:  
*								(c) Copyright 2011-2012, Realtek Corp.
*                                           			All Rights Reserved
*
*                                                  			V0.01
*
* File : RS0551C.C
********************************************************************************
*/

/*
********************************************************************************
 \file		RS0551C.c
 \brief	

GENERAL DESCRIPTION:

EXTERNALIZED FUNCTIONS:
  
Copyright (c) 2011-2012 by Realtek Incorporated.  All Rights Reserved.

 \version 0.01
 \date    2011-2012

********************************************************************************
*/

/*
================================================================================

                        EDIT HISTORY FOR MODULE

when        	who		Ver		what, where, why
---------- 	-----   	-----	--------------------------------------------------------------
2011-11-30	Jimmy	0.01		Init version

================================================================================
*/
#include "Inc.h"

#ifdef RTS58XX_SP_RS0551C
extern U16 GetSensorFormatHeight(U16 const Format, bit const Flag);
extern U16 GetSensorFormatWidth(U16 const Format, bit const Flag);
#define MIPI_DATA_LANE_ALL_EN	(MIPI_DATA_LANE0_EN | MIPI_DATA_LANE1_EN)

U8 l_byMipiRxLaneEn;
U8 l_byMipiRxDphyType;
U8 l_byMipiRxSpecType;	
U8 l_byMipiRxHSTerm;

OV_CTT_t code gc_RS0551C_CTT[3] =
{
	{3000,0x0100,0x0100,0x0100},
	{4150,0x0100,0x0100,0x0100},
	{6500,0x0100,0x0100,0x0100},
};

U8 code gc_RS0551C_CCM[21] = {0,1,0,0,0,0,    0,0,0,1,0,0,  0,0,0,0,0,1, 0,0,0};

static U8 RS0551MIPITxLaneNum(void )
{
	U8 i;
	U8 bytmp = 0;	

	for(i=0;i<8;i++)
	{
		if (l_byMipiRxLaneEn&(1<<i))
		{
			bytmp++;
		}
	}

	return bytmp;
	
}

static void RS0551MIPITxAphyInit(void )
{
	Write_SenReg(0x042a, 0x00);//cmu debug enable
	Write_SenReg(0x0410, 0x00);//mipi power enable
	Write_SenReg(0x0409, 0x01);//CMU enable
	Write_SenReg(0x0400, 0x01);//tx power enable
	Write_SenReg(0x0426, 0x00);//cmu power value setting
}

static void RS0551MIPITxDphyInit(void )
{
	// MIPI TX DTYPE 0-RAW8 1-RAW10 2-RAW12 3 RAW14
	switch (l_byMipiRxSpecType)
	{
		case MIPI_DATA_TYPE_RAW8:
			Write_SenReg(0x0301, 0x00);
			break;
		//case MIPI_DATA_TYPE_RAW12:
		//	Write_SenReg(0x0301, 0x02);			
		//	break;
		//case MIPI_DATA_TYPE_RAW14:
		//	Write_SenReg(0x0301, 0x03);				
		//	break;
		case MIPI_DATA_TYPE_YUYV8:
			Write_SenReg(0x0301, 0x00);
			break;
		case MIPI_DATA_TYPE_YUYV10:
			Write_SenReg(0x0301, 0x01);
			break;
		case MIPI_DATA_TYPE_RAW10:
		default:
			Write_SenReg(0x0301, 0x01);				
			break;			
	}
		
	Write_SenReg(0x0302, (((l_byMipiRxLaneEn&0x0C)<<4)|(l_byMipiRxLaneEn&0x03)));	// MIPI TX LANE CONFIG
	Write_SenReg(0x0311, 0x00);		// MIPI TX DATA SRC 0-FIXP 1-CCS
	Write_SenReg(0x0317, (RS0551MIPITxLaneNum() -1));	// MIPI TX LANE Num 0-1 1-2 2-3 3-4
	Write_SenReg(0x0318, 0xE4);	// MIPI TX LANE Mapping 
	Write_SenReg(0x0319, l_byMipiRxSpecType);	// MIPI TX TYPE
	Write_SenReg(0x0418, 0x07);	// MIPI TX LDO CONFIG	
}


// Configure PLL mipi clock
// mipi_clk = bitclk(include rising and failing)
// mipi_clk = Fref * (NC+2+FC/4096)/PreDV/(PstDV+2)....FC=0,PreDV=1,PstDV+2=2,Fref=12Mhz
// mipi_tx_clk = mipi_clk/8
static void RS0551MIPITxAphyFreqSet(U16 const wMipiClk, U8 const byFpDiv)	//Unit :1M
{
	U16 wSSCNC;

	wSSCNC = wMipiClk/12-2;

	//txclk=mipiclk/8
	Write_SenReg(0x0467, 0x00);	//fix mipi tx clock soure 0= mipi_clk/2	1=mipi_clk/4
	//Write_SenReg(0x0467, 0x01);	//fix mipi tx clock soure 0= mipi_clk/2	1=mipi_clk/4
	Write_SenReg(0x031a, 0x00);	//fix mipi tx clock sel 	0= clk_src/4	1=clk_src/2 
	//fpclk=mipiclk/2/(byFpDiv-1)
	Write_SenReg(0x021a, byFpDiv/2);	//fixp pattern clock source sel 0=clk_src 1=clk_src/2 2=clk_src/4 3=SSOR_SYSCLK	

	if (g_bySnrType== 0)
	{
		if (byFpDiv == 1)
		{
			Write_SenReg(0x0701, 0x80);
		}
		else if (byFpDiv == 2)
		{
			Write_SenReg(0x0701, 0x81);			
		}
		else	//byFpDiv == 4
		{
			Write_SenReg(0x0701, 0x85);			
		}
	}
	
	//PLL pre-div=0, post-div=0,
	Write_SenReg(0x0415, 0x00);	//PLL Pre-div 0-1 1-2 2-4 4-8	
	Write_SenReg(0x0424, 0x00);	//PLL Post-div	
	Write_SenReg(0x042E, 0x00);	//0=vco bypass post-div 1= div	 	
	Write_SenReg(0x0434, 0x00);
	Write_SenReg(0x0435, 0x00);	
	Write_SenReg(0x0436,  INT2CHAR(wSSCNC, 0));	
	Write_SenReg(0x0437,  INT2CHAR(wSSCNC, 1));	
	
}

static U8 RS0551MIPITxGetUINum(U8 const byMinTime, U8 const byMinUINum, U16 const wMipiClk)
{
	U8 bytmp;	//Unit:8UI

	bytmp = (U8)(((float)byMinTime*wMipiClk/1000 + byMinUINum)/8) + 1;
	
	return bytmp;
}

// mipi_clk = bitclk(include rising and failing)
static void RS0551MIPITxTimingInit(U16 const wMipiClk)
{
	U16 wTmp;
	//UI = mipi_clk
	//Unit: 8UI

	Write_SenReg(0x0305, RS0551MIPITxGetUINum(100, 0, wMipiClk));	// MIPI Tx HS EXIT Time  more than 100nS	
	wTmp = (U16)((U32)100*wMipiClk/8+1);
	Write_SenReg(0x0303, INT2CHAR(wTmp, 0));						// MIPI TX Init Time 	more than 100uS	
	Write_SenReg(0x0304, INT2CHAR(wTmp, 1));
	Write_SenReg(0x0306, RS0551MIPITxGetUINum(50, 0, wMipiClk));		// MIPI TX LPX Time  	more than 50ns
	Write_SenReg(0x0307, RS0551MIPITxGetUINum(60, 52, wMipiClk));	// MIPI CK POST Time	more than 60nS + 52UI	
	Write_SenReg(0x0308, RS0551MIPITxGetUINum(68, 0, wMipiClk));		// MIPI CK PRPR Time	38nS~ 95nS
	Write_SenReg(0x0309, RS0551MIPITxGetUINum(232, 0 ,wMipiClk));	// MIPI CK ZERO Time	CK_ZERO_TIME + CK_PRPR_TIME > 300nS
	Write_SenReg(0x030A, 2);											// MIPI CK PRE Time		more than 8UI	
	Write_SenReg(0x030B, RS0551MIPITxGetUINum(45, 4, wMipiClk));		// MIPI HS PRPR Time	 40nS+4UI ~ 85UI+6UI
	Write_SenReg(0x030C, RS0551MIPITxGetUINum(100, 6, wMipiClk));	// MIPI HS ZERO Time	HS_ZERO_TIME + HS_PRPR_TIME > 145nS + 10UI
	Write_SenReg(0x030D, RS0551MIPITxGetUINum(60, 0, wMipiClk));		// MIPI TX TRAIL Time	more than 60nS
	Write_SenReg(0x030E, RS0551MIPITxGetUINum(60, 0 ,wMipiClk));		//MIPI TX VLD Time		more than 60nS	need small than HS_ZERO_TIME	
	Write_SenReg(0x030E,0x00);		// MIPI TX IF CFG0

}

static U32 RS0551CLKSet(U16 SetFormat, U8 byFps )
{
	U16 wMipiClk;
	U32 dwMipiClk;
	U8 byLaneNum;
	U8 byDataWidth;
	U8 byDiv = 1;
	U32 dwFixPclk;

	//byLaneNum = 1;

	if (g_bySnrType== 1)
	{
		switch (l_byMipiRxSpecType)
		{
			case MIPI_DATA_TYPE_RAW8:
				byDataWidth = 8;
				break;
			//case MIPI_DATA_TYPE_RAW12:
			//	byDataWidth = 12;		
			//	break;
			//case MIPI_DATA_TYPE_RAW14:
			//	byDataWidth = 14;			
			//	break;
			case MIPI_DATA_TYPE_YUYV8:
				byDataWidth = 8;
				break;
			case MIPI_DATA_TYPE_YUYV10:
				byDataWidth = 10;
				break;
			case MIPI_DATA_TYPE_RAW10:
			default:
				byDataWidth = 10;				
				break;			
		}

		byLaneNum = RS0551MIPITxLaneNum();
		//SNR(("byLaneNum %bx\n",byLaneNum));

		if(g_bySensor_YuvMode == YUV422_MODE)
		{
			dwMipiClk = g_dwPclk*byDataWidth*2/byLaneNum;
		}
		else
		{
			dwMipiClk = g_dwPclk*byDataWidth/byLaneNum;
		}
		
		wMipiClk = dwMipiClk/1000000;	//Unit: 1M

		
		if (dwMipiClk >= (g_dwPclk*8))
		{
			byDiv =4;
		}
		else if (dwMipiClk >= (g_dwPclk*4))
		{
			byDiv =2;
		}
		else
		{
			byDiv =1;
		}

		//Fix 0x0467 =0 clk_src=mipi_clk/2
		//0x021a=byDiv  fixp_cls_sel 0->clk_src 1->clk_src/2 2->clk_src/4
		dwFixPclk = dwMipiClk/byDiv/2;
		g_dwPclk = dwFixPclk;

	}	
	
	else
	{
	
		//dwMipiClk = g_dwPclk<<1;
		dwMipiClk = g_dwPclk<<2;
		wMipiClk = dwMipiClk/1000000;	//Unit: 1M
		byDiv =2;

		dwFixPclk = g_dwPclk;
		g_dwPclk = dwFixPclk;

		
	}
	
	RS0551MIPITxAphyFreqSet(wMipiClk, byDiv);
	RS0551MIPITxTimingInit(wMipiClk);


	return dwFixPclk;
}

static void RS0551Run(void )
{
	if(g_bySensor_YuvMode == YUV422_MODE)
	{
		Write_SenReg(0x0210, 0xC0);		//Frame Num=0		
	}
	else
	{
		Write_SenReg(0x0210, 0x80);		//Frame Num=0		
	}
	if (g_bySnrType== 1)
	{	
		Write_SenReg(0x0300, 0x01);		// MIPI TX Enable
	}
	else
	{
		Write_SenReg(0x0700, 0x01);		// MIPI TX Enable		
	}
	
}

static void RS0551CFixpSet(U32 dwFixpPclk, U16 SetFormat, U8 byFps)
{
	U16 wWidth;
	U16 wHeight;
	U16 wTmp;
	U16 wTmp2;
       U16 wFixpWidth;
	
	wWidth = GetSensorFormatWidth(SetFormat,0);
	wHeight = GetSensorFormatHeight(SetFormat,0);
	g_wSensorWidthBefBLC = wWidth;
	g_wSensorHeightBefBLC = wHeight;
	if(g_bySensor_YuvMode == YUV422_MODE)
	{
		wFixpWidth = wWidth *2;
	}
	else
	{
		wFixpWidth = wWidth;
	}		
	Write_SenReg(0x0200, INT2CHAR(wFixpWidth, 1));	// Frame width	
	Write_SenReg(0x0201, INT2CHAR(wFixpWidth, 0));	
	Write_SenReg(0x0202, INT2CHAR(wHeight, 1));	// Frame height	
	Write_SenReg(0x0203, INT2CHAR(wHeight, 0));		

	if (g_bySnrType== 0)
	{
		Write_SenReg(0x0706, INT2CHAR(wFixpWidth, 1));	// Frame width	
		Write_SenReg(0x0705, INT2CHAR(wFixpWidth, 0));	
		Write_SenReg(0x0708, INT2CHAR(wHeight, 1));	// Frame height	
		Write_SenReg(0x0707, INT2CHAR(wHeight, 0));	
		Write_SenReg(0x0704, 0x01);	//Falling edge output data
	}

	Write_SenReg(0x0211, 100);	// FIXP BLOCK START X	
	Write_SenReg(0x0212, 100);	// FIXP BLOCK START Y
	Write_SenReg(0x0213, 0);		// FIXP BLOCK START H	

	Write_SenReg(0x0217, 20);	// FIXP MARGIN WIDTH
	Write_SenReg(0x0218, 20);	// FIXP MARGIN HEIGHT
	Write_SenReg(0x0219, 0);		// FIXP MARGIN SIZE H

	wTmp = (wWidth - 100*2 - 20*7)/6;
	wTmp2 = (wHeight - 100*2 - 20*5)/4;	
	Write_SenReg(0x0214, INT2CHAR(wTmp, 0));	// FIXP BLOCK WIDTH
	Write_SenReg(0x0215, INT2CHAR(wTmp2, 0));	// FIXP BLOCK HEIGHT
	Write_SenReg(0x0216, (INT2CHAR(wTmp, 1)<<4) | INT2CHAR(wTmp2, 1));	// FIXP BLOCK SIZE H

	Write_SenReg(0x0206, 0);	
	Write_SenReg(0x0207, 20);	// Dummy line = 20

	g_wAEC_LineNumber = wHeight + 6;
	g_wAECExposureRowMax = g_wAEC_LineNumber - 4;

	wTmp = dwFixpPclk/g_wAEC_LineNumber/byFps;	//HsyncWidth

	if ( (wTmp%2) != 0)	//Even Number and larger fps
	{
		wTmp -= 1;
	}
	
	g_wSensorHsyncWidth = wTmp;
	wTmp -= wFixpWidth;
	Write_SenReg(0x0208, 0);		// speed ctrl = 0
	Write_SenReg(0x0204,  INT2CHAR(wTmp,1));	
	Write_SenReg(0x0205,  INT2CHAR(wTmp,0));
	
	Write_SenReg(0x0209, 0);		//Frame Num=0	


}

static void RS0551CSet(U16 SetFormat, U8 byFps)
{
	U32 dwFixpPclk;

	RS0551MIPITxAphyInit();
	RS0551MIPITxDphyInit();
	dwFixpPclk = RS0551CLKSet(SetFormat, byFps);
	RS0551CFixpSet(dwFixpPclk, SetFormat, byFps);
	RS0551Run();
}

void InitRS0551CIspParams()
{
	// D50 R/G/B gain
	g_wAWBRGain_Last= 0x136;	
	g_wAWBGGain_Last= 0x100;
	g_wAWBBGain_Last= 0x19D;
	g_wProjectGR = g_wAWBRGain_Last;
	g_wProjectGB = g_wAWBBGain_Last;

	// Special ISP
	g_bySaturation_Def =64;
	g_byContrast_Def = 36;
	g_wDynamicISPEn = 0x00;
}


//this function is also called during Probe stage to calculate the transfer_size/LWM/HWM
void GetSensorPclkHsync(U16 wSensorSPFormat,U8 byFps)
{
	// Save for Property Page SetSensorIntegrationTime
	//if (STREAM_SNR_PRVW_TWO == g_byStreamMode)
	//g_dwPclk = g_dwPclk;
	//g_wSensorHsyncWidth = g_wSensorHsyncWidth;
}

void RS0551CSetFormatFps(U16 wSetFormat, U8 byFps)
{	
	U16 pwData;
	g_bySnrType = 1;
	g_bySensor_YuvMode = RAW_MODE; 
	l_byMipiRxLaneEn 	= MIPI_DATA_LANE_ALL_EN;
	l_byMipiRxDphyType = MIPI_DATA_FORMAT_RAW10 | MIPI_YUV_TYPE_NULL;
	l_byMipiRxSpecType  = MIPI_DATA_TYPE_RAW10;					
	l_byMipiRxHSTerm	= HSTERM_EN_TIME_11;
	if ((g_wSensorSPFormat&wSetFormat) ==  QXGA_FRM)
	{
	    //-----------------------------Dphy Setting--------------------------------
	    if (g_bySnrType)
	    {
        		l_byMipiRxLaneEn 	= MIPI_DATA_LANE_ALL_EN;
        		l_byMipiRxDphyType = MIPI_DATA_FORMAT_RAW10 | MIPI_YUV_TYPE_NULL;
        		l_byMipiRxSpecType  = MIPI_DATA_TYPE_RAW10;					
        		l_byMipiRxHSTerm	= HSTERM_EN_TIME_22;
	    }

        l_byMipiRxLaneEn 	= MIPI_DATA_LANE_ALL_EN;
	    g_bySubResolution_For_LSC = 0;

            if(g_bySensor_YuvMode==YUV422_MODE)
            {
                    g_dwPclk = 60000000;
            }
            else
            {
                    g_dwPclk = 90000000;
            }
	
	
		//g_bySensor_YuvMode = RAW_MODE;
		// this function will change g_dwPclk and g_dwMIPIclk
		RS0551CSet(wSetFormat, byFps);
		GetSensorPclkHsync(QXGA_FRM,byFps);
	}
	else if ((g_wSensorSPFormat&wSetFormat) ==  HD1080P_FRM)
	{
		//SNR(("RS0551C HD1080P MIPI\n"));
		//if (g_byCCICurSnrIntf == MIPI_SNR)
		{
		
		    //-----------------------------Dphy Setting--------------------------------
			l_byMipiRxLaneEn 	= MIPI_DATA_LANE_ALL_EN;
			l_byMipiRxDphyType = MIPI_DATA_FORMAT_RAW10 | MIPI_YUV_TYPE_NULL;
			l_byMipiRxSpecType  = MIPI_DATA_TYPE_RAW10;					
			l_byMipiRxHSTerm	= HSTERM_EN_TIME_44;	
		}


		g_bySubResolution_For_LSC = 0;
		g_dwPclk = 72000000;

		g_bySensor_YuvMode = RAW_MODE;	
		
		// this function will change g_dwPclk and g_dwMIPIclk		
		RS0551CSet(wSetFormat, byFps);
		GetSensorPclkHsync(HD1080P_FRM,byFps);		
	}	
	else if ((g_wSensorSPFormat&wSetFormat) ==  HD800P_FRM)
	{

	    //-----------------------------Dphy Setting--------------------------------
	
    	g_bySubResolution_For_LSC = 0;

		if (byFps <=10 )
		{
			//2560x800x10=20480000
			g_dwPclk = 45000000;
		}
		else
		{
			//2560x800x30=61440000
			g_dwPclk = 55000000;
		}

		
		//g_bySensor_YuvMode = YUV422_MODE;	
		
		// this function will change g_dwPclk and g_dwMIPIclk		
		RS0551CSet(wSetFormat, byFps);
		GetSensorPclkHsync(HD800P_FRM,byFps);		
	}
	else if ((g_wSensorSPFormat&wSetFormat) ==  HD720P_FRM)
	{
		g_bySnrType = 1;
		g_bySensor_YuvMode = RAW_MODE;
		//if (g_bySnrType == 1)
		{
		    //-----------------------------Dphy Setting--------------------------------
			l_byMipiRxLaneEn 	= MIPI_DATA_LANE0_EN;
			l_byMipiRxDphyType = MIPI_DATA_FORMAT_RAW10 | MIPI_YUV_TYPE_NULL;
			l_byMipiRxSpecType  = MIPI_DATA_TYPE_RAW10;					
			l_byMipiRxHSTerm	= HSTERM_EN_TIME_44;	
		}


		g_bySubResolution_For_LSC = 0;

		if (10 == byFps)
		{
			g_dwPclk = 60000000;
		}
		else		//30FPS
		{
			g_dwPclk = 75000000;
		}
		
		//g_bySensor_YuvMode = RAW_MODE;	
		
		// this function will change g_dwPclk and g_dwMIPIclk		
		RS0551CSet(wSetFormat, byFps);
		GetSensorPclkHsync(HD720P_FRM,byFps);		
	}

//#ifdef _MIPI_EXIST_
	if (g_bySnrType == 1)
	 {
		SetMipiDphy(l_byMipiRxLaneEn, l_byMipiRxDphyType, l_byMipiRxSpecType, l_byMipiRxHSTerm);
	 }

//#endif
}

#if 0
void CfgRS0551CControlAttr(void)
{	
	U8 i;
	
	g_bySensorSize = SENSOR_SIZE_QSXGA;
	g_wSensorSPFormat = QSXGA_FRM|HD1080P_FRM|HD720P_FRM;//QSXGA_FRM|HD1080P_FRM|HD720P_FRM;
	memcpy(g_asOvCTT, gc_RS0551C_CTT,sizeof(gc_RS0551C_CTT));
	g_aVideoFormat[0].byFormatType 	 	= FORMAT_TYPE_YUY2;
	//g_aVideoFormat[0].bySubFormatType	= FORMAT_SUBTYPE_UYVY;
	g_aVideoFormat[0].wFrameMaxWidth	= VIDEO_WIDTH_2592;
	g_aVideoFormat[0].wFrameMaxHeight	= VIDEO_HEIGHT_1944;
	g_aVideoFormat[0].byMaxFps			= FPS_30;			

	
	for (i=0;i<21;i++)
	{
		g_aCCM[i]= gc_RS0551C_CCM[i];
#ifdef _SMOOTH_DYNAMIC_CCM_
		g_aCCMTarget[i] = g_aCCM[i];
#endif
	}
	
	InitRS0551CIspParams();
}
#endif

void CfgRS0551CControlAttr(void)
{
	U8 i = 0;
	U8 byFrameNum;
	
	{
		memcpy(g_asOvCTT, gc_RS0551C_CTT,sizeof(gc_RS0551C_CTT));	
	}

	{
#if (_CHIP_ID_ & _RTS5840_)
	XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
	XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V79;
#elif (_CHIP_ID_ & _RTS5832_)
	XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
	XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V85;
#elif (_CHIP_ID_ & _RTS5829B_)
	XBYTE[REG_TUNESVA] =  SVA_VOL_2V73;
	XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V80;
#endif
		//XBYTE[REG_TUNESVIO_MASTER] =  SVIO_VOL_1V84;
	}

	g_bySensorSize = SENSOR_SIZE_QXGA;
	g_wSensorSPFormat = QXGA_FRM|HD800P_FRM|HD720P_FRM;
	//g_bySensorSize = SENSOR_SIZE_VGA;
	//g_wSensorSPFormat = VGA_FRM;
	//g_bySensorSize = SENSOR_SIZE_HD720P;
	//g_wSensorSPFormat = HD720P_FRM;
	
	//if (XBYTE[USB_SPEED] == HIGH_SPEED)	
	//{	
	//	byFrameNum = 1;
	//}
	//else
	//{
	//	byFrameNum = 4;
	//}
	byFrameNum = 2;
	
	g_aVideoFormat[0].byaVideoFrameTbl[0] = byFrameNum;
	//g_aVideoFormat[0].byaVideoFrameTbl[2]= F_SEL_1280_720;
	g_aVideoFormat[0].byaVideoFrameTbl[1]= F_SEL_320_240;
	g_aVideoFormat[0].byaVideoFrameTbl[2]= F_SEL_1280_800;
	g_aVideoFormat[0].byaVideoFrameTbl[4]= F_SEL_2592_1944;
	//g_aVideoFormat[0].byaVideoFrameTbl[5]= F_SEL_3264_2448;
	
	g_aVideoFormat[0].byaStillFrameTbl[0] = byFrameNum;	
	g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_320_240;
	g_aVideoFormat[0].byaStillFrameTbl[2]=F_SEL_1280_800;
	g_aVideoFormat[0].byaStillFrameTbl[3]=F_SEL_1280_960;
	g_aVideoFormat[0].byaStillFrameTbl[4]=F_SEL_2592_1944;
	//g_aVideoFormat[0].byaStillFrameTbl[5]=F_SEL_3264_2448;

	g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_1280_720] = FPS_10;	
	g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_1280_960] = FPS_8;	
	g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_1280_800] = FPS_5;
	g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_320_240] =FPS_10|FPS_5;
	//g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_3264_2448] =FPS_30 |FPS_15;
	g_aVideoFormat[0].byFormatType = FORMAT_TYPE_YUY2;

#ifdef _ENABLE_MJPEG_
	g_aVideoFormat[1].byaVideoFrameTbl[0] = 2;
	g_aVideoFormat[1].byaVideoFrameTbl[1]= F_SEL_1280_720;
	g_aVideoFormat[1].byaVideoFrameTbl[2]= F_SEL_2048_1536;
	g_aVideoFormat[1].byaVideoFrameTbl[3]= F_SEL_1280_960;
	g_aVideoFormat[1].byaVideoFrameTbl[4]=F_SEL_2592_1944;
	//g_aVideoFormat[1].byaVideoFrameTbl[5]=F_SEL_3264_2448;
	
	g_aVideoFormat[1].byaStillFrameTbl[0] = 2;	
	g_aVideoFormat[1].byaStillFrameTbl[1]=F_SEL_1280_720;
	g_aVideoFormat[1].byaStillFrameTbl[2]=F_SEL_2048_1536;
	g_aVideoFormat[1].byaStillFrameTbl[3]=F_SEL_1280_960;
	g_aVideoFormat[1].byaStillFrameTbl[4]=F_SEL_2592_1944;
	//g_aVideoFormat[1].byaStillFrameTbl[5]=F_SEL_3264_2448;
	
	g_aVideoFormat[1].waFrameFpsBitmap[F_SEL_1280_720] = FPS_30;	
	g_aVideoFormat[1].waFrameFpsBitmap[F_SEL_1280_800] = FPS_30;	
	g_aVideoFormat[1].waFrameFpsBitmap[F_SEL_1280_960] = FPS_30;	
	g_aVideoFormat[1].waFrameFpsBitmap[F_SEL_2048_1536] = FPS_20|FPS_20|FPS_5;
	//g_aVideoFormat[1].waFrameFpsBitmap[F_SEL_3264_2448] = FPS_30|FPS_15;
	g_aVideoFormat[1].byFormatType = FORMAT_TYPE_MJPG;	
#endif		

	for (i=0;i<21;i++)
	{
		g_aCCM[i]= gc_RS0551C_CCM[i];
	}
	
   	WhiteBalanceTempAutoItem.Last = 0;//last

	InitRS0551CIspParams();
}

#if 0
void RS0551C_POR(void )
{
	ENTER_SENSOR_RESET();

	// power on
	uDelay(1);
	
	// Initialize I2C Controller Timing
	LEAVE_SENSOR_RESET();	

	// clock select 24M
#ifdef _IC_CODE_	
	CHANGE_CCS_CLK(CCS_CLK_SEL_96M |CCS_CLK_DIVIDER_4);	
#else //_IC_CODE_
	CHANGE_CCS_CLK(CCS_CLK_SEL_24M);
#endif //_IC_CODE_

	EnableSensorHCLK();	
	SensorIsOpen();
	WaitTimeOut_Delay(1);  //OV5648 spec request 8192clk
}
#endif

void RS0551C_POR(void)
{
	ENTER_SENSOR_RESET();

	// power on
	SensorPowerControl(SWITCH_ON,EN_DELAYTIME);
#ifdef _FT2_REMOVE_TEST
#else
	uDelay(1);
#endif
	// Initialize I2C Controller Timing
	LEAVE_SENSOR_RESET();

	// clock select 24M
	if( g_bySSCEnable== TRUE)
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_SSC_24M);
	}
	else
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_96M|CCS_CLK_DIVIDER_4);
	}

	EnableSensorHCLK();

	ENTER_SENSOR_RESET();
#ifdef _FT2_REMOVE_TEST
#else
	WaitTimeOut_Delay(2);
#endif
	LEAVE_SENSOR_RESET();

	g_bySensorIsOpen = SENSOR_OPEN;
	// hemonel 2008-07-22: fix write sensor register fail bug
#ifdef _FT2_REMOVE_TEST
#else
	WaitTimeOut_Delay(10);
#endif
}

void RS0551C_POR_OFF(U8 byDelayCtrl)
{
	// hemonel 2009-07-10: reset low before HCLK off for 6AA and SIV120B spec
	ENTER_SENSOR_RESET();	// drive sensor reset low when sensor

	if (byDelayCtrl == EN_DELAYTIME)
	{
		uDelay(1);
	}	


	DisableSensorHCLK();	// close sensor hclk

	if (byDelayCtrl == EN_DELAYTIME)
	{
		uDelay(1);
	}

	SensorPowerControl(SWITCH_OFF,byDelayCtrl);
	
//#ifndef _MIPI_EXIST_
	// pull control sensor pin
	//CCS_PIN_PULLCTL_ACTIVE();
//#endif
	g_bySensorIsOpen = SENSOR_CLOSED;
}
#endif

