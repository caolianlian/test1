#include "pc_cam.h"
#include "camutil.h"
#include "Version.h"

#include "camctl.h"

/*
U8 code g_cVdFwVerSection0[8] =
{
#ifdef _CACHE_MODE_
	VERSION_HEADER_CA, //header
#else
	0x00,
#endif
	_BACKENDIC_ID_,// backend IC ID
	CUSTOMER_PROJECT_NO,  //customer project No.
	_CUSTOMER_ID_,
	_FW_SUB_VER_,
	_FW_MAIN_VER_,
	_CUSTOM_FW_VER_L_,
	_CUSTOM_FW_VER_H_
};
*/

FWVersion_t code g_cVdFwVerSection0 = {
	VERSION_HEADER_CA,						//U8 byHeader;
	20,										//U8 byLen;
	VERSION_MAGIC_TAG,						//U32 dwMagictag;
	BACKENDIC_ID,							//U16 wICName;
	CUSTOMER_VENDOR_ID,					//U16 wCusVendor;
	CUSTOMER_PROJECT_NO,					//U16 wCusProject;
	FW_TRUNK_VERSION,						//U32 dwFWVer;
	FW_CUSTOMER_VERSION,					//U32 dwCusVer;

	// Reserved for customer
	0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,	//U8 Reserved[12];
	0xFF,0xFF,0xFF,0xFF,
};

