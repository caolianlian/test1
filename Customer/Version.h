#ifndef _VERSION_H_
#define _VERSION_H_

#include "pc_cam.h"

// header
#define VD_VER_STRUCT_VER	0x10
#define CODETYPE_VD			0x02
#define CODETYPE_CACHE		0x01

#define VERSION_HEADER_VD		(VD_VER_STRUCT_VER|CODETYPE_VD)
#define VERSION_HEADER_CA		(VD_VER_STRUCT_VER|CODETYPE_CACHE)

// Magic Tag
#define VERSION_MAGIC_TAG	0x12345678

// PID
#define VERSION_RTS5820 0x20
#define VERSION_RTS5801 0x01
#define VERSION_RTS5803 0x03
#define VERSION_RTS5821 0x21
#define VERSION_RTS5822 0x22
#define VERSION_RTS5827 0x27
#define VERSION_RTS5829 0x29
#define VERSION_RTS5840 0x40

#define  BACKENDIC_ID 		VERSION_RTS5840


// Customer Project No.: BCD coding
#define CUSTOMER_PROJECT_NO		0x01

// Customer ID: BCD coding. If customer has defined ID use customer's ID, if not, we define ID
#define MM_SIMPLO		0x0001
#define MM_GOTEK		0x0002
#define MM_KYE			0x0003
#define MM_BISON		0x0004
#define MM_FOXLINK		0x0005
#define MM_CC_C			0x0006
#define MM_VISTAPOINT	0x0007
#define MM_AZUREWAVE	0x0008
#define MM_QRI			0x0009
#define MM_LITE_ON		0x0010
#define MM_COMMERCE 0x0011
#define MM_SUNNYOPTICS	0x0012
#define MM_IMAGIC		0x0013
#define MM_COASIA		0x0014
#define MM_X_R			0x0015
#define MM_SUYIN		0x0016
#define MM_SDEVICES		0x0017
#define MM_CHICONY		0x0018
#define MM_BITLAND		0x0019
#define MM_NUMUGA		0x0020
#define MM_REALTEK		0x0bda
#define MM_ABOCOM		0x0031

#define CUSTOMER_VENDOR_ID    MM_REALTEK

// customer code change version
#define FW_CUS_RLS_VER		0x0000	// release version
#define FW_CUS_RVN_VER   	0x0000	// revision version
#define FW_CUSTOMER_VERSION (((U32)FW_CUS_RLS_VER<<16)|FW_CUS_RVN_VER)
#endif
