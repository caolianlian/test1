#ifndef _Sam6AA_H_
#define _Sam6AA_H_

void S5K6AASetFormatFps(U8 SetFormat, U8 Fps);
void	SetS5K6AASharpness(U8 bySetValue);
void SetS5K6AAEffect(U8 byEffect);
void SetS5K6AAImgDir(U8 bySnrImgDir);
void SetS5K6AABandingFilter(U8 byFPS, U8 byLightFrq);
void SetS5K6AAWBTemp(U16 wRgain, U16 wGgain, U16 wBgain);
void SetS5K6AAWBTempAuto(U8 bySetValue);
void SetS5K6AABackLightComp(U8 bySetValue);
void SetS5K6AAIntegrationTimeAuto(U8 bySetValue);
void SetS5K6AAIntegrationTime(U32 dwSetValue);
void SetS5K6AAOutputDim(U16 wWidth, U16 wHeight);
void CfgS5K6AAControlAttr(void);
void SetS5K6AAGain(float fGain);
extern code OV_CTT_t gc_S5K6AA_CTT[3];
#endif // _Sam6AA_H_