#include "Inc.h"



#ifdef RTS58XX_SP_MI2020

/*

t_RegSettingWW code gc_MI2020_Setting[]=
{
#ifndef MI2020_USE_SOC_LSC
	{0x3386,0x0501},//Disable MCU
	{0x3212,0x0000},//select raw bayer data output
#else
	{0x3210,0x0004},//enable soc lens shading. disable others
	{0x3212,0x0001},//select SOC no FIFO
	{0x332e,0x0100}, //processed bayer pattern.
#endif
//	{0x301a,0x0a08},//parallel  disable , stop streaming,unlock register..
	//master_clk =0.125* clk_in * pll_m/(pll_n+1)
	//pixclk = master_clk /2 , /3, /4
	//power off pll
//	{0x341E, 0x8f09},
	//setting  pll
//	{0x341C, 0x0250},//pll_m=80,pll_n=2
	//power up pll
//	{0x341E, 0x8f09},
	//power off pll_bypass
//	{0x341E, 0x8f08},

	{0x3044, 0x0542},//from MI2020 Wizard AP
	{0x3216,0x02cf}, //from MI2020 Wizard AP

//	{0x321c,0x06E6}//from MT9D112-REV3.ini, make slope work for 80Mhz Mclk.
};
*/

code MI2020_FpsSetting_t  g_staMi2020SVGAFpsSetting[]=
{
	{1,/*CCS_HCLK_24M,*/(808+8192),(608+1614),2000,0x0228,20000000},
	{3,/*CCS_HCLK_24M,*/(808+6000),(608+371),1634,0x0228,20000000},
	{5,/*CCS_HCLK_24M,*/(808+1000),(608+1604),704,0x0228,20000000},
	{8,/*CCS_HCLK_24M,*/(808+1000),(608+2985),413,0x0228,20000000},
	{9,/*CCS_HCLK_24M,*/(808+2564), (608+51),    74,0x0228,20000000},
	{10,/*CCS_HCLK_24M,*/(808+2226), (608+51),594,0x0228,20000000},
	{15,/*CCS_HCLK_24M,*/(808+1215),(608+51),176,0x0228,20000000},
	{20,/*CCS_HCLK_24M,*/(808+743),(608+51),618,0x0A96,20455000},
	//23
	{23,/*CCS_HCLK_24M,*/(808+745),(608+51),255,0x099d,23550000},
	//25
	{25,/*CCS_HCLK_24M,*/(808+747),(608+51),255,0x0BCD,25625000},
	{30,/*CCS_HCLK_24M,*/(808+743),(608+51),239,0x08B8,30666667}
};

code MI2020_FpsSetting_t  g_staMi2020UXGAFpsSetting[]=
{
	{1,/*CCS_HCLK_24M,*/(1608+8192),(1208+832),4095,0x0228,20000000},
	{3,/*CCS_HCLK_24M,*/(1608+3678),(1208+53),1020,0x0228,20000000},
	{5,/*CCS_HCLK_24M,*/(1608 + 1035),(1208 + 53),510,0x0864, 16666667},  //get 6fps setting from 2020 wizard, then change PLL control
	///
	{7,/*CCS_HCLK_24M,*/(1608 + 657),(1208 + 53),977,0x0228, 20000000},  //get 6fps setting from 2020 wizard, then change PLL control

	{8,/*CCS_HCLK_24M,*/(1608 + 486),(1208 + 53),   91,0x0BA9, 21125000},
	{9,/*CCS_HCLK_24M,*/(1608 + 477),(1208 + 53), 444,0x088e, 23666667},
	{10,/*CCS_HCLK_24M,*/(1608 + 479),(1208 + 53),111,0x0ac1, 26318000},
	{11,/*CCS_HCLK_24M,*/(1608 + 476),(1208 + 53),175,0x0aD4, 28909000},
	//
	{12,/*CCS_HCLK_24M,*/(1608 + 481),(1208 + 53),1187,0x0BFD, 31625000},

	{15,/*CCS_HCLK_24M,*/(1608 + 476),(1208 + 53),647,0x06B8,39429000}
};


t_RegSettingWW code gc_MI2020_SOC_LSC_Setting[]=
{
//[Lens Correction 02/05/09 17:35:53]
	{ 0x34CE, 0x01A8},     // LENS_CORRECTION_CONTROL
	{ 0x34D0, 0x6633},     // ZONE_BOUNDS_X1_X2
	{ 0x34D2, 0x319A},     // ZONE_BOUNDS_X0_X3
	{ 0x34D4, 0x9463},     // ZONE_BOUNDS_X4_X5
	{ 0x34D6, 0x4B25},     // ZONE_BOUNDS_Y1_Y2
	{ 0x34D8, 0x2670},     // ZONE_BOUNDS_Y0_Y3
	{ 0x34DA, 0x724C},     // ZONE_BOUNDS_Y4_Y5
	{ 0x34DC, 0xFF04},     // CENTER_OFFSET
	{ 0x34DE, 0x0143},     // FX_RED
	{ 0x34E6, 0x012F},     // FY_RED
	{ 0x34EE, 0x0BFA},     // DF_DX_RED
	{ 0x34F6, 0x0EE1},     // DF_DY_RED
	{ 0x3500, 0xB7F5},     // SECOND_DERIV_ZONE_0_RED
	{ 0x3508, 0x2A00},     // SECOND_DERIV_ZONE_1_RED
	{ 0x3510, 0x3556},     // SECOND_DERIV_ZONE_2_RED
	{ 0x3518, 0x346D},     // SECOND_DERIV_ZONE_3_RED
	{ 0x3520, 0x357B},     // SECOND_DERIV_ZONE_4_RED
	{ 0x3528, 0x4141},     // SECOND_DERIV_ZONE_5_RED
	{ 0x3530, 0x19AA},     // SECOND_DERIV_ZONE_6_RED
	{ 0x3538, 0xE2B1},     // SECOND_DERIV_ZONE_7_RED
	{ 0x354C, 0x07FF},     // K_FACTOR_IN_K_FX_FY_R_TL
	{ 0x3544, 0x07FF},     // K_FACTOR_IN_K_FX_FY_R_TR
	{ 0x355C, 0x0472},     // K_FACTOR_IN_K_FX_FY_R_BL
	{ 0x3554, 0x07FF},     // K_FACTOR_IN_K_FX_FY_R_BR
	{ 0x34E0, 0x0149},     // FX_GREEN
	{ 0x34E8, 0x00D3},     // FY_GREEN
	{ 0x34F0, 0x0D70},     // DF_DX_GREEN
	{ 0x34F8, 0x0E97},     // DF_DY_GREEN
	{ 0x3502, 0xDAD1},     // SECOND_DERIV_ZONE_0_GREEN
	{ 0x350A, 0x10D6},     // SECOND_DERIV_ZONE_1_GREEN
	{ 0x3512, 0x2551},     // SECOND_DERIV_ZONE_2_GREEN
	{ 0x351A, 0x285E},     // SECOND_DERIV_ZONE_3_GREEN
	{ 0x3522, 0x2B56},     // SECOND_DERIV_ZONE_4_GREEN
	{ 0x352A, 0x2725},     // SECOND_DERIV_ZONE_5_GREEN
	{ 0x3532, 0x08ED},     // SECOND_DERIV_ZONE_6_GREEN
	{ 0x353A, 0x0112},     // SECOND_DERIV_ZONE_7_GREEN
	{ 0x354E, 0x07FF},     // K_FACTOR_IN_K_FX_FY_G1_TL
	{ 0x3546, 0x07FF},     // K_FACTOR_IN_K_FX_FY_G1_TR
	{ 0x355E, 0x0600},     // K_FACTOR_IN_K_FX_FY_G1_BL
	{ 0x3556, 0x018E},     // K_FACTOR_IN_K_FX_FY_G1_BR
	{ 0x34E4, 0x00ED},     // FX_BLUE
	{ 0x34EC, 0x0052},     // FY_BLUE
	{ 0x34F4, 0x0E56},     // DF_DX_BLUE
	{ 0x34FC, 0x015C},     // DF_DY_BLUE
	{ 0x3506, 0xC7D6},     // SECOND_DERIV_ZONE_0_BLUE
	{ 0x350E, 0x1DE6},     // SECOND_DERIV_ZONE_1_BLUE
	{ 0x3516, 0x1C35},     // SECOND_DERIV_ZONE_2_BLUE
	{ 0x351E, 0x2540},     // SECOND_DERIV_ZONE_3_BLUE
	{ 0x3526, 0x1541},     // SECOND_DERIV_ZONE_4_BLUE
	{ 0x352E, 0x091E},     // SECOND_DERIV_ZONE_5_BLUE
	{ 0x3536, 0xFEE8},     // SECOND_DERIV_ZONE_6_BLUE
	{ 0x353E, 0xF704},     // SECOND_DERIV_ZONE_7_BLUE
	{ 0x3552, 0x05A1},     // K_FACTOR_IN_K_FX_FY_B_TL
	{ 0x354A, 0x03FF},     // K_FACTOR_IN_K_FX_FY_B_TR
	{ 0x3562, 0x018E},     // K_FACTOR_IN_K_FX_FY_B_BL
	{ 0x355A, 0x018E},     // K_FACTOR_IN_K_FX_FY_B_BR
	{ 0x34E2, 0x00E2},     // FX_GREEN2
	{ 0x34EA, 0x0079},     // FY_GREEN2
	{ 0x34F2, 0x0CD8},     // DF_DX_GREEN2
	{ 0x34FA, 0x0F86},     // DF_DY_GREEN2
	{ 0x3504, 0xDB07},     // SECOND_DERIV_ZONE_0_GREEN2
	{ 0x350C, 0x1705},     // SECOND_DERIV_ZONE_1_GREEN2
	{ 0x3514, 0x2732},     // SECOND_DERIV_ZONE_2_GREEN2
	{ 0x351C, 0x2E5F},     // SECOND_DERIV_ZONE_3_GREEN2
	{ 0x3524, 0x1A57},     // SECOND_DERIV_ZONE_4_GREEN2
	{ 0x352C, 0x1426},     // SECOND_DERIV_ZONE_5_GREEN2
	{ 0x3534, 0xFA9D},     // SECOND_DERIV_ZONE_6_GREEN2
	{ 0x353C, 0xDBE0},     // SECOND_DERIV_ZONE_7_GREEN2
	{ 0x3550, 0x03FF},     // K_FACTOR_IN_K_FX_FY_G2_TL
	{ 0x3548, 0x07FF},     // K_FACTOR_IN_K_FX_FY_G2_TR
	{ 0x3560, 0x03FF},     // K_FACTOR_IN_K_FX_FY_G2_BL
	{ 0x3558, 0x07FF},     // K_FACTOR_IN_K_FX_FY_G2_BR
	{ 0x3540, 0x01A0},     // X2_FACTORS
	{ 0x3542, 0x0000},     // GLOBAL_OFFSET_FXY_FUNCTION
	/*
	STATE= Lens Correction Center X, 820
	STATE= Lens Correction Center Y, 600
	BITFIELD= 0x3210, 0x0004, 1 //LENS_CORRECTION
	//[Lens shading correction 09-02-06]
	{0x34CE, 0x21A8}, 	// LENS_CORRECTION_CONTROL
	{0x34D0, 0x6432}, 	// ZONE_BOUNDS_X1_X2
	{0x34D2, 0x3297}, 	// ZONE_BOUNDS_X0_X3
	{0x34D4, 0x9765}, 	// ZONE_BOUNDS_X4_X5
	{0x34D6, 0x4B26}, 	// ZONE_BOUNDS_Y1_Y2
	{0x34D8, 0x2671}, 	// ZONE_BOUNDS_Y0_Y3
	{0x34DA, 0x714C}, 	// ZONE_BOUNDS_Y4_Y5
	{0x34DC, 0x0000}, 	// CENTER_OFFSET
	{0x34DE, 0x0143}, 	// FX_RED
	{0x34E6, 0x012F}, 	// FY_RED
	{0x34EE, 0x0C7A}, 	// DF_DX_RED
	{0x34F6, 0x0A94}, 	// DF_DY_RED
	{0x3500, 0x07CA}, 	// SECOND_DERIV_ZONE_0_RED
	{0x3508, 0x0C0B}, 	// SECOND_DERIV_ZONE_1_RED
	{0x3510, 0x316B}, 	// SECOND_DERIV_ZONE_2_RED
	{0x3518, 0x3B69}, 	// SECOND_DERIV_ZONE_3_RED
	{0x3520, 0x397D}, 	// SECOND_DERIV_ZONE_4_RED
	{0x3528, 0x4242}, 	// SECOND_DERIV_ZONE_5_RED
	{0x3530, 0x0BCF}, 	// SECOND_DERIV_ZONE_6_RED
	{0x3538, 0x22A0}, 	// SECOND_DERIV_ZONE_7_RED
	{0x354C, 0x0609}, 	// K_FACTOR_IN_K_FX_FY_R_TL
	{0x3544, 0x058B}, 	// K_FACTOR_IN_K_FX_FY_R_TR
	{0x355C, 0x05B1}, 	// K_FACTOR_IN_K_FX_FY_R_BL
	{0x3554, 0x060A}, 	// K_FACTOR_IN_K_FX_FY_R_BR
	{0x34E0, 0x0123}, 	// FX_GREEN
	{0x34E8, 0x00E0}, 	// FY_GREEN
	{0x34F0, 0x0EF6}, 	// DF_DX_GREEN
	{0x34F8, 0x0BC4}, 	// DF_DY_GREEN
	{0x3502, 0x129E}, 	// SECOND_DERIV_ZONE_0_GREEN
	{0x350A, 0x05C3}, 	// SECOND_DERIV_ZONE_1_GREEN
	{0x3512, 0x1F61}, 	// SECOND_DERIV_ZONE_2_GREEN
	{0x351A, 0x2B5A}, 	// SECOND_DERIV_ZONE_3_GREEN
	{0x3522, 0x2F5A}, 	// SECOND_DERIV_ZONE_4_GREEN
	{0x352A, 0x2626}, 	// SECOND_DERIV_ZONE_5_GREEN
	{0x3532, 0x0B0A}, 	// SECOND_DERIV_ZONE_6_GREEN
	{0x353A, 0x1C31}, 	// SECOND_DERIV_ZONE_7_GREEN
	{0x354E, 0x064F}, 	// K_FACTOR_IN_K_FX_FY_G1_TL
	{0x3546, 0x0576}, 	// K_FACTOR_IN_K_FX_FY_G1_TR
	{0x355E, 0x067D}, 	// K_FACTOR_IN_K_FX_FY_G1_BL
	{0x3556, 0x0483}, 	// K_FACTOR_IN_K_FX_FY_G1_BR
	{0x34E4, 0x00E6}, 	// FX_BLUE
	{0x34EC, 0x006F}, 	// FY_BLUE
	{0x34F4, 0x0EE8}, 	// DF_DX_BLUE
	{0x34FC, 0x0C24}, 	// DF_DY_BLUE
	{0x3506, 0x41BA}, 	// SECOND_DERIV_ZONE_0_BLUE
	{0x350E, 0xFADD}, 	// SECOND_DERIV_ZONE_1_BLUE
	{0x3516, 0x1946}, 	// SECOND_DERIV_ZONE_2_BLUE
	{0x351E, 0x2542}, 	// SECOND_DERIV_ZONE_3_BLUE
	{0x3526, 0x1D44}, 	// SECOND_DERIV_ZONE_4_BLUE
	{0x352E, 0x101F}, 	// SECOND_DERIV_ZONE_5_BLUE
	{0x3536, 0xEF05}, 	// SECOND_DERIV_ZONE_6_BLUE
	{0x353E, 0x1E4C}, 	// SECOND_DERIV_ZONE_7_BLUE
	{0x3552, 0x061D}, 	// K_FACTOR_IN_K_FX_FY_B_TL
	{0x354A, 0x0202}, 	// K_FACTOR_IN_K_FX_FY_B_TR
	{0x3562, 0x06E5}, 	// K_FACTOR_IN_K_FX_FY_B_BL
	{0x355A, 0x0497}, 	// K_FACTOR_IN_K_FX_FY_B_BR
	{0x34E2, 0x00F2}, 	// FX_GREEN2
	{0x34EA, 0x00A6}, 	// FY_GREEN2
	{0x34F2, 0x0C81}, 	// DF_DX_GREEN2
	{0x34FA, 0x0BB1}, 	// DF_DY_GREEN2
	{0x3504, 0x1F08}, 	// SECOND_DERIV_ZONE_0_GREEN2
	{0x350C, 0x1504}, 	// SECOND_DERIV_ZONE_1_GREEN2
	{0x3514, 0x2144}, 	// SECOND_DERIV_ZONE_2_GREEN2
	{0x351C, 0x3058}, 	// SECOND_DERIV_ZONE_3_GREEN2
	{0x3524, 0x2660}, 	// SECOND_DERIV_ZONE_4_GREEN2
	{0x352C, 0x1C2B}, 	// SECOND_DERIV_ZONE_5_GREEN2
	{0x3534, 0x06D5}, 	// SECOND_DERIV_ZONE_6_GREEN2
	{0x353C, 0x04D1}, 	// SECOND_DERIV_ZONE_7_GREEN2
	{0x3550, 0x0019}, 	// K_FACTOR_IN_K_FX_FY_G2_TL
	{0x3548, 0x05BD}, 	// K_FACTOR_IN_K_FX_FY_G2_TR
	{0x3560, 0x059F}, 	// K_FACTOR_IN_K_FX_FY_G2_BL
	{0x3558, 0x061E}, 	// K_FACTOR_IN_K_FX_FY_G2_BR
	{0x3540, 0x0020}, 	// X2_FACTORS
	{0x3542, 0x0000} 	// GLOBAL_OFFSET_FXY_FUNCTION


	*/
	//[Len=s Correction 12/18/08 16:48:31]
	/*
	{0x34CE, 0x81A0}, 	// LENS_CORRECTION_CONTROL
	{0x34D0, 0x6432}, 	// ZONE_BOUNDS_X1_X2
	{0x34D2, 0x3296}, 	// ZONE_BOUNDS_X0_X3
	{0x34D4, 0x9664}, 	// ZONE_BOUNDS_X4_X5
	{0x34D6, 0x5028}, 	// ZONE_BOUNDS_Y1_Y2
	{0x34D8, 0x2878}, 	// ZONE_BOUNDS_Y0_Y3
	{0x34DA, 0x7850}, 	// ZONE_BOUNDS_Y4_Y5
	{0x34DC, 0x0000}, 	// CENTER_OFFSET
	{0x34DE, 0x0157}, 	// FX_RED
	{0x34E6, 0x007A}, 	// FY_RED
	{0x34EE, 0x0D44}, 	// DF_DX_RED
	{0x34F6, 0x0FDF}, 	// DF_DY_RED
	{0x3500, 0x9E0F}, 	// SECOND_DERIV_ZONE_0_RED
	{0x3508, 0x0A09}, 	// SECOND_DERIV_ZONE_1_RED
	{0x3510, 0x2A2C}, 	// SECOND_DERIV_ZONE_2_RED
	{0x3518, 0x353B}, 	// SECOND_DERIV_ZONE_3_RED
	{0x3520, 0x4040}, 	// SECOND_DERIV_ZONE_4_RED
	{0x3528, 0x424F}, 	// SECOND_DERIV_ZONE_5_RED
	{0x3530, 0x22EA}, 	// SECOND_DERIV_ZONE_6_RED
	{0x3538, 0xACA1}, 	// SECOND_DERIV_ZONE_7_RED
	{0x354C, 0x05F4}, 	// K_FACTOR_IN_K_FX_FY_R_TL
	{0x3544, 0x0646}, 	// K_FACTOR_IN_K_FX_FY_R_TR
	{0x355C, 0x046B}, 	// K_FACTOR_IN_K_FX_FY_R_BL
	{0x3554, 0x0492}, 	// K_FACTOR_IN_K_FX_FY_R_BR
	{0x34E0, 0x0153}, 	// FX_GREEN
	{0x34E8, 0x0039}, 	// FY_GREEN
	{0x34F0, 0x0E3D}, 	// DF_DX_GREEN
	{0x34F8, 0x0074}, 	// DF_DY_GREEN
	{0x3502, 0xBEF0}, 	// SECOND_DERIV_ZONE_0_GREEN
	{0x350A, 0x04F6}, 	// SECOND_DERIV_ZONE_1_GREEN
	{0x3512, 0x1630}, 	// SECOND_DERIV_ZONE_2_GREEN
	{0x351A, 0x212C}, 	// SECOND_DERIV_ZONE_3_GREEN
	{0x3522, 0x302D}, 	// SECOND_DERIV_ZONE_4_GREEN
	{0x352A, 0x2824}, 	// SECOND_DERIV_ZONE_5_GREEN
	{0x3532, 0x1605}, 	// SECOND_DERIV_ZONE_6_GREEN
	{0x353A, 0xE8EC}, 	// SECOND_DERIV_ZONE_7_GREEN
	{0x354E, 0x076A}, 	// K_FACTOR_IN_K_FX_FY_G1_TL
	{0x3546, 0x0704}, 	// K_FACTOR_IN_K_FX_FY_G1_TR
	{0x355E, 0x0445}, 	// K_FACTOR_IN_K_FX_FY_G1_BL
	{0x3556, 0x0410}, 	// K_FACTOR_IN_K_FX_FY_G1_BR
	{0x34E4, 0x0121}, 	// FX_BLUE
	{0x34EC, 0x0022}, 	// FY_BLUE
	{0x34F4, 0x0E1B}, 	// DF_DX_BLUE
	{0x34FC, 0x0F8D}, 	// DF_DY_BLUE
	{0x3506, 0xE804}, 	// SECOND_DERIV_ZONE_0_BLUE
	{0x350E, 0x0FF9}, 	// SECOND_DERIV_ZONE_1_BLUE
	{0x3516, 0x1A29}, 	// SECOND_DERIV_ZONE_2_BLUE
	{0x351E, 0x2623}, 	// SECOND_DERIV_ZONE_3_BLUE
	{0x3526, 0x2520}, 	// SECOND_DERIV_ZONE_4_BLUE
	{0x352E, 0x1920}, 	// SECOND_DERIV_ZONE_5_BLUE
	{0x3536, 0x0719}, 	// SECOND_DERIV_ZONE_6_BLUE
	{0x353E, 0xB6DD}, 	// SECOND_DERIV_ZONE_7_BLUE
	{0x3552, 0x07FF}, 	// K_FACTOR_IN_K_FX_FY_B_TL
	{0x354A, 0x0582}, 	// K_FACTOR_IN_K_FX_FY_B_TR
	{0x3562, 0x0404}, 	// K_FACTOR_IN_K_FX_FY_B_BL
	{0x355A, 0x0437}, 	// K_FACTOR_IN_K_FX_FY_B_BR
	{0x34E2, 0x00FB}, 	// FX_GREEN2
	{0x34EA, 0x0027}, 	// FY_GREEN2
	{0x34F2, 0x0DB0}, 	// DF_DX_GREEN2
	{0x34FA, 0x0042}, 	// DF_DY_GREEN2
	{0x3504, 0xC419}, 	// SECOND_DERIV_ZONE_0_GREEN2
	{0x350C, 0x1307}, 	// SECOND_DERIV_ZONE_1_GREEN2
	{0x3514, 0x1B21}, 	// SECOND_DERIV_ZONE_2_GREEN2
	{0x351C, 0x3331}, 	// SECOND_DERIV_ZONE_3_GREEN2
	{0x3524, 0x3132}, 	// SECOND_DERIV_ZONE_4_GREEN2
	{0x352C, 0x2137}, 	// SECOND_DERIV_ZONE_5_GREEN2
	{0x3534, 0x09DA}, 	// SECOND_DERIV_ZONE_6_GREEN2
	{0x353C, 0xCACC}, 	// SECOND_DERIV_ZONE_7_GREEN2
	{0x3550, 0x0080}, 	// K_FACTOR_IN_K_FX_FY_G2_TL
	{0x3548, 0x07FF}, 	// K_FACTOR_IN_K_FX_FY_G2_TR
	{0x3560, 0x0428}, 	// K_FACTOR_IN_K_FX_FY_G2_BL
	{0x3558, 0x049C}, 	// K_FACTOR_IN_K_FX_FY_G2_BR
	{0x3540, 0x0080}, 	// X2_FACTORS
	{0x3542, 0x0000} 	// GLOBAL_OFFSET_FXY_FUNCTION
	*/
//OLD SETTING
	/* {0x34CE, 0x81A0}, 	// LENS_CORRECTION_CONTROL
	{ 0x34D0, 0x6231}, 	// ZONE_BOUNDS_X1_X2
	{ 0x34D2, 0x3394}, 	// ZONE_BOUNDS_X0_X3
	{ 0x34D4, 0x9A67}, 	// ZONE_BOUNDS_X4_X5
	{ 0x34D6, 0x4221}, 	// ZONE_BOUNDS_Y1_Y2
	{ 0x34D8, 0x2B63}, 	// ZONE_BOUNDS_Y0_Y3
	{ 0x34DA, 0x8055}, 	// ZONE_BOUNDS_Y4_Y5
	{ 0x34DC, 0xEDFC}, 	// CENTER_OFFSET
	{ 0x34DE, 0x0107}, 	// FX_RED
	{ 0x34E6, 0x008D}, 	// FY_RED
	{ 0x34EE, 0x0DA7}, 	// DF_DX_RED
	{ 0x34F6, 0x0D53}, 	// DF_DY_RED
	{ 0x3500, 0x0117}, 	// SECOND_DERIV_ZONE_0_RED
	{ 0x3508, 0x0508}, 	// SECOND_DERIV_ZONE_1_RED
	{ 0x3510, 0x1620}, 	// SECOND_DERIV_ZONE_2_RED
	{ 0x3518, 0x2C30}, 	// SECOND_DERIV_ZONE_3_RED
	{ 0x3520, 0x2C2F}, 	// SECOND_DERIV_ZONE_4_RED
	{ 0x3528, 0x363F}, 	// SECOND_DERIV_ZONE_5_RED
	{ 0x3530, 0x26EA}, 	// SECOND_DERIV_ZONE_6_RED
	{ 0x3538, 0xC6BD}, 	// SECOND_DERIV_ZONE_7_RED
	{ 0x354C, 0x04F6}, 	// K_FACTOR_IN_K_FX_FY_R_TL
	{ 0x3544, 0x0592}, 	// K_FACTOR_IN_K_FX_FY_R_TR
	{ 0x355C, 0x0009}, 	// K_FACTOR_IN_K_FX_FY_R_BL
	{ 0x3554, 0x0452}, 	// K_FACTOR_IN_K_FX_FY_R_BR
	{ 0x34E0, 0x013C}, 	// FX_GREEN
	{ 0x34E8, 0x0071}, 	// FY_GREEN
	{ 0x34F0, 0x0E48}, 	// DF_DX_GREEN
	{ 0x34F8, 0x0D94}, 	// DF_DY_GREEN
	{ 0x3502, 0x0CF4}, 	// SECOND_DERIV_ZONE_0_GREEN
	{ 0x350A, 0x07F9}, 	// SECOND_DERIV_ZONE_1_GREEN
	{ 0x3512, 0x0C28}, 	// SECOND_DERIV_ZONE_2_GREEN
	{ 0x351A, 0x1F28}, 	// SECOND_DERIV_ZONE_3_GREEN
	{ 0x3522, 0x2C2E}, 	// SECOND_DERIV_ZONE_4_GREEN
	{ 0x352A, 0x2E27}, 	// SECOND_DERIV_ZONE_5_GREEN
	{ 0x3532, 0x1D04}, 	// SECOND_DERIV_ZONE_6_GREEN
	{ 0x353A, 0xF9F2}, 	// SECOND_DERIV_ZONE_7_GREEN
	{ 0x354E, 0x057F}, 	// K_FACTOR_IN_K_FX_FY_G1_TL
	{ 0x3546, 0x059A}, 	// K_FACTOR_IN_K_FX_FY_G1_TR
	{ 0x355E, 0x0436}, 	// K_FACTOR_IN_K_FX_FY_G1_BL
	{ 0x3556, 0x0010}, 	// K_FACTOR_IN_K_FX_FY_G1_BR
	{ 0x34E4, 0x0122}, 	// FX_BLUE
	{ 0x34EC, 0x0055}, 	// FY_BLUE
	{ 0x34F4, 0x0E1A}, 	// DF_DX_BLUE
	{ 0x34FC, 0x0D25}, 	// DF_DY_BLUE
	{ 0x3506, 0x2003}, 	// SECOND_DERIV_ZONE_0_BLUE
	{ 0x350E, 0x12FD}, 	// SECOND_DERIV_ZONE_1_BLUE
	{ 0x3516, 0x1324}, 	// SECOND_DERIV_ZONE_2_BLUE
	{ 0x351E, 0x2722}, 	// SECOND_DERIV_ZONE_3_BLUE
	{ 0x3526, 0x2C23}, 	// SECOND_DERIV_ZONE_4_BLUE
	{ 0x352E, 0x1C21}, 	// SECOND_DERIV_ZONE_5_BLUE
	{ 0x3536, 0x1C17}, 	// SECOND_DERIV_ZONE_6_BLUE
	{ 0x353E, 0xC3E2}, 	// SECOND_DERIV_ZONE_7_BLUE
	{ 0x3552, 0x0622}, 	// K_FACTOR_IN_K_FX_FY_B_TL
	{ 0x354A, 0x04D1}, 	// K_FACTOR_IN_K_FX_FY_B_TR
	{ 0x3562, 0x0441}, 	// K_FACTOR_IN_K_FX_FY_B_BL
	{ 0x355A, 0x0447}, 	// K_FACTOR_IN_K_FX_FY_B_BR
	{ 0x34E2, 0x0104}, 	// FX_GREEN2
	{ 0x34EA, 0x0059}, 	// FY_GREEN2
	{ 0x34F2, 0x0DA1}, 	// DF_DX_GREEN2
	{ 0x34FA, 0x0DB7}, 	// DF_DY_GREEN2
	{ 0x3504, 0x0316}, 	// SECOND_DERIV_ZONE_0_GREEN2
	{ 0x350C, 0x170B}, 	// SECOND_DERIV_ZONE_1_GREEN2
	{ 0x3514, 0x1622}, 	// SECOND_DERIV_ZONE_2_GREEN2
	{ 0x351C, 0x2A30}, 	// SECOND_DERIV_ZONE_3_GREEN2
	{ 0x3524, 0x312B}, 	// SECOND_DERIV_ZONE_4_GREEN2
	{ 0x352C, 0x282F}, 	// SECOND_DERIV_ZONE_5_GREEN2
	{ 0x3534, 0x0EDE}, 	// SECOND_DERIV_ZONE_6_GREEN2
	{ 0x353C, 0xE5D8}, 	// SECOND_DERIV_ZONE_7_GREEN2
	{ 0x3550, 0x0408}, 	// K_FACTOR_IN_K_FX_FY_G2_TL
	{ 0x3548, 0x063D}, 	// K_FACTOR_IN_K_FX_FY_G2_TR
	{ 0x3560, 0x042D}, 	// K_FACTOR_IN_K_FX_FY_G2_BL
	{ 0x3558, 0x0466}, 	// K_FACTOR_IN_K_FX_FY_G2_BR
	{ 0x3540, 0x0080}, 	// X2_FACTORS
	{ 0x3542, 0x0000} 	// GLOBAL_OFFSET_FXY_FUNCTION
	*/

	/*
	STATE= Lens Correction Falloff, 100
	STATE= Lens Correction Center X, 788
	STATE= Lens Correction Center Y, 528
	BITFIELD= 0x3210, 0x0004, 1 //LENS_CORRECTION
	*/
};

//find propriety setting
MI2020_FpsSetting_t* GetMi2020FpsSetting(U8 Format,U8 Fps)
{
	U8 i;
	U8 Idx=0;
	if(g_bIsHighSpeed)
	{
		Idx= 2; //8fps for Sxga, 10 fps for svga
	}
	else
	{
		Idx = 1;
	}
	if(Format ==SVGA_FRM)
	{
		for(i=0; i< (sizeof(g_staMi2020SVGAFpsSetting)/sizeof(MI2020_FpsSetting_t)); i++)
		{
			if(g_staMi2020SVGAFpsSetting[i].byFps ==Fps)
			{
				Idx=i;
				break;
			}
		}
		return &g_staMi2020SVGAFpsSetting[Idx];
	}
	else//if(Format ==UXGA_FRM)
	{
		for(i=0; i< (sizeof(g_staMi2020UXGAFpsSetting)/sizeof(MI2020_FpsSetting_t)); i++)
		{
			if(g_staMi2020UXGAFpsSetting[i].byFps ==Fps)
			{
				Idx=i;
				break;
			}
		}
		return &g_staMi2020UXGAFpsSetting[Idx];

	}
}


void Mi2020Refresh()
{
	if(g_bySensorCurFormat==SVGA_FRM)
	{
		Write_MI_Vairable(MI_DRV_ID_SEQ|0X03|MIVAR_DATA_WIDTH_8_LOG, 6);
	}
	else
	{
		Write_MI_Vairable(MI_DRV_ID_SEQ|0X03|MIVAR_DATA_WIDTH_8_LOG, 6);
		WaitTimeOut_Delay(5);
		Write_MI_Vairable(MI_DRV_ID_SEQ|0X03|MIVAR_DATA_WIDTH_8_LOG, 1);
		WaitTimeOut_Delay(10);
		Write_MI_Vairable(MI_DRV_ID_SEQ|0X03|MIVAR_DATA_WIDTH_8_LOG, 2);
	}
	MI_PollWaitRefreshDone(50);
}


void	SetMi2020OutputDim(U16 wWidth,U16 wHeight)
{
	if(g_bySensorCurFormat==SVGA_FRM)
	{
		Write_MI_Vairable(MI_DRV_ID_MODE|0X03|MIVAR_DATA_WIDTH_16_LOG, wWidth);//output_Width_A;
		Write_MI_Vairable(MI_DRV_ID_MODE|0X05|MIVAR_DATA_WIDTH_16_LOG, wHeight);//output_ Height_A;
		//start operation mode
		Write_MI_Vairable(MI_DRV_ID_SEQ|0x20|MIVAR_DATA_WIDTH_8_LOG, 0x0000); //go to preview mode
		Write_MI_Vairable(MI_DRV_ID_SEQ|0x03|MIVAR_DATA_WIDTH_8_LOG, 0x0001); //go to preview mode

		Write_MI_Vairable(MI_DRV_ID_SEQ|0x03|MIVAR_DATA_WIDTH_8_LOG, 0x0006); //refresh mode
	}
	else
	{
		Write_MI_Vairable(MI_DRV_ID_MODE|0X07|MIVAR_DATA_WIDTH_16_LOG, wWidth);//output_Width_b;
		Write_MI_Vairable(MI_DRV_ID_MODE|0X09|MIVAR_DATA_WIDTH_16_LOG, wHeight);//output_ Height_b;

		//start operation mode
		Write_MI_Vairable(MI_DRV_ID_SEQ|0x20|MIVAR_DATA_WIDTH_8_LOG, 0x0072); //go to capture mode
		Write_MI_Vairable(MI_DRV_ID_SEQ|0x03|MIVAR_DATA_WIDTH_8_LOG, 0x0002); //go to Capture mode

	}
	MI_PollWaitRefreshDone(50);
	WaitTimeOut_Delay(2);
}


t_RegSettingWW code g_cMi2020VarSetting[]=
{
	//////////////////
	//Mode A
	///////////////////

	//MISC settings 1
	{MI_DRV_ID_MODE|0x17|MIVAR_DATA_WIDTH_16_LOG,0x2111 }, //row speed A
	{MI_DRV_ID_MODE|0x19|MIVAR_DATA_WIDTH_16_LOG,0x046c}, //Read mode A
	{MI_DRV_ID_MODE|0x95|MIVAR_DATA_WIDTH_16_LOG, 0x0002}, //output_format_A, swap chrominance byte with luminance.
	//timing
	{MI_DRV_ID_MODE|0x1b|MIVAR_DATA_WIDTH_16_LOG, 0x024f }, //sample time pck
	{MI_DRV_ID_MODE|0x1d|MIVAR_DATA_WIDTH_16_LOG, 0x0102 }, //fine_correction
	{MI_DRV_ID_MODE|0x1f|MIVAR_DATA_WIDTH_16_LOG, 633}, //Fine integration time min
	{MI_DRV_ID_MODE|0x21|MIVAR_DATA_WIDTH_16_LOG, 341}, //Fine integration time max margin
	//ADC setting
	{MI_DRV_ID_MODE|0x27|MIVAR_DATA_WIDTH_16_LOG, 0x2020}, //{0X3084,0x2020},//dac_id_4_5 = 0x2020
	{MI_DRV_ID_MODE|0x29|MIVAR_DATA_WIDTH_16_LOG, 0x2020}, //{0X3086,0x2020},//dac_id_6_7[7:0] = 0x20
	{MI_DRV_ID_MODE|0x2b|MIVAR_DATA_WIDTH_16_LOG, 0x1020}, //{0X3088,0x1020},//dac_id_8_9[7:0] = 0x20
	{MI_DRV_ID_MODE|0x2d|MIVAR_DATA_WIDTH_16_LOG, 0x2007}, //{0X308a,0x2007}//dac_id_10_11[15:8] = 0x20
	///////////////////
	//Mode B
	///////////////////
	//misc setting
	{MI_DRV_ID_MODE|0x39|MIVAR_DATA_WIDTH_16_LOG,0x2111 }, //row speed b
	{MI_DRV_ID_MODE|0x3b|MIVAR_DATA_WIDTH_16_LOG,0x0024 }, //Read mode b
	{MI_DRV_ID_MODE|0x97|MIVAR_DATA_WIDTH_16_LOG, 0x0002}, //output_format_B, swap chrominance byte with luminance.
	//timing
	{MI_DRV_ID_MODE|0x3d|MIVAR_DATA_WIDTH_16_LOG, 0x0120 }, //sample time pck
	{MI_DRV_ID_MODE|0x3f|MIVAR_DATA_WIDTH_16_LOG, 0x00a4}, //fine_correction
	{MI_DRV_ID_MODE|0x41|MIVAR_DATA_WIDTH_16_LOG, 361}, //Fine integration time min
	{MI_DRV_ID_MODE|0x43|MIVAR_DATA_WIDTH_16_LOG, 164}, //Fine integration time max margin
	//ADC setting
	{MI_DRV_ID_MODE|0x49|MIVAR_DATA_WIDTH_16_LOG, 0x2020}, //{0X3084,0x2020},//dac_id_4_5 = 0x2020
	{MI_DRV_ID_MODE|0x4b|MIVAR_DATA_WIDTH_16_LOG, 0x2020}, //{0X3086,0x2020},//dac_id_6_7[7:0] = 0x20
	{MI_DRV_ID_MODE|0x4b|MIVAR_DATA_WIDTH_16_LOG, 0x1020}, //{0X3088,0x1020},//dac_id_8_9[7:0] = 0x20
	{MI_DRV_ID_MODE|0x4f|MIVAR_DATA_WIDTH_16_LOG, 0x2007} //{0X308a,0x2007}//dac_id_10_11[15:8] = 0x20
};



void Mi2020SetFormatFps(U8 SetFormat,U8 Fps)
{
	U32 dwTmp1;
	U32 dwTmp2;
	U16 wTmp1;


	MI2020_FpsSetting_t* pMi2020_FpsSetting;

	pMi2020_FpsSetting = GetMi2020FpsSetting( SetFormat, Fps);
	Write_SenReg(0x301a,0x0a00);//paraller disable, stop driving pin, stop streaming, unlock registers
	Write_SenReg(0x3212,0x0001);//select SOC no FIFO

//	g_dwPixelClk = pMi2020_FpsSetting->dwPixelClk;

	//PLL control
	Write_SenReg(0x341E,0x8F09); //PLL/ Clk_in control: BYPASS PLL
	Write_SenReg(0x341C,pMi2020_FpsSetting->wPLLCtlReg);
	WaitTimeOut_Delay(1);                   // Allow PLL to lock
	Write_SenReg(0x341E,0x8F09);  //PLL/ Clk_in control: PLL ON, bypassed = 36617
	Write_SenReg(0x341E, 0x8F08); //PLL/ Clk_in control: USE PLL = 36616


	//YUV_Ycbcr_control
	//sRGB color space ,ITU 601 Range.
	Write_SenReg(0x337C, 0x000d);




	//anti flick
	dwTmp1   =  pMi2020_FpsSetting->wHorizClks*100000 ;
	dwTmp1 /= (pMi2020_FpsSetting->dwPixelClk/10); //line time, unit: 1us.
	//60Hz
	wTmp1     =  (1000000/120) / dwTmp1;                           //50Hz lines
	dwTmp2   =  (1000000/120) -  (dwTmp1*wTmp1);       //remainder
	if(dwTmp2 > (dwTmp1/2))
	{
		wTmp1+=1;
	}
	Write_MI_Vairable(MI_DRV_ID_ANTF|0x11|MIVAR_DATA_WIDTH_16_LOG,wTmp1 ); //60Hz R9 step
	Write_MI_Vairable(MI_DRV_ID_ANTF|0x15|MIVAR_DATA_WIDTH_16_LOG,wTmp1 ); //60Hz R9 step
	//50Hz
	wTmp1     =  (1000000/100) / dwTmp1;                           //50Hz lines
	dwTmp2   =  (1000000/100) -  (dwTmp1*wTmp1);       //remainder
	if(dwTmp2 > (dwTmp1/2))
	{
		wTmp1+=1;
	}
	Write_MI_Vairable(MI_DRV_ID_ANTF|0x13|MIVAR_DATA_WIDTH_16_LOG,wTmp1); //50Hz R9 step
	Write_MI_Vairable(MI_DRV_ID_ANTF|0x17|MIVAR_DATA_WIDTH_16_LOG,wTmp1); //50Hz R9 step
	wTmp1  /=5;

	Write_MI_Vairable(MI_DRV_ID_ANTF|0x8|MIVAR_DATA_WIDTH_8_LOG, wTmp1 ); //search_f1_50
	Write_MI_Vairable(MI_DRV_ID_ANTF|0x9|MIVAR_DATA_WIDTH_8_LOG, (wTmp1+2) ); //search_f2_50

	Write_MI_Vairable(MI_DRV_ID_ANTF|0xa|MIVAR_DATA_WIDTH_8_LOG, (wTmp1+3) ); //search_f1_60
	Write_MI_Vairable(MI_DRV_ID_ANTF|0xb|MIVAR_DATA_WIDTH_8_LOG, (wTmp1+5) ); //search_f2_60

	WriteMIVarSettingWW((sizeof(g_cMi2020VarSetting)/sizeof(t_RegSettingWW)), g_cMi2020VarSetting);

	if((g_bySensorSPFormat & SetFormat) == SVGA_FRM) //SVGA_FRM
	{
		//frame rate
		Write_MI_Vairable(MI_DRV_ID_MODE|0x15|MIVAR_DATA_WIDTH_16_LOG, pMi2020_FpsSetting->wExtraDelay); //sensor extra delay A
		Write_MI_Vairable(MI_DRV_ID_MODE|0x23|MIVAR_DATA_WIDTH_16_LOG, pMi2020_FpsSetting->wVertRows); //sensor Frame length lines A
		Write_MI_Vairable(MI_DRV_ID_MODE|0x25|MIVAR_DATA_WIDTH_16_LOG, pMi2020_FpsSetting->wHorizClks); //sensor line length pck A

	}
	else   //UXGA_FRM
	{
		//frame rate
		Write_MI_Vairable(MI_DRV_ID_MODE|0x45|MIVAR_DATA_WIDTH_16_LOG, pMi2020_FpsSetting->wVertRows); //sensor Frame length lines B
		Write_MI_Vairable(MI_DRV_ID_MODE|0x47|MIVAR_DATA_WIDTH_16_LOG, pMi2020_FpsSetting->wHorizClks); //sensor line length pck B
		Write_MI_Vairable(MI_DRV_ID_MODE|0x37|MIVAR_DATA_WIDTH_16_LOG, pMi2020_FpsSetting->wExtraDelay); //sensor extra delay B
	}
	Write_SenReg(0x301a,0x0acc);//paraller enable,  drivin pin, start streaming, lock registers
	Write_SenReg(0x3210,0x01F8); //

	SetMi2020ISPMisc();
}



void SetMi2020WindowCrop(U16 wXStart,U16 wXEnd,U16 wYStart,U16 wYEnd )
{

	//DBG(("@12-->%d,%d,%d,%d\n",wXStart, wXEnd, wYStart, wYEnd));
	if(g_bySensorCurFormat==SVGA_FRM)
	{
		Write_MI_Vairable(MI_DRV_ID_MODE|0X51|MIVAR_DATA_WIDTH_16_LOG, wXStart);//crop_x0_A;
		Write_MI_Vairable(MI_DRV_ID_MODE|0X53|MIVAR_DATA_WIDTH_16_LOG, wXEnd);//crop_x1_A;
		Write_MI_Vairable(MI_DRV_ID_MODE|0X55|MIVAR_DATA_WIDTH_16_LOG, wYStart);//crop_Y0_A;
		Write_MI_Vairable(MI_DRV_ID_MODE|0X57|MIVAR_DATA_WIDTH_16_LOG, wYEnd);//crop_Y1_A;
	}
	else
	{
		Write_MI_Vairable(MI_DRV_ID_MODE|0X5f|MIVAR_DATA_WIDTH_16_LOG, wXStart);//crop_x0_B;
		Write_MI_Vairable(MI_DRV_ID_MODE|0X61|MIVAR_DATA_WIDTH_16_LOG, wXEnd);//crop_x1_B;
		Write_MI_Vairable(MI_DRV_ID_MODE|0X63|MIVAR_DATA_WIDTH_16_LOG, wYStart);//crop_Y0_B;
		Write_MI_Vairable(MI_DRV_ID_MODE|0X65|MIVAR_DATA_WIDTH_16_LOG, wYEnd);//crop_Y1_B;
	}
}

#ifndef _USE_BK_SPECIAL_EFFECT_
void SetMi2020SensorEffect(U8 byEffect)
{
	U16 wTmp;
	switch(byEffect)
	{
	case SNR_EFFECT_NEGATIVE:
		wTmp=0x6443;
		break;
	case SNR_EFFECT_MONOCHROME:
		wTmp=0x6441;
		break;
	case SNR_EFFECT_SEPIA:
		wTmp=0x6442;
		break;
	default:
		wTmp=0x6440;
		break;
	}
	Write_MI_Vairable(MI_DRV_ID_MODE|0X99|MIVAR_DATA_WIDTH_16_LOG, wTmp);//crop_Y1_B;
	Write_MI_Vairable(MI_DRV_ID_MODE|0X9B|MIVAR_DATA_WIDTH_16_LOG, wTmp);//crop_Y1_B;
	//Write_MI_Vairable(MI_DRV_ID_SEQ|0X03|MI2020_DATA_WIDTH_8_LOG, 5);
	Mi2020Refresh();
}
#endif

void SetMi2020ImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.
{
	Write_MI_Vairable(MI_DRV_ID_MODE|0X19|MIVAR_DATA_WIDTH_16_LOG, 0x046C|(U16)bySnrImgDir);//mode_sensor_mode_A
	Write_MI_Vairable(MI_DRV_ID_MODE|0X3B|MIVAR_DATA_WIDTH_16_LOG, 0x0024|(U16)bySnrImgDir);//mode_sensor_mode_B

	Mi2020Refresh();
}

void SetMi2020PwrLineFreq(U8 byFps, U8 bySetValue)
{
	U8 byTmp;

	byFps = byFps; // for delete warning
	if(bySetValue == PWR_LINE_FRQ_DIS)
	{
		Write_MI_Vairable(MI_DRV_ID_SEQ|0x23|MIVAR_DATA_WIDTH_8_LOG,1); //PreviewParEnter.fd
		Write_MI_Vairable(MI_DRV_ID_SEQ|0x2a|MIVAR_DATA_WIDTH_8_LOG,1); //PreviewPar.fd
		Write_MI_Vairable(MI_DRV_ID_SEQ|0x31|MIVAR_DATA_WIDTH_8_LOG,1); //PreviewParLeave.fd
		Write_MI_Vairable(MI_DRV_ID_SEQ|0x38|MIVAR_DATA_WIDTH_8_LOG,1); //CapParEnter.fd
		Write_MI_Vairable(MI_DRV_ID_ANTF|0x04|MIVAR_DATA_WIDTH_8_LOG,0x00); //CapParEnter.fd
		DBG(("PWR_LINE_FRQ_DIS\n"));
	}
	else
	{
		Write_MI_Vairable(MI_DRV_ID_SEQ|0x23|MIVAR_DATA_WIDTH_8_LOG,2); //PreviewParEnter.fd
		Write_MI_Vairable(MI_DRV_ID_SEQ|0x2a|MIVAR_DATA_WIDTH_8_LOG,2); //PreviewPar.fd
		Write_MI_Vairable(MI_DRV_ID_SEQ|0x31|MIVAR_DATA_WIDTH_8_LOG,2); //PreviewParLeave.fd
		Write_MI_Vairable(MI_DRV_ID_SEQ|0x38|MIVAR_DATA_WIDTH_8_LOG,2); //CapParEnter.fd
		if(bySetValue == PWR_LINE_FRQ_50)//50Hz
		{
			//	DBG(("PWR_LINE_FRQ_50\n"));
			byTmp = 0xc0;
		}
		else//60Hz
		{
			//	DBG(("PWR_LINE_FRQ_60\n"));
			byTmp = 0x80;
		}
		Write_MI_Vairable(MI_DRV_ID_ANTF|0x4|MIVAR_DATA_WIDTH_8_LOG,byTmp); //CapParEnter.fd
	}

	Write_MI_Vairable(MI_DRV_ID_SEQ|0x3|MIVAR_DATA_WIDTH_8_LOG,0x06);
}

void SetMi2020LSC()
{
	//cheney.cai 2009-06-24,do not select setting ,just replace setting when EEPROM configuration exist
	//if((g_byVdSOCIspSettingExistBitmap&VD_SNRISP_REGSETTING_LSC) == 0)
	{
		//DBG(("load vd lsc setting in fw\n"));
		//LoadVdSensorSetting((U8*)gc_MI2020_SOC_LSC_Setting,sizeof(gc_MI2020_SOC_LSC_Setting)/5);
		WriteSensorSettingWW(sizeof(gc_MI2020_SOC_LSC_Setting)/4, gc_MI2020_SOC_LSC_Setting);
		Write_SenReg_Mask(0x3210,0x0004,0x0004);//enble lens shading.
	}
}
void SetMi2020CCM()
{
	//default CCM

	//adjusted saturation
	Write_MI_Vairable(MI_DRV_ID_SEQ|0X15|MIVAR_DATA_WIDTH_8_LOG, 0xED);
	Write_MI_Vairable(MI_DRV_ID_AWB|0X52|MIVAR_DATA_WIDTH_16_LOG, 0x5001);

}
void SetMi2020AE()
{
	U8 byTmp;
	byTmp=(g_byOVAEB_Normal+g_byOVAEW_Normal)/2;
	Write_MI_Vairable(MI_DRV_ID_AE|0x6|MIVAR_DATA_WIDTH_8_LOG, byTmp);//ae.Target
	byTmp=(g_byOVAEW_Normal-g_byOVAEB_Normal)/2;
	Write_MI_Vairable(MI_DRV_ID_AE|0x7|MIVAR_DATA_WIDTH_8_LOG, byTmp);//ae.Target
}
void SetMi2020AWB()
{
	Write_MI_Vairable(MI_DRV_ID_SEQ|0xb|MIVAR_DATA_WIDTH_8_LOG,0x01); //Awb cont buff,slowest AWB speed
	Write_MI_Vairable(MI_DRV_ID_AWB|0x30|MIVAR_DATA_WIDTH_16_LOG,0xffcc);
	//Write_MI_Vairable(MI_DRV_ID_SEQ|0xc|MI1330_DATA_WIDTH_8_LOG,0x02); //Awb cont step,smooth AWB transition
	Write_MI_Vairable(MI_DRV_ID_AWB|0x64|MIVAR_DATA_WIDTH_8_LOG,0x40); //Awb KR_L, A light R gain
	Write_MI_Vairable(MI_DRV_ID_AWB|0x65|MIVAR_DATA_WIDTH_8_LOG,0x96); //Awb KG_L, A light G gain
	Write_MI_Vairable(MI_DRV_ID_AWB|0x66|MIVAR_DATA_WIDTH_8_LOG,0x90); //Awb KB_L, A light B gain
	//Write_MI_Vairable(MI_DRV_ID_AWB|0x67|MI1330_DATA_WIDTH_8_LOG,0x7a); //Awb KR_R, Day light R gain
	//Write_MI_Vairable(MI_DRV_ID_AWB|0x68|MI1330_DATA_WIDTH_8_LOG,0x88); //Awb KG_R, Day light G gain
	//Write_MI_Vairable(MI_DRV_ID_AWB|0x69|MI1330_DATA_WIDTH_8_LOG,0x68); //Awb KB_R, Day light B gain

	Write_MI_Vairable(MI_DRV_ID_AWB|0x67|MIVAR_DATA_WIDTH_8_LOG,0x78); //Awb KR_R, Day light R gain
	Write_MI_Vairable(MI_DRV_ID_AWB|0x68|MIVAR_DATA_WIDTH_8_LOG,0x80); //Awb KG_R, Day light G gain
	Write_MI_Vairable(MI_DRV_ID_AWB|0x69|MIVAR_DATA_WIDTH_8_LOG,0x70); //Awb KB_R, Day light B gain
}
void SetMi2020WBTempAuto(U8 bySetValue)
{
	if(bySetValue)
	{
		Write_MI_Vairable_Mask(MI_DRV_ID_SEQ|0x2|MIVAR_DATA_WIDTH_8_LOG, 0x04, 0x04);

		//	DBG(("Mi2020 AWB ON\n"));
	}
	else
	{
		Write_MI_Vairable_Mask(MI_DRV_ID_SEQ|0x2|MIVAR_DATA_WIDTH_8_LOG, 0x0, 0x04);
		//	DBG(("Mi2020 AWB OFF\n"));
	}
}



void SetMi2020WBTemp(U16 wSetValue)
{
	Write_MI_Vairable(MI_DRV_ID_AWB|0x51|MIVAR_DATA_WIDTH_8_LOG,MapWBTemp2MicronSensorWBPositon(wSetValue) );
}




void SetMi2020DPC()
{

}



void SetMi2020NRC()
{
	Write_MI_Vairable(MI_DRV_ID_SEQ|0X15|MIVAR_DATA_WIDTH_8_LOG, 0xED);//Seq_LLMODE
	Write_MI_Vairable(MI_DRV_ID_SEQ|0X16|MIVAR_DATA_WIDTH_8_LOG, 48);//
	Write_MI_Vairable(MI_DRV_ID_SEQ|0X17|MIVAR_DATA_WIDTH_8_LOG, 85);//

	Write_MI_Vairable(MI_DRV_ID_SEQ|0X3e|MIVAR_DATA_WIDTH_8_LOG, 4);//
	Write_MI_Vairable(MI_DRV_ID_SEQ|0X3f|MIVAR_DATA_WIDTH_8_LOG, 14);//
	Write_MI_Vairable(MI_DRV_ID_SEQ|0X40|MIVAR_DATA_WIDTH_8_LOG, 4);//
	Write_MI_Vairable(MI_DRV_ID_SEQ|0X41|MIVAR_DATA_WIDTH_8_LOG, 4);//
	Write_MI_Vairable(MI_DRV_ID_SEQ|0X42|MIVAR_DATA_WIDTH_8_LOG, 50);//
	Write_MI_Vairable(MI_DRV_ID_SEQ|0X43|MIVAR_DATA_WIDTH_8_LOG, 15);//
	Write_MI_Vairable(MI_DRV_ID_SEQ|0X44|MIVAR_DATA_WIDTH_8_LOG, 50);//
	Write_MI_Vairable(MI_DRV_ID_SEQ|0X45|MIVAR_DATA_WIDTH_8_LOG, 50);//
	Write_MI_Vairable(MI_DRV_ID_SEQ|0X46|MIVAR_DATA_WIDTH_8_LOG, 5);//
	Write_MI_Vairable(MI_DRV_ID_SEQ|0X47|MIVAR_DATA_WIDTH_8_LOG, 58);//
}

void SetMi2020MISC()
{
	Write_MI_Vairable(MI_DRV_ID_SEQ|0X15|MIVAR_DATA_WIDTH_8_LOG, 0xED);//Seq_LLMODE
	Write_MI_Vairable(MI_DRV_ID_SEQ|0X16|MIVAR_DATA_WIDTH_8_LOG, 48);//
	Write_MI_Vairable(MI_DRV_ID_SEQ|0X17|MIVAR_DATA_WIDTH_8_LOG, 85);//
	Write_MI_Vairable(MI_DRV_ID_SEQ|0X18|MIVAR_DATA_WIDTH_8_LOG, 30);//
	Write_MI_Vairable(MI_DRV_ID_SEQ|0X19|MIVAR_DATA_WIDTH_8_LOG, 4);//
	Write_MI_Vairable(MI_DRV_ID_SEQ|0X1a|MIVAR_DATA_WIDTH_8_LOG, 10);//
	Write_MI_Vairable(MI_DRV_ID_SEQ|0X1b|MIVAR_DATA_WIDTH_8_LOG, 32);//
	Write_MI_Vairable(MI_DRV_ID_SEQ|0X1c|MIVAR_DATA_WIDTH_8_LOG, 2);//
	Write_MI_Vairable(MI_DRV_ID_SEQ|0X1d|MIVAR_DATA_WIDTH_8_LOG, 0);//
	Write_MI_Vairable(MI_DRV_ID_SEQ|0X1e|MIVAR_DATA_WIDTH_8_LOG, 0);//
	Write_MI_Vairable(MI_DRV_ID_SEQ|0X1f|MIVAR_DATA_WIDTH_8_LOG, 4);//
}

void 	SetMi2020BackLightComp(U8 bySetValue)
{
	U16 wTmp;
	U8 byTmp;
	//AE target.
	DBG(("bySetValue=%bx\n",bySetValue));
	if(bySetValue==1)
	{
		byTmp=(g_byOVAEB_BLC+g_byOVAEW_BLC)/2;
	}
	else
	{
		byTmp=(g_byOVAEB_Normal+g_byOVAEW_Normal)/2;
	}
	//DBG(("byTmp=%bx\n",byTmp));
	Write_MI_Vairable(MI_DRV_ID_AE|0x6|MIVAR_DATA_WIDTH_8_LOG, byTmp);//ae.Target
	//weight
	wTmp = 0x000f + ((0xf-((bySetValue)<<2))<<4);
	Write_MI_Vairable(MI_DRV_ID_AE|0x17|MIVAR_DATA_WIDTH_8_LOG,wTmp);
}



void 	CfgMi2020ControlAttr()
{
	g_bySensorSize = SENSOR_SIZE_UXGA;
	g_bySensorSPFormat = UXGA_FRM|SVGA_FRM;

	{
//		memcpy(g_asOvCTT,gc_MI2020_CTT,sizeof(gc_MI2020_CTT));
	}

	{
		// AE backlight compensation parameter
		g_byOVAEW_Normal = MI2020_AEW;
		g_byOVAEB_Normal = MI2020_AEB;

		g_byOVAEW_BLC = g_byOVAEW_Normal + 0x20;
		g_byOVAEB_BLC = g_byOVAEB_Normal + 0x20;
	}

	{
		//g_bySV28VoltSel =  SV28_VOL_2V9;
		//g_bySV18VoltSel=  SV18_VOL_1V9;
		// hemonel 2010-09-08: support 1.8V sensor IO
#if (_CHIP_ID_ & _RTS5840_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V79;
#elif (_CHIP_ID_ & _RTS5832_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V85;
#elif (_CHIP_ID_ & _RTS5829B_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V82;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V80;
#endif
	}

//	if(g_byFormatCfgExist==0) // format config not exist, use default setting.
	{
		InitFormatFrameFps();
	}


}



#ifndef _USE_BK_HSBC_ADJ_
void SetMI2020Brightness(S16 swSetValue)
{
	S16 swTmp;
	//find  setting  first.

	swTmp=swSetValue;

	if(swTmp<0)
	{
		swTmp=0;
	}
	Write_MI_Vairable(MI_DRV_ID_MODE|0x9D|MIVAR_DATA_WIDTH_8_LOG,swTmp );
	Write_MI_Vairable(MI_DRV_ID_MODE|0x9E|MIVAR_DATA_WIDTH_8_LOG,swTmp);
	//Write_MI_Vairable(MI_DRV_ID_SEQ|0X03|MI2020_DATA_WIDTH_8_LOG, 5);
	Mi2020Refresh();
}

void SetMI2020Contrast(U16 wSetValue)
{
	U16 wTmp;
	U16 wTmp2;
	//find  setting  first.
	wTmp=wSetValue;

	DBG(("@5@%x\n",wTmp));
	if(wTmp == 0)
	{
		wTmp=4;
	} // no contrast,use noise-reduction contrast.
	//DBG(("@4@%x\n",wSetValue));
	//DBG(("@3@%x\n",wTmp));

	if(g_bySensorCurFormat == SVGA_FRM)
	{
		Read_MI_Vairable(MI_DRV_ID_MODE|0x6d|MIVAR_DATA_WIDTH_8_LOG,&wTmp2);
		wTmp2 &= 0x000f;
		wTmp2 |=(wTmp<<4);
		//DBG(("@1@%x\n",wTmp2));
		Write_MI_Vairable(MI_DRV_ID_MODE|0x6d|MIVAR_DATA_WIDTH_8_LOG,wTmp2);
	}
	else
	{
		Read_MI_Vairable(MI_DRV_ID_MODE|0x6e|MIVAR_DATA_WIDTH_8_LOG,&wTmp2);
		wTmp2 &= 0x000f;
		wTmp2 |=(wTmp<<4);
		//DBG(("@2@%x\n",wTmp2));
		Write_MI_Vairable(MI_DRV_ID_MODE|0x6e|MIVAR_DATA_WIDTH_8_LOG,wTmp2);
	}
	Mi2020Refresh();

}
#endif

void SetMi2020ISPMisc()
{
	//cheney.cai 2009-06-24,do not select setting ,just replace setting when EEPROM configuration exist
	//if(0==(VD_SNRISP_REGSETTING_LSC&g_byVdSOCIspSettingExistBitmap))
	{
		SetMi2020LSC();
	}
	//cheney.cai 2009-06-24,do not select setting ,just replace setting when EEPROM configuration exist
	//if(0==(VD_SNRISP_REGSETTING_CCM&g_byVdSOCIspSettingExistBitmap))
	{
		SetMi2020CCM();
	}
	//cheney.cai 2009-06-24,do not select setting ,just replace setting when EEPROM configuration exist
	//if(0==(VD_SNRISP_REGSETTING_AE&g_byVdSOCIspSettingExistBitmap))
	{
		SetMi2020AE();
	}
	//cheney.cai 2009-06-24,do not select setting ,just replace setting when EEPROM configuration exist
	//if(0==(VD_SNRISP_REGSETTING_AWB&g_byVdSOCIspSettingExistBitmap))
	{
		SetMi2020AWB();
	}

	//cheney.cai 2009-06-24,do not select setting ,just replace setting when EEPROM configuration exist
	//if(0==(VD_SNRISP_REGSETTING_GAMMA&g_byVdSOCIspSettingExistBitmap))
	{
		//SetMi2020GAMMA();
	}

	SetMi2020DPC();
	//cheney.cai 2009-06-24,do not select setting ,just replace setting when EEPROM configuration exist
	//if(0==(VD_SNRISP_REGSETTING_NRC&g_byVdSOCIspSettingExistBitmap))
	{
		SetMi2020NRC();
	}
	//cheney.cai 2009-06-24,do not select setting ,just replace setting when EEPROM configuration exist
	//if(0==(VD_SNRISP_REGSETTING_MISC&g_byVdSOCIspSettingExistBitmap))
	{
		SetMi2020MISC();
	}

}
U16 GetMi2020AEGain()
{
	U16 wRegValue;
	U16 wTotalGain;
	//get register value 0x305e
	/*
	bit[10:8] 0 Digital Gain. Legal values 1-7.
	bit[7]   0    Analog gain = (bit [7] + 1) * initial gain.
	bit[6:0] 0x0010 Initial Gain
	Initial gain = bits [6:0] * 1/16.
	*/
	Read_SenReg(0x305e, &wRegValue);
	//Read_MI_Vairable(MI_DRV_ID_AE|0x1D|MIVAR_DATA_WIDTH_8_LOG, &wRegValue);
	//bit[6:0]
	wTotalGain =  (wRegValue&0x7f);
	//bit[7]
	if(wRegValue&0x80)
	{
		wTotalGain <<=1;
	}
	//bit[10:8]
	wTotalGain  *=  ((wRegValue&0x0700)>>8);

	return wTotalGain;
}

void SetMi2020IntegrationTimeAuto(U8 bySetValue)
{
}

void SetMi2020IntegrationTime(U16 wEspline)
{
}
#endif
