#ifndef _SIV120D_H_
#define _SIV120D_H_

#define SIV120D_AE_TARGET	0x78

void SIV120DSetFormatFps(U8 SetFormat, U8 Fps);
void CfgSIV120DControlAttr(void);
void	SetSIV120DSharpness(U8 bySetValue);
void SetSIV120DImgDir(U8 bySnrImgDir);
void SetSIV120DOutputDim(U16 wWidth,U16 wHeight);
void SetSIV120DPwrLineFreq(U8 byFPS, U8 byLightFrq);
void SetSIV120DWBTemp(U16 wRgain, U16 wGgain, U16 wBgain);
void SetSIV120DWBTempAuto(U8 bySetValue);
void SetSIV120DBackLightComp(U8 bySetValue);
extern code OV_CTT_t gc_SIV120D_CTT[3];
U16 GetSIV120DAEGain(void);
void SetSIV120DIntegrationTimeAuto(U8 bySetValue);
void SetSIV120DIntegrationTime(U16 wEspline);
void SetSIV120DGain(float fGain);
#endif


