#ifndef _YACC611_H_
#define _YACC611_H_

void YACC611SetFormatFps(U16 SetFormat, U8 Fps);
void CfgYACC611ControlAttr(void);
void SetYACC611ImgDir(U8 bySnrImgDir);
void SetYACC611IntegrationTime(U16 wEspline);
void SetYACC611Exposuretime_Gain(float fExpTime, float fTotalGain);
void InitYACC611IspParams();
void YACC611_POR();
void SetYACC611DynamicISP(U8 byAEC_Gain);
void SetYACC611DynamicISP_AWB(U16 wColorTempature);
void SetYACC611Gain(fGain);
#endif // _OV9710_H_

