#ifndef _OV2680_H_
#define _OV2680_H_

void OV2680SetFormatFps(U16 SetFormat, U8 Fps);
void CfgOV2680ControlAttr(void);
void SetOV2680ImgDir(U8 bySnrImgDir);
void SetOV2680IntegrationTime(U16 wEspline);
void SetOV2680Exposuretime_Gain(float fExpTime, float fTotalGain);
void InitOV2680IspParams();
void OV2680_POR();
void InitOV2680IspParams();
void SetOV2680DynamicISP(U8 bymode);
void SetOV2680DynamicISP_AWB(U16 wColorTempature);
void SetOV2680Gain(float fGain);
#endif // _OV9710_H_


