#include "Inc.h"

#ifdef RTS58XX_SP_GC1036

#ifdef _ENABLE_OUTDOOR_DETECT_
Dyanmic_AWB_Boundary_t code gc_AWBDynmaicBoundary={28,-8,16,8,50,30,2,21};
#endif

OV_CTT_t code gc_GC1036_CTT[3] =
{
	{2900,0x112,0x100,0x250},
	{4600,0x190,0x100,0x190},
	{6600,0x1D8,0x100,0x124},
};

code GC_FpsSetting_t  g_staGC1036FpsSetting[]=
{
// FPS ExtraDummyPixel byExtraDummyLine clkrc 	   pclk
//	{5,		(1682+1683+1),	         (3),		14135400},		//GC1036 clk too low
	{5,		(2041),	             (53),	    (0x83),	      24000000},
	{10,		(857),	             (53),          (0x83), 		24000000},
	{15,		(1529),		       (90),          (0x88), 		60000000},
	{20,		(1023),	             (90),          (0x88), 		60000000},
	{25,		(671),	             (90),          (0x88),		60000000},
	{30,		(338),			 (90),         (0x88),		60000000}, 	// adjust fps >=30fps for msoc jitter
};


t_RegSettingBB code gc_GC1036_Setting[] =
{
/////////////////////////////////////////////////////
//////////////////////   SYS   //////////////////////
/////////////////////////////////////////////////////
{0xfe,0x80},
{0xfe,0x80},
{0xfe,0x80},
{0xf2,0x00}, //sync_pad_io_ebi
{0xf6,0x00}, //up down
{0xfc,0x06},
{0xf7,0x15}, //pll enable
{0xf8,0x88}, //Pll mode 2
{0xf9,0xfe}, //[0] pll enable
{0xfa,0x10}, //div
{0xfe,0x00},

/////////////////////////////////////////////////////
////////////////   ANALOG & CISCTL   ////////////////
/////////////////////////////////////////////////////
{0x03,0x03},
{0x04,0x2a}, //30fps
{0x05,0x01},
{0x06,0x52},
{0x07,0x00},
{0x08,0x5a},
{0x0a,0xf8}, //row start
{0x0c,0xa0}, //0c//col start
{0x0d,0x02},
{0x0e,0xe0},
{0x0f,0x05}, //Window setting
{0x10,0x10}, 
{0x17,0x15}, //01//14//[0]mirror [1]flip
{0x18,0x1e}, //sdark off
{0x19,0x06},
{0x1a,0x01},
{0x1b,0x48},
{0x1e,0x88},
{0x20,0x03},
{0x21,0x7f}, //2f//20//rsg
{0x22,0x80},
{0x23,0xc1},
{0x24,0x2f}, //16 //PAD drive
{0x26,0x01}, //analog gain
{0x27,0x30}, 
{0x3f,0x00}, //PRC mode

/////////////////////////////////////////////////////
//////////////////////   ISP   //////////////////////
/////////////////////////////////////////////////////
{0x8b,0xa0},
{0x8c,0x02}, //hsync polarity
{0x90,0x01},
{0x92,0x02}, //00/crop win y
{0x94,0x06}, //04/crop win x
{0x95,0x02}, //crop win height
{0x96,0xd0},
{0x97,0x05}, //crop win width
{0x98,0x00},

/////////////////////////////////////////////////////
//////////////////////   BLK   //////////////////////
/////////////////////////////////////////////////////
{0x40,0x25}, //2b //[3] BLK after Gain
{0x41,0x04}, //82
{0x5e,0x20}, //18/current offset ratio
{0x5f,0x20},
{0x60,0x20},
{0x61,0x20},
{0x62,0x20},
{0x63,0x20},
{0x64,0x20},
{0x65,0x20},
{0x66,0x00}, //dark current ratio
{0x67,0x00},
{0x68,0x00},
{0x69,0x00},

/////////////////////////////////////////////////////
//////////////////////   GAIN   /////////////////////
/////////////////////////////////////////////////////
{0xb2,0x00},
{0xb3,0x40},
{0xb4,0x40},
{0xb5,0x40},

/////////////////////////////////////////////////////
////////////////////   DARK SUN   ///////////////////
/////////////////////////////////////////////////////
{0xb8,0x0f},
{0xb9,0x23},
{0xba,0xff},
{0xbc,0x80}, //dark sun_en
{0xbd,0x00},
{0xbe,0xff},
{0xbf,0x09},

{0xfe,0x03},
{0x01,0x00},
{0x02,0x00},
{0x03,0x00},
{0x06,0x00},
{0x10,0x00},
{0x15,0x00},
{0xfe,0x00},
{0xf2,0x0f},


};

#ifdef _MIPI_EXIST_
t_RegSettingBB code gc_GC1036_mipi_Setting[] =
{

/////////////////////////////////////////////////////
//////////////////////	 MIPI	/////////////////////
/////////////////////////////////////////////////////
{0xfe,0x03},
{0x01,0x03},
{0x02,0x11},
{0x03,0x21},
{0x06,0x80},
{0x11,0x2b},
{0x12,0x40},
{0x13,0x06},
{0x15,0x12},
{0x04,0x20},
{0x05,0x00},
{0x17,0x01},
{0x10,0x90},
{0xfe,0x00},

};

#endif


static GC_FpsSetting_t*  GetOvFpsSetting(U8 Fps, GC_FpsSetting_t staOvFpsSetting[], U8 byArrayLen)
{
	U8 i;
	U8 Idx;

	ISP_MSG((" array size = %bd\n", byArrayLen));

	if(g_bIsHighSpeed)
	{
		Idx=2; //8fps for uxga, 10 fps for vga
	}
	else
	{
		Idx = 0;
	}

	for(i=0; i< byArrayLen; i++)
	{
		ISP_MSG((" fps i = %bd\n",i));
		if(staOvFpsSetting[i].byFps ==Fps)
		{
			Idx=i;
			break;
		}
	}
	ISP_MSG((" get fps idx = %bd\n",Idx));
	return &staOvFpsSetting[Idx];
}

static U16 MapSnrGlbGain2SnrRegSetting(U16 wGain)
{
	U16 wSnrRegGain = 0;
	U16 bySnrRegGain_Low=0;
	U16 bySnrRegGain_High=0;
	//QDBG(("byGain = %bd-> ",byGain));

		if(wGain < 64 )
		{
			wGain <<= 2;
			bySnrRegGain_Low=wGain;
			bySnrRegGain_High=0x40;
		}	
		else 
		{    
		      bySnrRegGain_Low=0xff;
			bySnrRegGain_High=wGain;
		}	

		bySnrRegGain_High<<=8;
		
		wSnrRegGain=bySnrRegGain_High|bySnrRegGain_Low;

	
	//QDBG(("sensorGain = %d    ",sensorGain));
	return wSnrRegGain;
}

float SnrRegSetting2SnrGlbGain(U16 wSnrRegGain)
{
	U16 wGain;
	float fGainOffset;
	U8 i;

	if(wSnrRegGain >= 0x70)
	{
		fGainOffset = 1.1; //0.99933; //0.5;
	}
	else if(wSnrRegGain >= 0x30)
	{
		fGainOffset = 0.360577; //0.32349; //0.2;
	}
	else if(wSnrRegGain >= 0x10)
	{
		fGainOffset = 0.107287; //0.11564; //0.0625;
	}
	else
	{
		fGainOffset = 0;
	}

	wGain = (wSnrRegGain&0x0f) + 16;
	wSnrRegGain >>= 4;

	for(i = 0; i < 4; i++)
	{
		if((wSnrRegGain&1) == 1)
		{
			wGain <<= 1;
		}

		wSnrRegGain >>= 1;
	}

	//return ((float)wGain)/16.0-fGainOffset;
	//return (((float)wGain)/16.0-fGainOffset-1.0)*1.27 + 1.0;
	return ((((float)wGain)/16.0-fGainOffset)-1.0)*1.2 + 1.0;
}

//this function is also called during Probe stage to calculate the transfer_size/LWM/HWM
void GetSensorPclkHsync(U16 wSensorSPFormat,U8 byFps)
{
	GC_FpsSetting_t *pGC1036_FpsSetting;

	wSensorSPFormat =wSensorSPFormat;
	
	pGC1036_FpsSetting=GetOvFpsSetting(byFps, g_staGC1036FpsSetting, sizeof(g_staGC1036FpsSetting)/sizeof(GC_FpsSetting_t));
	g_wSensorHsyncWidth = pGC1036_FpsSetting->wExtraDummyPixel; // this for manual exposure
	g_dwPclk = pGC1036_FpsSetting->dwPixelClk;		// this for scale speed
}

void GC1036SetFormatFps(U16 SetFormat, U8 Fps)
{
	U16 wTemp;
	GC_FpsSetting_t *pGC1036_FpsSetting;

	// hemonel 2010-04-13: for delete complier warning
	SetFormat = SetFormat;

	pGC1036_FpsSetting=GetOvFpsSetting(Fps, g_staGC1036FpsSetting, sizeof(g_staGC1036FpsSetting)/sizeof(GC_FpsSetting_t));
	// 1) change hclk if necessary

	// initial all register setting
	WriteSensorSettingBB(sizeof(gc_GC1036_Setting)/2, gc_GC1036_Setting);
#ifdef _MIPI_EXIST_
	WriteSensorSettingBB(sizeof(gc_GC1036_mipi_Setting)/2, gc_GC1036_mipi_Setting);
#endif

	// 2) write sensor register
	//Write_SenReg(0x0305, pGC1036_FpsSetting->byClkrc);
	
	wTemp = pGC1036_FpsSetting->wExtraDummyPixel;
	Write_SenReg(0x06, INT2CHAR(wTemp, 0)); //Write dummy pixel LSB
	Write_SenReg(0x05, INT2CHAR(wTemp, 1)); //Write dummy pixel MSB
	Write_SenReg(0x08, pGC1036_FpsSetting->byExtraDummyLine); //Write Dummy line
	Write_SenReg(0xf8, pGC1036_FpsSetting->byClkrc);//Write PLL
	
	g_wSensorHsyncWidth= 1296+248+pGC1036_FpsSetting->wExtraDummyPixel;
	g_wAECExposureRowMax = 1095;	// this for max exposure time
	g_wAEC_LineNumber = 1100;	// this for AE insert dummy line algothrim
      g_dwPclk = pGC1036_FpsSetting->dwPixelClk;	
	g_wSensorWidthBefBLC = 1280;
	g_wSensorHeightBefBLC = 720;

	InitGC1036IspParamsByResolution(g_wCurFrameWidth, g_wCurFrameHeight);

#ifdef _MIPI_EXIST_
	//-----------------------------Dphy Setting--------------------------------
	SetMipiDphy(MIPI_DATA_LANE0_EN, \
	            MIPI_DATA_FORMAT_RAW10 , MIPI_DATA_TYPE_RAW10, \
	            HSTERM_EN_TIME_33);
#endif
}

void CfgGC1036ControlAttr(void)
{
	U8 i;

	g_bySensorSize = SENSOR_SIZE_HD720P;
	g_wSensorSPFormat =HD720P_FRM;
#ifdef _ENABLE_MJPEG_
	g_byQtableScale = 6;	//HD720P HD800P Setting=6 for 7 comp rate
#endif
	{
		memcpy(g_asOvCTT, gc_GC1036_CTT,sizeof(gc_GC1036_CTT));
	}

	{
		// hemonel 2010-09-08: support 1.8V sensor IO
#if (_CHIP_ID_ & _RTS5840_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V79;
#elif (_CHIP_ID_ & _RTS5832_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V85;
#elif (_CHIP_ID_ & _RTS5829B_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V82;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V80;
#endif
	}

	{
		//====== resolution  setting ===========
		// -- preview ---
		g_aVideoFormat[0].byaVideoFrameTbl[0] = 7;		// resolution number
		g_aVideoFormat[0].byaVideoFrameTbl[1]=F_SEL_640_480;
		g_aVideoFormat[0].byaVideoFrameTbl[2]=F_SEL_160_120;
		g_aVideoFormat[0].byaVideoFrameTbl[3]=F_SEL_176_144;
		g_aVideoFormat[0].byaVideoFrameTbl[4]=F_SEL_320_240;
		g_aVideoFormat[0].byaVideoFrameTbl[5]=F_SEL_352_288;
		g_aVideoFormat[0].byaVideoFrameTbl[6]=F_SEL_320_180;
		g_aVideoFormat[0].byaVideoFrameTbl[7]=F_SEL_1280_720;
		//g_aVideoFormat[0].byaVideoFrameTbl[8]=F_SEL_1280_800;
		//g_aVideoFormat[0].byaVideoFrameTbl[8]=F_SEL_960_540;
		//g_aVideoFormat[0].byaVideoFrameTbl[9]=F_SEL_848_480;
		//g_aVideoFormat[0].byaVideoFrameTbl[10]=F_SEL_640_360;
		//g_aVideoFormat[0].byaVideoFrameTbl[11]=F_SEL_424_240;
		//--still image--
		g_aVideoFormat[0].byaStillFrameTbl[0] = 6;	// resolution number
		//g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_1280_800;
		g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_1280_720;
		g_aVideoFormat[0].byaStillFrameTbl[2]=F_SEL_160_120;
		g_aVideoFormat[0].byaStillFrameTbl[3]=F_SEL_176_144;
		g_aVideoFormat[0].byaStillFrameTbl[4]=F_SEL_320_240;
		g_aVideoFormat[0].byaStillFrameTbl[5]=F_SEL_352_288;
		g_aVideoFormat[0].byaStillFrameTbl[6]=F_SEL_640_480;
		//g_aVideoFormat[0].byaStillFrameTbl[7]=F_SEL_960_540;
		//g_aVideoFormat[0].byaStillFrameTbl[8]=F_SEL_848_480;
		//g_aVideoFormat[0].byaStillFrameTbl[9]=F_SEL_640_360;
		//g_aVideoFormat[0].byaStillFrameTbl[10]=F_SEL_424_240;
		//g_aVideoFormat[0].byaStillFrameTbl[11]=F_SEL_320_180;
		//====== fps  setting ===========
		{
			for(i=0; i<10; i++)
			{
				/*
					F_SEL_640_480    (0)
					F_SEL_160_120    (1)
					F_SEL_176_144    (2)
					F_SEL_320_180	(3)
					F_SEL_320_200    (4)
					F_SEL_320_240    (5)
					F_SEL_352_288    (6)
					F_SEL_424_240	(7)
					F_SEL_640_360	(8)
					F_SEL_640_400    (9)
				*/
				g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_15|FPS_20|FPS_25|FPS_30;
			}
			for(i=10; i<18; i++)
			{
				/*
					F_SEL_800_600    (10)
					F_SEL_848_480	(11)
					F_SEL_960_540	 (12)
					F_SEL_1024_768   (13)
					F_SEL_1280_720   (14)
					F_SEL_1280_800   (15)
					F_SEL_1280_960   (16)
					F_SEL_1280_1024  (17)
				*/
				g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_10;
			}
		}
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_800_600]= FPS_25|FPS_20|FPS_15|FPS_10|FPS_5;
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_848_480]= FPS_25|FPS_20|FPS_15|FPS_10|FPS_5;
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_960_540]= FPS_20|FPS_15|FPS_10|FPS_5;

		//====== format type  setting ===========
		g_aVideoFormat[0].byFormatType = FORMAT_TYPE_YUY2;

#ifdef _ENABLE_MJPEG_
		// copy YUY2 setting to MJPEG setting
		memcpy(&(g_aVideoFormat[1]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

		// modify MJPEG FPS setting
		for(i=10; i<18; i++)
		{
			g_aVideoFormat[1].waFrameFpsBitmap[i] = (FPS_15|FPS_20|FPS_25|FPS_30);
		}

		// modify MJPEG format type
		g_aVideoFormat[1].byFormatType = FORMAT_TYPE_MJPG;
#endif

#ifdef _ENABLE_M420_FMT_
		// copy YUY2 setting to M420 setting
		memcpy(&(g_aVideoFormat[2]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

		// modify M420 FPS setting
		{
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_800_600] =FPS_25|FPS_20|FPS_15|FPS_10|FPS_5;//|FPS_1;             //bit07 F_800_600
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_960_540] =FPS_25|FPS_20|FPS_15|FPS_10|FPS_5;//|FPS_5|FPS_3|FPS_1;                    //bit08 F_960_720
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1024_768] =FPS_15|FPS_10|FPS_5;//|FPS_1;                    //bit09 F_1024_768
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_720] =FPS_10|FPS_5;//FPS_9|FPS_8|FPS_1;    //bit10 F_1280_720
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_800] =FPS_10|FPS_5;

			// modify M420 format type
			g_aVideoFormat[2].byFormatType = FORMAT_TYPE_M420;
		}
#endif
	}

	{
		//Brightness - BrightnessItem

		//Contrast - ContrastItem

		//Saturation - SaturationItem

		//Hue - 	HueItem

		//Sharpness - SharpnessItem
		//	SharpnessItem.Min = 0;
		//	SharpnessItem.Max = 8;
		//	SharpnessItem.Res = 1;
		//	SharpnessItem.Def = 3;
		//	SharpnessItem.Last  =SharpnessItem.Def;

		// gamma - 	GammaItem

		// gain - GainItem

		// whitebalance - WhiteBalanceTempItem

		// backlight - BackLightCompItem

		// powerline -PwrLineFreqItem
		//	PwrLineFreqItem.Def = PWR_LINE_FRQ_60;
		//	PwrLineFreqItem.Last = PwrLineFreqItem.Def;

		// exposure time -	ExposureTimeAbsolutItem

		// low light - LowLightCompItem
	}

	InitGC1036IspParams();
}

void SetGC1036IntegrationTime(U16 wEspline)
{
	U16 wFrameLenLines;
	U16 wExpMax = g_wAECExposureRowMax-2;

	if(wEspline > wExpMax)
	{
		wFrameLenLines = wEspline + 20;
	}
	else
	{
		wFrameLenLines = g_wAEC_LineNumber;
	}

	if (wFrameLenLines%2==1)
	{
		wFrameLenLines++;
	}
/*
	//-----------Write  frame length or dummy lines------------
	Write_SenReg(0x0341, INT2CHAR(wFrameLenLines, 0));
	Write_SenReg(0x0340, INT2CHAR(wFrameLenLines, 1));

	//-----------Write Exposuretime setting-----------
	Write_SenReg(0x0203, INT2CHAR(wEspline, 0));	// change exposure value low
	Write_SenReg(0x0202, INT2CHAR(wEspline, 1));	// change exposure value high

	//----------- write gain setting-------------------
	Write_SenReg(0x0205, 0x0f);	// fixed gain at manual exposure control
	Write_SenReg(0x0204, 0);
	*/
}

void SetGC1036ImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.
{
	WaitFrameSync(ISP_INT1,ISP_DATA_START_INT);

	bySnrImgDir ^= 0x02; 	// GC1036 output mirrored image at default, so need mirror the image for normal output
	Write_SenReg_Mask(0x17, bySnrImgDir, 0x03);
	if(bySnrImgDir==0x02)
	{
	
      Write_SenReg(0x92, 0x03);
	Write_SenReg(0x94, 0x05); 
	}
	else if(bySnrImgDir==3)
	{
      Write_SenReg(0x92, 0x03);
	Write_SenReg(0x94, 0x06); 
	}
	else if(bySnrImgDir==0)
	{
      Write_SenReg(0x92, 0x02);
	Write_SenReg(0x94, 0x05); 
	}	
	else
	{
       Write_SenReg(0x92, 0x02);
	 Write_SenReg(0x94, 0x06); 
	}
	WaitFrameSync(ISP_INT1,ISP_DATA_END_INT);
      
/*
	if(bySnrImgDir & 0x01)
	{
#ifdef _MIPI_EXIST_
		SetBLCWindowStart(1, 1);
#else
		SetBkWindowStart(1, 1);
#endif
	}
	else
	{
#ifdef _MIPI_EXIST_
		SetBLCWindowStart(0, 1);
#else
		SetBkWindowStart(0, 1);
#endif
	}
	*/
}

void SetGC1036Exposuretime_Gain(float fExpTime, float fTotalGain)
{
	U16 wExposureRows_floor;
	U16  wGainRegSetting; //data
	float fSnrGlbGain;
	float fExposureRows;
	U16 wSetDummy;
	float fTemp;
	U16 wExposurePixels;

	wGainRegSetting = MapSnrGlbGain2SnrRegSetting((U16)(fTotalGain*16.0));
	//wGainRegSetting = MapSnrGlbGain2SnrRegSetting((U16)(((fTotalGain-1.0)/1.27+1.0)*16.0));
	//wGainRegSetting = MapSnrGlbGain2SnrRegSetting((U16)(((fTotalGain-1.0)/1.2+1.0)*16.0));
	fSnrGlbGain = SnrRegSetting2SnrGlbGain(wGainRegSetting);
	
	fExposureRows = fExpTime/g_fSensorRowTimes;	//unit : 1row
	wExposureRows_floor = (U16)(fExposureRows);
	fTemp = fExpTime-((float)wExposureRows_floor*(g_fSensorRowTimes)); //fine integration time;unit:  us
	wExposurePixels =(U16)(fTemp * (float)(g_dwPclk/10000)) ; //unit: pixels

	// set dummy
	wSetDummy = g_wAFRInsertDummylines + g_wAEC_LineNumber;
	//-----------Write  frame length or dummy lines------------
	if (wSetDummy%2 == 1)
	{
		wSetDummy++;
	}
	
	// write exposure, gain, dummy line
	if(fabs(g_fCurExpTime-fExpTime) > 0.0001)
	{
		// group register hold
		//Write_SenReg(0x0104, 0x01);
  
		Write_SenReg(0x06, INT2CHAR(wSetDummy, 0));
		Write_SenReg(0x05, INT2CHAR(wSetDummy, 1));

		//-----------Write Exposuretime setting-----------
		Write_SenReg(0x04, INT2CHAR(wExposureRows_floor, 0));	// change exposure value low
		Write_SenReg(0x03, INT2CHAR(wExposureRows_floor, 1));	// change exposure value high


		//----------- write gain setting-------------------
		Write_SenReg(0xb1, INT2CHAR(wGainRegSetting, 0));
		Write_SenReg(0xb0, INT2CHAR(wGainRegSetting, 1));
             
		//SetISPAEGain(fTotalGain, fSnrGlbGain, 1);

		// group register release
		//Write_SenReg(0x0104, 0);
		g_fCurExpTime = fExpTime;
	}
	else
	{
		// group register hold
		//Write_SenReg(0x0104, 1);

		Write_SenReg(0x06, INT2CHAR(wSetDummy, 0));
		Write_SenReg(0x05, INT2CHAR(wSetDummy, 1));

		// write gain setting
		Write_SenReg(0xb1, INT2CHAR(wGainRegSetting, 0));
		Write_SenReg(0xb0, INT2CHAR(wGainRegSetting, 1));

		//SetISPAEGain(fTotalGain, fSnrGlbGain, 1);

		// group register release
		//Write_SenReg(0x0104, 0);
	}


	return;
}

void GC1036_POR(void )
{
#ifdef _MIPI_EXIST_
	Init_MIPI();
#else
	Init_CCS();
#endif
	ENTER_SENSOR_RESET();
	// power on
	SensorPowerControl(SWITCH_ON,EN_DELAYTIME);
	uDelay(1);
	// Initialize I2C Controller Timing
	LEAVE_SENSOR_RESET();
	// clock select 24M
	if( g_bySSCEnable== TRUE)
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_SSC_24M);
	}
	else
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_96M|CCS_CLK_DIVIDER_4);
	}

	EnableSensorHCLK();
	g_bySensorIsOpen = SENSOR_OPEN;
	WaitTimeOut_Delay(1);  //GC1036 spec request 8192clk
}

void InitGC1036IspParams(void )
{
	// AWB initial gain
	//use D50 R/G/B gain as AWB initial value
	g_wAWBRGain_Last= 0x17C;
	g_wAWBGGain_Last= 0x100;
	g_wAWBBGain_Last= 0x226;
	g_wProjectGR = g_wAWBRGain_Last;
	g_wProjectGB = g_wAWBBGain_Last;

	// Special ISP
	g_bySaturation_Def =64;
	g_byContrast_Def = 32; //Neil Tuning at chicony
	g_byTgamma_rate_max=63;
	g_byTgamma_rate_min =20;

	//g_wDynamicISPEn = 0;
	g_wDynamicISPEn = DYNAMIC_LSC_CT_EN| DYNAMIC_LSC_EN | DYNAMIC_SHARPNESS_EN | DYNAMIC_SHARPPARAM_EN | DYNAMIC_GAMMA_EN | DYNAMIC_CCM_CT_EN;
}

void InitGC1036IspParamsByResolution(U16 wCurWidth, U16 wCurHeight)
{
	wCurWidth = wCurWidth;
	wCurHeight = wCurHeight;
}

void SetGC1036DynamicISP(U8 byAEC_Gain)
{
	byAEC_Gain = byAEC_Gain;
}

void SetGC1036DynamicISP_AWB(U16 wColorTempature)
{
	wColorTempature =wColorTempature;

	// hemonel 2011-07-04: Albert use only one LSC curve to pass MSOC premium test, so delete these code
	/*
	#ifdef _MSOC_TEST_
	U8 i;

	// LSC Curve dynamic
	if ( (g_wDynamicISPEn&DYNAMIC_LSC_EN) == DYNAMIC_LSC_EN)
	{
		if(wColorTempature < 4000)
		{
	          		for (i=0;i<6;i++)
	          		{
	          			g_awLensShadingCenter[i] = gc_wLSC_Center_Offset_GC1036_3500K[i];
	          		}

				for (i=0;i<48;i++)
				{
					XBYTE[ISP_NLSC_CURVE_R_BASE+i]  = gc_byLSC_RGB_Curve_GC1036_3500K[0][i];
					XBYTE[ISP_NLSC_CURVE_G_BASE+i]  = gc_byLSC_RGB_Curve_GC1036_3500K[1][i];
					XBYTE[ISP_NLSC_CURVE_B_BASE+i]  = gc_byLSC_RGB_Curve_GC1036_3500K[2][i];
				}
		}
		else if(wColorTempature > 4500)
		{
	          		for (i=0;i<6;i++)
	          		{
	          			g_awLensShadingCenter[i] = gc_wLSC_Center_Offset_GC1036_D65[i];
	          		}

				for (i=0;i<48;i++)
				{
					XBYTE[ISP_NLSC_CURVE_R_BASE+i]  = gc_byLSC_RGB_Curve_GC1036_D65[0][i];
					XBYTE[ISP_NLSC_CURVE_G_BASE+i]  = gc_byLSC_RGB_Curve_GC1036_D65[1][i];
					XBYTE[ISP_NLSC_CURVE_B_BASE+i]  = gc_byLSC_RGB_Curve_GC1036_D65[2][i];
				}
		}

	}
	#endif
	*/
}
#endif
