#ifndef _OV9728_H_
#define _OV9728_H_

void OV9728SetFormatFps(U16 SetFormat, U8 Fps);
void CfgOV9728ControlAttr(void);
void SetOV9728ImgDir(U8 bySnrImgDir);
void SetOV9728IntegrationTime(U16 wEspline);
void SetOV9728Exposuretime_Gain(float fExpTime, float fTotalGain);
void InitOV9728IspParams(void );
void InitOV9728IspParamsByResolution(U16 wCurWidth, U16 wCurHeight);
void OV9728_POR(void );
void SetOV9728DynamicISP(U8 byAEC_Gain);
void SetOV9728DynamicISP_AWB(U16 wColorTempature);
void SetOV9728Gain(float fGain);
#endif // _OV9728_H_

