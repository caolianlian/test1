#ifndef _OV7740_H_
#define _OV7740_H_

#define OV7740_AEW	0x53 //0x3c, for AE not stable__JQG_20090810
#define OV7740_AEB	0x30
#define OV7740_VPT	0x72

extern code OV_CTT_t gc_OV7740_CTT[3];
void OV7740SetFormatFps(U8 SetFormat, U8 byFps);
void CfgOV7740ControlAttr(void);
void	SetOV7740Sharpness(U8 bySetValue);
void SetOV7740ImgDir(U8 bySnrImgDir) ;
void SetOV7740WindowCrop(U16 wXStart,U16 wXEnd,U16 wYStart,U16 wYEnd );
void SetOV7740OutputDim(U16 wWidth,U16 wHeight);
void SetOV7740PwrLineFreq(U8 byFPS, U8 byLightFrq);
void SetOV7740WBTemp(U16 wSetValue);
OV_CTT_t GetOV7740AwbGain(void);
void SetOV7740WBTempAuto(U8 bySetValue);
void SetOV7740BackLightComp(U8 bySetValue);
void SetOV7740IntegrationTimeAuto(U8 bySetValue);
void SetOV7740IntegrationTime(U16 wEspline);
void SetOV7740Gain(float fGain);
#endif // _OV7740_H_
