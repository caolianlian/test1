#ifndef _OV9715_H_
#define _OV9715_H_

void OV9715SetFormatFps(U8 SetFormat, U8 Fps);
void CfgOV9715ControlAttr(void);
void SetOV9715ImgDir(U8 bySnrImgDir);
void SetOV9715IntegrationTime(U16 wEspline);
void SetOV9715Exposuretime_Gain(float fExpTime, float fTotalGain);
void OV9715_POR();
void InitOV9715IspParams();
void SetOV9715DynamicISP(U8 byAEC_Gain);
void SetOV9715DynamicISP_AWB(U16 wColorTempature);
#endif // _OV9710_H_

