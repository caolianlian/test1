#ifndef _MI1330_H_
#define _MI1330_H_

#define MI1330_AEW    (70+10)
#define MI1330_AEB     (70-10)


typedef struct MI1330_FpsSetting
{
	U8 byFps;
	U16 wHorizClks;   // 0x300c lines_length_pck
	U16 wVertRows;   //0x300A frame_length_lines
	U16 wExtraDelay; //0x3018 extra delay
	U16 wPLLCtlReg; // 0x341c register
	U32 dwPixelClk;

} MI1330_FpsSetting_t;

void Mi1330SetFormatFps(U8 SetFormat,U8 Fps);

void Mi1330Refresh();
void CfgMI1330ControlAttr();
void SetMi1330OutputDim(U16 wWidth,U16 wHeight);
void SetMi1330WindowCrop(U16 wXStart,U16 wXEnd,U16 wYStart,U16 wYEnd );
void SetMi1330SensorEffect(U8 byEffect);
void SetMi1330ImgDir(U8 bySnrImgDir); //flip: bit1,mirror bit0.
void SetMI1330Brightness(S16 swSetValue);
void SetMi1330PwrLineFreq(U8 byFps, U8 bySetValue);
void SetMi1330ISPMisc();
void SetMi1330BackLightComp(U8 bySetValue);
void SetMi1330WBTempAuto(U8 bySetValue);
void SetMi1330WBTemp(U16 wSetValue);
U16 GetMi1330AEGain();
void SetMi1330IntegrationTimeAuto(U8 bySetValue);
void SetMi1330IntegrationTime(U16 wEspline);
void SetMI1330SoftReset(void );
#endif
