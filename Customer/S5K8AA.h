#ifndef _S5K8AA_H_
#define _S5K8AA_H_

void S5K8AASetFormatFps(U16 SetFormat, U8 Fps);
void CfgS5K8AAControlAttr(void);
void SetS5K8AAImgDir(U8 bySnrImgDir);
void SetS5K8AAIntegrationTime(U32 dwSetValue);
void SetS5K8AAExposuretime_Gain(float fExpTime, float fTotalGain);
void InitS5K8AAIspParams();
void S5K8AA_POR();
void SetS5K8AADynamicISP(U8 byAEC_Gain);
void SetS5K8AADynamicISP_AWB(U16 wColorTempature);
void SetS5K8AAGain(float fGain);
#endif