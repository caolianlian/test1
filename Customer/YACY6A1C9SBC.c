#include "Inc.h"

#ifdef RTS58XX_SP_YACY6A1C9SBC

U8 gc_digital_gain_h;
U8 gc_digital_gain_l;

#ifdef _ENABLE_OUTDOOR_DETECT_
Dyanmic_AWB_Boundary_t code gc_AWBDynmaicBoundary = {28, -8, 16, 8, 50, 30, 2, 21};
#endif

OV_CTT_t code gc_tYACY6A1C9SBC_CTT[3] =
{
	{2800, 0x112, 0x100, 0x250},
	{4000, 0x190, 0x100, 0x190},
	{6500, 0x1D8, 0x100, 0x124},
};

code OV_FpsSetting_t  g_staYACY6A1C9SBCFpsSetting[] =
{
//    FPS       ExtraDummyPixel   clkrc 	    pclk		//clkrc not used in HI165
//    {3,		(22500),	     (4),		81000000},
	{5,		(9906),	     (4),		42400000},
	{10,		(4952),	     (4), 	       42400000},
	{15,		(3302),	     (4), 		42400000},
	{30,		(1650),	     (4),		42400000}, // adjust fps >=30fps for msoc jitter
	//{30,		(1600),	     (4),		42000000}, // adjust fps >=30fps for msoc jitter
};

t_RegSettingBB code gc_YACY6A1C9SBC_800P_30fps_mipi_Setting[] =
{
///////////////////////////////////////////////////////////////////////////////
// Sensor Initial Start
///////////////////////////////////////////////////////////////////////////////

//I2C_ID = 0x60
//I2C_BYTE = 0x11

///////////////////////////////////////////////////////////////////////////////
// reset
///////////////////////////////////////////////////////////////////////////////
{0x03, 0x00},
{0x01, 0x01},
{0x01, 0x07},
{0x01, 0x01},

///////////////////////////////////////////////////////////////////////////////
// pad drive / pll setting
///////////////////////////////////////////////////////////////////////////////

{0x03, 0x00},
{0x02, 0x13},		// pclk_drive = 111b, i2c_drive = 111b
{0x0c, 0x11},		// d_pad_drive = 111b, gpio_pad_drive = 111b

// PLL INITAL //
{0x03, 0x00}, //page03
{0x07, 0x05}, //mode_pll1  24mhz / (5+1) = 4mhz
{0x08, 0x35}, //mode_pll2  isp clk = 84Mhz;
{0x09, 0x93}, //mode_pll3  isp clk div = 2/5
{0x07, 0xa5}, //mode_pll1
{0x07, 0xa5}, //mode_pll1
{0x07, 0xa5}, //mode_pll1
{0x0a, 0x80}, //mode_pll4


////////////////////////////////////////////
//Page 0x00
////////////////////////////////////////////
{0x03,	0x00},
{0x10,	0x00},
{0x11,	0x81},
{0x13,	0x00},
{0x14,	0x20},
{0x15,	0x88},
{0x17,	0x10},
{0x19,	0x00},
{0x1c,	0x00},
{0x1f,	0x00},
{0x20,	0x00},
{0x21,	0x02},
{0x22,	0x00},
{0x23,	0x02},
{0x24,	0x03},
{0x25,	0x28},
{0x26,	0x05},
{0x27,	0x08},
{0x48,	0x03},
{0x49,	0x2c},
{0x4a,	0x05},
{0x4b,	0x0c},
{0x4c,	0x06},
{0x4d,	0x40},
{0x4e,	0x03},
{0x4f,	0x58},
{0x54,	0x09},
{0x55,	0x03},
{0x56,	0x04},
{0x57,	0x40},
{0x58,	0x0d},
{0x5c,	0x0a},
{0x60,	0x00},
{0x78,	0x04},
{0x79,	0x04},
{0x80,	0x00},
{0x81,	0x00},
{0x82,	0x00},
{0x83,	0x00},
{0x84,	0x05},
{0x85,	0x10},
{0x86,	0x03},
{0x87,	0x30},
{0x90,	0x00},
{0x91,	0x02},

////////////////////////////////////////////
//Page 0x01
////////////////////////////////////////////
{0x03,	0x01},
{0x10,	0x62},
{0x11,	0x00},
{0x12,	0x66},
{0x13,	0x30},
{0x14,	0x40},
{0x15,	0x00},
{0x16,	0x80},
{0x17,	0x06},
{0x18,	0x00},
{0x19,	0x0d},
{0x1a,	0x01},
{0x1b,	0x00},
{0x50,	0x08},
{0x51,	0x88},
{0x60,	0x00},
{0x61,	0x00},
{0x62,	0x00},
{0x63,	0x00},
{0x78,	0xE0},//C0 -> E0 : ramp_offset_p@rst : 박효근 책임
{0x79,	0x00},
{0x7a,	0xC0},
{0x7b,	0x00},
{0x7c,	0x00},
{0x7d,	0x00},
{0x7e,	0x02},

////////////////////////////////////////////
//Page 0x02
////////////////////////////////////////////
{0x03,	0x02},
{0x10,	0x00},
{0x13,	0x00},
{0x16,	0x00},
{0x19,	0x00},
{0x1a,	0x00},
{0x1b,	0x00},
{0x1c,	0xc0},
{0x1d,	0x20},
{0x20,  0x9D}, //A7 -> 9D : ipxl1 : 박효근 책임
{0x21,  0x9D}, //A7 -> 9D : ipxl2 : 박효근 책임
{0x22,	0x9D}, //A0 -> 9D : iamp1 : 박효근 책임
{0x23,	0x50},
{0x24,	0x00}, //98 -> 00 : power save : 박효근 책임
{0x2a,	0x07},
{0x2e,	0x20},
{0x2f,	0x20},
{0x30,	0x00},
{0x31,	0x18},
{0x32,	0x44},
{0x33,	0x02},
{0x34,	0x50},
{0x35,	0x00},
{0x36,	0x08},
{0x37,	0x2c},
{0x3b,	0x07},
{0x3c,	0x07},
{0x3d,	0xf8},
{0x3f,	0x00},
{0x40,	0x00},
{0x41,	0x8a},
{0x42,	0x07},
{0x43,	0x25},
{0x46,	0x0E},
{0x47,	0x13},
{0x48,	0x1E},
{0x49,	0x17},
{0x4a,	0x0f},
{0x4b,	0x08},
{0x4c,	0x05},
{0x4d,	0x88},
{0x4e,	0x03},
{0x4f,	0x00},
{0x50,	0xa1},
{0x51,	0x1c},
{0x52,	0x73},
{0x54,	0xc0},
{0x55,	0x40},
{0x56,	0x33}, 
{0x58,	0x18},
{0x59,	0x16},
{0x5b,	0x00},
{0x5c,	0x02},
{0x5d,	0x00},
{0x62,	0x00},
{0x63,	0xc8},
{0x67,	0x3f},
{0x6a,	0x03},
{0x6b,	0x00},
{0x6c,	0x3c},
{0x70,	0x00},
{0x71,	0x50},
{0x72,	0x02},
{0x73,	0xfa},
{0x74,	0x00},
{0x75,	0x50},
{0x76,	0x01},
{0x77,	0xa5},
{0xa0,	0x01},
{0xa1,  0x5D}, //55 -> 5D ; ramp rst on time : 박효근 책임
{0xa2,	0x02},
{0xa3,  0x85}, //8b -> 85 ; ramp rst off time : 박효근 책임
{0xa4,	0x02},
{0xa5,	0xdd},
{0xa6,	0x06},
{0xa7,	0x30},
{0xa8,	0x01},
{0xa9,	0x4f},
{0xaa,	0x02},
{0xab,	0x23},
{0xac,	0x02},
{0xad,	0x74},
{0xae,	0x04},
{0xaf,	0x17},
{0xb0,	0x00},
{0xb1,  0x60}, //68 -> 60 : ramp clk mask on1 time : 박효근 책임
{0xb2,	0x01},
{0xb3,  0x20}, //28 -> 20 : ramp clk mask off1 time : 박효근 책임
{0xb4,	0x00},
{0xb5,	0x12},
{0xb6,	0x03},
{0xb7,	0x38},
{0xb8,	0x00},
{0xb9,	0x4d},
{0xba,	0x00},
{0xbb,	0xcd},
{0xbc,	0x00},
{0xbd,	0x10},
{0xbe,	0x01},
{0xbf,	0x90},
{0xc0,	0x00},
{0xc1,  0x60}, //68 -> 60 : ramp preset on1 time : 박효근 책임
{0xc2,	0x01},
{0xc3,  0x22},// //2a -> 22 : ramp preset off1 time : 박효근 책임
{0xc4,	0x00},
{0xc5,	0x12},
{0xc6,	0x03},
{0xc7,	0x3a},
{0xc8,	0x00},
{0xc9,	0x4d},
{0xca,	0x00},
{0xcb,	0xcf},
{0xcc,	0x00},
{0xcd,	0x10},
{0xce,	0x01},
{0xcf,	0x92},
{0xe8,	0x00},
{0xe9,	0x00},

////////////////////////////////////////////
//Page 0x03
////////////////////////////////////////////
{0x03,	0x03},
{0x10,	0x01},
{0x11,  0x5d}, //55 -> 5d ; col init on time : 박효근 책임
{0x12,	0x00},
{0x13,	0x01},
{0x14,	0x06},
{0x15,	0xee},
{0x16,	0x06},
{0x17,	0xf8},
{0x18,	0x00},
{0x19,  0x46}, //52 -> 46 ; s1 on1 time : 박효근 책임
{0x1a,	0x05},
{0x1b,	0x5a},
{0x1c,	0x00},
{0x1d,  0x46}, //52 -> 46 ; s1 on2 time : 박효근 책임
{0x1e,	0x05},
{0x1f,	0x5a},
{0x20,	0x00},
{0x21,  0x46}, //52 -> 46 ; s2 on1 time : 박효근 책임
{0x22,	0x00},
{0x23,  0x78}, //84 -> 78 ; s2 off1 time : 박효근 책임
{0x24,	0x00},
{0x25,  0x46}, //52 -> 46 ; s2 on2 time : 박효근 책임
{0x26,	0x00},
{0x27,  0x78}, //84 -> 78 ; s2 off1 time : 박효근 책임
{0x28,	0x00},
{0x29,  0x46}, //52 -> 46 ; s3 on1 time : 박효근 책임
{0x2a,	0x00},
{0x2b,  0x8E}, //86 -> 8E : s3 off time : 박효근 책임
{0x2c,	0x00},
{0x2d,  0x46}, //52 -> 46 ; s3 on2 time : 박효근 책임
{0x2e,	0x00},
{0x2f,  0x8E}, //86 -> 8E : s3 off time : 박효근 책임
{0x30,	0x00},
{0x31,	0x01},
{0x32,	0x03},
{0x33,	0x42},
{0x34,	0x00},
{0x35,	0x01},
{0x36,	0x03},
{0x37,	0x42},
{0x38,	0x00},
{0x39,	0x01},
{0x3a,	0x01},
{0x3b,	0xa0},
{0x3c,	0x00},
{0x3d,	0x01},
{0x3e,	0x01},
{0x3f,	0xa0},
{0x40,	0x00},
{0x41,	0x05},
{0x42,	0x00},
{0x43,	0x48},
{0x44,	0x00},
{0x45,	0x02},
{0x46,	0x00},
{0x47,	0x74},
{0x48,	0x00},
{0x49,	0x09},
{0x4a,	0x00},
{0x4b,	0x45},
{0x4c,	0x00},
{0x4d,	0x09},
{0x4e,	0x00},
{0x4f,	0x45},
{0x50,	0x00},
{0x51,	0x0b},
{0x52,	0x00},
{0x53,	0x33},
{0x54,	0x00},
{0x55,	0x0b},
{0x56,	0x00},
{0x57,	0x33},
{0x58,	0x00},
{0x59,	0x0b},
{0x5A,	0x00},
{0x5b,	0x33},
{0x60,	0x00},
{0x61,	0x07},
{0x62,	0x00},
{0x63,  0x15}, //0f -> 15 : pxl_ary_pwr_ctl : 박효근 책임
{0x64,	0x00},
{0x65,	0x0f},
{0x66,	0x00},
{0x67,	0x07},
{0x68,	0x00},
{0x69,	0x04},
{0x6A,	0x00},
{0x6B,	0x4a},
{0x70,	0x00},
{0x71,	0x1a},
{0x72,	0x05},
{0x73,	0x5e},
{0x74,	0x00},
{0x75,	0x24},
{0x76,	0x00},
{0x77,  0x40},// //4c -> 40 ; rx off time : 박효근 책임
{0x78,	0x05},
{0x79,	0x5b},
{0x7A,	0x05},
{0x7B,	0x63},
{0x7C,	0x05},
{0x7D,	0x5b},
{0x7E,	0x05},
{0x7F,	0x63},
{0x80,	0x01},
{0x81,  0xc6}, //cc -> c6 ; tx on time : 박효근 책임
{0x82,	0x02},
{0x83,  0x0c}, //12 -> 0c ; tx off time : 박효근 책임
{0x84,	0x05},
{0x85,	0x5b},
{0x86,	0x05},
{0x87,	0x63},
{0x88,	0x05},
{0x89,	0x5b},
{0x8A,	0x05},
{0x8B,	0x63},
{0x90,	0x00},
{0x91,	0x1c},
{0x92,	0x05},
{0x93,	0x57},
{0x94,	0x00},
{0x95,	0x1c},
{0x96,	0x05},
{0x97,	0x57},
{0x98,	0x05},
{0x99,	0x57},
{0x9a,	0x00},
{0x9b,	0x1c},
{0x9c,	0x05},
{0x9d,	0x57},
{0x9e,	0x00},
{0x9f,	0x1c},
{0xa0,	0x00},
{0xa1,	0x07},
{0xa2,	0x00},
{0xa3,	0x15},
{0xa4,	0x00},
{0xa5,	0x07},
{0xa6,	0x00},
{0xa7,	0x15},
{0xa8,	0x00},
{0xa9,	0x20},
{0xaa,	0x00},
{0xab,	0x30},
{0xac,	0x00},
{0xad,	0x20},
{0xae,	0x00},
{0xaf,	0x30},
{0xb4,	0x00},
{0xb5,	0x00},
{0xb6,	0x00},
{0xb7,	0xc8},
{0xb8,	0x01},
{0xb9,	0x20},
{0xba,	0x02},
{0xbb,	0x9d},
{0xe0,	0x00},
{0xe1,	0x1a},
{0xe2,	0x01},
{0xe3,  0xbc}, //0xc2=>0xbc : for cds_rst_t : 박효근 책임
{0xe4,	0x01},
{0xe5,	0xe9}, //0xef=>0xe9 : for cds_sig_t : 박효근 책임
{0xe6,	0x05},
{0xe7,	0x58},
{0xe8,	0x00},
{0xe9,  0x43},// //4f -> 43 ; cds rst clp en on time : 박효근 책임
{0xea,	0x01},
{0xeb,  0xbc}, //c2 -> bc ; cds rst clp en off time : 박효근 책임
{0xec,	0x06},
{0xed,	0xfc},
{0xee,	0x00},
{0xef,	0x00},
{0xf0,	0x00},
{0xf1,	0x74},
{0xfa,	0x01},
{0xfb,	0xe9}, //0xef=>0xe9 : for sig clamp : 박효근 책임
{0xfc,	0x05},
{0xfd,	0x44},

////////////////////////////////////////////
//Page 0x04
////////////////////////////////////////////
{0x03,	0x04},
{0x10,	0x03},
{0x11,	0x01},
{0x12,	0x22},
{0x13,	0x22},
{0x14,	0x00},
{0x15,	0x00},
{0x20,	0x00},
{0x21,	0x38},
{0x22,	0x00},
{0x23,	0x70},
{0x24,	0x00},
{0x25,	0xa8},
{0x26,	0x00},
{0x27,	0xc5},
{0x28,	0x01},
{0x29,	0x8a},
{0x2a,	0x02},
{0x2b,	0x4f},
{0x30,	0x00},
{0x31,	0x20},
{0x32,	0x00},
{0x33,	0x40},
{0x34,	0x00},
{0x35,	0x60},
{0x36,	0x00},
{0x37,	0x62},
{0x38,	0x00},
{0x39,	0xc4},
{0x3a,	0x01},
{0x3b,	0x26},
{0x50,	0x00},
{0x58,	0x00},
{0x59,	0x1c},
{0x5a,	0x05},
{0x5b,	0x5d},
{0x5c,	0x00},
{0x5d,	0x42}, //0x4e=>0x42 : for cds_amp1_bais_smpl_ctl : 박효근 책임
{0x5e,	0x05},
{0x5f,	0x5d},
{0x60,	0x00},
{0x61,	0x42}, //0x4e=>0x42 : for cds_amp2_bias_smpl_ctl : 박효근 책임
{0x62,	0x05},
{0x63,	0x5d},
{0x70,  0x0A}, //08 -> 0A ; amp1 bw adaptive ctrl on : 박효근 책임
{0x71,	0x3b},
{0x72,	0x3b},
{0x73,	0x10},
{0x74,	0x1c},
{0x75,	0x1c},
{0x76,	0x07},
{0x80,	0xf0},
{0x81,	0x00},
{0x82,	0x70},
{0x83,	0x00},
{0x84,	0x70},
{0x85,	0x00},
{0x86,	0x30},
{0x87,	0x00},
{0x88,	0x30},
{0x89,	0x00},
{0x8a,	0x10},
{0x8b,	0x00},
{0x8c,	0x10},
{0x8d,	0x00},
{0x8e,	0x00},
{0x8f,	0x00},
{0x90,	0x03},
{0x91,	0x88},
{0x92,	0x88},
{0x93,	0x88},
{0x94,	0x88},
{0x95,	0x00},
{0x96,	0x00},
{0x97,  0x18}, //30 -> 18 ; vrst offset @ag=4~8/adaptive ctrl : 박효근 책임
{0x98,  0x18}, //30 -> 18 ; vrst offset @ag=8~16/adaptive ctrl : 박효근 책임
{0xa0,	0x00},
{0xa1,	0x93}, //0x8b=>0x93    //for cds_s1 scene DMA outdoor : 박효근 책임
{0xa2,	0x00},
{0xa3,	0x93}, //0x8b=>0x93    //for cds_s1 scene DMA indoor : 박효근 책임
{0xa4,	0x05},
{0xa5,	0x52},
{0xa6,	0x01},
{0xa7,	0xe9}, //0xef=>0xe9 : for cds_s1 scene DMA outdoor : 박효근 책임
{0xa8,	0x01},
{0xa9,	0xe9}, //0xef=>0xe9 : for cds_s1 scene DMA indoor : 박효근 책임
{0xaa,	0x00},
{0xab,	0x52},
{0xb0,	0x00},
{0xb1,	0x04},
{0xb2,	0x00},
{0xb3,	0x04},
{0xb4,	0x00},

////////////////////////////////////////////
//Page 0x0a
////////////////////////////////////////////
{0x03,	0x0A},
{0x69,	0x01},

////////////////////////////////////////////
//Page 0x08
////////////////////////////////////////////
{0x03,	0x08},
{0x10,	0x01},

////////////////////////////////////////////
//Page 0x10
////////////////////////////////////////////
{0x03,	0x10},
{0x10,	0x00},
{0x20,	0x01},
{0x40,	0x3D},
{0x50,	0x07},

////////////////////////////////////////////
//Page 0x20
////////////////////////////////////////////
{0x03,	0x20},
{0x10,	0x44},
//{0x10,	0x64},//sam added
{0x12,	0x01},//sam added
{0x22,	0x03},
{0x23,	0x56},
{0x26,	0xff},
{0x27,	0xff},
{0x29,	0x00},
{0x2a,	0x02},
{0x2b,	0x00},
{0x2c,	0x02},
{0x30,	0x03},
{0x31,	0x56},
{0x60,	0xF0},
{0x61,	0x00},

///////////////////////////////////////////////////////////////////////////////
// MIPI TX Setting
///////////////////////////////////////////////////////////////////////////////
{0x03,0x05},  // Page05
{0x11,0x10},  // lvds_ctl_2 Non Con CLK
{0x12,0x00},  // crc_ctl
{0x13,0x00},  // serial_ctl
{0x14,0x00},  // ser_out_ctl_1
{0x15,0x00},  // dphy_fifo_ctl
{0x16,0x02},  // lvds_inout_ctl1
{0x18,0x80},  // lvds_inout_ctl3
{0x1a,0xf0},  // lvds_time_ctl
{0x1c,0x08},  // tlpx_time_l_dp
{0x1d,0x0c},  // tlpx_time_l_dn
{0x1e,0x06},  // hs_zero_time
{0x1f,0x09},  // hs_trail_time
{0x21,0xb8},  // hs_sync_code
{0x22,0x00},  // frame_start_id
{0x23,0x01},  // frame_end_id
{0x24,0x2b},  // long_packet_id
{0x27,0x00},  // lvds_frame_end_cnt_h
{0x28,0x02},  // lvds_frame_end_cnt_l
{0x30,0x06},  // l_pkt_wc_h  // Full = 1288*5/4 (Bayer10)
{0x31,0x4a},  // l_pkt_wc_l
{0x32,0x0e},  // clk_zero_time
{0x33,0x0b},  // clk_post_time
{0x34,0x03},  // clk_prepare_time
{0x35,0x03},  // clk_trail_time
{0x36,0x01},  // clk_tlpx_time_dp
{0x37,0x06},  // clk_tlpx_time_dn
{0x39,0x0f},  // lvds_bias_ctl
{0x3a,0x00},  // lvds_test_tx
//{0x4c,0x41},  // hs_wakeup_size_h
//{0x4d,0x20},  // hs_wakeup_size_l
{0x4e,0x00},  // mipi_int_time_h
{0x4f,0xff},  // mipi_int_time_l
{0x50,0x10},  // cntns_clk_wait_h
{0x51,0x00},  // cntns_clk_wait_l
{0x10,0x1d},  // lvds_ctl_1

///////////////////////////////////////////////////////////////////////////////
// sleep off
///////////////////////////////////////////////////////////////////////////////

{0x03, 0x00},
{0x01, 0x00},	// sleep off
///////////////////////////////////////////////////////////////////////////////
// end of this set
///////////////////////////////////////////////////////////////////////////////
};

static OV_FpsSetting_t*  GetOvFpsSetting(U8 Fps, OV_FpsSetting_t staOvFpsSetting[], U8 byArrayLen)
{
	U8 i;
	U8 Idx;

	ISP_MSG((" array size = %bd\n", byArrayLen));

	if(g_bIsHighSpeed)
	{
		Idx=2; //Default FPS in Highspeed:15
	}
	else
	{
		Idx = 0; //Default FPS in FULL or LOW speed:5
	}

	for(i=0; i< byArrayLen; i++)
	{
		ISP_MSG((" fps i = %bd\n",i));
		
		if(staOvFpsSetting[i].byFps ==Fps)
		{
			Idx=i;
			break;
		}
	}
	
	ISP_MSG((" get fps idx = %bd\n",Idx));
	
	return &staOvFpsSetting[Idx];
}


static U16 MapSnrGlbGain2SnrRegSetting(float wGain)
{
	U16  wSensorGain = 0;
		
	if(wGain <= 256) // MAX Analog Gain : 16; MIN Analog Gain : 1
	{
		//wSensorGain = wGain*2;//(wGain/16)*32
		wSensorGain = 8192/wGain -32;
		//printf("%x\n",wSensorGain);
		wSensorGain = wSensorGain<<7;
		//printf("%x\n\n",wSensorGain);
		gc_digital_gain_h = 0x04;//digital gain 1x
		gc_digital_gain_l = 0x00;
	}
	else if(wGain < 1024) //16*16*4
	{
		wSensorGain = 0x0000;//16
		gc_digital_gain_h = (U8)(wGain/256);//(wGain/16)/16
		gc_digital_gain_l = (U8)(wGain-gc_digital_gain_h*256);
	}
	else
	{
		wSensorGain = 0x0000;//16
		gc_digital_gain_h = 0x0F;//digital gain 4x
		gc_digital_gain_l = 0xFF;
	}
			
	return wSensorGain;
}

static float SnrRegSetting2SnrGlbGain(U16 wSnrRegGain)
{
#if 0
	float fAnalogGain;
	float fDigitalGain;

	fAnalogGain = 256.0/(((float)wSnrRegGain/2)+16.0);
	fDigitalGain = ((float)(gc_digital_gain_h*256+ gc_digital_gain_l))/1024.0;
	return fAnalogGain*fDigitalGain;
#endif
	return (256.0/(((float)(wSnrRegGain>>7)/2)+16.0))*(((float)(gc_digital_gain_h*256+ gc_digital_gain_l))/1024.0);
}

//this function is also called during Probe stage to calculate the transfer_size/LWM/HWM
void GetSensorPclkHsync(U16 wSensorSPFormat,U8 byFps)
{
	OV_FpsSetting_t *pYACY6A1C9SBC_FpsSetting=NULL;
	
	// hemonel 2010-04-13: for delete complier warning
	wSensorSPFormat =wSensorSPFormat;
	
	pYACY6A1C9SBC_FpsSetting=GetOvFpsSetting(byFps, g_staYACY6A1C9SBCFpsSetting, sizeof(g_staYACY6A1C9SBCFpsSetting)/sizeof(OV_FpsSetting_t));
	
	g_wSensorHsyncWidth = pYACY6A1C9SBC_FpsSetting->wExtraDummyPixel; // this for manual exposure
	g_dwPclk = pYACY6A1C9SBC_FpsSetting->dwPixelClk;		// this for scale speed
}

void YACY6A1C9SBCSetFormatFps(U16 SetFormat, U8 Fps)
{
	U16 wTemp;
	OV_FpsSetting_t *pYACY6A1C9SBC_FpsSetting;
	
	SetFormat = SetFormat;

	pYACY6A1C9SBC_FpsSetting = GetOvFpsSetting(Fps, g_staYACY6A1C9SBCFpsSetting, sizeof(g_staYACY6A1C9SBCFpsSetting)/sizeof(OV_FpsSetting_t));

	// initial all register setting
#if 0
	WriteSensorSettingBB(sizeof(gc_YACY6A1C9SBC_800P_30fps_mipi_Setting_part1)/2, gc_YACY6A1C9SBC_800P_30fps_mipi_Setting_part1);
	uDelay(50);
	WriteSensorSettingBB(sizeof(gc_YACY6A1C9SBC_800P_30fps_mipi_Setting_part2)/2, gc_YACY6A1C9SBC_800P_30fps_mipi_Setting_part2);
	uDelay(50);
	WriteSensorSettingBB(sizeof(gc_YACY6A1C9SBC_800P_30fps_mipi_Setting_part3)/2, gc_YACY6A1C9SBC_800P_30fps_mipi_Setting_part3);
	uDelay(50);
	WriteSensorSettingBB(sizeof(gc_YACY6A1C9SBC_800P_30fps_mipi_Setting_part4)/2, gc_YACY6A1C9SBC_800P_30fps_mipi_Setting_part4);
	uDelay(50);
	WriteSensorSettingBB(sizeof(gc_YACY6A1C9SBC_800P_30fps_mipi_Setting_part5)/2, gc_YACY6A1C9SBC_800P_30fps_mipi_Setting_part5);
	uDelay(50);
	WriteSensorSettingBB(sizeof(gc_YACY6A1C9SBC_800P_30fps_mipi_Setting_part6)/2, gc_YACY6A1C9SBC_800P_30fps_mipi_Setting_part6);
	uDelay(50);
	WriteSensorSettingBB(sizeof(gc_YACY6A1C9SBC_800P_30fps_mipi_Setting_part7)/2, gc_YACY6A1C9SBC_800P_30fps_mipi_Setting_part7);
	uDelay(50);
	WriteSensorSettingBB(sizeof(gc_YACY6A1C9SBC_800P_30fps_mipi_Setting_part8)/2, gc_YACY6A1C9SBC_800P_30fps_mipi_Setting_part8);
	uDelay(50);
	WriteSensorSettingBB(sizeof(gc_YACY6A1C9SBC_800P_30fps_mipi_Setting_part9)/2, gc_YACY6A1C9SBC_800P_30fps_mipi_Setting_part9);
#endif
#if 0	
	WriteSensorSettingBB(4, gc_YACY6A1C9SBC_800P_30fps_mipi_Setting);
	uDelay(50);
	WriteSensorSettingBB(11, gc_YACY6A1C9SBC_800P_30fps_mipi_Setting+4*2);
	uDelay(50);
	WriteSensorSettingBB(484, gc_YACY6A1C9SBC_800P_30fps_mipi_Setting+15*2);
	uDelay(50);
	WriteSensorSettingBB(34, gc_YACY6A1C9SBC_800P_30fps_mipi_Setting+499*2);
	uDelay(50);
	WriteSensorSettingBB(2, gc_YACY6A1C9SBC_800P_30fps_mipi_Setting+533*2);
	uDelay(50);
#endif
	//printf("%d\n",sizeof(gc_YACY6A1C9SBC_800P_30fps_mipi_Setting)/2);
	WriteSensorSettingBB(sizeof(gc_YACY6A1C9SBC_800P_30fps_mipi_Setting)/2, gc_YACY6A1C9SBC_800P_30fps_mipi_Setting);
	//printf("Sensor setting download finished!\n");
	
	// 2) write sensor register for current FPS
	//Write_SenReg(0x0305, pYACY6A1C9SBC_FpsSetting->byClkrc);
	wTemp = pYACY6A1C9SBC_FpsSetting->wExtraDummyPixel;
	//-----------Write  frame width------------
	Write_SenReg(0x03, 0x00);
	Write_SenReg(0x4c, INT2CHAR(wTemp, 1));//H
	Write_SenReg(0x4d, INT2CHAR(wTemp, 0));//L

// 3) update variable for AE
       GetSensorPclkHsync(HD800P_FRM,Fps);

	//g_wSensorHsyncWidth = 1600; // this for manual exposure
	g_wAEC_LineNumber = 856;	// this for AE insert dummy line algothrim
	g_wAECExposureRowMax = 854; //856-2;	// 0x202b~0x202c max margin 2

	g_wSensorWidthBefBLC = 1280;
	g_wSensorHeightBefBLC = 800;
		
	InitYACY6A1C9SBCIspParamsByResolution(g_wCurFrameWidth, g_wCurFrameHeight);

	//-----------------------------Dphy Setting--------------------------------
	SetMipiDphy(MIPI_DATA_LANE0_EN, \
	            MIPI_DATA_FORMAT_RAW10 , MIPI_DATA_TYPE_RAW10, \
	            HSTERM_EN_TIME_33);   
}

void CfgYACY6A1C9SBCControlAttr(void)
{
	U8 i;

	g_bySensorSize = SENSOR_SIZE_HD800P;
	g_wSensorSPFormat = HD800P_FRM;

	memcpy(g_asOvCTT, gc_tYACY6A1C9SBC_CTT,sizeof(gc_tYACY6A1C9SBC_CTT));//get manual white balance

	//sensor board
	//  SVA:2.8V SVIO:1.8V
	//XBYTE[REG_TUNESVA] = SVA_VOL_2V81;
	//XBYTE[REG_TUNESVIO] = SVIO_VOL_1V81;
	//module
#if (_CHIP_ID_ & _RTS5840_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_1V16;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_2V79;
#elif (_CHIP_ID_ & _RTS5832_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_1V20;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_2V81;
#elif (_CHIP_ID_ & _RTS5829B_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_1V17;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_2V81;
#endif	

	//====== resolution  setting ===========
	// -- preview ---
	g_aVideoFormat[0].byaVideoFrameTbl[0] = 9;		// resolution number
	g_aVideoFormat[0].byaVideoFrameTbl[1] = F_SEL_640_480;
	g_aVideoFormat[0].byaVideoFrameTbl[2] = F_SEL_320_180;
	g_aVideoFormat[0].byaVideoFrameTbl[3] = F_SEL_320_240;
	g_aVideoFormat[0].byaVideoFrameTbl[4] = F_SEL_424_240;
	g_aVideoFormat[0].byaVideoFrameTbl[5] = F_SEL_640_360;
	g_aVideoFormat[0].byaVideoFrameTbl[6] = F_SEL_848_480;
	g_aVideoFormat[0].byaVideoFrameTbl[7] = F_SEL_960_540;
	g_aVideoFormat[0].byaVideoFrameTbl[8] = F_SEL_1280_720;
	g_aVideoFormat[0].byaVideoFrameTbl[9] = F_SEL_1280_800;
		//--still image--
	g_aVideoFormat[0].byaStillFrameTbl[0] = 9;	// resolution number
	g_aVideoFormat[0].byaStillFrameTbl[1] = F_SEL_640_480;
	g_aVideoFormat[0].byaStillFrameTbl[2] = F_SEL_320_180;
	g_aVideoFormat[0].byaStillFrameTbl[3] = F_SEL_320_240;
	g_aVideoFormat[0].byaStillFrameTbl[4] = F_SEL_424_240;
       g_aVideoFormat[0].byaStillFrameTbl[5] = F_SEL_640_360;
	g_aVideoFormat[0].byaStillFrameTbl[6] = F_SEL_848_480;
	g_aVideoFormat[0].byaStillFrameTbl[7] = F_SEL_960_540;
	g_aVideoFormat[0].byaStillFrameTbl[8] = F_SEL_1280_720;
	g_aVideoFormat[0].byaStillFrameTbl[9] = F_SEL_1280_800;
	
	//====== fps  setting ===========		
	for(i=0; i<10; i++)
	{
		g_aVideoFormat[0].waFrameFpsBitmap[i] = FPS_15|FPS_20|FPS_25|FPS_30;
	}
	
	for(i=10; i<19; i++)
	{
		g_aVideoFormat[0].waFrameFpsBitmap[i] = FPS_10|FPS_5;
	}
	
	g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_1920_1080] = FPS_5;

	//====== format type  setting ===========
	g_aVideoFormat[0].byFormatType = FORMAT_TYPE_YUY2;

#ifdef _ENABLE_MJPEG_
	// copy YUY2 setting to MJPEG setting
	memcpy(&(g_aVideoFormat[1]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

	// modify MJPEG FPS setting
	for(i=10; i<20; i++)
	{
		g_aVideoFormat[1].waFrameFpsBitmap[i] = (FPS_15|FPS_20|FPS_25|FPS_30);
	}

	// modify MJPEG format type
	g_aVideoFormat[1].byFormatType = FORMAT_TYPE_MJPG;
#endif

#ifdef _ENABLE_M420_FMT_
	// copy YUY2 setting to M420 setting
	memcpy(&(g_aVideoFormat[2]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

	// modify M420 FPS setting
	g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_800_600] = FPS_25|FPS_20|FPS_15|FPS_10|FPS_5;        
	g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_960_540] = FPS_25|FPS_20|FPS_15|FPS_10|FPS_5;        
	g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1024_768] = FPS_15|FPS_10|FPS_5;                  
	g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_720] = FPS_10|FPS_5;  
	
	// modify M420 format type
	g_aVideoFormat[2].byFormatType = FORMAT_TYPE_M420;
	
#endif
	
	InitYACY6A1C9SBCIspParams();
}

void SetYACY6A1C9SBCIntegrationTime(U16 wEspline)
{
//#if 0
	U16 wFrameLenLines;
	U16 wExpMax = g_wAECExposureRowMax;

	//printf("wEspline = %d\n", wEspline);

	if(wEspline > wExpMax)
	{
		wFrameLenLines = wEspline + 2;
	}
	else
	{
		wFrameLenLines = g_wAEC_LineNumber;
	}

	if (wFrameLenLines%2==1)
	{
		wFrameLenLines++;
	}

	Write_SenReg(0x03, 0x00);
	Write_SenReg(0x4e, INT2CHAR(wFrameLenLines, 1));//H
	Write_SenReg(0x4f, INT2CHAR(wFrameLenLines, 0));//L

	uDelay(5000);
	
	// group register hold
       Write_SenReg(0x03, 0x00);
	Write_SenReg_Mask(0x1f, 0x01, 0x01);

	//-----------Write  frame length or dummy lines------------
	//Write_SenReg(0x03, 0x00);
	//Write_SenReg(0x4e, INT2CHAR(wFrameLenLines, 1));//H
	//Write_SenReg(0x4f, INT2CHAR(wFrameLenLines, 0));//L

	//-----------Write Exposuretime setting-----------
	Write_SenReg(0x03, 0x20);
	Write_SenReg(0x23, INT2CHAR(wEspline, 0));	// coarse_integration_time  low
	Write_SenReg(0x22, INT2CHAR(wEspline, 1));	// coarse_integration_time  high

	//----------- write gain setting-------------------
	//analog
	Write_SenReg(0x03, 0x20);
	Write_SenReg(0x60, 0xf0);//H //fixed at 1x
	Write_SenReg(0x61, 0x00);//L
	
	//digital
	Write_SenReg(0x03, 0x08);
	Write_SenReg(0x20, 0x04);//R //fixed at 1x
	Write_SenReg(0x22, 0x04);//G
	Write_SenReg(0x24, 0x04);//B
	Write_SenReg(0x21, 0x00);
	Write_SenReg(0x23, 0x00);
	Write_SenReg(0x25, 0x00);

	SetISPAEGain(1, 1, 1);
	
       //group register release
	Write_SenReg(0x03, 0x00);
	Write_SenReg_Mask(0x1f, 0x00, 0x01);
//#endif
}

void SetYACY6A1C9SBCImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.
{
	WaitFrameSync(ISP_INT1, ISP_DATA_START_INT);

	bySnrImgDir ^= 0x00;
	Write_SenReg(0x03, 0x00);
	Write_SenReg_Mask(0x11, bySnrImgDir, 0x03);

	WaitFrameSync(ISP_INT1, ISP_DATA_END_INT);
	
	switch(bySnrImgDir)
	{
	       case 0:	
		SetBLCWindowStart(0, 1);//not this case
                             break;
              case 1:			  		
		SetBLCWindowStart(1, 1);	   	
                             break;
	}
}

void SetYACY6A1C9SBCExposuretime_Gain(float fExpTime, float fTotalGain)
{
//#if 0
	U16  wExposureRows_floor;
	U16  wGainRegSetting; //data
	float  fSnrGlbGain;
	U16   wSetDummy;
	//float fTemp;
	//U16 wExposurePixels;

	wGainRegSetting=MapSnrGlbGain2SnrRegSetting((U16)(fTotalGain*16.0));
	fSnrGlbGain = SnrRegSetting2SnrGlbGain(wGainRegSetting);

	wExposureRows_floor = (U16)(fExpTime/g_fSensorRowTimes);  //unit : 1row

	/*
	printf("\n%f %f\n",fExpTime,fTotalGain);
	printf("%x\n",wGainRegSetting);
	printf("%f\n",fSnrGlbGain);
	//printf("%bx %bx\n",gc_digital_gain_h,gc_digital_gain_l);
	printf("%x\n",wExposureRows_floor);
	printf("%bu\n",g_byAEMeanValue);
	printf("%f\n",g_fSensorRowTimes);*/
//	fTemp = (float)wExpTime-((float)wExposureRows_floor*(g_fSensorRowTimes)); //fine integration time;unit:  us
//	wExposurePixels =(U16)(fTemp * (float)(g_dwPclk/10000)) ; //unit: pixels

	// set dummy
	wSetDummy = g_wAFRInsertDummylines + g_wAEC_LineNumber;
	//-----------Write  frame length or dummy lines------------
	if (wSetDummy%2 == 1)
	{
		wSetDummy++;
	}

	// write exposure, gain, dummy line
	if(fabs(g_fCurExpTime-fExpTime) > 0.0001)
	{
		WaitFrameSync(ISP_INT1, ISP_FRM_END_INT);
		
		// group register hold
		Write_SenReg(0x03, 0x00);
		Write_SenReg_Mask(0x1f, 0x01, 0x01);

		//-----------Write  frame length or dummy lines------------
		Write_SenReg(0x03, 0x00);
		Write_SenReg(0x4e, INT2CHAR(wSetDummy, 1));//H
		Write_SenReg(0x4f, INT2CHAR(wSetDummy, 0));//L

		//Write Exposuretime setting
		Write_SenReg(0x03, 0x20);
		Write_SenReg(0x23, INT2CHAR(wExposureRows_floor, 0));	// coarse_integration_time  low
		Write_SenReg(0x22, INT2CHAR(wExposureRows_floor, 1));	// coarse_integration_time  high

		//group register release
		/*
		Write_SenReg(0x03, 0x00);
		Write_SenReg_Mask(0x1f, 0x00, 0x01);

		// delay one frame for sync
		WaitFrameSync(ISP_INT1, ISP_DATA_START_INT);
		
		// group register hold
		Write_SenReg(0x03, 0x00);
		Write_SenReg_Mask(0x1f, 0x01, 0x01);*/

		WaitFrameSync(ISP_INT1, ISP_FRM_END_INT);

		//write analog gain setting
		Write_SenReg(0x60, INT2CHAR(wGainRegSetting, 1));//H
		Write_SenReg(0x61, INT2CHAR(wGainRegSetting, 0));//L

		//set digital gain
		Write_SenReg(0x03, 0x08);
		Write_SenReg(0x20, gc_digital_gain_h);//R
		Write_SenReg(0x22, gc_digital_gain_h);//G
		Write_SenReg(0x24, gc_digital_gain_h);//B
		Write_SenReg(0x21, gc_digital_gain_l);
		Write_SenReg(0x23, gc_digital_gain_l);
		Write_SenReg(0x25, gc_digital_gain_l);
	
		SetISPAEGain(fTotalGain, fSnrGlbGain, 1); //delay 1 frame

		WaitFrameSync(ISP_INT1, ISP_DATA_END_INT);
		
		//group register release
		Write_SenReg(0x03, 0x00);
		Write_SenReg_Mask(0x1f, 0x00, 0x01);
		
		g_fCurExpTime = fExpTime;	// back up exposure rows
	}
	else
	{
		WaitFrameSync(ISP_INT1, ISP_FRM_END_INT);
		
		// group register hold
		Write_SenReg(0x03, 0x00);
		Write_SenReg_Mask(0x1f, 0x01, 0x01);
		
		//-----------Write  frame length or dummy lines------------
		Write_SenReg(0x03, 0x00);
		Write_SenReg(0x4e, INT2CHAR(wSetDummy, 1));//H
		Write_SenReg(0x4f, INT2CHAR(wSetDummy, 0));//L
		
		// write analog gain setting
		Write_SenReg(0x03, 0x20);
		Write_SenReg(0x60, INT2CHAR(wGainRegSetting, 1));//H
		Write_SenReg(0x61, INT2CHAR(wGainRegSetting, 0));//L
		
		//set digital gain
		Write_SenReg(0x03, 0x08);
		Write_SenReg(0x20, gc_digital_gain_h);//R
		Write_SenReg(0x22, gc_digital_gain_h);//G
		Write_SenReg(0x24, gc_digital_gain_h);//B
		Write_SenReg(0x21, gc_digital_gain_l);
		Write_SenReg(0x23, gc_digital_gain_l);
		Write_SenReg(0x25, gc_digital_gain_l);

		SetISPAEGain(fTotalGain, fSnrGlbGain, 1);

		WaitFrameSync(ISP_INT1, ISP_DATA_END_INT);

		// group register release
		Write_SenReg(0x03, 0x00);
		Write_SenReg_Mask(0x1f, 0x00, 0x01);
	}
	
	return;
//#endif
}

void YACY6A1C9SBC_POR(void)
{
	//CHIP_EN pull up
	//XBYTE[0xFF9E] = 0x10;	//XBYTE[PG_DELINK_CTRL] = PAD_DELINK_OUT
	//XBYTE[0xFF9E] = 0x80;	//XBYTE[PG_DELINK_CTRL] = PAD_DELINK_PULL_UP
	XBYTE[0xFF9E] = 0x40;

	ENTER_SENSOR_RESET();//RESETB pull low
	Init_MIPI();
	
	// power on
	g_bySnrPowerOnSeq =SNR_PWRCTL_SEQ_GPIO8| (SNR_PWRCTL_SEQ_SV18<<2)|(SNR_PWRCTL_SEQ_SV28<<4);
	
	SensorPowerControl(SWITCH_ON, EN_DELAYTIME);
	uDelay(21);
	// Initialize I2C Controller Timing
	
	// clock select 24M
	if( g_bySSCEnable== TRUE)
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_SSC_24M);
	}
	else
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_96M|CCS_CLK_DIVIDER_4);
	}
	EnableSensorHCLK();

	uDelay(41);//21
	
	//CHIP_EN pull down
	XBYTE[0xFF9E] = 0x10;	//XBYTE[PG_DELINK_CTRL] = PAD_DELINK_OUT
	//XBYTE[0xFF9E] = 0x40;	//XBYTE[PG_DELINK_CTRL] = PAD_DELINK_PULL_DOWN

	uDelay(201);//101
	LEAVE_SENSOR_RESET();//RESETB pull up
	
	g_bySensorIsOpen = SENSOR_OPEN;
	WaitTimeOut_Delay(1);  //HI165 spec request 16 MCU clk
}

static void InitYACY6A1C9SBCIspParams(void)
{
	// AWB initial gain
	//use D50 R/G/B gain as AWB initial value
	g_wAWBRGain_Last = 0x17C;
	g_wAWBGGain_Last = 0x100;
	g_wAWBBGain_Last = 0x226;
	g_wProjectGR = g_wAWBRGain_Last;
	g_wProjectGB = g_wAWBBGain_Last;

	// Special ISP
	g_bySaturation_Def = 64;
	g_byContrast_Def = 32; //Neil Tuning at chicony
	g_byTgamma_rate_max = 63;
	g_byTgamma_rate_min = 20;

	//g_wDynamicISPEn = 0;
	g_wDynamicISPEn = DYNAMIC_LSC_CT_EN| DYNAMIC_LSC_EN | DYNAMIC_SHARPNESS_EN | DYNAMIC_SHARPPARAM_EN | DYNAMIC_GAMMA_EN | DYNAMIC_CCM_CT_EN;

#ifdef _ENABLE_MJPEG_
	g_byQtableScale = 6;	//HD720P HD800P Setting=6 for 7 comp rate
#endif
}

void InitYACY6A1C9SBCIspParamsByResolution(U16 wCurWidth, U16 wCurHeight)
{
	wCurWidth = wCurWidth;
	wCurHeight = wCurHeight;
}

void SetYACY6A1C9SBCDynamicISP(U8 byAEC_Gain)
{
	byAEC_Gain = byAEC_Gain;
}

void SetYACY6A1C9SBCDynamicISP_AWB(U16 wColorTempature)
{
	wColorTempature =wColorTempature;

	// hemonel 2011-07-04: Albert use only one LSC curve to pass MSOC premium test, so delete these code
	/*
	#ifdef _MSOC_TEST_
	U8 i;

	// LSC Curve dynamic
	if ( (g_wDynamicISPEn&DYNAMIC_LSC_EN) == DYNAMIC_LSC_EN)
	{
		if(wColorTempature < 4000)
		{
	          		for (i=0;i<6;i++)
	          		{
	          			g_awLensShadingCenter[i] = gc_wLSC_Center_Offset_OV9726_3500K[i];
	          		}

				for (i=0;i<48;i++)
				{
					XBYTE[ISP_NLSC_CURVE_R_BASE+i]  = gc_byLSC_RGB_Curve_OV9726_3500K[0][i];
					XBYTE[ISP_NLSC_CURVE_G_BASE+i]  = gc_byLSC_RGB_Curve_OV9726_3500K[1][i];
					XBYTE[ISP_NLSC_CURVE_B_BASE+i]  = gc_byLSC_RGB_Curve_OV9726_3500K[2][i];
				}
		}
		else if(wColorTempature > 4500)
		{
	          		for (i=0;i<6;i++)
	          		{
	          			g_awLensShadingCenter[i] = gc_wLSC_Center_Offset_OV9726_D65[i];
	          		}

				for (i=0;i<48;i++)
				{
					XBYTE[ISP_NLSC_CURVE_R_BASE+i]  = gc_byLSC_RGB_Curve_OV9726_D65[0][i];
					XBYTE[ISP_NLSC_CURVE_G_BASE+i]  = gc_byLSC_RGB_Curve_OV9726_D65[1][i];
					XBYTE[ISP_NLSC_CURVE_B_BASE+i]  = gc_byLSC_RGB_Curve_OV9726_D65[2][i];
				}
		}

	}
	#endif
	*/
}
#endif
