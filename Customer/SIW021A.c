#include "Inc.h"

#ifdef RTS58XX_SP_SIW021A
//_________________________
#define DYNAMIC_DarkMode_CCM_EN		0x0800
//__________________________
U8 CCM_frame_count =0;
U8 g_byCCM2State  = 0x00;

U16 MapSnrGlbGain2SnrRegSetting(U8 byGain);
void WriteSIW021AExposuretime(U16 wExposureRows);
void WriteSIW021AGain(U16 wGain);
void WriteSIW021ADummyLine(U16 wDummy);
U8 Digital_fractional_sensorGain;

OV_CTT_t code gc_SIW021A_CTT[3] =
{
	{2800,224,256,464},
	{4050,336,256,416},
	{6700,416,256,296},
};

typedef struct SIW021A_FpsSetting
{
	U8 byFps;
	U8 byClkSource;
	U8 byClkDiv;
	U8 byFBDiv;
	U8 byPreScaler;
	U8 bySysClkDiv;
	U32 dwPixelClk;
} SIW021A_FpsSetting_t;      //Jimmy.100820.HiMax HM1055


code SIW021A_FpsSetting_t  g_staSIW021AFpsSetting[]=
{
	//  FPS  wClkSource, wClkDiv, wFBDiv, wPreScaler, BLANKRH    BLANKRL, bySysClkDiv, pclk = 24M/(clkrc+1)
	//{3,    0x80,0x99,       512,            459,            (0x00),   (0x07),      (0x00),        72000000},  // 12M  29.49678485
	{5,      (0x80),(0x01),(0x17),(0x60),(0x00),12000000},  // 12M  29.49678485
	//{7,    0x80,0x99,       512,            459,            (0x00),   (0x07),      (0x00),        72000000},  // 12M  29.49678485
	//{10,    (0x80),(0x00),(0x17),(0x60),(0x00),24000000},  // 12M  29.49678485
	{10,    (0x00),(0x00),(0x17),(0x60),(0x00),6000000},
	//{15,    (0x00),(0x00),(0x17),(0x60),(0x00),36000000},  // 12M  15.000375
	//{20,   0x80,0x99,       768,            765,            (0x01),   (0x06),      (0x00),        72000000},  // 12M  20.00720259
	//{25,   0x80,0x99,       640,            612,            (0x00),   (0x6D),      (0x00),        72000000},  // 12M  25.000625
	{30,    (0x00),(0x00),(0x17),(0x60),(0x00),32000000},  // 12M  29.49678485
};

t_RegSettingWB code gc_SIW021A_Setting[] =
{

	{0x00, 0x00},
	{0x03, 0x04},
	{0x10, 0x0D},
	{0x11, 0x32},

	{0x00, 0x01},
	{0x04, 0x10},
	{0x05, 0x00},
	{0x08, 0xA2},
	{0x09, 0x60},
	{0x0A, 0x60},
	{0x10, 0x75},
	{0x11, 0x11},
	{0x18, 0x44},
	{0x20, 0x01},
	{0x21, 0x00},
	{0x22, 0x00},
	{0x23, 0x05},
	{0x24, 0x0f},
	{0x25, 0x00},
	{0x26, 0x70},
	{0x27, 0x03},
	{0x28, 0x30},
	{0x2a, 0x03},
	{0x2d, 0x01},
	{0x2e, 0x1f},
	{0x30, 0x01},
	{0x31, 0x00},
	{0x32, 0x00},
	{0x33, 0x05},
	{0x34, 0x0f},
	{0x35, 0x00},
	{0x36, 0x70},
	{0x37, 0x03},
	{0x38, 0x30},
	{0x3a, 0x03},
	{0x3d, 0x00},
	{0x3e, 0x0b},
	{0x40, 0x03},
	{0x41, 0x00},
	{0x45, 0x04},
	{0x51, 0x10},

	{0x00, 0x02},
	{0x10, 0x00},
	{0x11, 0x0f},
	{0x1b, 0x08},
	{0x30, 0x00},
	{0x31, 0x08},
	{0x32, 0x00},
	{0x34, 0xa0},
	{0x35, 0xcf},
	{0x36, 0xcf},

	{0x00, 0x03},
	{0x10, 0x00},
	{0x60, 0x80},
	{0x61, 0x80},

	{0x00, 0x04},
	{0x10, 0x40},
	{0x11, 0x0d},
	{0x12, 0x39},
	{0x14, 0xA8},
	{0x15, 0x10},
	{0x2E, 0x00},
	{0x90, 0x00},
	{0x91, 0x00},
	{0x92, 0x00},
	{0x93, 0x00},
	{0x94, 0x05},
	{0x95, 0x00},
	{0x96, 0x03},
	{0x97, 0x20},
	{0xA0, 0x3E},
	{0xA3, 0x08},
	{0x26, 0x00},
	{0x27, 0x00},
	{0x28, 0x00},
	{0x29, 0x00},

	{0x00, 0x07},
	{0xe0, 0x29},
	{0xe1, 0x08},
	{0xe2, 0x10},
	{0xe3, 0x29},
	{0xe4, 0x28},
	{0xe5, 0x10},
	{0xe6, 0x29},
	{0xe7, 0x28},
	{0xe8, 0x10},
//============ 110811_PaoCHi , Allen denoise setting ===============
	{0x00, 0x04},
	{0x10, 0xC1},
	{0x3F, 0x02},
	{0x40, 0x28},
	{0x41, 0x10},
	{0x42, 0x04},
	{0x43, 0x38},
	{0x44, 0x0c},
	{0x45, 0x24},
	{0x46, 0x60},
	{0x47, 0x10},
	{0x48, 0x0c},
	{0x4d, 0x02},
	{0x50, 0x68},
	{0x51, 0x00},
	{0x52, 0x00},
	{0x53, 0x00},
	{0x54, 0x02},
	{0x55, 0x00},
	{0x56, 0xe7},
	{0x57, 0x44},
	{0x58, 0x4a},
	{0x59, 0x18},
	{0x5a, 0x08},
	{0x5b, 0x08},
	{0x5c, 0x10},
	{0x5d, 0x20},
	{0x5e, 0x10},
	{0x5f, 0x10},
	{0x60, 0x02},
	{0x61, 0x18},
	{0x62, 0x40},
	{0x63, 0x10},
	{0x66, 0x16},
	{0x67, 0x16},
	{0x6B, 0x0f},
	{0x6C, 0x0f},
	{0x6f, 0x18},
	{0x70, 0x97},
	{0x71, 0xf8},
	{0x72, 0x0f},
	{0x73, 0x99},
	{0x74, 0xa0},
	{0x75, 0x99},
	{0x76, 0xc0},
	{0x77, 0x88},
	{0x78, 0x10},
	{0x79, 0x00},
	{0x7a, 0x08},
	{0x7B, 0x0c},
	{0x7C, 0x10},
	{0x7D, 0x10},
	{0x7E, 0x10},
	{0x7F, 0x08},
	{0x80, 0x28},
	{0x81, 0x20},
	{0x82, 0x06},
	{0x83, 0x80},
	{0x84, 0x30},
	{0x85, 0x24},
	{0x86, 0x60},
	{0x87, 0x10},
	{0x88, 0x08},
	{0x8b, 0x02},

//====================================================
	{0x00, 0x01},
	{0x03, 0x01},

};

static SIW021A_FpsSetting_t*  GetHMFpsSetting(U8 Fps, SIW021A_FpsSetting_t staHMFpsSetting[], U8 byArrayLen)
{
	U8 i;
	U8 Idx;

	if(g_bIsHighSpeed)
	{
		Idx=2; //8fps for uxga, 10 fps for vga
	}
	else
	{
		Idx = 0;
	}

	for(i=0; i< byArrayLen; i++)
	{
		if(staHMFpsSetting[i].byFps ==Fps)
		{
			Idx=i;
			break;
		}
	}
	return &staHMFpsSetting[Idx];

}
/*
static OV_FpsSetting_t*  GetOvFpsSetting(U8 Fps, OV_FpsSetting_t staOvFpsSetting[], U8 byArrayLen)
{
	U8 i;
	U8 Idx;
	ISP_MSG((" array size = %bd\n", byArrayLen));
	if(g_bIsHighSpeed)
	{
		Idx=2; //8fps for uxga, 10 fps for vga
	}
	else
	{
		Idx = 0;
	}
	for(i=0;i< byArrayLen;i++)
	{
		ISP_MSG((" fps i = %bd\n",i));
		if(staOvFpsSetting[i].byFps ==Fps)
		{
			Idx=i;
			break;
		}
	}
	ISP_MSG((" get fps idx = %bd\n",Idx));
	return &staOvFpsSetting[Idx];
}
*/

static U16 MapSnrGlbGain2SnrRegSetting(U16 wGain)
{
	return wGain * 8;
}

float SnrRegSetting2SnrGlbGain(U16 wSnrRegGain)
{
	return (float)wSnrRegGain/128.0;
}


//this function is also called during Probe stage to calculate the transfer_size/LWM/HWM
void GetSensorPclkHsync(U16 wSensorSPFormat,U8 byFps)
{
	SIW021A_FpsSetting_t *pHMFpsSetting;

	wSensorSPFormat = wSensorSPFormat; // for delete warning

	pHMFpsSetting = GetHMFpsSetting(Fps, g_staSIW021AFpsSetting, sizeof(g_staSIW021AFpsSetting)/sizeof(SIW021A_FpsSetting_t));
	g_wSensorHsyncWidth = 1315; //1361; // this for manual exposure
	g_dwPclk = pHMFpsSetting->dwPixelClk;		// this for scale speed
}

void SIW021ASetFormatFps(U16 SetFormat, U8 Fps)
{
	//XBYTE[0xfecd] = 0x03;  // 0x07(Normal Mode)
	SIW021A_FpsSetting_t *pHMFpsSetting;

	SetFormat = SetFormat;
	//CHANGE_CCS_CLK(CCS_CLK_SEL_SSC_48M|CCS_CLK_DIVIDER_1);
	CHANGE_CCS_CLK(CCS_CLK_SEL_60M|CCS_CLK_DIVIDER_1);
	pHMFpsSetting = GetHMFpsSetting(Fps, g_staSIW021AFpsSetting, sizeof(g_staSIW021AFpsSetting)/sizeof(SIW021A_FpsSetting_t));
	WriteSensorSettingWB(sizeof(gc_SIW021A_Setting)/sizeof(t_RegSettingWB), gc_SIW021A_Setting);

	//g_dwPclk= pHMFpsSetting->dwPixelClk;

	if (Fps ==10)
	{
		Write_SenReg(0x00, 0X01);
		Write_SenReg(0x09, 0X34);
		Write_SenReg(0x0a, 0X34);
	}

	// 3) update variable for AE
	//g_wSensorHsyncWidth = 1315; //1361; // this for manual exposure
	//g_dwPclk = pHMFpsSetting->dwPixelClk;		// this for scale speed
	GetSensorPclkHsync(HD800P_FRM,Fps);
	g_wAECExposureRowMax = 800;	// this for max exposure time
	g_wAEC_LineNumber = 810;	// this for AE insert dummy line algothrim

	g_wSensorWidthBefBLC = 1280;
	g_wSensorHeightBefBLC = 800;

	//InitOV9726IspParamsByResolution(g_wCurFrameWidth, g_wCurFrameHeight);

	//Write_SenReg(0x08, 22);	// change to AE page
}

/*
void HM1055SetFormatFps(U16 SetFormat, U8 Fps, U8 byFormatType)
{
	U16 wTemp;
	//U8 i;
	OV_FpsSetting_t *pHM1055_FpsSetting;
	SetFormat = SetFormat;
	byFormatType = byFormatType;
	pHM1055_FpsSetting=GetOvFpsSetting(Fps, g_staHM1055FpsSetting, sizeof(g_staHM1055FpsSetting)/sizeof(OV_FpsSetting_t));
	WriteSensorSettingWB(sizeof(gc_HM1055_Setting)/3, gc_HM1055_Setting);
	Write_SenReg(0x25, pHM1055_FpsSetting->byClkrc);
	wTemp = pHM1055_FpsSetting->wExtraDummyPixel;
      g_wSensorHsyncWidth = pHM1055_FpsSetting->wExtraDummyPixel;
	g_dwPclk = pHM1055_FpsSetting->dwPixelClk;		// this for scale speed
	if(Fps ==30)
	{
      	g_wAEC_LineNumber = 731;	// this for AE insert dummy line algothrim  (scope measured) //非重點
	g_wAECExposureRowMax = 748;	// this for max exposure timev            (scope measured)  //這才是重點。30fps時允許的曝光最多hsync列數(包含Vsync的 High(用量的)和 Low duty(用算的))
	}
	if(Fps ==15)
	{
	Write_SenReg(0x10,0x01);
	Write_SenReg(0x11,0x02);
	Write_SenReg(0x0100,0x01);
	g_wAEC_LineNumber = 731;
	g_wAECExposureRowMax = 996;  //when 15fps, low duty increase, so guess that exposureRowMax will still increase with the time ratio.
	}
	if(Fps ==25)
	{
	Write_SenReg(0x10,0x00);
	Write_SenReg(0x11,0x9a);
	Write_SenReg(0x0100,0x01);
	g_wAEC_LineNumber = 731;
	g_wAECExposureRowMax = 896;  //when 15fps, low duty increase, so guess that exposureRowMax will still increase with the time ratio.
	}
	if(Fps ==20)
	{
	g_wAEC_LineNumber = 731;
	g_wAECExposureRowMax = 748;  //when 15fps, low duty increase, so guess that exposureRowMax will still increase with the time ratio.
	}
	if(Fps ==10)
	{
	Write_SenReg(0x10,0x00);
	Write_SenReg(0x11,0x02);
	Write_SenReg(0x0100,0x01);
	g_wAEC_LineNumber = 731;
	g_wAECExposureRowMax = 748;  //when 15fps, low duty increase, so guess that exposureRowMax will still increase with the time ratio.
	}
	g_wSensorWidthBefBLC = 1280;
	g_wSensorHeightBefBLC = 720;
	SetBkWindowStart(1, 0);
	DBG_SENSOR(("Fps=%bu\n",Fps));
	#if 0
	{
		for (i=0;i<6;i++)
		{
			g_awLensShadingCenter[i] = gc_wLSC_Center_Offset_HM1055[i];
		}
	}
	#endif

	return;
}
*/

void SetSIW021AGain(float fGain)
{
}

void CfgSIW021AControlAttr(void)
{
	U8 i;
	g_bySensorSize = SENSOR_SIZE_HD800P; //SENSOR_SIZE_HD720P;
	g_wSensorSPFormat = HD800P_FRM;
	{
		memcpy(g_asOvCTT, gc_SIW021A_CTT,sizeof(gc_SIW021A_CTT));
	}
	{
#ifdef SP_HM1055_IO18
		XBYTE[REG_TUNES18_28] = SV28_VOL_1V8|SV18_VOL_2V9;	// SV18 and SV28 voltage control register
#else
#if (_CHIP_ID_ & _RTS5840_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_3V04;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V70;
#elif (_CHIP_ID_ & _RTS5832_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_3V00;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V70;
#elif (_CHIP_ID_ & _RTS5829B_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_3V00;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V71;
#endif
#endif
	}

	{
		g_aVideoFormat[0].byaVideoFrameTbl[0] = 7;
		g_aVideoFormat[0].byaVideoFrameTbl[1]=F_SEL_640_480;
		g_aVideoFormat[0].byaVideoFrameTbl[2]=F_SEL_160_120;
		g_aVideoFormat[0].byaVideoFrameTbl[3]=F_SEL_176_144;
		g_aVideoFormat[0].byaVideoFrameTbl[4]=F_SEL_352_288;
		g_aVideoFormat[0].byaVideoFrameTbl[5]=F_SEL_320_240;
		g_aVideoFormat[0].byaVideoFrameTbl[6]=F_SEL_1280_720;
		g_aVideoFormat[0].byaVideoFrameTbl[7]=F_SEL_1280_800;
		//g_aVideoFormat[0].byaVideoFrameTbl[6]=F_SEL_800_600;
		//g_aVideoFormat[0].byaVideoFrameTbl[7]=F_SEL_960_720;
		//g_aVideoFormat[0].byaVideoFrameTbl[6]=F_SEL_1280_720;
		g_aVideoFormat[0].byaStillFrameTbl[0] = 5;
		g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_1280_720;
		g_aVideoFormat[0].byaStillFrameTbl[2]=F_SEL_160_120;
		g_aVideoFormat[0].byaStillFrameTbl[3]=F_SEL_320_240;
		g_aVideoFormat[0].byaStillFrameTbl[4]=F_SEL_640_480;
		g_aVideoFormat[0].byaStillFrameTbl[5]=F_SEL_1280_720;

		for(i=0; i<7; i++)
		{
			g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_30; //FPS_15|FPS_30;
		}
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_800_600] =FPS_10;//|FPS_10;//FPS_9|FPS_8|FPS_5|FPS_1;                             //bit10 F_1280_720
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_960_720] =FPS_10;//|FPS_10;//FPS_9|FPS_8|FPS_5|FPS_1;                             //bit10 F_1280_720
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_1280_720] =FPS_10;//|FPS_10;//FPS_9|FPS_8|FPS_5|FPS_1;                             //bit10 F_1280_720
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_1280_800] =FPS_10;//|FPS_10;//FPS_9|FPS_8|FPS_5|FPS_1;                             //bit10 F_1280_720
		g_aVideoFormat[0].byFormatType = FORMAT_TYPE_YUY2;
#ifdef _ENABLE_MJPEG_
		memcpy(&(g_aVideoFormat[1]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));
		for(i=7; i<12; i++) //bit0-6 VGA and smaller size, 30fps max
		{
			g_aVideoFormat[1].waFrameFpsBitmap[i] =FPS_30; //FPS_15|FPS_30;//|FPS_1|FPS_3|FPS_5|FPS_10;
		}
		g_aVideoFormat[1].byFormatType = FORMAT_TYPE_MJPG;
#endif
#ifdef _ENABLE_M420_FMT_
		memcpy(&(g_aVideoFormat[2]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));
		g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_720] =FPS_11|FPS_5;
		g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_800] =FPS_11|FPS_5;
		g_aVideoFormat[2].byFormatType = FORMAT_TYPE_M420;
#endif
	}
	{
	}
	InitSIW021AIspParams();
}
void SetSIW021AIntegrationTime(U16 wEspline)
{
	U16 wFrameLenLines;
	U16 wExpMax = g_wAECExposureRowMax-2;
	if(wEspline > wExpMax)
	{
		wFrameLenLines = wEspline + 20;
	}
	else
	{
		wFrameLenLines = g_wAEC_LineNumber;
	}
	if (wFrameLenLines%2==1)
	{
		wFrameLenLines++;
	}
	Write_SenReg(0x00, 02);	// change to AE page
	Write_SenReg(0x31, INT2CHAR(wEspline, 0));	// change Coarse integration time low
	Write_SenReg(0x30, INT2CHAR(wEspline, 1));	// change Coarse integration time high

}
void SetSIW021AImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.
{
	bySnrImgDir =bySnrImgDir;
	bySnrImgDir ^= 0x01; 	// HM1055 output mirrored image at default, so need mirror the image for normal output
	if(bySnrImgDir & 0x01)
	{
		SetBkWindowStart(1, 0);
	}
	else
	{
		SetBkWindowStart(0, 0);
	}
	XBYTE[0xfecd] = 0x03; //Neil Change the PCLK latch
}


void WaitFrameStart(void)
{
	if(g_byStartVideo)
	{
		XBYTE[0x8007/*ISP_INT_FLAG0*/] = 0x40/*ISP_FRAMESTART_INT*/;	// clear ISP Statictis interrupt
		WaitTimeOut(0x8007/*ISP_INT_FLAG0*/,0x40/* ISP_FRAMESTART_INT*/, 1, 30);
	}
}


void SetSIW021AExposuretime_Gain(float fExpTime, float fTotalGain)
{
	U16 wExposureRows_floor;
	U16  wGainRegSetting; //data
	float fSnrGlbGain;
	float fExposureRows;
	U16 wSetDummy;
	float fTemp;
	U16 wExposurePixels;
	wGainRegSetting=MapSnrGlbGain2SnrRegSetting((U16)(fTotalGain*16.0));
	fSnrGlbGain = SnrRegSetting2SnrGlbGain(wGainRegSetting);
	fExposureRows = fExpTime/g_fSensorRowTimes;	//unit : 1row  //____即((float)g_wSensorHsyncWidth*(float)10000)/(float)g_dwPclk;     // unit microsecond
	wExposureRows_floor = (U16)(fExposureRows);
	fTemp = fExpTime-((float)wExposureRows_floor*(g_fSensorRowTimes)); //fine integration time;unit:  us
	wExposurePixels =(U16)(fTemp * (float)(g_dwPclk/10000)) ; //unit: pixels
	if(fabs(g_fCurExpTime-fExpTime) > 0.0001)
	{
		wSetDummy = g_wAFRInsertDummylines + g_wAEC_LineNumber;

		if (wSetDummy%2 == 1)
		{
			wSetDummy++;
		}

		Write_SenReg(0x00, 0x02);
		Write_SenReg(0x31, INT2CHAR(wExposureRows_floor, 0));	// change exposure value low
		Write_SenReg(0x30, INT2CHAR(wExposureRows_floor, 1));	// change exposure value high
		//Write_SenReg(0x32, Digital_fractional_sensorGain);
		//WaitFrameStart();
		//WaitFrameStart();
		WaitFrameSync(ISP_INT1, ISP_DATA_START_INT);
		Write_SenReg(0x34, INT2CHAR(wGainRegSetting, 0));	// change exposure value low
		Write_SenReg(0x33, INT2CHAR(wGainRegSetting, 1));	// change exposure value high

		SetISPAEGain(fTotalGain, fSnrGlbGain, 1);
		
		g_fCurExpTime = fExpTime;	// back up exposure rows
	}
	else
	{

		Write_SenReg(0x34, INT2CHAR(wGainRegSetting, 0));	// change exposure value low
		Write_SenReg(0x33, INT2CHAR(wGainRegSetting, 1));	// change exposure value high

		SetISPAEGain(fTotalGain, fSnrGlbGain, 1);
	}

	return;
}
/*
void SIW021A_POR()
{
}
*/
void InitSIW021AIspParams()
{
	// AWB initial gain
	//use D50 R/G/B gain as AWB initial value
	g_wAWBRGain_Last= 0x150;
	g_wAWBGGain_Last= 0x108;
	g_wAWBBGain_Last= 0x1A0;
	g_wProjectGR = g_wAWBRGain_Last;
	g_wProjectGB = g_wAWBBGain_Last;

	g_bySaturation_Def = 64;
	g_byContrast_Def =32;  //32 Contrast default
	//g_wDynamicISPEn = DYNAMIC_DarkMode_CCM_EN|DYNAMIC_SHARPPARAM_EN|DYNAMIC_GAMMA_EN;//|DYNAMIC_CCM_CT_EN;
	g_wDynamicISPEn = DYNAMIC_LSC_EN|DYNAMIC_SHARPPARAM_EN|DYNAMIC_CCM_CT_EN|DYNAMIC_SHARPNESS_EN;

	//U8 i;

}
void SetSIW021ADynamicISP(U8 byAEC_Gain)
{
////_______________Dark mode
	if  ( (g_wDynamicISPEn&DYNAMIC_DarkMode_CCM_EN) == DYNAMIC_DarkMode_CCM_EN)
	{
		if(CCM_frame_count == 5)
		{
			CCM_frame_count = 0;
			if((byAEC_Gain ==g_wAECGlobalgainmax)&&(g_byAEMeanValue <= 20 ) &&(XBYTE[0x8011]==0x05))	//only for HD720
			{
				if(g_byCCM2State != 0x03)
				{
					XBYTE[0x8361] = 0; //0x8361
					XBYTE[0x8369] = 0; //0x8369
					XBYTE[0x8367] = 0; //0x8367
					XBYTE[0x8375] = 0x01;
					g_byCCM2State = 0x03;  //0x03 = Dark_CCM, but here, use define will result error.
				}
			}
			else// if(wColorTempature > 2900)
			{
				if(g_byCCM2State == 0x03) //only do once, for DarkMode come back condition.
				{
					XBYTE[0x8361] = 1;
					XBYTE[0x8369] = 1;
					XBYTE[0x8367] = 1;
					XBYTE[0x8375] = 0x01;
					g_byCCM2State = 0x00;  //0x00 = D65_CCM,
				}
			}
		}
		else
		{
			CCM_frame_count++;
		}
	}
///_________________________

}
void SetSIW021ADynamicISP_AWB(U16 wColorTempature)
{
	wColorTempature =wColorTempature;


}
#endif
