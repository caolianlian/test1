#ifndef _IMX119PQH5_IQ_H_
#define _IMX119PQH5_IQ_H_

IQTABLE_t code ct_IQ_Table=
{
	// IQ Header
	{
		IQ_TABLE_AP_VERSION,	// AP version
		sizeof(IQTABLE_t)+8,
		0x00,	// IQ version High
		0x00,	// IQ version Low
		0xFF,
		0xFF,
		0xFF
	},

	// BLC
	{
		// normal BLC:  offset_R,offsetG1,offsetG2,offsetB
		{64,64,64,64},
		//{16,16,16,16},
		//{0,0,0,0},
		// Low lux BLC:  offset_R,offsetG1,offsetG2,offsetB
		{64,64,64,64},
		//{16,16,16,16},
		//{0,0,0,0},
	},

	// LSC
	{
		// circle LSC
		{
			// circle LSC curve
			{
				//:3500k
				/*	{128, 0, 128, 0, 129, 0, 132, 0, 136, 0, 141, 0, 148, 0, 156, 0, 165, 0, 176, 0, 186, 0, 196, 0, 203, 0, 203, 0, 203, 0, 203, 0, 203, 0, 203, 0, 203, 0, 203, 0, 203, 0, 203, 0, 203, 0, 203, 0, },
					{128, 0, 128, 0, 130, 0, 133, 0, 137, 0, 143, 0, 150, 0, 158, 0, 168, 0, 178, 0, 189, 0, 201, 0, 211, 0, 211, 0, 211, 0, 211, 0, 211, 0, 211, 0, 211, 0, 211, 0, 211, 0, 211, 0, 211, 0, 211, 0, },
					{128, 0, 128, 0, 129, 0, 132, 0, 135, 0, 139, 0, 144, 0, 150, 0, 156, 0, 163, 0, 170, 0, 178, 0, 183, 0, 183, 0, 183, 0, 183, 0, 183, 0, 183, 0, 183, 0, 183, 0, 183, 0, 183, 0, 183, 0, 183, 0, },
				*/
				//d65
				/*	{128, 0, 129, 0, 132, 0, 137, 0, 146, 0, 157, 0, 171, 0, 190, 0, 212, 0, 240, 0, 9, 1, 38, 1, 63, 1, 63, 1, 63, 1, 63, 1, 63, 1, 63, 1, 63, 1, 63, 1, 63, 1, 63, 1, 63, 1, 63, 1, },
					{128, 0, 128, 0, 131, 0, 136, 0, 142, 0, 151, 0, 162, 0, 177, 0, 194, 0, 215, 0, 235, 0, 4, 1, 30, 1, 30, 1, 30, 1, 30, 1, 30, 1, 30, 1, 30, 1, 30, 1, 30, 1, 30, 1, 30, 1, 30, 1, },
					{128, 0, 129, 0, 131, 0, 136, 0, 142, 0, 151, 0, 162, 0, 176, 0, 194, 0, 214, 0, 234, 0, 3, 1, 24, 1, 24, 1, 24, 1, 24, 1, 24, 1, 24, 1, 24, 1, 24, 1, 24, 1, 24, 1, 24, 1, 24, 1, },
				*/
				//d50
				/*		{128, 0, 128, 0, 132, 0, 137, 0, 141, 0, 148, 0, 158, 0, 177, 0, 195, 0, 207, 0, 235, 0, 5, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, },
						{128, 0, 128, 0, 131, 0, 136, 0, 141, 0, 148, 0, 158, 0, 177, 0, 194, 0, 207, 0, 236, 0, 5, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, },
						{128, 0, 128, 0, 131, 0, 135, 0, 141, 0, 148, 0, 158, 0, 175, 0, 192, 0, 207, 0, 233, 0, 3, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, },
				*/
				//d50 60%
				/*		{128, 0, 128, 0, 130, 0, 133, 0, 137, 0, 142, 0, 150, 0, 158, 0, 168, 0, 181, 0, 193, 0, 215, 0, 231, 0, 231, 0, 231, 0, 231, 0, 231, 0, 231, 0, 231, 0, 231, 0, 231, 0, 231, 0, 231, 0, 231, 0,},
						{128, 0, 128, 0, 130, 0, 132, 0, 136, 0, 142, 0, 149, 0, 157, 0, 168, 0, 180, 0, 193, 0, 209, 0, 225, 0, 225, 0, 225, 0, 225, 0, 225, 0, 225, 0, 225, 0, 225, 0, 225, 0, 225, 0, 225, 0, 225, 0, },
						{128, 0, 128, 0, 129, 0, 132, 0, 136, 0, 141, 0, 148, 0, 156, 0, 166, 0, 178, 0, 190, 0, 205, 0, 224, 0, 224, 0, 224, 0, 224, 0, 224, 0, 224, 0, 224, 0, 224, 0, 224, 0, 224, 0, 224, 0, 224, 0, },
				*/
				/*
						//1012 Leo++ 80%
						{128,   0, 129,   0, 134,   0, 144,   0, 157,   0, 178,   0, 206,   0, 242,   0,  31,   1,  86,   1, 144,   1, 201,   1,  22,   2,  22,   2,  22,   2,  22,   2,  22,   2,  22,   2,  22,   2,  22,   2,  22,   2,  22,   2,  22,   2, },
						{128,   0, 131,   0, 136,   0, 146,   0, 159,   0, 178,   0, 203,   0, 234,   0,  16,   1,  58,   1, 104,   1, 147,   1, 192,   1, 192,   1, 192,   1, 192,   1, 192,   1, 192,   1, 192,   1, 192,   1, 192,   1, 192,   1, 192,   1, },
						{128,   0, 129,   0, 133,   0, 140,   0, 152,   0, 168,   0, 190,   0, 216,   0, 250,   0,  33,   1,  74,   1, 110,   1, 159,   1, 159,   1, 159,   1, 159,   1, 159,   1, 159,   1, 159,   1, 159,   1, 159,   1, 159,   1, 159,   1, },
				*/
				/*
						//1021 Leo++ 70%
						{128,   0, 130,   0, 134,   0, 141,   0, 153,   0, 170,   0, 194,   0, 224,   0,   6,   1,  51,   1, 100,   1, 148,   1, 213,   1, 213,   1, 213,   1, 213,   1, 213,   1, 213,   1, 213,   1, 213,   1, 213,   1, 213,   1, 213,   1, },
						{128,   0, 130,   0, 134,   0, 142,   0, 153,   0, 169,   0, 190,   0, 215,   0, 246,   0,  25,   1,  62,   1,  98,   1, 136,   1, 136,   1, 136,   1, 136,   1, 136,   1, 136,   1, 136,   1, 136,   1, 136,   1, 136,   1, 136,   1, },
						{128,   0, 130,   0, 133,   0, 139,   0, 149,   0, 162,   0, 181,   0, 203,   0, 232,   0,   8,   1,  43,   1,  73,   1, 109,   1, 109,   1, 109,   1, 109,   1, 109,   1, 109,   1, 109,   1, 109,   1, 109,   1, 109,   1, 109,   1, },
				*/
				/*
				//1021 Leo++ 60%
				{128,   0, 129,   0, 132,   0, 138,   0, 148,   0, 161,   0, 180,   0, 204,   0, 234,   0,  14,   1,  53,   1,  91,   1, 142,   1, 142,   1, 142,   1, 142,   1, 142,   1, 142,   1, 142,   1, 142,   1, 142,   1, 142,   1, 142,   1, },
				{128,   0, 129,   0, 133,   0, 139,   0, 148,   0, 160,   0, 177,   0, 197,   0, 222,   0, 249,   0,  23,   1,  51,   1,  81,   1,  81,   1,  81,   1,  81,   1,  81,   1,  81,   1,  81,   1,  81,   1,  81,   1,  81,   1,  81,   1, },
				{128,   0, 129,   0, 132,   0, 136,   0, 144,   0, 155,   0, 170,   0, 187,   0, 210,   0, 235,   0,   7,   1,  31,   1,  59,   1,  59,   1,  59,   1,  59,   1,  59,   1,  59,   1,  59,   1,  59,   1,  59,   1,  59,   1,  59,   1, },
				*/
				/*
						//1021 Leo++ 50%
						{128,   0, 129,   0, 131,   0, 136,   0, 142,   0, 152,   0, 166,   0, 184,   0, 206,   0, 232,   0,   5,   1,  33,   1,  71,   1,  71,   1,  71,   1,  71,   1,  71,   1,  71,   1,  71,   1,  71,   1,  71,   1,  71,   1,  71,   1, },
						{128,   0, 129,   0, 131,   0, 136,   0, 142,   0, 152,   0, 164,   0, 179,   0, 197,   0, 217,   0, 239,   0,   4,   1,  26,   1,  26,   1,  26,   1,  26,   1,  26,   1,  26,   1,  26,   1,  26,   1,  26,   1,  26,   1,  26,   1, },
						{128,   0, 129,   0, 131,   0, 134,   0, 140,   0, 148,   0, 159,   0, 172,   0, 188,   0, 207,   0, 228,   0, 245,   0,  10,   1,  10,   1,  10,   1,  10,   1,  10,   1,  10,   1,  10,   1,  10,   1,  10,   1,  10,   1,  10,   1, },
				*/
				{128, 0, 129, 0, 133, 0, 141, 0, 154, 0, 174, 0, 200, 0, 234, 0, 19, 1, 68, 1, 121, 1, 181, 1, 254, 1, 95, 2, 144, 2, 144, 2, 144, 2, 144, 2, 144, 2, 144, 2, 144, 2, 144, 2, 144, 2, 144, 2, },
				{128, 0, 128, 0, 133, 0, 141, 0, 153, 0, 171, 0, 194, 0, 225, 0, 6, 1, 48, 1, 94, 1, 148, 1, 215, 1, 39, 2, 39, 2, 39, 2, 39, 2, 39, 2, 39, 2, 39, 2, 39, 2, 39, 2, 39, 2, 39, 2, },
				{128, 0, 129, 0, 132, 0, 139, 0, 151, 0, 167, 0, 188, 0, 217, 0, 252, 0, 35, 1, 81, 1, 133, 1, 194, 1, 254, 1, 254, 1, 254, 1, 254, 1, 254, 1, 254, 1, 254, 1, 254, 1, 254, 1, 254, 1, 254, 1, },
			},
			// circle LSC center: R Center Hortizontal, R Center Vertical, G Center Hortizontal, G Center Vertical,B Center Hortizontal, B Center Vertical
			//	{672 ,402,672 ,402,672 ,402}, //For3500k
			//{618, 557,618, 557,618, 557}, //For5000k
			//{686, 408,686, 408,686, 408},//d65
			{659,514,660,520,640,512},//0530 4300K lenov office light
		},

		// micro LSC
		{
			// micro LSC grid mode
			2,
			// micro LSC matrix
			{0},
		},

		// dynamic LSC
		{
			40,   // Dynamic LSC Gain Threshold Low
			70,  // Dynamic LSC Gain Threshold High
			0x1f,	// Dynamic LSC Adjust rate at Gain Threshold Low
			0x12,	// Dynamic LSC Adjust rate at Gain Threshold High
			//{33,35,38,54,56,34},	// rough r gain before LSC, from A, U30, CWF, D50, D65, D75
			//{78,70,61,44,38,72},	// rough b gain before LSC
			{41,42,48,51,60,62},	// g_aAWBRoughGain_R
			{72,58,57,50,43,37},	// g_aAWBRoughGain_B
			50,		// start threshold of dynamic LSC by CT, white pixel millesimal
			25,		// end threshold of dynamic LSC by CT, white pixel millesimal
			100,		// LSC switch color temperature threshold buffer
			{3100,3800,4700,5800,7000},	// LSC switch color temperature threshold
			{
				{0x28,0x20,0x1f,},//a=2850k
				{0x24,0x20,0x1f,},//3500k
				{0x20,0x20,0x20,},//cwf=4150k
				{0x20,0x20,0x20,},//d50=5000k
				{0x20,0x20,0x26,},//d65=6500k
				{0x20,0x20,0x20,},//d75=6500k
			},
		},
	},

	// CCM
	{

		// D65 light CCM
		//	{121, 1,  115, 8,  6, 8,  89, 8,  153, 1,  64, 8,  32, 8,  166, 8,  198, 1,  },//25%
		//{128, 1,  115, 8,  12, 8,  89, 8,  179, 1,  89, 8,  25, 8,  166, 8,  192, 1,  },
		//	{64,1,51,8,11,8,59,8,132,1,72,8,7,8,92,8,101,1, 0,0,0},//111014_PaoChi
		//    {90,1,77,8,11,8,71,8,150,1,78,8,65,8,40,8,108,1, 0,0,0},//111014_PaoChi
		//    {103,1,83,8,17,8,71,8,150,1,78,8,65,8,40,8,108,1, 0,0,0},//111014_PaoChi
		//	{110,1,104,8,7,8,77,8,147,1,70,8,13,8,118,8,131,1, 0,0,0},//111018_Leo
		{0x1c2,-0x109,72,-0x6f,0x1c2,-82,-21,-167,0x1b9},//0531
		{221,30,6,16,234,6,16,30,210},//0526_CCM5_de_80%
		{0x142,-60,-5,-13,0x138,-44,35,-8,230},//0530


		//	{0, 1,  0, 0,  0, 0, 0, 0,  0, 1,  0, 0,  0, 0,  0, 0,  0, 1, 0,0,0 },
		160,	// dynamic CCM Gain Threshold Low
		240,	// dynamic CCM Gain Threshold High
		3000,	// dynamic CCM A light Color Temperature Switch Threshold
		3600	// dynamic CCM D65 light Color Temperature Switch Threshold
	},

	// Gamma
	{
		// normal light gamma
		//  {0, 8, 15, 23, 31, 41, 50, 59, 70, 85, 100, 116, 130, 142, 152, 161, 169, 176, 182, 188, 192, 202, 210, 218, 225, 232, 240, 248, },
		//{0, 6, 13, 20, 27, 34, 42, 49, 58, 77, 96, 115, 131, 144, 153, 162, 169, 175, 181, 186, 191, 200, 209, 217, 225, 233, 241, 248, },
		//{0, 6, 12, 18, 24, 30, 36, 43, 51, 69, 87, 107, 126, 141, 154, 165, 174, 182, 188, 194, 199, 208, 216, 222, 227, 234, 241, 248, },
		//	{0, 4, 8, 13, 17, 21, 27, 35, 44, 56, 71, 84, 97, 111, 124, 135, 144, 152, 161, 169, 177, 191, 202, 213, 222, 231, 240, 248, },
		//	{0, 8, 15, 23, 30, 37, 45, 52, 60, 74, 87, 99, 112, 124, 133, 144, 153, 162, 169, 177, 183, 195, 205, 214, 223, 231, 239, 247},
		//	{0, 5, 13, 22, 32, 42, 52, 62, 71, 87, 102, 116, 129, 141, 152, 161, 169, 176, 182, 187, 193, 202, 210, 218, 225, 232, 240, 248},//111013
		//	{0, 6, 13, 20, 27, 34, 42, 50, 58, 77, 96, 115, 131, 144, 153, 162, 169, 175, 181, 186, 191, 200, 209, 217, 225, 233, 241, 248},//111019
		//	{0, 7, 15, 23, 31, 39, 47, 55, 63, 79, 96, 115, 131, 144, 153, 162, 169, 175, 181, 186, 191, 200, 209, 217, 225, 233, 241, 248},//111020_orig
		{0, 9, 18, 27, 35, 44, 52, 60, 67, 82, 95, 107, 118, 129, 138, 148, 158, 166, 174, 182, 189, 202, 213, 222, 230, 238, 244, 250},
		//_______________________

		// low light gamma
		{0, 6, 13, 20, 27, 34, 42, 49, 58, 77, 96, 115, 131, 144, 153, 162, 169, 175, 181, 186, 191, 200, 209, 217, 225, 233, 241, 248, },
		80,	// dynamic Gamma Gain Threshold Low
		144	// dynamic Gamma Gain Threshold High
	},

	// AE
	{
		// AE target
		{
			40,// Histogram Ratio Low
			40,// Histogram Ratio High
			60,// YMean Target Low
			66,// YMean Target
			80,// YMean Target High
			10,// Histogram Position Low
			190,// Histogram Position High
			3// Dynamic AE Target decrease value
		},
		// AE limit
		{
			13, // AE step Max value at 50Hz power line frequency
			16, // AE step Max value at 60Hz power line frequency
			255,	// AE global gain Max value
			//92, // AE global gain Max value
			64, // AE continous frame rate gain threshold
			160, // AE discrete frame rate 15fps gain threshold	 when 15fps condition, and Gain over this value, it will change to 30fps.
			128, // AE discrete frame rate 30fps gain threshold	 ,when 30fps condition and Gain over this value, it will change to 15 fps.
			//120	// AE HighLight mode threshold
			65	// AE HighLight mode threshold
		},

		// AE weight
		{
			{
				/*1,1,1,1,1,
				1,3,3,3,1,
				1,6,6,6,1,
				2,5,5,5,2,
				3,4,4,4,3,*/
				1,1,1,1,1,
				1,2,3,2,1,
				1,2,8,2,1,
				1,2,8,2,1,
				1,2,5,2,1,
			}
		},
		// AE sensitivity
		{
			0.05,// g_fAEC_Adjust_Th
			//16,	// AE Latency time
			6,	// AE Latency time
			//20,	// Ymean diff threshold for judge AE same block
			8,	// hemonel 2011-07-14: modify Neil adjust parameter from 20 to 8
			22	// same block count threshold for judge AE scene variation
		},
	},


	// AWB
	{
		{
			//A U30 CWF D50 D65 D75
			{41,42,48,51,60,62},	// g_aAWBRoughGain_R
			{72,58,57,50,43,37},	// g_aAWBRoughGain_B
			13, // K1
			110,	// B1
			80,//95,	// B2
			16, // sK3
			17, // sB3
			16,	// sK4
			-75,	// sB4
			6, // sK5
			60, // sB5
			0,	// sK6
			10, // sB6
			118,	// B_up
			75,//85,	// B_down
			30, // sB_left
			-85,	// sB_right
			230,	// Ymean range upper limit
			5,	// Ymean range low limit
			10,	// RGB sum threshold for AWB hold
		},
		// AWB advanced
		{
			5,	// white point ratio threshold
			38, // g_byAWBFineMax_RG
			26, // g_byAWBFineMin_RG
			38, // g_byAWBFineMax_BG
			26, // g_byAWBFineMin_BG
			225,	// g_byAWBFineMax_Bright
			20, // g_byAWBFineMin_Bright
			30	// g_byFtGainTh
		},
		// AWB sensitivity
		{
			6, // g_wAWBGainDiffTh
			1, // g_byAWBGainStep
			5, // g_byAWBFixed_YmeanTh
			10,	// g_byAWBColorDiff_Th
			8	// g_byAWBDiffWindowsTh
		}
	},

	// Texture
	{
		// sharpness
		{
			0x60,	// for CIF
			0x60,	// for VGA
			0x60,	// for HD
			0x20,	// for low lux
			64,		// dynamic sharpness gain threshold low
			150		// dynamic sharpness gain threshold high
		},
		// sharpness & denoise paramter
		{
			// CIF
			{{
					0x06,  	//ISP_NR_EDGE_THD,
					0x0a,	//ISP_NR_MM_THD1
					0x01, 	//ISP_NR_MODE0_LPF,
					0x02, 	//ISP_NR_MODE1_LPF,
					0x57,	//ISP_NR_MODE,
					0x01,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x02,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x02,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x81,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x08,	//ISP_INTP_EDGE_THD1,
					0x15,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x07,	//ISP_RGB_IIR_CTRL
					0x60,	//ISP_RGB_VIIR_COEF
					0X66,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x0d,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x01, 	//ISP_RGB_DIIR_COFF_CUT
					0x06,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG


				},
				{
					0x0c,  	//ISP_NR_EDGE_THD,
					0x06,	//ISP_NR_MM_THD1
					0x01, 	//ISP_NR_MODE0_LPF,
					0x02, 	//ISP_NR_MODE1_LPF,
					0x7f,	//ISP_NR_MODE,
					0x01,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x02,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x02,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x81,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x08,	//ISP_INTP_EDGE_THD1,
					0x15,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x07,	//ISP_RGB_IIR_CTRL
					0x60,	//ISP_RGB_VIIR_COEF
					0x88,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x0e,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x01, 	//ISP_RGB_DIIR_COFF_CUT
					0x0a,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG

				},
				{
					0x10,  	//ISP_NR_EDGE_THD,
					0x02,	//ISP_NR_MM_THD1
					0x00, 	//ISP_NR_MODE0_LPF,
					0x01, 	//ISP_NR_MODE1_LPF,
					0xff,	//ISP_NR_MODE,
					0x01,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x03,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x03,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x81,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x0a,	//ISP_INTP_EDGE_THD1,
					0x15,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x07,	//ISP_RGB_IIR_CTRL
					0x80,	//ISP_RGB_VIIR_COEF
					0xaa,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x0e,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x02, 	//ISP_RGB_DIIR_COFF_CUT
					0x0b,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG


				},
				{
					0x10,  	//ISP_NR_EDGE_THD,
					0x00,	//ISP_NR_MM_THD1
					0x00, 	//ISP_NR_MODE0_LPF,
					0x01, 	//ISP_NR_MODE1_LPF,
					0xff,	//ISP_NR_MODE,
					0x01,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x03,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x03,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x81,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x0c,	//ISP_INTP_EDGE_THD1,
					0x15,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x07,	//ISP_RGB_IIR_CTRL
					0xa0,	//ISP_RGB_VIIR_COEF
					0xcc,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x0e,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x03, 	//ISP_RGB_DIIR_COFF_CUT
					0x0c,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG


				}
			},
			// VGA resolution
			{{
					0x06,  	//ISP_NR_EDGE_THD,
					0x0a,	//ISP_NR_MM_THD1
					0x01, 	//ISP_NR_MODE0_LPF,
					0x02, 	//ISP_NR_MODE1_LPF,
					0x57,	//ISP_NR_MODE,
					0x01,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x02,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x02,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x81,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x08,	//ISP_INTP_EDGE_THD1,
					0x15,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x07,	//ISP_RGB_IIR_CTRL
					0x60,	//ISP_RGB_VIIR_COEF
					0X66,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x0d,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x01, 	//ISP_RGB_DIIR_COFF_CUT
					0x06,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG

				},
				{
					0x0c,  	//ISP_NR_EDGE_THD,
					0x06,	//ISP_NR_MM_THD1
					0x01, 	//ISP_NR_MODE0_LPF,
					0x02, 	//ISP_NR_MODE1_LPF,
					0x7f,	//ISP_NR_MODE,
					0x01,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x02,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x02,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x81,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x08,	//ISP_INTP_EDGE_THD1,
					0x15,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x07,	//ISP_RGB_IIR_CTRL
					0x60,	//ISP_RGB_VIIR_COEF
					0x88,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x0e,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x01, 	//ISP_RGB_DIIR_COFF_CUT
					0x0a,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG


				},
				{
					0x10,  	//ISP_NR_EDGE_THD,
					0x02,	//ISP_NR_MM_THD1
					0x00, 	//ISP_NR_MODE0_LPF,
					0x01, 	//ISP_NR_MODE1_LPF,
					0xff,	//ISP_NR_MODE,
					0x01,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x03,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x03,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x81,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x0a,	//ISP_INTP_EDGE_THD1,
					0x15,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x07,	//ISP_RGB_IIR_CTRL
					0x80,	//ISP_RGB_VIIR_COEF
					0xaa,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x0e,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x02, 	//ISP_RGB_DIIR_COFF_CUT
					0x0b,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG

				},
				{
					0x10,  	//ISP_NR_EDGE_THD,
					0x00,	//ISP_NR_MM_THD1
					0x00, 	//ISP_NR_MODE0_LPF,
					0x01, 	//ISP_NR_MODE1_LPF,
					0xff,	//ISP_NR_MODE,
					0x01,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x03,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x03,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x81,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x0c,	//ISP_INTP_EDGE_THD1,
					0x15,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x07,	//ISP_RGB_IIR_CTRL
					0xa0,	//ISP_RGB_VIIR_COEF
					0xcc,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x0e,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x03, 	//ISP_RGB_DIIR_COFF_CUT
					0x0c,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG

				}
			},
			// HD resolution
			//[Albert, 2011/06/16]+++
			{{
					0x06,  	//ISP_NR_EDGE_THD,
					0x0a,	//ISP_NR_MM_THD1
					0x01, 	//ISP_NR_MODE0_LPF,
					0x02, 	//ISP_NR_MODE1_LPF,
					0x57,	//ISP_NR_MODE,
					0x01,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x02,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x02,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x82,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x08,	//ISP_INTP_EDGE_THD1,
					0x15,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x07,	//ISP_RGB_IIR_CTRL
					0x60,	//ISP_RGB_VIIR_COEF
					0X66,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x0d,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x01, 	//ISP_RGB_DIIR_COFF_CUT
					0x06,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG
				},
				{
					0x0c,  	//ISP_NR_EDGE_THD,
					0x06,	//ISP_NR_MM_THD1
					0x01, 	//ISP_NR_MODE0_LPF,
					0x02, 	//ISP_NR_MODE1_LPF,
					0x7f,	//ISP_NR_MODE,
					0x01,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x02,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x02,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x82,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x08,	//ISP_INTP_EDGE_THD1,
					0x15,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x07,	//ISP_RGB_IIR_CTRL
					0x60,	//ISP_RGB_VIIR_COEF
					0x88,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x0e,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x01, 	//ISP_RGB_DIIR_COFF_CUT
					0x0a,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG

				},
				{
					0x10,  	//ISP_NR_EDGE_THD,
					0x02,	//ISP_NR_MM_THD1
					0x00, 	//ISP_NR_MODE0_LPF,
					0x01, 	//ISP_NR_MODE1_LPF,
					0xff,	//ISP_NR_MODE,
					0x01,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x03,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x03,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x82,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x0a,	//ISP_INTP_EDGE_THD1,
					0x15,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x07,	//ISP_RGB_IIR_CTRL
					0x80,	//ISP_RGB_VIIR_COEF
					0xaa,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x0e,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x02, 	//ISP_RGB_DIIR_COFF_CUT
					0x0b,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG

				},
				{
					0x10,  	//ISP_NR_EDGE_THD,
					0x00,	//ISP_NR_MM_THD1
					0x00, 	//ISP_NR_MODE0_LPF,
					0x01, 	//ISP_NR_MODE1_LPF,
					0xff,	//ISP_NR_MODE,
					0x01,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x03,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x03,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x82,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x0c,	//ISP_INTP_EDGE_THD1,
					0x15,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x07,	//ISP_RGB_IIR_CTRL
					0xa0,	//ISP_RGB_VIIR_COEF
					0xcc,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x0e,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x03, 	//ISP_RGB_DIIR_COFF_CUT
					0x0c,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG

				}
			},
			48,		// dynamic denoise gain threshold 0
			80,		// dynamic denoise gain threshold 1
			112		// dynamic denoise gain threshold 2
		}
	},
	// UV offset
	{
		0,		// A light U offset
		0,		// A light V offset
		0,//0x24,		// D65 light U offset
		0,//0x22,		// D65 light V offset
		3300,	// Dynamic UV offset threshold for A light
		4000	// Dynamic UV offset threshold for D65 light
	},

	// gamma 2
	{
		// normal lux
		{
			// R - G diff gamma
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			// B - G diff gamma
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		},

		// low lux
		{
			// R - G diff gamma
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			// B - G diff gamma
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		},
	},

	// Texture 2
	{
		// corner denoise
		{
			0x100,	//d0(start) for RGB domain
			0x300,	//d1(end) for RGB domain
			0x100,	//d0(start) for YUV domain
			0x300,	//d1(end) for YUV domain
			{
				{
					0x10,	//ISP_RDLOC_MAX
					0x41,	//ISP_RDLOC_RATE
					0x10,	//ISP_GLOC_MAX
					0x08,	//ISP_GLOC_RATE
					0x41,	//ISP_ILOC_MAX
					0x08,	//ISP_ILOC_RATE
					0x04,	//ISP_LOC_MAX
					0x41,	//ISP_LOC_RATE
				},
				{
					0x10,	//ISP_RDLOC_MAX
					0x41,	//ISP_RDLOC_RATE
					0x10,	//ISP_GLOC_MAX
					0x41,	//ISP_GLOC_RATE
					0x02,	//ISP_ILOC_MAX
					0x08,	//ISP_ILOC_RATE
					0x4,	//ISP_LOC_MAX
					0x41,	//ISP_LOC_RATE
				},
				{
					0x10,	//ISP_RDLOC_MAX
					0x41,	//ISP_RDLOC_RATE
					0x10,	//ISP_GLOC_MAX
					0x41,	//ISP_GLOC_RATE
					0x02,	//ISP_ILOC_MAX
					0x08,	//ISP_ILOC_RATE
					0x4,	//ISP_LOC_MAX
					0x41,	//ISP_LOC_RATE
				},
				{
					0x10,	//ISP_RDLOC_MAX
					0x41,	//ISP_RDLOC_RATE
					0x10,	//ISP_GLOC_MAX
					0x41,	//ISP_GLOC_RATE
					0x02,	//ISP_ILOC_MAX
					0x08,	//ISP_ILOC_RATE
					0x4,	//ISP_LOC_MAX
					0x41,	//ISP_LOC_RATE
				},
			},
		},
		// uv denoise
		{
			{
				{
					0x8,   //ISP_EEH_UVIIR_Y_CUTS
					4,   //ISP_EEH_UVIIR_Y_CMIN
					0xc,	 //ISP_EEH_IIR_COEF
					2,	 //ISP_EEH_IIR_CUTS
					2,  //ISP_EEh_IIR_STEP
				},
				{
					0x8,   //ISP_EEH_UVIIR_Y_CUTS
					4,   //ISP_EEH_UVIIR_Y_CMIN
					0xc,	 //ISP_EEH_IIR_COEF
					2,	 //ISP_EEH_IIR_CUTS
					2,  //ISP_EEh_IIR_STEP
				},
				{
					0x8,   //ISP_EEH_UVIIR_Y_CUTS
					4,   //ISP_EEH_UVIIR_Y_CMIN
					0xf,	 //ISP_EEH_IIR_COEF
					3,	 //ISP_EEH_IIR_CUTS
					2, //ISP_EEh_IIR_STEP
				},
				{
					0x8,   //ISP_EEH_UVIIR_Y_CUTS
					4,   //ISP_EEH_UVIIR_Y_CMIN
					0xf,	 //ISP_EEH_IIR_COEF
					4,	 //ISP_EEH_IIR_CUTS
					2, //ISP_EEh_IIR_STEP
				},
			},
		},
		// noise reduction additional
		{
			{0x1E,0x1E,0x1E,0x1E,},	//ISP_EEH_CRC_RATE
			{1,1,1,1,},	//ISP_RD_MM2_RATE
			{
				{
					0x02,	//ISP_NR_MMM_D0
					0x22,	//ISP_NR_MMM_D1
					0x01,	//ISP_NR_MMM_RATE
					0x04,	//ISP_NR_MMM_MIN
					0x80,	//ISP_NR_MMM_MAX
				},
				{
					0x02,	//ISP_NR_MMM_D0
					0x22,	//ISP_NR_MMM_D1
					0x01,	//ISP_NR_MMM_RATE
					0x04,	//ISP_NR_MMM_MIN
					0x80,	//ISP_NR_MMM_MAX
				},
				{
					0x02,	//ISP_NR_MMM_D0
					0x22,	//ISP_NR_MMM_D1
					0x01,	//ISP_NR_MMM_RATE
					0x04,	//ISP_NR_MMM_MIN
					0x80,	//ISP_NR_MMM_MAX
				},
				{
					0x02,	//ISP_NR_MMM_D0
					0x22,	//ISP_NR_MMM_D1
					0x01,	//ISP_NR_MMM_RATE
					0x04,	//ISP_NR_MMM_MIN
					0x80,	//ISP_NR_MMM_MAX
				},
			},
		},
	},

	// Edge enhance
	{
		0x01, 			//ISP_BRIGHT_RATE
		0x02, 			//ISP_BRIGHT_TRM_B1
		0x10, 			//ISP_BRIGHT_TRM_B2
		0x0e, 			//ISP_BRIGHT_TRM_K
		0x03, 			//ISP_BRIGHT_TRM_THD0
		0x12, 			//ISP_BRIGHT_TRM_THD1
		0x01, 			//ISP_DARK_RATE
		0x02, 			//ISP_DARK_TRM_B1
		0x10, 			//ISP_DARK_TRM_B2
		0x0e, 			//ISP_DARK_TRM_K
		0x03, 			//ISP_DARK_TRM_THD0
		0x12, 			//ISP_DARK_TRM_THD1
		20,  				//ISP_EDG_DIFF_C0
		3, 			//ISP_EDG_DIFF_C1
		-1, 			//ISP_EDG_DIFF_C2
		-3, 	    		//ISP_EDG_DIFF_C3
		-2, 			//ISP_EDG_DIFF_C4
		0x08, 			//ISP_EEH_SHARP_ARRAY 0
		0x0C, 			//ISP_EEH_SHARP_ARRAY10
		0x10, 			//ISP_EEH_SHARP_ARRAY11
		0x0C, 			//ISP_EEH_SHARP_ARRAY1
		0x10, 			//ISP_EEH_SHARP_ARRAY2
		0x0C, 			//ISP_EEH_SHARP_ARRAY3
		0x18, 			//ISP_EEH_SHARP_ARRAY4
		0x18, 			//ISP_EEH_SHARP_ARRAY5
		0x10, 			//ISP_EEH_SHARP_ARRAY6
		0x10, 			//ISP_EEH_SHARP_ARRAY7
		0x10, 			//ISP_EEH_SHARP_ARRAY8
		0x08, 			//ISP_EEH_SHARP_ARRAY9

	},

	// flase color
	{
		{
			{
				0x02, 			//ISP_FCRD_MIN
				0x12, 			//ISP_FCRD_YCUTS
				0x8c, 			//ISP_FCRD_CFG
				0x1f, 			//ISP_FCRD_MAX
				0x20,//ISP_MOIRE_RATE
				0x02,//MOIRE detect threshold
			},
			{
				0x02, 			//ISP_FCRD_MIN
				0x12, 			//ISP_FCRD_YCUTS
				0x8e, 			//ISP_FCRD_CFG
				0x1f, 			//ISP_FCRD_MAX
				0x20,//ISP_MOIRE_RATE
				0x02,//MOIRE detect threshold
			},
			{
				0x02, 			//ISP_FCRD_MIN
				0x12, 			//ISP_FCRD_YCUTS
				0x8f, 			//ISP_FCRD_CFG
				0x1f, 			//ISP_FCRD_MAX
				0x20,//ISP_MOIRE_RATE
				0x02,//MOIRE detect threshold
			},
			{
				0x02, 			//ISP_FCRD_MIN
				0x12, 			//ISP_FCRD_YCUTS
				0x8f, 			//ISP_FCRD_CFG
				0x1f, 			//ISP_FCRD_MAX
				0x20,//ISP_MOIRE_RATE
				0x02,//MOIRE detect threshold
			},
		}

	},

	// dead pixel cancel
	{
		0x01,	//  ISP_DDP_CTRL
		0x00, 	// ISP_DDP_SEL
		0x04,	//	ISP_DP_THD_D1
		0x02, 	//	ISP_DP_BRIGHT_THD_MIN
		0x0A,	//  ISP_DP_BRIGHT_THD_MAX
		0x03, 	//  ISP_DP_DARK_THD_MIN
		0x0c,	//  ISP_DP_DARK_THD_MAX
		0x02,	//  ISP_DDP_BRIGHT_RATE
		0x02,	//  ISP_DDP_DARK_RATE
	},

	// UV color tune
	{
		//A light UV color tune
		{
			0, //ISP_UVT_UCENTER
			0, //ISP_UVT_VCENTER
			0, //ISP_UVT_UINC
			0, //ISP_UVT_VINC
		},
		//D65 light UV color tune
		{
			0,  //ISP_UVT_UCENTER
			0,    //ISP_UVT_VINC
			0,   //ISP_UVT_UINC
			0,   //ISP_UVT_VINC
		},
		3300, // Dynamic UV color tune threshold for A light
		4000,  // Dynamic UV color tune threshold for D65 light
	},
	// NLC
	{
		// G NLC
		{0, 16, 32, 48, 64, 80, 96, 112, 128, 144, 160, 176, 192, 208, 224, 240},
		// R - G diff
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		// B - G diff
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
	},

	// HDR
	{
		// HDR threshold
		{
			10,		// Histogram dark pixel threshold for start HDR function
			10,     // Histogram bright pixel threshold for start HDR function
			60, 	// Histogram dark pixel max number
			64,		// Hdr tune max value

		},

		// FW HDR
		{
			// HDR gamma
			{0,23,43,58,73,81,88,94,99,109,117,125,132,139,145,151,157,163,168,173,177,184,190,195,201,210,222,237},
		},

		// HW HDR
		{
			// tgamma threshold
			0x8,
			// tgamma rate
			0x10,
			// HDR LPF COEF
			{0, 1, 3, 5, 6, 6, 5, 4, 2},
			// HDR halo thd
			0x10,
			// HDR curver
			{115, 115, 114, 112, 111, 109, 108, 106, 104, 100, 96, 91, 85, 79, 72, 64, 54, 45, 35, 26, 16, 10, 6, 3},
			// HDR max curver
			{192, 144, 112, 80, 61, 52, 42, 36, 32},
			//HDR step
			0x4,
			//local constrast curver
			{24, 26, 26, 24, 20, 20, 20,20, 20, 20, 20, 20, 20, 23, 26, 26},
			//local constrast rate min
			0xd,
			//local constrast rate max
			0xd,
			//local constrast step
			0x10
		},
	}



};
#endif // _OV9726_IQ_H_
