#ifndef _ST171_H_
#define _ST171_H_

void ST171SetFormatFps(U16 SetFormat, U8 Fps);
void CfgST171ControlAttr(void);
void SetST171ImgDir(U8 bySnrImgDir);
void SetST171BandingFilter(U8 byFPS);
void SetST171IntegrationTime(U16 wEspline);
void SetST171Exposuretime_Gain(float fExpTime, float fTotalGain);
void InitST171IspParams();
void ST171_POR();
void SetST171DynamicISP(U8 byAEC_Gain);
void SetST171DynamicISP_AWB(U16 wColorTempature);
void SetST171Gain(float fGain);
#endif // _ST171_H_

