#include "Inc.h"

#ifdef RTS58XX_SP_SIV120D


// we need to change matrix by experiment
code OV_CTT_t gc_SIV120D_CTT[3] =
{
	{3000,0x8A,0x88,0xBF},
	{4050,0xB8,0x88,0xA9},
	{6500,0xD2,0x88,0x7B},
};

// hemonel 2009-03-17: copy SIV120D fps
code OV_FpsSetting_t  g_staSIV120DFpsSetting[]=
{
	//  FPS	extradummypixel clkrc	pclk = 24M/2^(clkrc+1)
	//	{1,		(1176+1),	(23),	  1000000},	// 1M
	{3,		(1000),		(3),	  	 1500000},	// 2M	// 2.998
	{5, 		(1200),		(2),	  	 3000000},	// 2M
	//	{8,  		(1500),		(1),		 6000000},	// 4M	// 7.995
	{9,  		(1333),		(1),		 6000000},	// 4M	// 8.995
	{10,		(1200),		(1),		 6000000},	// 4M
	{15,		(800),		(1),		 6000000},	// 6M
	{20,		(1200),		(0),		12000000},	// 8M
	{25,		(960),		(0),		12000000},	// 12M	// hemonel 2009-04-20: fix scale down abnormal to change 157 to 156
	{30,		(800),		(0),		12000000},	// 12M
};



t_RegSettingWB code gc_SIV120D_Setting[] =
{
	//====================================================
	//SIV120D INITIAL SETTING
	///ChipID	SIV120D	# Don't delete or edit this line !!!
	//# This file is for "Internal V1.3". ================
	//==================================================
	//[Addr]	[Data]

	// SNR
	{0x000,	0x00},
	{0x004,	0x04},
	{0x005,	0x03},
	{0x010,	0x34},
	{0x011,	0x27},
	{0x012,	0x21},
	{0x016,	0xc6},
	{0x017,	0xaa},

	{0x020,	0x00},	//P_BNKT
	{0x021,	0x01},	//P_HBNKT
	{0x022,	0x01},	//P_ROWFIL
	{0x023,	0x01},	//P_VBNKT

	// AE
	{0x100,	0x01},
	{0x111,	0x14}, 	// 6fps at lowlux
	{0x112,	0x78},	// D65 target 0x74
	{0x113,	0x78},	// CWF target 0x74
	{0x114,	0x78},	// A target   0x74
	{0x11E,	0x08},	// ini gain	0x08
	{0x134,	0x7d},
	{0x140,	0x60}, 	// Max x6
	/*
	{0x141,	0x20},	#AG_TOP1	0x28
	{0x142,	0x20},	#AG_TOP0	0x28
	{0x143,	0x00},	#AG_MIN1	0x08
	{0x144,	0x00},	#AG_MIN0	0x08
	{0x145,	0x00},	#G50_dec	0x09
	{0x146,	0x0a},	#G33_dec	0x17
	{0x147,	0x10},	#G25_dec	0x1d
	{0x148,	0x13},	#G20_dec	0x21
	{0x149,	0x15},	#G12_dec	0x23
	{0x14a,	0x18},	#G09_dec	0x24
	{0x14b,	0x1a},	#G06_dec	0x26
	{0x14c,	0x1d},	#G03_dec	0x27
	{0x14d,	0x20},	#G100_inc	0x27
	{0x14e,	0x10},	#G50_inc	0x1a
	{0x14f,	0x0a},	#G33_inc	0x14
	{0x150,	0x08},	#G25_inc	0x11
	{0x151,	0x06},	#G20_inc	0x0f
	{0x152,	0x05},	#G12_inc	0x0d
	{0x153,	0x04},	#G09_inc	0x0c
	{0x154,	0x02},	#G06_inc	0x0a
	{0x155,	0x01},	#G03_inc	0x09
	*/

	//	AWB
	{0x200,	0x02},
	{0x210, 	0xd3},
	{0x211,  	0xc0},
	{0x212,  	0x80},
	{0x213, 	0x80},
	{0x214,	0x7e},
	{0x215,	0xfe},	// R gain Top
	{0x216,  	0x80},	// R gain bottom
	{0x217, 	0xcb},	// B gain Top
	{0x218,  	0x70},	// B gain bottom 0x80
	{0x219,  	0x94},	// Cr top value 0x90
	{0x21a,  	0x6c},	// Cr bottom value 0x70
	{0x21c,  	0x6c},	// Cb bottom value 0x70
	{0x21d,  	0x94},	// 0xa0
	{0x21e,  	0x6c},	// 0x60
	{0x220,  	0xe8},	// AWB luminous top value
	{0x221,  	0x30},	// AWB luminous bottom value 0x20
	{0x222,  	0xa4},
	{0x223,  	0x20},
	{0x225,  	0x20},
	{0x226,  	0x0f},
	{0x227,  	0x60},	// BRTSRT
	{0x228,  	0xb4},	// BRTRGNTOP result 0xb2
	{0x229,  	0xb0},	// BRTRGNBOT
	{0x22a,  	0x92},	// BRTBGNTOP result 0x90
	{0x22b, 	0x8e},	// BRTBGNBOT
	{0x22c,  	0x88},	// RGAINCONT
	{0x22d, 	0x88},	// BGAINCONT

	{0x230,  	0x00},
	{0x231,  	0x10},
	{0x232,  	0x00},
	{0x233,  	0x10},
	{0x234,  	0x02},
	{0x235,  	0x76},
	{0x236,  	0x01},
	{0x237,  	0xd6},
	{0x240,  	0x01},
	{0x241,  	0x04},
	{0x242,  	0x08},
	{0x243,  	0x10},
	{0x244, 	0x12},
	{0x245,  	0x35},
	{0x246,  	0x64},
	{0x250,  	0x33},
	{0x251,  	0x20},
	{0x252,  	0xe5},
	{0x253,  	0xfb},
	{0x254,  	0x13},
	{0x255,  	0x26},
	{0x256,  	0x07},
	{0x257,  	0xf5},
	{0x258,  	0xea},
	{0x259,  	0x21},

	{0x262,	0x88},	//G gain

	{0x263, 	0xb3},	 //R D30 to D20
	{0x264,  	0xc3},	 //B D30 to D20
	{0x265,  	0xb3},	 //R D20 to D30
	{0x266,  	0xc3},	 //B D20 to D30

	{0x267,  	0xdd},	 //R D65 to D30
	{0x268, 	0xa0},	 //B D65 to D30
	{0x269,  	0xdd},	 //R D30 to D65
	{0x26a,  	0xa0},	 //B D30 to D65

	//IDP
	{0x300,	0x03},
	{0x310,	0xff},
	{0x311,	0x1d},
	{0x312,	0x3d},
	{0x314,	0x04},	// don't change

	// DPCNR
	{0x317, 	0x28}, 	// DPCNRCTRL
	{0x318, 	0x00}, 	//DPTHR
	{0x319, 	0x56},   	//C DP Number ( Normal [7:6] Dark [5:4] ) | [3:0] DPTHRMIN
	{0x31A, 	0x56},   	//G DP Number ( Normal [7:6] Dark [5:4] ) | [3:0] DPTHRMAX
	{0x31B, 	0x12},   	//DPTHRSLP( [7:4] @ Normal | [3:0] @ Dark )
	{0x31C, 	0x04},   	//NRTHR
	{0x31D, 	0x00},   	//[5:0] NRTHRMIN 0x48
	{0x31E, 	0x00},   	//[5:0] NRTHRMAX 0x48
	{0x31F, 	0x08},   	//NRTHRSLP( [7:4] @ Normal | [3:0] @ Dark )	0x2f
	{0x320, 	0x04},  	//IllumiInfo STRTNOR
	{0x321, 	0x0f},  	//IllumiInfo STRTDRK

	// Gamma
	{0x330,	0x00}, 		//0x0
	{0x331,	0x04},		//0x3
	{0x332,	0x0b},		//0xb
	{0x333,	0x24},	//0x1f
	{0x334,	0x49},	//0x43
	{0x335,	0x66},	//0x5f
	{0x336,	0x7C},	//0x74
	{0x337,	0x8D},	//0x85
	{0x338,	0x9B},	//0x94
	{0x339,	0xAA},	//0xA2
	{0x33a,	0xb6},	//0xAF
	{0x33b,	0xca},		//0xC6
	{0x33c,	0xdc},		//0xDB
	{0x33d,	0xef},		//0xEF
	{0x33e, 	0xf8},		//0xF8
	{0x33f,	0xFF},		//0xFF

	// Shading Register Setting
	{0x340,	0x0a},
	{0x341,	0xdc},
	{0x342,	0x44},
	{0x343,	0x33},
	{0x344,	0x33},
	{0x345,	0x11},
	{0x346,	0x11},	// left R gain[7:4], right R gain[3:0]
	{0x347,	0x22},	// top R gain[7:4], bottom R gain[3:0]
	{0x348,	0x01},	// left Gr gain[7:4], right Gr gain[3:0] 0x21
	{0x349,	0x01},	// top Gr gain[7:4], bottom Gr gain[3:0]
	{0x34a,	0x01},	// left Gb gain[7:4], right Gb gain[3:0] 0x02
	{0x34b,	0x01},	// top Gb gain[7:4], bottom Gb gain[3:0]
	{0x34c,	0x01},	// left B gain[7:4], right B gain[3:0]
	{0x34d,	0x00},	// top B gain[7:4], bottom B gain[3:0]
	{0x34e,	0x04},	// X-axis center high[3:2], Y-axis center high[1:0]
	{0x34f,	0x50},	// X-axis center low[7:0] 0x50
	{0x350,	0xf8},	// Y-axis center low[7:0] 0xf6
	{0x351,	0x80},	// Shading Center Gain
	{0x352,	0x00},	// Shading R Offset
	{0x353,	0x00},	// Shading Gr Offset
	{0x354,	0x00},	// Shading Gb Offset
	{0x355,	0x00},	// Shading B Offset

	// Interpolation
	{0x360,	0x57},	//INT outdoor condition
	{0x361,	0xff},		//INT normal condition

	{0x362,	0x77},	//ASLPCTRL 7:4 GE, 3:0 YE
	{0x363,	0x38},	//YDTECTRL (YE) [7] fixed,
	{0x364,	0x38},	//GPEVCTRL (GE) [7] fixed,

	{0x366,	0x0c},		//SATHRMIN
	{0x367,	0xff},
	{0x368,	0x04},	//SATHRSRT
	{0x369,	0x08},	//SATHRSLP

	{0x36a,	0xaf},		//PTDFATHR [7] fixed, [5:0] value
	{0x36b,	0x78},	//PTDLOTHR [6] fixed, [5:0] value
	{0x36d,	0x84},	//YFLTCTRL

	// Color matrix (D65) - Daylight
	{0x371,	0x42},	//0x40
	{0x372,	0xbf},	//0xb9
	{0x373,	0x00},	//0x07
	{0x374,	0x0f},	//0x15
	{0x375,	0x31},	//0x21
	{0x376,	0x00},	//0x0a
	{0x377,	0x00},	//0xf8
	{0x378,	0xbc},	//0xc5
	{0x379,	0x44},	//0x46

	// Color matrix (D30) - CWF
	{0x37a,	0x3d},	//0x3a
	{0x37b,	0xcd},	//0xcd
	{0x37c,	0xf6},  	//0xfa
	{0x37d,	0x14},  	//0x12
	{0x37e,	0x2c},  	//0x2c
	{0x37f,	0x00},  	//0x02
	{0x380,	0xf6},  	//0xf7
	{0x381,	0xc7},  	//0xc7
	{0x382,	0x43},  	//0x42

	// Color matrix (D20) - A
	{0x383,	0x3d},	//0x38
	{0x384,	0xcd},  	//0xc4
	{0x385,	0xf6},  	//0x04
	{0x386,	0x14},  	//0x07
	{0x387,	0x2c},  	//0x25
	{0x388,	0x00},  	//0x14
	{0x389,	0xf6},  	//0xf0
	{0x38a,	0xc7},  	//0xc2
	{0x38b,	0x43},  	//0x4f

	{0x38c,	0x10},	//CMA select

	{0x38d,	0xa4},	//programmable edge
	{0x38e,	0x02},	//PROGEVAL
	{0x38f,	0x00},	//Cb/Cr coring

	{0x390,	0x15},	//GEUGAIN
	{0x391,	0x15},	//GEDGAIN
	{0x392,	0xf0},	//Ucoring [7:4] max, [3:0] min
	{0x394,	0x00},	//Uslope (1/128)
	{0x396,	0xf0},	//Dcoring [7:4] max, [3:0] min
	{0x398,	0x00},	//Dslope (1/128)

	{0x39a,	0x08},
	{0x39b,	0x18},

	{0x39f,	0x0c},	//YEUGAIN
	{0x3a0,	0x0c},	//YEDGAIN
	{0x3a1,	0x33},	//Yecore [7:4]upper [3:0]down

	{0x3a9,	0x10},	// Cr saturation 0x12
	{0x3aa,	0x10},	// Cb saturation 0x12
	{0x3ab,	0x82},	// Brightness
	{0x3ae,	0x40},	// Hue
	{0x3af,	0x86},	// Hue
	{0x3b9,	0x10},	// 0x20 lowlux color
	{0x3ba,	0x20},	// 0x10 lowlux color

	//inverse color space conversion
	{0x3cc,	0x40},
	{0x3cd,	0x00},
	{0x3ce,	0x58},
	{0x3cf,	0x40},
	{0x3d0,	0xea},
	{0x3d1,	0xd3},
	{0x3d2,	0x40},
	{0x3d3,	0x6f},
	{0x3d4,	0x00},

	// ee/nr
	{0x3d9,	0x0b},
	{0x3da,	0x1f},
	{0x3db,	0x05},
	{0x3dc,	0x08},
	{0x3dd,	0x3c},
	{0x3de,	0xf9},	//NOIZCTRL

	/*
		//dark offset
		{0x3df,	0x10},
		{0x3e0,	0x60},
		{0x3e1,	0x90},
		{0x3e2,	0x08},
		{0x3e3,	0x0a},
	*/

	//memory speed
	{0x3e5,	0x15},
	{0x3e6,	0x20},
	{0x3e7,	0x04},

	//Sensor On
	{0x000,	0x00},
	{0x003,	0x05},

	// [END]

};

static OV_FpsSetting_t*  GetOvFpsSetting(U8 Fps, OV_FpsSetting_t staOvFpsSetting[], U8 byArrayLen)
{

	U8 i;
	U8 Idx;

//	ISP_MSG((" array size = %bd\n", byArrayLen));

	if(g_bIsHighSpeed)
	{
		Idx=2; //8fps for uxga, 10 fps for vga
	}
	else
	{
		Idx = 0;
	}

	for(i=0; i< byArrayLen; i++)
	{
//		ISP_MSG((" fps i = %bd\n",i));
		if(staOvFpsSetting[i].byFps ==Fps)
		{
			Idx=i;
			break;
		}
	}
//	ISP_MSG((" get fps idx = %bd\n",Idx));
	return &staOvFpsSetting[Idx];

}

//this function is also called during Probe stage to calculate the transfer_size/LWM/HWM
void GetSensorPclkHsync(U16 wSensorSPFormat,U8 byFps)
{
	OV_FpsSetting_t *pSIV120D_FpsSetting;

	wSensorSPFormat = wSensorSPFormat; // for delete warning

	pSIV120D_FpsSetting=GetOvFpsSetting(Fps, g_staSIV120DFpsSetting, sizeof(g_staSIV120DFpsSetting)/sizeof(OV_FpsSetting_t));
	g_wSensorHsyncWidth = pSIV120D_FpsSetting->wExtraDummyPixel;
	g_dwPclk= pSIV120D_FpsSetting->dwPixelClk;
}

void SIV120DSetFormatFps(U8 SetFormat, U8 Fps)
{
	OV_FpsSetting_t *pSIV120D_FpsSetting;
	U16 wHBnkt;

	SetFormat = SetFormat; // for delete warning

	pSIV120D_FpsSetting=GetOvFpsSetting(Fps, g_staSIV120DFpsSetting, sizeof(g_staSIV120DFpsSetting)/sizeof(OV_FpsSetting_t));

	// initial all register setting
	WriteSensorSettingWB(sizeof(gc_SIV120D_Setting)/3, gc_SIV120D_Setting);

	// 2) write sensor register for fps
	Write_SenReg_Mask(0x004, (pSIV120D_FpsSetting->byClkrc)<<2, 0x0C);	// write CLKRC
	wHBnkt =  pSIV120D_FpsSetting->wExtraDummyPixel -(800-1);
	Write_SenReg(0x021, INT2CHAR(wHBnkt,0));	//Write dummy pixel LSB
	Write_SenReg_Mask(0x020, wHBnkt>>4, 0xF0); // Writedummy pixel MSB

	// 3) update variable for AE
//	g_wAECExposureRowMax = 500;
	// hemonel 2009-08-10: for calculate manual exposure time
	//g_wSensorHsyncWidth = pSIV120D_FpsSetting->wExtraDummyPixel;
	//g_dwPclk= pSIV120D_FpsSetting->dwPixelClk;
	GetSensorPclkHsync(VGA_FRM,Fps);

}

void CfgSIV120DControlAttr(void)
{
	g_bySensorSize = SENSOR_SIZE_VGA;
	g_bySensorSPFormat = VGA_FRM;

	{
		memcpy(g_asOvCTT,gc_SIV120D_CTT,sizeof(gc_SIV120D_CTT));
	}

	{
		// AE backlight compensation parameter
		g_byOVAEW_Normal = SIV120D_AE_TARGET;
		g_byOVAEB_Normal = SIV120D_AE_TARGET;

		g_byOVAEW_BLC = g_byOVAEW_Normal + 0x20;
		g_byOVAEB_BLC = g_byOVAEB_Normal + 0x20;
	}

	{
		//g_bySV28VoltSel =  SV28_VOL_2V9;
		//g_bySV18VoltSel=  SV18_VOL_1V5;
		//	XBYTE[REG_TUNESVIO_SVA] =  SV28_VOL_2V9|SV18_VOL_1V5;
		// hemonel 2010-09-08: support 1.8V sensor IO
#if (_CHIP_ID_ & _RTS5840_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V79;
#elif (_CHIP_ID_ & _RTS5832_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V85;
#elif (_CHIP_ID_ & _RTS5829B_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V82;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V80;
#endif
	}

//	if(g_byFormatCfgExist==0) // format config not exist, use default setting.
	{
		InitFormatFrameFps();
	}


}

#ifndef _USE_BK_HSBC_ADJ_
void SetSIV120DBrightness(S16 swSetValue)
{

	U16 wBrightVal;

	//Read_SenReg(0xAB, &wBrightSign);
	if(swSetValue>=0)
	{
		wBrightVal = swSetValue;
	}
	else
	{
		wBrightVal = -swSetValue + 0x80;
	}
	Write_SenReg(0x3AB, wBrightVal);	// write Brightness value

	return;

}


void	SetSIV120DContrast(U16 wSetValue)
{


	Write_SenReg_Mask(0x03AC, wSetValue, 0x3F);		// [7] contrast gain enable, [5:0] contrast gain (x1/16)
	Write_SenReg(0x03AD, 0x80);		// Y offset value @ contrast gain adaptaion

	return;



}

void	SetSIV120DSaturation(U16 wSetValue)
{

	Write_SenReg_Mask(0x03A9, wSetValue, 0x3F);
	Write_SenReg_Mask(0x03A9, wSetValue, 0x3F);
	return;


}

void	SetSIV120DHue(S16 swSetValue)
{


	U8 byHueCos,byHueSin, byHueCosSign, byHueSinSign;
	float fHue;

	if(swSetValue>=0)
	{
		byHueSinSign = 0x00;
	}
	else
	{
		byHueSinSign = 0x80;
	}
	if((swSetValue <= 90) &&(swSetValue >= -90))
	{
		byHueCosSign = 0x00;
	}
	else
	{
		byHueCosSign = 0x80;
	}

	fHue = (float)swSetValue/(float)180*3.1415926;
	byHueCos = abs(cos(fHue)*64);
	byHueSin = abs(sin(fHue)*64);

	Write_SenReg(0x3AE, byHueCos + byHueCosSign);
	Write_SenReg(0x3AF, byHueSin + byHueSinSign);

	return;


}
#endif

// manual sharpness mode
void	SetSIV120DSharpness(U8 bySetValue)
{


	U8 byGreenSharpLimit;
	U8 byYSharpLimit;

	switch(bySetValue)
	{
	case 0:
		byGreenSharpLimit = 0x00;
		byYSharpLimit = 0x00;
		break;
	case 1:
		byGreenSharpLimit = 0x06;
		byYSharpLimit = 0x04;
		break;
	case 2:
		byGreenSharpLimit = 0x0C;
		byYSharpLimit = 0x08;
		break;
	case 3:
		byGreenSharpLimit = 0x12;
		byYSharpLimit = 0x0C;
		break;
	case 4:	// default value
		byGreenSharpLimit = 0x18;
		byYSharpLimit = 0x10;
		break;
	case 5:
		byGreenSharpLimit = 0x24;
		byYSharpLimit = 0x14;
		break;
	case 6:
		byGreenSharpLimit = 0x32;
		byYSharpLimit = 0x18;
		break;
	default:
		byGreenSharpLimit = 0x4A;
		byYSharpLimit = 0x1C;
		break;
	}

	Write_SenReg(0x390, byGreenSharpLimit);	// Green Edge upper gain
	Write_SenReg(0x391, byGreenSharpLimit);	// Green Edge down gain
	Write_SenReg(0x39F, byYSharpLimit);		// Y Edge upper gain
	Write_SenReg(0x3A0, byYSharpLimit);		// Y Edge down gain

}


#ifndef _USE_BK_SPECIAL_EFFECT_
void SetSIV120DEffect(U8 byEffect)
{
	/*
	U8 bySDE = 0x20; //SDE: special digital effect enable
	U8 byUvalue = 0x80, byVvalue = 0x80;

	switch (byEffect)
	{
		case SNR_EFFECT_NEGATIVE:
			//byEfft = 0x20;
			bySDE = 0x40;
			break;
		case SNR_EFFECT_MONOCHROME:
			//byEfft = 0x20;
			//bySDE = 0x20;
			break;
		case SNR_EFFECT_SEPIA:
			//byEfft = 0x20;
			bySDE = 0x18;
			byUvalue = 0x40;
			byVvalue = 0xa0;
			break;
		case SNR_EFFECT_GREENISH:
			//byEfft = 0x20;
			bySDE = 0x18;
			byUvalue = 0x60;
			byVvalue = 0x60;
			break;
		case SNR_EFFECT_REDDISH:
			//byEfft = 0x20;
			bySDE = 0x18;
			byUvalue = 0x80;
			byVvalue = 0xc0;
			break;
		case SNR_EFFECT_BLUISH:
			//byEfft = 0x20;
			bySDE = 0x18;
			byUvalue = 0xa0;
			byVvalue = 0x40;
			break;
		default:
			//byEfft = 0x0; //no effect
			bySDE = 0x04;
			break;
	}
	//Write_SenReg_Mask(0x64, byEfft, 0x20);
	Write_SenReg_Mask(0xa6, bySDE, 0xf8);
	Write_SenReg(0x60, byUvalue);
	Write_SenReg(0x61, byVvalue); */
}
#endif

void SetSIV120DGain(float fGain)
{
}

void SetSIV120DImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.
{
	/***
		0x01 mirror
		0x02 flip
		0x03 mirror and flip
	***/
	Write_SenReg_Mask(0x004, bySnrImgDir,0x03);
}

/*
void SetSIV120DWindowCrop(U16 wXStart,U16 wXEnd,U16 wYStart,U16 wYEnd )
{

}
*/

void SetSIV120DOutputDim(U16 wWidth,U16 wHeight)
{


	U8 byVMODE;
	U16 byBkWinHeight = 0, byBkWinWidth = 0;

	wWidth = wWidth; 	// for delete warning

	switch (wHeight)
	{
	case 120:
		byVMODE = 0x00;
		break;
	case 144:
		byVMODE = 0x01;
		//adjust window start&end
		byBkWinHeight = (240 - 144)/2;
		byBkWinWidth = (320 -176);
		//Write_SenReg(0x3C1,0x48);
		//Write_SenReg(0x3C3,0x30);

		break;
	case 240:
		byVMODE = 0x01;
		break;
	case 288:
		byVMODE = 0x03;
		//adjust window start&end
		byBkWinHeight = (480 -288)/2;
		byBkWinWidth = (640 -352);
		//Write_SenReg(0x3C1,0x90);
		//Write_SenReg(0x3C3,0x60);
		break;
	case 480:
		byVMODE = 0x03;
		break;
	default:	// 640x480
		byVMODE = 0x03;
		break;
	}
	SetBkWindowStart(byBkWinWidth, byBkWinHeight);
	Write_SenReg_Mask(0x005, byVMODE, 0x03);

}

void SetSIV120DPwrLineFreq(U8 byFPS, U8 byLightFrq)
{


	U8 byD50Base, byD60Base;

	// hemonel 2009-07-06: optimize code
	byD50Base= CalAntiflickerStep(byFPS, 50, 500);// (U16)byFPS*500/(U16)100;	// 500: the number of rows at one frame with LA measured
	byD60Base= CalAntiflickerStep(byFPS, 60, 500);//(U16)byFPS*500/(U16)120;

	if (byLightFrq == PWR_LINE_FRQ_50)
	{
		Write_SenReg(0x134, byD50Base);
	}
	else if (byLightFrq == PWR_LINE_FRQ_60)
	{
		Write_SenReg(0x134, byD60Base);
	}

}

void SetSIV120DWBTemp(U16 wRgain, U16 wGgain, U16 wBgain)
{


//	OV_CTT_t ctt;

//	ctt = OV_SetColorTemperature(wSetValue);

	Write_SenReg(0x260, wRgain);
	Write_SenReg(0x262, wGgain);
	Write_SenReg(0x261, wBgain);

}

/*
OV_CTT_t GetSIV120DAwbGain(void)
{
	OV_CTT_t ctt;

	Read_SenReg(0x260, &ctt.wRgain);
	Read_SenReg(0x262, &ctt.wGgain);
	Read_SenReg(0x261, &ctt.wBgain);

	return ctt;
}
*/
void SetSIV120DWBTempAuto(U8 bySetValue)
{

	if(bySetValue)
	{
		// atuo white balance
		Write_SenReg_Mask(0x210, 0xC0,0xC0);
	}
	else
	{
		// manual white balance
		Write_SenReg_Mask(0x210, 0x00,0xC0);
	}


}

void SetSIV120DBackLightComp(U8 bySetValue)
{

	U8 byAE_TARGET;

	if(bySetValue)
	{
		byAE_TARGET = g_byOVAEW_BLC;
	}
	else
	{
		byAE_TARGET = g_byOVAEW_Normal;
	}

	Write_SenReg(0x112, byAE_TARGET);
	Write_SenReg(0x113, byAE_TARGET);
	Write_SenReg(0x114, byAE_TARGET);

}

U16 GetSIV120DAEGain(void)
{

	U16 wTemp;
	U16 wGain;

	Read_SenReg(0x132, &wTemp);

	wGain = (wTemp&0x1F) + 32;	// FGAINx32
	wGain <<=(wTemp>>5);

	return (wGain>>1);	// Gainx16

}

void SetSIV120DIntegrationTimeAuto(U8 bySetValue)
{
	if(bySetValue == EXPOSURE_TIME_AUTO_MOD_APERTPRO)
	{
		// auto exposure
		Write_SenReg_Mask(0x110, 0x80,  0x80);
	}
	else
	{
		// manual expsoure
		Write_SenReg_Mask(0x110, 0x00,  0x80);
	}
}

void SetSIV120DIntegrationTime(U16 wEspline)
{
	// write exposure time to register
	Write_SenReg(0x131, INT2CHAR(wEspline, 0));
	Write_SenReg(0x130, INT2CHAR(wEspline, 1));
}
#endif //RTS58XX_SP_SIV120D


