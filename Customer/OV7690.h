#ifndef _OV7690_H_
#define _OV7690_H_

#define OV7690_AEW 0x78
#define OV7690_AEB	0x68

void OV7690SetFormatFps(U8 SetFormat, U8 Fps);
void	SetOV7690Sharpness(U8 bySetValue);

//void CfgOV7670ControlAttr(void);
//void SetOV7670Brightness(S16 swSetValue);
//void	SetOV7670Contrast(U16 wSetValue);
//void	SetOV7670Saturation(U16 wSetValue);
//void	SetOV7670Hue(S16 swSetValue);
//void	SetOV7670Sharpness(U8 bySetValue);
void SetOV7690Effect(U8 byEffect);
void SetOV7690ImgDir(U8 bySnrImgDir);
//void SetOV7690WindowCrop(U16 wXStart,U16 wXEnd,U16 wYStart,U16 wYEnd );
//void SetOV7690OutputDim(U16 wWidth,U16 wHeight);
void SetOV7690PwrLineFreq(U8 byFPS, U8 byLightFrq);
void SetOV7690WBTemp(U16 wRgain, U16 wGgain, U16 wBgain);
void SetOV7690WBTempAuto(U8 bySetValue);
void SetOV7690BackLightComp(U8 bySetValue);
extern code OV_CTT_t gc_OV7690_CTT[3];
void CfgOV7690ControlAttr(void);
void SetOV7690IntegrationTimeAuto(U8 bySetValue);
void SetOV7690IntegrationTime(U16 wEspline);
void SetOV7690Gain(float fGain);
#endif // _OV7670_H_

