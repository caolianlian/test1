#include "Inc.h"

#ifdef RTS58XX_SP_YACBAA0S

OV_CTT_t code gc_YACBAA0S_CTT[3] =
{
	{2800,0x28,0x20,0x55},
	{4050,0x46,0x20,0x48},
	{6500,0x52,0x20,0x2D},
};

//-----------20090731__JQG-------------
// YACBAA0S FPS calu: (664+Hblank)X(504+VBlank) /pclk = 1/FPS
//			for YUV: PCLK = MCLK / (2 X 2^CLKRC)
//			Vblank: 4
OV_FpsSetting_t code g_staYACBAA0SFpsSetting[]=
{
	//  FPS	extradummypixel clkrc	pclk
//	{1,		(1176+1),	(23),	  1000000},	// 1M
//	{3,		(523+1),	(11),	  2000000},	// 2M
//	{5, 		(0),		(11),	  2000000},	// 2M
//	{8,  		(196+1),	(5),		  4000000},	// 4M
	{9,  		(0x0288),	(1),		  6000000},
	{10,		(0x06a2),	(0),		12000000},
	{15,		(0x0390),	(0),		12000000},
	{20,		(0x0200),	(0),		12000000},
	{25,		(0x0118),	(0),		12000000},
	{30,		(0x007b),	(0),		12000000},// 12M
};

t_RegSettingBB code gc_YACBAA0S_Setting[] =
{
	// ______Common setting____
	{0x03,0x00},//page 0
	{0x01,0xf1},
	{0x01,0xf3},
	{0x01,0xf1},

	{0x03,0x20},//page 20
	{0x10,0x0c},

	{0x03,0x22},//page 22
	{0x10,0x69},

	{0x03,0x00},//page 0
	{0x10,0x00},
	{0x11,0x90},
	{0x12,0x00},
	{0x20,0x00},
	{0x21,0x05},
	{0x22,0x00},
	{0x23,0x07},
	{0x24,0x01},
	{0x25,0xe0},
	{0x26,0x02},
	{0x27,0x80},
	{0x40,0x00},
	{0x41,0x88},
	{0x42,0x00},
	{0x43,0x04},
	{0x80,0x0E},
	{0x81,0xb1},
	{0x90,0x0c},
	{0x91,0x0d},
	{0x92,0x60},
	{0x93,0x30},
	{0xa0,0x41},
	{0xa1,0x41},
	{0xa2,0x41},
	{0xa3,0x41},
	{0xa4,0x41},
	{0xa5,0x41},
	{0xa6,0x41},
	{0xa7,0x41},
	{0xa8,0x45},
	{0xa9,0x45},
	{0xaa,0x45},
	{0xab,0x45},
	{0xac,0x45},
	{0xad,0x45},
	{0xae,0x45},
	{0xaf,0x45},

	{0x03,0x02},//page 2
	{0x1a,0x31},
	{0x1c,0x00},
	{0x1d,0x03},
	{0x20,0x33},
	{0x21,0x77},
	{0x22,0xAD},
	{0x34,0xff},
	{0x54,0x30},
	{0x60,0x46},
	{0x61,0x60},
	{0x62,0x48},
	{0x63,0x4a},
	{0x64,0x4d},
	{0x65,0x58},
	{0x72,0x48},
	{0x73,0x5b},
	{0x74,0x48},
	{0x75,0x5b},
	{0x80,0x02},
	{0x81,0x40},
	{0x82,0x07},
	{0x83,0x0b},
	{0x84,0x07},
	{0x85,0x0b},
	{0x86,0x07},
	{0x87,0x0b},
	{0x92,0x20},
	{0x93,0x30},
	{0x94,0x20},
	{0x95,0x30},
	{0xa8,0x03},
	{0xa9,0x18},
	{0xaa,0x03},
	{0xab,0x38},
	{0xa0,0x03},
	{0xa1,0x3f},

	{0x03,0x10},//page 10
	{0x10,0x03},
	{0x11,0x03},
	{0x12,0x30},
	{0x40,0x08},
	{0x41,0x00},
	{0x50,0xae},
	{0x60,0x1f},
	{0x61,0xa0},
	{0x62,0x98},
	{0x63,0x50},
	{0x64,0xff},

	{0x03,0x11},//page 11
	//{0x10,0x1d},
	//{0x11,0x0E},
	//{0x21,0x55},
	//{0x60,0x20},
	//{0x62,0x43},
	//{0x63,0x63},
	{0x10,0x1d}, // low pass filter
	{0x11,0x0E},
	{0x21,0x55},
	{0x60,0x63},
	{0x61,0x33},
	{0x62,0x43},
	{0x63,0x83},

	{0x03,0x12},//page 12
	{0x40,0x21},
	{0x41,0x07},
	{0x50,0x03},
	{0x70,0x1d},
	{0x74,0x04},
	{0x75,0x06},
	{0x90,0x5d},
	{0x91,0x10},
	{0xb0,0xc9},

	{0x03,0x13},//page 13
	{0x10,0x29},
	{0x11,0x07},
	{0x12,0x01},
	{0x13,0x02},
	{0x20,0x01},
	{0x21,0x01},
	{0x23,0x18},
	{0x24,0x03},
	{0x29,0x59},
	{0x80,0x0d},
	{0x81,0x01},
	{0x83,0x5d},
	{0x90,0x07},
	{0x91,0x03},
	{0x93,0x19},
	{0x94,0x03},
	{0x95,0x00},

	{0x03,0x14},//page 14
	{0x10,0x07},
	{0x20,0x9F},//80____lens correction
	{0x21,0x8F},//80
	{0x22,0x70},//70
	{0x23,0x70},//60
	{0x24,0x68},//60
	{0x25,0x59},
	{0x26,0x55},
	/*
	{0x20,0x80},
	{0x21,0x80},
	{0x22,0x70},//0x73
	{0x23,0x60},
	{0x24,0x60},
	{0x25,0x59},
	{0x26,0x55},
	*/
	{0x03,0x15},//page 15
	{0x10,0x0f},
	{0x14,0x36},
	{0x16,0x28},
	{0x17,0x2f},
	{0x30,0x5c},
	{0x31,0x26},
	{0x32,0x0a},
	{0x33,0x11},
	{0x34,0x65},
	{0x35,0x14},
	{0x36,0x01},
	{0x37,0x33},
	{0x38,0x74},
	{0x40,0x00},
	{0x41,0x00},
	{0x42,0x00},
	{0x43,0x8b},
	{0x44,0x07},
	{0x45,0x04},
	{0x46,0x84},
	{0x47,0xa1},
	{0x48,0x25},

	{0x03,0x16},//page 16
	{0x10,0x01},
	{0x30,0x00},
	{0x31,0x1E},
	{0x32,0x2D},
	{0x33,0x3C},
	{0x34,0x5b},
	{0x35,0x75},
	{0x36,0x8c},
	{0x37,0x9f},
	{0x38,0xaf},
	{0x39,0xbd},
	{0x3a,0xca},
	{0x3b,0xdd},
	{0x3c,0xec},
	{0x3d,0xf7},
	{0x3e,0xff},

	{0x03,0x17},//page 17
	{0xc0,0x03},
	{0xc4,0x4b},
	{0xC5,0x3f},
	{0xc6,0x02},
	{0xC7,0x20},

	{0x03,0x20},//page 20
	{0x10,0xcc},
	{0x11,0x00},
	{0x15,0xd1},
	{0x1c,0x50},
	{0x1d,0x24},
	{0x1e,0x28},
	{0x20,0x00},
	{0x28,0x0f},
	{0x29,0xaa},
	{0x2a,0xf0},
	{0x2b,0x34},
	{0x2d,0x07},
	{0x30,0xf8},
	{0x60,0x00},
	{0x70,0x42},
	{0x78,0x22},
	{0x79,0x21},
	{0x7A,0x22},

	{0x83,0x01},//manual exposure time
	{0x84,0x24},
	{0x85,0xf8},

	{0x83,0x00},//manual exposure time // for Hynix AE bug  50Hz
	{0x84,0x75},
	{0x85,0x30},

	{0x86,0x01},//min exposure time___max frame rate
	{0x87,0x90},

	{0x88,0x03},//max exposure time___min frame rate___0x3a98*15=0x30d4*18=0x036ee8
	{0x89,0x6e},//0xdc}, //___change this for anti-flicker___
	{0x8a,0xe8},//0x6C},//___50Hz ratio is 15, min FPS is 100/15 = 120/18 = 6.7___

	{0x88,0x02},//------only for 50Hz, 60Hz maybe flicker, 60Hz use 0x02ab98 --------
	{0x89,0xbf},//------only for 50Hz, 60Hz maybe flicker--------
	{0x8a,0x20},//------only for 50Hz, 60Hz maybe flicker--------//___50Hz ratio is 15, min FPS is 100/12 = 8.3___

	{0x88,0x06},//max exposure time___min frame rate___0x3a98*30=0x30d4*36=0x06ddd0
	{0x89,0xdd}, //___change this for anti-flicker___
	{0x8a,0xd0},//___50Hz ratio is 20, min FPS is 100/30 = 120/36 =3.3___

	//{0x88,0x07},//max exposure time___min frame rate___0x3a98*30=0x30d4*36=0x06ddd0
	//{0x89,0x8d}, //___change this for anti-flicker___
	//{0x8a,0x98},//___50Hz ratio is 20, min FPS is 100/33=3___

	{0x8b,0x3a},//50Hz exposure time step
	{0x8c,0x98},

	{0x8d,0x30},//60Hz exposure time step
	{0x8e,0xd4},

	{0x8f,0xc4},
	{0x90,0x68},
	{0x9c,0x01},
	{0x9d,0x90},
	{0x9e,0x00},
	{0x9f,0xc8},
	{0xb0,0x24},
	{0xb1,0x24},
	{0xb2,0xb0},
	{0xb3,0x24},
	{0xb4,0x24},
	{0xb5,0x59},
	{0xb6,0x3e},
	{0xb7,0x35},
	{0xb8,0x31},
	{0xb9,0x2e},
	{0xba,0x2c},
	{0xbb,0x2b},
	{0xbc,0x2a},
	{0xbd,0x29},
	{0xc0,0x14},
	{0xc3,0x59},
	{0xc4,0x55},

	// ______AWB setting____
	{0x03,0x22},//page 22
	{0x10,0x6a},
	{0x11,0x2e},
	{0x38,0x12},
	{0x40,0xe3},
	{0x41,0xaa},
	{0x42,0x44},
	{0x46,0x0a},
	{0x80,0x38},
	{0x81,0x20},
	{0x82,0x30},
	{0x83,0x5a},
	{0x84,0x1a},
	{0x85,0x58},
	{0x86,0x24},
	{0x87,0x5a},
	{0x88,0x34},
	{0x89,0x40},
	{0x8a,0x2A},
	{0x8f,0x58},
	{0x90,0x54},
	{0x91,0x50},
	{0x92,0x4b},
	{0x93,0x46},
	{0x94,0x45},
	{0x95,0x44},
	{0x96,0x43},
	{0x97,0x3E},
	{0x98,0x30},
	{0x99,0x23},
	{0x9a,0x16},
	{0x9b,0x07},
	{0x10,0xea},

	// ______AE setting____
	{0x03,0x20},//page 20
	{0x10,0xcc},// 0x9c-- 50Hz; 0xcc---auto detect
	{0x01,0xf0},
};

t_RegSettingBB code gc_YACBAA0S_Scale_Setting[] =
{
	{0x03, 0x13}, //Page mode 13
	{0x83, 0x5c}, //disable edge 2nd

	{0x03, 0x18}, //Page mode 18
	{0x10, 0x05}, //enable scaling
	{0x20, 0x01},
	{0x21, 0x80},
	{0x22, 0x01},
	{0x23, 0x20},
	{0x2c, 0x0d},
	{0x2d, 0x55},
	{0x2e, 0x0d},
	{0x2f, 0xff},
	{0x30, 0x4e},
};

t_RegSettingBB code gc_YACBAA0S_NonScale_Setting[] =
{
	{0x03, 0x13},//Page mode 13
	{0x83, 0x5d},
	{0x03, 0x18},//Page mode 18
	{0x10, 0x00},
};

static OV_FpsSetting_t*  GetHynixFpsSetting(U8 Fps, OV_FpsSetting_t staHynixFpsSetting[], U8 byArrayLen)
{
	U8 i;
	U8 Idx;

	if (g_bIsHighSpeed)
	{
		Idx=2; //8fps for uxga, 10 fps for vga
	}
	else
	{
		Idx = 0;
	}

	for(i=0; i< byArrayLen; i++)
	{
		if(staHynixFpsSetting[i].byFps ==Fps)
		{
			Idx=i;
			break;
		}
	}
	return &staHynixFpsSetting[Idx];
}

//this function is also called during Probe stage to calculate the transfer_size/LWM/HWM
void GetSensorPclkHsync(U16 wSensorSPFormat,U8 byFps)
{
	OV_FpsSetting_t *pSensor_FpsSetting;

	wSensorSPFormat = wSensorSPFormat; // for delete warning

	pSensor_FpsSetting=GetHynixFpsSetting(byFps, g_staYACBAA0SFpsSetting, sizeof(g_staYACBAA0SFpsSetting)/sizeof(OV_FpsSetting_t));
	g_wSensorHsyncWidth = pSensor_FpsSetting->wExtraDummyPixel + 640; //1361; // this for manual exposure
	g_dwPclk= pSensor_FpsSetting->dwPixelClk;
}

void YACBAA0SSetFormatFps(U8 SetFormat, U8 byFps)
{
	OV_FpsSetting_t *pSensor_FpsSetting;

	SetFormat = SetFormat;

	pSensor_FpsSetting=GetHynixFpsSetting(byFps, g_staYACBAA0SFpsSetting, sizeof(g_staYACBAA0SFpsSetting)/sizeof(OV_FpsSetting_t));

	WriteSensorSettingBB(sizeof(gc_YACBAA0S_Setting)>>1, gc_YACBAA0S_Setting);//sensor common set

	Write_SenReg(0x03, 0x00);//page 0
	Write_SenReg(0x11, 0x90);//90->variation,    94->fix
	Write_SenReg(0x12, pSensor_FpsSetting->byClkrc); // write CLKRC

	Write_SenReg(0x40, INT2CHAR(pSensor_FpsSetting->wExtraDummyPixel,1)); //hsync_high byte.
	Write_SenReg(0x41, INT2CHAR(pSensor_FpsSetting->wExtraDummyPixel,0)); //hsync_low byte
	Write_SenReg(0x42, 0x00);//vsync_high byte
	Write_SenReg(0x43, 0x04);//Vsync_low byte

	//-------add for anti-flicker under low freq PCLK
	if(byFps < 10)//Fps < 10, PCLK = 6MHz, new_step= step/4
	{
		Write_SenReg(0x03, 0x20);//page 20
		Write_SenReg(0x8b, 0x1d);//0x0e);//50Hz
		Write_SenReg(0x8c, 0x4c);//0xa6);
		Write_SenReg(0x8d, 0x18);//0x0c);//60Hz
		Write_SenReg(0x8e, 0x6a);//0x35);
	}
	else //Fps > = 10, PCLK = 24MHz
	{
		Write_SenReg(0x03, 0x20);//page 20
		Write_SenReg(0x8b, 0x3a);//50Hz
		Write_SenReg(0x8c, 0x98);
		Write_SenReg(0x8d, 0x30);//60Hz
		Write_SenReg(0x8e, 0xd4);

	}
	//-------

	//g_wAECExposureRowMax = 504; //not use this param
	//g_wSensorHsyncWidth = 664+pSensor_FpsSetting->wExtraDummyPixel;
	//g_dwPclk= pSensor_FpsSetting->dwPixelClk;
	GetSensorPclkHsync(VGA_FRM,byFps);
}


void CfgYACBAA0SControlAttr(void)
{
	g_bySensorSize = SENSOR_SIZE_VGA;
	g_wSensorSPFormat = VGA_FRM;

	{
		memcpy(g_asOvCTT,gc_YACBAA0S_CTT,sizeof(gc_YACBAA0S_CTT));
	}

	{
		// AE backlight compensation parameter
		g_byOVAEW_Normal = YACBAA0S_AE_TARGET;
		g_byOVAEB_Normal = YACBAA0S_AE_TARGET;

		g_byOVAEW_BLC = g_byOVAEW_Normal + 0x20;
		g_byOVAEB_BLC = g_byOVAEB_Normal + 0x20;
	}

	{
		//g_bySV28VoltSel =  SV28_VOL_2V9;
		//	XBYTE[REG_TUNESVIO_SVA] =  SV28_VOL_2V9|SV18_VOL_1V8;
		// hemonel 2010-09-08: support 1.8V sensor IO
#if (_CHIP_ID_ & _RTS5840_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V79;
#elif (_CHIP_ID_ & _RTS5832_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V85;
#elif (_CHIP_ID_ & _RTS5829B_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V82;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V80;
#endif
	}

	{
		InitFormatFrameFps();
	}


}


#ifndef _USE_BK_HSBC_ADJ_
void SetYACBAA0SBrightness(S16 swSetValue)
{

	return;
}


void	SetYACBAA0SContrast(U16 wSetValue)
{

	return;
}

void	SetYACBAA0SSaturation(U16 wSetValue)
{

	return;
}

void	SetYACBAA0SHue(S16 swSetValue)
{

	return;
}
#endif

void	SetYACBAA0SSharpness(U8 bySetValue)
{
	U8 bySharpLowerLimit;//, bySharpUpperLimit;

	switch (bySetValue)
	{
	case 0:
		bySharpLowerLimit = 0x00;
		//bySharpUpperLimit = 0x00;
		break;
	case 1:
		bySharpLowerLimit = 0x03;
		//bySharpUpperLimit = 0x03;
		break;
	case 2:
		bySharpLowerLimit = 0x05;
		//bySharpUpperLimit = 0x05;
		break;
	case 3:
		bySharpLowerLimit = 0x07;
		//bySharpUpperLimit = 0x07;
		break;
	case 4:
		bySharpLowerLimit = 0x10;
		//bySharpUpperLimit = 0x10;
		break;
	case 5:
		bySharpLowerLimit = 0x18;
		//bySharpUpperLimit = 0x18;
		break;
	case 6:
		bySharpLowerLimit = 0x20;
		//bySharpUpperLimit = 0x20;
		break;
	default:
		bySharpLowerLimit = 0x28;
		//bySharpUpperLimit = 0x28;
		break;
	}

	//only set 0x90 2nd edgement, don't set 0x20 1th edgement(noise too much)
	Write_SenReg(0x03, 0x13);
	Write_SenReg_Mask(0x90, bySharpLowerLimit, 0x3F);
	Write_SenReg_Mask(0x91, bySharpLowerLimit, 0x3F);

}

void SetYACBAA0SGain(float fGain)
{
}

void SetYACBAA0SImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.
{
	/***0x00 normal
		0x01 mirror
		0x02 flip
		0x03 mirror and flip
	***/
	Write_SenReg(0x03, 0x00);
	Write_SenReg_Mask(0x11, bySnrImgDir, 0x03);
	Write_SenReg(0x03, 0x02);
	Write_SenReg_Mask(0x1a, ~((bySnrImgDir &0x01)<<4), 0x10);
}


void SetYACBAA0SOutputDim(U16 wWidth,U16 wHeight)
{
	U8 byVmode;
	U16 byBkWinWidth = 0;

	wWidth = wWidth;

	switch (wHeight)
	{
	case 120:
		byVmode = 0x20;
		WriteSensorSettingBB(sizeof(gc_YACBAA0S_NonScale_Setting)>>1, gc_YACBAA0S_NonScale_Setting);
		break;
	case 144:
		byVmode = 0x10;
		//Write_SenReg(0x03, 0x00); //Page mode 0 //____use window___
		//Write_SenReg(0x21, ((480-288)/2 + 5)); // start x
		//Write_SenReg(0x23, ((640-352)/2 + 7)); // start y
		//Write_SenReg(0x24, 0x01); //height
		//Write_SenReg(0x25, 0x20);
		//Write_SenReg(0x26, 0x01); //width
		//Write_SenReg(0x27, 0x60);

		byBkWinWidth = (376-352)/2; //_______use scaling,then crop_____
		WriteSensorSettingBB(sizeof(gc_YACBAA0S_Scale_Setting)>>1, gc_YACBAA0S_Scale_Setting);
		break;
	case 240:
		byVmode = 0x10;
		WriteSensorSettingBB(sizeof(gc_YACBAA0S_NonScale_Setting)>>1, gc_YACBAA0S_NonScale_Setting);
		break;
	case 288:
		byVmode = 0x00;
		//Write_SenReg(0x03, 0x00); //Page mode 0//____use window___
		//Write_SenReg(0x21, ((480-288)/2 + 5)); // start x
		//Write_SenReg(0x23, ((640-352)/2 + 7)); // start y
		//Write_SenReg(0x24, 0x01); //height
		//Write_SenReg(0x25, 0x20);
		//Write_SenReg(0x26, 0x01); //width
		//Write_SenReg(0x27, 0x60);

		byBkWinWidth = (376-352);//_______use scaling,then crop_____
		WriteSensorSettingBB(sizeof(gc_YACBAA0S_Scale_Setting)>>1, gc_YACBAA0S_Scale_Setting);
		break;
	case 480:
	default:
		byVmode = 0x00;
		WriteSensorSettingBB(sizeof(gc_YACBAA0S_NonScale_Setting)>>1, gc_YACBAA0S_NonScale_Setting);
		break;
	}

	Write_SenReg(0x03, 0x00); //Page mode 0
	Write_SenReg_Mask(0x10, byVmode, 0x30);

	Write_SenReg(0x03, 0x20); //Page mode 20 // for Hynix AE bug  50Hz
	Write_SenReg(0x83, 0x00);
	Write_SenReg(0x84, 0x75);
	Write_SenReg(0x85, 0x30);

//	XBYTE[CCS_HSYNC_TCTL0] = byBkWinWidth;
	SetBkWindowStart(byBkWinWidth, 0);
}


void SetYACBAA0SPwrLineFreq(U8 byFPS, U8 byLightFrq)
{
	U8 byPwrSel;
	byFPS = byFPS;

	if (PWR_LINE_FRQ_50==byLightFrq)
	{
		byPwrSel = 0x9c;
	}
	else if (PWR_LINE_FRQ_60==byLightFrq)
	{
		byPwrSel = 0x8c;
	}
	else //PWR_LINE_FRQ_DIS --- auto de-banding
	{
		byPwrSel = 0xcc;//0xcc;
	}
	Write_SenReg(0x03, 0x20); //Page mode 20
	Write_SenReg_Mask(0x10, 0x00, 0x0f); // low nibble must set 0 first
	Write_SenReg(0x10, byPwrSel);
}


void SetYACBAA0SWBTemp(U16 wRgain, U16 wGgain, U16 wBgain)
{
//	OV_CTT_t ctt;

//	ctt = OV_SetColorTemperature(wSetValue);

	Write_SenReg(0x03, 0x22);
	Write_SenReg(0x80, wRgain); // 趨 勢 對，但 單位 色溫 不對.  R'(6bit) = 1/2R(RT)
	Write_SenReg(0x81, wGgain);
	Write_SenReg(0x82, wBgain);// color temperature range map
}

/*
OV_CTT_t GetYACBAA0SAwbGain(void)
{
	OV_CTT_t ctt;

	Write_SenReg(0x03, 0x22);//page 22
	Read_SenReg(0x80, &ctt.wRgain);
	Read_SenReg(0x81, &ctt.wGgain);
	Read_SenReg(0x82, &ctt.wBgain);

	return ctt;
}
*/
void SetYACBAA0SWBTempAuto(U8 bySetValue)
{
	if(bySetValue)
	{
		// atuo white balance
		Write_SenReg(0x03, 0x22);
		Write_SenReg_Mask(0x10, 0x80,0x80);
		Write_SenReg_Mask(0x11, 0x06,0x07);
	}
	else
	{
		// manual white balance
		Write_SenReg(0x03, 0x22);
		Write_SenReg_Mask(0x10, 0x00,0x80);
		Write_SenReg_Mask(0x11, 0x00,0x07);
	}
}


void SetYACBAA0SBackLightComp(U8 bySetValue)
{
	U8 byAEtarg;

//	if(bySetValue==2)
	{
//		byAEtarg = 0x84;
	}
//	else if(bySetValue==1)
	if(bySetValue)
	{
		byAEtarg = 0x5D;
	}
	else
	{
		byAEtarg = 0x42;
	}
	Write_SenReg(0x03, 0x20);
	Write_SenReg(0x70, byAEtarg);

}

U16 GetYACBAA0SAEGain()
{
	U16 wAG;
	Write_SenReg(0x03, 0x20);
	Read_SenReg(0xb0, &wAG);
	return wAG;
}

void SetYACBAA0SIntegrationTimeAuto(U8 bySetValue)
{
	U16 wTmp;
	Write_SenReg(0x03, 0x20);	// write page address

	if(bySetValue == EXPOSURE_TIME_AUTO_MOD_APERTPRO)
	{
		// auto exposure
		Read_SenReg(0x10, &wTmp);
		Write_SenReg_Mask(0x10, 0x00,  0x8F);
		Write_SenReg_Mask(0x10, (0x80|(U8)wTmp), 0x8f); // for Hynix AE bug_20091015

		//Write_SenReg_Mask(0x10, 0x80,  0x80);

	}
	else
	{
		// manual expsoure
		Write_SenReg_Mask(0x10, 0x00,  0x80);
	}
}

void SetYACBAA0SIntegrationTime(U32 dwSetValue)
{
	U32 dwExpTime;
	if (dwSetValue < 200)  // for Hynix AE bug_20091015
	{
		dwSetValue = 200;
	}
	else if (dwSetValue > 8000)
	{
		dwSetValue = 8000;
	}
	//dwExpTime  =  (dwSetValue*100000/8) * (g_dwPclk/1000000000); //unit: ns; dwsetvalue unit: 100us
	dwExpTime  =  (dwSetValue * (g_dwPclk/10000)) /8;

	Write_SenReg(0x03, 0x20);	// write page address

	// write exposure time to register
	Write_SenReg(0x83, LONG2CHAR(dwExpTime, 2));
	Write_SenReg(0x84, LONG2CHAR(dwExpTime, 1));
	Write_SenReg(0x85, LONG2CHAR(dwExpTime, 0));
}
#endif

