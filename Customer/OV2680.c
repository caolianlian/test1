#include "Inc.h"


#ifdef RTS58XX_SP_OV2680

void InitOV2680IspParamByFormat(SetFormat);

/*
*********************************************************************************************************
*                                 manual white balance control parameter
*
*	1st column: color-temperature. color-temperature must be from little to big
*	2nd column: Rgain
*	3rd column: Ggain
*	4th column: Bgain
*
*	this array used for manual white balance control, these data are get from Judge II light box
*
*********************************************************************************************************
*/
OV_CTT_t code gc_OV2680_CTT[3] =
{
	{3000,0xEF,0x100,0x2AC},
	{4150,0x17C,0x100,0x226},
	{6500,0x1BF,0x100,0x122},
};

code OV_FpsSetting_t  g_staOV2680UXGAFpsSetting[]=
{
	// FPS ExtraDummyPixel clkrc 	   pclk
	{3,		(5666),			(0x02), 		22000000},	
	{5,		(3400),			(0x02), 		22000000},
	{8,		(3186),			(0x01), 		33000000},
	{9,		(2832),			(0x01), 		33000000},
	{10,		(2550),			(0x01), 		33000000},
	{15,		(1700),			(0x01), 		33000000},
	{20,		(2550),			(0x00), 		66000000},
	{25,		(2040),			(0x00),		66000000},
	{30,		(1700),			(0x00),		66000000},
};

code OV_FpsSetting_t  g_staOV2680HD720PFpsSetting[]=
{
	// FPS ExtraDummyPixel clkrc 	   pclk
	{3,		(9648),			(0x02), 		22000000},	
	{5,		(5788),			(0x02), 		22000000},
	{8,		(5426),			(0x01), 		33000000},
	{9,		(4824),			(0x01), 		33000000},
	{10,		(4342),			(0x01), 		33000000},
	{15,		(2894),			(0x01), 		33000000},
	{20,		(4342),			(0x00), 		66000000},
	{25,		(3472),			(0x00),		66000000},
	{30,		(2894),			(0x00),		66000000},
};

t_RegSettingWB code gc_OV2680_UXGA_Setting[] =
{
	//{0x0103 ,0x01},	// soft reset
	{0x3002 ,0x00},	// PAD EN
	{0x3016 ,0x1c},	// mipi dphy
	{0x3018 ,0x44},	// raw10
	{0x3020 ,0x00},
	{0x3080 ,0x02},	//PLL Prediv------------->24Mhz/Prediv*multi/(1+sysclkdiv)(1+spdiv) = sys_clk
	{0x3082 ,0x37},	//PLL multi
	{0x3084 ,0x09},	//PLL sys clk div
	{0x3085 ,0x04},	//PLL dac div
	{0x3086 ,0x00},	//PLL sp div
	{0x3501 ,0x4e},
	{0x3502 ,0xe0},
	{0x3503 ,0x13},	//0x13 gain delay 1 frame
	{0x350b ,0x36},
	{0x3600 ,0xb4},
	{0x3603 ,0x39},
	{0x3604 ,0x24},
	{0x3605 ,0x00},
	{0x3620 ,0x26},
	{0x3621 ,0x37},
	{0x3622 ,0x04},
	{0x3628 ,0x00},
	{0x3705 ,0x3c},
	{0x370c ,0x50},
	{0x370d ,0xc0},
	{0x3718 ,0x88},
	{0x3720 ,0x00},
	{0x3721 ,0x00},
	{0x3722 ,0x00},
	{0x3723 ,0x00},
	{0x3738 ,0x00},
	{0x370a ,0x21},
	{0x3717 ,0x58},
	{0x3781 ,0x80},
	{0x3789 ,0x60},
	{0x3800 ,0x00},	// x addr start
	{0x3801 ,0x00},
	{0x3802 ,0x00},	// y addr start
	{0x3803 ,0x00},
	{0x3804 ,0x06},	// x addr end	= 1615
	{0x3805 ,0x4f},
	{0x3806 ,0x04},	// y addr end   = 1215
	{0x3807 ,0xbf},
	{0x3808 ,0x06},	// x output size = 1604
	{0x3809 ,0x44},
	{0x380a ,0x04},	// y output size = 1201
	{0x380b ,0xb1},
	{0x380c ,0x06},	// HTS = 1700
	{0x380d ,0xa4},
	{0x380e ,0x05},	// VTS = 1294
	{0x380f ,0x0e},
	{0x3810 ,0x00},	// ISP X WIN OFFSET
	{0x3811 ,0x08},
	{0x3812 ,0x00},	// ISP Y WIN OFFSET
	{0x3813 ,0x08},
	{0x3814 ,0x11},	// X INC
	{0x3815 ,0x11},	// Y INC
	{0x3819 ,0x04},
	{0x3820 ,0xc0},
	{0x3821 ,0x00},
	{0x4000 ,0x81},
	{0x4001 ,0x40},
	{0x4008 ,0x02},
	{0x4009 ,0x09},
	{0x4602 ,0x02},
	{0x481f ,0x36},
	{0x4825 ,0x36},
	{0x4837 ,0x18},
	{0x5002 ,0x30},
	{0x5080 ,0x00},
	{0x5081 ,0x41},
	{0x0100 ,0x01},	// Streaming on
	{0x0101 ,0x01},
	{0x1000 ,0x01},
	{0x0129 ,0x10},
};

t_RegSettingWB code gc_OV2680_HD720P_Setting[] =
{
	//{0x0103 ,0x01},
	{0x3002 ,0x00},
	{0x3016 ,0x1c},
	{0x3018 ,0x44},
	{0x3020 ,0x00},
	{0x3080 ,0x02},	//PLL Prediv------------->24Mhz/Prediv*multi/(1+sysclkdiv)(1+spdiv) = sys_clk
	{0x3082 ,0x37},	//PLL multi
	{0x3084 ,0x09},	//PLL sys clk div
	{0x3085 ,0x04},	//PLL dac div
	{0x3086 ,0x00},	//PLL sp div
	{0x3501 ,0x2d},
	{0x3502 ,0x80},
	{0x3503 ,0x13},	//0x13 gain delay 1 frame
	{0x350b ,0x36},
	{0x3600 ,0xb4},
	{0x3603 ,0x39},
	{0x3604 ,0x24},
	{0x3605 ,0x00},
	{0x3620 ,0x26},
	{0x3621 ,0x37},
	{0x3622 ,0x04},
	{0x3628 ,0x00},
	{0x3705 ,0x3c},
	{0x370c ,0x50},
	{0x370d ,0xc0},
	{0x3718 ,0x88},
	{0x3720 ,0x00},
	{0x3721 ,0x00},
	{0x3722 ,0x00},
	{0x3723 ,0x00},
	{0x3738 ,0x00},
	{0x370a ,0x21},
	{0x3717 ,0x58},
	{0x3781 ,0x80},
	{0x3789 ,0x60},
	{0x3800 ,0x00},	// x addr start = 160
	{0x3801 ,0xa0},
	{0x3802 ,0x00},	// y addr start = 242
	{0x3803 ,0xf2},
	{0x3804 ,0x05},	// x addr end	= 1455
	{0x3805 ,0xaf},
	{0x3806 ,0x03},	// y addr end   = 973
	{0x3807 ,0xcd},
	{0x3808 ,0x05},	// x output size = 1284
	{0x3809 ,0x04},
	{0x380a ,0x02},	// y output size = 721
	{0x380b ,0xd1},
	{0x380c ,0x0B},	// HTS = 2894
	{0x380d ,0x4E},
	{0x380e ,0x02},	// VTS = 760
	{0x380f ,0xf8},
	{0x3810 ,0x00},	// ISP X WIN OFFSET
	{0x3811 ,0x08},
	{0x3812 ,0x00},	// ISP Y WIN OFFSET
	{0x3813 ,0x06},
	{0x3814 ,0x11},	// X INC
	{0x3815 ,0x11},	// Y INC
	{0x3819 ,0x04},
	{0x3820 ,0xc0},
	{0x3821 ,0x00},
	{0x4000 ,0x81},
	{0x4001 ,0x40},
	{0x4008 ,0x02},
	{0x4009 ,0x09},
	{0x4602 ,0x02},
	{0x481f ,0x36},
	{0x4825 ,0x36},
	{0x4837 ,0x18},
	{0x5002 ,0x30},
	{0x5080 ,0x00},
	{0x5081 ,0x41},
	{0x0100 ,0x01},
	{0x0101 ,0x01},
	{0x1000 ,0x01},
	{0x0129 ,0x10},
};


/*
*********************************************************************************************************
*                                         Find FPS Setting pointer
* FUNCTION GetOvFpsSetting
*********************************************************************************************************
*/
/**
  Find FPS setting pointer in FPS setting array. These setting will be writen to sensor register so that sensor will preview with the
  FPS. This function is called by OV2710SetFormatFps() function.

  \param
  	Fps 		- the frame rate to be found
  	staOvFpsSetting        - the FPS setting array pointer
  	byArrayLen        - the length of the FPS setting array

  \retval
  	If find, return	the expected FPS setting pointer.
  	If not find, use default FPS setting pointer.
 *********************************************************************************************************
*/
static OV_FpsSetting_t*  GetOvFpsSetting(U8 Fps, OV_FpsSetting_t staOvFpsSetting[], U8 byArrayLen)
{
	U8 i;
	U8 Idx;

	ISP_MSG((" array size = %bd\n", byArrayLen));

	if(g_bIsHighSpeed)
	{
		Idx=2; //8fps for uxga, 10 fps for vga
	}
	else
	{
		Idx = 0;
	}

	for(i=0; i< byArrayLen; i++)
	{
		ISP_MSG((" fps i = %bd\n",i));
		if(staOvFpsSetting[i].byFps ==Fps)
		{
			Idx=i;
			break;
		}
	}
	ISP_MSG((" get fps idx = %bd\n",Idx));
	return &staOvFpsSetting[Idx];
}

static U16 MapSnrGlbGain2SnrRegSetting(U16 wGain)
{
	U16  wSensorGain=0;
	
	if(wGain>248)    //Max Analog Gain here 248=15.5*16
	{
		wSensorGain = 0xF8;
	}
	else
	{
		wSensorGain = wGain;
	}
	
	return wSensorGain;
}

float SnrRegSetting2SnrGlbGain(U16 wSnrRegGain)
{
	return ((float)wSnrRegGain/16);
}

//this function is also called during Probe stage to calculate the transfer_size/LWM/HWM
void GetSensorPclkHsync(U16 wSensorSPFormat,U8 byFps)
{
	OV_FpsSetting_t *pOv2680_FpsSetting;

	if(wSensorSPFormat == HD720P_FRM)
	{
		pOv2680_FpsSetting=GetOvFpsSetting(byFps, g_staOV2680HD720PFpsSetting, sizeof(g_staOV2680HD720PFpsSetting)/sizeof(OV_FpsSetting_t));
	}
	else//HD1080P_FRM
	{
		pOv2680_FpsSetting=GetOvFpsSetting(byFps, g_staOV2680UXGAFpsSetting, sizeof(g_staOV2680UXGAFpsSetting)/sizeof(OV_FpsSetting_t));
	}
	
	g_wSensorHsyncWidth = pOv2680_FpsSetting->wExtraDummyPixel; // this for manual exposure
	g_dwPclk = pOv2680_FpsSetting->dwPixelClk;		// this for scale speed
}

void OV2680SetFormatFps(U16 SetFormat, U8 Fps)
{
	OV_FpsSetting_t *pOv2680_FpsSetting;
	U16 wTemp;

	if((g_wSensorSPFormat & SetFormat) == HD720P_FRM)
	{
		// initial all register setting
		WriteSensorSettingWB(sizeof(gc_OV2680_HD720P_Setting)/3, gc_OV2680_HD720P_Setting);
		// write fps setting
		pOv2680_FpsSetting=GetOvFpsSetting(Fps, g_staOV2680HD720PFpsSetting, sizeof(g_staOV2680HD720PFpsSetting)/sizeof(OV_FpsSetting_t));
		Write_SenReg_Mask(0x3086, pOv2680_FpsSetting->byClkrc, 0x0f);
		wTemp = pOv2680_FpsSetting->wExtraDummyPixel;
		Write_SenReg(0x380C, INT2CHAR(wTemp, 1)); //Write dummy pixel MSB
		Write_SenReg(0x380D, INT2CHAR(wTemp, 0)); //Write dummy pixel LSB

		GetSensorPclkHsync(HD720P_FRM,Fps);
		g_wAECExposureRowMax = 746;	//hemonel 2010-12-09: We use 7. At OV2710 When porting using manual exp control, the max exposure, at least should subtract 6
		g_wAEC_LineNumber = 760;	// this for AE insert dummy line algothrim	

		g_wSensorWidthBefBLC = 1280;
		g_wSensorHeightBefBLC = 720;
	}
	else
	{
		// initial all register setting
		WriteSensorSettingWB(sizeof(gc_OV2680_UXGA_Setting)/3, gc_OV2680_UXGA_Setting);
		// write fps setting
		pOv2680_FpsSetting=GetOvFpsSetting(Fps, g_staOV2680UXGAFpsSetting, sizeof(g_staOV2680UXGAFpsSetting)/sizeof(OV_FpsSetting_t));
		Write_SenReg_Mask(0x3086, pOv2680_FpsSetting->byClkrc, 0x0F);
		wTemp = pOv2680_FpsSetting->wExtraDummyPixel;
		Write_SenReg(0x380C, INT2CHAR(wTemp, 1)); //Write dummy pixel MSB
		Write_SenReg(0x380D, INT2CHAR(wTemp, 0)); //Write dummy pixel LSB

		GetSensorPclkHsync(HD1080P_FRM,Fps);
		g_wAECExposureRowMax = 1280;
		g_wAEC_LineNumber = 1294;	// this for AE insert dummy line algothrim		

		g_wSensorWidthBefBLC = 1600;
		g_wSensorHeightBefBLC = 1200;

	}

	InitOV2680IspParamByFormat(SetFormat);
#ifdef _MIPI_EXIST_
	//-----------------------------Dphy Setting--------------------------------
	SetMipiDphy(MIPI_DATA_LANE0_EN, \
	            MIPI_DATA_FORMAT_RAW10, MIPI_DATA_TYPE_RAW10, \
	            HSTERM_EN_TIME_33);
#endif

}

void CfgOV2680ControlAttr(void)
{
	U8 i;

	g_bySensorSize = SENSOR_SIZE_UXGA;
	g_wSensorSPFormat =UXGA_FRM|HD720P_FRM;

	{
		memcpy(g_asOvCTT, gc_OV2680_CTT,sizeof(gc_OV2680_CTT));
	}

	{
#if (_CHIP_ID_ & _RTS5840_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V79;
#elif (_CHIP_ID_ & _RTS5832_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V85;
#elif (_CHIP_ID_ & _RTS5829B_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V82;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V80;
#endif
	}

//	if(g_byFormatCfgExist==0) // format config not exist, use default setting.
	{


		//====== resolution  setting ===========
		// -- preview ---
		g_aVideoFormat[0].byaVideoFrameTbl[0] = 12;		// resolution number
		g_aVideoFormat[0].byaVideoFrameTbl[1]=F_SEL_640_480;
		g_aVideoFormat[0].byaVideoFrameTbl[2]=F_SEL_320_180;
		g_aVideoFormat[0].byaVideoFrameTbl[3]=F_SEL_320_240;
		g_aVideoFormat[0].byaVideoFrameTbl[4]=F_SEL_352_288;
		g_aVideoFormat[0].byaVideoFrameTbl[5]=F_SEL_424_240;
		g_aVideoFormat[0].byaVideoFrameTbl[6]=F_SEL_640_360;
		g_aVideoFormat[0].byaVideoFrameTbl[7]=F_SEL_640_400;
		g_aVideoFormat[0].byaVideoFrameTbl[8]=F_SEL_848_480;
		g_aVideoFormat[0].byaVideoFrameTbl[9]=F_SEL_960_540;
		g_aVideoFormat[0].byaVideoFrameTbl[10]=F_SEL_1280_720;
		g_aVideoFormat[0].byaVideoFrameTbl[11]=F_SEL_1280_800;
		g_aVideoFormat[0].byaVideoFrameTbl[12]=F_SEL_1600_1200;
		//--still image--
		g_aVideoFormat[0].byaStillFrameTbl[0] = 5;	// resolution number
		g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_1600_1200;
		g_aVideoFormat[0].byaStillFrameTbl[2]=F_SEL_1280_720;
		g_aVideoFormat[0].byaStillFrameTbl[3]=F_SEL_320_240;
		g_aVideoFormat[0].byaStillFrameTbl[4]=F_SEL_352_288;
		g_aVideoFormat[0].byaStillFrameTbl[5]=F_SEL_640_480;

		//====== fps  setting ===========
		for(i=0; i<10; i++)
		{
			/*640_480,
			160_120,
			176_144,
			320_200,
			320_240,
			352_288,
			640_400*/
			g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_15|FPS_20|FPS_25|FPS_30;
		}

		for(i=10; i<13; i++)
		{

			g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_20|FPS_10|FPS_8|FPS_5;
		}
		
		for(i=13; i<16; i++)
		{

			g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_8|FPS_5;
		}

		for(i=16; i<20; i++)
		{
			g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_3 |FPS_5;
		}

		//====== format type  setting ===========
		g_aVideoFormat[0].byFormatType = FORMAT_TYPE_YUY2;

#ifdef _ENABLE_MJPEG_
		// copy YUY2 setting to MJPEG setting
		memcpy(&(g_aVideoFormat[1]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

		// modify MJPEG FPS setting
		for(i=0;i<20;i++)
		{
			/*
			 F_SEL_800_600    (7)
			 F_SEL_960_720    (8)
			 F_SEL_1024_768   (9)
			 F_SEL_1280_720   (10)
			 F_SEL_1280_800   (11)
			 F_SEL_1280_960   (12)
			 F_SEL_1280_1024  (13)
			 F_SEL_1600_1200  (14)
			 F_SEL_1920_1080  (15)
			 F_SEL_1600_900	 (16)
			 */
			g_aVideoFormat[1].waFrameFpsBitmap[i] = (FPS_10|FPS_15|FPS_20|FPS_25|FPS_30);
		}		
		//for(i=11;i<17;i++)
		//{
		//	g_aVideoFormat[1].waFrameFpsBitmap[i] = (FPS_5|FPS_10|FPS_15);
		//}
		// modify MJPEG format type
		g_aVideoFormat[1].byFormatType = FORMAT_TYPE_MJPG;

#endif

#ifdef _ENABLE_M420_FMT_
		// copy YUY2 setting to M420 setting
		memcpy(&(g_aVideoFormat[2]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

		// modify M420 FPS setting
		{
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_640_480] =FPS_25|FPS_20|FPS_15|FPS_10|FPS_5;//|FPS_1;             //bit07 F_800_600
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_960_540] =FPS_25|FPS_20|FPS_15|FPS_10|FPS_5;//|FPS_5|FPS_3|FPS_1;                    //bit08 F_960_720
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1024_768] =FPS_15|FPS_10|FPS_5;//|FPS_1;                    //bit09 F_1024_768
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_720] =FPS_10|FPS_5;//FPS_9|FPS_8|FPS_1;    //bit10 F_1280_720
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_800] =FPS_10|FPS_5;
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1920_1080] =FPS_5;
			// modify M420 format type
			g_aVideoFormat[2].byFormatType = FORMAT_TYPE_M420;
		}
#endif

	}

	{
		//Brightness - BrightnessItem

		//Contrast - ContrastItem

		//Saturation - SaturationItem

		//Hue - 	HueItem

		//Sharpness - SharpnessItem
		//	SharpnessItem.Min = 0;
		//	SharpnessItem.Max = 8;
		//	SharpnessItem.Res = 1;
		//	SharpnessItem.Def = 3;
		//	SharpnessItem.Last  =SharpnessItem.Def;

		// gamma - 	GammaItem

		// gain - GainItem

		// whitebalance - WhiteBalanceTempItem

		// backlight - BackLightCompItem

		// powerline -PwrLineFreqItem

		// exposure time -	ExposureTimeAbsolutItem

		// low light - LowLightCompItem
	}
	InitOV2680IspParams();
}

void SetOV2680IntegrationTime(U16 wEspline)
{

	U16 wFrameLenLines;
	U32 dwExposureTime;
	
	if(wEspline > g_wAECExposureRowMax)
	{
		wFrameLenLines = wEspline + 14;
	}
	else
	{
		wFrameLenLines = g_wAEC_LineNumber;
	}

	if (wFrameLenLines%2==1)
	{
		wFrameLenLines++;
	}
	
	dwExposureTime = ((U32)wEspline)<<4;

	//-----------Write  frame length/dummy lines------------
	Write_SenReg(0x380E, INT2CHAR(wFrameLenLines, 1));
	Write_SenReg(0x380F, INT2CHAR(wFrameLenLines, 0));

	//-----------Write Exposuretime setting-----------
	Write_SenReg(0x3500, LONG2CHAR(dwExposureTime, 2));	// change exposure value high
	Write_SenReg(0x3501, LONG2CHAR(dwExposureTime, 1));	// change exposure value
	Write_SenReg(0x3502, LONG2CHAR(dwExposureTime, 0));	// change exposure value low

	//----------- write gain setting-------------------
	Write_SenReg(0x350A, 0);	// fixed gain at manual exposure control
	Write_SenReg(0x350B, 0);
}

void SetOV2680Gain(float fGain)
{
}

void SetOV2680ImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.
{
	Write_SenReg_Mask(0x3820, (bySnrImgDir&0x02)<<1, 0x04);	// flip
	Write_SenReg_Mask(0x3821, (bySnrImgDir&0x01)<<2, 0x04); // mirror

	switch(bySnrImgDir)
	{
		case 0:
		default:
			SetBLCWindowStart(0, 1);
			break;
		case 1:
			SetBLCWindowStart(0, 1);
			break;
		case 2:
			SetBLCWindowStart(0, 0);
			break;
		case 3:
			SetBLCWindowStart(0, 0);
			break;
	}	
	
}

void SetOV2680Exposuretime_Gain(float fExpTime, float fTotalGain)
{

	U16  wGainRegSetting; //data
	float fSnrGlbGain;
	U32 dwExposureTime;
	U8 byTemp;
	U16 wSetDummy;
	
	wGainRegSetting=MapSnrGlbGain2SnrRegSetting((U16)(fTotalGain*16.0));
	fSnrGlbGain = SnrRegSetting2SnrGlbGain(wGainRegSetting);
	dwExposureTime = (U32)(fExpTime*16/g_fSensorRowTimes);

	// hemonel 2010-12-09: image occur white point issue
	//					OV note that exposure tline need less than one line then can use sub-line
	if(dwExposureTime> 16)
	{
		byTemp = dwExposureTime&0x0F;		
		dwExposureTime = dwExposureTime&0xFFFFFFF0;	// cut sub-line
		if(byTemp >= 8)	// round sub-line to tline
		{
			dwExposureTime+=16;
		}
	}

	// write exposure, gain, dummy line
	if(fabs(g_fCurExpTime-fExpTime) > 0.0001)
	{	
		AE_MSG((" 1-- %u, %lu, %u\n",g_wAFRInsertDummylines,dwExposureTime,wGainRegSetting));
	
		//AE_MSG(("set exposure and gain\n"));

		wSetDummy = g_wAFRInsertDummylines + g_wAEC_LineNumber;
		
		// group register hold
		// hemonel 2010-12-14: Group register hold function working need whitch video has previewed
		//				otherwise these register will not be writen into sensor register
		if(g_byStartVideo)	
		{
			Write_SenReg(0x3208, 0x00);	// Enable group0
		}
		
		//-----------Write Exposuretime setting-----------		
		Write_SenReg(0x3500, LONG2CHAR(dwExposureTime, 2));	// change exposure value high
		Write_SenReg(0x3501, LONG2CHAR(dwExposureTime, 1));	// change exposure value  
		Write_SenReg(0x3502, LONG2CHAR(dwExposureTime, 0));	// change exposure value low

		Write_SenReg(0x380e, INT2CHAR(wSetDummy, 1));
		Write_SenReg(0x380f, INT2CHAR(wSetDummy, 0));

		// group register release
		if(g_byStartVideo)
		{
			Write_SenReg(0x3208, 0x10);	// End group0
			Write_SenReg(0x3208, 0xA0);	// Launch group0
		}

		WaitFrameSync(ISP_INT1, ISP_DATA_START_INT);
		if(g_byStartVideo)	
		{
			Write_SenReg(0x3208, 0x01);	// Enable group0
		}		
		//----------- write gain setting-------------------	
		//Write_SenReg(0x350A, 0);		
		Write_SenReg(0x350B, INT2CHAR(wGainRegSetting, 0));	
		SetISPAEGain(fTotalGain, fSnrGlbGain, 1);
		// group register release
		if(g_byStartVideo)
		{
			Write_SenReg(0x3208, 0x11);	// End group0
			Write_SenReg(0x3208, 0xA1);	// Launch group0
		}
		
		g_fCurExpTime = fExpTime;	// back up exposure rows			
	}
	else
	{	
		//AE_MSG(("set gain\n"));
		AE_MSG((" 2-- %u, %lu, %u\n",g_wAFRInsertDummylines,dwExposureTime,wGainRegSetting));
		wSetDummy = g_wAFRInsertDummylines + g_wAEC_LineNumber;
		
		// group register hold
		if(g_byStartVideo)
		{
			Write_SenReg(0x3208, 0x00);	// Enable group0
		}

		Write_SenReg(0x380e, INT2CHAR(wSetDummy, 1));
		Write_SenReg(0x380f, INT2CHAR(wSetDummy, 0));

		// group register hold
		if(g_byStartVideo)
		{
			Write_SenReg(0x3208, 0x00);	// Enable group0
		}
		
		// write gain setting
		Write_SenReg(0x350A, 0);
		Write_SenReg(0x350B, INT2CHAR(wGainRegSetting, 0));

		SetISPAEGain(fTotalGain, fSnrGlbGain, 1);

		// group register release
		if(g_byStartVideo)
		{
			Write_SenReg(0x3208, 0x10);	// End group0
			Write_SenReg(0x3208, 0xA0);	// Launch group0
		}
	}	

	return;
}

void OV2680_POR()
{
	Init_MIPI();
	ENTER_SENSOR_RESET();

	// power on
	SensorPowerControl(SWITCH_ON,EN_DELAYTIME);
	uDelay(1);

	// Initialize I2C Controller Timing
	LEAVE_SENSOR_RESET();

	// clock select 24M
	if( g_bySSCEnable== TRUE)
	{
#ifdef _IC_CODE_
		CHANGE_CCS_CLK(SSC_CLK_SEL_96M|SSC_CLK_SEL_DIV4);
#else
		CHANGE_CCS_CLK(SSC_CLK_SEL_96M);
#endif
	}
	else
	{
#ifdef _IC_CODE_
		CHANGE_CCS_CLK(CCS_CLK_SEL_96M|CCS_CLK_DIVIDER_4);
#else
		CHANGE_CCS_CLK(CCS_CLK_SEL_96M|CCS_CLK_DIVIDER_4);
#endif
	}

	EnableSensorHCLK();
	g_bySensorIsOpen = SENSOR_OPEN;
	WaitTimeOut_Delay(1);  //OV2710 spec request 8192clk
}

/*
*********************************************************************************************************
*                                 	Backend ISP IQ Parameter setting
*
*********************************************************************************************************
*/


void InitOV2680IspParamByFormat(SetFormat)
{


	SetFormat = SetFormat;
	// LSC & MLSC

}

void InitOV2680IspParams()
{

	// D50 R/G/B gain
	g_wAWBRGain_Last= 0x136;
	g_wAWBGGain_Last= 0x100;
	g_wAWBBGain_Last= 0x19D;
	g_wProjectGR = g_wAWBRGain_Last;
	g_wProjectGB = g_wAWBBGain_Last;

	g_wDynamicISPEn = DYNAMIC_LSC_CT_EN | DYNAMIC_LSC_EN | DYNAMIC_SHARPNESS_EN | DYNAMIC_SHARPPARAM_EN | DYNAMIC_GAMMA_EN | DYNAMIC_CCM_CT_EN;
}


void SetOV2680DynamicISP(U8 byAEC_Gain)
{
	byAEC_Gain = byAEC_Gain;
}

void SetOV2680DynamicISP_AWB(U16 wColorTempature)
{
	wColorTempature =wColorTempature;
}
#endif
