#include "Inc.h"

#ifdef RTS58XX_SP_OV2650

code OV_CTT_t gc_OV2650_CTT[3] =
{
	{3000,0x480,0x400,0x620},
	{4050,0x500,0x400,0x540},
	{6500,0x600,0x400,0x400},
};

// hemonel 2009-09-01: modify pclk value
// column = 1940, row = 1235
code OV2650_FpsSetting_t  g_staOV2650SVGAFpsSetting[]=
{
	// FPS  		clkrc		PLL 		ExtraDummyPixel
//				{1,		(29),	(0x34), 	    	0x00},
	{3,     (9),        (0x34),         0x00 , 1800000},	// hemonel 2009-09-01: modify pclk value
	{5,     (5),        (0x34),         0x00 , 3000000},
	{10,        (2),        (0x34),     0x00 , 6000000},
	{15,        (1),        (0x34),     0x00 , 9000000},
	{20,        (0),        (0x38),     0x00 , 12000000},
	{23,        (0),        (0x36),     0xAB , 15000000},
	{25,        (0),        (0x36),     0x04 , 15000000},
	{30,        (0),        (0x34),     0x00 , 18000000},
};
code OV2650_FpsSetting_t  g_staOV2650UXGAFpsSetting[]=
{
	// FPS         clkrc       PLL         ExtraDummyPixel
//              {1,     (14),   (0x34),     0x00},
	{3,     (4),        (0x34),     0x00 , 7200000},
	{5,     (2),        (0x34),     0x00 , 12000000},
	{7,     (1),        (0x34),     0x8A , 18000000},  //15fps-7.5fps: clkrc=0->1; 7.5fps->7fps: dummy= 0->
	{8,     (0),        (0x39),     0xB6 , 21000000},
	{9,     (0),        (0x38),     0xD8 , 24000000},
	{10,    (0),        (0x38),     0x00 , 24000000},
	{11,    (0),        (0x37),     0x2E , 27000000},
	{12,    (0),        (0x36),     0x51 , 30000000},  // 15fps-> 12.5fps: PLL=0x34->0x36; 12.5fps->12fps: dummy=0x00->0x51
	{15,    (0),        (0x34),     0x00 , 36000000},
};

//----------------------------------
// intial sensor register setting
//----------------------------------
t_RegSettingWB code gc_OV2650_Setting[] =
{
	// soft reset
//	{0x3012, 0x80},

	// IO & Clock & Analog Setup
	{0x308c, 0x80},
	{0x308d, 0x0e},
	{0x360b, 0x00},
	{0x30b0, 0xff},
	{0x30b1, 0xff},
	{0x30b2, 0x24},

	//PLL
	// hemonel 2008-11-27: delete these repeated setting in scale/format setting function
//	{0x300e, 0x34},
	// end
	{0x300f, 0xa6},
	{0x3010, 0x81},
	{0x3082, 0x01},
	{0x30f4, 0x01},
	{0x3090, 0x33},
	{0x3091, 0xc0},
	{0x30ac, 0x42},

	//
	{0x30d1, 0x08},
	{0x30a8, 0x56},
	//{0x3015, 0x03},
	{0x3015, 0x53}, //night mode: allow 1 dummy lines
	{0x3093, 0x00},
	{0x307e, 0xe5},
	{0x3079, 0x00},
	{0x30aa, 0x42},
	{0x3017, 0x40},
	{0x30f3, 0x82},
	{0x306a, 0x0c},
	{0x306d, 0x00},
	{0x336a, 0x3c},
	{0x3076, 0x6a},
	{0x30d9, 0x8c},
//	{0x3016, 0x82},
	{0x3016, 0xa2}, // night mode trigger 8x: 0x92  // modify By Ban 0xa2

	{0x3601, 0x30},
	{0x304e, 0x88},
	{0x30f1, 0x82},

	// PLL/CLOCKRC
	// hemonel 2008-11-27: delete these repeated setting in scale/format setting function
//	{0x3011, 0x02},
	// end

	// AEC/AGC
	// hemonel 2008-11-27: delete these repeated setting in scale/format setting function
//	{0x3013, 0xf7},
//	{0x301c, 0x13},
//	{0x301d, 0x17},
	// end
	{0x3070, 0x3e},
	{0x3072, 0x34},

	//D5060
	{0x30af, 0x00},
	{0x3048, 0x1f},
	{0x3049, 0x4e},
	{0x304a, 0x20},
	{0x304f, 0x20},
	{0x304b, 0x02},
	{0x304c, 0x00},
	{0x304d, 0x02},
	{0x304f, 0x20},
	{0x30a3, 0x10},
//   {0x3013, 0xf7},
	//{0x3014, 0x44},
//    {0x3014, 0x4C}, // night mode enable
	// hemonel 2008-11-27: delete these repeated setting in scale/format setting function
//	{0x3071, 0x00},
//	{0x3070, 0x3e},
//	{0x3073, 0x00},
//	{0x3072, 0x34},
//	{0x301c, 0x12},
//	{0x301d, 0x16},
	// end
	{0x304d, 0x42},
	{0x304a, 0x40},
	{0x304f, 0x40},
	{0x3095, 0x07},
	{0x3096, 0x16},
	{0x3097, 0x1d},

	// window setup
	// hemonel 2008-11-27: delete these repeated setting in scale/format setting function
//	{0x300e, 0x38},
	// end
	{0x3020, 0x01},
	{0x3021, 0x18},
	{0x3022, 0x00},
	// hemonel 2008-11-27: delete these repeated setting in scale/format setting function
//	{0x3023, 0x0a},
//	{0x3024, 0x06},
//	{0x3025, 0x58},
//	{0x3026, 0x04},
//	{0x3027, 0xbc},
//	{0x3088, 0x06},
//	{0x3089, 0x40},
//	{0x308a, 0x04},
//	{0x308b, 0xb0},
//	{0x3316, 0x64},
//	{0x3317, 0x4b},
//	{0x3318, 0x00},
	// end
	{0x331a, 0x64},
	{0x331b, 0x4b},
	{0x331c, 0x00},
	{0x3100, 0x00},

	// UVadjust
	{0x3301, 0xff},
	{0x338B, 0x14},
	{0x338c, 0x09},
	{0x338d, 0x80},

	// Sharpness/De-noise
	{0x3370, 0xd0},
	{0x3371, 0x00},
	{0x3372, 0x00},
	{0x3373, 0x40},
	{0x3374, 0x10},
	{0x3375, 0x10},
	{0x3376, 0x04},
	{0x3377, 0x00},
	{0x3378, 0x04},
	{0x3379, 0x80},

	// BLC
	{0x3069, 0x84},
	{0x307c, 0x10},
	{0x3087, 0x02},

	// Other function
	{0x3300, 0xfc},
	// hemonel 2008-11-27: delete these repeated setting in scale/format setting function
//	{0x3302, 0x01},
	// end
	{0x3400, 0x00},
	{0x3606, 0x20},
	{0x3601, 0x30},
	{0x300e, 0x34},
	{0x30f3, 0x83},
	{0x304e, 0x88},
	// hemonel 2008-12-03: add brightness/contrast/hue/saturation enable
	{0x3391, 0x07},
	// end

	// Sleep on -> sleep off
//    {0x3086, 0x0f}, // hemonel 2009-11-18: delete for fast WIA capture
//   {0x3086, 0x00}, // hemonel 2009-11-18: delete for fast WIA capture

	//Auto Switch
	{0x3306, 0x88}, //disable auto sharp
//    {0x3013, 0xf7},
};

t_RegSettingWB code gc_OV2650_SVGA_Setting[] =
{
	//{0x3012, 0x10},
	//{0x30dc, 0x00},
	{0x3023, 0x06},
//	{0x300e, 0x38},
	{0x3012, 0x10},
	{0x302A, 0x02},
	{0x302B, 0x6a},
	{0x306f, 0x14},
	{0x3024, 0x06},
	{0x3025, 0x58},
	{0x3026, 0x02},
	{0x3027, 0x61},
	//{0x3088, 0x06},
	//{0x3089, 0x40},
	//{0x308a, 0x02},
	//{0x308b, 0x58},
	//{0x3100, 0x00},
	{0x3319, 0x0c},
	{0x331d, 0x4c},
	{0x3302, 0x10},
	{0x3316, 0x64},
	{0x3317, 0x25},
	{0x3318, 0x80},
	{0x3088, 0x03},
	{0x3089, 0x20},
	{0x308a, 0x02},
	{0x308b, 0x58},
//	{0x3011, 0x02},
	{0x3018, OV2650_AEW},
	{0x3019, OV2650_AEB},
	{0x301a, OV2650_VPT},
	{0x3069, 0x84},// for flip mirror bug__20091120__

};
t_RegSettingWB code gc_OV2650_UXGA_Setting[] =
{
	//{0x3012, 0x00},
	//{0x30dc, 0x00},
	{0x3023, 0x0A},
//	{0x300e, 0x34},
	{0x3012, 0x00},
	{0x302A, 0x04},
	{0x302B, 0xD4},
	{0x306f, 0x54},
	{0x3024, 0x06},
	{0x3025, 0x58},
	{0x3026, 0x04},
	{0x3027, 0xBC},
	//{0x3088, 0x06},
	//{0x3089, 0x40},
	//{0x308a, 0x04},
	//{0x308b, 0xB0},
	//{0x3100, 0x00},
	{0x3319, 0x6C},
	{0x331d, 0x6C},
	{0x3302, 0x01},
	{0x3316, 0x64},
	{0x3317, 0x4B},
	{0x3318, 0x00},
	{0x3088, 0x06},
	{0x3089, 0x40},
	{0x308a, 0x04},
	{0x308b, 0xB0},
	{0x3018, OV2650_AEW},
	{0x3019, OV2650_AEB},
	{0x301a, OV2650_VPT},
};


t_RegSettingWB code gc_OV2650_ISP_Setting[]=
{
	//OV2650_LSC CWF 100%
	//20090409---jqg
	{0x3350, 0x30}, //rx
	{0x3351, 0x25}, //ry
	{0x3352, 0x80}, //ry[3:0],rx[3:0]  //20090923 Foxlink requeset for Lens noise
	{0x3353, 0x16}, //a1 ////20090923 Foxlink requeset for Lens noise
	{0x3354, 0x00}, //b1
	{0x3355, 0x84}, //b2, a0 30

	{0x3356, 0x31}, //rx
	{0x3357, 0x25}, //ry
	{0x3358, 0x80}, //ry[3:0],rx[3:0]
	{0x3359, 0x12}, //a1 //20090923 Foxlink requeset for Lens noise
	{0x335a, 0x00}, //b1
	{0x335b, 0x84}, //b2, a0 30

	{0x335c, 0x30}, //rx
	{0x335d, 0x25}, //ry
	{0x335e, 0x82}, //ry[3:0],rx[3:0] //20090923 Foxlink requeset for Lens noise
	{0x335f, 0x12}, //a1 //20090923 Foxlink requeset for Lens noise
	{0x3360, 0x00}, //b1
	{0x3361, 0x84}, //b2, a0 30

	{0x3363, 0x70},
	{0x3364, 0x7f},
	{0x3365, 0x00},
	{0x3366, 0x00},

	//OV2650_AE
	//OV2650_AWB

	{0x3320,0x98},
	{0x3321,0x11},
	{0x3322,0x92},
	{0x3323,0x01},
//{0x3324,0x96},
	{0x3325,0x02},
	{0x3326,0xff},
	{0x3327,0x0d},
	{0x3328,0x0c},
	{0x3329,0x0f},
	{0x332a,0x5E},  // 0x59
	{0x332b,0x50},
	{0x332c,0x3b},
	{0x332d,0xba},
	{0x332e,0x3a},
	{0x332f,0x39},
	{0x3330,0x4a},
	{0x3331,0x4c},
	{0x3332,0xf0},
	{0x3333,0x0a},
	{0x3334,0xf0},
	{0x3335,0xf0},
	{0x3336,0xf0},
	{0x3337,0x40},
	{0x3338,0x40},
	{0x3339,0x40},
	{0x333a,0x00},
	{0x333b,0x00},

	// for 90%
	{0x3380, 0x20},
	{0x3381, 0x4a},
	{0x3382, 0x10},
	{0x3383, 0x16},
	{0x3384, 0xa0},
	{0x3385, 0xb7},
	{0x3386, 0x8e},
	{0x3387, 0x7f},
	{0x3388, 0xd },
	{0x3389, 0x98},
	{0x338a, 0x1 },
	// for foxlink 20090731 start
	// UVadjust
	// {0x3301, 0xff},
	// {0x338B, 0x1a}, //0x1a
	// {0x338c, 0x10}, //0x10
	// {0x338d, 0x20}, //0x20
	// {0x338e, 0x1f},
	// for foxlink 20090731 end

	//for foxlink 20090731 start
	// Sharpness/De-noise
	{0x3370, 0xff}, //0xff
	{0x3371, 0x04}, //0x04
	{0x3372, 0x00},
	{0x3373, 0x40},
	{0x3374, 0x10},
	{0x3375, 0x18}, //0x18
	{0x3376, 0x04},
	{0x3377, 0x00},
	{0x3378, 0x04},
	{0x3379, 0x70}, //0x70
	//for foxlink 20090731 end

	//for Foxlink20090731 start
	// BLC
//   {0x306c, 0x10},
	{0x3069, 0x8a}, //0x8a
	{0x307c, 0x10},
	{0x3087, 0x02},
	//for foxlink 20090731 end

	//OV2650_GAMMA for foxlink 20090803 start
	{0x3340, 0x12},
	{0x3341, 0x22},
	{0x3342, 0x37},
	{0x3343, 0x47},
	{0x3344, 0x57},
	{0x3345, 0x65},
	{0x3346, 0x71},
	{0x3347, 0x7a},
	{0x3348, 0x83},
	{0x3349, 0x92},
	{0x334a, 0x9f},
	{0x334b, 0xab},
	{0x334c, 0xc2},
	{0x334d, 0xd3},
	{0x334e, 0xe2},
	{0x334f, 0x12},
	// //OV2650_GAMMA for foxlink 20090803 end
	{0x3394, 0x38},
	{0x3395, 0x38},

//{0x3013,0xf7},

	{0x334f,0x13},
	{0x3340,0x00},
	{0x3341,0x15},
	{0x3342,0x2d},
	{0x3343,0x44},
	{0x3344,0x53},
	{0x3345,0x62},
	{0x3346,0x6f},
	{0x3347,0x78},
	{0x3348,0x81},
	{0x3349,0x91},
	{0x334a,0x9d},
	{0x334b,0xac},
	{0x334c,0xbf},
	{0x334d,0xd1},
	{0x334e,0xe0},

	{0x3366,0x00},
	{0x3301,0xff},

	{0x3380,0x1e},
	{0x3381,0x45},
	{0x3382,0x22},
	{0x3383,0x48},
	{0x3384,0x8e},
	{0x3385,0xd6},
	{0x3386,0xad},
	{0x3387,0x96},
	{0x3388,0x14},
	{0x3389,0x98},
	{0x338a,0x01},

	{0x338b,0x1a},
	{0x338c,0x10},
	{0x338d,0x20},
	{0x338e,0x1f},

	{0x3370,0xff},
	{0x3371,0x05},
	{0x3372,0x00},
	{0x3373,0x38},
	{0x3374,0x10},
	{0x3375,0x18},
	{0x3376,0x04},
	{0x3377,0x00},
	{0x3378,0x04},
	{0x3379,0x70},
	{0x3069,0xc4},
//{0x3087,0x22},


};
//find property setting
OV2650_FpsSetting_t*  GetOv2650FpsSetting(U8 Fps, OV2650_FpsSetting_t staOvFpsSetting[], U8 byArrayLen)
{
	U8 i;
	U8 Idx;

	ISP_MSG((" array size = %bd\n", byArrayLen));

	if(g_bIsHighSpeed)
	{
		Idx=2; //8fps for uxga, 10 fps for vga
	}
	else
	{
		Idx = 0;
	}

	for(i=0; i< byArrayLen; i++)
	{
		ISP_MSG((" fps i = %bd\n",i));
		if(staOvFpsSetting[i].byFps ==Fps)
		{
			Idx=i;
			break;
		}
	}
	ISP_MSG((" get fps idx = %bd\n",Idx));
	return &staOvFpsSetting[Idx];
}

//this function is also called during Probe stage to calculate the transfer_size/LWM/HWM
void GetSensorPclkHsync(U16 wSensorSPFormat,U8 byFps)
{
	OV2650_FpsSetting_t *pOv2650_FpsSetting = NULL;
	
	if(wSensorSPFormat == SVGA_FRM)
	{
		pOv2650_FpsSetting = GetOv2650FpsSetting(byFps, g_staOV2650SVGAFpsSetting, sizeof(g_staOV2650SVGAFpsSetting)/sizeof(OV2650_FpsSetting_t));
		g_wSensorHsyncWidth = 970+pOv2650_FpsSetting->byExtraDummyPixel;
	}
	else
	{
		pOv2650_FpsSetting = GetOv2650FpsSetting(byFps, g_staOV2650UXGAFpsSetting, sizeof(g_staOV2650UXGAFpsSetting)/sizeof(OV2650_FpsSetting_t));
		g_wSensorHsyncWidth = 1940+pOv2650_FpsSetting->byExtraDummyPixel;
	}
	g_dwPclk = pOv2650_FpsSetting->dwPixelClk;		// this for scale speed
}

void OV2650SetFormatFps(U16 SetFormat, U8 Fps)
{
	OV2650_FpsSetting_t *pOv2650_FpsSetting;


	// write sensor setting: UXGA and SVGA common setting
	WriteSensorSettingWB(sizeof(gc_OV2650_Setting)/3, gc_OV2650_Setting);

	// hemonel 2009-11-18: add for fast WIA capture
	Write_SenReg_Mask(0x3324, 0x96, 0xDF);
	Write_SenReg_Mask(0x3014, 0x4C, 0xF7);
	Write_SenReg_Mask(0x3013, 0xf7, 0xFA);
	if((g_wSensorSPFormat & SetFormat) == SVGA_FRM)
	{
		pOv2650_FpsSetting = GetOv2650FpsSetting(Fps, g_staOV2650SVGAFpsSetting, sizeof(g_staOV2650SVGAFpsSetting)/sizeof(OV2650_FpsSetting_t));
		// 1) change hclk if necessary

		// 2) write sensor register for format

		// SVGA
		WriteSensorSettingWB(sizeof(gc_OV2650_SVGA_Setting)/3, gc_OV2650_SVGA_Setting);
		Write_SenReg(0x3362, 0x90);//  Lens shading for SVGA
		//Write_SenReg(0x3015, 0x23);//  20090908 For MIN 15fps
		Write_SenReg(0x3015, 0x53);//  20090908 For MIN 7.5fps

		// 3) write sensor register for fps
		Write_SenReg(0x3011, pOv2650_FpsSetting->byClkrc);// write CLKRC
		Write_SenReg(0x300E, pOv2650_FpsSetting->byPLL);// write PLL
		Write_SenReg(0x302C, pOv2650_FpsSetting->byExtraDummyPixel);// write dummy pixel

		// 4) update variable for AE
		g_wAECExposureRowMax = 617;
		GetSensorPclkHsync(SVGA_FRM, Fps);
		//g_wSensorHsyncWidth = 970+pOv2650_FpsSetting->byExtraDummyPixel;
		//g_dwPclk= pOv2650_FpsSetting->dwPixelClk;
	}
	else
	{
		pOv2650_FpsSetting = GetOv2650FpsSetting(Fps, g_staOV2650UXGAFpsSetting, sizeof(g_staOV2650UXGAFpsSetting)/sizeof(OV2650_FpsSetting_t));
		// 1) change hclk if necessary

		// 2) write sensor register for format

		// UXGA
		WriteSensorSettingWB(sizeof(gc_OV2650_UXGA_Setting)/3, gc_OV2650_UXGA_Setting);
		Write_SenReg(0x3362, 0x80);// Lens shading for UXGA

		// 3) write sensor register for fps
		Write_SenReg(0x3011, pOv2650_FpsSetting->byClkrc);// write CLKRC
		Write_SenReg(0x300E, pOv2650_FpsSetting->byPLL);// write PLL
		Write_SenReg(0x302C, pOv2650_FpsSetting->byExtraDummyPixel);// write dummy pixel

		// 4) update variable for AE
		g_wAECExposureRowMax = 1235;
		GetSensorPclkHsync(UXGA_FRM, Fps);
		//g_wSensorHsyncWidth = 1940+pOv2650_FpsSetting->byExtraDummyPixel;
		//g_dwPclk= pOv2650_FpsSetting->dwPixelClk;
	}

	// hemonel 2010-01-13: move to here
	WriteSensorSettingWB(sizeof(gc_OV2650_ISP_Setting)/3, gc_OV2650_ISP_Setting);
	// 4) update variable for AE
}



void CfgOV2650ControlAttr(void)
{
	U8 i;

	g_bySensorSize = SENSOR_SIZE_UXGA;
	g_wSensorSPFormat = UXGA_FRM|SVGA_FRM;

	{
		memcpy(g_asOvCTT,gc_OV2650_CTT,sizeof(gc_OV2650_CTT));
	}

	{
		// AE backlight compensation parameter
		g_byOVAEW_Normal = OV2650_AEW;
		g_byOVAEB_Normal = OV2650_AEB;

		g_byOVAEW_BLC = g_byOVAEW_Normal + 0x20;
		g_byOVAEB_BLC = g_byOVAEB_Normal + 0x20;
	}

	{
		//g_bySV28VoltSel =  SV28_VOL_2V9;
		//g_bySV18VoltSel=  SV18_VOL_1V5;
		// hemonel 2010-09-08: support 1.8V sensor IO
#if (_CHIP_ID_ & _RTS5840_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V79;
#elif (_CHIP_ID_ & _RTS5832_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V85;
#elif (_CHIP_ID_ & _RTS5829B_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V82;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V80;
#endif	}

//	if(g_byFormatCfgExist==0) // format config not exist, use default setting.
	{
		//====== resolution  setting ===========
		// -- preview ---
		g_aVideoFormat[0].byaVideoFrameTbl[0] = 11;
		g_aVideoFormat[0].byaVideoFrameTbl[1]=F_SEL_640_480;
		g_aVideoFormat[0].byaVideoFrameTbl[2]=F_SEL_160_120;
		g_aVideoFormat[0].byaVideoFrameTbl[3]=F_SEL_176_144;
		g_aVideoFormat[0].byaVideoFrameTbl[4]=F_SEL_320_240;
		g_aVideoFormat[0].byaVideoFrameTbl[5]=F_SEL_352_288;
		g_aVideoFormat[0].byaVideoFrameTbl[6]=F_SEL_800_600;
		g_aVideoFormat[0].byaVideoFrameTbl[7]=F_SEL_1024_768;
		g_aVideoFormat[0].byaVideoFrameTbl[8]=F_SEL_1280_720;
		g_aVideoFormat[0].byaVideoFrameTbl[9]=F_SEL_1280_800;
		g_aVideoFormat[0].byaVideoFrameTbl[10]=F_SEL_1280_1024;
		g_aVideoFormat[0].byaVideoFrameTbl[11]=F_SEL_1600_1200;
		//still image
		g_aVideoFormat[0].byaStillFrameTbl[0] = 11;
		g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_1600_1200;
		g_aVideoFormat[0].byaStillFrameTbl[2]=F_SEL_160_120;
		g_aVideoFormat[0].byaStillFrameTbl[3]=F_SEL_176_144;
		g_aVideoFormat[0].byaStillFrameTbl[4]=F_SEL_320_240;
		g_aVideoFormat[0].byaStillFrameTbl[5]=F_SEL_352_288;
		g_aVideoFormat[0].byaStillFrameTbl[6]=F_SEL_640_480;
		g_aVideoFormat[0].byaStillFrameTbl[7]=F_SEL_800_600;
		g_aVideoFormat[0].byaStillFrameTbl[8]=F_SEL_1024_768;
		g_aVideoFormat[0].byaStillFrameTbl[9]=F_SEL_1280_720;
		g_aVideoFormat[0].byaStillFrameTbl[10]=F_SEL_1280_800;
		g_aVideoFormat[0].byaStillFrameTbl[11]=F_SEL_1280_1024;

		//====== fps  setting ===========
		for(i=0; i<7; i++)
		{
			/*640_480,
			160_120,
			176_144,
			320_200,
			320_240,
			352_288,
			640_400*/
			g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_15|FPS_20|FPS_25|FPS_30;
		}

		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_800_600]=FPS_23|FPS_20;
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_1024_768]=FPS_8|FPS_5;
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_1280_720] =FPS_8|FPS_5;
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_1280_800] =FPS_8|FPS_5;
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_1280_1024] =FPS_8|FPS_5;
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_1600_1200] =FPS_5;
		//====== format type  setting ===========
		g_aVideoFormat[0].byFormatType = FORMAT_TYPE_YUY2;
#ifdef _ENABLE_MJPEG_
		// copy YUY2 setting to MJPEG setting
		memcpy(&g_aVideoFormat[1], &g_aVideoFormat[0], sizeof(g_aVideoFormat[0]));

		// modify MJPEG FPS setting
		for(i=7; i<17; i++) //bit7-14 SVGA and larger size .
		{
			g_aVideoFormat[1].waFrameFpsBitmap[i] =FPS_5|FPS_8|FPS_15;//|FPS_1|FPS_3|FPS_5;
		}

		// modify MJPEG format type
		g_aVideoFormat[1].byFormatType = FORMAT_TYPE_MJPG;
#endif

	}


}

#ifndef _USE_BK_HSBC_ADJ_
void SetOV2650Brightness(S16 swSetValue)
{
	U16 wBrightSign;
	U16 wBrightVal;

	Read_SenReg(0x3390, &wBrightSign);
	if(swSetValue>=0)
	{
		wBrightSign &= 0xF7;	// Brightness sign positive
		wBrightVal = swSetValue;
	}
	else
	{
		wBrightSign |= 0x08;		// Brightness sign negative
		wBrightVal = -swSetValue;
	}
	Write_SenReg(0x3390, wBrightSign);	// write Brightness sign
	Write_SenReg(0x339A, wBrightVal);

	return;
}


void	SetOV2650Contrast(U16 wSetValue)
{
	Write_SenReg(0x3399, wSetValue);

	return;
}

void	SetOV2650Saturation(U16 wSetValue)
{
	Write_SenReg(0x3394, wSetValue);
	Write_SenReg(0x3395, wSetValue);

	return;
}

void	SetOV2650Hue(S16 swSetValue)
{
	U8 byHueCos,byHueSin,bySign;
	U16 wHueSinSign;
	float fHue;


	if(swSetValue>=0)
	{
		if(swSetValue<=90)
		{
			// 0~pi/2
			bySign = 0x01;
		}
		else
		{
			// pi/2~pi
			bySign = 0x31;
		}
	}
	else
	{
		if(swSetValue>=-90)
		{
			// -pi/2~0
			bySign = 0x02;
		}
		else
		{
			// -pi~-pi/2
			bySign = 0x32;
		}
	}

	fHue = (float)swSetValue/(float)180*3.1415926;
	byHueCos = abs(cos(fHue)*128);
	byHueSin = abs(sin(fHue)*128);

	Read_SenReg(0x3390, &wHueSinSign);
	Write_SenReg(0x3390, (wHueSinSign&0xCC)|bySign);
	Write_SenReg(0x3392, byHueCos);
	Write_SenReg(0x3393, byHueSin);

	return;
}
#endif
//RTS58XX use auto sharpness mode : -2, -1, 0 , 1, 2
/*
@@ 0 1 Sharpness off
60 3306 08 08
60 3371 00


@@ 0 2 Auto Sharpness
60 3306 00 08

@@ 2 3 Auto Sharpness Level -2
60 3306 00 08
60 3376 01
60 3377 00
60 3378 10
60 3379 80

@@ 2 4 Auto Sharpness Level -1
60 3306 00 08
60 3376 02
60 3377 00
60 3378 8
60 3379 80


@@ 2 6 Auto Sharpness Level 0
60 3306 00 08
60 3376 04
60 3377 00
60 3378 4
60 3379 80

@@ 2 7 Auto Sharpness Level +1
60 3306 00 08
60 3376 06
60 3377 00
60 3378 4
60 3379 80


@@ 2 8 Auto Sharpness Level +2
60 3306 00 08
60 3376 08
60 3377 00
60 3378 4
60 3379 80


@@ 0 10 Manual Sharpness
60 3306 08 08

@@ 10 11 Manual Sharpness Level -1
60 3306 08 08
60 3371 00

@@ 10 12 Manual Sharpness Level 0
60 3306 08 08
60 3371 02

@@ 10 13 Manual Sharpness Level +1
60 3306 08 08
60 3371 04
*/
void	SetOV2650Sharpness(U8 bySetValue)
{
	U8 byShp_th1;

	switch(bySetValue)
	{
	case 0 :
		//       byShp_th1 = 0x05;
		byShp_th1 = 0x01;
		break;
	case 1 :
		//      byShp_th1 = 0x08;
		byShp_th1 = 0x03;
		break;
	case 2 :
		//        byShp_th1 = 0x0a;
		byShp_th1 = 0x05;
		break;
	case 3:
		//        byShp_th1 = 0x0f;
		byShp_th1 = 0x08;
		break;
	case 4:
		//          byShp_th1 = 0x18;
		byShp_th1 = 0x0a;
		break;
	case 5:
	default:
		byShp_th1 = 0x0f;
		//            byShp_th1 = 0x1f;
		break;
	}
	Write_SenReg(0x3371, byShp_th1);

}

#ifndef _USE_BK_SPECIAL_EFFECT_
void SetOV2650Effect(U8 byEffect)
{
	U8 bySDE = 0;

	if(byEffect&SNR_EFFECT_NEGATIVE)
	{
		// negative
		bySDE |= 0x40;
	}

	if(byEffect&SNR_EFFECT_MONOCHROME)
	{
		// gray
		bySDE |= 0x20;
	}

	Write_SenReg_Mask(0x3391, bySDE, 0x60);
}
#endif


/*
	@@ Miror(Array) ON
	60 3090 08 08
	@@ Miror(Array) OFF
	60 3090 00 08


	@@ Mirror on
	60 307c 02 02

	@@ Mirror off
	60 307c 00 02

	@@ vFlip on
	60 307c 01 01

	@@ vFlip off
	60 307c 00 01
	*/

void SetOV2650ImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.
{
	Write_SenReg_Mask(0x3090, bySnrImgDir<<3, 0x08);	// Miror(Array)
	Write_SenReg_Mask(0x307C, ((bySnrImgDir&0x01)<<1)|((bySnrImgDir&0x02)>>1),0x03);	// Mirror, flip control
}
/*
void SetOV2650WindowCrop(U16 wXStart,U16 wXEnd,U16 wYStart,U16 wYEnd )
{
	U16 wWidth,wHeight;

	DBG(("@12-->%d,%d,%d,%d\n",wXStart, wXEnd, wYStart, wYEnd));

	// recalculate window
	wWidth = wXEnd - wXStart;	// width
	wHeight = wYEnd - wYStart;	// height
	if(wWidth <=800)
	{	// at vario/skip mode, horizontal is not sub sample
		wWidth <<= 1;
	}

	// update window
	Write_SenReg(0x3316, wWidth>>4);		// scale input hor[10:4]
	Write_SenReg(0x3317,wHeight>>4);		// scale input ver[10:4]
	Write_SenReg(0x3318, (wWidth&0x0F)| ((wHeight&0x0F)<<4));	// scale input hor,ver low

	return;
}

void SetOV2650OutputDim(U16 wWidth,U16 wHeight)
{
	if((wWidth != 1600)&&(wHeight != 1200))
	{	// need scale
		Write_SenReg(0x3302, 0x10);	// scale_en
	}
	else
	{	// only crop
		Write_SenReg(0x3302, 0x01);	// disable DCW and Zoom
	}

	Write_SenReg(0x3088, INT2CHAR(wWidth,1));	// image output width H
	Write_SenReg(0x3089,INT2CHAR(wWidth,0));	// image output width L
	Write_SenReg(0x308a, INT2CHAR(wHeight,1));	// image output height H
	Write_SenReg(0x308b, INT2CHAR(wHeight,0));	// image output height L
	Write_SenReg(0x331a, wWidth>>4);	// isp output hor high
	Write_SenReg(0x331b,wHeight>>4);	// isp output ver high
	Write_SenReg(0x331c, (wWidth&0x0F)| ((wHeight&0x0F)<<4));	//isp output hor,ver low
}
*/
static U16 CalAntiflickerStep(U8 byFps, U8 byFrq, U16 wRowNumber)
{
	return ((U16)byFps * wRowNumber/(U16)byFrq+1)/2; // round to integer
}

static OV_BandingFilter_t  OV_SetBandingFilter(U8 byFps)
{
	OV_BandingFilter_t BdFilter;

	BdFilter.wD50Base= CalAntiflickerStep(byFps, 50, g_wAECExposureRowMax);//((U16)byFps*g_wAECExposureRowMax/(U16)50+1)/2;
	BdFilter.byD50Step= 100/byFps -1;

	BdFilter.wD60Base= CalAntiflickerStep(byFps, 60, g_wAECExposureRowMax);//((U16)byFps*g_wAECExposureRowMax/(U16)60+1)/2;
	BdFilter.byD60Step= 120/byFps -1;

	if(BdFilter.wD50Base<0x10)
	{
		BdFilter.wD50Base= CalAntiflickerStep(byFps, 25, g_wAECExposureRowMax);//(U16)byFps*g_wAECExposureRowMax/(U16)(100/2);
		BdFilter.byD50Step= (100/2)/byFps -1;
	}

	if(BdFilter.wD60Base<0x10)
	{
		BdFilter.wD60Base= CalAntiflickerStep(byFps, 30, g_wAECExposureRowMax);//(U16)byFps*g_wAECExposureRowMax/(U16)(120/2);
		BdFilter.byD60Step= (120/2)/byFps -1;
	}

	return BdFilter;

}

// anti-banding lines = (1/120)/ Tline for 60Hz, (1/100)/Tline for 50Hz
// Tline = 1/(fps*Exposure row max)
// anti-banding lines = (fps*exposure row max) /120 for 60Hz, (fps*exposure row max) /100 for 50Hz
void SetOV2650PwrLineFreq(U8 byFPS, U8 byLightFrq)
{
	U8 byBandCtl;
	OV_BandingFilter_t BdFilter;

	// hemonel 2009-07-06: optimize code
	BdFilter = OV_SetBandingFilter(byFPS);
	/*
	wD50Val = (U16)byFPS*g_wAECExposureRowMax/(U16)100;
	byD50Step = 100/byFPS -1;
	wD60Val = (U16)byFPS*g_wAECExposureRowMax/(U16)120;
	byD60Step = 120/byFPS -1;

	if(wD50Val<0x10)
	{
		wD50Val = (U16)byFPS*g_wAECExposureRowMax/(U16)50;
		byD50Step = 50/byFPS -1;
	}
	if(wD60Val<0x10)
	{
		wD60Val = (U16)byFPS*g_wAECExposureRowMax/(U16)60;
		byD60Step = 60/byFPS -1;
	}
	*/

	// D50
	Write_SenReg(0x3071, INT2CHAR(BdFilter.wD50Base,1));
	Write_SenReg(0x3070, INT2CHAR(BdFilter.wD50Base,0));
	Write_SenReg(0x301C, BdFilter.byD50Step);

	//D60
	Write_SenReg(0x3073, INT2CHAR(BdFilter.wD60Base,1));
	Write_SenReg(0x3072, INT2CHAR(BdFilter.wD60Base,0));
	Write_SenReg(0x301D, BdFilter.byD60Step);

	if(PWR_LINE_FRQ_DIS == byLightFrq)
	{
		// disable banding filter, auto banding filter
		byBandCtl = 0x40;
	}
	else if(PWR_LINE_FRQ_50 == byLightFrq)
	{
		// select BD50 as banding filter
		byBandCtl = 0x80;
	}
	else
	{
		// select BD60 as banding filter
		byBandCtl = 0x00;
	}
	Write_SenReg_Mask(0x3014, byBandCtl, 0xC0);
}

void SetOV2650WBTemp(U16 wRgain, U16 wGgain, U16 wBgain)
{
//	OV_CTT_t ctt;

//	ctt = OV_SetColorTemperature(wSetValue);

	Write_SenReg(0x3337, wRgain>>4);	// R gain [11:4]
	Write_SenReg(0x3338, wGgain>>4);	// G gain [11:4]
	Write_SenReg(0x3339, wBgain>>4);	// B gain [11:4]
	Write_SenReg_Mask(0x333A, wRgain<<4, 0xF0);	// R gain [3:0]
	Write_SenReg_Mask(0x333A, wGgain,	0x0F);		// G gain [3:0]
	Write_SenReg_Mask(0x333B, wBgain<<4, 0xF0);	// B gain [3:0]
}

void SetOV2650WBTempAuto(U8 bySetValue)
{
	if(bySetValue)
	{
		// atuo white balance
		Write_SenReg_Mask(0x3306, 0x00,0x02);
	}
	else
	{
		// manual white balance
		Write_SenReg_Mask(0x3306, 0x02,0x02);
	}
}
void SetOV2650BackLightComp(U8 bySetValue)
{
	U8 byAEW,byAEB;

	if(bySetValue)
	{
		byAEW = g_byOVAEW_BLC;
		byAEB = g_byOVAEB_BLC;
	}
	else
	{
		byAEW = g_byOVAEW_Normal;
		byAEB = g_byOVAEB_Normal;
	}
	Write_SenReg(0x3018, byAEW);
	Write_SenReg(0x3019, byAEB);
}

void SetOV2650IntegrationTimeAuto(U8 bySetValue)
{
	if(bySetValue == EXPOSURE_TIME_AUTO_MOD_APERTPRO)
	{
		Write_SenReg_Mask(0x3013, 0x05, 0x05);
		// auto exposure
		if(LowLightCompItem.Last)//(g_byLowLightCompLast)	// hemonel 2009-09-22: add low light compensation
		{
			Write_SenReg_Mask(0x3014, 0x08, 0x08);  // enable night mode
		}
		else	// hemonel 2009-09-22: add low light compensation
		{
			Write_SenReg_Mask(0x3014, 0x00, 0x08);  // close night mode
			Write_SenReg(0x302E, 0);
			Write_SenReg(0x302D, 0);
		}
	}
	else
	{
		// manual expsoure
		Write_SenReg_Mask(0x3014, 0x00, 0x08);  // close night mode
		Write_SenReg_Mask(0x3013, 0x00, 0x05);
	}
}

void SetOV2650IntegrationTime(U16 wEspline)
{
	U16 wDummyline = 0;
	U16 wExpMax = g_wAECExposureRowMax-2;

	if(wEspline > wExpMax)
	{
		wDummyline = wEspline - wExpMax;
		wEspline = wExpMax;
	}
	Write_SenReg(0x3003, INT2CHAR(wEspline, 0));
	Write_SenReg(0x3002, INT2CHAR(wEspline, 1));
	Write_SenReg(0x302E, INT2CHAR(wDummyline, 0));
	Write_SenReg(0x302D, INT2CHAR(wDummyline, 1));
}

void SetOV2650Gain(float fGain)
{
}

// hemonel 2009-09-22: add low light compensation
void SetOV2650LowLightComp(U8 bySetValue)
{
	if(bySetValue)
	{
		// dynamic frame rate
		Write_SenReg_Mask(0x3014, 0x08, 0x08);  // enable night mode
	}
	else
	{
		// fix frame rate
		Write_SenReg_Mask(0x3014, 0x00, 0x08);  // close night mode
		Write_SenReg(0x302E, 0);
		Write_SenReg(0x302D, 0);
	}
}

#ifdef _STILL_IMG_BACKUP_SETTING_
// hemonel 2009-11-19: preview to still image backup AE and AWB
void OV2650PreviewToCapture(U16 wPreviewWidth, U16 wStillWidth)
{
	U16 wTemp;
	U16 wExp=0, wGain;

	Write_SenReg_Mask(0x3324, 0x40, 0x40);	// fix AWB
	Write_SenReg_Mask(0x3014, 0x00, 0x08);	// disable auto night mode fix dummy line
	Write_SenReg_Mask(0x3013, 0x00, 0x05);	// fix AEC/AGC

	// read exposure
	Read_SenReg(0x3003, &wTemp);
	wExp += wTemp;
	Read_SenReg(0x3002, &wTemp);
	wExp += wTemp*256;
	Read_SenReg(0x302E, &wTemp);
	wExp += wTemp;
	Read_SenReg(0x302D, &wTemp);
	wExp += wTemp*256;
	// read gain
	Read_SenReg(0x3000, &wGain);

	// back up exposure and gain at preview
	g_wPreviewBackupExposure = wExp;
	g_wPreviewBackupGain = wGain;

	// convert exposure and gain for capture mode
	if((wPreviewWidth<= 800)&&(wStillWidth > 800))
	{
		wExp = (U16)2*(U16)wExp*(U16)g_byStillFPS/(U16)g_byCommitFPS;
		g_wAECExposureRowMax = 1235;
	}
	else if((wPreviewWidth<= 800)&&(wStillWidth<=800))
	{
		wExp = (U16)wExp*(U16)g_byStillFPS/(U16)g_byCommitFPS;
		g_wAECExposureRowMax = 617;
	}
	else
	{
		wExp = (U16)wExp*(U16)g_byStillFPS/((U16)g_byCommitFPS*(U16)2);
		g_wAECExposureRowMax = 617;
	}

	// set exposure and gain for capture mode
	SetOV2650IntegrationTime(wExp);
	Write_SenReg(0x3000, wGain);
}

// hemonel 2009-11-19: preview to still image backup AE and AWB
void OV2650CaptureToPreview()
{
	// restore preview mode exposure and gain
	if(g_wPreviewBackupExposure !=0)
	{
		SetOV2650IntegrationTime(g_wPreviewBackupExposure);
		Write_SenReg(0x3000, g_wPreviewBackupGain);
	}

	//enable AWB and AEC
	Write_SenReg_Mask(0x3324, 0x00, 0x40);	// enable AWB
}
#endif //#ifdef _STILL_IMG_BACKUP_SETTING_
#endif
