#ifndef _YACC6A1S_H_
#define _YACC6A1S_H_

void YACC6A1SSetFormatFps(U16 SetFormat, U8 Fps);
void CfgYACC6A1SControlAttr(void);
void SetYACC6A1SImgDir(U8 bySnrImgDir);
void SetYACC6A1SIntegrationTime(U32 dwSetValue);
void SetYACC6A1SExposuretime_Gain(float fExpTime, float fTotalGain);
void InitYACC6A1SIspParams();
void YACC6A1S_POR();
void SetYACC6A1SDynamicISP(U8 byAEC_Gain);
void SetYACC6A1SDynamicISP_AWB(U16 wColorTempature);
#endif // _OV9710_H_

