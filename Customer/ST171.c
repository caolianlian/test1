#include "Inc.h" 

#ifdef RTS58XX_SP_ST171
//_________________________
#define DYNAMIC_DarkMode_CCM_EN		0x0800
//__________________________
U8 CCM_frame_count =0;
U8 g_byCCM2State  = 0x00;


U8 g_byPwrLineFreqLast;


void WriteST171Exposuretime(U16 wExposureRows);
void WriteST171Gain(U16 wGain);
void WriteST171DummyLine(U16 wDummy);	
U8 Digital_fractional_sensorGain;

OV_CTT_t code gc_ST171_CTT[3] =
{
	{2800,224,256,464},
	{4050,336,256,416},
	{6700,416,256,296},
};

typedef struct ST171_FpsSetting
{
	U8 byFps;
	U8 byPLL;   
	U8 byVBLANK_H;   
	U8 byVBLANK_L;    
	U8 byHBLANK_H;   
	U8 byHBLANK_L; 
	U16 wSensorHsyncWidth;
	U16 wAEC_LineNumber;
	U32 dwPixelClk;		
} ST171_FpsSetting_t;      //Jimmy.100820.HiMax HM1055


code ST171_FpsSetting_t  g_staST171FpsSetting_60HZ[]=
{//  FPS     byPLL,	 byVBLANK_H,	 byVBLANK_L,	byHBLANK_H,	byHBLANK_L,	wSensorHsyncWidth,	wAEC_LineNumber,	pclk = 24M/(clkrc+1)
     {5, (0x71),  (0x00),     (0x0A),     (0x07),     (0x3C),        {3225},        {744},      12000000},  // 12M  29.49678485
     {10,(0x71),  (0x00),     (0x0A),     (0x00),     (0xEF),        {1612},		{744},		12000000},
     {15,(0x51),  (0x00),     (0x0A),     (0x00),     (0x3C),		 {1433},		{744},		16000000},  // 12M  15.000375  
     {30,(0x50),  (0x00),     (0x0A),     (0x00),     (0x3C),		 {1433},		{744},  	32000000},  // 12M  29.49678485
};
code ST171_FpsSetting_t  g_staST171FpsSetting_50HZ[]=
{//  FPS     byPLL,	 byVBLANK_H,	 byVBLANK_L,	byHBLANK_H,	byHBLANK_L,	wSensorHsyncWidth,	wAEC_LineNumber,	pclk = 24M/(clkrc+1)
     {5, (0x71),  (0x00),     (0x1A),     (0x06),     (0xF8),        {3157},		{760},		12000000},  // 12M  29.49678485
     {10,(0x71),  (0x00),     (0x42),     (0x00),     (0x7F),		 {1500},		{800},		12000000},
     {15,(0x51),  (0x00),     (0x20),     (0x00),     (0x12),		 {1391},		{766},		16000000},  // 12M  15.000375  
     {30,(0x50),  (0x00),     (0x10),     (0x00),     (0x31),		 {1422},		{750},		32000000},  // 12M  29.49678485
};


t_RegSettingWB code gc_ST171_Setting[] =
{
	
	{0x00, 0x00}, //snr block															
	{0x03, 0x04}, //chip_cntr															
																						
																						
	{0x00, 0x01}, //snr block															
	{0x0a, 0x50}, //pll 48mhz -> 64mhz													
	{0x08, 0x90}, //pll on																
																						
	{0x11, 0x21}, //pxl_bias															
	{0x12, 0x10}, //ramp_bias1															
	{0x14, 0x84}, //abs_cntr1															
																						
	{0x17, 0x10}, //cp_cntr 															
	{0x18, 0xa3}, //ncp_cntr, ncp on													
																						
	{0x31, 0x00},																		
	{0x32, 0x00},																		
	{0x33, 0x05}, //1300																
	{0x34, 0x13},																		
	{0x35, 0x00},																		
	{0x36, 0x00},																		
	{0x37, 0x02}, //732 																
	{0x38, 0xdb},																		
	{0x39, 0x00}, //																	
	{0x3a, 0x0a},																		
	{0x3b, 0x00},																		
	{0x3c, 0x00},																		
	{0x3d, 0x00}, //																	
	{0x3e, 0x3c},																		
	{0x3f, 0x07},																		
																						
	{0x20, 0x05},																		
	{0x21, 0x05}, //8step																
	{0x22, 0xd0},																		
	{0x23, 0x00}, //gain x1.0															
	{0x24, 0x00}, //																	
	{0x25, 0x80}, //																	
																						
	{0x40, 0x83}, //bl_cntr 															
																						
	{0x41, 0x02}, //ramp_cntr1, 550mv													
	{0x42, 0x20}, //ramp_cntr2															
	{0x44, 0x07}, //ramp_offset1, (ramp_offset1 - 0)*4 = 28code @ 10bit 				
	{0x45, 0x07}, //ramp_offset2, ramp_offset1 = ramp_offset2							
																						
	{0x50, 0x00}, //test_50 															
	{0x51, 0x00}, //test_51 															
	{0x54, 0x25}, //test_54 															
	{0x56, 0x00}, //test_56 															
																						
	{0xac, 0x50}, //ramp_ld_short_h 													
	{0xad, 0x43}, //ramp_ld_short_start 												
	{0xae, 0xfd}, //ramp_ld_short_stop													
	{0xb0, 0x0b}, //int_gsel_start														
	{0xb1, 0x00}, //scan_gsel_h 														
	{0xb2, 0xfc}, //scan_gsel_start 													
	{0xb3, 0x00}, //ncp_en_cgain														
	{0xb4, 0x00}, //ncp_en_shut_h														
	{0xb5, 0x00}, //ncp_en_shut_l														
	{0xb7, 0x00}, //ncp_dis_cgain														
	{0xb8, 0x00}, //ncp_dis_shut_h														
	{0xb9, 0x00}, //ncp_dis_shut_l														
																						
																						
	{0x00, 0x02}, //idp block															
	{0x10, 0xdc}, //ipfun blc on, dark pixel median on, dpc on, nr on					
	{0x11, 0x0d}, //sigcnt																
	{0x12, 0x01}, //outfmt, 10-bit dpced bayer data 									
	{0x13, 0x80}, //isptst																
																						
	{0x20, 0x00}, //ofstctrl															
																						
	{0x30, 0x30}, //rowblctrl															
	{0x31, 0x00}, //rblacktarget														
	{0x32, 0x00}, //rblacktgtslp														
	{0x33, 0x1c}, //rblacktgtslq														
	{0x34, 0xff}, //rblacktgtmax														
	{0x36, 0x10}, //rowggreadvl 														
	{0x37, 0x10}, //rowrbreadvl 														
																						
	{0x40, 0x00}, //tstfun																
																						
	{0x51, 0x68}, //dctrln																
	{0x52, 0x0f}, //dctrlp																
	{0x53, 0x00}, //dctrlg																
	{0x54, 0x00}, //dctrlc																
	{0x55, 0x02}, //dctrld																
	{0x56, 0x00}, //dctrlf																
	{0x57, 0xe7}, //dctrla																
	{0x58, 0x44}, //dptctl																
	{0x59, 0x4a}, //dptcth																
	{0x5a, 0x18}, //psadth																
	{0x5b, 0x08}, //pttnthl 															
	{0x5c, 0x08}, //pttnthh 															
	{0x5e, 0x20}, //ptlvlth 															
	{0x5f, 0x10}, //dnthld																
	{0x60, 0x10}, //pnthld																
	{0x61, 0x04}, //nthld																
	{0x62, 0x10}, //sstrt																
	{0x63, 0x10}, //sslop																
	{0x64, 0x10}, //ssend																
	{0x65, 0x48}, //dstrt																
	{0x66, 0x40}, //dslop																
																						
	{0x6b, 0x00}, //dfltctrl															
	{0x6c, 0x0f}, //fmodctrl															
																						
	{0x94, 0x25}, //m1spda																
	{0x95, 0x01}, //m1spdb																
	{0x96, 0x02}, //m1spdc																
																						
	{0xb0, 0xc0}, //bnrctrl 															
	{0xb1, 0x04}, //bnrthv																
	{0xb2, 0x04}, //bnrthvslpn															
	{0xb3, 0x20}, //bnrthvslpd															
	{0xb4, 0x10}, //bnrstrnor															
	{0xb5, 0x40}, //bnrstrdrk															
																						
																						
	{0x00, 0x01},																		
	{0x03, 0x01}, // snr enable 														
};


static ST171_FpsSetting_t*  GetHMFpsSetting(U8 Fps, ST171_FpsSetting_t staHMFpsSetting[], U8 byArrayLen)
{
	U8 i;
	U8 Idx;

	if(g_bIsHighSpeed)
	{
		Idx=2; //8fps for uxga, 10 fps for vga	
	}
	else
	{
		Idx = 0;
	}

	for(i=0;i< byArrayLen;i++)
	{
		if(staHMFpsSetting[i].byFps ==Fps)
		{
			Idx=i;
			break;
		}	
	}
	return &staHMFpsSetting[Idx];	

}

static U16 MapSnrGlbGain2SnrRegSetting(U16 wGain)
{
      
	U32 data sensorGain=0;	
	U32 data sensorDgain=0;
	U32 data sensorAgain=0;
	//U16 data wGain_before;

    	if(wGain>g_wAECGlobalgainmax) 
		{
			wGain=g_wAECGlobalgainmax;
		}

		if(wGain < 32)
		{
			sensorAgain = (U32)wGain << 3; 
			sensorDgain =  0x0000;
		}
		else if(wGain < 64)
		{
			sensorAgain = (U32)wGain << 2;	
			sensorDgain =  0x0001;
		}
		else if(wGain < 128)
		{
			sensorAgain = (U32)wGain  << 1;	
			sensorDgain =  0x0002;
		}
		else //if(wGain < 128)
   		{
			sensorAgain = (U32)wGain  << 1;	
			sensorDgain =  0x0002;   		
  		}

		sensorGain = (sensorDgain<<12)|sensorAgain;
          	//wGain_before = wGain;  
	      return (U16)sensorGain;
}

float SnrRegSetting2SnrGlbGain(U16 wSnrRegGain)
{
	return (float)wSnrRegGain/128.0;
}


void SetST171BandingFilter(U8 byFPS)
{
	ST171_FpsSetting_t *pHMFpsSetting;
	
	if(PwrLineFreqItem.Last == PWR_LINE_FRQ_60)
	{
   		pHMFpsSetting = GetHMFpsSetting(byFPS, g_staST171FpsSetting_60HZ, sizeof(g_staST171FpsSetting_60HZ)/sizeof(ST171_FpsSetting_t));
	}
	else if(PwrLineFreqItem.Last == PWR_LINE_FRQ_50)
	{
		pHMFpsSetting = GetHMFpsSetting(byFPS, g_staST171FpsSetting_50HZ, sizeof(g_staST171FpsSetting_50HZ)/sizeof(ST171_FpsSetting_t));
	}

	Write_SenReg(0x00, 0x01);

	Write_SenReg(0x0a, pHMFpsSetting->byPLL);

	Write_SenReg(0x39, pHMFpsSetting->byVBLANK_H);
	Write_SenReg(0x3a, pHMFpsSetting->byVBLANK_L);
	
	Write_SenReg(0x3d, pHMFpsSetting->byHBLANK_H);
	Write_SenReg(0x3e, pHMFpsSetting->byHBLANK_L);

	g_wSensorHsyncWidth = pHMFpsSetting->wSensorHsyncWidth; // this for manual exposure
	g_wAECExposureRowMax = pHMFpsSetting->wAEC_LineNumber - 4;	// this for max exposure time
	g_wAEC_LineNumber = pHMFpsSetting->wAEC_LineNumber;	// this for AE insert dummy line algothrim
	
}


//this function is also called during Probe stage to calculate the transfer_size/LWM/HWM
void GetSensorPclkHsync(U16 wSensorSPFormat,U8 byFps)
{
	ST171_FpsSetting_t *pHMFpsSetting;

	wSensorSPFormat = wSensorSPFormat; // for delete warning

	if(PwrLineFreqItem.Def== PWR_LINE_FRQ_60)
	{
   		pHMFpsSetting = GetHMFpsSetting(byFps, g_staST171FpsSetting_60HZ, sizeof(g_staST171FpsSetting_60HZ)/sizeof(ST171_FpsSetting_t));
	}
	else
	{
		pHMFpsSetting = GetHMFpsSetting(byFps, g_staST171FpsSetting_50HZ, sizeof(g_staST171FpsSetting_50HZ)/sizeof(ST171_FpsSetting_t));
	}
	g_wSensorHsyncWidth = pHMFpsSetting->wSensorHsyncWidth; // this for manual exposure
	g_dwPclk = pHMFpsSetting->dwPixelClk;		// this for scale speed
}


void ST171SetFormatFps(U16 SetFormat, U8 Fps)
{	
	ST171_FpsSetting_t *pHMFpsSetting;

	SetFormat = SetFormat;
	CHANGE_CCS_CLK(CCS_CLK_SEL_96M|CCS_CLK_DIVIDER_2);


	if(PwrLineFreqItem.Def == PWR_LINE_FRQ_60)
	{
   		pHMFpsSetting = GetHMFpsSetting(Fps, g_staST171FpsSetting_60HZ, sizeof(g_staST171FpsSetting_60HZ)/sizeof(ST171_FpsSetting_t));
	}
	else
	{
		pHMFpsSetting = GetHMFpsSetting(Fps, g_staST171FpsSetting_50HZ, sizeof(g_staST171FpsSetting_50HZ)/sizeof(ST171_FpsSetting_t));
	}
	g_byPwrLineFreqLast = PwrLineFreqItem.Def;


	WriteSensorSettingWB(sizeof(gc_ST171_Setting)/sizeof(t_RegSettingWB), gc_ST171_Setting);

	g_dwPclk= pHMFpsSetting->dwPixelClk;


	Write_SenReg(0x00, 0x01);

	Write_SenReg(0x0a, pHMFpsSetting->byPLL);

	Write_SenReg(0x39, pHMFpsSetting->byVBLANK_H);
	Write_SenReg(0x3a, pHMFpsSetting->byVBLANK_L);
	
	Write_SenReg(0x3d, pHMFpsSetting->byHBLANK_H);
	Write_SenReg(0x3e, pHMFpsSetting->byHBLANK_L);


	// 3) update variable for AE
	GetSensorPclkHsync(HD720P_FRM,Fps);
	g_wAECExposureRowMax = pHMFpsSetting->wAEC_LineNumber - 4;	// this for max exposure time
	g_wAEC_LineNumber = pHMFpsSetting->wAEC_LineNumber;	// this for AE insert dummy line algothrim

	g_wSensorWidthBefBLC = 1280;
	g_wSensorHeightBefBLC = 720;

	//InitOV9726IspParamsByResolution(g_wCurFrameWidth, g_wCurFrameHeight);

	//Write_SenReg(0x08, 22);	// change to AE page
}

void CfgST171ControlAttr(void)
{
	U8 i;
	g_bySensorSize = SENSOR_SIZE_HD720P; //SENSOR_SIZE_HD720P;
	g_wSensorSPFormat = HD720P_FRM;
	{	
		memcpy(g_asOvCTT, gc_ST171_CTT,sizeof(gc_ST171_CTT));		
	}

#ifdef SP_HM1055_IO18
	XBYTE[REG_TUNES18_28] = SV28_VOL_1V8|SV18_VOL_2V9;	// SV18 and SV28 voltage control register
#else

#if (_CHIP_ID_ & _RTS5840_)
	XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
	XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V94;
#elif (_CHIP_ID_ & _RTS5832_)
	XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
	XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V90;
#elif (_CHIP_ID_ & _RTS5829B_)
	XBYTE[REG_TUNESVA] =  SVA_VOL_2V82;
	XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V95;
#endif
#endif		

	g_bySnrPowerOnSeq = SNR_PWRCTL_SEQ_GPIO8| (SNR_PWRCTL_SEQ_SV18<<4)|(SNR_PWRCTL_SEQ_SV28<<2);

	//====== resolution  setting ===========
	// -- preview ---
	g_aVideoFormat[0].byaVideoFrameTbl[0] = 7;		// resolution number	
	g_aVideoFormat[0].byaVideoFrameTbl[1]=F_SEL_640_480;
	g_aVideoFormat[0].byaVideoFrameTbl[2]=F_SEL_352_288;
	g_aVideoFormat[0].byaVideoFrameTbl[3]=F_SEL_320_240;
	g_aVideoFormat[0].byaVideoFrameTbl[4]=F_SEL_176_144;		
	g_aVideoFormat[0].byaVideoFrameTbl[5]=F_SEL_160_120;
	//g_aVideoFormat[0].byaVideoFrameTbl[6]=F_SEL_640_400;
	g_aVideoFormat[0].byaVideoFrameTbl[6]=F_SEL_1280_720;
	g_aVideoFormat[0].byaVideoFrameTbl[7]=F_SEL_640_480;	
	//--still image--
	g_aVideoFormat[0].byaStillFrameTbl[0] = 4;	// resolution number	
	g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_640_480;
	g_aVideoFormat[0].byaStillFrameTbl[2]=F_SEL_320_240;
	g_aVideoFormat[0].byaStillFrameTbl[3]=F_SEL_160_120;
	g_aVideoFormat[0].byaStillFrameTbl[4]=F_SEL_1280_720;

	for(i=0; i<10; i++)
	{
		g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_5|FPS_10|FPS_15|FPS_30;
	}
	g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_800_600] =FPS_10;//|FPS_10;//FPS_9|FPS_8|FPS_5|FPS_1;                             //bit10 F_1280_720
	//g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_960_720] =FPS_10;//|FPS_10;//FPS_9|FPS_8|FPS_5|FPS_1;                             //bit10 F_1280_720
	g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_1280_720] =FPS_10;//|FPS_10;//FPS_9|FPS_8|FPS_5|FPS_1;                             //bit10 F_1280_720
	g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_1280_800] =FPS_10;//|FPS_10;//FPS_9|FPS_8|FPS_5|FPS_1;                             //bit10 F_1280_720
	g_aVideoFormat[0].byFormatType = FORMAT_TYPE_YUY2;
#ifdef _ENABLE_MJPEG_
	memcpy(&(g_aVideoFormat[1]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));
	for(i=0; i<19; i++) //bit0-6 VGA and smaller size, 30fps max
	{
		g_aVideoFormat[1].waFrameFpsBitmap[i] = FPS_5|FPS_10|FPS_15|FPS_30;
	}
	g_aVideoFormat[1].byFormatType = FORMAT_TYPE_MJPG;
#endif
#ifdef _ENABLE_M420_FMT_
	memcpy(&(g_aVideoFormat[2]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));
	g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_720] =FPS_11|FPS_5;
	g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_800] =FPS_11|FPS_5;
	g_aVideoFormat[2].byFormatType = FORMAT_TYPE_M420;
#endif


	InitST171IspParams();
}
void SetST171IntegrationTime(U16 wEspline)
{
	U16 wFrameLenLines;
	U16 wExpMax = g_wAECExposureRowMax-2;
	if(wEspline > wExpMax)
	{
		wFrameLenLines = wEspline + 20;
	}
	else
	{
		wFrameLenLines = g_wAEC_LineNumber;
	}
	if (wFrameLenLines%2==1)
	{
		wFrameLenLines++;
	}
	Write_SenReg(0x00, 01);	// change to AE page
	Write_SenReg(0x22, INT2CHAR(wEspline, 0));	// change Coarse integration time low
	Write_SenReg(0x21, INT2CHAR(wEspline, 1));	// change Coarse integration time high 

}

void SetST171Gain(float fGain)
{
}

void SetST171ImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.		
{
  	bySnrImgDir =bySnrImgDir;
	bySnrImgDir ^= 0x01; 	// HM1055 output mirrored image at default, so need mirror the image for normal output
	if(bySnrImgDir & 0x01)
	{
		Write_SenReg(0x00, 0x01);
		Write_SenReg(0x04, 0x50);
		SetBkWindowStart(1, 0);
	}
	else
	{
		Write_SenReg(0x00, 0x01);
		Write_SenReg(0x04, 0x53);
		//SetBkWindowStart(0, 0);
		SetBkWindowStart(0, 1);
	}
	XBYTE[0xfecd] = 0x03; //Neil Change the PCLK latch
}


void WaitFrameStart(void)
{
	if(g_byStartVideo)
	{
		XBYTE[0x8007/*ISP_INT_FLAG0*/] = 0x40/*ISP_FRAMESTART_INT*/;	// clear ISP Statictis interrupt 	
		WaitTimeOut(0x8007/*ISP_INT_FLAG0*/,0x40/* ISP_FRAMESTART_INT*/, 1, 30);
	}
}

	
void SetST171Exposuretime_Gain(float fExpTime, float fTotalGain)
{
	U16 wExposureRows_floor;
	U16  wGainRegSetting; //data
	float fSnrGlbGain;
	U16 wAGainRegSetting;
	U16 wDGainRegSetting;	
	float fExposureRows;
	U16 wSetDummy;	
	float fTemp;
	U16 wExposurePixels;

	wGainRegSetting=MapSnrGlbGain2SnrRegSetting((U16)(fTotalGain*16.0));
	fSnrGlbGain = SnrRegSetting2SnrGlbGain(wGainRegSetting);
	wAGainRegSetting = (wGainRegSetting & 0x03FF);
	wDGainRegSetting = ((wGainRegSetting) >> 12);
	fExposureRows = fExpTime/g_fSensorRowTimes;	//unit : 1row  //____�Y((float)g_wSensorHsyncWidth*(float)10000)/(float)g_dwPclk;     // unit microsecond
	wExposureRows_floor = (U16)(fExposureRows);	
	fTemp = fExpTime-((float)wExposureRows_floor*(g_fSensorRowTimes)); //fine integration time;unit:  us
	wExposurePixels =(U16)(fTemp * (float)(g_dwPclk/10000)) ; //unit: pixels
	if(fabs(g_fCurExpTime-fExpTime) > 0.0001)
	{	
		wSetDummy = g_wAFRInsertDummylines + g_wAEC_LineNumber;		

		Write_SenReg(0x00, 0x01);
		Write_SenReg(0x22, INT2CHAR(wExposureRows_floor, 0));	// change exposure value low
		Write_SenReg(0x21, INT2CHAR(wExposureRows_floor, 1));	// change exposure value high 

		WaitFrameSync(ISP_INT1, ISP_DATA_START_INT);
		Write_SenReg(0x25, INT2CHAR(wAGainRegSetting, 0));	// change exposure value low
		Write_SenReg(0x24, INT2CHAR(wAGainRegSetting, 1));	// change exposure value low		
		Write_SenReg_Mask(0x23, INT2CHAR(wDGainRegSetting, 0),0x007F); 

		g_fCurExpTime = fExpTime;	// back up exposure rows		
	}		
	else
	{	
		Write_SenReg(0x25, INT2CHAR(wAGainRegSetting, 0));	// change exposure value low
		Write_SenReg(0x24, INT2CHAR(wAGainRegSetting, 1));	// change exposure value low			
		Write_SenReg_Mask(0x23, INT2CHAR(wDGainRegSetting, 0),0x007F); 
	}

	SetISPAEGain(fTotalGain, fTotalGain, 1);


	if( g_byPwrLineFreqLast != PwrLineFreqItem.Last )
	{
		SetST171BandingFilter(g_byCurFps);
		g_byPwrLineFreqLast = PwrLineFreqItem.Last;
	}

	return;
}

void InitST171IspParams()
{
	// AWB initial gain 
	//use D50 R/G/B gain as AWB initial value
	g_wAWBRGain_Last= 0x150;
	g_wAWBGGain_Last= 0x108;
	g_wAWBBGain_Last= 0x1A0;
	g_wProjectGR = g_wAWBRGain_Last;
	g_wProjectGB = g_wAWBBGain_Last;
	
  	g_bySaturation_Def = 64;
	g_byContrast_Def =35;//32;  //32 Contrast default
	
	//XBYTE[0x8520] = 0x06; //RAW denoise CB/Cr balance
	//XBYTE[0x85FA] = 0x0F; //Edge Enhance  ISP_BRIGHT_RATE
	//XBYTE[0x85FB] = 0x0F; //Edge Enhance  ISP_DARK_RATE
	g_wDynamicISPEn = 0;//DYNAMIC_LSC_EN|DYNAMIC_SHARPPARAM_EN|DYNAMIC_CCM_CT_EN|DYNAMIC_DarkMode_CCM_EN|DYNAMIC_GAMMA_EN|DYNAMIC_YGAMMA_EN;
	g_wDynamicISPEn = DYNAMIC_LSC_EN | DYNAMIC_SHARPPARAM_EN | DYNAMIC_CCM_CT_EN | DYNAMIC_CCM_BRIGHT_EN; //| DYNAMIC_HDR_EN; //| DYNAMIC_CCM_BRIGHT_EN; //| DYNAMIC_CCM_CT_EN | DYNAMIC_HDR_EN;
	
	//U8 i;

}
void SetST171DynamicISP(U8 byAEC_Gain)
{

byAEC_Gain = byAEC_Gain;

/*
if(g_wAEC_Gain <= 0x10 && g_wAFRInsertDummylines == 1)//Highlight mode
{
	XBYTE[0x8306] = 0;
	XBYTE[0x8304] = 0;
	XBYTE[0x830A] = 0;
	XBYTE[0x8308] = 0;
}
else
{
	XBYTE[0x8306] = 4;
	XBYTE[0x8304] = 5;
	XBYTE[0x830A] = 5;
	XBYTE[0x8308] = 4;
}
*/

}
void SetST171DynamicISP_AWB(U16 wColorTempature)
{
wColorTempature =wColorTempature;


}
#endif
