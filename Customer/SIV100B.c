#include "Inc.h"

#ifdef RTS58XX_SP_SIV100B

OV_CTT_t code gc_SIV100B_CTT[3] =
{
	{2800,0x8C,0x80,0xCE},
	{4050,0xB4,0x80,0xBE},
	{6500,0xD2,0x80,0x89},
};

OV_FpsSetting_t code g_staSIV100BFpsSetting[]=
{
	//FPS	extradummypixel clkrc	pclk = 24M/(2^(clkrc+1))
	{3,		(1000-800),		(3),	  	 1500000},
	{5, 		(1200-800),		(2),	  	 3000000},
	{8,		(1500-800),		(1),		 6000000},
	{9,		(1333-800),		(1),		 6000000},
	{10,		(1200-800),		(1),		 6000000},
	{15,		(800-800),		(1),		 6000000},
	{20,		(1200-800),		(0),		12000000},
	{25,		(960-800),		(0),		12000000},
	{30,		(800-800),		(0),		12000000},
};


t_RegSettingBB code gc_SIV100B_Setting[] =
{
	{0x04, 0x00}, //clkRC = 0
	{0x11, 0x04}, // #0x0a#0x09#black sun 2.8V
	{0x12, 0x0a},
	{0x13, 0x1f}, // #ABS 2.8V
	{0x16, 0x89},
	{0x1b, 0x90},
	{0x1f, 0x52},

	{0x20, 0x00}, //SIV100B  60Hz - 24.00FPS(120/5) 24MHz
	{0x21, 0x00},
	{0x22, 0x00}, //0x7c:24fps;
	{0x34, 0x7d},

	{0x23, 0x00}, //SIV100B  50Hz - 24.00FPS(100/4) 24MHz
	{0x24, 0x00},
	{0x25, 0x00}, //0x7c:24fps;
	{0x35, 0x96},

	{0x33, 0x14}, //# 6FPS ---minimum fps

	//---AE
	{0x40, 0x00},
	{0x41, 0x84}, //#AE target 0x84 0108
	{0x42, 0x7f}, // AE range---high nibble, low nibble
	{0x43, 0xc0}, //gain ceiling, max gain->increase Ep
	{0x44, 0x58}, //#again = 7.5x             |>
	{0x45, 0x28}, //#0x20 #Gain changer(Normal)0x2f (r:noise)
	{0x46, 0x08}, //#0x0a #Gain changer(N Down) 0x12->0x08
	{0x47, 0x15},  // gain range control   <|
	{0x48, 0x1e}, //#0x1d #Range 1 0x0d (r:AE hunting)
	{0x49, 0x13}, //#0x0e #Range 2
	{0x4a, 0x63}, //#0x73 #Rgane 3
	{0x4b, 0x82}, //#Shut Rate control 0x89 �����Ұ�
	{0x4c, 0x3c},
	{0x4e, 0x17}, //#AEADC Off
	{0x4f, 0x8a}, //#20050302
	{0x50, 0x94},

	{0x5a, 0x00}, // 	#AF Off

	//---AWB
	{0x60, 0xc8}, //   #cb 0106  ---AWB misc---
	{0x61, 0x88}, //	#0x8e 0105 condition
	{0x62, 0x01}, //   #AWB Lange #0x01
	{0x63, 0x80},
	{0x64, 0x80},
	{0x65, 0xd2}, //#R Gain Top
	{0x66, 0x83}, //#R Gain Bottom
	{0x67, 0xd6}, //#B Gain Top
	{0x68, 0x80}, //#B Gain Bottom 0108
	{0x69, 0x8a}, //#0x90	#Cr Top Value
	{0x6a, 0x70}, //#Cr Bottom Value
	{0x6b, 0x90}, //#Cb Top Value   #0x90 a7
	{0x6c, 0x70}, //#Cb Bottom Value   #0x70
	{0x6d, 0x88}, //#0x84
	{0x6e, 0x77}, //#0x7b
	{0x6f, 0x46}, //#0x84 #AWB URNG 0109
	{0x70, 0xd8}, //#0xd8 0109
	{0x71, 0x40}, //#0x70
	{0x72, 0x05},
	{0x73, 0x02}, //#0x30
	{0x74, 0x0c}, //   #0x07 0x10 0109
	{0x75, 0x04}, //#0x32
	{0x76, 0x20}, //#0x32
	{0x77, 0xb7}, //#0xb0 0109
	{0x78, 0x95}, //#0xb5 0109

	//---IDP, image digital process
	{0x80, 0xaf}, //   #shading on 0105  ---ISP misc---awb, gma, ccm, lsc
	{0x81, 0x1d}, // ---video signal conrol
	{0x82, 0xfd}, // ---output format select
	{0x83, 0x00}, // 	#shading on/off
	{0x86, 0xa1}, // ---DPC control misc
	{0x87, 0x04}, //   #0x18
	{0x88, 0x26}, // 	#DPC start Gain 0x20
	{0x89, 0x0f},

	{0x92, 0x44}, //#filter control
	{0x93, 0x20},
	{0x94, 0x20},
	{0x95, 0x40},
	{0x96, 0x18},
	{0x97, 0x20},
	{0x98, 0x20},
	{0x99, 0x24},
	{0x9a, 0x50},

	//---shading
	{0xa4, 0xaa},
	{0xa5, 0xaa}, // LSC area gain value |>
	{0xa6, 0xad},
	{0xa7, 0xda},
	{0xa8, 0xcc}, //  <|
	{0xa9, 0x22}, // RGB x, y gain |>
	{0xaa, 0x33},
	{0xab, 0x00},
	{0xac, 0x00},
	{0xad, 0x00},
	{0xae, 0x00}, //  <|
	{0xaf, 0xa0}, //#shading x position
	{0xb0, 0x88}, // shadingy position

	//---gamma
	{0xb1, 0x00}, // GMA |>
	{0xb2, 0x08},
	{0xb3, 0x11},
	{0xb4, 0x25},
	{0xb5, 0x45},
	{0xb6, 0x5f},
	{0xb7, 0x74},
	{0xb8, 0x87},
	{0xb9, 0x97},
	{0xba, 0xa5},
	{0xbb, 0xb2},
	{0xbc, 0xc9},
	{0xbd, 0xdd},
	{0xbe, 0xf0},
	{0xbf, 0xf8},
	{0xc0, 0xff}, //  <|

	//---CCM
	{0xc1, 0x3d}, //  CCM  1/64  |>
	{0xc2, 0xc6},
	{0xc3, 0xfd},
	{0xc4, 0x10},
	{0xc5, 0x21},
	{0xc6, 0x10},
	{0xc7, 0xf3},
	{0xc8, 0xbd},
	{0xc9, 0x50}, //  <|

	//---Edge
	{0xca, 0x90}, // sharpness control, 1\4\8 pixel mean
	{0xcb, 0xa },  //  edge gain upper
	{0xcc, 0x10}, //  edge gain lower
	{0xcd, 0x06},
	{0xce, 0x06},
	{0xcf, 0x20},
	{0xd0, 0x20},
	{0xd1, 0x26}, //#Edge start gain
	{0xd2, 0x86},
	{0xd3, 0x00},

	//---contrast
	{0xd4, 0x10}, //#Contrast 0x14
	{0xd5, 0x14}, //---saturation Cr color gain
	{0xd6, 0x14}, //---saturation Cb color gain
	{0xd7, 0x00}, //---brightness control Y Setup (e:down)
	{0xd8, 0x00}, //---Special Effect select
	{0xd9, 0x00}, //SDE Cb
	{0xda, 0x00}, //SDE Cr
	{0xdb, 0xff}, // y top
	{0xdc, 0x00}, // y bottom

	//---saturation
	{0xe1, 0x2a}, //#color suppress Start Gain 0x30
	{0xe2, 0x6b}, //#0xd7 #0x6b #color suppress slpoe

	{0xf0, 0x24}, //---window start\width MSB
	{0xf2, 0x80}, //---window H width LSB
	{0xf4, 0xe0}, //---window V width LSB

	{0x40, 0x84}, //#AEBLC ON
	{0x03, 0x05}, //no HI-Z, start video ouput
};

static OV_FpsSetting_t*  GetOvFpsSetting(U8 Fps, OV_FpsSetting_t staOvFpsSetting[], U8 byArrayLen)
{
	U8 i;
	U8 Idx;

	//ISP_MSG((" array size = %bd\n", byArrayLen));

	if (g_bIsHighSpeed)
	{
		Idx=2; //8fps for uxga, 10 fps for vga
	}
	else
	{
		Idx = 0;
	}

	for(i=0; i< byArrayLen; i++)
	{
		//ISP_MSG((" fps i = %bd\n",i));
		if(staOvFpsSetting[i].byFps ==Fps)
		{
			Idx=i;
			break;
		}
	}
	//ISP_MSG((" get fps idx = %bd\n",Idx));
	return &staOvFpsSetting[Idx];
}

//this function is also called during Probe stage to calculate the transfer_size/LWM/HWM
void GetSensorPclkHsync(U16 wSensorSPFormat,U8 byFps)
{
	OV_FpsSetting_t *pSIV100B_FpsSetting;

	wSensorSPFormat = wSensorSPFormat; // for delete warning

	pSIV100B_FpsSetting=GetOvFpsSetting(byFps, g_staSIV100BFpsSetting, sizeof(g_staSIV100BFpsSetting)/sizeof(OV_FpsSetting_t));
	g_wSensorHsyncWidth = pSIV100B_FpsSetting->wExtraDummyPixel+800;
	g_dwPclk= pSIV100B_FpsSetting->dwPixelClk;
}

void SIV100BSetFormatFps(U8 SetFormat, U8 byFps)
{
	OV_FpsSetting_t *pSIV100B_FpsSetting;

	SetFormat = SetFormat; // for delete warning

	pSIV100B_FpsSetting=GetOvFpsSetting(byFps, g_staSIV100BFpsSetting, sizeof(g_staSIV100BFpsSetting)/sizeof(OV_FpsSetting_t));

	WriteSensorSettingBB(sizeof(gc_SIV100B_Setting)>>1, gc_SIV100B_Setting);//sensor common set

	Write_SenReg_Mask(0x04, pSIV100B_FpsSetting->byClkrc<<2, 0x0C); // write CLKRC

	//---PWR 50Hz Write dummy pixel ---//
	Write_SenReg_Mask(0x20, (INT2CHAR(pSIV100B_FpsSetting->wExtraDummyPixel,1)<<4), 0x30);//MSB
	Write_SenReg(0x21, INT2CHAR(pSIV100B_FpsSetting->wExtraDummyPixel,0)); //LSB.

	//---PWR 60Hz Write dummy pixel ---//
	Write_SenReg_Mask(0x23, (INT2CHAR(pSIV100B_FpsSetting->wExtraDummyPixel,1)<<4), 0x30);//MSB
	Write_SenReg(0x24, INT2CHAR(pSIV100B_FpsSetting->wExtraDummyPixel,0)); //LSB

	g_wAECExposureRowMax = 500; // from SETi files
	//g_wSensorHsyncWidth = pSIV100B_FpsSetting->wExtraDummyPixel+800;
	//g_dwPclk= pSIV100B_FpsSetting->dwPixelClk;
	GetSensorPclkHsync(VGA_FRM,byFps);
}


void CfgSIV100BControlAttr(void)
{
	g_bySensorSize = SENSOR_SIZE_VGA;
	g_wSensorSPFormat = VGA_FRM;

	{
		memcpy(g_asOvCTT,gc_SIV100B_CTT,sizeof(gc_SIV100B_CTT));
	}

	{
		// AE backlight compensation parameter
		g_byOVAEW_Normal = SIV100B_AE_TARGET;
		g_byOVAEB_Normal = SIV100B_AE_TARGET;

		g_byOVAEW_BLC = g_byOVAEW_Normal + 0x20;
		g_byOVAEB_BLC = g_byOVAEB_Normal + 0x20;
	}

	{
		//g_bySV28VoltSel =  SV28_VOL_2V9;
		//	XBYTE[REG_TUNESVIO_SVA] =  SV28_VOL_2V9|SV18_VOL_1V8;
		// hemonel 2010-09-08: support 1.8V sensor IO
#if (_CHIP_ID_ & _RTS5840_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V79;
#elif (_CHIP_ID_ & _RTS5832_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V85;
#elif (_CHIP_ID_ & _RTS5829B_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V82;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V80;
#endif
	}

	{
		InitFormatFrameFps();
	}


}


#ifndef _USE_BK_HSBC_ADJ_
void SetSIV100BBrightness(S16 swSetValue)
{
	U16 wBrightVal;

	if(swSetValue>=0)
	{
		wBrightVal = swSetValue;
	}
	else
	{
		wBrightVal = -swSetValue + 0x80;
	}
	Write_SenReg(0xD7, wBrightVal);	// write Brightness value

	return;
}


void	SetSIV100BContrast(U16 wSetValue)
{
	Write_SenReg(0xD4, wSetValue);//Y contrast gain[5:0]  (x1/16)
	Write_SenReg(0xD5, (wSetValue+4));//Cr  saturation gain[5:0] (x1/16)
	Write_SenReg(0xD6, (wSetValue+4));//Cb  saturation gain[5:0] (x1/16)

	return;
}

void	SetSIV100BSaturation(U16 wSetValue)
{
	Write_SenReg(0xD5, wSetValue);//Cr  saturation gain[5:0] (x1/16)
	Write_SenReg(0xD6, wSetValue);//Cb  saturation gain[5:0] (x1/16)

	return;
}

void	SetSIV100BHue(S16 swSetValue)
{
	U8 byHueCos,byHueSin, byHueCosSign, byHueSinSign;
	U8 byCb, byCr;
	float fHue;

	if(swSetValue>=0)
	{
		byHueSinSign = 0x00;
	}
	else
	{
		byHueSinSign = 0x80;
	}
	if((swSetValue <= 90) &&(swSetValue >= -90))
	{
		byHueCosSign = 0x00;
	}
	else
	{
		byHueCosSign = 0x80;
	}

	fHue = (float)swSetValue/(float)180*3.1415926;
	byHueCos = abs(cos(fHue)*64);
	byHueSin = abs(sin(fHue)*64);

	Read_SenReg(0x64, &byCb);
	Read_SenReg(0x64, &byCr);

	//U=U*cos(a) + V*sin(a)
	byCb = byCb * byHueCos + byCr * byHueSin;

	//V=V*cos(a) + U*sin(a)
	byCr = byCr * byHueCos + byCb * byHueSin;

	Write_SenReg(0x64, byCb);
	Write_SenReg(0x63, byCr);

	return;
}
#endif

void	SetSIV100BSharpness(U8 bySetValue)
{

	U8 bySharp;

	switch(bySetValue)
	{
	case 0:
		bySharp = 0x00;
		break;
	case 1:
		bySharp = 0x05;
		break;
	case 2:
		bySharp = 0x0a; //recommend
		break;
	case 3:
		bySharp = 0x10;
		break;
	case 4:
		bySharp = 0x1a;
		break;
	case 5:
		bySharp = 0x2a;
		break;
	case 6:
		bySharp = 0x3a;
		break;
	default:
		bySharp = 0x4a;
		break;
	}

	Write_SenReg(0xcb, bySharp); // Edge upper gain
	Write_SenReg(0xcc, (bySharp+6)); // Edge down gain
}

void SetSIV100BGain(float fGain)
{
}

void SetSIV100BImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.
{
	/***0x00 normal
		0x01 mirror
		0x02 flip
		0x03 mirror and flip
	***/
	Write_SenReg_Mask(0x04, bySnrImgDir, 0x03);
}


void SetSIV100BOutputDim(U16 wWidth,U16 wHeight)
{
	U8 byVmode;
	U16 byBkWinHeight = 0, byBkWinWidth = 0;
	wWidth = wWidth;

	switch (wHeight)
	{
	case 120:
		byVmode = 0x04;
		break;
	case 144:
		//byVmode = 0x01;
		//---use backend ISP crop the image---JQG__20090818__
		byVmode = 0x05;
		byBkWinHeight = (240 -144)/2;
		byBkWinWidth = (320 -176);
		break;
	case 240:
		byVmode = 0x05;
		break;
	case 288:
		//byVmode = 0x03;
		//byVmode = 0x01;
		//---use backend ISP crop the image---JQG__20090818__
		byVmode = 0x07;
		byBkWinHeight = (480 -288)/2;
		byBkWinWidth = (640 -352);
		break;
	case 480:
	default:
		byVmode = 0x07;
		break;
	}

//	XBYTE[CCS_HSYNC_TCTL0] = byBkWinWidth;
//	XBYTE[CCS_VSYNC_TCTL0] = byBkWinHeight;
	SetBkWindowStart(byBkWinWidth, byBkWinHeight);
	Write_SenReg_Mask(0x05, byVmode, 0x07);
	//WaitTimeOut_Delay(1); //fix display error bug, jqg__used in sensor ISP crop_20090819__
}


void SetSIV100BPwrLineFreq(U8 byFPS, U8 byLightFrq)
{
	U8 byFrqSel, byBD60Base, byBD50Base;
	U16 wTemp;

	byBD60Base = ((U16)byFPS * g_wAECExposureRowMax/60+1)/2;
	byBD50Base = ((U16)byFPS * g_wAECExposureRowMax/50+1)/2;
	if (byLightFrq == PWR_LINE_FRQ_60)
	{
		byFrqSel = 0x00;
	}
	else //if (byLightFrq == PWR_LINE_FRQ_50)
	{
		byFrqSel = 0x02;
	}

	Write_SenReg(0x35, byBD50Base);//---set 50Hz banding filter---//
	Write_SenReg(0x34, byBD60Base);//---set 60Hz banding filter---//

	// hemonel 2009-7-10: image output delay when low fps switch anti-banding register 0x40[1]
	// solution: before switch anti-banding register, close image output and open image output after switch.
	Read_SenReg(0x40, &wTemp);
	if((wTemp&0x0002) != byFrqSel)		// hemonel 2009-7-10: if power line frequency not change, not modify register, windows vista bug
	{
		Write_SenReg(0x03,0x04);
		Write_SenReg_Mask(0x40, byFrqSel, 0x02);
		Write_SenReg(0x03,0x05);
	}
}


void SetSIV100BWBTemp(U16 wSetValue)
{
	OV_CTT_t ctt;

	ctt = OV_SetColorTemperature(wSetValue);

	Write_SenReg(0x7a, ctt.wRgain);
	Write_SenReg(0x7c, ctt.wGgain);
	Write_SenReg(0x7b, ctt.wBgain);

}


void SetSIV100BWBTempAuto(U8 bySetValue)
{
	if(bySetValue)
	{
		Write_SenReg_Mask(0x60, 0xC0, 0xC0);// atuo white balance
	}
	else
	{
		Write_SenReg_Mask(0x60, 0x00, 0xC0);// manual white balance
	}

}


void SetSIV100BBackLightComp(U8 bySetValue)
{
	U8 byAE_TARGET;

	if(bySetValue)
	{
		byAE_TARGET = g_byOVAEW_BLC;
	}
	else
	{
		byAE_TARGET = g_byOVAEW_Normal;
	}

	Write_SenReg(0x41, byAE_TARGET);
}

U16 GetSIV100BAEGain(void)
{
	U16 wTemp;
	U16 wGain;

	Read_SenReg(0x32, &wTemp);

	wGain = (wTemp&0x1F) + 32;	// FGAINx32
	wGain <<=((wTemp>>5)&0x03);

	return (wGain>>1);	// Gainx16
}

void SetSIV100BIntegrationTimeAuto(U8 bySetValue)
{
	if(bySetValue == EXPOSURE_TIME_AUTO_MOD_APERTPRO)
	{
		// auto exposure
		Write_SenReg_Mask(0x40, 0x80,  0x80);
	}
	else
	{
		// manual expsoure
		Write_SenReg_Mask(0x40, 0x00,  0x80);
	}
}

void SetSIV100BIntegrationTime(U16 wEspline)
{
	// write exposure time to register
	Write_SenReg(0x31, INT2CHAR(wEspline, 0));
	Write_SenReg(0x30, INT2CHAR(wEspline, 1));
}
#endif

