#ifndef _CUSISP_H_
#define _CUSISP_H_

void CusISPInit(void);
void CusAWBStaticsStart(void);
void CusAEStaticsStart(void);
void CusAFStaticsStart(void);
void CusAWBAlgorithm(void);
void CusAEAlgorithm(void);
void CusAFAlgorithm(void);
void CusCalledPerFrame(void);

#endif

