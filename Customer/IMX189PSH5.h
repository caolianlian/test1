#ifndef _IMX189PSH5_H_
#define _IMX189PSH5_H_

void IMX189PSH5SetFormatFps(U16 SetFormat, U8 Fps);
void CfgIMX189PSH5ControlAttr(void);
void SetIMX189PSH5ImgDir(U8 bySnrImgDir);
void SetIMX189PSH5IntegrationTime(U16 wEspline);
void SetIMX189PSH5Exposuretime_Gain(float fExpTime, float fTotalGain);
void InitIMX189PSH5IspParams(void );
void InitIMX189PSH5IspParamsByResolution(U16 wCurWidth, U16 wCurHeight);
void IMX189PSH5_POR(void );
void SetIMX189PSH5DynamicISP(U8 byAEC_Gain);
void SetIMX189PSH5DynamicISP_AWB(U16 wColorTempature);
#endif // _IMX189PSH5_H_

