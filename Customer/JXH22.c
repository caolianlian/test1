#include "inc.h"

#if (defined RTS58XX_SP_JXH22) 

//void InitJXH22IspParamByFormat(U8 SetFormat);

/*
*********************************************************************************************************
*                                 manual white balance control parameter
* 
*	1st column: color-temperature. color-temperature must be from little to big
*	2nd column: Rgain
*	3rd column: Ggain
*	4th column: Bgain
*
*	this array used for manual white balance control, these data are get from Judge II light box
*	
*********************************************************************************************************
*/
OV_CTT_t code gc_JXH22_CTT[3] =
{
	{3000,0xEF,0x100,0x2AC},
	{4150,0x17C,0x100,0x226},
	{6500,0x1BF,0x100,0x122},
};


code OV_FpsSetting_t  g_staJXH22HD720PFpsSetting[]=
{
//	FPS 		ExtraDummyPixel  clkrc		pclk
#ifdef _MIPI_EXIST_
	{5, 	(5124),		(0x80), 	38430000},
	{8, 	(3416),		(0x80), 	38430000},
	{10,	(2562),		(0x80), 	38430000},
	{15,	(1708), 	(0x80), 	38430000},
	{20,    (1281),     (0x80),     38430000},
    {25,    (1025),     (0x80),     38430000},
	{30,	(854),		(0x80), 	38430000},
#else
	{5, 	(854),		(0x03), 	6405000},
	{8, 	(854),		(0x02), 	38430000},
	{10,	(854),		(0x43), 	12810000},
	{15,	(854),		(0x01), 	19215000},
	{20,	(1281), 	(0x80), 	38430000},
	{25,	(1025), 	(0x80), 	38430000},
	{30,	(854),		(0x80), 	38430000},

#endif
};

/*
2012-09-14 darcy_lu

In JXH22 , MIPI clock and pixel clock is set by different path

MIPI bps per lane = MIPI clock * 2 = HCLK /PRE_DIV0 * DIV124 * (129 - DIVCNT7B) / (SDIV0 + 1) / (MIPI_DIV + 1)
                                                  = HCLK /(0x3003[2:0]) * 0x3003[7:6] * (129 - 0x3004[6:0]) / (0x3005[3:0]) / (0x3005[7:4])
                                                  
in OV setting , MIPI clock is large more then (pixel clock *10/2/(lane number)). In this case , MIPI DHY clock in RLE0590 is large than ISP clock , 
data can not processed by ISP module, so we can adjust    DIVCNT7B to reduce sensor MIPI clock and MIPI DPHY clock                                               
                                                  
*/
/*
*********************************************************************************************************
*                                 	Sensor FPS Parameter setting 
* 
*	1st column: FPS. FPS order must be from little to big
*	2nd column: Dummy Pixel. sensor extra dummy pixel or frame lines
*	3rd column: PLL.	adjust sensor PLL 
*	4th column: PCLK. sensor one-pixel output clock frequency, unit of Hz. If YUYV data output, this clock is the half of PCLK
*********************************************************************************************************
*/

#ifdef _MIPI_EXIST_
t_RegSettingBB code gc_JXH22_HD_30fps_MIPI_Setting[] =
{
	//;=========================================
	//;INI Create Date : 2014/10/22
	//;Terra Ver : Terra20141016-00
	//;Create By easonlin
	//;==================INI==================
	//;;Output Detail:
	//;;MCLK:24 MHz
	//;;PCLK:38.4
	//;;Mipi PCLK:38.4
	//;;VCO:384
	//;;FrameW:1500
	//;;FrameH:854


	//[JXH22_1280x720x30_Mipi_1L_10b.reg]
	//;;INI Start
	{0x12, 0x40},
	//;;DVP Setting
	{0x1F, 0x00},
	//;;PLL Setting
	{0x0E, 0x1D},
	{0x0F, 0x09},
	{0x10, 0x20},
	{0x11, 0x80},
	//;;Frame/Window
	{0x20, 0xDC},
	{0x21, 0x05},
	{0x22, 0x56},
	{0x23, 0x03},
	{0x24, 0x00},
	{0x25, 0xD0},
	{0x26, 0x25},
	{0x27, 0xBB},
	//{0x28, 0x0D},
	{0x28,0x0E}, //20141020 Modify by SOI for correct pixel array
	{0x29, 0x00},
	{0x2A, 0xAC},
	{0x2B, 0x10},
	{0x2C, 0x01},
	{0x2D, 0x0A},
	{0x2E, 0xC2},
	{0x2F, 0x20},
	//;;Interface
	{0x1D, 0x00},
	{0x1E, 0x00},
	{0x6C, 0x10},
	{0x73, 0x33},
	{0x70, 0x69},
	{0x76, 0x40},
	{0x77, 0x06},
	{0x67, 0x30},
	{0x6D, 0x09},
	//;;AE/AG/ABLC
	{0x13, 0xC7},
	{0x14, 0x80},
	{0x16, 0xC0},
	{0x17, 0x40},
	{0x18, 0xD5},
	{0x19, 0x04},
	{0x37, 0x35},
	{0x38, 0x98},
	{0x4a, 0x03},
	{0x49, 0x10},
	//;;Array/AnADC/PWC
	{0x69, 0x32},
	//;;INI End
	{0x12, 0x00},
	//;;PWDN Setting
};
#else
t_RegSettingBB code gc_JXH22_HD_30fps_Setting[] =
{		
    //Output Detail:
    //MCLK:24 MHz
    //PCLK:38.4
    //VCO:384
    //FrameW:1500
    //FrameH:854

    //JXH22_1280x720x30_DVP_10b
    //INI Start
    {0x12,0x40},
    //DVP Setting
    {0x1F,0x00},
    //PLL Setting
    {0x0E,0x1D},
    {0x0F,0x09},
    {0x10,0x20},
    {0x11,0x80},
    //Frame/Window
    {0x20,0xDC},
    {0x21,0x05},
    {0x22,0x56},
    {0x23,0x03},
    {0x24,0x00},
    {0x25,0xD0},
    {0x26,0x25},
    {0x27,0xBB},
    //{0x28,0x0D},
    {0x28,0x0E}, //20141020 Modify by SOI for correct pixel array
    {0x29,0x00},
    {0x2A,0xAC},
    {0x2B,0x10},
    {0x2C,0x01},
    {0x2D,0x0A},
    {0x2E,0xC2},
    {0x2F,0x20},
    //Interface
    {0x1D,0xFF},
    //{0x1E,0x1F},
    {0x1E,0x9F}, //20141020 Modify by SOI for contour issue
    {0x6C,0x90},
    {0x73,0xB3},
    {0x70,0x69},
    {0x76,0x40},
    {0x77,0x06},
    //AE/AG/ABLC
    {0x13,0xC7},
    {0x14,0x80},
    {0x16,0xC0},
    {0x17,0x40},
    {0x18,0xD5},
    {0x19,0x04},
    {0x37,0x35},
    {0x38,0x98},
    {0x4a,0x03},
    {0x49,0x10},
    //Array/AnADC/PWC
    {0x69,0x74},
    //INI End
    {0x12,0x00},
    //PWDN Setting				
};
#endif


static OV_FpsSetting_t*  GetOvFpsSetting(U8 Fps, OV_FpsSetting_t staOvFpsSetting[], U8 byArrayLen)
{
    U8 i;
    U8 Idx;

    ISP_MSG((" array size = %bd\n", byArrayLen));

    if(g_bIsHighSpeed)
    {
        Idx=2; //8fps for uxga, 10 fps for vga
    }
    else
    {
        Idx = 0;
    }

    for(i=0; i< byArrayLen; i++)
    {
        ISP_MSG((" fps i = %bd\n",i));
        if(staOvFpsSetting[i].byFps ==Fps)
        {
            Idx=i;
            break;
        }
    }
    ISP_MSG((" get fps idx = %bd\n",Idx));
    //g_byHIDBtnLast = Fps;
    return &staOvFpsSetting[Idx];
}

/*
*********************************************************************************************************
*                                         Mapping AE gain to sensor gain register 
* FUNCTION MapSnrGlbGain2SnrRegSetting
*********************************************************************************************************
*/
static U16 MapSnrGlbGain2SnrRegSetting(U16 wGain)
{
    U16  sensorGain=0;
    //float temp;
    U8  i;

    //g_byHIDBtnLast = wGain;
    for(i=0;i<4;i++)
	//for(i=0;i<3;i++)
	{
        if(wGain >= 32)
        {
            wGain >>= 1;
            sensorGain |=  (0x01<<(i+4));
        }
        else
        {
            sensorGain |= (wGain-16);
            break;
        }   
    }

    return sensorGain;
}

float SnrRegSetting2SnrGlbGain(U16 wSnrRegGain)
{
	U16 wGain;
	float fGainOffset;
	U8 i;

	if(wSnrRegGain >= 0x70)
	{
		fGainOffset = 1.1; //0.99933; //0.5;
	}
	else if(wSnrRegGain >= 0x30)
	{
		fGainOffset = 0.360577; //0.32349; //0.2;
	}
	else if(wSnrRegGain >= 0x10)
	{
		fGainOffset = 0.107287; //0.11564; //0.0625;
	}
	else
	{
		fGainOffset = 0;
	}

	wGain = (wSnrRegGain&0x0f) + 16;
	wSnrRegGain >>= 4;

	for(i = 0; i < 4; i++)
	{
		if((wSnrRegGain&1) == 1)
		{
			wGain <<= 1;
		}

		wSnrRegGain >>= 1;
	}

	//return ((float)wGain)/16.0-fGainOffset;
	//return (((float)wGain)/16.0-fGainOffset-1.0)*1.27 + 1.0;
	return (((float)wGain)/16.0-fGainOffset-1.0)*1.2 + 1.0;
}

//this function is also called during Probe stage to calculate the transfer_size/LWM/HWM
void GetSensorPclkHsync(U16 wSensorSPFormat,U8 byFps)
{
	wSensorSPFormat = wSensorSPFormat;
	byFps = byFps;
}

void JXH22SetFormatFps(U16 SetFormat, U8 Fps)
{
	U16 wTemp;
	OV_FpsSetting_t *pJXH22_FpsSetting;
	
	// hemonel 2010-04-13: for delete complier warning
	SetFormat = SetFormat;
	
	//g_byHIDBtnLast = Fps;
	pJXH22_FpsSetting=GetOvFpsSetting(Fps, g_staJXH22HD720PFpsSetting, sizeof(g_staJXH22HD720PFpsSetting)/sizeof(OV_FpsSetting_t));
	// initial all register setting

#ifdef _MIPI_EXIST_
	WriteSensorSettingBB(sizeof(gc_JXH22_HD_30fps_MIPI_Setting)/2, gc_JXH22_HD_30fps_MIPI_Setting);
#else
	WriteSensorSettingBB(sizeof(gc_JXH22_HD_30fps_Setting)/2, gc_JXH22_HD_30fps_Setting);
#endif

	// write fps setting
	Write_SenReg(0x11, pJXH22_FpsSetting->byClkrc);
	wTemp = pJXH22_FpsSetting->wExtraDummyPixel;
	Write_SenReg(0x022, INT2CHAR(wTemp, 0)); //Write dummy pixel MSB
	Write_SenReg(0x023, INT2CHAR(wTemp, 1)); //Write dummy pixel LSB

	// update firmware variable
	g_dwPclk = pJXH22_FpsSetting->dwPixelClk;		 // this for scale speed
	g_wSensorHsyncWidth = 1500;//1320;
	//g_wAECExposureRowMax = 833-3;//948;	
	//g_wAEC_LineNumber = 833;	  // this for AE insert dummy line algothrim
	g_wAECExposureRowMax = wTemp-3;//948;	
	g_wAEC_LineNumber = wTemp;	  // this for AE insert dummy line algothrim


	g_wSensorWidthBefBLC = 1280;
	g_wSensorHeightBefBLC = 720;

	GetSensorPclkHsync(HD720P_FRM,Fps);


#ifdef _MIPI_EXIST_
	//-----------------------------Dphy Setting--------------------------------
	SetMipiDphy(MIPI_DATA_LANE0_EN, \
	            MIPI_DATA_FORMAT_RAW10 , MIPI_DATA_TYPE_RAW10, \
	            HSTERM_EN_TIME_33);
#endif
}

void CfgJXH22ControlAttr(void)
{
	U8 i;
	
	g_bySensorSize = SENSOR_SIZE_HD720P;
	g_wSensorSPFormat = HD720P_FRM;
	memcpy(g_asOvCTT, gc_JXH22_CTT,sizeof(gc_JXH22_CTT));
	
#if (_CHIP_ID_ & _RTS5840_)
	XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
	XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V79;
#elif (_CHIP_ID_ & _RTS5832_)
	XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
	XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V85;
#elif (_CHIP_ID_ & _RTS5829B_)
	XBYTE[REG_TUNESVA] =  SVA_VOL_2V73;
	XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V80;
#endif

	//--preview--
	g_aVideoFormat[0].byaVideoFrameTbl[0] = 8;		// resolution number	
	g_aVideoFormat[0].byaVideoFrameTbl[1]=F_SEL_640_480;
	g_aVideoFormat[0].byaVideoFrameTbl[2]=F_SEL_320_180;
	g_aVideoFormat[0].byaVideoFrameTbl[3]=F_SEL_320_240;
	g_aVideoFormat[0].byaVideoFrameTbl[4]=F_SEL_424_240;
	g_aVideoFormat[0].byaVideoFrameTbl[5]=F_SEL_640_360;
	g_aVideoFormat[0].byaVideoFrameTbl[6]=F_SEL_848_480;
	g_aVideoFormat[0].byaVideoFrameTbl[7]=F_SEL_960_540;
	g_aVideoFormat[0].byaVideoFrameTbl[8]=F_SEL_1280_720;
	//--still image--
	g_aVideoFormat[0].byaStillFrameTbl[0] = 2;	// resolution number	
	g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_640_480;
	g_aVideoFormat[0].byaStillFrameTbl[2]=F_SEL_1280_720;		
	//====== fps  setting ===========
	for(i=0;i<15;i++)
	{
	 	g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_30;			
	}

	g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_848_480]= FPS_10;
	g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_960_540]= FPS_10;
	g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_1280_720]= FPS_10;
	
	//====== format type  setting ===========
	g_aVideoFormat[0].byFormatType = FORMAT_TYPE_YUY2;	

#ifdef _ENABLE_MJPEG_
	// -- preview ---
	g_aVideoFormat[1].byaVideoFrameTbl[0] = 8;		// resolution number	
	g_aVideoFormat[1].byaVideoFrameTbl[1]=F_SEL_640_480;
	g_aVideoFormat[1].byaVideoFrameTbl[2]=F_SEL_320_180;
	g_aVideoFormat[1].byaVideoFrameTbl[3]=F_SEL_320_240;
	g_aVideoFormat[1].byaVideoFrameTbl[4]=F_SEL_424_240;
	g_aVideoFormat[1].byaVideoFrameTbl[5]=F_SEL_640_360;
	g_aVideoFormat[1].byaVideoFrameTbl[6]=F_SEL_848_480;
	g_aVideoFormat[1].byaVideoFrameTbl[7]=F_SEL_960_540;
	g_aVideoFormat[1].byaVideoFrameTbl[8]=F_SEL_1280_720;


	//--still image--
	g_aVideoFormat[1].byaStillFrameTbl[0] = 2;	// resolution number	
	g_aVideoFormat[1].byaStillFrameTbl[1]=F_SEL_1280_720;
	g_aVideoFormat[1].byaStillFrameTbl[2]=F_SEL_640_480;	
	for(i=0;i<15;i++)
	{
		g_aVideoFormat[1].waFrameFpsBitmap[i] = FPS_30|FPS_25|FPS_20|FPS_15|FPS_10|FPS_5;
	}		
// modify MJPEG format type
	g_aVideoFormat[1].byFormatType = FORMAT_TYPE_MJPG;	
#endif	

	InitJXH22IspParams();
}	
		

//called when manual AE
void SetJXH22IntegrationTime(U16 wEspline)
{
	U16 wFrameLenLines;
	U16 wExpMax = g_wAECExposureRowMax-2;
	
	  if(wEspline > wExpMax)
	  {
		  wFrameLenLines = wEspline + 20;
	  }
	  else
	  {
		  wFrameLenLines = g_wAEC_LineNumber;
	  }
	
	  if (wFrameLenLines%2==1)
	  {
		  wFrameLenLines++;
	  }
	
	
	  //-----------Write  frame length or dummy lines------------
	  Write_SenReg(0x22, INT2CHAR(wFrameLenLines, 0));
	  Write_SenReg(0x23, INT2CHAR(wFrameLenLines, 1));
	
	  //-----------Write Exposuretime setting-----------
	  Write_SenReg(0x01, INT2CHAR(wEspline, 0));	// change exposure value low
	  Write_SenReg(0x02, INT2CHAR(wEspline, 1));	// change exposure value high
	
	  //----------- write gain setting-------------------
	 // Write_SenReg(0x0205, 0x0f); // fixed gain at manual exposure control
	 // Write_SenReg(0x0204, 0);
	  Write_SenReg(0x00, 0x0f);
}

void SetJXH22ImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.		
{	
	U16 wRegValue;
	
#ifdef _MIPI_EXIST_
	SetBLCWindowStart(0, 0);
#else
	SetBkWindowStart(0, 0);
#endif


	Read_SenReg(0x12,&wRegValue);//bit5 mirror;bit4 flip
	wRegValue = wRegValue&(~0x30);
	switch(bySnrImgDir)
	{
		case 0:
			Write_SenReg(0x12,wRegValue|0x00);
			break;
		case 1:
			Write_SenReg(0x12,wRegValue|0x10);
			break;
		case 2:
			Write_SenReg(0x12,wRegValue|0x20);
			break;
		case 3:
			Write_SenReg(0x12,wRegValue|0x30);
			break;
		default:
			break;
	}
}

/****************************************************************************************************
 Internal algorithm AE gain is in unit of 1/16. But the unit of sensor gain register is not the same as the Internal algorithm AE gain.
 This function will transform the Internal algorithm AE gain to Sensor gain. This function is called by SetJXH22Exposuretime_Gain().

  \param 
  	wGain 		- the Internal algorithm AE gain in unit of 1/16.  	

  \retval 
  	the sensor gain which will be directly writen to sensor gain regsiter.
****************************************************************************************************/

void SetJXH22Exposuretime_Gain(float fExpTime, float fTotalGain)
{

	U16 wExposureRows_floor;
	U16  wGainRegSetting; //data
	float fSnrGlbGain;
	float fExposureRows;
	U16 wSetDummy;
	//float fTemp;
	//U16 wExposurePixels;

	//wGainRegSetting = MapSnrGlbGain2SnrRegSetting((U16)(fTotalGain*16.0));
	//wGainRegSetting = MapSnrGlbGain2SnrRegSetting((U16)(((fTotalGain-1.0)/1.27+1.0)*16.0));
	//wGainRegSetting = MapSnrGlbGain2SnrRegSetting((U16)(((fTotalGain-1.0)/1.2+1.0)*16.0));
	wGainRegSetting=MapSnrGlbGain2SnrRegSetting((U16)(fTotalGain*16.0));
	if((wGainRegSetting != 0) && ((wGainRegSetting&0xf) == 0))
	{
		wGainRegSetting = ((wGainRegSetting>>1)|0xf);
	}
	
	fSnrGlbGain = SnrRegSetting2SnrGlbGain(wGainRegSetting);
	
	fExposureRows = fExpTime/g_fSensorRowTimes;	//unit : 1row
	wExposureRows_floor = (U16)(fExposureRows);

	// write exposure, gain, dummy line
    if(fabs(g_fCurExpTime-fExpTime) > 0.0001)
    {
        // set dummy
        wSetDummy = g_wAFRInsertDummylines + g_wAEC_LineNumber;

            
	     //-----------Write  frame length or dummy lines------------
	     if (wSetDummy%2 == 1)
	     {
	     	wSetDummy++;
	     }
        
		Write_SenReg(0x22, INT2CHAR(wSetDummy, 0));
		Write_SenReg(0x23, INT2CHAR(wSetDummy, 1));

        //-----------Write Exposuretime setting-----------
        Write_SenReg(0x01, INT2CHAR(wExposureRows_floor, 0)); // change exposure value low
        Write_SenReg(0x02, INT2CHAR(wExposureRows_floor, 1)); // change exposure value high


		WaitFrameSync(ISP_INT1, ISP_DATA_START_INT);
		
		//----------- write gain setting-------------------
        Write_SenReg(0x00, INT2CHAR(wGainRegSetting, 0));

		
        ///Write_SenReg(0x0Ca, 0x00);
        //Write_SenReg(0x0Cb, INT2CHAR(wGainRegSetting, 0)); 
		

		//SetISPAEGain(fTotalGain, fSnrGlbGain, 1);        

        // group register release
        //Write_SenReg_Mask(0x12, 0x08, 0x08);
        g_fCurExpTime = fExpTime;
    }
    else
    {
		wSetDummy = g_wAFRInsertDummylines + g_wAEC_LineNumber;
		
		//-----------Write  frame length or dummy lines------------
		if (wSetDummy%2 == 1)
		{
			wSetDummy++;
		}
		
		 //if(g_byCommitFPS >10) 
         {
         
	         Write_SenReg(0x22, INT2CHAR(wSetDummy, 0));
	         Write_SenReg(0x23, INT2CHAR(wSetDummy, 1));

			//-----------Write Exposuretime setting-----------
       		 Write_SenReg(0x01, INT2CHAR(wExposureRows_floor, 0)); // change exposure value low
       		 Write_SenReg(0x02, INT2CHAR(wExposureRows_floor, 1)); // change exposure value high
	         
         }	

		WaitFrameSync(ISP_INT1, ISP_DATA_START_INT);
		
        // write gain setting
        Write_SenReg(0x00, INT2CHAR(wGainRegSetting, 0));

		//Write_SenReg(0x0C4, 0x00);
        //Write_SenReg(0x0C5, INT2CHAR(wGainRegSetting, 0)); 
        
		//SetISPAEGain(fTotalGain, fSnrGlbGain, 1);   
       
        // group register release
        //Write_SenReg_Mask(0x12, 0x08, 0x08);
    }
    return;

}

void JXH22_POR(void)
{
#ifdef _MIPI_EXIST_
	Init_MIPI();
#else
	Init_CCS();
#endif
	ENTER_SENSOR_RESET();
	
	// power on
	SensorPowerControl(SWITCH_ON);
	uDelay(1);

	// Initialize I2C Controller Timing
	LEAVE_SENSOR_RESET();

	// clock select 24M
	if( g_bySSCEnable== TRUE)
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_SSC_24M);
	}
	else
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_96M|CCS_CLK_DIVIDER_4);
	}

	EnableSensorHCLK();
	g_bySensorIsOpen = SENSOR_OPEN;
	WaitTimeOut_Delay(1);  //JXH22 spec request 8192clk
}

/*
*********************************************************************************************************
*                                 	Backend ISP IQ Parameter setting 
* 
*********************************************************************************************************
*/
void InitJXH22IspParams(void)
{
	// D50 R/G/B gain
	g_wAWBRGain_Last= 0x136;	
	g_wAWBGGain_Last= 0x100;
	g_wAWBBGain_Last= 0x19D;
	g_wProjectGR = g_wAWBRGain_Last;
	g_wProjectGB = g_wAWBBGain_Last;

	g_bySaturation_Def =64;
	g_byContrast_Def = 32;

	//g_wDynamicISPEn = DYNAMIC_LSC_CT_EN|DYNAMIC_SHARPPARAM_EN|DYNAMIC_CCM_BRIGHT_EN;
	g_wDynamicISPEn =  0;
	
#ifdef _ENABLE_MJPEG_
	g_byQtableScale=6;
#endif
}

void SetJXH22DynamicISP(U8 byAEC_Gain)
{
	byAEC_Gain = byAEC_Gain;
	
}

void SetJXH22DynamicISP_AWB(U16 wColorTempature)
{
	wColorTempature =wColorTempature;	
}

void SetJXH22SoftReset(void )
{
	return;	
}
#endif
