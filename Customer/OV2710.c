#include "Inc.h"

#ifdef RTS58XX_SP_OV2710

void InitOV2710IspParamByFormat(SetFormat);

/*
*********************************************************************************************************
*                                 manual white balance control parameter
*
*	1st column: color-temperature. color-temperature must be from little to big
*	2nd column: Rgain
*	3rd column: Ggain
*	4th column: Bgain
*
*	this array used for manual white balance control, these data are get from Judge II light box
*
*********************************************************************************************************
*/
OV_CTT_t code gc_OV2710_CTT[3] =
{
	{3000,0xEF,0x100,0x2AC},
	{4150,0x17C,0x100,0x226},
	{6500,0x1BF,0x100,0x122},
};

/*
*********************************************************************************************************
*                                 	Sensor FPS Parameter setting
*
*	1st column: FPS. FPS order must be from little to big
*	2nd column: Dummy Pixel. sensor extra dummy pixel or frame lines
*	3rd column: PLL.	adjust sensor PLL
*	4th column: PCLK. sensor one-pixel output clock frequency, unit of Hz. If YUYV data output, this clock is the half of PCLK
*********************************************************************************************************
*/
code OV_FpsSetting_t  g_staOV2710HD720FpsSetting[]=
{
	// FPS ExtraDummyPixel clkrc 	   pclk
	{5,		(1792),			(0xB0), 		  6666667},
	{8,		(2240),			(0x50), 		13333333},
	{9,		(1992),			(0x50), 		13333333},
	{10,		(1792),			(0x50), 		13333333},
	{15,		(1792),			(0x30), 		20000000},
	{20,		(1792),			(0x20), 		26666667},
	{25,		(2150),			(0x10),		40000000},
	{30,		(1792),			(0x10),		40000000},
};

code OV_FpsSetting_t  g_staOV2710FHDFpsSetting[]=
{
	// FPS ExtraDummyPixel clkrc 	   pclk
	{5,		(2420),			(0x50), 		13333333},
	{8,		(3026),			(0x20), 		26666667},
	{9,		(2690),			(0x20), 		26666667},
	{10,		(2420),			(0x20), 		26666667},
	{15,		(2420),			(0x10), 		40000000},
	{20,		(3630),			(0x00), 		80000000},
	{25,		(2904),			(0x00),		80000000},
	{30,		(2420),			(0x00),		80000000},
};

/*
*********************************************************************************************************
*                                 	Sensor Register setting
* param:
*	the 1st column: sensor register address
*	the 2nd column: sensor register value
*********************************************************************************************************
*/
t_RegSettingWB code gc_OV2710_HD720_Setting[] =
{
	// OV2710R1C_A10.ovd DVP 1280x720 setting
	{0x3103, 0x03},
	{0x3008, 0x82},
	{0x3017, 0x7f},
	{0x3018, 0xfc},
	{0x3706, 0x61},
	{0x3712, 0x0c},
	{0x3630, 0x6d},
	{0x3801, 0xb4},
	{0x3621, 0x04},
	{0x3604, 0x60},
	{0x3603, 0xa7},
	{0x3631, 0x26},
	{0x3600, 0x04},
	{0x3620, 0x37},
	{0x3623, 0x00},
	{0x3702, 0x9e},
	{0x3703, 0x74},
	{0x3704, 0x10},
	{0x370d, 0x0f},
	{0x3713, 0x8b},
	{0x3714, 0x74},
	{0x3710, 0x9e},
	{0x3801, 0xc4},
	{0x3605, 0x05},
	{0x3606, 0x12},
	{0x302d, 0x90},
	{0x370b, 0x40},
	{0x3716, 0x31},
	{0x380d, 0x74},
	{0x5181, 0x20},
	{0x518f, 0x00},
	{0x4301, 0xff},
	{0x4303, 0x00},
	{0x3a00, 0x78},
	{0x300f, 0x88},
	{0x3011, 0x28},
	{0x3a1a, 0x06},
	{0x3a18, 0x00},
	{0x3a19, 0x7a},
	{0x3a13, 0x54},
	{0x382e, 0x0f},
	{0x381a, 0x1a},
	{0x381c, 0x10},
	{0x381d, 0xb8},
	{0x381e, 0x02},
	{0x381f, 0xdc},
	{0x3820, 0x0a},
	{0x3821, 0x29},
	{0x3804, 0x05},
	{0x3805, 0x00},
	{0x3806, 0x02},
	{0x3807, 0xd0},
	{0x3808, 0x05},
	{0x3809, 0x00},
	{0x380a, 0x02},
	{0x380b, 0xd0},
	{0x380e, 0x02},
	{0x380f, 0xe8},
	{0x380c, 0x07},
	{0x380d, 0x00},
	{0x5688, 0x03},
	{0x5684, 0x05},
	{0x5685, 0x00},
	{0x5686, 0x02},
	{0x5687, 0xd0},
	{0x3a08, 0x1b},
	{0x3a09, 0xe6},
	{0x3a0a, 0x17},
	{0x3a0b, 0x40},
	{0x3a0e, 0x01},
	{0x3a0d, 0x02},
	{0x3a0f, 0x40},
	{0x3a10, 0x38},
	{0x3a1b, 0x48},
	{0x3a1e, 0x30},
	{0x3a11, 0x90},
	{0x3a1f, 0x10},

	{0x3010, 0x10},

	// hemonel 2010-12-01: vertical output 1082 column for crop
	{0x380A, 0x02},
	{0x380B, 0xd2},

	// hemonel 2010-12-03: set sensor ISP function
	{0x5000, 0x5F},	// disable LENC, enable dead pixel cancel
	{0x5001, 0x4E},	// disable AWB
	{0x3503, 0x17},	// disable AEC
};

t_RegSettingWB code gc_OV2710_FHD_Setting[] =
{
	// OV2710R1C_A10.ovd DVP 1920x1080 setting
	{0x3103, 0x03},
	{0x3008, 0x82},
	{0x3017, 0x7f},
	{0x3018, 0xfc},
	{0x3706, 0x61},
	{0x3712, 0x0c},
	{0x3630, 0x6d},
	{0x3801, 0xb4},
	{0x3621, 0x04},
	{0x3604, 0x60},
	{0x3603, 0xa7},
	{0x3631, 0x26},
	{0x3600, 0x04},
	{0x3620, 0x37},
	{0x3623, 0x00},
	{0x3702, 0x9e},
	{0x3703, 0x74},
	{0x3704, 0x10},
	{0x370d, 0x0f},
	{0x3713, 0x8b},
	{0x3714, 0x74},
	{0x3710, 0x9e},
	{0x3801, 0xc4},
	{0x3605, 0x05},
	{0x3606, 0x12},
	{0x302d, 0x90},
	{0x370b, 0x40},
	{0x3716, 0x31},
	{0x380d, 0x74},
	{0x5181, 0x20},
	{0x518f, 0x00},
	{0x4301, 0xff},
	{0x4303, 0x00},
	{0x3a00, 0x78},
	{0x300f, 0x88},
	{0x3011, 0x28},
	{0x3a1a, 0x06},
	{0x3a18, 0x00},
	{0x3a19, 0x7a},
	{0x3a13, 0x54},
	{0x382e, 0x0f},
	{0x381a, 0x1a},
	{0x5688, 0x03},
	{0x5684, 0x07},
	{0x5685, 0xa0},
	{0x5686, 0x04},
	{0x5687, 0x43},
	{0x3a0f, 0x40},
	{0x3a10, 0x38},
	{0x3a1b, 0x48},
	{0x3a1e, 0x30},
	{0x3a11, 0x90},
	{0x3a1f, 0x10},

	{0x3010, 0x00},

	// hemonel 2010-12-01: vertical output 1082 column for crop
	{0x380A, 0x04},
	{0x380B, 0x3a},

	{0x380E, 0x04},
	{0x380F, 0x4E},
	
	// hemonel 2010-12-03: set sensor ISP function
	{0x5000, 0x5F},	// disable LENC, enable dead pixel cancel
	{0x5001, 0x4E},	// disable AWB
	{0x3503, 0x17},	// disable AEC
};

#ifdef _MIPI_EXIST_
t_RegSettingWB code  gc_OV2710_FHD_mipi_Setting[] =
{

	{0x3804, 0x07},     //1924
	{0x3805, 0x84},
	{0x3806, 0x04},     //1281
	{0x3807, 0x39},
	{0x3808, 0x07},     //1924
	{0x3809, 0x84},
	{0x380A, 0x04},     //1281
	{0x380B, 0x39},

	{0x300f, 0xc3},
	{0x3011, 0x0a},

	{0x3017, 0x00},     //PAD Output Enable
	{0x3018, 0x00},     //PAD Output Enable
	{0x300e, 0x04},     //MIPI Enable
	{0x4801, 0x0f},     //mipi

};

t_RegSettingWB code  gc_OV2710_HD720_mipi_Setting[] =
{

	{0x3804, 0x05},     //1284
	{0x3805, 0x04},
	{0x3806, 0x02},     //724
	{0x3807, 0xD4},
	{0x3808, 0x05},     //1284
	{0x3809, 0x04},
	{0x380A, 0x02},     //724
	{0x380B, 0xD4},

	{0x300f, 0xc3},
	{0x3011, 0x0a},

	{0x3017, 0x00},     //PAD Output Enable
	{0x3018, 0x00},     //PAD Output Enable
	{0x300e, 0x04},     //MIPI Enable
	{0x4801, 0x0f},     //mipi
};

#endif

/*
*********************************************************************************************************
*                                         Find FPS Setting pointer
* FUNCTION GetOvFpsSetting
*********************************************************************************************************
*/
/**
  Find FPS setting pointer in FPS setting array. These setting will be writen to sensor register so that sensor will preview with the
  FPS. This function is called by OV2710SetFormatFps() function.

  \param
  	Fps 		- the frame rate to be found
  	staOvFpsSetting        - the FPS setting array pointer
  	byArrayLen        - the length of the FPS setting array

  \retval
  	If find, return	the expected FPS setting pointer.
  	If not find, use default FPS setting pointer.
 *********************************************************************************************************
*/
static OV_FpsSetting_t*  GetOvFpsSetting(U8 Fps, OV_FpsSetting_t staOvFpsSetting[], U8 byArrayLen)
{
	U8 i;
	U8 Idx;

	ISP_MSG((" array size = %bd\n", byArrayLen));

	if(g_bIsHighSpeed)
	{
		Idx=2; //8fps for uxga, 10 fps for vga
	}
	else
	{
		Idx = 0;
	}

	for(i=0; i< byArrayLen; i++)
	{
		ISP_MSG((" fps i = %bd\n",i));
		if(staOvFpsSetting[i].byFps ==Fps)
		{
			Idx=i;
			break;
		}
	}
	ISP_MSG((" get fps idx = %bd\n",Idx));
	return &staOvFpsSetting[Idx];
}

/*
*********************************************************************************************************
*                                         Mapping AE gain to sensor gain register
* FUNCTION MapSnrGlbGain2SnrRegSetting
*********************************************************************************************************
*/
/**
 Internal algorithm AE gain is in unit of 1/16. But the unit of sensor gain register is not the same as the Internal algorithm AE gain.
 This function will transform the Internal algorithm AE gain to Sensor gain. This function is called by SetOV2710Exposuretime_Gain().

  \param
  	byGain 		- the Internal algorithm AE gain in unit of 1/16.

  \retval
  	the sensor gain which will be directly writen to sensor gain regsiter.
 *********************************************************************************************************
*/
/*static U16 MapSnrGlbGain2SnrRegSetting(float fTotalGain)
{
	U16 wGain = (U16)(fTotalGain*16.0);
	U16  sensorGain=0;
	U8  i;
	//QDBG(("byGain = %bd-> ",byGain));

	for(i=0; i<4; i++)
	{
		if(wGain >= 32)
		{
			wGain >>= 1;
			sensorGain |=  (0x01<<(i+4));
		}
		else
		{
			sensorGain |= (wGain-16);
			break;
		}
	}

	//QDBG(("sensorGain = %d    ",sensorGain));
	return sensorGain;
}

float SnrRegSetting2SnrGlbGain(U16 wSnrRegGain)
{
	U16 wGain;
	U8 i;

	wGain = (wSnrRegGain&0x0f) + 16;
	wSnrRegGain >>= 4;

	for(i = 0; i < 4; i++)
	{
		if((wSnrRegGain&1) == 1)
		{
			wGain <<= 1;
		}

		wSnrRegGain >>= 1;
	}

	return (float)wGain/16.0;
}*/

static U16 MapSnrGlbGain2SnrRegSetting(float fTotalGain)
{
	U16 wGain = (U16)(((fTotalGain-1.0)/1.19559 + 1.0)*16.0);
	U16  sensorGain=0;
	U8  i;
	//QDBG(("byGain = %bd-> ",byGain));

	for(i=0; i<4; i++)
	{
		if(wGain >= 32)
		{
			wGain >>= 1;
			sensorGain |=  (0x01<<(i+4));
		}
		else
		{
			sensorGain |= (wGain-16);
			break;
		}
	}

	//QDBG(("sensorGain = %d    ",sensorGain));
	return sensorGain;
}

float SnrRegSetting2SnrGlbGain(U16 wSnrRegGain)
{
	U16 wGain;
	U8 i;

	wGain = (wSnrRegGain&0x0f) + 16;
	wSnrRegGain >>= 4;

	for(i = 0; i < 4 && wSnrRegGain != 0; i++)
	{
		if((wSnrRegGain&1) == 1)
		{
			wGain <<= 1;
		}

		wSnrRegGain >>= 1;
	}

	switch(i)
	{
	case 0:
		return 1.19559*((float)wGain)/16.0 - 0.213051;
		break;
		
	case 1:
		return 1.15956*((float)wGain)/16.0 - 0.199173;
		break;
		
	case 2:
		return 1.13971*((float)wGain)/16.0 - 0.266085;
		break;
		
	case 3:
		return 1.16815*((float)wGain)/16.0 - 0.881696;
		break;

	case 4:
		return 1.1*((float)wGain)/16.0 - 1.0;

	default:
		return ((float)wGain)/16.0;
	}
}

//this function is also called during Probe stage to calculate the transfer_size/LWM/HWM
void GetSensorPclkHsync(U16 wSensorSPFormat,U8 byFps)
{
	OV_FpsSetting_t *pOv2710_FpsSetting;

	if(wSensorSPFormat == HD720P_FRM)
	{
		pOv2710_FpsSetting=GetOvFpsSetting(byFps, g_staOV2710HD720FpsSetting, sizeof(g_staOV2710HD720FpsSetting)/sizeof(OV_FpsSetting_t));
	}
	else//HD1080P_FRM
	{
		pOv2710_FpsSetting=GetOvFpsSetting(byFps, g_staOV2710FHDFpsSetting, sizeof(g_staOV2710FHDFpsSetting)/sizeof(OV_FpsSetting_t));
	}
	
	g_wSensorHsyncWidth = pOv2710_FpsSetting->wExtraDummyPixel; // this for manual exposure
	g_dwPclk = pOv2710_FpsSetting->dwPixelClk;		// this for scale speed
}

void OV2710SetFormatFps(U16 SetFormat, U8 Fps)
{
	OV_FpsSetting_t *pOv2710_FpsSetting;
	U16 wTemp;

	if((g_wSensorSPFormat & SetFormat) == HD720P_FRM)
	{
		// initial all register setting
		WriteSensorSettingWB(sizeof(gc_OV2710_HD720_Setting)/3, gc_OV2710_HD720_Setting);
#ifdef _MIPI_EXIST_
		WriteSensorSettingWB(sizeof(gc_OV2710_HD720_mipi_Setting)/3, gc_OV2710_HD720_mipi_Setting);
		//g_wMipiActiveWidth= 1284;
#endif
		// write fps setting
		pOv2710_FpsSetting=GetOvFpsSetting(Fps, g_staOV2710HD720FpsSetting, sizeof(g_staOV2710HD720FpsSetting)/sizeof(OV_FpsSetting_t));
		Write_SenReg_Mask(0x3010, pOv2710_FpsSetting->byClkrc, 0xF0);
		wTemp = pOv2710_FpsSetting->wExtraDummyPixel;
		Write_SenReg(0x380C, INT2CHAR(wTemp, 1)); //Write dummy pixel MSB
		Write_SenReg(0x380D, INT2CHAR(wTemp, 0)); //Write dummy pixel LSB

		// update firmware variable	
		//g_wSensorHsyncWidth =  pOv2710_FpsSetting->wExtraDummyPixel; // this for manual exposure
		//g_dwPclk = pOv2710_FpsSetting->dwPixelClk;		// this for scale speed
		GetSensorPclkHsync(HD720P_FRM,Fps);
		g_wAECExposureRowMax = 737;	//hemonel 2010-12-09: We use 7. At OV2710 When porting using manual exp control, the max exposure, at least should subtract 6
		g_wAEC_LineNumber = 744;	// this for AE insert dummy line algothrim	

		g_wSensorWidthBefBLC = 1280;
		g_wSensorHeightBefBLC = 720;
		//	g_byLSCCurve_Idx = 1;
	}
	else
	{
		// initial all register setting
		WriteSensorSettingWB(sizeof(gc_OV2710_FHD_Setting)/3, gc_OV2710_FHD_Setting);
#ifdef _MIPI_EXIST_
		WriteSensorSettingWB(sizeof(gc_OV2710_FHD_mipi_Setting)/3, gc_OV2710_FHD_mipi_Setting);
		//g_wMipiActiveWidth= 1924;
#endif
		// write fps setting
		pOv2710_FpsSetting=GetOvFpsSetting(Fps, g_staOV2710FHDFpsSetting, sizeof(g_staOV2710FHDFpsSetting)/sizeof(OV_FpsSetting_t));
		Write_SenReg_Mask(0x3010, pOv2710_FpsSetting->byClkrc, 0xF0);
		wTemp = pOv2710_FpsSetting->wExtraDummyPixel;
		Write_SenReg(0x380C, INT2CHAR(wTemp, 1)); //Write dummy pixel MSB
		Write_SenReg(0x380D, INT2CHAR(wTemp, 0)); //Write dummy pixel LSB

		// update firmware variable		
		//g_wSensorHsyncWidth = pOv2710_FpsSetting->wExtraDummyPixel; // this for manual exposure
		//g_dwPclk = pOv2710_FpsSetting->dwPixelClk;		// this for scale speed
		GetSensorPclkHsync(HD1080P_FRM,Fps);
		g_wAECExposureRowMax = 1097;	//hemonel 2010-12-09: We use 7. At OV2710 When porting using manual exp control, the max exposure, at least should subtract 6
		g_wAEC_LineNumber = 1104;	// this for AE insert dummy line algothrim		

		g_wSensorWidthBefBLC = 1920;
		g_wSensorHeightBefBLC = 1080;
//		g_byLSCCurve_Idx = 0;
	}

//  g_wBLCStartX = 0;
//  g_wBLCStartY = 0;
	Write_SenReg_Mask(0x302c, 0x80, 0xC0);

	InitOV2710IspParamByFormat(SetFormat);
#ifdef _MIPI_EXIST_
	//-----------------------------Dphy Setting--------------------------------
	SetMipiDphy(MIPI_DATA_LANE0_EN, \
	            MIPI_DATA_FORMAT_RAW10, MIPI_DATA_TYPE_RAW10, \
	            HSTERM_EN_TIME_33);
#endif

}

void CfgOV2710ControlAttr(void)
{
	U8 i;

	g_bySensorSize = SENSOR_SIZE_FHD;
	g_wSensorSPFormat =HD1080P_FRM;

	{
		memcpy(g_asOvCTT, gc_OV2710_CTT,sizeof(gc_OV2710_CTT));
	}

	{
#if (_CHIP_ID_ & _RTS5840_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_3V30;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V79;
#elif (_CHIP_ID_ & _RTS5832_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_3V33;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V85;
#elif (_CHIP_ID_ & _RTS5829B_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_3V32;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V80;
#endif
	}

//	if(g_byFormatCfgExist==0) // format config not exist, use default setting.
	{
		//====== resolution  setting ===========
		// -- preview ---
		g_aVideoFormat[0].byaVideoFrameTbl[0] = 12;		// resolution number
		g_aVideoFormat[0].byaVideoFrameTbl[1]=F_SEL_640_480;
		g_aVideoFormat[0].byaVideoFrameTbl[2]=F_SEL_320_180;
		g_aVideoFormat[0].byaVideoFrameTbl[3]=F_SEL_320_240;
		g_aVideoFormat[0].byaVideoFrameTbl[4]=F_SEL_352_288;
		g_aVideoFormat[0].byaVideoFrameTbl[5]=F_SEL_424_240;
		g_aVideoFormat[0].byaVideoFrameTbl[6]=F_SEL_640_360;
		g_aVideoFormat[0].byaVideoFrameTbl[7]=F_SEL_640_400;
		g_aVideoFormat[0].byaVideoFrameTbl[8]=F_SEL_848_480;
		g_aVideoFormat[0].byaVideoFrameTbl[9]=F_SEL_960_540;
		g_aVideoFormat[0].byaVideoFrameTbl[10]=F_SEL_1280_720;
		g_aVideoFormat[0].byaVideoFrameTbl[11]=F_SEL_1280_800;
		g_aVideoFormat[0].byaVideoFrameTbl[12]=F_SEL_1920_1080;
		//--still image--
		g_aVideoFormat[0].byaStillFrameTbl[0] = 5;	// resolution number
		g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_1920_1080;
		g_aVideoFormat[0].byaStillFrameTbl[2]=F_SEL_1280_720;
		g_aVideoFormat[0].byaStillFrameTbl[3]=F_SEL_320_240;
		g_aVideoFormat[0].byaStillFrameTbl[4]=F_SEL_352_288;
		g_aVideoFormat[0].byaStillFrameTbl[5]=F_SEL_640_480;

		//====== fps  setting ===========
		for(i=0; i<7; i++)
		{
			/*640_480,
			160_120,
			176_144,
			320_200,
			320_240,
			352_288,
			640_400*/
			g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_15|FPS_20|FPS_25|FPS_30;
		}
		for(i=7; i<16; i++)
		{

			g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_10|FPS_5;
		}

		for(i=16; i<20; i++)
		{
			g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_3 |FPS_5;
		}

		//====== format type  setting ===========
		g_aVideoFormat[0].byFormatType = FORMAT_TYPE_YUY2;

#ifdef _ENABLE_MJPEG_
		// copy YUY2 setting to MJPEG setting
		memcpy(&(g_aVideoFormat[1]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

		// modify MJPEG FPS setting
		for(i=0;i<20;i++)
		{
			/*
			 F_SEL_800_600    (7)
			 F_SEL_960_720    (8)
			 F_SEL_1024_768   (9)
			 F_SEL_1280_720   (10)
			 F_SEL_1280_800   (11)
			 F_SEL_1280_960   (12)
			 F_SEL_1280_1024  (13)
			 F_SEL_1600_1200  (14)
			 F_SEL_1920_1080  (15)
			 F_SEL_1600_900	 (16)
			 */
			g_aVideoFormat[1].waFrameFpsBitmap[i] = (FPS_10|FPS_15|FPS_20|FPS_25|FPS_30);
		}		
		//for(i=11;i<17;i++)
		//{
		//	g_aVideoFormat[1].waFrameFpsBitmap[i] = (FPS_5|FPS_10|FPS_15);
		//}
		// modify MJPEG format type
		g_aVideoFormat[1].byFormatType = FORMAT_TYPE_MJPG;

#endif

#ifdef _ENABLE_M420_FMT_
		// copy YUY2 setting to M420 setting
		memcpy(&(g_aVideoFormat[2]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

		// modify M420 FPS setting
		{
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_640_480] =FPS_25|FPS_20|FPS_15|FPS_10|FPS_5;//|FPS_1;             //bit07 F_800_600
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_960_540] =FPS_25|FPS_20|FPS_15|FPS_10|FPS_5;//|FPS_5|FPS_3|FPS_1;                    //bit08 F_960_720
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1024_768] =FPS_15|FPS_10|FPS_5;//|FPS_1;                    //bit09 F_1024_768
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_720] =FPS_10|FPS_5;//FPS_9|FPS_8|FPS_1;    //bit10 F_1280_720
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_800] =FPS_10|FPS_5;
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1920_1080] =FPS_5;
			// modify M420 format type
			g_aVideoFormat[2].byFormatType = FORMAT_TYPE_M420;
		}
#endif

	}

	{
		//Brightness - BrightnessItem

		//Contrast - ContrastItem

		//Saturation - SaturationItem

		//Hue - 	HueItem

		//Sharpness - SharpnessItem
		//	SharpnessItem.Min = 0;
		//	SharpnessItem.Max = 8;
		//	SharpnessItem.Res = 1;
		//	SharpnessItem.Def = 3;
		//	SharpnessItem.Last  =SharpnessItem.Def;

		// gamma - 	GammaItem

		// gain - GainItem

		// whitebalance - WhiteBalanceTempItem

		// backlight - BackLightCompItem

		// powerline -PwrLineFreqItem

		// exposure time -	ExposureTimeAbsolutItem

		// low light - LowLightCompItem
	}
	InitOV2710IspParams();
}

void SetOV2710IntegrationTime(U16 wEspline)
{
	U16 wAECVTS;
	U32 dwExposureTime;

	if(wEspline > g_wAECExposureRowMax)
	{
		wAECVTS = wEspline - g_wAECExposureRowMax;
		if (wAECVTS%2==1)
		{
			wAECVTS++;
		}
	}
	else
	{
		wAECVTS = 0;
	}
	dwExposureTime = ((U32)wEspline)<<4;

	//-----------Write  frame length/dummy lines------------
	Write_SenReg(0x350C, INT2CHAR(wAECVTS, 1));
	Write_SenReg(0x350D, INT2CHAR(wAECVTS, 0));

	//-----------Write Exposuretime setting-----------
	Write_SenReg(0x3500, LONG2CHAR(dwExposureTime, 2));	// change exposure value high
	Write_SenReg(0x3501, LONG2CHAR(dwExposureTime, 1));	// change exposure value
	Write_SenReg(0x3502, LONG2CHAR(dwExposureTime, 0));	// change exposure value low

	//----------- write gain setting-------------------
	Write_SenReg(0x350A, 0);	// fixed gain at manual exposure control
	Write_SenReg(0x350B, 0);
}

void SetOV2710Gain(float fGain)
{
}

void SetOV2710ImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.
{
	Write_SenReg_Mask(0x3818, ((bySnrImgDir&0x01)<<6)|((bySnrImgDir&0x02)<<4), 0x60);
	switch(bySnrImgDir)
	{
	case 0:
	default:
		Write_SenReg(0x382E, 0x0F);
		Write_SenReg(0x381a, 0x1A);
		Write_SenReg(0x3803, 0x0A);
		Write_SenReg(0x3621, 0x04);
#ifdef _MIPI_EXIST_
		SetBLCWindowStart(0, 1);
#else
		SetBkWindowStart(0, 1);
#endif
		break;
	case 1:
		Write_SenReg(0x382E, 0x0F);
		Write_SenReg(0x381a, 0x1A);
		Write_SenReg(0x3803, 0x0a);
		Write_SenReg(0x3621, 0x14);
#ifdef _MIPI_EXIST_
		SetBLCWindowStart(0, 1);
#else
		SetBkWindowStart(0, 1);
#endif
		break;
	case 2:
		Write_SenReg(0x382E, 0x0F);
		Write_SenReg(0x381a, 0x1A);
		Write_SenReg(0x3803, 0x09);
		Write_SenReg(0x3621, 0x04);
#ifdef _MIPI_EXIST_
		SetBLCWindowStart(0, 0);
#else
		SetBkWindowStart(0, 1);
#endif
		break;
	case 3:
		Write_SenReg(0x382E, 0x0F);
		Write_SenReg(0x381a, 0x1A);
		Write_SenReg(0x3803, 0x09);
		Write_SenReg(0x3621, 0x14);
#ifdef _MIPI_EXIST_
		SetBLCWindowStart(0, 0);
#else
		SetBkWindowStart(0, 1);
#endif
		break;
	}
}

void SetOV2710Exposuretime_Gain(float fExpTime, float fTotalGain)
{
	U16  wGainRegSetting; //data
	float fSnrGlbGain;
	U32 dwExposureTime;
	U8 byTemp;
	
	wGainRegSetting=MapSnrGlbGain2SnrRegSetting(fTotalGain);
	fSnrGlbGain = SnrRegSetting2SnrGlbGain(wGainRegSetting);
	dwExposureTime = (U32)(fExpTime*16/g_fSensorRowTimes);
	
	// hemonel 2010-12-09: image occur white point issue
	//					OV note that exposure tline need less than one line then can use sub-line
	if(dwExposureTime> 16)
	{
		byTemp = dwExposureTime&0x0F;
		dwExposureTime = dwExposureTime&0xFFFFFFF0;	// cut sub-line
		if(byTemp >= 8)	// round sub-line to tline
		{
			dwExposureTime+=16;
		}
	}

	// write exposure, gain, dummy line
	if(fabs(g_fCurExpTime-fExpTime) > 0.0001)
	{
		//AE_MSG(("set exposure and gain\n"));

		// group register hold
		// hemonel 2010-12-14: Group register hold function working need whitch video has previewed
		//				otherwise these register will not be writen into sensor register
		if(g_byStartVideo)
		{
			Write_SenReg(0x3212, 0x00);	// Enable group0
		}

		//-----------Write  frame length/dummy lines------------
		Write_SenReg(0x350C, INT2CHAR(g_wAFRInsertDummylines, 1));
		Write_SenReg(0x350D, INT2CHAR(g_wAFRInsertDummylines, 0));

		//-----------Write Exposuretime setting-----------
		Write_SenReg(0x3500, LONG2CHAR(dwExposureTime, 2));	// change exposure value high
		Write_SenReg(0x3501, LONG2CHAR(dwExposureTime, 1));	// change exposure value
		Write_SenReg(0x3502, LONG2CHAR(dwExposureTime, 0));	// change exposure value low

		// group register release
		if(g_byStartVideo)
		{
			Write_SenReg(0x3212, 0x10);	// End group0
			Write_SenReg(0x3212, 0xA0);	// Launch group0
		}

		// delay one frame for sync
		WaitFrameSync(ISP_INT1, ISP_DATA_START_INT);
		// group register hold
		if(g_byStartVideo)
		{
			Write_SenReg(0x3212, 0x01);	// Enable group0
		}

		//----------- write gain setting-------------------
		Write_SenReg(0x350A, INT2CHAR(wGainRegSetting, 1));
		Write_SenReg(0x350B, INT2CHAR(wGainRegSetting, 0));

		SetISPAEGain(fTotalGain, fSnrGlbGain, 0);

		// group register release
		if(g_byStartVideo)
		{
			Write_SenReg(0x3212, 0x11);	// End group0
			Write_SenReg(0x3212, 0xA1);	// Launch group0
		}
		
		g_fCurExpTime = fExpTime;	// back up exposure rows
	}
	else
	{
		// group register hold
		if(g_byStartVideo)
		{
			Write_SenReg(0x3212, 0x00);	// Enable group0
		}
		//-----------Write  frame length/dummy lines------------
		Write_SenReg(0x350C, INT2CHAR(g_wAFRInsertDummylines, 1));
		Write_SenReg(0x350D, INT2CHAR(g_wAFRInsertDummylines, 0));
		// write gain setting
		Write_SenReg(0x350A, INT2CHAR(wGainRegSetting, 1));
		Write_SenReg(0x350B, INT2CHAR(wGainRegSetting, 0));

		SetISPAEGain(fTotalGain, fSnrGlbGain, 0);

		// group register release
		if(g_byStartVideo)
		{
			Write_SenReg(0x3212, 0x10);	// End group0
			Write_SenReg(0x3212, 0xA0);	// Launch group0
		}
	}

	return;
}

void OV2710_POR()
{
#ifdef _MIPI_EXIST_
	Init_MIPI();
#else
	Init_CCS();
#endif
	ENTER_SENSOR_RESET();

	// power on
	SensorPowerControl(SWITCH_ON,EN_DELAYTIME);
	uDelay(1);

	// Initialize I2C Controller Timing
	LEAVE_SENSOR_RESET();

	// clock select 24M
	if( g_bySSCEnable== TRUE)
	{
#ifdef _IC_CODE_
		CHANGE_CCS_CLK(SSC_CLK_SEL_96M|SSC_CLK_SEL_DIV4);
#else
		CHANGE_CCS_CLK(SSC_CLK_SEL_96M);
#endif
	}
	else
	{
#ifdef _IC_CODE_
		CHANGE_CCS_CLK(CCS_CLK_SEL_96M|CCS_CLK_DIVIDER_4);
#else
		CHANGE_CCS_CLK(CCS_CLK_SEL_96M|CCS_CLK_DIVIDER_4);
#endif
	}

	EnableSensorHCLK();
	g_bySensorIsOpen = SENSOR_OPEN;
	WaitTimeOut_Delay(1);  //OV2710 spec request 8192clk
}

/*
*********************************************************************************************************
*                                 	Backend ISP IQ Parameter setting
*
*********************************************************************************************************
*/


void InitOV2710IspParamByFormat(SetFormat)
{


	SetFormat = SetFormat;
	// LSC & MLSC

}

void InitOV2710IspParams()
{

	// D50 R/G/B gain
	g_wAWBRGain_Last= 0x136;
	g_wAWBGGain_Last= 0x100;
	g_wAWBBGain_Last= 0x19D;
	g_wProjectGR = g_wAWBRGain_Last;
	g_wProjectGB = g_wAWBBGain_Last;

	g_wDynamicISPEn = DYNAMIC_LSC_CT_EN | DYNAMIC_LSC_EN | DYNAMIC_SHARPNESS_EN | DYNAMIC_SHARPPARAM_EN | DYNAMIC_GAMMA_EN | DYNAMIC_CCM_CT_EN;
}


void SetOV2710DynamicISP(U8 byAEC_Gain)
{
	byAEC_Gain = byAEC_Gain;
}

void SetOV2710DynamicISP_AWB(U16 wColorTempature)
{
	wColorTempature =wColorTempature;
}
#endif
