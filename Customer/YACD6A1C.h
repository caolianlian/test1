#ifndef _YACD6A1C_H_
#define _YACD6A1C_H_

void YACD6A1CSetFormatFps(U16 SetFormat, U8 Fps);
void CfgYACD6A1CControlAttr(void);
void SetYACD6A1CImgDir(U8 bySnrImgDir);
void SetYACD6A1CIntegrationTime(U16 wEspline);
void SetYACD6A1CExposuretime_Gain(float fExpTime, float fTotalGain);
void InitYACD6A1CIspParams();
void YACD6A1C_POR(void);
void SetYACD6A1CDynamicISP(U8 byAEC_Gain);
void SetYACD6A1CDynamicISP_AWB(U16 wColorTempature);
void SetYACD6A1CGain(float fGain);
#endif // _OV9710_H_

