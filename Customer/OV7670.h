#ifndef _OV7670_H_
#define _OV7670_H_

#define OV7670_AEW 0x50 //---20090422
#define OV7670_AEB	0x40

void OV7670SetFormatFps(U8 SetFormat, U8 Fps);
void CfgOV7670ControlAttr(void);
void SetOV7670Brightness(S16 swSetValue);
void	SetOV7670Contrast(U16 wSetValue);
void	SetOV7670Saturation(U16 wSetValue);
void	SetOV7670Hue(S16 swSetValue);
void	SetOV7670Sharpness(U8 bySetValue);
void SetOV7670Effect(U8 byEffect);
void SetOV7670ImgDir(U8 bySnrImgDir);
//void SetOV7670WindowCrop(U16 wXStart,U16 wXEnd,U16 wYStart,U16 wYEnd );
void SetOV7670OutputDim(U16 wWidth,U16 wHeight);
void SetOV7670PwrLineFreq(U8 byFPS, U8 byLightFrq);
void SetOV7670WBTemp(U16 wSetValue);
OV_CTT_t GetOV7670AwbGain(void);
void SetOV7670WBTempAuto(U8 bySetValue);
void SetOV7670BackLightComp(U8 bySetValue);
extern code OV_CTT_t gc_OV7670_CTT[3];
U8 GetOV7670Yavg();
U16 GetOV7670Exposuretime();
void SetOV7670IntegrationTimeAuto(U8 bySetValue);
void SetOV7670IntegrationTime(U16 wEspline);
extern U8 code g_OV7670_CCM[21];
void SetOV7670Gain(float fGain);
#endif // _OV7670_H_
