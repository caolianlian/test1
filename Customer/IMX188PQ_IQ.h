#ifndef _IMX188PQ_IQ_H_
#define _IMX188PQ_IQ_H_


IQTABLE_t code ct_IQ_Table=
{
	// IQ Header
	{
		IQ_TABLE_AP_VERSION,	// AP version
		sizeof(IQTABLE_t)+8,
		0x00,	// IQ version High
		0x00,	// IQ version Low
		0xFF,
		0xFF,
		0xFF
	},

	// BLC
	{
		// normal BLC:  offset_R,offsetG1,offsetG2,offsetB
		{62,62,62,62},
		//{53,43,43,55},
		//{64,64,64,64},
		// Low lux BLC:  offset_R,offsetG1,offsetG2,offsetB
		{48,20,20,52},
		//{16,16,16,16},
		//{0,0,0,0},
	},

	// LSC
	{
		// circle LSC
		{
			// circle LSC curve
			{  
				//Acer PK
				/*  
				{128,	0, 128,   0, 132,	0, 139,   0, 149,	0, 164,   0, 182,	0, 205,   0, 234,	0,	15,   1,  54,	1, 101,   1, 147,	1, 147,   1, 147,	1, 147,   1, 147,	1, 147,   1, 147,	1, 147,   1, 147,	1, 147,   1, 147,	1, 147,   1, },
				{128,	0, 128,   0, 131,	0, 137,   0, 145,	0, 157,   0, 171,	0, 189,   0, 209,	0, 236,   0,   9,	1,	43,   1,  76,	1,	76,   1,  76,	1,	76,   1,  76,	1,	76,   1,  76,	1,	76,   1,  76,	1,	76,   1,  76,	1,	76,   1, },
				{128,	0, 128,   0, 131,	0, 137,   0, 145,	0, 156,   0, 170,	0, 186,   0, 206,	0, 231,   0,   2,	1,	35,   1,  63,	1,	63,   1,  63,	1,	63,   1,  63,	1,	63,   1,  63,	1,	63,   1,  63,	1,	63,   1,  63,	1,	63,   1, },
				*/
				
				//CWF
				{128,	0, 129,   0, 133,	0, 139,   0, 147,	0, 158,   0, 173,	0, 189,   0, 208,	0, 231,   0,   3,	1,	39,   1,  82,	1,	82,   1,  82,	1,	82,   1,  82,	1,	82,   1,  82,	1,	82,   1,  82,	1,	82,   1,  82,	1,	82,   1, },
				{128,	0, 128,   0, 132,	0, 136,   0, 143,	0, 152,   0, 163,	0, 175,   0, 188,	0, 204,   0, 225,	0, 253,   0, 253,	0, 253,   0, 253,	0, 253,   0, 253,	0, 253,   0, 253,	0, 253,   0, 253,	0, 253,   0, 253,	0, 253,   0, },
				{128,	0, 128,   0, 131,	0, 136,   0, 142,	0, 151,   0, 161,	0, 172,   0, 184,	0, 200,   0, 221,	0, 252,   0,  45,	1,	45,   1,  45,	1,	45,   1,  45,	1,	45,   1,  45,	1,	45,   1,  45,	1,	45,   1,  45,	1,	45,   1, },
				},
			// circle LSC center: R Center Hortizontal, R Center Vertical, G Center Hortizontal, G Center Vertical,B Center Hortizontal, B Center Vertical
			//{604,416,604,416,604,416},//0530 4300K lenov office light
			{615,382,632,377,632,392},
		},

		// micro LSC
		{
			// micro LSC grid mode
			2,
			// micro LSC matrix
			{
				0x15, 0x19, 0x1b, 0x1a, 0x1a, 0x19, 0x18, 0x17, 0x17, 0x16, 0x15, 0x14, 0x13, 0x12, 0x10, 0xf, 0xe, 0x12, 0x12, 0x11, 0x16, 0x19, 0x18, 0x18, 0x17, 0x16, 0x16, 0x16, 0x16, 0x15, 0x13, 0x12, 0x11, 0x10, 0xe, 0xc, 0xa, 0xd, 0x10, 0xf, 0x19, 0x19, 0x17, 0x16, 0x15, 0x16, 0x16, 0x15, 0x14, 0x13, 0x12, 0x11, 0x10, 0xe, 0xd, 0xc, 0xa, 0x8, 0xc, 0xd, 0x19, 0x18, 0x16, 0x15, 0x15, 0x15, 0x14, 0x14, 0x12, 0x11, 0x10, 0xf, 0xe, 0xc, 0xb, 0xa, 0x9, 0x7, 0x9, 0xc, 0x19, 0x16, 0x14, 0x13, 0x14, 0x14, 0x13, 0x11, 0x10, 0xf, 0xe, 0xd, 0xc, 0xa, 0xa, 0x9, 0x8, 0x6, 0x6, 0xa, 0x16, 0x14, 0x13, 0x12, 0x12, 0x12, 0x10, 0xe, 0xd, 0xd, 0xd, 0xb, 0xa, 0x8, 0x8, 0x7, 0x7, 0x5, 0x4, 0x9, 0x15, 0x12, 0x12, 0x11, 0x11, 0x10, 0xe, 0xc, 0xb, 0xb, 0xb, 0xa, 0x8, 0x6, 0x6, 0x5, 0x5, 0x4, 0x4, 0x7, 0x14, 0x12, 0x10, 0xf, 0xf, 0xe, 0xc, 0xb, 0xa, 0x9, 0x9, 0x8, 0x7, 0x5, 0x5, 0x4, 0x4, 0x3, 0x2, 0x7, 0x12, 0x11, 0xf, 0xe, 0xd, 0xd, 0xb, 0x9, 0x8, 0x7, 0x7, 0x6, 0x5, 0x4, 0x3, 0x3, 0x3, 0x2, 0x2, 0x6, 0x12, 0x10, 0xe, 0xd, 0xc, 0xb, 0x9, 0x7, 0x6, 0x6, 0x5, 0x4, 0x3, 0x3, 0x2, 0x2, 0x2, 0x1, 0x3, 0x5, 0x10, 0xf, 0xd, 0xc, 0xa, 0x8, 0x8, 0x7, 0x5, 0x4, 0x3, 0x2, 0x2, 0x2, 0x2, 0x1, 0x1, 0x0, 0x3, 0x6, 0xd, 0xe, 0xc, 0xb, 0x9, 0x8, 0x7, 0x5, 0x4, 0x3, 0x3, 0x2, 0x2, 0x2, 0x1, 0x1, 0x0, 0x2, 0x4, 0x5, 0xb, 0xb, 0xc, 0xb, 0x9, 0x7, 0x6, 0x4, 0x5, 0x4, 0x2, 0x2, 0x2, 0x1, 0x1, 0x0, 0x1, 0x3, 0x5, 0x6, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
				0x13, 0x14, 0x15, 0x14, 0x13, 0x12, 0x12, 0x12, 0x11, 0x11, 0x10, 0xf, 0xe, 0xd, 0xc, 0xb, 0xb, 0xf, 0xe, 0xd, 0x12, 0x14, 0x13, 0x12, 0x11, 0x10, 0x11, 0x10, 0x10, 0xf, 0xe, 0xe, 0xd, 0xc, 0xb, 0x9, 0x8, 0xa, 0xd, 0xc, 0x14, 0x14, 0x12, 0x10, 0x10, 0x10, 0x10, 0x10, 0xf, 0xf, 0xd, 0xd, 0xc, 0xb, 0xa, 0x9, 0x8, 0x6, 0x9, 0xa, 0x14, 0x12, 0x10, 0xf, 0xf, 0x10, 0xf, 0xf, 0xe, 0xd, 0xc, 0xb, 0xa, 0x9, 0x8, 0x8, 0x7, 0x6, 0x6, 0x9, 0x14, 0x11, 0xf, 0xe, 0xf, 0xf, 0xe, 0xd, 0xc, 0xb, 0xb, 0xa, 0x9, 0x8, 0x7, 0x7, 0x6, 0x5, 0x5, 0x7, 0x12, 0xf, 0xe, 0xd, 0xe, 0xd, 0xd, 0xb, 0xa, 0xa, 0x9, 0x8, 0x7, 0x6, 0x6, 0x5, 0x5, 0x4, 0x3, 0x6, 0x10, 0xe, 0xd, 0xc, 0xc, 0xc, 0xb, 0xa, 0x9, 0x9, 0x8, 0x7, 0x6, 0x5, 0x4, 0x4, 0x4, 0x3, 0x2, 0x5, 0xf, 0xd, 0xc, 0xb, 0xb, 0xb, 0x9, 0x8, 0x7, 0x7, 0x7, 0x6, 0x5, 0x4, 0x3, 0x3, 0x3, 0x2, 0x1, 0x4, 0xf, 0xc, 0xb, 0xa, 0xa, 0x9, 0x8, 0x7, 0x6, 0x6, 0x5, 0x5, 0x3, 0x3, 0x3, 0x3, 0x2, 0x1, 0x1, 0x4, 0xe, 0xc, 0xa, 0x9, 0x9, 0x8, 0x7, 0x6, 0x5, 0x4, 0x4, 0x3, 0x2, 0x2, 0x2, 0x2, 0x1, 0x0, 0x1, 0x4, 0xc, 0xb, 0x9, 0x8, 0x7, 0x7, 0x6, 0x5, 0x4, 0x3, 0x3, 0x2, 0x2, 0x2, 0x2, 0x1, 0x0, 0x0, 0x2, 0x3, 0xa, 0xb, 0x9, 0x7, 0x6, 0x6, 0x5, 0x4, 0x4, 0x3, 0x2, 0x2, 0x2, 0x2, 0x1, 0x0, 0x0, 0x1, 0x2, 0x3, 0x9, 0xa, 0x9, 0x8, 0x7, 0x6, 0x5, 0x4, 0x4, 0x3, 0x3, 0x2, 0x2, 0x2, 0x1, 0x0, 0x0, 0x3, 0x3, 0x3, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
				0x15, 0x15, 0x16, 0x15, 0x13, 0x13, 0x12, 0x12, 0x11, 0x10, 0x10, 0xf, 0xe, 0xd, 0xc, 0xb, 0xb, 0xe, 0xe, 0xc, 0x14, 0x15, 0x13, 0x12, 0x11, 0x12, 0x11, 0x11, 0x11, 0xf, 0xe, 0xd, 0xd, 0xc, 0xb, 0x9, 0x7, 0xa, 0xd, 0xc, 0x16, 0x15, 0x12, 0x11, 0x11, 0x11, 0x11, 0x10, 0x10, 0xe, 0xd, 0xc, 0xb, 0xb, 0xa, 0x9, 0x7, 0x6, 0x9, 0xa, 0x16, 0x14, 0x11, 0x10, 0x10, 0x10, 0x10, 0xf, 0xe, 0xd, 0xc, 0xb, 0xa, 0x9, 0x8, 0x7, 0x6, 0x5, 0x6, 0x9, 0x15, 0x12, 0x10, 0xf, 0x10, 0xf, 0xf, 0xe, 0xc, 0xc, 0xb, 0xa, 0x8, 0x7, 0x7, 0x6, 0x5, 0x4, 0x4, 0x8, 0x13, 0x10, 0xf, 0xf, 0xf, 0xe, 0xd, 0xc, 0xb, 0xa, 0x9, 0x9, 0x7, 0x5, 0x5, 0x5, 0x4, 0x3, 0x3, 0x6, 0x11, 0xf, 0xe, 0xe, 0xe, 0xd, 0xc, 0xa, 0x9, 0x8, 0x8, 0x8, 0x6, 0x4, 0x4, 0x4, 0x3, 0x2, 0x1, 0x5, 0x11, 0xe, 0xd, 0xd, 0xd, 0xc, 0xa, 0x9, 0x8, 0x7, 0x7, 0x6, 0x5, 0x4, 0x3, 0x3, 0x2, 0x1, 0x1, 0x4, 0x10, 0xe, 0xd, 0xc, 0xc, 0xb, 0xa, 0x8, 0x7, 0x6, 0x6, 0x5, 0x4, 0x3, 0x3, 0x3, 0x2, 0x0, 0x1, 0x4, 0x10, 0xd, 0xc, 0xb, 0xa, 0xa, 0x9, 0x7, 0x5, 0x5, 0x4, 0x3, 0x2, 0x2, 0x3, 0x3, 0x1, 0x0, 0x1, 0x4, 0xf, 0xd, 0xb, 0xa, 0x9, 0x8, 0x8, 0x6, 0x5, 0x3, 0x3, 0x2, 0x2, 0x2, 0x2, 0x1, 0x0, 0x0, 0x1, 0x3, 0xd, 0xd, 0xb, 0x9, 0x8, 0x7, 0x7, 0x6, 0x5, 0x4, 0x3, 0x2, 0x2, 0x2, 0x1, 0x0, 0x0, 0x1, 0x3, 0x3, 0xa, 0xc, 0xc, 0xa, 0x8, 0x8, 0x6, 0x5, 0x4, 0x3, 0x4, 0x4, 0x4, 0x2, 0x2, 0x0, 0x1, 0x2, 0x3, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,	
			},
		},

		// dynamic LSC
		{
			80,   // Dynamic LSC Gain Threshold Low
			144,  // Dynamic LSC Gain Threshold High
			0x1f,	// Dynamic LSC Adjust rate at Gain Threshold Low
			0x12,	// Dynamic LSC Adjust rate at Gain Threshold High
			//{33,35,38,54,56,34},	// rough r gain before LSC, from A, U30, CWF, D50, D65, D75
			//{78,70,61,44,38,72},	// rough b gain before LSC
			{36,43,55,59,73,76},	// g_aAWBRoughGain_R
			{93,79,71,55,44,43},	// g_aAWBRoughGain_B
			50,		// start threshold of dynamic LSC by CT, white pixel millesimal
			25,		// end threshold of dynamic LSC by CT, white pixel millesimal
			100,		// LSC switch color temperature threshold buffer
			{3100,3800,4700,5800,7000},	// LSC switch color temperature threshold
			{
				{0x20,0x12,0x12,},//a=2850k
				{0x20,0x1e,0x18,},//3500k
				{0x20,0x20,0x20,},//cwf=4150k ok
				{0x20,0x16,0x18,},//d50=5000k
				{0x20,0x16,0x18,},//d65=6500k ok
				{0x20,0x16,0x18,},//d75=6500k
			},
		},
	},

	// CCM
	{

		// D65 light CCM
		//{0x157,-105,18,-59,0x176,-59,-7,-122,0x181,},
		//{0x17b,-77,-46,-86,0x175,-31,34,-195,0x1a1,},
		//{0x153,-44,-38,-62,0x158,-26,20,-135,0x174,}, //Sony CCM V1
		{0x16a,-157,51,-71,0x1b1,-105,4,-290,0x21d,}, //Sony CCM V2
		//{0x1a1,-213,52,-102,0x1c7,-96,16,-252,0x1eb,},
		//A light CCM
		{221,30,6,16,234,6,16,30,210},//0526_CCM5_de_80%
		//Low light CCM
		{0x17f,-104,-23,-85,0x191,-60,15,-243,0x1e4,},


		//	{0, 1,  0, 0,  0, 0, 0, 0,  0, 1,  0, 0,  0, 0,  0, 0,  0, 1, 0,0,0 },
		160,	// dynamic CCM Gain Threshold Low
		240,	// dynamic CCM Gain Threshold High
		3000,	// dynamic CCM A light Color Temperature Switch Threshold
		3600	// dynamic CCM D65 light Color Temperature Switch Threshold
	},

	// Gamma
	{
		// normal light gamma
		{ 0, 10, 20, 30, 40, 50, 60, 69, 77, 93, 106, 118, 129, 139, 146, 153, 160, 166, 172, 177, 182, 192, 202, 212, 221, 230, 239, 247, },
		//{0, 9, 17, 25, 33, 41, 49, 57, 64, 78, 90, 101, 113, 123, 132, 143, 153, 161, 169, 176, 183, 195, 205, 214, 222, 230, 239, 247, },
		//0.63
		//{0, 9, 19, 28, 36, 46, 53, 60, 67, 82, 95, 107, 118, 129, 138, 148, 158, 166, 174, 182, 189, 200, 210, 218, 228, 234, 244, 250,},
		//0.65
		//{0, 9, 18, 27, 35, 44, 52, 60, 67, 82, 95, 107, 118, 129, 138, 148, 158, 166, 174, 182, 189, 202, 213, 222, 230, 238, 244, 250},
		//_______________________

		// low light gamma
		{0, 6, 13, 20, 27, 34, 42, 49, 58, 77, 96, 115, 131, 144, 153, 162, 169, 175, 181, 186, 191, 200, 209, 217, 225, 233, 241, 248, },
		80,	// dynamic Gamma Gain Threshold Low
		144	// dynamic Gamma Gain Threshold High
	},

	// AE
	{
		// AE target
		{
			40,	// Histogram Ratio Low
			40,// Histogram Ratio High
			65,	// YMean Target Low
			75,	// YMean Target
			85,	// YMean Target High
			10,	// Histogram Position Low
			190,// Histogram Position High
			0	// Dynamic AE Target decrease value
		},
		// AE limit
		{
			10,	// AE step Max value at 50Hz power line frequency
			12,	// AE step Max value at 60Hz power line frequency
			255,	// AE global gain Max value
			96,	// AE continous frame rate gain threshold
			160,	// AE discrete frame rate 15fps gain threshold
			80,	// AE discrete frame rate 30fps gain threshold
			70	,// AE HighLight mode threshold
		},
		// AE weight
		{
			{
				4,4,4,4,4,
				5,5,5,5,5,
				6,6,7,6,6,
				6,6,7,6,6,
				5,6,5,6,5,
			}
		},
		// AE sensitivity
		{
			0.06,	// g_fAEC_Adjust_Th
			16,	// AE Latency time
			//20,	// Ymean diff threshold for judge AE same block
			8,	// hemonel 2011-07-14: modify Neil adjust parameter from 20 to 8
			22	// same block count threshold for judge AE scene variation
		},
	},


	// AWB
	{
		{
			{35,32,50,58,63,67},	// g_aAWBRoughGain_R
			{100,91,70,54,45,43},	// g_aAWBRoughGain_B
			11,	// K1
			115,//120,	// B1
			85, //90,	// B2
			16,	// sK3
			43,//33,	// sB3
			16,//31,	// sK4
			-50, //-80,//-150,	// sB4
			-45, //16,	// sK5
			180, //77,	// sB5
			5, // 2,//15,	// sK6
			10, // 30, //15,//-80,	// sB6
			125,//130,	// B_up
			78, //80,	// B_down
			53,//43,	// sB_left
			-60,//-95,//-160,	// sB_right
			230,	// Ymean range upper limit
			0,	// Ymean range low limit
			500,	// RGB sum threshold for AWB hold
		},
		// AWB advanced
		{
			5,	// white point ratio threshold
			38, // g_byAWBFineMax_RG
			26, // g_byAWBFineMin_RG
			38, // g_byAWBFineMax_BG
			26, // g_byAWBFineMin_BG
			225,	// g_byAWBFineMax_Bright
			20, // g_byAWBFineMin_Bright
			30	// g_byFtGainTh
		},
		// AWB sensitivity
		{
			4, // g_wAWBGainDiffTh
			4, // g_byAWBGainStep
			10,	// g_byAWBFixed_YmeanTh
			7,	// g_byAWBColorDiff_Th
			12	// g_byAWBDiffWindowsTh
		}
	},

		// Texture
	{
		// sharpness
		{
			100,	// for CIF
			100,	// for VGA
			120,	// for HD
			0x20,// for low lux
			64,	// dynamic sharpness gain threshold low
			150	// dynamic sharpness gain threshold high
		},
		// sharpness & denoise paramter
		{
			// CIF
			{{
					0x0c,  	//ISP_NR_EDGE_THD,
					0x0a,	//ISP_NR_MM_THD1
					0x0, 	//ISP_NR_MODE0_LPF,
					0x0, 	//ISP_NR_MODE1_LPF,
					0xff,	//ISP_NR_MODE,
					0x00,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x02,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x02,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x86,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x0c,	//ISP_INTP_EDGE_THD1,
					0x3f,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x07,	//ISP_RGB_IIR_CTRL
					0x60,	//ISP_RGB_VIIR_COEF
					0xCC,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x06,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x01, 	//ISP_RGB_DIIR_COFF_CUT
					0x0a,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG
				},
				{
					0x0C,	//ISP_NR_EDGE_THD
					0x0A,	//ISP_NR_MM_THD1
					0x01,	//ISP_NR_MODE0_LPF
					0x01,	//ISP_NR_MODE1_LPF
					0x57,	//ISP_NR_MODE
					0x00,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x02,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD
					0x02,	//ISP_NR_CHAOS_CFG
					0x00,	//ISP_NR_DTHD_CFG
					0x85,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0
					0x0C,	//ISP_INTP_EDGE_THD1
					0x15,	//ISP_INTP_MODE
					0x02,	//ISP_INTP_CHAOS_MAX
					0x04,	//ISP_INTP_CHAOS_THD
					0x02,	//ISP_INTP_CHAOS_CFG
					0x00,	//ISP_INTP_DTHD_CFG
					0x07,	//ISP_RGB_IIR_CTRL
					0x50,	//ISP_RGB_VIIR_COEF
					0x66,	//ISP_RGB_HLPF_COEF
					0xA2,	//ISP_RGB_DIIR_MAX
					0x06,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x01,	//ISP_RGB_DIIR_COFF_CUT
					0x0A,	//ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG
				},
				{
					0x08,	//ISP_NR_EDGE_THD
					0x0A,	//ISP_NR_MM_THD1
					0x01,	//ISP_NR_MODE0_LPF
					0x01,	//ISP_NR_MODE1_LPF
					0x6B,	//ISP_NR_MODE
					0x01,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x02,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD
					0x02,	//ISP_NR_CHAOS_CFG
					0x00,	//ISP_NR_DTHD_CFG
					0x82,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0
					0x0C,	//ISP_INTP_EDGE_THD1
					0x16,	//ISP_INTP_MODE
					0x02,	//ISP_INTP_CHAOS_MAX
					0x04,	//ISP_INTP_CHAOS_THD
					0x02,	//ISP_INTP_CHAOS_CFG
					0x00,	//ISP_INTP_DTHD_CFG
					0x0F,	//ISP_RGB_IIR_CTRL
					0xA0,	//ISP_RGB_VIIR_COEF
					0xAA,	//ISP_RGB_HLPF_COEF
					0xA2,	//ISP_RGB_DIIR_MAX
					0x0D,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x01,	//ISP_RGB_DIIR_COFF_CUT
					0x06,	//ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG
				},
				{
					0x0e,  	//ISP_NR_EDGE_THD,
					0x0a,	//ISP_NR_MM_THD1
					0x0, 	//ISP_NR_MODE0_LPF,
					0x0, 	//ISP_NR_MODE1_LPF,
					0xff,	//ISP_NR_MODE,
					0x01,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x02,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x02,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x86,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x0c,	//ISP_INTP_EDGE_THD1,
					0x3f,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x0F,	//ISP_RGB_IIR_CTRL
					0xC0,	//ISP_RGB_VIIR_COEF
					0xCC,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x0d,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x01, 	//ISP_RGB_DIIR_COFF_CUT
					0x0c,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG

				}
			},
			// VGA resolution
			{{
				0x0C,	//ISP_NR_EDGE_THD
				0x0A,	//ISP_NR_MM_THD1
				0x01,	//ISP_NR_MODE0_LPF
				0x01,	//ISP_NR_MODE1_LPF
				0x57,	//ISP_NR_MODE
				0x00,	//ISP_NR_COLOR_ENABLE_CTRL
				0x05,	//ISP_NR_COLOR_MAX
				0x02,	//ISP_NR_CHAOS_MAX
				0x04,	//ISP_NR_CHAOS_THD
				0x02,	//ISP_NR_CHAOS_CFG
				0x00,	//ISP_NR_DTHD_CFG
				0x83,	//ISP_NR_GRGB_CTRL
				0x04,	//ISP_INTP_EDGE_THD0
				0x0C,	//ISP_INTP_EDGE_THD1
				0x01,	//ISP_INTP_MODE
				0x02,	//ISP_INTP_CHAOS_MAX
				0x04,	//ISP_INTP_CHAOS_THD
				0x02,	//ISP_INTP_CHAOS_CFG
				0x00,	//ISP_INTP_DTHD_CFG
				0x07,	//ISP_RGB_IIR_CTRL
				0x30,	//ISP_RGB_VIIR_COEF
				0x66,	//ISP_RGB_HLPF_COEF
				0xA2,	//ISP_RGB_DIIR_MAX
				0x06,	//ISP_RGB_DIIR_COEF
				0x0C,	//ISP_RGB_BRIGHT_COEF
				0x01,	//ISP_RGB_DIIR_COFF_CUT
				0x0A,	//ISP_EDG_DCT_THD1
0x03,	//ISP_EEH_DTHD_CFG
				},
				{
						0x0C,	//ISP_NR_EDGE_THD
						0x0A,	//ISP_NR_MM_THD1
						0x01,	//ISP_NR_MODE0_LPF
						0x01,	//ISP_NR_MODE1_LPF
						0x57,	//ISP_NR_MODE
						0x00,	//ISP_NR_COLOR_ENABLE_CTRL
						0x05,	//ISP_NR_COLOR_MAX
						0x02,	//ISP_NR_CHAOS_MAX
						0x04,	//ISP_NR_CHAOS_THD
						0x02,	//ISP_NR_CHAOS_CFG
						0x00,	//ISP_NR_DTHD_CFG
						0x85,	//ISP_NR_GRGB_CTRL
						0x04,	//ISP_INTP_EDGE_THD0
						0x0C,	//ISP_INTP_EDGE_THD1
						0x15,	//ISP_INTP_MODE
						0x02,	//ISP_INTP_CHAOS_MAX
						0x04,	//ISP_INTP_CHAOS_THD
						0x02,	//ISP_INTP_CHAOS_CFG
						0x00,	//ISP_INTP_DTHD_CFG
						0x07,	//ISP_RGB_IIR_CTRL
						0x50,	//ISP_RGB_VIIR_COEF
						0x66,	//ISP_RGB_HLPF_COEF
						0xA2,	//ISP_RGB_DIIR_MAX
						0x06,	//ISP_RGB_DIIR_COEF
						0x0C,	//ISP_RGB_BRIGHT_COEF
						0x01,	//ISP_RGB_DIIR_COFF_CUT
						0x0A,	//ISP_EDG_DCT_THD1
						0x03,	//ISP_EEH_DTHD_CFG
				},
				{
							0x08,	//ISP_NR_EDGE_THD
							0x0A,	//ISP_NR_MM_THD1
							0x01,	//ISP_NR_MODE0_LPF
							0x01,	//ISP_NR_MODE1_LPF
							0x6B,	//ISP_NR_MODE
							0x01,	//ISP_NR_COLOR_ENABLE_CTRL
							0x05,	//ISP_NR_COLOR_MAX
							0x02,	//ISP_NR_CHAOS_MAX
							0x04,	//ISP_NR_CHAOS_THD
							0x02,	//ISP_NR_CHAOS_CFG
							0x00,	//ISP_NR_DTHD_CFG
							0x82,	//ISP_NR_GRGB_CTRL
							0x04,	//ISP_INTP_EDGE_THD0
							0x0C,	//ISP_INTP_EDGE_THD1
							0x16,	//ISP_INTP_MODE
							0x02,	//ISP_INTP_CHAOS_MAX
							0x04,	//ISP_INTP_CHAOS_THD
							0x02,	//ISP_INTP_CHAOS_CFG
							0x00,	//ISP_INTP_DTHD_CFG
							0x0F,	//ISP_RGB_IIR_CTRL
							0xA0,	//ISP_RGB_VIIR_COEF
							0xAA,	//ISP_RGB_HLPF_COEF
							0xA2,	//ISP_RGB_DIIR_MAX
							0x0D,	//ISP_RGB_DIIR_COEF
							0x0C,	//ISP_RGB_BRIGHT_COEF
							0x01,	//ISP_RGB_DIIR_COFF_CUT
							0x06,	//ISP_EDG_DCT_THD1
							0x03,	//ISP_EEH_DTHD_CFG
				},
				{
					0x0e,  	//ISP_NR_EDGE_THD,
					0x0a,	//ISP_NR_MM_THD1
					0x01, 	//ISP_NR_MODE0_LPF,
					0x01, 	//ISP_NR_MODE1_LPF,
					0xbb,	//ISP_NR_MODE,
					0x01,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x02,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x02,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x82,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x0c,	//ISP_INTP_EDGE_THD1,
					0x16,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x0F,	//ISP_RGB_IIR_CTRL
					0xC0,	//ISP_RGB_VIIR_COEF
					0xcc,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x0d,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x01, 	//ISP_RGB_DIIR_COFF_CUT
					0x0c,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG

				}
			},
			// HD resolution
			//[Albert, 2011/06/16]+++
			{{
				0x0C,	//ISP_NR_EDGE_THD
				0x0A,	//ISP_NR_MM_THD1
				0x01,	//ISP_NR_MODE0_LPF
				0x01,	//ISP_NR_MODE1_LPF
				0x57,	//ISP_NR_MODE
				0x00,	//ISP_NR_COLOR_ENABLE_CTRL
				0x05,	//ISP_NR_COLOR_MAX
				0x02,	//ISP_NR_CHAOS_MAX
				0x04,	//ISP_NR_CHAOS_THD
				0x02,	//ISP_NR_CHAOS_CFG
				0x00,	//ISP_NR_DTHD_CFG
				0x83,	//ISP_NR_GRGB_CTRL
				0x04,	//ISP_INTP_EDGE_THD0
				0x0C,	//ISP_INTP_EDGE_THD1
				0x01,	//ISP_INTP_MODE
				0x02,	//ISP_INTP_CHAOS_MAX
				0x04,	//ISP_INTP_CHAOS_THD
				0x02,	//ISP_INTP_CHAOS_CFG
				0x00,	//ISP_INTP_DTHD_CFG
				0x07,	//ISP_RGB_IIR_CTRL
				0x30,	//ISP_RGB_VIIR_COEF
				0x66,	//ISP_RGB_HLPF_COEF
				0xA2,	//ISP_RGB_DIIR_MAX
				0x06,	//ISP_RGB_DIIR_COEF
				0x0C,	//ISP_RGB_BRIGHT_COEF
				0x01,	//ISP_RGB_DIIR_COFF_CUT
				0x0A,	//ISP_EDG_DCT_THD1
				0x03,	//ISP_EEH_DTHD_CFG
				},
				{
						0x0C,	//ISP_NR_EDGE_THD
						0x0A,	//ISP_NR_MM_THD1
						0x01,	//ISP_NR_MODE0_LPF
						0x01,	//ISP_NR_MODE1_LPF
						0x57,	//ISP_NR_MODE
						0x00,	//ISP_NR_COLOR_ENABLE_CTRL
						0x05,	//ISP_NR_COLOR_MAX
						0x02,	//ISP_NR_CHAOS_MAX
						0x04,	//ISP_NR_CHAOS_THD
						0x02,	//ISP_NR_CHAOS_CFG
						0x00,	//ISP_NR_DTHD_CFG
						0x85,	//ISP_NR_GRGB_CTRL
						0x04,	//ISP_INTP_EDGE_THD0
						0x0C,	//ISP_INTP_EDGE_THD1
						0x15,	//ISP_INTP_MODE
						0x02,	//ISP_INTP_CHAOS_MAX
						0x04,	//ISP_INTP_CHAOS_THD
						0x02,	//ISP_INTP_CHAOS_CFG
						0x00,	//ISP_INTP_DTHD_CFG
						0x07,	//ISP_RGB_IIR_CTRL
						0x50,	//ISP_RGB_VIIR_COEF
						0x66,	//ISP_RGB_HLPF_COEF
						0xA2,	//ISP_RGB_DIIR_MAX
						0x06,	//ISP_RGB_DIIR_COEF
						0x0C,	//ISP_RGB_BRIGHT_COEF
						0x01,	//ISP_RGB_DIIR_COFF_CUT
						0x0A,	//ISP_EDG_DCT_THD1
						0x03,	//ISP_EEH_DTHD_CFG
				},
				{
							0x08,	//ISP_NR_EDGE_THD
							0x0A,	//ISP_NR_MM_THD1
							0x01,	//ISP_NR_MODE0_LPF
							0x01,	//ISP_NR_MODE1_LPF
							0x6B,	//ISP_NR_MODE
							0x01,	//ISP_NR_COLOR_ENABLE_CTRL
							0x05,	//ISP_NR_COLOR_MAX
							0x02,	//ISP_NR_CHAOS_MAX
							0x04,	//ISP_NR_CHAOS_THD
							0x02,	//ISP_NR_CHAOS_CFG
							0x00,	//ISP_NR_DTHD_CFG
							0x82,	//ISP_NR_GRGB_CTRL
							0x04,	//ISP_INTP_EDGE_THD0
							0x0C,	//ISP_INTP_EDGE_THD1
							0x16,	//ISP_INTP_MODE
							0x02,	//ISP_INTP_CHAOS_MAX
							0x04,	//ISP_INTP_CHAOS_THD
							0x02,	//ISP_INTP_CHAOS_CFG
							0x00,	//ISP_INTP_DTHD_CFG
							0x0F,	//ISP_RGB_IIR_CTRL
							0xA0,	//ISP_RGB_VIIR_COEF
							0xAA,	//ISP_RGB_HLPF_COEF
							0xA2,	//ISP_RGB_DIIR_MAX
							0x0D,	//ISP_RGB_DIIR_COEF
							0x0C,	//ISP_RGB_BRIGHT_COEF
							0x01,	//ISP_RGB_DIIR_COFF_CUT
							0x06,	//ISP_EDG_DCT_THD1
							0x03,	//ISP_EEH_DTHD_CFG
				},
				{
					0x0e,  	//ISP_NR_EDGE_THD,
					0x0a,	//ISP_NR_MM_THD1
					0x01, 	//ISP_NR_MODE0_LPF,
					0x01, 	//ISP_NR_MODE1_LPF,
					0xbb,	//ISP_NR_MODE,
					0x01,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x02,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x02,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x82,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x0c,	//ISP_INTP_EDGE_THD1,
					0x16,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x0F,	//ISP_RGB_IIR_CTRL
					0xC0,	//ISP_RGB_VIIR_COEF
					0xcc,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x0d,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x01, 	//ISP_RGB_DIIR_COFF_CUT
					0x0c,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG

				}
			},
			40,		// dynamic denoise gain threshold 0
			112,		// dynamic denoise gain threshold 1
			196		// dynamic denoise gain threshold 2
		}
	},
	// UV offset
	{
		0,		// A light U offset
		0,		// A light V offset
		0,//0x24,		// D65 light U offset
		0,//0x22,		// D65 light V offset
		3300,	// Dynamic UV offset threshold for A light
		4000	// Dynamic UV offset threshold for D65 light
	},

	// gamma 2
	{
		// normal lux
		{
			//R - G diff gamma
			{ 0, 0, 0, -1, -1, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, },
			//B - G diff gamma
			{ 0, 0, 0, -1, -1, -1, -1, -3, -3, -5, -6, -7, -7, -7, -6, -5, },
		},

		// low lux
		{
			// R - G diff gamma
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			// B - G diff gamma
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		},
	},

	// Texture 2
	{
		// corner denoise
		{
			0x100,	//d0(start) for RGB domain
			0x300,	//d1(end) for RGB domain
			0x100,	//d0(start) for YUV domain
			0x300,	//d1(end) for YUV domain
			{
				{
					0x08,	//ISP_RDLOC_MAX
					0x20,	//ISP_RDLOC_RATE
					0x08,	//ISP_GLOC_MAX
					0x20,	//ISP_GLOC_RATE
					0x02,	//ISP_ILOC_MAX
					0x04,	//ISP_ILOC_RATE
					0x10,	//ISP_LOC_MAX
					0x40,	//ISP_LOC_RATE
				},
				{
						0x08,	//ISP_RDLOC_MAX
						0x20,	//ISP_RDLOC_RATE
						0x08,	//ISP_GLOC_MAX
						0x20,	//ISP_GLOC_RATE
						0x02,	//ISP_ILOC_MAX
						0x04,	//ISP_ILOC_RATE
						0x10,	//ISP_LOC_MAX
						0x40,	//ISP_LOC_RATE
				},
				{
							0x10,	//ISP_RDLOC_MAX
							0x40,	//ISP_RDLOC_RATE
							0x10,	//ISP_GLOC_MAX
							0x40,	//ISP_GLOC_RATE
							0x02,	//ISP_ILOC_MAX
							0x04,	//ISP_ILOC_RATE
							0x10,	//ISP_LOC_MAX
							0x40,	//ISP_LOC_RATE
				},
				{
					0x10,	//ISP_RDLOC_MAX
					0x40,	//ISP_RDLOC_RATE
					0x10,	//ISP_GLOC_MAX
					0x40,	//ISP_GLOC_RATE
					0x02,	//ISP_ILOC_MAX
					0x04,	//ISP_ILOC_RATE
					0x10,	//ISP_LOC_MAX
					0x40,	//ISP_LOC_RATE
				},
			},
		},
		// uv denoise
		{
			{
				{
					0x8,   //ISP_EEH_UVIIR_Y_CUTS
					4,   //ISP_EEH_UVIIR_Y_CMIN
					0xa,	 //ISP_EEH_IIR_COEF
					2,	 //ISP_EEH_IIR_CUTS
					1,  //ISP_EEh_IIR_STEP
				},
				{
						0x08,	//ISP_EEH_UVIIR_Y_CUTS
						0x04,	//ISP_EEH_UVIIR_Y_CMIN
						0x0A,	//ISP_EEH_IIR_COEF
						0x02,	//ISP_EEH_IIR_CUTS
						0x01,	//ISP_EEH_IIR_STEP
				},
				{
							0x08,	//ISP_EEH_UVIIR_Y_CUTS
							0x04,	//ISP_EEH_UVIIR_Y_CMIN
							0x0B,	//ISP_EEH_IIR_COEF
							0x02,	//ISP_EEH_IIR_CUTS
							0x01,	//ISP_EEH_IIR_STEP
				},
				{
					0x8,   //ISP_EEH_UVIIR_Y_CUTS
					4,   //ISP_EEH_UVIIR_Y_CMIN
					0xb,	 //ISP_EEH_IIR_COEF
					2,	 //ISP_EEH_IIR_CUTS
					1, //ISP_EEh_IIR_STEP
				},
			},
		},
		// noise reduction additional
		{
			{0x1E,0x1E,0x1E,0x1E,},	//ISP_EEH_CRC_RATE
			{2,2,1,1,},	//ISP_RD_MM2_RATE
			{
				{
					0x02,	//ISP_NR_MMM_D0
					0x22,	//ISP_NR_MMM_D1
					0xE0,	//ISP_NR_MMM_RATE
					0x10,	//ISP_NR_MMM_MIN
					0x80,	//ISP_NR_MMM_MAX
				},
				{
						0x02,	//ISP_NR_MMM_D0
						0x22,	//ISP_NR_MMM_D1
						0xE0,	//ISP_NR_MMM_RATE
						0x10,	//ISP_NR_MMM_MIN
						0x80,	//ISP_NR_MMM_MAX
				},
				{
							0x02,	//ISP_NR_MMM_D0
							0x22,	//ISP_NR_MMM_D1
							0x44,	//ISP_NR_MMM_RATE
							0x02,	//ISP_NR_MMM_MIN
							0xA4,	//ISP_NR_MMM_MAX
				},
				{
					0x02,	//ISP_NR_MMM_D0
					0x22,	//ISP_NR_MMM_D1
					0x78,	//ISP_NR_MMM_RATE
					0x10,	//ISP_NR_MMM_MIN
					0x40,	//ISP_NR_MMM_MAX
				},
			},
		},
	},

	// Edge enhance
	{
		0x03, 			//ISP_BRIGHT_RATE
		0x02, 			//ISP_BRIGHT_TRM_B1
		0x10, 			//ISP_BRIGHT_TRM_B2
		0x0a, 			//ISP_BRIGHT_TRM_K
		0x03, 			//ISP_BRIGHT_TRM_THD0
		0x18, 			//ISP_BRIGHT_TRM_THD1
		0x03, 			//ISP_DARK_RATE
		0x02, 			//ISP_DARK_TRM_B1
		0x10, 			//ISP_DARK_TRM_B2
		0x0a, 			//ISP_DARK_TRM_K
		0x03, 			//ISP_DARK_TRM_THD0
		0x18, 			//ISP_DARK_TRM_THD1
		20,  				//ISP_EDG_DIFF_C0
		3, 			//ISP_EDG_DIFF_C1
		-1, 			//ISP_EDG_DIFF_C2
		-3, 	    		//ISP_EDG_DIFF_C3
		-2, 			//ISP_EDG_DIFF_C4
		0x08, 			//ISP_EEH_SHARP_ARRAY 0
		0x0C, 			//ISP_EEH_SHARP_ARRAY10
		0x10, 			//ISP_EEH_SHARP_ARRAY11
		0x0C, 			//ISP_EEH_SHARP_ARRAY1
		0x10, 			//ISP_EEH_SHARP_ARRAY2
		0x0C, 			//ISP_EEH_SHARP_ARRAY3
		0x18, 			//ISP_EEH_SHARP_ARRAY4
		0x18, 			//ISP_EEH_SHARP_ARRAY5
		0x10, 			//ISP_EEH_SHARP_ARRAY6
		0x10, 			//ISP_EEH_SHARP_ARRAY7
		0x10, 			//ISP_EEH_SHARP_ARRAY8
		0x08, 			//ISP_EEH_SHARP_ARRAY9

	},

	// flase color and morie
	{
		{
			{
				0x02, 			//ISP_FCRD_MIN
				0x12, 			//ISP_FCRD_YCUTS
				0x90, 			//ISP_FCRD_CFG
				0x1f, 			//ISP_FCRD_MAX
				0x20,			//ISP_MOIRE_RATE
				0x02,			//MOIRE detect threshold
			},
			{
				0x02, 			//ISP_FCRD_MIN
				0x12, 			//ISP_FCRD_YCUTS
				0x90, 			//ISP_FCRD_CFG
				0x1f, 			//ISP_FCRD_MAX
				0x20,			//ISP_MOIRE_RATE
				0x02,			//MOIRE detect threshold
			},
			{
				0x02, 			//ISP_FCRD_MIN
				0x12, 			//ISP_FCRD_YCUTS
				0x90, 			//ISP_FCRD_CFG
				0x1f, 			//ISP_FCRD_MAX
				0x10,			//ISP_MOIRE_RATE
				0x04,			//MOIRE detect threshold
			},
			{
				0x02, 			//ISP_FCRD_MIN
				0x12, 			//ISP_FCRD_YCUTS
				0x90, 			//ISP_FCRD_CFG
				0x1f, 			//ISP_FCRD_MAX
				0x10,			//ISP_MOIRE_RATE
				0x04,			//MOIRE detect threshold
			},
		}

	},

	// dead pixel cancel
	{
		0x01,	//  ISP_DDP_CTRL
		0x00, 	// ISP_DDP_SEL
		0x04,	//	ISP_DP_THD_D1
		0x02, 	//	ISP_DP_BRIGHT_THD_MIN
		0x0A,	//  ISP_DP_BRIGHT_THD_MAX
		0x03, 	//  ISP_DP_DARK_THD_MIN
		0x10,	//  ISP_DP_DARK_THD_MAX
		0x02,	//  ISP_DDP_BRIGHT_RATE
		0x02,	//  ISP_DDP_DARK_RATE
	},

	// UV color tune
	{
		//A light UV color tune
		{
			0, //ISP_UVT_UCENTER
			0, //ISP_UVT_VCENTER
			0, //ISP_UVT_UINC
			0, //ISP_UVT_VINC
		},
		//D65 light UV color tune
		{
			0,  //ISP_UVT_UCENTER
			0,    //ISP_UVT_VINC
			0,   //ISP_UVT_UINC
			0,   //ISP_UVT_VINC
		},
		3300, // Dynamic UV color tune threshold for A light
		4000,  // Dynamic UV color tune threshold for D65 light
	},

	// NLC
	{
		// G NLC
		{0, 16, 32, 48, 64, 80, 96, 112, 128, 144, 160, 176, 192, 208, 224, 240},
		// R - G diff
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		// B - G diff
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
	},

	// HDR
	{
		// HDR threshold
		{
			10,		// Histogram dark pixel threshold for start HDR function
			10,     // Histogram bright pixel threshold for start HDR function
			60, 	// Histogram dark pixel max number
			64,		// Hdr tune max value

		},

		// FW HDR
		{
			// HDR gamma
			{0,23,43,58,73,81,88,94,99,109,117,125,132,139,145,151,157,163,168,173,177,184,190,195,201,210,222,237},
		},

		// HW HDR
		{
			// tgamma threshold
			0x8,
			// tgamma rate
			0x10,
			// HDR LPF COEF
			{0, 1, 3, 5, 6, 6, 5, 4, 2},
			// HDR halo thd
			0x10,
			// HDR curver
			{115, 115, 114, 112, 111, 109, 108, 106, 104, 100, 96, 91, 85, 79, 72, 64, 54, 45, 35, 26, 16, 10, 6, 3},
			// HDR max curver
			{192, 144, 112, 80, 61, 52, 42, 36, 32},
			//HDR step
			0x1,
			//local constrast curver
			{15, 26, 26, 19, 13, 10, 7, 8, 10, 12, 13, 16, 20, 23, 26, 26},
			//local constrast rate min
			0xd,
			//local constrast rate max
			0xd,
			//local constrast step
			0x1
		},
	}

};
#endif // _OV9726_IQ_H_
