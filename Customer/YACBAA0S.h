#ifndef _YACBAA0S_H_
#define _YACBAA0S_H_

#define YACBAA0S_AE_TARGET 0x42

extern OV_CTT_t code gc_YACBAA0S_CTT[3];
void YACBAA0SSetFormatFps(U8 SetFormat, U8 byFps);
void SetYACBAA0SImgDir(U8 bySnrImgDir) ;
void CfgYACBAA0SControlAttr(void);
#ifndef _USE_BK_HSBC_ADJ_
void SetYACBAA0SBrightness(S16 swSetValue);
void	SetYACBAA0SContrast(U16 wSetValue);
void	SetYACBAA0SSaturation(U16 wSetValue);
void	SetYACBAA0SHue(S16 swSetValue);
#endif
void	SetYACBAA0SSharpness(U8 bySetValue);
void SetYACBAA0SOutputDim(U16 wWidth,U16 wHeight);
void SetYACBAA0SPwrLineFreq(U8 byFPS, U8 byLightFrq);
void SetYACBAA0SWBTemp(U16 wRgain, U16 wGgain, U16 wBgain);
OV_CTT_t GetYACBAA0SAwbGain(void);
void SetYACBAA0SWBTempAuto(U8 bySetValue);
void SetYACBAA0SBackLightComp(U8 bySetValue);
U16  GetYACBAA0SAEGain();
void SetYACBAA0SIntegrationTimeAuto(U8 bySetValue);
void SetYACBAA0SIntegrationTime(U32 dwSetValue);
void SetYACBAA0SGain(float fGain);
#endif

