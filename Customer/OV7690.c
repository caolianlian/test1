#include "Inc.h"

#ifdef RTS58XX_SP_OV7690


code OV_CTT_t gc_OV7690_CTT[3] =
{
	{3000,0x40,0x4C,0xAA},
	{4050,0x4C,0x40,0x7E},
	{6500,0x50,0x40,0x54},
};

void CfgOV7690ControlAttr(void)
{
	U8 i;
	g_bySensorSize = SENSOR_SIZE_VGA;
	g_wSensorSPFormat = VGA_FRM;

	{
		memcpy(g_asOvCTT,gc_OV7690_CTT,sizeof(gc_OV7690_CTT));
	}

	{
		// AE backlight compensation parameter
		g_byOVAEW_Normal = OV7690_AEW;
		g_byOVAEB_Normal = OV7690_AEB;

		g_byOVAEW_BLC = g_byOVAEW_Normal + 0x20;
		g_byOVAEB_BLC = g_byOVAEB_Normal + 0x20;
	}

	{
		//g_bySV28VoltSel =  SV28_VOL_2V9;
		//g_bySV18VoltSel=  SV18_VOL_1V5;
		//	XBYTE[REG_TUNESVIO_SVA] =  SV28_VOL_2V9|SV18_VOL_1V5;
		// hemonel 2010-09-08: support 1.8V sensor IO
#if (_CHIP_ID_ & _RTS5840_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V79;
#elif (_CHIP_ID_ & _RTS5832_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V85;
#elif (_CHIP_ID_ & _RTS5829B_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V82;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V80;
#endif
	}

//	if(g_byFormatCfgExist==0) // format config not exist, use default setting.
	{
		//InitFormatFrameFps();
	}

	{
		//====== resolution  setting ===========
		// -- preview ---
		g_aVideoFormat[0].byaVideoFrameTbl[0] = 6;		// resolution number
		g_aVideoFormat[0].byaVideoFrameTbl[1]=F_SEL_640_480;
		g_aVideoFormat[0].byaVideoFrameTbl[2]=F_SEL_160_120;
		g_aVideoFormat[0].byaVideoFrameTbl[3]=F_SEL_176_144;
		g_aVideoFormat[0].byaVideoFrameTbl[4]=F_SEL_320_240;
		g_aVideoFormat[0].byaVideoFrameTbl[5]=F_SEL_352_288;
		g_aVideoFormat[0].byaVideoFrameTbl[6]=F_SEL_640_480;
		//--still image--
		g_aVideoFormat[0].byaStillFrameTbl[0] = 6;	// resolution number
		g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_640_480;
		g_aVideoFormat[0].byaStillFrameTbl[2]=F_SEL_160_120;
		g_aVideoFormat[0].byaStillFrameTbl[3]=F_SEL_176_144;
		g_aVideoFormat[0].byaStillFrameTbl[4]=F_SEL_320_240;
		g_aVideoFormat[0].byaStillFrameTbl[5]=F_SEL_352_288;
		g_aVideoFormat[0].byaStillFrameTbl[6]=F_SEL_640_480;

		//====== fps  setting ===========
		for(i=0; i<7; i++)
		{
			/*640_480,
			160_120,
			176_144,
			320_200,
			320_240,
			352_288,
			640_400*/
			g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_15|FPS_20|FPS_25|FPS_30;
		}

		//====== format type  setting ===========
		g_aVideoFormat[0].byFormatType = FORMAT_TYPE_YUY2;

#ifdef _ENABLE_MJPEG_
		// copy YUY2 setting to MJPEG setting
		memcpy(&(g_aVideoFormat[1]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

		// modify MJPEG format type
		g_aVideoFormat[1].byFormatType = FORMAT_TYPE_MJPG;
#endif

#ifdef _ENABLE_M420_FMT_
		// copy YUY2 setting to M420 setting
		memcpy(&(g_aVideoFormat[2]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

		// modify M420 format type
		g_aVideoFormat[2].byFormatType = FORMAT_TYPE_M420;
#endif

	}
	


}
// hemonel 2009-04-22: from cski
/*
code OV_FpsSetting_t  g_staOV7690FpsSetting[]=
			{//  FPS	extradummypixel clkrc	loop_divider
			//	{1,		(1176+1),	(23),	  1000000},	// 1M
				//{3,		(523+1),	(11),	  2000000},	// 2M	// 2.998
				//{5, 		(0),		(11),	  2000000},	// 2M
			//	{8,  		(196+1),	(5),		  4000000},	// 4M	// 7.995
			//	{9,  		(87+1),	(5),		  4000000},	// 4M	// 8.995
				//{10,		(0),		(5),		  4000000},	// 4M
				{15,		(780),		(1),		  (0x10)},	// 6M
				{20,		(1172),		(0),		  (0x30)},	// 8M
				{25,		(938),	       (0),		(0x10)},	// 12M
				{30,		(780),		(0),		(0x10)},	// 24M
			 };
*/
code OV_FpsSetting_t  g_staOV7690FpsSetting[]=
{
	//  FPS extradummypixel clkrc loop_divider
	// {1,  (1176+1), (23),   1000000}, // 1M
	{3,   (780), 	(9),   	(0x10)}, // pixcel clk =1.2M // 2.998
	{5,   (780),  	(5),   	(0x10)}, // 2M
	// {8,    (196+1), (5),    4000000}, // 4M // 7.995
	// {9,    (87+1), (5),    4000000}, // 4M // 8.995
	{10,  (780),  	(2),    	(0x10)}, // 4M
	{15,  (780),  	(1),    	(0x10)}, // 6M
	{20,  (1172),  	(0),    	(0x10)}, // 8M
	{25,  (938),        (0),  	(0x10)}, // 12M
	{30,  (780),  	(0),  	(0x10)}, // pixcel clk =12M
};

#if 0
t_RegSettingBB code gc_OV7690_Setting[] =
// ov7690.ovd VGA setting
{
//	{0x12, 0x80}, //	reset
	{0x11, 0x03}, //
	{0x3a, 0x04}, // YUYV
	{0x12, 0x00}, //
	// window: H x V, V = 480, H unknown
	{0x17, 0x13}, //
	{0x18, 0x01}, //
	{0x32, 0xb6}, //
	{0x19, 0x02}, //
	{0x1a, 0x7a}, //
	{0x03, 0x0a}, //
	//scale
	{0x0c, 0x00}, // scale/dcw disable
	{0x3e, 0x00}, // normal PCLK at scale
	{0x70, 0x3a}, // scale parameter
	{0x71, 0x35}, // scale parameter
	{0x72, 0x11}, // scale parameter
	{0x73, 0xf0}, // scale parameter
	{0xa2, 0x02}, // scale output delay

	// gamma
	{0x7a, 0x20}, //
	{0x7b, 0x10}, //
	{0x7c, 0x1e}, //
	{0x7d, 0x35}, //
	{0x7e, 0x5a}, //
	{0x7f, 0x69}, //
	{0x80, 0x76}, //
	{0x81, 0x80}, //
	{0x82, 0x88}, //
	{0x83, 0x8f}, //
	{0x84, 0x96}, //
	{0x85, 0xa3}, //
	{0x86, 0xaf}, //
	{0x87, 0xc4}, //
	{0x88, 0xd7}, //
	{0x89, 0xe8}, //

	// AEC
	{0x13, 0xe0}, // fast AEC/ AEC step size unlimited/banding filter on
	{0x00, 0x00}, //
	{0x10, 0x00}, //
	{0x0d, 0x40}, //
	{0x14, 0x18}, // max AGC = 4x
	{0xa5, 0x05}, //
	{0xab, 0x07}, //
	{0x24, 0x95}, // AEW
	{0x25, 0x33}, // AEB
	{0x26, 0xe3}, // VPT
	{0x9f, 0x78}, //
	{0xa0, 0x68}, //
	{0xa1, 0x03}, //
	{0xa6, 0xd8}, //
	{0xa7, 0xd8}, //
	{0xa8, 0xf0}, //
	{0xa9, 0x90}, //
	{0xaa, 0x94}, // histogram AEC
	{0x13, 0xe5}, //

	// common control
	{0x0e, 0x61}, //
	{0x0f, 0x4b}, //
	{0x16, 0x02}, //
	{0x1e, 0x07}, //
	{0x21, 0x02}, //
	{0x22, 0x91}, //
	{0x29, 0x07}, //
	{0x33, 0x0b}, //
	{0x35, 0x0b}, //
	{0x37, 0x1d}, //
	{0x38, 0x71}, //
	{0x39, 0x2a}, //
	{0x3c, 0x78}, //
	{0x4d, 0x40}, //
	{0x4e, 0x20}, //
	{0x69, 0x00}, //
	{0x6b, 0x4a}, //
	{0x74, 0x10}, //
	{0x8d, 0x4f}, //
	{0x8e, 0x00}, //
	{0x8f, 0x00}, //
	{0x90, 0x00}, //
	{0x91, 0x00}, //
	{0x96, 0x00}, //
	{0x9a, 0x80}, //
	{0xb0, 0x84}, //
	{0xb1, 0x0c}, //
	{0xb2, 0x0e}, //
	{0xb3, 0x82}, //
	{0xb8, 0x0a}, //

	// AWB control
	{0x43, 0x0a}, //
	{0x44, 0xf0}, //
	{0x45, 0x34}, //
	{0x46, 0x58}, //
	{0x47, 0x28}, //
	{0x48, 0x3a}, //
	{0x59, 0x88}, //
	{0x5a, 0x88}, //
	{0x5b, 0x44}, //
	{0x5c, 0x67}, //
	{0x5d, 0x49}, //
	{0x5e, 0x0e}, //
	{0x6c, 0x0a}, //
	{0x6d, 0x55}, //
	{0x6e, 0x11}, //
	{0x6f, 0x9f}, // ;9e for advance AWB
	{0x6a, 0x40}, //
	{0x01, 0x40}, //
	{0x02, 0x40}, //
	{0x13, 0xe7}, //

	// CCM
	{0x4f, 0x80}, //
	{0x50, 0x80}, //
	{0x51, 0x00}, //
	{0x52, 0x22}, //
	{0x53, 0x5e}, //
	{0x54, 0x80}, //
	{0x58, 0x9e}, //

	// other ISP control
	{0x41, 0x08}, // Enable AWB gain
	{0x3f, 0x00}, //
	{0x75, 0x05}, //
	{0x76, 0xe1}, //
	{0x4c, 0x00}, //
	{0x77, 0x01}, //
	{0x3d, 0xc2}, //
	{0x4b, 0x09}, //
	{0xc9, 0x60}, //
	{0x41, 0x38}, // Enable auto sharpness and auto de-noise/ enable awb gain
	{0x56, 0x40}, //

	//
	{0x34, 0x11}, //
	{0x3b, 0x02}, //
	{0xa4, 0x88}, //
	{0x96, 0x00}, //
	{0x97, 0x30}, //
	{0x98, 0x20}, //
	{0x99, 0x30}, //
	{0x9a, 0x84}, //
	{0x9b, 0x29}, //
	{0x9c, 0x03}, //
	{0x9d, 0x4c}, //
	{0x9e, 0x3f}, //
	{0x78, 0x04}, //

	//
	{0x79, 0x01}, //
	{0xc8, 0xf0}, //
	{0x79, 0x0f}, //
	{0xc8, 0x00}, //
	{0x79, 0x10}, //
	{0xc8, 0x7e}, //
	{0x79, 0x0a}, //
	{0xc8, 0x80}, //
	{0x79, 0x0b}, //
	{0xc8, 0x01}, //
	{0x79, 0x0c}, //
	{0xc8, 0x0f}, //
	{0x79, 0x0d}, //
	{0xc8, 0x20}, //
	{0x79, 0x09}, //
	{0xc8, 0x80}, //
	{0x79, 0x02}, //
	{0xc8, 0xc0}, //
	{0x79, 0x03}, //
	{0xc8, 0x40}, //
	{0x79, 0x05}, //
	{0xc8, 0x30}, //
	{0x79, 0x26}, //
};
#else
t_RegSettingBB code gc_OV7690_Setting[] =
{

//#if 0

	{0x12,0x80},



//=Format////////=;;

	{0x12,0x00},
	{0x82,0x03},
	{0xd0,0x48},
	{0x80,0x7f},
	{0x3e,0x30},
	{0x22,0x00},

//////////////
	{0x0c,0x16},
	{0x48,0x42},
	{0x41,0x43},
	{0x81,0xff},
	{0x21,0x44},
	{0x16,0x03},
	{0x39,0x80},
	/*
	 //=Resolution////////=;;

	{0x17,0xf9},
	{0x18,0x5c},
	{0x19,0x6c},
	{0x1a,0x96},


	{0xc8,0x01},
	{0xc9,0x60}, //;ISP input hsize (640)
	{0xca,0x01},
	{0xcb,0x20}, //;ISP input vsize (480)

	{0xcc,0x01},
	{0xcd,0x60}, //;ISP output hsize (640)
	{0xce,0x01},
	{0xcf,0x20}, //;ISP output vsize (480)




	  */

//=Lens Correction////////;;
	{0x85,0x90},
	{0x86,0x00},
	{0x87,0x00},
	{0x88,0x10},
	{0x89,0x30},
	{0x8a,0x29},
	{0x8b,0x26},

//////////Color Matrix////////////////;;

	{0xbb,0x80},
	{0xbc,0x62},
	{0xbd,0x1e},
	{0xbe,0x26},
	{0xbf,0x7b},
	{0xc0,0xac},
	{0xc1,0x1e},

//=Edge + Denoise////////////////;;
	{0xb7,0x0c},
	{0xb8,0x04},
	{0xb9,0x00},
	{0xba,0x04},

//=UVAdjust////////////////;;
	{0x5A,0x14},
	{0x5B,0xA2},
	{0x5C,0x70},
	{0x5d,0x20},


//////////AEC/AGC target////////////////;

	{0x24,0x78},
	{0x25,0x68},
	{0x26,0xb3},

//////////Gamma////////////////;;

	{0xa3,0x0b},
	{0xa4,0x15},
	{0xa5,0x2a},
	{0xa6,0x51},
	{0xa7,0x63},
	{0xa8,0x74},
	{0xa9,0x83},
	{0xaa,0x91},
	{0xab,0x9e},
	{0xac,0xaa},
	{0xad,0xbe},
	{0xae,0xce},
	{0xaf,0xe5},
	{0xb0,0xf3},
	{0xb1,0xfb},
	{0xb2,0x06},



//=AWB////////=;;

//Simple////////;;

//Advance////////;;

	{0x8c,0x5d},
	{0x8d,0x11},
	{0x8e,0x12},
	{0x8f,0x11},
	{0x90,0x50},
	{0x91,0x22},
	{0x92,0xd1},
	{0x93,0xa7},
	{0x94,0x23},
	{0x95,0x3b},
	{0x96,0xff},
	{0x97,0x00},
	{0x98,0x4a},
	{0x99,0x46},
	{0x9a,0x3d},
	{0x9b,0x3a},
	{0x9c,0xf0},
	{0x9d,0xf0},
	{0x9e,0xf0},
	{0x9f,0xff},
	{0xa0,0x56},
	{0xa1,0x55},
	{0xa2,0x13},



//General Control////////;;

	{0x14,0x20},
	{0x13,0xf7},
	{0x15, 0x90}, // hemonel 2009-04-17:night mode, 1/2 frame rate.





//};
//  #if 0
//#endif
};
/*
t_RegSettingBB code gc_OV7690_LSC_Setting[] =
{
	{0x85,0x90},// center X
	{0x86,0x00},// center Y
	{0x87,0x00},// G curve compensation
	{0x88,0x10},// LSC enable
	{0x89,0x30}, // LSC enable
	{0x8a,0x29},// B curve compensation
	{0x8b,0x26},// R curve compensation

};

t_RegSettingBB code gc_OV7690_AWB_Setting[] =
{

	{0x8c,0x5d},
	{0x8d,0x11},
	{0x8e,0x12},
	{0x8f,0x11},
	{0x90,0x50},
	{0x91,0x22},
	{0x92,0xd1},
	{0x93,0xa7},
	{0x94,0x23},
	{0x95,0x3b},
	{0x96,0xff},
	{0x97,0x00},
	{0x98,0x4a},
	{0x99,0x46},
	{0x9a,0x3d},
	{0x9b,0x3a},
	{0x9c,0xf0},
	{0x9d,0xf0},
	{0x9e,0xf0},
	{0x9f,0xff},
	{0xa0,0x56},
	{0xa1,0x55},
	{0xa2,0x13},


};
*/
t_RegSettingBB code gc_OV7690_ISP_Setting[] =
{
	//=Lens Correction//2009-04-14 Setting from Pao-Chi
	{0x85,0x90},
	{0x86,0x00},
	{0x87,0x20},
	{0x88,0x09},
	{0x89,0x3a},
	{0x8a,0x32},
	{0x8b,0x2f},

	//Color Matrix//2009-04-14 Setting from Pao-Chi
	{0xbb,0x72},
	{0xbc,0x58},
	{0xbd,0x1a},
	{0xbe,0x17},
	{0xbf,0x72},
	{0xc0,0x88},
	{0xc1,0x1e},

	//=UVAdjust// //2009-04-14 Setting from Pao-Chi
	{0x5A,0x4a},
	{0x5B,0x9f},
	{0x5C,0x48},
	{0x5d,0x32},


	//Advance AWB//2009-04-14 Setting from Pao-Chi
	{0x8c,0x5c},
	{0x8d,0x11},
	{0x8e,0x12},
	{0x8f,0x19},
	{0x90,0x50},
	{0x91,0x20},
	{0x92,0x98},
	{0x93,0xa0},
	{0x94,0x2a},
	{0x95,0x1c},
	{0x96,0xff},
	{0x97,0x00},
	{0x98,0x3b},
	{0x99,0x29},
	{0x9a,0x15},
	{0x9b,0x42},
	{0x9c,0xf0},
	{0x9d,0xf0},
	{0x9e,0xf0},
	{0x9f,0xff},
	{0xa0,0x6c},
	{0xa1,0x56},
	{0xa2,0x0e},

	//General Control////////;;
	{0x68,0xb8},  //2009-04-14 Setting from Pao-Chi
};

t_RegSettingBB code gc_OV7690_640X480[] =
{
	{0x17,0x69},
	{0x18,0xa4},
	{0x19,0x0c},
	{0x1a,0xf6},
	{0xc8,0x02},
	{0xc9,0x80},
	{0xca,0x01},
	{0xcb,0xe0},
	{0xcc,0x02},
	{0xcd,0x80}, //;ISP input hsize (640)
	{0xce,0x01},
	{0xcf,0xe0}, //ISP input vsize (480)

};
#if 0
t_RegSettingBB code gc_OV7690_320X240[] =
{
	{0x17,0x69},
	{0x18,0xa4},
	{0x19,0x0c},  //{0x19,0x04},
	{0x1a,0xf6},
	{0xc8,0x02},
	{0xc9,0x80},
	{0xca,0x01}, //{0xca,0x00},
	{0xcb,0xe0},//{0xcb,0xf0},
	{0xcc,0x01},
	{0xcd,0x40},
	{0xce,0x00},
	{0xcf,0xf0},


};

t_RegSettingBB code gc_OV7690_176X144[] =
{



	{0x17,0x69},//{0x17,0x83},
	{0x18,0xa4}, //{0x18,0x97},
	{0x19,0x0c}, //{0x19,0x04},
	{0x1a,0xf6},//{0x1a,0xf6},

	{0xc8,0x02},
	{0xc9,0x80},//{0xc9,0x4c},
	{0xca,0x01},//{0xca,0x00},
	{0xcb,0xe0},//{0xcb,0xf0},

	{0xcc,0x00},
	{0xcd,0xb0},
	{0xce,0x00},
	{0xcf,0x90},



};

t_RegSettingBB code gc_OV7690_160X120[] =
{



	{0x17,0x69},//{0x17,0x83},
	{0x18,0xa4}, //{0x18,0x97},
	{0x19,0x0c}, //{0x19,0x04},
	{0x1a,0xf6},//{0x1a,0xf6},

	{0xc8,0x02},
	{0xc9,0x80},//{0xc9,0x4c},
	{0xca,0x01},//{0xca,0x00},
	{0xcb,0xe0},//{0xcb,0xf0},

	{0xcc,0x00},
	{0xcd,0xa0},
	{0xce,0x00},
	{0xcf,0x78},



};
t_RegSettingBB code gc_OV7690_352X288[] =
{


	{0x17,0x69},//{0x17,0xf9},
	{0x18,0xa4},//{0x18,0x5c},
	{0x19,0x0c},//{0x19,0x6c},
	{0x1a,0xf6},//{0x1a,0x96},

	{0xc8,0x02}, //{0xc8,0x01},
	{0xc9,0x80},//{0xc9,0x60},
	{0xca,0x01},
	{0xcb,0xe0},//{0xcb,0x20},

	{0xcc,0x01},
	{0xcd,0x60},
	{0xce,0x01},
	{0xcf,0x20},


};
#endif
//t_RegSettingBB code gc_OV7690_160X120[] =
//{
//};
#endif

#if 0
void SetOV7690OutputDim(U16 wWidth,U16 wHeight)
{
	wHeight = wHeight; // for delete warning

	switch(wWidth)

	{
	case 320:
		WriteSensorSettingBB(sizeof(gc_OV7690_320X240)>>1, gc_OV7690_320X240);
		break;
	case 640:
		WriteSensorSettingBB(sizeof(gc_OV7690_640X480)>>1, gc_OV7690_640X480);
//ISP input vsize (480)
		break;
	case 352:
		WriteSensorSettingBB(sizeof(gc_OV7690_352X288)>>1, gc_OV7690_352X288);
		break;
	case 176:
		WriteSensorSettingBB(sizeof(gc_OV7690_176X144)>>1, gc_OV7690_176X144);
		break;
	case 160:
		WriteSensorSettingBB(sizeof(gc_OV7690_160X120)>>1, gc_OV7690_160X120);
		break;
	default:
		WriteSensorSettingBB(sizeof(gc_OV7690_640X480)>>1, gc_OV7690_640X480);
		break;

	}
}
#endif
/*

if ((wWidth==320)&&(wHeight==240))
	{
	Write_SenReg(0x17,0x69);
	Write_SenReg(0x18,0xa4);
	Write_SenReg(0x19,0x04);
	Write_SenReg(0x1a,0xf6);

	Write_SenReg(0xc8,0x02);
	Write_SenReg(0xc9,0x80);
	Write_SenReg(0xca,0x00);
	Write_SenReg(0xcb,0xf0);

	Write_SenReg(0xcc,0x01);
	Write_SenReg(0xcd,0x40);
	Write_SenReg(0xce,0x00);
	Write_SenReg(0xcf,0xf0);
	}
else if ((wWidth==176)&&(wHeight==144))
	{
	Write_SenReg(0x17,0x83);
	Write_SenReg(0x18,0x97);
	Write_SenReg(0x19,0x04);
	Write_SenReg(0x1a,0xf6);

	Write_SenReg(0xc8,0x02);
	Write_SenReg(0xc9,0x4c);
	Write_SenReg(0xca,0x00);
	Write_SenReg(0xcb,0xf0);

	Write_SenReg(0xcc,0x00);
	Write_SenReg(0xcd,0xb0);
	Write_SenReg(0xce,0x00);
	Write_SenReg(0xcf,0x90);

	}
else if ((wWidth==352)&&(wHeight==288))
	{
	Write_SenReg(0x17,0xf9);
	Write_SenReg(0x18,0x5c);
	Write_SenReg(0x19,0x6c);
	Write_SenReg(0x1a,0x96);

	Write_SenReg(0xc8,0x01);
	Write_SenReg(0xc9,0x60);
	Write_SenReg(0xca,0x01);
	Write_SenReg(0xcb,0x20);

	Write_SenReg(0xcc,0x01);
	Write_SenReg(0xcd,0x60);
	Write_SenReg(0xce,0x01);
	Write_SenReg(0xcf,0x20);

	}
else if ((wWidth==320)&&(wHeight==240))
	{
	Write_SenReg(0x17,0x69);
	Write_SenReg(0x18,0xa4);
	Write_SenReg(0x19,0x04);
	Write_SenReg(0x1a,0xf6);

	Write_SenReg(0xc8,0x02);
	Write_SenReg(0xc9,0x80);
	Write_SenReg(0xca,0x00);
	Write_SenReg(0xcb,0xf0);

	Write_SenReg(0xcc,0x01);
	Write_SenReg(0xcd,0x40);
	Write_SenReg(0xce,0x00);
	Write_SenReg(0xcf,0xf0);

	}
else if ((wWidth==160)&&(wHeight==120))
	{
	}
else
	{
	Write_SenReg(0x17, 0x69);
       Write_SenReg(0x18, 0xa4);
	Write_SenReg(0x19, 0x0c);
	Write_SenReg(0x1A, 0xf6);

	Write_SenReg(0x17,0x69);
	Write_SenReg(0x18,0xa4);
	Write_SenReg(0x19,0x0c);
	Write_SenReg(0x1a,0xf6);

	Write_SenReg(0xc8,0x02);
	Write_SenReg(0xc9,0x80); //;ISP input hsize (640)
	Write_SenReg(0xca,0x01);
	Write_SenReg(0xcb,0xe0); //ISP input vsize (480)
	}
}
*/


static OV_FpsSetting_t*  GetOvFpsSetting(U8 Fps, OV_FpsSetting_t staOvFpsSetting[], U8 byArrayLen)
{
	U8 i;
	U8 Idx;

	ISP_MSG((" array size = %bd\n", byArrayLen));

	if(g_bIsHighSpeed)
	{
		Idx=2; //8fps for uxga, 10 fps for vga
	}
	else
	{
		Idx = 0;
	}

	for(i=0; i< byArrayLen; i++)
	{
		ISP_MSG((" fps i = %bd\n",i));
		if(staOvFpsSetting[i].byFps ==Fps)
		{
			Idx=i;
			break;
		}
	}
	ISP_MSG((" get fps idx = %bd\n",Idx));
	return &staOvFpsSetting[Idx];
}

void SetOV7690PwrLineFreq(U8 byFPS, U8 byLightFrq)
{
	U8 byBandingFilterVal,byBandingFilterStep;
	U8 byBandingTime;
	U8 tmp_byBandingFilterStep;
	//U16 wTemp;
	OV_FpsSetting_t *pOv7690_FpsSetting;

	pOv7690_FpsSetting = GetOvFpsSetting(byFPS, g_staOV7690FpsSetting, sizeof(g_staOV7690FpsSetting)/sizeof(OV_FpsSetting_t));
	//pOv7690_FpsSetting = g_staOV7690FpsSetting;

	if(PWR_LINE_FRQ_DIS == byLightFrq)
	{
		// disable banding filter
		Write_SenReg_Mask(0x13, 0x00, 0x30);
	}
	else
	{
		// enable banding filter
		Write_SenReg_Mask(0x13, 0x20, 0x30);

		// calculate banding filter value and step
		// hemonel 2009-04-23: modify banding filter value from practical pixel clock.
		/*
		if(PWR_LINE_FRQ_50 == byLightFrq)
		{
			byBandingTime = 100;
		             byBandingFilterVal =120000/(pOv7690_FpsSetting->wExtraDummyPixel);

		}
		else
		{
			byBandingTime = 120;
			byBandingFilterVal =100000/(pOv7690_FpsSetting->wExtraDummyPixel);
		}
		*/
		if(PWR_LINE_FRQ_50 == byLightFrq)
		{
			byBandingTime = 100;
			byBandingFilterVal =(120000/((pOv7690_FpsSetting->byClkrc)+1))/(pOv7690_FpsSetting->wExtraDummyPixel);
		}
		else
		{
			byBandingTime = 120;
			byBandingFilterVal =(100000/((pOv7690_FpsSetting->byClkrc)+1))/(pOv7690_FpsSetting->wExtraDummyPixel);
		}

		byBandingFilterStep = byBandingTime/byFPS -1;

		// hemonel 2009-02-18 bug: AE not work when fps too slow
		// banding filter value must be more than 0x10
		if(byBandingFilterVal<0x10)
		{
			byBandingTime>>=1;
			byBandingFilterVal <<=1;
			byBandingFilterStep = byBandingTime/byFPS -1;
		}

		if(PWR_LINE_FRQ_50 == byLightFrq)
		{

			Write_SenReg_Mask(0x14, 0x01, 0x01);
			tmp_byBandingFilterStep = byBandingFilterStep;
			Write_SenReg_Mask(0x20,  (tmp_byBandingFilterStep<<3), 0x80);

			tmp_byBandingFilterStep = byBandingFilterStep;
			Write_SenReg_Mask(0x21,  (tmp_byBandingFilterStep <<4), 0xf0);
			Write_SenReg_Mask(0x50,  (byBandingFilterVal), 0xff);

		}
		else
		{

			Write_SenReg_Mask(0x14, 0x00, 0x01);
			tmp_byBandingFilterStep = byBandingFilterStep;
			Write_SenReg_Mask(0x20,  (tmp_byBandingFilterStep<<2), 0x40);
			tmp_byBandingFilterStep = byBandingFilterStep;
			Write_SenReg_Mask(0x21,  (tmp_byBandingFilterStep <<0), 0x0f);
			Write_SenReg_Mask(0x51,  (byBandingFilterVal), 0xff);
		}

		//Write_SenReg(0x22, byBandingFilterVal);
		//Write_SenReg(0x23, byBandingFilterStep);
	}
}
#if 0
void SetOV7690WindowCrop(U16 wXStart,U16 wXEnd,U16 wYStart,U16 wYEnd )
{
	U16 oring_sizeX,oring_sizeX2;
	//DBG(("@12-->%d,%d,%d,%d\n",wXStart, wXEnd, wYStart, wYEnd));

	// recalculate window
	wXEnd = wXEnd - wXStart ;	// width
	wYEnd = wYEnd - wYStart;	// height
	oring_sizeX = wXEnd;
	oring_sizeX2 = wXEnd;
	//oring_sizeY = wYEnd;
	//wXStart ;
	//wYStart ;
	//window crop

	// update window
	Write_SenReg(0x17, wXStart);
	Write_SenReg(0x18, wXEnd>>2);
	Write_SenReg_Mask(0x16, (oring_sizeX <<6),0x40);
	Write_SenReg(0x19, wYStart);
	Write_SenReg(0x1A, wYEnd>>1);

	//Write_SenReg_Mask(0x32, ((wYStart&0x01)<<6)|((wXStart&0x03)<<4)|((wYEnd&0x01)<<2)|(wXEnd&0x03), 0x77);
	/*
	Write_SenReg(0x17,0x69);
	Write_SenReg(0x18,0xa4);
	Write_SenReg(0x19,0x0c);
	Write_SenReg(0x1a,0xf6);
	*/


	switch(oring_sizeX2)

	{
	case 320:
		WriteSensorSettingBB(sizeof(gc_OV7690_320X240)>>1, gc_OV7690_320X240);
		break;
	case 640:
		WriteSensorSettingBB(sizeof(gc_OV7690_640X480)>>1, gc_OV7690_640X480);

		break;
	case 352:
		WriteSensorSettingBB(sizeof(gc_OV7690_352X288)>>1, gc_OV7690_352X288);
		break;
	case 176:
		WriteSensorSettingBB(sizeof(gc_OV7690_176X144)>>1, gc_OV7690_176X144);
		break;
	case 160:
		WriteSensorSettingBB(sizeof(gc_OV7690_160X120)>>1, gc_OV7690_160X120);
		break;
	default:
		WriteSensorSettingBB(sizeof(gc_OV7690_640X480)>>1, gc_OV7690_640X480);
		break;

	}


	/*
		Write_SenReg(0xc8,0x02);
		Write_SenReg(0xc9,0x80); //;ISP input hsize (640)
		Write_SenReg(0xca,0x01);
		Write_SenReg(0xcb,0xe0); //ISP input vsize (480)

		Write_SenReg(0xcc,0x02);
		Write_SenReg(0xcd,0x80); //ISP output hsize (640)
		Write_SenReg(0xce,0x01);
		Write_SenReg(0xcf,0xe0);//ISP output vsize (480)
	       */


	return;
}
#endif
#ifndef _USE_BK_SPECIAL_EFFECT_
void SetOV7690Effect(U8 byEffect)
{

	U8 byEfft = 0x20, bySDE = 0x20; //SDE: special digital effect enable
	U8 byUvalue = 0x80, byVvalue = 0x80;

	switch (byEffect)
	{
	case SNR_EFFECT_NEGATIVE:
		//byEfft = 0x20;
		bySDE = 0x40;
		break;
	case SNR_EFFECT_MONOCHROME:
		//byEfft = 0x20;
		bySDE = 0x20;
		break;
	case SNR_EFFECT_SEPIA:
		//byEfft = 0x20;
		bySDE = 0x18;
		byUvalue = 0x40;
		byVvalue = 0xa0;
		break;
	case SNR_EFFECT_GREENISH:
		//byEfft = 0x20;
		bySDE = 0x18;
		byUvalue = 0x60;
		byVvalue = 0x60;
		break;
	case SNR_EFFECT_REDDISH:
		//byEfft = 0x20;
		bySDE = 0x18;
		byUvalue = 0x80;
		byVvalue = 0xc0;
		break;
	case SNR_EFFECT_BLUISH:
		//byEfft = 0x20;
		bySDE = 0x18;
		byUvalue = 0xa0;
		byVvalue = 0x40;
		break;
	default:
		byEfft = 0x0; //no effect
		bySDE = 0x0;
		break;
	}
	Write_SenReg_Mask(0xd2, bySDE, 0x60);
//	Write_SenReg_Mask(0x64, byEfft, 0x20);
//	Write_SenReg_Mask(0xa6, bySDE, 0xf8);
//	Write_SenReg(0x60, byUvalue);
//	Write_SenReg(0x61, byVvalue);

}
#endif //#ifndef _USE_BK_SPECIAL_EFFECT_

void SetOV7690Gain(float fGain)
{
}

void SetOV7690ImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.
{
	/***
		0x40 mirror
		0x80 flip
		0xc0 mirror and flip
	***/
	Write_SenReg_Mask(0x0C, (bySnrImgDir&0x03)<<6,0xC0);
}

void SetOV7690WBTemp(U16 wRgain, U16 wGgain, U16 wBgain) {

	Write_SenReg(0x02, wRgain);
	Write_SenReg(0x03, wGgain);
	Write_SenReg(0x01, wBgain);
}
void SetOV7690WBTempAuto(U8 bySetValue)
{
	if(bySetValue)
	{
		// atuo white balance
		Write_SenReg_Mask(0x13, 0x02,0x02);
	}
	else
	{
		// manual white balance
		Write_SenReg_Mask(0x13, 0x00,0x02);
	}
}


// manual sharpness mode
void	SetOV7690Sharpness(U8 bySetValue)
{


	Write_SenReg_Mask(0xb4, 0x26, 0x2f);
	Write_SenReg_Mask(0xb6, (bySetValue<<1), 0x0f);

}


void SetOV7690BackLightComp(U8 bySetValue)
{
	U8 byAEW,byAEB;

	if(bySetValue)
	{
		byAEW = g_byOVAEW_BLC;
		byAEB = g_byOVAEB_BLC;
	}
	else
	{
		byAEW = g_byOVAEW_Normal;
		byAEB = g_byOVAEB_Normal;
	}
	Write_SenReg(0x24, byAEW);
	Write_SenReg(0x25, byAEB);
}


/*
static OV_FpsSetting_t*  GetOvFpsSetting(U8 Fps, OV_FpsSetting_t staOvFpsSetting[], U8 byArrayLen)
{
	U8 i;
	U8 Idx;

	ISP_MSG((" array size = %bd\n", byArrayLen));

	if(g_bIsHighSpeed)
	{
		Idx=2; //8fps for uxga, 10 fps for vga
	}
	else
	{
		Idx = 0;
	}

	for(i=0;i< byArrayLen;i++)
	{
		ISP_MSG((" fps i = %bd\n",i));
		if(staOvFpsSetting[i].byFps ==Fps)
		{
			Idx=i;
			break;
		}
	}
	ISP_MSG((" get fps idx = %bd\n",Idx));
	return &staOvFpsSetting[Idx];
}
*/

void OV7690SetFormatFps(U8 SetFormat, U8 Fps)   //cski_20090327
{
	OV_FpsSetting_t *pOv7690_FpsSetting;

	SetFormat = SetFormat; // for delete warning

	//DBG_OV7690(("enter ov7690fps function"));
	pOv7690_FpsSetting=GetOvFpsSetting(Fps, g_staOV7690FpsSetting, sizeof(g_staOV7690FpsSetting)/sizeof(OV_FpsSetting_t));
	// 1) change hclk if necessary
	// ov7670 hclk at 24M ok

	// initial all register setting
	WriteSensorSettingBB(sizeof(gc_OV7690_Setting)>>1, gc_OV7690_Setting);
	// hemonel 2010-01-13: move to here
	WriteSensorSettingBB(sizeof(gc_OV7690_ISP_Setting) >>1, gc_OV7690_ISP_Setting);

	WriteSensorSettingBB(sizeof(gc_OV7690_640X480)>>1, gc_OV7690_640X480);


	// 2) write sensor register for fps
	Write_SenReg_Mask(0x29, (U8)pOv7690_FpsSetting->dwPixelClk, 0x30);	// write CLKRC
	Write_SenReg_Mask(0x11, pOv7690_FpsSetting->byClkrc, 0x3F);	// write CLKRC
	Write_SenReg(0x2B, INT2CHAR(pOv7690_FpsSetting->wExtraDummyPixel,0));	//Write dummy pixel LSB
	Write_SenReg_Mask(0x2A, (pOv7690_FpsSetting->wExtraDummyPixel)>>4, 0xF0); // Writedummy pixel MSB

	// 3) update variable for AE
	//g_wAECExposureRowMax = (492);
}

void SetOV7690IntegrationTimeAuto(U8 bySetValue)
{
}

void SetOV7690IntegrationTime(U16 wEspline)
{
}
#endif
