#include "Inc.h"
#include "CamUvcRtkExtCtl.h"

//          ===>product string<===
U8	code 	iProduct[] =
{
	12,  0x03,
	'U',
	'S',
	'B',
	' ',
	'C',
	'a',
	'm',
	'e',
	'r',
	'a'
};

//          ===>Device Descriptor<===
U8 code VideoDevDesc[18]=
{
	DESC_SIZE_DEV, //bLength:
	USB_DEVICE_DESCRIPTOR_TYPE, //bDescriptorType:
#ifdef _USB2_LPM_
	0x01, //bcdUSB version:LPM: 0210H
#else
	0x00, //bcdUSB version:NORM:0200H
#endif
	0x02,
	DEV_CLS_MULTI, //bDeviceClass:                  -> This is a Multi-interface Function Code Device
	DEV_SUBCLS_COMMON, //bDeviceSubClass:               -> This is the Common Class Sub Class
	DEV_PROTOCOL_IAD, //bDeviceProtocol:               -> This is the Interface Association Descriptor protocol
	0x40, //bMaxPacketSize0:              = (64) Bytes
	_RT_VID_L_, //idVendor:                     = Realtek Corp.
	_RT_VID_H_,
	_5840_PID_L_, //idProduct:
	_5840_PID_H_,
    FW_CUS_LIB_VER, //bcdDevice:                   1.00
    FW_TRK_VER,
	I_MANUFACTURER, //iManufacturer:
	I_PRODUCT, //iProduct:
	I_SERIALNUMBER, //iSerialNumber:
	0x01 //bNumConfigurations:
};

void InitCustomizedVars(void )
{
	// property page control enable/disable
#ifdef _AF_ENABLE_
	g_wCamTrmCtlSel=CT_CTL_SEL_AUTO_EXP_MODE
	                |CT_CTL_SEL_AUTO_EXP_PRY     	// hemonel 2009-09-21: enable low light compensation for foxlink+HP commercial
	                |CT_CTL_SEL_EXPOSURE_TIME_ABS
	                //CT_CTL_SEL_EXPOSURE_TIME_REL
	                |CT_CTL_SEL_PANTILT_ABS
	                |CT_CTL_SEL_ZOOM_ABS
	                |CT_CTL_SEL_ROLL_ABS
	                |CT_CTL_SEL_FOCUS_ABS
	                ;
	g_byCamTrmCtlSel_Ext = CT_CTL_SEL_FOCUS_AUTO;
#else
	g_wCamTrmCtlSel=CT_CTL_SEL_AUTO_EXP_MODE
	                |CT_CTL_SEL_AUTO_EXP_PRY     	// hemonel 2009-09-21: enable low light compensation for foxlink+HP commercial
	                |CT_CTL_SEL_EXPOSURE_TIME_ABS
	                //CT_CTL_SEL_EXPOSURE_TIME_REL
	                |CT_CTL_SEL_PANTILT_ABS
	                |CT_CTL_SEL_ZOOM_ABS
	                |CT_CTL_SEL_ROLL_ABS
	                ;
	g_byCamTrmCtlSel_Ext = 0;
#endif

	g_wProcUnitCtlSel=PU_CTL_SEL_BRIGHTNESS
	                  |PU_CTL_SEL_CONTRAST
	                  |PU_CTL_SEL_HUE
	                  |PU_CTL_SEL_SATURATION
	                  |PU_CTL_SEL_SHARPNESS
	                  |PU_CTL_SEL_GAMMA
	                  |PU_CTL_SEL_WHITE_BALANCE_TEMPERATURE
	                  //|PU_CTL_SEL_WHITE_BALANCE_COMPONENT
	                  |PU_CTL_SEL_BACKLIGHT_COMPENSATION
	                  //|PU_CTL_SEL_GAIN
	                  |PU_CTL_SEL_POWER_LINE_FREQUENCY
	                  //|PU_CTL_SEL_HUE_AUTO
	                  |PU_CTL_SEL_WHITE_BALANCE_TEMPERATURE_AUTO
	                  //|PU_CTL_SEL_WHITE_BALANCE_COMPONENT_AUTO
	                  //|PU_CTL_SEL_DIGITAL_MULTIPLIER
	                  //|PU_CTL_SEL_DIGITAL_MULTIPLIER_LIMIT
	                  ;

#if defined _RTK_EXTENDED_CTL_ || defined _LENOVO_IDEA_EYE_EXTENDEDUNIT_
	g_dwRtkExtUnitCtlSel = 0
#ifdef _RTK_EXTENDED_CTL_
							|RTK_EXT_CTL_SEL_ISP_SPECIAL_EFFECT
							|RTK_EXT_CTL_SEL_EVCOM
							|RTK_EXT_CTL_SEL_CTE
							|RTK_EXT_CTL_SEL_ROI
							|RTK_EXT_CTL_SEL_ROISTS
							|RTK_EXT_CTL_SEL_PREVIEW_LED_OFF
							|RTK_EXT_CTL_SEL_ISO
#endif							
#ifdef _LENOVO_IDEA_EYE_EXTENDEDUNIT_
							|RTK_EXT_CTL_SEL_IDEAEYE_SENSITIVITY	
							|RTK_EXT_CTL_SEL_IDEAEYE_STATUS		
							|RTK_EXT_CTL_SEL_IDEAEYE_MODE
#endif		
							;							
#endif

	// property page process unit control configure
	BrightnessItem.Min = CTL_MIN_PU_BRIGHTNESS;//min
	BrightnessItem.Max = CTL_MAX_PU_BRIGHTNESS;//max
	BrightnessItem.Res = CTL_RES_PU_BRIGHTNESS;//res
	BrightnessItem.Def = CTL_DEF_PU_BRIGHTNESS;//def
	BrightnessItem.Des = CTL_DEF_PU_BRIGHTNESS;//des
	BrightnessItem.Last = CTL_DEF_PU_BRIGHTNESS;//last

	ContrastItem.Min = CTL_MIN_PU_CONTRAST;//min
	ContrastItem.Max = CTL_MAX_PU_CONTRAST;//max
	ContrastItem.Res = CTL_RES_PU_CONTRAST;//res
	ContrastItem.Def = CTL_DEF_PU_CONTRAST;//def
	ContrastItem.Des = CTL_DEF_PU_CONTRAST;//des
	ContrastItem.Last = CTL_DEF_PU_CONTRAST;//last

	HueItem.Min = CTL_MIN_PU_HUE;//min
	HueItem.Max = CTL_MAX_PU_HUE;//max
	HueItem.Res = CTL_RES_PU_HUE;//res
	HueItem.Def = CTL_DEF_PU_HUE;//def
	HueItem.Des = CTL_DEF_PU_HUE;//des
	HueItem.Last = CTL_DEF_PU_HUE;//last

	SaturationItem.Min = CTL_MIN_PU_SATURATION;//min
	SaturationItem.Max = CTL_MAX_PU_SATURATION;//max
	SaturationItem.Res = CTL_RES_PU_SATURATION;//res
	SaturationItem.Def = CTL_DEF_PU_SATURATION;//def
	SaturationItem.Des = CTL_DEF_PU_SATURATION;//des
	SaturationItem.Last = CTL_DEF_PU_SATURATION;//last

	SharpnessItem.Min = CTL_MIN_PU_SHARPNESS;//min
	SharpnessItem.Max = CTL_MAX_PU_SHARPNESS;//max
	SharpnessItem.Res = CTL_RES_PU_SHARPNESS;//res
	SharpnessItem.Def = CTL_DEF_PU_SHARPNESS;//def
	SharpnessItem.Des = CTL_DEF_PU_SHARPNESS;//des
	SharpnessItem.Last = CTL_DEF_PU_SHARPNESS;//last

	GammaItem.Min = CTL_MIN_PU_GAMMA;//min
	GammaItem.Max = CTL_MAX_PU_GAMMA;//max
	GammaItem.Res = CTL_RES_PU_GAMMA;//res
	GammaItem.Def = CTL_DEF_PU_GAMMA;//def
	GammaItem.Des = CTL_DEF_PU_GAMMA;//des
	GammaItem.Last = CTL_DEF_PU_GAMMA;//last

	WhiteBalanceTempItem.Min = CTL_MIN_PU_WHITE_BALANCE_TEMPERATURE;//min
	WhiteBalanceTempItem.Max = CTL_MAX_PU_WHITE_BALANCE_TEMPERATURE;//max
	WhiteBalanceTempItem.Res = CTL_RES_PU_WHITE_BALANCE_TEMPERATURE;//res
	WhiteBalanceTempItem.Def = CTL_DEF_PU_WHITE_BALANCE_TEMPERATURE;//def
	WhiteBalanceTempItem.Des = CTL_DEF_PU_WHITE_BALANCE_TEMPERATURE;//des
	WhiteBalanceTempItem.Last = CTL_DEF_PU_WHITE_BALANCE_TEMPERATURE;//last

	WhiteBalanceTempAutoItem.Min = 0;//min
	WhiteBalanceTempAutoItem.Max = 1;//max
	WhiteBalanceTempAutoItem.Res = 1;//res
	WhiteBalanceTempAutoItem.Def = 1;//def
	WhiteBalanceTempAutoItem.Des = 1;//des
	WhiteBalanceTempAutoItem.Last = 1;//last

	BackLightCompItem.Min = CTL_MIN_PU_BACKLIGHTCOMP;//min
	BackLightCompItem.Max = CTL_MAX_PU_BACKLIGHTCOMP;//max
	BackLightCompItem.Res = CTL_RES_PU_BACKLIGHTCOMP;//res
	BackLightCompItem.Def = CTL_DEF_PU_BACKLIGHTCOMP;//def
	BackLightCompItem.Des = CTL_DEF_PU_BACKLIGHTCOMP;//des
	BackLightCompItem.Last = CTL_DEF_PU_BACKLIGHTCOMP;//last

	GainItem.Min = CTL_MIN_PU_GAIN;//min
	GainItem.Max = CTL_MAX_PU_GAIN;//max
	GainItem.Res = CTL_RES_PU_GAIN;//res
	GainItem.Def = CTL_DEF_PU_GAIN;//def
	GainItem.Des = CTL_DEF_PU_GAIN;//des
	GainItem.Last = CTL_DEF_PU_GAIN;//last

	PwrLineFreqItem.Min = CTL_MIN_PU_PWRLINEFRQ;//min
	PwrLineFreqItem.Max = CTL_MAX_PU_PWRLINEFRQ;//max
	PwrLineFreqItem.Res = CTL_RES_PU_PWRLINEFRQ;//res
	PwrLineFreqItem.Def = CTL_DEF_PU_PWRLINEFRQ;//def
	PwrLineFreqItem.Des = CTL_DEF_PU_PWRLINEFRQ;//des
	PwrLineFreqItem.Last = CTL_DEF_PU_PWRLINEFRQ;//last

	// property page camera terminal control configure
	ExposureTimeAbsolutItem.Min = CTL_MIN_CT_EXPOSURE_TIME_ABSOLUTE;//min
	ExposureTimeAbsolutItem.Max = CTL_MAX_CT_EXPOSURE_TIME_ABSOLUTE;//max
	ExposureTimeAbsolutItem.Res = CTL_RES_CT_EXPOSURE_TIME_ABSOLUTE;//res
	ExposureTimeAbsolutItem.Def = CTL_DEF_CT_EXPOSURE_TIME_ABSOLUTE;//def
	ExposureTimeAbsolutItem.Des = CTL_DEF_CT_EXPOSURE_TIME_ABSOLUTE;//des
	ExposureTimeAbsolutItem.Last = CTL_DEF_CT_EXPOSURE_TIME_ABSOLUTE;//last

	ExposureTimeAutoItem.Min = 0;//min
	ExposureTimeAutoItem.Max = 1;//max
	ExposureTimeAutoItem.Res = EXPOSURE_TIME_AUTO_MOD_MANUAL	|EXPOSURE_TIME_AUTO_MOD_APERTPRO;//res
	ExposureTimeAutoItem.Def = EXPOSURE_TIME_AUTO_MOD_APERTPRO;//def
	ExposureTimeAutoItem.Des = EXPOSURE_TIME_AUTO_MOD_APERTPRO;//des
	ExposureTimeAutoItem.Last = EXPOSURE_TIME_AUTO_MOD_APERTPRO;//last

	LowLightCompItem.Min = CTL_MIN_PU_LOWLIGHTCOMP;//min
	LowLightCompItem.Max = CTL_MAX_PU_LOWLIGHTCOMP;//max
	LowLightCompItem.Res = CTL_RES_PU_LOWLIGHTCOMP;//res
	LowLightCompItem.Def = CTL_DEF_PU_LOWLIGHTCOMP;//def
	LowLightCompItem.Des = CTL_DEF_PU_LOWLIGHTCOMP;//des
	LowLightCompItem.Last = CTL_DEF_PU_LOWLIGHTCOMP;//last

	PanItem.Min = CTL_MIN_PU_PAN;//min
	PanItem.Max = CTL_MAX_PU_PAN;//max
	PanItem.Res = CTL_RES_PU_PAN;//res
	PanItem.Def = CTL_DEF_PU_PAN;//def
	PanItem.Des = CTL_DEF_PU_PAN;//des
	PanItem.Last = CTL_DEF_PU_PAN;//last

	TiltItem.Min = CTL_MIN_PU_TILT;//min
	TiltItem.Max = CTL_MAX_PU_TILT;//max
	TiltItem.Res = CTL_RES_PU_TILT;//res
	TiltItem.Def = CTL_DEF_PU_TILT;//def
	TiltItem.Des = CTL_DEF_PU_TILT;//des
	TiltItem.Last = CTL_DEF_PU_TILT;//last

	ZoomItem.Min = CTL_MIN_PU_ZOOM;//min
	ZoomItem.Max = CTL_MAX_PU_ZOOM;//max
	ZoomItem.Res = CTL_RES_PU_ZOOM;//res
	ZoomItem.Def = CTL_DEF_PU_ZOOM;//def
	ZoomItem.Des = CTL_DEF_PU_ZOOM;//des
	ZoomItem.Last = CTL_DEF_PU_ZOOM;//last

	RollItem.Min = CTL_MIN_PU_ROLL;//min
	RollItem.Max = CTL_MAX_PU_ROLL;//max
	RollItem.Res = CTL_RES_PU_ROLL;//res
	RollItem.Def = CTL_DEF_PU_ROLL;//def
	RollItem.Des = CTL_DEF_PU_ROLL;//des
	RollItem.Last = CTL_DEF_PU_ROLL;//last
#ifdef _AF_ENABLE_
	FocusAbsolutItem.Min = CTL_MIN_CT_FOCUS_ABSOLUTE;//min
	FocusAbsolutItem.Max = CTL_MAX_CT_FOCUS_ABSOLUTE;//max
	FocusAbsolutItem.Res = CTL_RES_CT_FOCUS_ABSOLUTE;//res
	FocusAbsolutItem.Def = CTL_DEF_CT_FOCUS_ABSOLUTE;//def
	FocusAbsolutItem.Des = CTL_DEF_CT_FOCUS_ABSOLUTE;//des
	FocusAbsolutItem.Last = CTL_DEF_CT_FOCUS_ABSOLUTE;//last

	FocusAutoItem.Min = CTL_MIN_CT_FOCUS_AUTO;//min
	FocusAutoItem.Max = CTL_MAX_CT_FOCUS_AUTO;//max
	FocusAutoItem.Res = CTL_RES_CT_FOCUS_AUTO;//res
	FocusAutoItem.Def = CTL_DEF_CT_FOCUS_AUTO;//def
	FocusAutoItem.Des = CTL_DEF_CT_FOCUS_AUTO;//des
	FocusAutoItem.Last = CTL_DEF_CT_FOCUS_AUTO;//last
#endif

	// vendor property page control configure
	TCorrectionItem.Min = CTL_MIN_EXTU_TRAPEZIUMCORRECTION;//min
	TCorrectionItem.Max = CTL_MAX_EXTU_TRAPEZIUMCORRECTION;//max
	TCorrectionItem.Res = CTL_RES_EXTU_TRAPEZIUMCORRECTION;//res
	TCorrectionItem.Def = CTL_DEF_EXTU_TRAPEZIUMCORRECTION;//def
	TCorrectionItem.Des = CTL_DEF_EXTU_TRAPEZIUMCORRECTION;//des
	TCorrectionItem.Last = CTL_DEF_EXTU_TRAPEZIUMCORRECTION;//last

#ifdef _RTK_EXTENDED_CTL_
	RtkExtISPSpecialEffectItem.Min = 0;//min
	RtkExtISPSpecialEffectItem.Max = CTL_MAX_RTKEXT_SPECIALEFFECT;//max
	RtkExtISPSpecialEffectItem.Res = 0;//res
	RtkExtISPSpecialEffectItem.Def = CTL_DEF_RTKEXT_SPECIALEFFECT;//def
	RtkExtISPSpecialEffectItem.Des = CTL_DEF_RTKEXT_SPECIALEFFECT;//des
	RtkExtISPSpecialEffectItem.Last = CTL_DEF_RTKEXT_SPECIALEFFECT;//last

	RtkExtEVCompensationItem.Min = CTL_MIN_RTKEXT_EVCOMP;//min
	RtkExtEVCompensationItem.Max = CTL_MAX_RTKEXT_EVCOMP;//max
	RtkExtEVCompensationItem.Res = CTL_RES_RTKEXT_EVCOMP;//res
	RtkExtEVCompensationItem.Def = CTL_DEF_RTKEXT_EVCOMP;//def
	RtkExtEVCompensationItem.Des = CTL_DEF_RTKEXT_EVCOMP;//des
	RtkExtEVCompensationItem.Last = CTL_DEF_RTKEXT_EVCOMP;//last

	RtkExtROIItem.Max.bmAutoControls = CTL_MAX_RTKEXT_ROI_AUTOFUNCTION;

	RtkExtISOItem.Min = CTL_MIN_RTKEXT_ISO;//min
	RtkExtISOItem.Max = CTL_MAX_RTKEXT_ISO;//max
	RtkExtISOItem.Res = CTL_RES_RTKEXT_ISO;//res
	RtkExtISOItem.Def = CTL_DEF_RTKEXT_ISO;//def
	RtkExtISOItem.Des = CTL_DEF_RTKEXT_ISO;//des
	RtkExtISOItem.Last = CTL_DEF_RTKEXT_ISO;//last
#endif

#ifdef _LENOVO_IDEA_EYE_EXTENDEDUNIT_
	InitIdeaEyeCtl();
#endif
	
	g_bySnrImgDir = 0;
}

void	SetBkBrightness(S8 sbySetValue)
{
#ifdef _LENOVO_JAPAN_PROPERTY_PAGE_
	S8 sbyBright;

	if ((U8)sbySetValue >= BrightnessItem.Def)
	{
		sbyBright = LinearIntp_Word_Signed(BrightnessItem.Def, CTL_DEF_PU_BRIGHTNESS_STANDARD, BrightnessItem.Max, CTL_MAX_PU_BRIGHTNESS_STANDARD, (U8)sbySetValue);
	}	
	else
	{
		sbyBright = LinearIntp_Word_Signed(BrightnessItem.Def, CTL_DEF_PU_BRIGHTNESS_STANDARD, BrightnessItem.Min, CTL_MIN_PU_BRIGHTNESS_STANDARD, (U8)sbySetValue);
	}	
	
	XBYTE[ISP_Y_OFFSET]  = sbyBright;
#else
	XBYTE[ISP_Y_OFFSET] = (U8)sbySetValue;
#endif
}

void	SetBkContrast(U8 bySetValue)
{
#ifdef _LENOVO_JAPAN_PROPERTY_PAGE_
	U8 contrast;
	if (bySetValue >= ContrastItem.Def)
	{
		contrast = LinearIntp_Byte(ContrastItem.Def, CTL_DEF_PU_CONTRAST_STANDARD, ContrastItem.Max, CTL_MAX_PU_CONTRAST_STANDARD, bySetValue);
	}	
	else
	{
		contrast = LinearIntp_Byte(ContrastItem.Def, CTL_DEF_PU_CONTRAST_STANDARD, ContrastItem.Min, CTL_MIN_PU_CONTRAST_STANDARD, bySetValue);
	}	

	if(contrast >= CTL_DEF_PU_CONTRAST_STANDARD)
	{
		XBYTE[ISP_CONTRAST]  = LinearIntp_Byte(CTL_DEF_PU_CONTRAST_STANDARD, g_byContrast_Def, CTL_MAX_PU_CONTRAST_STANDARD, 80, contrast);
	}
	else
	{
		XBYTE[ISP_CONTRAST]  = LinearIntp_Byte(CTL_DEF_PU_CONTRAST_STANDARD, g_byContrast_Def, CTL_MIN_PU_CONTRAST_STANDARD, 16, contrast);
	}
#else
	//heonel 2010-06-22 follow Dell V0.11
	if(bySetValue >= ContrastItem.Def)
	{
		XBYTE[ISP_CONTRAST]  = LinearIntp_Byte(ContrastItem.Def, g_byContrast_Def, ContrastItem.Max, 80, bySetValue);
	}
	else
	{
		XBYTE[ISP_CONTRAST]  = LinearIntp_Byte(ContrastItem.Def, g_byContrast_Def, ContrastItem.Min, 16, bySetValue);
	}
#endif	
}

void	SetBkSaturation(U8 bySetValue)
{
	U8 byTemp;

	//heonel 2010-06-22 follow Dell V0.11
	if(bySetValue >= SaturationItem.Def)
	{
		byTemp  = LinearIntp_Byte(SaturationItem.Def, g_bySaturation_Def, SaturationItem.Max, 255, bySetValue);
	}
	else
	{
		byTemp  = LinearIntp_Byte(SaturationItem.Def, g_bySaturation_Def, SaturationItem.Min, 0, bySetValue);
	}
	XBYTE[ISP_U_GAIN] = byTemp;
	XBYTE[ISP_V_GAIN] = byTemp;
}

void	SetBkHue (S16 swSetValue)
{
	SetBkHueRT(swSetValue);
}

void SetGainParam(U8 byGain)
{
	if(byGain >= GainItem.Def)
	{
		XBYTE[ISP_Y_GAIN] = LinearIntp_Byte_Bound(GainItem.Def, 0x80, GainItem.Max, 0xE0, byGain);
	}
	else
	{
		XBYTE[ISP_Y_GAIN] = LinearIntp_Byte_Bound(GainItem.Min, 0x40, GainItem.Def, 0x80, byGain);
	}
}

#ifdef _LENOVO_JAPAN_PROPERTY_PAGE_
U8 MappingSharpness(U8 bySetValue)
{
	U8 bysharpness;
	if(bySetValue >= SharpnessItem.Def)
	{
		bysharpness  = LinearIntp_Byte(SharpnessItem.Def, CTL_DEF_PU_SHARPNESS_STANDARD, SharpnessItem.Max, CTL_MAX_PU_SHARPNESS_STANDARD, bySetValue);
	}
	else
	{
		bysharpness  = LinearIntp_Byte(SharpnessItem.Def, CTL_DEF_PU_SHARPNESS_STANDARD, SharpnessItem.Min, CTL_MIN_PU_SHARPNESS_STANDARD, bySetValue);
	}	
	if(bySetValue >= CTL_DEF_PU_SHARPNESS_STANDARD)
	{
		bysharpness  = LinearIntp_Byte(CTL_DEF_PU_SHARPNESS_STANDARD, g_bySharpness_Def, CTL_MAX_PU_SHARPNESS_STANDARD, 255, bysharpness);
	}
	else
	{
		bysharpness  = LinearIntp_Byte(CTL_DEF_PU_SHARPNESS_STANDARD, g_bySharpness_Def, CTL_MIN_PU_SHARPNESS_STANDARD, 0, bysharpness);
	}	
	return bysharpness;
}
#endif
void SetGammaParam(U16 wSetValue)		//bydir =0: normarl dir; 1: from lowlux to normal; 2: from noamal to lowlux
{
	U8 i;
	U16 wX1,wY1,wX2, wY2;
	static U8 code gc_byYgammaMax[16]= {0,8,16,45,61,73,84,93,102,118,133,146,159,182,204,224}; // y = x^0.6
	// Y gamma: y=x				 {0,8,16,24,32,40,48,56,64,80,96,112,128,160,192,224};
	static U8 code gc_byYgammaMin[16]= {0,8,16,17,19,22,26,31,36,47,61,76,93,131,175,224};	// y=x^1.6
#ifdef _LENOVO_JAPAN_PROPERTY_PAGE_
	U16 gamma;
	if (wSetValue>=GammaItem.Def)
		gamma = LinearIntp_Word(GammaItem.Def, CTL_DEF_PU_GAMMA_STANDARD, GammaItem.Max, CTL_MAX_PU_GAMMA_STANDARD, wSetValue);
	else
		gamma = LinearIntp_Word(GammaItem.Def, CTL_DEF_PU_GAMMA_STANDARD, GammaItem.Min, CTL_MIN_PU_GAMMA_STANDARD, wSetValue);
	for(i =0; i<16; i++)
	{
		wX1 = CTL_DEF_PU_GAMMA_STANDARD;
		wY1 = g_Ygamma[i];
		if(gamma > wX1)
		{
			wX2 = CTL_MAX_PU_GAMMA_STANDARD;
			wY2 = gc_byYgammaMax[i];
		}
		else if (gamma < wX1)
		{
			wX2 = CTL_MIN_PU_GAMMA_STANDARD;
			wY2 =  gc_byYgammaMin[i];
		}
		else
		{
			wX2 = CTL_MIN_PU_GAMMA_STANDARD;
			wY2 = gc_byYgammaMin[i];
		}
		XBYTE[ISP_YGAMMA_P0+i] =  ClipWord(LinearIntp_Word(wX1, wY1, wX2, wY2, gamma), 0, 255);
	}
#else

	for(i =0; i<16; i++)
	{
		wX1 = GammaItem.Def;
		wY1 = g_Ygamma[i];
		if(wSetValue > wX1)
		{
			wX2 = GammaItem.Max;
			wY2 = gc_byYgammaMax[i];
		}
		else if (wSetValue < wX1)
		{
			wX2 = GammaItem.Min;
			wY2 =  gc_byYgammaMin[i];
		}
		else
		{
			if (GammaItem.Def == GammaItem.Min)
			{
				wX2 = GammaItem.Max;
				wY2 = gc_byYgammaMax[i];
			}
			else
			{
				wX2 = GammaItem.Min;
				wY2 = gc_byYgammaMin[i];
			}
		}

		XBYTE[ISP_YGAMMA_P0+i] =  ClipWord(LinearIntp_Word(wX1, wY1, wX2, wY2, wSetValue), 0, 255);
	}

#endif	
	XBYTE[ISP_YGAMMA_SYNC] = 1;
}

