#ifndef _YACY9A1_H_
#define _YACY9A1_H_

void YACY9A1SetFormatFps(U16 SetFormat, U8 Fps);
void CfgYACY9A1ControlAttr(void);
void SetYACY9A1ImgDir(U8 bySnrImgDir);
void SetYACY9A1IntegrationTime(U16 wEspline);
void SetYACY9A1Exposuretime_Gain(float fExpTime, float fTotalGain);
void InitYACY9A1IspParams(void );
void InitYACY9A1IspParamsByResolution(U16 wCurWidth, U16 wCurHeight);
void YACY9A1_POR(void );
void SetYACY9A1DynamicISP(U8 byAEC_Gain);
void SetYACY9A1DynamicISP_AWB(U16 wColorTempature);
void SetYACY9A1Gain(float fGain);
#endif // _YACY9A1_H_

