#ifndef _OV9724_IQ_H_
#define _OV9724_IQ_H_

IQTABLE_t code ct_IQ_Table=
{
	// IQ Header
	{
		IQ_TABLE_AP_VERSION,	// AP version
		sizeof(IQTABLE_t)+8,
		0x00,	// IQ version High
		0x00,	// IQ version Low
		0xFF,
		0xFF,
		0xFF
	},

	// BLC
	{
		// normal BLC:  offset_R,offsetG1,offsetG2,offsetB
		{0,0,0,0},
		// Low lux BLC:  offset_R,offsetG1,offsetG2,offsetB
		{0,0,0,0},
	},

	// LSC
	{
		// circle LSC
		{
			// circle LSC curve
			{
				// For Largan 9361H
				/*
				{128, 0, 128, 0, 131, 0, 135, 0, 141, 0, 149, 0, 160, 0, 172, 0, 189, 0, 210, 0, 237, 0, 8, 1, 16, 1, 16, 1, 16, 1, 16, 1, 16, 1, 16, 1, 16, 1, 16, 1, 16, 1, 16, 1, 16, 1, 16, 1 },
				{128, 0, 129, 0, 131, 0, 135, 0, 141, 0, 149, 0, 159, 0, 171, 0, 187, 0, 207, 0, 232, 0, 1, 1, 17, 1, 17, 1, 17, 1, 17, 1, 17, 1, 17, 1, 17, 1, 17, 1, 17, 1, 17, 1, 17, 1, 17, 1 },
				{128, 0, 128, 0, 131, 0, 135, 0, 141, 0, 147, 0, 156, 0, 167, 0, 181, 0, 200, 0, 222, 0, 246, 0, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1},
				   */
				//For Largan 9388, Module No.8, Normal Light 90%
				/*
					{128, 0, 129, 0, 132, 0, 136, 0, 143, 0, 151, 0, 161, 0, 175, 0, 193, 0, 215, 0, 241, 0, 18, 1, 56, 1, 56, 1, 56, 1, 56, 1, 56, 1, 56, 1, 56, 1, 56, 1, 56, 1, 56, 1, 56, 1, 56, 1, },
				{128, 0, 128, 0, 131, 0, 136, 0, 142, 0, 150, 0, 160, 0, 173, 0, 190, 0, 211, 0, 235, 0, 8, 1, 43, 1, 43, 1, 43, 1, 43, 1, 43, 1, 43, 1, 43, 1, 43, 1, 43, 1, 43, 1, 43, 1, 43, 1, },
				{128, 0, 129, 0, 131, 0, 135, 0, 140, 0, 147, 0, 157, 0, 168, 0, 184, 0, 204, 0, 227, 0, 254, 0, 32, 1, 32, 1, 32, 1, 32, 1, 32, 1, 32, 1, 32, 1, 32, 1, 32, 1, 32, 1, 32, 1, 32, 1, },
				*/
				//For Largan 9388, Module No.7, Normal Light 90%
				/*
				{128, 0, 129, 0, 131, 0, 135, 0, 141, 0, 150, 0, 161, 0, 176, 0, 195, 0, 219, 0, 246, 0, 22, 1, 22, 1, 22, 1, 22, 1, 22, 1, 22, 1, 22, 1, 22, 1, 22, 1, 22, 1, 22, 1, 22, 1, 22, 1, },
				{128, 0, 129, 0, 131, 0, 135, 0, 141, 0, 149, 0, 160, 0, 173, 0, 190, 0, 211, 0, 234, 0, 7, 1, 7, 1, 7, 1, 7, 1, 7, 1, 7, 1, 7, 1, 7, 1, 7, 1, 7, 1, 7, 1, 7, 1, 7, 1, },
				{128, 0, 128, 0, 131, 0, 135, 0, 140, 0, 147, 0, 157, 0, 168, 0, 184, 0, 203, 0, 224, 0, 251, 0, 251, 0, 251, 0, 251, 0, 251, 0, 251, 0, 251, 0, 251, 0, 251, 0, 251, 0, 251, 0, 251, 0, 251, 0, },
				*/
				//For Largan 9388, Module No. 21
				{128, 0, 129, 0, 132, 0, 137, 0, 145, 0, 155, 0, 169, 0, 186, 0, 208, 0, 234, 0, 7, 1, 38, 1, 80, 1, 80, 1, 80, 1, 80, 1, 80, 1, 80, 1, 80, 1, 80, 1, 80, 1, 80, 1, 80, 1, 80, 1, },
				{128, 0, 128, 0, 130, 0, 134, 0, 139, 0, 147, 0, 156, 0, 167, 0, 181, 0, 199, 0, 218, 0, 241, 0, 16, 1, 16, 1, 16, 1, 16, 1, 16, 1, 16, 1, 16, 1, 16, 1, 16, 1, 16, 1, 16, 1, 16, 1, },
				{128, 0, 128, 0, 130, 0, 133, 0, 138, 0, 144, 0, 153, 0, 163, 0, 176, 0, 191, 0, 209, 0, 231, 0, 5, 1, 5, 1, 5, 1, 5, 1, 5, 1, 5, 1, 5, 1, 5, 1, 5, 1, 5, 1, 5, 1, 5, 1, },

			},
			// circle LSC center: R Center Hortizontal, R Center Vertical, G Center Hortizontal, G Center Vertical,B Center Hortizontal, B Center Vertical
			//{628,368,628,368,628,368},//For Module No.8
			//{640,416,640,416,640,416},//For Module No.7
			{604,394,604,394,604,394}, //For Module No.21
		},

		// micro LSC
		{
			// micro LSC grid mode
			2,
			// micro LSC matrix
			{0},
		},

		// dynamic LSC
		{
			80,   // Dynamic LSC Gain Threshold Low
			144,  // Dynamic LSC Gain Threshold High
			0x20,	// Dynamic LSC Adjust rate at Gain Threshold Low
			0x10,	// Dynamic LSC Adjust rate at Gain Threshold High
			{33,35,38,54,56,34},	// rough r gain before LSC, from A, U30, CWF, D50, D65, D75
			{78,70,61,44,38,72},	// rough b gain before LSC
			50,		// start threshold of dynamic LSC by CT, white pixel millesimal
			25,		// end threshold of dynamic LSC by CT, white pixel millesimal
			100,		// LSC switch color temperature threshold buffer
			{3100,3800,4700,5800,7000},	// LSC switch color temperature threshold
			{
				{0x20,0x20,0x20,},//a=2850k
				{0x20,0x20,0x20,},//3500k
				{0x1A,0x20,0x20,},//cwf=4150k
				{0x20,0x20,0x20,},//d50=5000k
				{0x20,0x20,0x20,},//d65=6500k
				{0x20,0x20,0x20,},//d75=6500k
			},
		},
	},

	// CCM
	{
		// D65 light CCM
		{0x163,-80,-19,-59,0x11d,30,2,-112,0x16e},	//For MSOC test, pass premium
		// A light CCM
		{0x199,-116,-37,-80,0x18a,-56,-9,-91,0x164},	//For MSOC test, pass premium
		// low lux CCM
		{0x100, 0, 0, 0, 0x100, 0, 0, 0, 0x100},
		80,	// dynamic CCM Gain Threshold Low
		144,	// dynamic CCM Gain Threshold High
		3000,	// dynamic CCM A light Color Temperature Switch Threshold
		3600	// dynamic CCM D65 light Color Temperature Switch Threshold
	},

	// Gamma
	{
		// normal light gamma
		//{0, 10, 16, 23, 30, 37 , 44, 52, 59, 75, 91, 106, 119, 130, 139, 147, 154, 161, 167, 173, 179, 190, 200, 210, 221, 230, 239, 247}, //[Albert, 2011/06/23], Demo @ Chicony and LiteOn
		//{0, 6, 13, 21, 30, 39, 48, 57, 66, 82, 96, 109, 120, 130, 138, 146, 153, 160, 166, 172, 178, 189, 199, 209, 220, 229, 238, 246}, //[Albert, 2011/06/24], For MSOC Test
		//{0, 4, 13, 24, 35, 45, 55, 65, 75, 91, 103, 113, 123, 132, 140, 147, 155, 162, 169, 175, 181, 193, 203, 213, 223, 232, 242, 248}, //Neil 0629 chicony tuning
		//{0, 5, 13, 24, 33, 42, 51, 59, 67, 82, 93, 103, 112, 120, 127, 134, 142, 149, 156, 162, 169, 182, 193, 205, 216, 226, 237, 246, },
		//xiaohou tuned
		//{0, 8, 16, 24, 32, 40, 48, 56, 64, 80, 96, 110, 124, 136, 147, 156, 163, 170, 176, 182, 187, 197, 207, 215, 223, 231, 239, 247, },
		{0, 5, 12, 19, 27, 38, 50, 61, 70, 87, 101, 115, 128, 140, 151, 158, 164, 170, 177, 183, 189, 197, 207, 215, 223, 231, 239, 247, },
		// low light gamma
		//{0, 11, 19, 25, 31, 37, 43, 48, 53, 63, 72, 81, 90, 98, 106, 114, 122, 130, 137, 144, 152, 166, 179, 193, 206, 218, 231, 243}, //[Albert, 2011/06/09]
		{0, 8, 16, 24, 32, 40, 48, 54, 60, 70, 80, 92, 105, 117, 126, 135, 144, 152, 159, 166, 174, 186, 198, 210, 220, 230, 239, 247, },
		80,	// dynamic Gamma Gain Threshold Low
		255	// dynamic Gamma Gain Threshold High
	},

	// AE
	{
		// AE target
		{
			40,// Histogram Ratio Low
			40,// Histogram Ratio High
			60,// YMean Target Low
			66,// YMean Target
			80,// YMean Target High
			10,// Histogram Position Low
			190,// Histogram Position High
			3// Dynamic AE Target decrease value
		},
		// AE limit
		{
			13,	// AE step Max value at 50Hz power line frequency
			16,	// AE step Max value at 60Hz power line frequency
			255,	// AE global gain Max value
			96,	// AE continous frame rate gain threshold
			220,	// AE discrete frame rate 15fps gain threshold
			168,	// AE discrete frame rate 30fps gain threshold
			120	// AE HighLight mode threshold
		},
		// AE weight
		{
			{
				4,4,4,4,4,
				5,5,5,5,5,
				6,6,7,6,6,
				6,6,7,6,6,
				5,6,5,6,5,
			}
		},
		// AE sensitivity
		{
			0.05,// g_fAEC_Adjust_Th
			16,	// AE Latency time
			//20,	// Ymean diff threshold for judge AE same block
			8,	// hemonel 2011-07-14: modify Neil adjust parameter from 20 to 8
			22	// same block count threshold for judge AE scene variation
		},
	},

	// AWB
	{
		{
			// AWB simple
			{33,35,38,54,56,34},	// g_aAWBRoughGain_R
			{78,70,61,44,38,72},// g_aAWBRoughGain_B
			9,	// K1
			90,	// B1
			66,	// B2
			10,	// sK3
			43,	// sB3
			16,	// sK4
			-49,	// sB4
			50,	// sK5
			-50,	// sB5
			2,	// sK6
			21,	// sB6
			100,	// B_up
			55,	// B_down
			55,	// sB_left
			-59,	// sB_right
			230,	// Ymean range upper limit
			0,	// Ymean range low limit
			500,	// RGB sum threshold for AWB hold
		},
		// AWB advanced
		{
			5,	// white point ratio threshold
			38,	// g_byAWBFineMax_RG
			26,	// g_byAWBFineMin_RG
			38,	// g_byAWBFineMax_BG
			26,	// g_byAWBFineMin_BG
			225,	// g_byAWBFineMax_Bright
			20,	// g_byAWBFineMin_Bright
			16// g_byFtGainTh
		},
		// AWB sensitivity
		{
			4, // g_wAWBGainDiffTh
			2, // g_byAWBGainStep
			10,	// g_byAWBFixed_YmeanTh
			7,	// g_byAWBColorDiff_Th
			12	// g_byAWBDiffWindowsTh
		}
	},

	// Texture
	{
		// sharpness
		{
			0x60,	// for CIF
			0x60,	// for VGA
			0x60,	// for HD
			0x20,	// for low lux
			64,		// dynamic sharpness gain threshold low
			150		// dynamic sharpness gain threshold high
		},
		// sharpness & denoise paramter
		{
			// CIF
			{{
					0x06,  	//ISP_NR_EDGE_THD,
					0x0a,	//ISP_NR_MM_THD1
					0x01, 	//ISP_NR_MODE0_LPF,
					0x02, 	//ISP_NR_MODE1_LPF,
					0x57,	//ISP_NR_MODE,
					0x01,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x02,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x02,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x81,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x08,	//ISP_INTP_EDGE_THD1,
					0x15,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x07,	//ISP_RGB_IIR_CTRL
					0x60,	//ISP_RGB_VIIR_COEF
					0X66,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x0d,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x01, 	//ISP_RGB_DIIR_COFF_CUT
					0x06,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG


				},
				{
					0x0c,  	//ISP_NR_EDGE_THD,
					0x06,	//ISP_NR_MM_THD1
					0x01, 	//ISP_NR_MODE0_LPF,
					0x02, 	//ISP_NR_MODE1_LPF,
					0x7f,	//ISP_NR_MODE,
					0x01,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x02,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x02,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x81,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x08,	//ISP_INTP_EDGE_THD1,
					0x15,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x07,	//ISP_RGB_IIR_CTRL
					0x60,	//ISP_RGB_VIIR_COEF
					0x88,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x0e,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x01, 	//ISP_RGB_DIIR_COFF_CUT
					0x0a,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG

				},
				{
					0x10,  	//ISP_NR_EDGE_THD,
					0x02,	//ISP_NR_MM_THD1
					0x00, 	//ISP_NR_MODE0_LPF,
					0x01, 	//ISP_NR_MODE1_LPF,
					0xff,	//ISP_NR_MODE,
					0x01,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x03,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x03,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x81,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x0a,	//ISP_INTP_EDGE_THD1,
					0x15,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x07,	//ISP_RGB_IIR_CTRL
					0x80,	//ISP_RGB_VIIR_COEF
					0xaa,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x0e,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x02, 	//ISP_RGB_DIIR_COFF_CUT
					0x0b,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG


				},
				{
					0x10,  	//ISP_NR_EDGE_THD,
					0x00,	//ISP_NR_MM_THD1
					0x00, 	//ISP_NR_MODE0_LPF,
					0x01, 	//ISP_NR_MODE1_LPF,
					0xff,	//ISP_NR_MODE,
					0x01,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x03,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x03,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x81,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x0c,	//ISP_INTP_EDGE_THD1,
					0x15,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x07,	//ISP_RGB_IIR_CTRL
					0xa0,	//ISP_RGB_VIIR_COEF
					0xcc,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x0e,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x03, 	//ISP_RGB_DIIR_COFF_CUT
					0x0c,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG


				}
			},
			// VGA resolution
			{{
					0x06,  	//ISP_NR_EDGE_THD,
					0x0a,	//ISP_NR_MM_THD1
					0x01, 	//ISP_NR_MODE0_LPF,
					0x02, 	//ISP_NR_MODE1_LPF,
					0x57,	//ISP_NR_MODE,
					0x01,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x02,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x02,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x81,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x08,	//ISP_INTP_EDGE_THD1,
					0x15,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x07,	//ISP_RGB_IIR_CTRL
					0x60,	//ISP_RGB_VIIR_COEF
					0X66,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x0d,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x01, 	//ISP_RGB_DIIR_COFF_CUT
					0x06,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG

				},
				{
					0x0c,  	//ISP_NR_EDGE_THD,
					0x06,	//ISP_NR_MM_THD1
					0x01, 	//ISP_NR_MODE0_LPF,
					0x02, 	//ISP_NR_MODE1_LPF,
					0x7f,	//ISP_NR_MODE,
					0x01,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x02,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x02,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x81,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x08,	//ISP_INTP_EDGE_THD1,
					0x15,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x07,	//ISP_RGB_IIR_CTRL
					0x60,	//ISP_RGB_VIIR_COEF
					0x88,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x0e,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x01, 	//ISP_RGB_DIIR_COFF_CUT
					0x0a,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG


				},
				{
					0x10,  	//ISP_NR_EDGE_THD,
					0x02,	//ISP_NR_MM_THD1
					0x00, 	//ISP_NR_MODE0_LPF,
					0x01, 	//ISP_NR_MODE1_LPF,
					0xff,	//ISP_NR_MODE,
					0x01,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x03,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x03,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x81,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x0a,	//ISP_INTP_EDGE_THD1,
					0x15,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x07,	//ISP_RGB_IIR_CTRL
					0x80,	//ISP_RGB_VIIR_COEF
					0xaa,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x0e,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x02, 	//ISP_RGB_DIIR_COFF_CUT
					0x0b,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG

				},
				{
					0x10,  	//ISP_NR_EDGE_THD,
					0x00,	//ISP_NR_MM_THD1
					0x00, 	//ISP_NR_MODE0_LPF,
					0x01, 	//ISP_NR_MODE1_LPF,
					0xff,	//ISP_NR_MODE,
					0x01,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x03,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x03,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x81,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x0c,	//ISP_INTP_EDGE_THD1,
					0x15,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x07,	//ISP_RGB_IIR_CTRL
					0xa0,	//ISP_RGB_VIIR_COEF
					0xcc,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x0e,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x03, 	//ISP_RGB_DIIR_COFF_CUT
					0x0c,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG

				}
			},
			// HD resolution
			//[Albert, 2011/06/16]+++
			{{
					0x06,  	//ISP_NR_EDGE_THD,
					0x0a,	//ISP_NR_MM_THD1
					0x01, 	//ISP_NR_MODE0_LPF,
					0x02, 	//ISP_NR_MODE1_LPF,
					0x57,	//ISP_NR_MODE,
					0x01,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x02,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x02,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x82,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x08,	//ISP_INTP_EDGE_THD1,
					0x15,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x07,	//ISP_RGB_IIR_CTRL
					0x60,	//ISP_RGB_VIIR_COEF
					0X66,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x0d,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x01, 	//ISP_RGB_DIIR_COFF_CUT
					0x06,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG
				},
				{
					0x0c,  	//ISP_NR_EDGE_THD,
					0x06,	//ISP_NR_MM_THD1
					0x01, 	//ISP_NR_MODE0_LPF,
					0x02, 	//ISP_NR_MODE1_LPF,
					0x7f,	//ISP_NR_MODE,
					0x01,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x02,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x02,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x82,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x08,	//ISP_INTP_EDGE_THD1,
					0x15,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x07,	//ISP_RGB_IIR_CTRL
					0x60,	//ISP_RGB_VIIR_COEF
					0x88,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x0e,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x01, 	//ISP_RGB_DIIR_COFF_CUT
					0x0a,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG

				},
				{
					0x10,  	//ISP_NR_EDGE_THD,
					0x02,	//ISP_NR_MM_THD1
					0x00, 	//ISP_NR_MODE0_LPF,
					0x01, 	//ISP_NR_MODE1_LPF,
					0xff,	//ISP_NR_MODE,
					0x01,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x03,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x03,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x82,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x0a,	//ISP_INTP_EDGE_THD1,
					0x15,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x07,	//ISP_RGB_IIR_CTRL
					0x80,	//ISP_RGB_VIIR_COEF
					0xaa,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x0e,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x02, 	//ISP_RGB_DIIR_COFF_CUT
					0x0b,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG

				},
				{
					0x10,  	//ISP_NR_EDGE_THD,
					0x00,	//ISP_NR_MM_THD1
					0x00, 	//ISP_NR_MODE0_LPF,
					0x01, 	//ISP_NR_MODE1_LPF,
					0xff,	//ISP_NR_MODE,
					0x01,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x03,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x03,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x82,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x0c,	//ISP_INTP_EDGE_THD1,
					0x15,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x07,	//ISP_RGB_IIR_CTRL
					0xa0,	//ISP_RGB_VIIR_COEF
					0xcc,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x0e,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x03, 	//ISP_RGB_DIIR_COFF_CUT
					0x0c,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG

				}
			},
			48,		// dynamic denoise gain threshold 0
			80,		// dynamic denoise gain threshold 1
			112		// dynamic denoise gain threshold 2
		}
	},
	// UV offset
	{
		0,		// A light U offset
		0,		// A light V offset
		0,//0x24,		// D65 light U offset
		0,//0x22,		// D65 light V offset
		3300,	// Dynamic UV offset threshold for A light
		4000	// Dynamic UV offset threshold for D65 light
	},

	// gamma 2
	{
		// normal lux
		{
			// R - G diff gamma
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			// B - G diff gamma
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		},

		// low lux
		{
			// R - G diff gamma
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			// B - G diff gamma
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		},
	},

	// Texture 2
	{
		// corner denoise
		{
			0x100,	//d0(start) for RGB domain
			0x300,	//d1(end) for RGB domain
			0x100,	//d0(start) for YUV domain
			0x300,	//d1(end) for YUV domain
			{
				{
					0x10,	//ISP_RDLOC_MAX
					0x41,	//ISP_RDLOC_RATE
					0x10,	//ISP_GLOC_MAX
					0x08,	//ISP_GLOC_RATE
					0x41,	//ISP_ILOC_MAX
					0x08,	//ISP_ILOC_RATE
					0x04,	//ISP_LOC_MAX
					0x41,	//ISP_LOC_RATE
				},
				{
					0x10,	//ISP_RDLOC_MAX
					0x41,	//ISP_RDLOC_RATE
					0x10,	//ISP_GLOC_MAX
					0x41,	//ISP_GLOC_RATE
					0x02,	//ISP_ILOC_MAX
					0x08,	//ISP_ILOC_RATE
					0x4,	//ISP_LOC_MAX
					0x41,	//ISP_LOC_RATE
				},
				{
					0x10,	//ISP_RDLOC_MAX
					0x41,	//ISP_RDLOC_RATE
					0x10,	//ISP_GLOC_MAX
					0x41,	//ISP_GLOC_RATE
					0x02,	//ISP_ILOC_MAX
					0x08,	//ISP_ILOC_RATE
					0x4,	//ISP_LOC_MAX
					0x41,	//ISP_LOC_RATE
				},
				{
					0x10,	//ISP_RDLOC_MAX
					0x41,	//ISP_RDLOC_RATE
					0x10,	//ISP_GLOC_MAX
					0x41,	//ISP_GLOC_RATE
					0x02,	//ISP_ILOC_MAX
					0x08,	//ISP_ILOC_RATE
					0x4,	//ISP_LOC_MAX
					0x41,	//ISP_LOC_RATE
				},
			},
		},
		// uv denoise
		{
			{
				{
					0x8,   //ISP_EEH_UVIIR_Y_CUTS
					4,   //ISP_EEH_UVIIR_Y_CMIN
					0xc,	 //ISP_EEH_IIR_COEF
					2,	 //ISP_EEH_IIR_CUTS
					2,  //ISP_EEh_IIR_STEP
				},
				{
					0x8,   //ISP_EEH_UVIIR_Y_CUTS
					4,   //ISP_EEH_UVIIR_Y_CMIN
					0xc,	 //ISP_EEH_IIR_COEF
					2,	 //ISP_EEH_IIR_CUTS
					2,  //ISP_EEh_IIR_STEP
				},
				{
					0x8,   //ISP_EEH_UVIIR_Y_CUTS
					4,   //ISP_EEH_UVIIR_Y_CMIN
					0xf,	 //ISP_EEH_IIR_COEF
					3,	 //ISP_EEH_IIR_CUTS
					2, //ISP_EEh_IIR_STEP
				},
				{
					0x8,   //ISP_EEH_UVIIR_Y_CUTS
					4,   //ISP_EEH_UVIIR_Y_CMIN
					0xf,	 //ISP_EEH_IIR_COEF
					4,	 //ISP_EEH_IIR_CUTS
					2, //ISP_EEh_IIR_STEP
				},
			},
		},
		// noise reduction additional
		{
			{0x1E,0x1E,0x1E,0x1E,},	//ISP_EEH_CRC_RATE
			{1,1,1,1,},	//ISP_RD_MM2_RATE
			{
				{
					0x02,	//ISP_NR_MMM_D0
					0x22,	//ISP_NR_MMM_D1
					0x01,	//ISP_NR_MMM_RATE
					0x04,	//ISP_NR_MMM_MIN
					0x24,	//ISP_NR_MMM_MAX
				},
				{
					0x02,	//ISP_NR_MMM_D0
					0x22,	//ISP_NR_MMM_D1
					0x01,	//ISP_NR_MMM_RATE
					0x04,	//ISP_NR_MMM_MIN
					0x24,	//ISP_NR_MMM_MAX
				},
				{
					0x02,	//ISP_NR_MMM_D0
					0x22,	//ISP_NR_MMM_D1
					0x01,	//ISP_NR_MMM_RATE
					0x04,	//ISP_NR_MMM_MIN
					0x24,	//ISP_NR_MMM_MAX
				},
				{
					0x02,	//ISP_NR_MMM_D0
					0x22,	//ISP_NR_MMM_D1
					0x01,	//ISP_NR_MMM_RATE
					0x04,	//ISP_NR_MMM_MIN
					0x24,	//ISP_NR_MMM_MAX
				},
			},
		},
	},

	// Edge enhance
	{
		0x01, 			//ISP_BRIGHT_RATE
		0x02, 			//ISP_BRIGHT_TRM_B1
		0x10, 			//ISP_BRIGHT_TRM_B2
		0x0e, 			//ISP_BRIGHT_TRM_K
		0x03, 			//ISP_BRIGHT_TRM_THD0
		0x12, 			//ISP_BRIGHT_TRM_THD1
		0x01, 			//ISP_DARK_RATE
		0x02, 			//ISP_DARK_TRM_B1
		0x10, 			//ISP_DARK_TRM_B2
		0x0e, 			//ISP_DARK_TRM_K
		0x03, 			//ISP_DARK_TRM_THD0
		0x12, 			//ISP_DARK_TRM_THD1
		20,  				//ISP_EDG_DIFF_C0
		3, 			//ISP_EDG_DIFF_C1
		-1, 			//ISP_EDG_DIFF_C2
		-3, 	    		//ISP_EDG_DIFF_C3
		-2, 			//ISP_EDG_DIFF_C4
		0x08, 			//ISP_EEH_SHARP_ARRAY 0
		0x0C, 			//ISP_EEH_SHARP_ARRAY10
		0x10, 			//ISP_EEH_SHARP_ARRAY11
		0x0C, 			//ISP_EEH_SHARP_ARRAY1
		0x10, 			//ISP_EEH_SHARP_ARRAY2
		0x0C, 			//ISP_EEH_SHARP_ARRAY3
		0x18, 			//ISP_EEH_SHARP_ARRAY4
		0x18, 			//ISP_EEH_SHARP_ARRAY5
		0x10, 			//ISP_EEH_SHARP_ARRAY6
		0x10, 			//ISP_EEH_SHARP_ARRAY7
		0x10, 			//ISP_EEH_SHARP_ARRAY8
		0x08, 			//ISP_EEH_SHARP_ARRAY9

	},

	// flase color
	{
		{
			{
				0x02, 			//ISP_FCRD_MIN
				0x12, 			//ISP_FCRD_YCUTS
				0x8c, 			//ISP_FCRD_CFG
				0x1f, 			//ISP_FCRD_MAX
				0x20,//ISP_MOIRE_RATE
				0x02,//MOIRE detect threshold
			},
			{
				0x02, 			//ISP_FCRD_MIN
				0x12, 			//ISP_FCRD_YCUTS
				0x8e, 			//ISP_FCRD_CFG
				0x1f, 			//ISP_FCRD_MAX
				0x20,//ISP_MOIRE_RATE
				0x02,//MOIRE detect threshold
			},
			{
				0x02, 			//ISP_FCRD_MIN
				0x12, 			//ISP_FCRD_YCUTS
				0x8f, 			//ISP_FCRD_CFG
				0x1f, 			//ISP_FCRD_MAX
				0x20,//ISP_MOIRE_RATE
				0x02,//MOIRE detect threshold
			},
			{
				0x02, 			//ISP_FCRD_MIN
				0x12, 			//ISP_FCRD_YCUTS
				0x8f, 			//ISP_FCRD_CFG
				0x1f, 			//ISP_FCRD_MAX
				0x20,//ISP_MOIRE_RATE
				0x02,//MOIRE detect threshold
			},
		}

	},

	// dead pixel cancel
	{
		0x01,	//  ISP_DDP_CTRL
		0x00, 	// ISP_DDP_SEL
		0x04,	//	ISP_DP_THD_D1
		0x02, 	//	ISP_DP_BRIGHT_THD_MIN
		0x0A,	//  ISP_DP_BRIGHT_THD_MAX
		0x03, 	//  ISP_DP_DARK_THD_MIN
		0x0c,	//  ISP_DP_DARK_THD_MAX
		0x02,	//  ISP_DDP_BRIGHT_RATE
		0x02,	//  ISP_DDP_DARK_RATE
	},

	// UV color tune
	{
		//A light UV color tune
		{
			0, //ISP_UVT_UCENTER
			0, //ISP_UVT_VCENTER
			0, //ISP_UVT_UINC
			0, //ISP_UVT_VINC
		},
		//D65 light UV color tune
		{
			0,  //ISP_UVT_UCENTER
			0,    //ISP_UVT_VINC
			0,   //ISP_UVT_UINC
			0,   //ISP_UVT_VINC
		},
		3300, // Dynamic UV color tune threshold for A light
		4000,  // Dynamic UV color tune threshold for D65 light
	},
	// NLC
	{
		// G NLC
		{0, 16, 32, 48, 64, 80, 96, 112, 128, 144, 160, 176, 192, 208, 224, 240},
		// R - G diff
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		// B - G diff
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
	},

	// HDR
	{
		// HDR threshold
		{
			10,		// Histogram dark pixel threshold for start HDR function
			10,     // Histogram bright pixel threshold for start HDR function
			60, 	// Histogram dark pixel max number
			64,		// Hdr tune max value

		},

		// FW HDR
		{
			// HDR gamma
			{0,23,43,58,73,81,88,94,99,109,117,125,132,139,145,151,157,163,168,173,177,184,190,195,201,210,222,237},
		},

		// HW HDR
		{
			// tgamma threshold
			0x8,
			// tgamma rate
			0x10,
			// HDR LPF COEF
			{0, 1, 3, 5, 6, 6, 5, 4, 2},
			// HDR halo thd
			0x10,
			// HDR curver
			{115, 115, 114, 112, 111, 109, 108, 106, 104, 100, 96, 91, 85, 79, 72, 64, 54, 45, 35, 26, 16, 10, 6, 3},
			// HDR max curver
			{192, 144, 112, 80, 61, 52, 42, 36, 32},
			//HDR step
			0x4,
			//local constrast curver
			{24, 26, 26, 24, 20, 20, 20,20, 20, 20, 20, 20, 20, 23, 26, 26},
			//local constrast rate min
			0xd,
			//local constrast rate max
			0xd,
			//local constrast step
			0x10
		},
	}
};
#endif // _OV9724_IQ_H_
