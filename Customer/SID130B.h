#ifndef _SID130B_H_
#define _SID130B_H_

#define SID130B_AE_TARGET	0x78

void SID130BSetFormatFps(U8 SetFormat, U8 Fps);
void CfgSID130BControlAttr(void);
void	SetSID130BSharpness(U8 bySetValue);
void SetSID130BImgDir(U8 bySnrImgDir);
void SetSID130BOutputDim(U16 wWidth,U16 wHeight);
void SetSID130BPwrLineFreq(U8 byFPS, U8 byLightFrq);
void SetSID130BWBTemp(U16 wSetValue);
//OV_CTT_t GetSIV120BAwbGain(void);
void SetSID130BWBTempAuto(U8 bySetValue);
void SetSID130BBackLightComp(U8 bySetValue);
extern code OV_CTT_t gc_SID130B_CTT[3];
void SetSID130BIntegrationTimeAuto(U8 bySetValue);
void SetSID130BIntegrationTime(U16 wEspline);
void SetSID130BGain(float fGain);
#endif // _OV7670_H_

