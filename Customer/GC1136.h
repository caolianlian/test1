#ifndef _GC1136_H_
#define _GC1136_H_

void GC1136SetFormatFps(U16 SetFormat, U8 Fps);
void CfgGC1136ControlAttr(void);
void SetGC1136ImgDir(U8 bySnrImgDir);
void SetGC1136IntegrationTime(U16 wEspline);
void SetGC1136Exposuretime_Gain(float fExpTime, float fTotalGain);
void InitGC1136IspParams(void );
void InitGC1136IspParamsByResolution(U16 wCurWidth, U16 wCurHeight);
void GC1136_POR(void );
void SetGC1136DynamicISP(U8 byAEC_Gain);
void SetGC1136DynamicISP_AWB(U16 wColorTempature);
#endif // _OV9710_H_

