#ifndef _OV9724_H_
#define _OV9724_H_

void OV9724SetFormatFps(U16 SetFormat, U8 Fps);
void CfgOV9724ControlAttr(void);
void SetOV9724ImgDir(U8 bySnrImgDir);
void SetOV9724IntegrationTime(U16 wEspline);
void SetOV9724Exposuretime_Gain(float fExpTime, float fTotalGain);
void InitOV9724IspParams(void);
void InitOV9724IspParamsByResolution(U16 wCurWidth, U16 wCurHeight);
void OV9724_POR(void);
void SetOV9724DynamicISP(U8 byAEC_Gain);
void SetOV9724DynamicISP_AWB(U16 wColorTempature);
void SetOV9724Gain(float fGain);
#endif // _OV9724_H_ 

