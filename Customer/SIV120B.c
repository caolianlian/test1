#include "Inc.h"

#ifdef RTS58XX_SP_SIV120B


code OV_CTT_t gc_SIV120B_CTT[3] =
{
	{2800,0x7E,0x80,0xC8},
	{4050,0xAA,0x80,0xB1},
	{6500,0xC7,0x80,0x82},
};

// hemonel 2009-03-17: copy SIV120B fps
code OV_FpsSetting_t  g_staSIV120BFpsSetting[]=
{
	//  FPS	extradummypixel clkrc	pclk = 24M/2^(clkrc+1)
	//	{1,		(1176+1),	(23),	  1000000},	// 1M
	{3,		(1000),		(3),	  	 1500000},	// 2M	// 2.998
	{5, 		(1200),		(2),	  	 3000000},	// 2M
	//	{8,  		(1500),		(1),		 6000000},	// 4M	// 7.995
	{9,  		(1333),		(1),		 6000000},	// 4M	// 8.995
	{10,		(1200),		(1),		 6000000},	// 4M
	{15,		(800),		(1),		 6000000},	// 6M
	{20,		(1200),		(0),		12000000},	// 8M
	{25,		(960),		(0),		12000000},	// 12M	// hemonel 2009-04-20: fix scale down abnormal to change 157 to 156
	{30,		(800),		(0),		12000000},	// 12M
};



t_RegSettingWB code gc_SIV120B_Setting[] =
{
	//====================================================
	//SIV120B INITIAL SETTING
	///ChipID	SIV120B	# Don't delete or edit this line !!!
	//# This file is for "Internal V1.3". ================
	//==================================================
	//[Addr]	[Data]

	//{0x00,	0x00}, //
	{0x004,	0x00}, //	PCLK divider = 1	// 0x04, PCLK divider = 2
	{0x013,	0x17}, //	#anti_black sun
	{0x016,	0xcf}, //	#0xb6

	{0x020,	0x00}, //	#P_BNKT
	{0x021,	0x01}, //	#P_HBNKT
	{0x022,	0x01}, //	#P_ROWFIL
	{0x023,	0x01}, //	#P_VBNKT

	{0x040,	0x00}, //	#0x8b
	{0x041,	0x00}, //  	#0x96
	{0x042,	0x00}, //  	#0xda
	{0x043,	0x00}, //	#0x83

	//# AE Register setting
	//{0x00,	0x01}, //
	{0x111,	0x14}, //  	# 6fps at lowlux
	{0x112,	SIV120B_AE_TARGET}, //0x78 //# D65 target 0x74
	{0x113,	SIV120B_AE_TARGET}, //0x78 //	# CWF target 0x74
	{0x114,	SIV120B_AE_TARGET}, //0x78 //	# A target   0x74
	{0x134,	0x7d}, //

	{0x140,	0x50}, //	# Max x8

	{0x141,	0x20}, //	#AG_TOP1	0x28
	{0x142,	0x20}, //	#AG_TOP0	0x28
	{0x143,	0x00}, //	#AG_MIN1	0x08
	{0x144,	0x00}, //	#AG_MIN0	0x08
	{0x145,	0x00}, //	#G50_dec	0x09
	{0x146,	0x0a}, //	#G33_dec	0x17
	{0x147,	0x10}, //	#G25_dec	0x1d
	{0x148,	0x13}, //	#G20_dec	0x21
	{0x149,	0x15}, //	#G12_dec	0x23
	{0x14a,	0x18}, //	#G09_dec	0x24
	{0x14b,	0x1a}, //	#G06_dec	0x26
	{0x14c,	0x1d}, //	#G03_dec	0x27
	{0x14d,	0x20}, //	#G100_inc	0x27
	{0x14e,	0x10}, //	#G50_inc	0x1a
	{0x14f,	0x0a}, //	#G33_inc	0x14
	{0x150,	0x08}, //	#G25_inc	0x11
	{0x151,	0x06}, //	#G20_inc	0x0f
	{0x152,	0x05}, //	#G12_inc	0x0d
	{0x153,	0x04}, //	#G09_inc	0x0c
	{0x154,	0x02}, //	#G06_inc	0x0a
	{0x155,	0x01}, //	#G03_inc	0x09

	//# AWB Register Setting
	//{0x00,  0x02}, //
	{0x210,  0xd3}, //
	{0x211,  0xc0}, //
	{0x212,  0x80}, //
	{0x213,  0x80}, //
	{0x214,  0x80}, //
	{0x215,  0xfe}, //		# R gain Top
	{0x216,  0x70}, //	# R gain bottom 0x80
	{0x217,  0xea}, //	# B gain Top
	{0x218,  0x80}, //	# B gain bottom
	{0x219,  0xa0}, //	# Cr top value
	{0x21a,  0x60}, //	# Cr bottom value
	{0x21b,  0xa0}, //	# Cb top value
	{0x21c,  0x60}, //		# Cb bottom value
	{0x21d,  0xa0}, //
	{0x21e,  0x70}, //
	{0x220,  0xe8}, //	# AWB luminous top value
	{0x221,  0x20}, //	# AWB luminous bottom value
	{0x222,  0xa4}, //
	{0x223,  0x20}, //
	{0x225,  0x20}, //
	{0x226,  0x0f}, //
	{0x227,  0x10}, //	# ST for outdoor enable
	{0x228,  0x1a}, //	# ST for outdoor disable
	{0x229,  0xb8}, //	# AWB R gain at outdoor
	{0x22a,  0x94}, //	# AWB B gain at outdoor

	{0x230,  0x00}, //
	{0x231,  0x10}, //
	{0x232,  0x00}, //
	{0x233,  0x10}, //
	{0x234,  0x02}, //
	{0x235,  0x76}, //
	{0x236,  0x01}, //
	{0x237,  0xd6}, //
	{0x240,  0x01}, //
	{0x241,  0x04}, //
	{0x242,  0x08}, //
	{0x243,  0x10}, //
	{0x244,  0x12}, //
	{0x245,  0x35}, //
	{0x246,  0x64}, //
	{0x250,  0x33}, //
	{0x251,  0x20}, //
	{0x252,  0xe5}, //
	{0x253,  0xfb}, //
	{0x254,  0x13}, //
	{0x255,  0x26}, //
	{0x256,  0x07}, //
	{0x257,  0xf5}, //
	{0x258,  0xea}, //
	{0x259,  0x21}, //

	{0x263,  0x98}, //	# R D30 to D20
	{0x264,  0xa8}, //	# B D30 to D20
	{0x265,  0x98}, //	# R D20 to D30
	{0x266,  0xa8}, //	# B D20 to D30
	{0x267,  0xc8}, //		# R D65 to D30
	{0x268,  0x9f}, //		# B D65 to D30
	{0x269,  0xc8}, //		# R D30 to D65
	{0x26a,  0x9f}, //		# B D30 to D65

	//# IDP Register Setting
	//{0x00,	0x03}, //
	{0x310,	0xff}, //
	{0x311,	0x1d}, // PCLK positive	// 0x0d, PCLK negative
	{0x312,	0xF5}, // YUYV		// 0xD5, YVYU

	{0x38c,	0x10}, //

	//# Shading Register Setting
	{0x340,	0x00}, //
	{0x341,	0x21}, //
	{0x342,	0x32}, //
	{0x343,	0x43}, //
	{0x344,	0x55}, //
	{0x345,	0x55}, //
	{0x346,	0x11}, //	# left R gain[7:4], right R gain[3:0]
	{0x347,	0x23}, //	# top R gain[7:4], bottom R gain[3:0]
	{0x348,	0x10}, //	# left Gr gain[7:4], right Gr gain[3:0]
	{0x349,	0x12}, //	# top Gr gain[7:4], bottom Gr gain[3:0]
	{0x34a,	0x01}, //	# left Gb gain[7:4], right Gb gain[3:0]
	{0x34b,	0x12}, //	# top Gb gain[7:4], bottom Gb gain[3:0]
	{0x34c,	0x00}, //	# left B gain[7:4], right B gain[3:0]
	{0x34d,	0x11}, //	# top B gain[7:4], bottom B gain[3:0]
	{0x34e,	0x04}, //	# X-axis center high[3:2], Y-axis center high[1:0]
	{0x34f,	0x50}, //	# X-axis center low[7:0]
	{0x350,	0xf6}, //	# Y-axis center low[7:0]
	{0x351,	0x80}, //	# Shading Center Gain
	{0x352,	0x00}, //	# Shading R Offset
	{0x353,	0x00}, //	# Shading Gr Offset
	{0x354,	0x00}, //	# Shading Gb Offset
	{0x355,	0x00}, //	# Shading B Offset

	//# Gamma
	{0x330,	0x0 }, // #0x0
	{0x331,	0x3 }, // #0x6
	{0x332,	0xA }, // #0x10
	{0x333,	0x1E}, // #0x27
	{0x334,	0x42}, // #0x48
	{0x335,	0x5E}, // #0x62
	{0x336,	0x74}, // #0x77
	{0x337,	0x86}, // #0x88
	{0x338,	0x96}, // #0x97
	{0x339,  0xA4}, // #0xA5
	{0x33a,  0xB1}, // #0xB2
	{0x33b,  0xC8}, // #0xC9
	{0x33c,  0xDC}, // #0xDB
	{0x33d, 	0xEF}, // #0xED
	{0x33e, 	0xF8}, // #0xF7
	{0x33f,  0xFF}, // #0xFF

	//# Color matrix (D65) - Daylight
	{0x371,	0x3b}, //	#0x3a
	{0x372,	0xca}, //	#0xc9
	{0x373,	0xfb}, //	#0xfe
	{0x374,	0x0f}, //	#0x10
	{0x375,	0x22}, //	#0x23
	{0x376,	0x0f}, //	#0x0b
	{0x377,	0xf7}, //	#0xfb
	{0x378,	0xc7}, //	#0xce
	{0x379,	0x42}, //	#0x38

	//# Color matrix (D30) - CWF
	{0x37a,	0x3b}, //	#0x39
	{0x37b,	0xcb}, //	#0xcd
	{0x37c,	0xfa}, //	#0xfa
	{0x37d,	0x0e}, //	#0x0a
	{0x37e,	0x21}, //	#0x29
	{0x37f,	0x11}, //	#0x0c
	{0x380,	0xf4}, //	#0xf5
	{0x381,	0xc4}, //	#0xc7
	{0x382,	0x48}, //	#0x44

	//# Color matrix (D20) - A
	{0x383,	0x3a}, //  #0x3a
	{0x384,	0xcd}, //  #0xcc
	{0x385,	0xf9}, //  #0xfa
	{0x386,	0x15}, //  #0xfe
	{0x387,	0x26}, //  #0x3a
	{0x388,	0x05}, //  #0x08
	{0x389,	0xed}, //  #0xf9
	{0x38a,	0xbb}, //  #0xc3
	{0x38b,	0x57}, //  #0x44

	//# DPCNR
	{0x317, 	0xC8}, //  # DPCNRCTRL
	{0x318, 	0x18}, //  # DPTHR
	{0x319, 	0x48}, //  # [7:6] G DP Number Thr @ Dark | [5:0] DPTHRMIN
	{0x31A, 	0x48}, //  # [7:6] G DP Number Thr @ Normal | [5:0] DPTHRMAX
	{0x31B, 	0x24}, //  # DPTHRSLP( [7:4] @ Normal | [3:0] @ Dark )
	{0x31C, 	0x00}, //  # NRTHR
	{0x31D, 	0x48}, //  # [7:6] C DP Number Thr @ Dark | [5:0] NRTHRMIN 0x48
	{0x31E, 	0x48}, //  # [7:6] C DP Number Thr @ Normal | [5:0] NRTHRMAX
	{0x31F, 	0x68}, //  # NRTHRSLP( [7:4] @ Normal | [3:0] @ Dark ) 0x20
	{0x320, 	0x04}, //  # IllumiInfo STRTNOR
	{0x321, 	0x0f}, //  # IllumiInfo STRTDRK

	{0x356,	0x10}, //	# lowlux shading enable
	{0x357,	0x92}, //	# lowlux shading
	{0x358,	0x00}, //	# lowlux shading on

	//# Edge gain
	{0x390,	0x18}, //	# upper gain
	{0x391,	0x18}, //	# down gain
	{0x392,	0x04}, //	# upper coring value
	{0x396,	0x04}, //	# down coring value

	{0x39f,	0x10}, //	# Yeugain
	{0x3a0,	0x10}, //	# Yedgain9
	{0x3a9,	0x12}, //	# Cr saturation
	{0x3aa,	0x12}, //	# Cb saturation
	{0x3b9,	0x10}, //	# 0x13 lowlux color
	{0x3ba,	0x20}, //	# 0x10 lowlux color

	//#0xdd	0xc7
	{0x3dd,	0x4f}, //	# ENHCTRL5
	{0x3de,	0xba}, //	# NOIZCTRL

	{0x3e5,	0x15}, //
	{0x3e6,	0x28}, //
	{0x3e7,	0x04}, //

	//#AE On
	//{0x00,	0x01}, //
	{0x110,	0x80}, //

	//#Sensor On
	//{0x00,	0x00}, //
	{0x003,	0x55}, //


	//[END]
	//# *****************************************************************************
	//#             HISTORY
	//#
	//# 수정일자 및 담당자 : 20081218 유문철
	//# 사용모듈 : 18BV18 #02 엔지온모듈
	//# 수정목적 : initial setting
	//# 수정사항 : 기존 SIV120B setting을 base로 수정
	//#
	//# *****************************************************************************

};

static OV_FpsSetting_t*  GetOvFpsSetting(U8 Fps, OV_FpsSetting_t staOvFpsSetting[], U8 byArrayLen)
{
	U8 i;
	U8 Idx;

	ISP_MSG((" array size = %bd\n", byArrayLen));

	if(g_bIsHighSpeed)
	{
		Idx=2; //8fps for uxga, 10 fps for vga
	}
	else
	{
		Idx = 0;
	}

	for(i=0; i< byArrayLen; i++)
	{
		ISP_MSG((" fps i = %bd\n",i));
		if(staOvFpsSetting[i].byFps ==Fps)
		{
			Idx=i;
			break;
		}
	}
	ISP_MSG((" get fps idx = %bd\n",Idx));
	return &staOvFpsSetting[Idx];
}

//this function is also called during Probe stage to calculate the transfer_size/LWM/HWM
void GetSensorPclkHsync(U16 wSensorSPFormat,U8 byFps)
{
	OV_FpsSetting_t *pSIV120B_FpsSetting;

	wSensorSPFormat = wSensorSPFormat; // for delete warning

	pSIV120B_FpsSetting=GetOvFpsSetting(Fps, g_staSIV120BFpsSetting, sizeof(g_staSIV120BFpsSetting)/sizeof(OV_FpsSetting_t));
	g_wSensorHsyncWidth = pSIV120B_FpsSetting->wExtraDummyPixel;
	g_dwPclk= pSIV120B_FpsSetting->dwPixelClk;
}

void SIV120BSetFormatFps(U8 SetFormat, U8 Fps)
{
	OV_FpsSetting_t *pSIV120B_FpsSetting;
	U16 wHBnkt;

	SetFormat = SetFormat; // for delete warning

	pSIV120B_FpsSetting=GetOvFpsSetting(Fps, g_staSIV120BFpsSetting, sizeof(g_staSIV120BFpsSetting)/sizeof(OV_FpsSetting_t));

	// initial all register setting
	WriteSensorSettingWB(sizeof(gc_SIV120B_Setting)/3, gc_SIV120B_Setting);

	// 2) write sensor register for fps
	Write_SenReg_Mask(0x004, (pSIV120B_FpsSetting->byClkrc)<<2, 0x0C);	// write CLKRC
	wHBnkt =  pSIV120B_FpsSetting->wExtraDummyPixel -(800-1);
	Write_SenReg(0x021, INT2CHAR(wHBnkt,0));	//Write dummy pixel LSB
	Write_SenReg_Mask(0x020, wHBnkt>>4, 0xF0); // Writedummy pixel MSB

	// 3) update variable for AE
//	g_wAECExposureRowMax = 500;
	// hemonel 2009-08-10: for calculate manual exposure time
	//g_wSensorHsyncWidth = pSIV120B_FpsSetting->wExtraDummyPixel;
	//g_dwPclk= pSIV120B_FpsSetting->dwPixelClk;
	GetSensorPclkHsync(VGA_FRM,Fps);
}

void CfgSIV120BControlAttr(void)
{
	g_bySensorSize = SENSOR_SIZE_VGA;
	g_wSensorSPFormat = VGA_FRM;

	{
		memcpy(g_asOvCTT,gc_SIV120B_CTT,sizeof(gc_SIV120B_CTT));
	}

	{
		// AE backlight compensation parameter
		g_byOVAEW_Normal = SIV120B_AE_TARGET;
		g_byOVAEB_Normal = SIV120B_AE_TARGET;

		g_byOVAEW_BLC = g_byOVAEW_Normal + 0x20;
		g_byOVAEB_BLC = g_byOVAEB_Normal + 0x20;
	}

	{
		//g_bySV28VoltSel =  SV28_VOL_2V9;
		//g_bySV18VoltSel=  SV18_VOL_1V5;
		//	XBYTE[REG_TUNESVIO_SVA] =  SV28_VOL_2V9|SV18_VOL_1V5;
		// hemonel 2010-09-08: support 1.8V sensor IO
#if (_CHIP_ID_ & _RTS5840_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V79;
#elif (_CHIP_ID_ & _RTS5832_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V85;
#elif (_CHIP_ID_ & _RTS5829B_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V82;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V80;
#endif
	}

	{
		InitFormatFrameFps();
	}


}

#ifndef _USE_BK_HSBC_ADJ_
void SetSIV120BBrightness(S16 swSetValue)
{
	U16 wBrightVal;

	//Read_SenReg(0xAB, &wBrightSign);
	if(swSetValue>=0)
	{
		wBrightVal = swSetValue;
	}
	else
	{
		wBrightVal = -swSetValue + 0x80;
	}
	Write_SenReg(0x3AB, wBrightVal);	// write Brightness value

	return;
}


void	SetSIV120BContrast(U16 wSetValue)
{
	Write_SenReg_Mask(0xAC, wSetValue, 0xBF);		// [7] contrast gain enable, [5:0] contrast gain (x1/16)
	Write_SenReg(0xAD, 0x80);		// Y offset value @ contrast gain adaptaion

	return;
}

void	SetSIV120BSaturation(U16 wSetValue)
{
	Write_SenReg(0x3A9, wSetValue);		// CR color saturation gain (x1/16)
	Write_SenReg(0x3AA, wSetValue);		// CB color saturation gain (x1/16)

	return;
}

void	SetSIV120BHue(S16 swSetValue)
{
	U8 byHueCos,byHueSin, byHueCosSign, byHueSinSign;
	float fHue;

	if(swSetValue>=0)
	{
		byHueSinSign = 0x00;
	}
	else
	{
		byHueSinSign = 0x80;
	}
	if((swSetValue <= 90) &&(swSetValue >= -90))
	{
		byHueCosSign = 0x00;
	}
	else
	{
		byHueCosSign = 0x80;
	}

	fHue = (float)swSetValue/(float)180*3.1415926;
	byHueCos = abs(cos(fHue)*64);
	byHueSin = abs(sin(fHue)*64);

	Write_SenReg(0x3AE, byHueCos + byHueCosSign);
	Write_SenReg(0x3AF, byHueSin + byHueSinSign);

	return;
}
#endif

// manual sharpness mode
void	SetSIV120BSharpness(U8 bySetValue)
{
	U8 byGreenSharpLimit;
	U8 byYSharpLimit;

	switch(bySetValue)
	{
	case 0:
		byGreenSharpLimit = 0x00;
		byYSharpLimit = 0x00;
		break;
	case 1:
		byGreenSharpLimit = 0x06;
		byYSharpLimit = 0x04;
		break;
	case 2:
		byGreenSharpLimit = 0x0C;
		byYSharpLimit = 0x08;
		break;
	case 3:
		byGreenSharpLimit = 0x12;
		byYSharpLimit = 0x0C;
		break;
	case 4:	// default value
		byGreenSharpLimit = 0x18;
		byYSharpLimit = 0x10;
		break;
	case 5:
		byGreenSharpLimit = 0x24;
		byYSharpLimit = 0x14;
		break;
	case 6:
		byGreenSharpLimit = 0x32;
		byYSharpLimit = 0x18;
		break;
	default:
		byGreenSharpLimit = 0x4A;
		byYSharpLimit = 0x1C;
		break;
	}

	Write_SenReg(0x390, byGreenSharpLimit);	// Green Edge upper gain
	Write_SenReg(0x391, byGreenSharpLimit);	// Green Edge down gain
	Write_SenReg(0x39F, byYSharpLimit);		// Y Edge upper gain
	Write_SenReg(0x3A0, byYSharpLimit);		// Y Edge down gain
}


#ifndef _USE_BK_SPECIAL_EFFECT_
void SetSIV120BEffect(U8 byEffect)
{
	/*
	U8 bySDE = 0x20; //SDE: special digital effect enable
	U8 byUvalue = 0x80, byVvalue = 0x80;

	switch (byEffect)
	{
		case SNR_EFFECT_NEGATIVE:
			//byEfft = 0x20;
			bySDE = 0x40;
			break;
		case SNR_EFFECT_MONOCHROME:
			//byEfft = 0x20;
			//bySDE = 0x20;
			break;
		case SNR_EFFECT_SEPIA:
			//byEfft = 0x20;
			bySDE = 0x18;
			byUvalue = 0x40;
			byVvalue = 0xa0;
			break;
		case SNR_EFFECT_GREENISH:
			//byEfft = 0x20;
			bySDE = 0x18;
			byUvalue = 0x60;
			byVvalue = 0x60;
			break;
		case SNR_EFFECT_REDDISH:
			//byEfft = 0x20;
			bySDE = 0x18;
			byUvalue = 0x80;
			byVvalue = 0xc0;
			break;
		case SNR_EFFECT_BLUISH:
			//byEfft = 0x20;
			bySDE = 0x18;
			byUvalue = 0xa0;
			byVvalue = 0x40;
			break;
		default:
			//byEfft = 0x0; //no effect
			bySDE = 0x04;
			break;
	}
	//Write_SenReg_Mask(0x64, byEfft, 0x20);
	Write_SenReg_Mask(0xa6, bySDE, 0xf8);
	Write_SenReg(0x60, byUvalue);
	Write_SenReg(0x61, byVvalue);*/
}
#endif

void SetSIV120BGain(float fGain)
{
}

void SetSIV120BImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.
{
	/***
		0x01 mirror
		0x02 flip
		0x03 mirror and flip
	***/
	Write_SenReg_Mask(0x004, bySnrImgDir,0x03);
}

/*
void SetSIV120BWindowCrop(U16 wXStart,U16 wXEnd,U16 wYStart,U16 wYEnd )
{

}
*/

void SetSIV120BOutputDim(U16 wWidth,U16 wHeight)
{
	U8 byVMODE;
	U16 wTemp, wHBnkt;

	wWidth = wWidth; 	// for delete warning

	switch (wHeight)
	{
	case 288:
		byVMODE = 0x02;
		break;
	case 144:
		byVMODE = 0x01;
		break;
	case 240:
		byVMODE = 0x05;
		break;
	case 120:
		byVMODE = 0x04;
		break;
	default:	// 640x480
		byVMODE = 0x06;
		break;
	}
	Write_SenReg_Mask(0x005, byVMODE, 0x07);

	if((wHeight == 288) || (wHeight == 144))
	{
		Read_SenReg(0x020, &wTemp);		//[7:4], high byte
		Read_SenReg(0x021, &wHBnkt); 	//[7:0], low byte
		wHBnkt += ((wTemp&0xF0)<<4);

		wHBnkt += (800-512);	// when VGA, width = 800, when CIF, width = 512
		Write_SenReg(0x021, INT2CHAR(wHBnkt,0));	//Write dummy pixel LSB
		Write_SenReg_Mask(0x020, wHBnkt>>4, 0xF0); // Writedummy pixel MSB
		Write_SenReg(0x023, 190+1);
	}
}

void SetSIV120BPwrLineFreq(U8 byFPS, U8 byLightFrq)
{
	U8 byD50Base, byD60Base;

	// hemonel 2009-07-06: optimize code
	byD50Base= CalAntiflickerStep(byFPS, 50, 500);// (U16)byFPS*500/(U16)100;	// 500: the number of rows at one frame with LA measured
	byD60Base= CalAntiflickerStep(byFPS, 60, 500);//(U16)byFPS*500/(U16)120;

	if (byLightFrq == PWR_LINE_FRQ_50)
	{
		Write_SenReg(0x134, byD50Base);
	}
	else if (byLightFrq == PWR_LINE_FRQ_60)
	{
		Write_SenReg(0x134, byD60Base);
	}
}

void SetSIV120BWBTemp(U16 wSetValue)
{
	OV_CTT_t ctt;

	ctt = OV_SetColorTemperature(wSetValue);

	Write_SenReg(0x260, ctt.wRgain);
	Write_SenReg(0x262, ctt.wGgain);
	Write_SenReg(0x261, ctt.wBgain);
}

// hemonel 2009-5-12: siv120b has three ccm matrix, so not use backend isp ccm
/*
OV_CTT_t GetSIV120BAwbGain(void)
{
	OV_CTT_t ctt;

	Read_SenReg(0x260, &ctt.wRgain);
	Read_SenReg(0x262, &ctt.wGgain);
	Read_SenReg(0x261, &ctt.wBgain);

	return ctt;
}
*/
void SetSIV120BWBTempAuto(U8 bySetValue)
{
	if(bySetValue)
	{
		// atuo white balance
		Write_SenReg_Mask(0x210, 0xC0,0xC0);
	}
	else
	{
		// manual white balance
		Write_SenReg_Mask(0x210, 0x00,0xC0);
	}
}

void SetSIV120BBackLightComp(U8 bySetValue)
{
	U8 byAE_TARGET;

	if(bySetValue)
	{
		byAE_TARGET = g_byOVAEW_BLC;
	}
	else
	{
		byAE_TARGET = g_byOVAEW_Normal;
	}

	Write_SenReg(0x112, byAE_TARGET);
	Write_SenReg(0x113, byAE_TARGET);
	Write_SenReg(0x114, byAE_TARGET);
}

U16 GetSIV120BAEGain(void)
{
	U16 wTemp;
	U16 wGain;

	Read_SenReg(0x132, &wTemp);

	wGain = (wTemp&0x1F) + 32;	// FGAINx32
	wGain <<=(wTemp>>5);

	return (wGain>>1);	// Gainx16
}

void SetSIV120BIntegrationTimeAuto(U8 bySetValue)
{
	if(bySetValue == EXPOSURE_TIME_AUTO_MOD_APERTPRO)
	{
		// auto exposure
		Write_SenReg_Mask(0x110, 0x80,  0x80);
	}
	else
	{
		// manual expsoure
		Write_SenReg_Mask(0x110, 0x00,  0x80);
	}
}

void SetSIV120BIntegrationTime(U16 wEspline)
{
	// write exposure time to register
	Write_SenReg(0x131, INT2CHAR(wEspline, 0));
	Write_SenReg(0x130, INT2CHAR(wEspline, 1));
}
#endif //RTS58XX_SP_SIV120B

