#ifndef _OV2722_H_
#define _OV2722_H_

#define OV2722_AEW	0x70// 0x78 ---jqg 20090422
#define OV2722_AEB	0x68// 0x68
#define OV2722_VPT	0xd4

void OV2722SetFormatFps(U16 SetFormat, U8 Fps);
void CfgOV2722ControlAttr(void);
void SetOV2722ImgDir(U8 bySnrImgDir);
void SetOV2722IntegrationTime(U16 wEspline);
void SetOV2722Exposuretime_Gain(float fExpTime, float fTotalGain);
void InitOV2722IspParams();
void OV2722_POR();
void SetOV2722DynamicISP(U8 byAEC_Gain);
void SetOV2722DynamicISP_AWB(U16 wColorTempature);
void SetOV2722Gain(float fGain);
#endif
