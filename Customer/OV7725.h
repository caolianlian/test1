#ifndef _OV7725_H_
#define _OV7725_H_

#define OV7725_AEW 0x40
#define OV7725_AEB	0x30


void OV7725SetFormatFps(U8 SetFormat, U8 Fps);
void CfgOV7725ControlAttr(void);
void SetOV7725Brightness(S16 swSetValue);
void	SetOV7725Contrast(U16 wSetValue);
void	SetOV7725Saturation(U16 wSetValue);
void	SetOV7725Hue(S16 swSetValue);
void	SetOV7725Sharpness(U8 bySetValue);
void SetOV7725Effect(U8 byEffect);
void SetOV7725ImgDir(U8 bySnrImgDir);
void SetOV7725WindowCrop(U16 wXStart,U16 wXEnd,U16 wYStart,U16 wYEnd );
void SetOV7725OutputDim(U16 wWidth,U16 wHeight);
void SetOV7725PwrLineFreq(U8 byFPS, U8 byLightFrq);
void SetOV7725WBTemp(U16 wRgain, U16 wGgain, U16 wBgain);
OV_CTT_t GetOV7725AwbGain(void);
void SetOV7725WBTempAuto(U8 bySetValue);
void SetOV7725BackLightComp(U8 bySetValue);
extern code OV_CTT_t gc_OV7725_CTT[3];
void SetOV7725IntegrationTimeAuto(U8 bySetValue);
void SetOV7725IntegrationTime(U16 wEspline);
void SetOV7725Gain(float fGain);
#endif // _OV7725_H_
