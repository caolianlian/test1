#ifndef _OV5640_H_
#define _OV5640_H_

#define OV5640_AEW	0x70// 0x78 ---jqg 20090422
#define OV5640_AEB	0x68// 0x68
#define OV5640_VPT	0xd4

typedef struct OV5640_FpsSetting
{
    U8 byFps;
    U8  byClkrc; // register 0x11: clock pre-scalar
    U8 byPLL;
    U8 byExtraDummyPixel;   // register 0x2a,0x2b: dummy pixel adjustment
    U32 dwPixelClk;
} OV5640_FpsSetting_t;
void OV5640_POR(void);
void OV5640SetFormatFps(U16 SetFormat, U8 Fps);
void CfgOV5640ControlAttr(void);

void SetOV5640Brightness(S8 sbySetValue);
void	SetOV5640Contrast(U8 wSetValue);
void	SetOV5640Saturation(U8 wSetValue);
void	SetOV5640Hue(S16 swSetValue);

void	SetOV5640Sharpness(U8 bySetValue);
void SetOV5640Effect(U8 byEffect);
void SetOV5640ImgDir(U8 bySnrImgDir);
void SetOV5640WindowCrop(U16 wXStart,U16 wXEnd,U16 wYStart,U16 wYEnd );
void SetOV5640OutputDim(U16 wWidth,U16 wHeight);
void SetOV5640PwrLineFreq(U8 byFPS, U8 byLightFrq);
void SetOV5640WBTemp(U16 wRgain, U16 wGgain, U16 wBgain);
void SetOV5640WBTempAuto(U8 bySetValue);
void SetOV5640BackLightComp(U8 bySetValue);
extern code OV_CTT_t gc_OV5640_CTT[3];
void SetOV5640IntegrationTimeAuto(U8 bySetValue);
void SetOV5640IntegrationTime(U16 wEspline);
void SetOV5640LowLightComp(U8 bySetValue);
void OV5640PreviewToCapture(U16 wPreviewWidth, U16 wStillWidth);
void OV5640CaptureToPreview();
void SetOV5640Gain(float fGain);
#endif // _OV775_H_

