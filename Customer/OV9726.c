#include "Inc.h"

#ifdef RTS58XX_SP_OV9726

#ifdef _ENABLE_OUTDOOR_DETECT_
Dyanmic_AWB_Boundary_t code gc_AWBDynmaicBoundary={28,-8,16,8,50,30,2,21};
#endif

OV_CTT_t code gc_OV9726_CTT[3] =
{
	{2900,0x112,0x100,0x250},
	{4600,0x190,0x100,0x190},
	{6600,0x1D8,0x100,0x124},
};

code OV_FpsSetting_t  g_staOv9726FpsSetting[]=
{
// FPS ExtraDummyPixel clkrc 	   pclk
//	{5,		(1682+1683+1),	(3),		14135400},		//OV9726 clk too low
	{5,		(1682+841+1),	(2),		21203200},
	{7,		(1683),			(4),		10601575}, 	// adjust fps >=30fps for msoc jitter
	{8,		(1682+1473+1),	(2), 		21203200},
	{9,		(1682+1121+1),	(2), 		21203200},
	{10,		(1682+841+1),		(2), 		21203200},
	{15,		(1683+1),			(2), 		21203200},
	{20,		(1682+841+1),		(0), 		42406300},
	{25,		(1682+337+1),		(0),		42406300},
	{30,		(1682),			(0),		42406300}, 	// adjust fps >=30fps for msoc jitter
};

/*	// hemonel 2011-07-04: Albert use only one LSC curve to pass MSOC premium test, so delete these code
#ifdef _MSOC_TEST_
// bozhang 2011-07-01: use the different LSC curve at A and D65 for pass MSOC premium test
U8 code gc_byLSC_RGB_Curve_OV9726_3500K[3][48] =
{

{128, 0, 130, 0, 133, 0, 137, 0, 144, 0, 152, 0, 163, 0, 179, 0, 199, 0, 225, 0, 0, 1, 34, 1, 68, 1, 68, 1, 68, 1, 68, 1, 68, 1, 68, 1, 68, 1, 68, 1, 68, 1, 68, 1, 68, 1, 68, 1, },
{128, 0, 130, 0, 133, 0, 137, 0, 143, 0, 151, 0, 162, 0, 176, 0, 193, 0, 215, 0, 241, 0, 16, 1, 39, 1, 39, 1, 39, 1, 39, 1, 39, 1, 39, 1, 39, 1, 39, 1, 39, 1, 39, 1, 39, 1, 39, 1, },
{128, 0, 130, 0, 132, 0, 135, 0, 141, 0, 148, 0, 157, 0, 169, 0, 185, 0, 204, 0, 227, 0, 254, 0, 31, 1, 31, 1, 31, 1, 31, 1, 31, 1, 31, 1, 31, 1, 31, 1, 31, 1, 31, 1, 31, 1, 31, 1, },

};
U8 code gc_byLSC_RGB_Curve_OV9726_D65[3][48] =
{   // For Largan 9361H
   	//20110630
   	{128, 0, 129, 0, 134, 0, 141, 0, 151, 0, 165, 0, 183, 0, 206, 0, 234, 0, 12, 1, 47, 1, 85, 1, 134, 1, 134, 1, 134, 1, 134, 1, 134, 1, 134, 1, 134, 1, 134, 1, 134, 1, 134, 1, 134, 1, 134, 1, },
	{128, 0, 129, 0, 132, 0, 136, 0, 143, 0, 151, 0, 163, 0, 176, 0, 194, 0, 216, 0, 241, 0, 11, 1, 44, 1, 44, 1, 44, 1, 44, 1, 44, 1, 44, 1, 44, 1, 44, 1, 44, 1, 44, 1, 44, 1, 44, 1, },
	{128, 0, 128, 0, 131, 0, 135, 0, 141, 0, 149, 0, 158, 0, 171, 0, 187, 0, 207, 0, 228, 0, 252, 0, 25, 1, 25, 1, 25, 1, 25, 1, 25, 1, 25, 1, 25, 1, 25, 1, 25, 1, 25, 1, 25, 1, 25, 1, },

};

U16 code gc_wLSC_Center_Offset_OV9726_D65[6]={594 ,376,594 ,376,594, 376};
U16 code gc_wLSC_Center_Offset_OV9726_3500K[6]={622 ,388,622, 388,622 ,388};


#endif
*/
// Sensor setting
t_RegSettingWB code gc_OV9726_Setting[] =
{
	//----PLL----
	{0x0301, 0xa}, // for HD
	{0x0303, 0x3},
	{0x0305, 0x1},
	{0x0307, 0x35}, // 42.4MHz


	//------OVD----
	{0x103 , 0x0},  // sw reset
	{0x3026, 0x0},  // out put
	{0x3027, 0x0},
	{0x3002, 0xe8},  //io ctl
	{0x3004, 0x3 },
	{0x3005, 0xff},
	{0x3703, 0x42},  //?--
	{0x3704, 0x10},
	{0x3705, 0x45},
	{0x3603, 0xaa},
	{0x3632, 0x2f},
	{0x3620, 0x66},
	{0x3621, 0xc0},  //--?
	{0x340 , 0x3 }, //frame length
	{0x341 , 0x48},
	{0x342 , 0x06},//line length
	{0x343 , 0x80},
	{0x202 , 0x3 }, //ae
	{0x203 , 0x38},
	{0x3833, 0x4 }, //?--
	{0x3835, 0x2 },
	{0x4702, 0x4 },
	{0x4704, 0x0 },
	{0x4706, 0x8 },
	{0x5052, 0x1 },
	{0x3819, 0x6e},
	{0x3817, 0x94},  //--?
	{0x3a18, 0x0 }, //ae gain ceiling
	{0x3a19, 0x7f},
	{0x404e, 0x7e}, //?--
	{0x3631, 0x52},
	{0x3633, 0x50},
	{0x3630, 0xd2},
	{0x3604, 0x8 },
	{0x3601, 0x40},
	{0x3602, 0x14},
	{0x3610, 0xa0},
	{0x3612, 0x20},   //--?
	{0x34c , 0x5 }, // frame valid width
	{0x34d , 0x02},//0x0 }, // 1280 +2
	{0x34e , 0x3 },
	//{0x34f , 0x20},  // 800
	{0x34f , 0x21}, // set this more than 0x20, for 1 frame sample
	{0x340 , 0x3 },
	{0x341 , 0x48},  // 840
	{0x342 , 0x6 },
	{0x343 , 0x80},  // 1664
	{0x202 , 0x3 },//ae
	{0x203 , 0x38},
	//{0x303 , 0x2 },//vt clk div
	{0x101 , 0x1 }, //image orientation ???
	{0x3707, 0x14},  //?
	{0x3622, 0x9f},
	{0x5047, 0x63},
	{0x4002, 0x45},  //blc
	{0x4009, 0x00},
	//{0x5000, 0x86}, //dpc auto, lens C enable, must close BK lensC

	//{0x5001, 0x1 },//awb
	//{0x5001, 0x0 },//awb disable ___JQG_20091016_
	//{0x3406, 0x0 },//awb auto
	//{0x3406, 0x1 },//awb gain manual ___JQG_20091016_

	//{0x3503, 0x10},
	{0x3503, 0x13},  //ae manual, bit 1 AGC manual; bit 0 AEC mauanl
	{0x4005, 0x18},  //blc
	{0x100 , 0x1 }, //sw standby : streaming
	//{0x303 , 0x2 }, //vt sys clk div
	{0x3a0f, 0x64},  //ae ctl
	{0x3a10, 0x54},
	{0x3a11, 0xc2},
	{0x3a1b, 0x64},
	{0x3a1e, 0x54},
	{0x3a1a, 0x5 },

	//{0x4009, 0x00 },
	//----LSC----
	{0x5000, 0x06},	//disable LSC
	{0x5805, 0x4 }, // r a2
	{0x580d, 0x4 }, //g a2
	{0x5815, 0x4 }, //b a2

	//----AWB----
	{0x5001, 0x0}, // disable AWB
	{0x5047, 0x01}, // b1, disable AWB gain, enable BLC, disable LSC
	{0x3406, 0x1 }, // b0, AWB gain manual

	//--Liany.20110722.used for color card
	//{0x0601, 0x02}, //output color card
};

#ifdef _MIPI_EXIST_
t_RegSettingWB code gc_OV9726_mipi_Setting[] =
{
	{0x034c ,   0x05},  //x_output_size_High-->1284
	{0x034d ,   0x04},  //x_output_size_Low         //Liany.0x00->0x04 for 5825 BLC
	{0x034e ,   0x03},  //y_output_size_High-->801
	{0x034f ,   0x21},  //y_output_size_Low         //Liany.0x20->0x21 for 5825 BLC

	{0x4801 ,   0x0f},  //MIPI_CTRL01
	{0x4803 ,   0x05},  //MIPI_CTRL03
	{0x4601 ,   0x16},  //VFIFO_READ_CONTROL
	{0x3014 ,   0x05},  //MIPI_ENABLE
	{0x3104 ,   0x20},  //PLL Setting will change MIPIclk and FPS
};

#endif


static OV_FpsSetting_t*  GetOvFpsSetting(U8 Fps, OV_FpsSetting_t staOvFpsSetting[], U8 byArrayLen)
{
	U8 i;
	U8 Idx;

	ISP_MSG((" array size = %bd\n", byArrayLen));

	if(g_bIsHighSpeed)
	{
		Idx=2; //8fps for uxga, 10 fps for vga
	}
	else
	{
		Idx = 0;
	}

	for(i=0; i< byArrayLen; i++)
	{
		ISP_MSG((" fps i = %bd\n",i));
		if(staOvFpsSetting[i].byFps ==Fps)
		{
			Idx=i;
			break;
		}
	}
	ISP_MSG((" get fps idx = %bd\n",Idx));
	return &staOvFpsSetting[Idx];
}

static U16 MapSnrGlbGain2SnrRegSetting(U16 wGain)
{
	U16 wSnrRegGain = 0;
	U8 i;
	//QDBG(("byGain = %bd-> ",byGain));

	for(i=0; i<4; i++)
	{
		if(wGain >= 32)
		{
			wGain >>= 1;
			wSnrRegGain |=  (0x01<<(i+4));
		}
		else
		{
			wSnrRegGain |= (wGain-16);
			break;
		}
	}
	
	//QDBG(("sensorGain = %d    ",sensorGain));
	return wSnrRegGain;
}

float SnrRegSetting2SnrGlbGain(U16 wSnrRegGain)
{
	U16 wGain;
	float fGainOffset;
	U8 i;

	if(wSnrRegGain >= 0x70)
	{
		fGainOffset = 1.1; //0.99933; //0.5;
	}
	else if(wSnrRegGain >= 0x30)
	{
		fGainOffset = 0.360577; //0.32349; //0.2;
	}
	else if(wSnrRegGain >= 0x10)
	{
		fGainOffset = 0.107287; //0.11564; //0.0625;
	}
	else
	{
		fGainOffset = 0;
	}

	wGain = (wSnrRegGain&0x0f) + 16;
	wSnrRegGain >>= 4;

	for(i = 0; i < 4; i++)
	{
		if((wSnrRegGain&1) == 1)
		{
			wGain <<= 1;
		}

		wSnrRegGain >>= 1;
	}

	//return ((float)wGain)/16.0-fGainOffset;
	//return (((float)wGain)/16.0-fGainOffset-1.0)*1.27 + 1.0;
	return ((((float)wGain)/16.0-fGainOffset)-1.0)*1.2 + 1.0;
}

//this function is also called during Probe stage to calculate the transfer_size/LWM/HWM
void GetSensorPclkHsync(U16 wSensorSPFormat,U8 byFps)
{
	OV_FpsSetting_t *pOv9726_FpsSetting;

	wSensorSPFormat =wSensorSPFormat;
	
	pOv9726_FpsSetting=GetOvFpsSetting(byFps, g_staOv9726FpsSetting, sizeof(g_staOv9726FpsSetting)/sizeof(OV_FpsSetting_t));
	g_wSensorHsyncWidth = pOv9726_FpsSetting->wExtraDummyPixel; // this for manual exposure
	g_dwPclk = pOv9726_FpsSetting->dwPixelClk;		// this for scale speed
}

void OV9726SetFormatFps(U16 SetFormat, U8 Fps)
{
	U16 wTemp;
	OV_FpsSetting_t *pOv9726_FpsSetting;

	// hemonel 2010-04-13: for delete complier warning
	SetFormat = SetFormat;

	pOv9726_FpsSetting=GetOvFpsSetting(Fps, g_staOv9726FpsSetting, sizeof(g_staOv9726FpsSetting)/sizeof(OV_FpsSetting_t));
	// 1) change hclk if necessary

	// initial all register setting
	WriteSensorSettingWB(sizeof(gc_OV9726_Setting)/3, gc_OV9726_Setting);
#ifdef _MIPI_EXIST_
	WriteSensorSettingWB(sizeof(gc_OV9726_mipi_Setting)/3, gc_OV9726_mipi_Setting);
#endif

	// 2) write sensor register
	Write_SenReg(0x0305, pOv9726_FpsSetting->byClkrc);
	wTemp = pOv9726_FpsSetting->wExtraDummyPixel;
	Write_SenReg(0x0343, INT2CHAR(wTemp, 0)); //Write dummy pixel LSB
	Write_SenReg(0x0342, INT2CHAR(wTemp, 1)); //Write dummy pixel MSB

	//_________________________________________________________
	//if 0x3503=07
	// (1) set settings (2) (3)gain work (4)exposure work (5)dummy work

	//if 0x3503=17
	// (1) set settings (2) (3) (4)exposure work&gain work  (5)dummy work
	Write_SenReg(0x3503, 0x17);
	Write_SenReg(0x3819, 0x6C); // fix image blink at insert dummy

	// 3) update variable for AE
	//g_wSensorHsyncWidth = pOv9726_FpsSetting->wExtraDummyPixel; // this for manual exposure
	//g_dwPclk = pOv9726_FpsSetting->dwPixelClk;		// this for scale speed
	GetSensorPclkHsync(HD800P_FRM,Fps);
	g_wAECExposureRowMax = 835;	// this for max exposure time
	g_wAEC_LineNumber = 840;	// this for AE insert dummy line algothrim

	g_wSensorWidthBefBLC = 1280;
	g_wSensorHeightBefBLC = 800;

	if (Fps == 5)
	{
		Write_SenReg(0x340, 0x06);
		Write_SenReg(0x341, 0xA1);
		g_wAECExposureRowMax = 1692;
		g_wAEC_LineNumber = 1697;
	}
	
	InitOV9726IspParamsByResolution(g_wCurFrameWidth, g_wCurFrameHeight);

#ifdef _MIPI_EXIST_
	//-----------------------------Dphy Setting--------------------------------
	SetMipiDphy(MIPI_DATA_LANE0_EN, \
	            MIPI_DATA_FORMAT_RAW10 , MIPI_DATA_TYPE_RAW10, \
	            HSTERM_EN_TIME_33);
#endif
}

void CfgOV9726ControlAttr(void)
{
	U8 i;

	g_bySensorSize = SENSOR_SIZE_HD800P;
	g_wSensorSPFormat =HD800P_FRM;
#ifdef _ENABLE_MJPEG_
	g_byQtableScale = 6;	//HD720P HD800P Setting=6 for 7 comp rate
#endif
	{
		memcpy(g_asOvCTT, gc_OV9726_CTT,sizeof(gc_OV9726_CTT));
	}

	{
		// hemonel 2010-09-08: support 1.8V sensor IO
#if (_CHIP_ID_ & _RTS5840_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V79;
#elif (_CHIP_ID_ & _RTS5832_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V85;
#elif (_CHIP_ID_ & _RTS5829B_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V82;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V80;
#endif
	}

	{
		//====== resolution  setting ===========
		// -- preview ---
		g_aVideoFormat[0].byaVideoFrameTbl[0] = 8;		// resolution number
		g_aVideoFormat[0].byaVideoFrameTbl[1]=F_SEL_640_480;
		g_aVideoFormat[0].byaVideoFrameTbl[2]=F_SEL_320_240;
		g_aVideoFormat[0].byaVideoFrameTbl[3]=F_SEL_320_180;
		g_aVideoFormat[0].byaVideoFrameTbl[4]=F_SEL_1280_800;
		g_aVideoFormat[0].byaVideoFrameTbl[5]=F_SEL_960_540;
		g_aVideoFormat[0].byaVideoFrameTbl[6]=F_SEL_848_480;
		g_aVideoFormat[0].byaVideoFrameTbl[7]=F_SEL_640_360;
		g_aVideoFormat[0].byaVideoFrameTbl[8]=F_SEL_424_240;

		
		//--still image--
#ifdef _ZERO_SHUTTLE_LAGGER_
		g_aVideoFormat[0].byaStillFrameTbl[0] = 5;	// resolution number
		g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_640_480;
		g_aVideoFormat[0].byaStillFrameTbl[2]=F_SEL_640_360;
		g_aVideoFormat[0].byaStillFrameTbl[3]=F_SEL_424_240;
		g_aVideoFormat[0].byaStillFrameTbl[4]=F_SEL_320_180;
		g_aVideoFormat[0].byaStillFrameTbl[5]=F_SEL_320_240;
#else
		g_aVideoFormat[0].byaStillFrameTbl[0] = 8;	// resolution number
		g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_1280_800;
		g_aVideoFormat[0].byaStillFrameTbl[2]=F_SEL_320_240;
		g_aVideoFormat[0].byaStillFrameTbl[3]=F_SEL_640_480;
		g_aVideoFormat[0].byaStillFrameTbl[4]=F_SEL_960_540;
		g_aVideoFormat[0].byaStillFrameTbl[5]=F_SEL_848_480;
		g_aVideoFormat[0].byaStillFrameTbl[6]=F_SEL_640_360;
		g_aVideoFormat[0].byaStillFrameTbl[7]=F_SEL_424_240;
		g_aVideoFormat[0].byaStillFrameTbl[8]=F_SEL_320_180;
#endif		



		//====== fps  setting ===========
		{
			for(i=0; i<10; i++)
			{
				/*
					F_SEL_640_480    (0)
					F_SEL_160_120    (1)
					F_SEL_176_144    (2)
					F_SEL_320_180	(3)
					F_SEL_320_200    (4)
					F_SEL_320_240    (5)
					F_SEL_352_288    (6)
					F_SEL_424_240	(7)
					F_SEL_640_360	(8)
					F_SEL_640_400    (9)
				*/
				g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_30;
			}
			for(i=10; i<18; i++)
			{
				/*
					F_SEL_800_600    (10)
					F_SEL_848_480	(11)
					F_SEL_960_540	 (12)
					F_SEL_1024_768   (13)
					F_SEL_1280_720   (14)
					F_SEL_1280_800   (15)
					F_SEL_1280_960   (16)
					F_SEL_1280_1024  (17)
				*/
				g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_10;
			}
		}
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_800_600]= FPS_25;
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_848_480]= FPS_25;
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_960_540]= FPS_20;

		//====== format type  setting ===========
		g_aVideoFormat[0].byFormatType = FORMAT_TYPE_YUY2;

#ifdef _ENABLE_MJPEG_
		// copy YUY2 setting to MJPEG setting
		memcpy(&(g_aVideoFormat[1]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

#ifdef _ZERO_SHUTTLE_LAGGER_
		g_aVideoFormat[1].byaStillFrameTbl[0] = 8;	// resolution number
		g_aVideoFormat[1].byaStillFrameTbl[1]=F_SEL_1280_720;
		g_aVideoFormat[1].byaStillFrameTbl[2]=F_SEL_320_240;
		g_aVideoFormat[1].byaStillFrameTbl[3]=F_SEL_640_480;
		g_aVideoFormat[1].byaStillFrameTbl[4]=F_SEL_960_540;
		g_aVideoFormat[1].byaStillFrameTbl[5]=F_SEL_848_480;
		g_aVideoFormat[1].byaStillFrameTbl[6]=F_SEL_640_360;
		g_aVideoFormat[1].byaStillFrameTbl[7]=F_SEL_424_240;
		g_aVideoFormat[1].byaStillFrameTbl[8]=F_SEL_320_180;
#endif

		// modify MJPEG FPS setting
		for(i=10; i<18; i++)
		{
			g_aVideoFormat[1].waFrameFpsBitmap[i] = (FPS_30);
		}

		// modify MJPEG format type
		g_aVideoFormat[1].byFormatType = FORMAT_TYPE_MJPG;
#endif

#ifdef _ENABLE_M420_FMT_
		// copy YUY2 setting to M420 setting
		memcpy(&(g_aVideoFormat[2]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

		// modify M420 FPS setting
		{
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_800_600] =FPS_25|FPS_20|FPS_15|FPS_10|FPS_5;//|FPS_1;             //bit07 F_800_600
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_960_540] =FPS_25|FPS_20|FPS_15|FPS_10|FPS_5;//|FPS_5|FPS_3|FPS_1;                    //bit08 F_960_720
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1024_768] =FPS_15|FPS_10|FPS_5;//|FPS_1;                    //bit09 F_1024_768
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_720] =FPS_10|FPS_5;//FPS_9|FPS_8|FPS_1;    //bit10 F_1280_720
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_800] =FPS_10|FPS_5;

			// modify M420 format type
			g_aVideoFormat[2].byFormatType = FORMAT_TYPE_M420;
		}
#endif
	}

	{
		//Brightness - BrightnessItem

		//Contrast - ContrastItem

		//Saturation - SaturationItem

		//Hue - 	HueItem

		//Sharpness - SharpnessItem
		//	SharpnessItem.Min = 0;
		//	SharpnessItem.Max = 8;
		//	SharpnessItem.Res = 1;
		//	SharpnessItem.Def = 3;
		//	SharpnessItem.Last  =SharpnessItem.Def;

		// gamma - 	GammaItem

		// gain - GainItem

		// whitebalance - WhiteBalanceTempItem

		// backlight - BackLightCompItem

		// powerline -PwrLineFreqItem
		//	PwrLineFreqItem.Def = PWR_LINE_FRQ_60;
		//	PwrLineFreqItem.Last = PwrLineFreqItem.Def;

		// exposure time -	ExposureTimeAbsolutItem

		// low light - LowLightCompItem
	}

	InitOV9726IspParams();
}

void SetOV9726IntegrationTime(U16 wEspline)
{
	U16 wFrameLenLines;
	U16 wExpMax = g_wAECExposureRowMax-2;

	if(wEspline > wExpMax)
	{
		wFrameLenLines = wEspline + 20;
	}
	else
	{
		wFrameLenLines = g_wAEC_LineNumber;
	}

	if (wFrameLenLines%2==1)
	{
		wFrameLenLines++;
	}

	//-----------Write  frame length or dummy lines------------
	Write_SenReg(0x0341, INT2CHAR(wFrameLenLines, 0));
	Write_SenReg(0x0340, INT2CHAR(wFrameLenLines, 1));

	//-----------Write Exposuretime setting-----------
	Write_SenReg(0x0203, INT2CHAR(wEspline, 0));	// change exposure value low
	Write_SenReg(0x0202, INT2CHAR(wEspline, 1));	// change exposure value high

	//----------- write gain setting-------------------
#ifdef _RTK_EXTENDED_CTL_
	if(RtkExtISOItem.Last == ISO_AUTO)
	{
		Write_SenReg(0x0205, 0x00);	// fixed gain at manual exposure control(*1)
		//Write_SenReg(0x0204, 0);
		SetISPAEGain(1, 1, 1);	//ISP gain fixed at *1
	}
#else
	Write_SenReg(0x0205, 0x00);	// fixed gain at manual exposure control(*1)
	SetISPAEGain(1, 1, 1);	//ISP gain fixed at *1
#endif//#ifdef _RTK_EXTENDED_CTL_
}

#ifdef _RTK_EXTENDED_CTL_
void SetOV9726Gain(float fGain)
{
	U16 wGain;
	float fSnrGlbGain;

	if( fabs(fGain)<0.0001 )//ISO auto
	{
		//ISO_MSG(("SetOV9726Gain: ISO auto,  wGain = %u \n",wGain));
		//do noting
	}
	else
	{
		wGain = MapSnrGlbGain2SnrRegSetting((U16)(fGain*16.0));
		if((wGain != 0) && ((wGain&0xf) == 0))
		{
			wGain = ((wGain>>1)|0xf);
		}
		fSnrGlbGain = SnrRegSetting2SnrGlbGain(wGain);

		//----------- write gain setting-------------------
		//ISO_MSG(("SetOV9726Gain: wGain = %u \n",wGain));
		//only analog gain, no digital gain
		Write_SenReg(0x0205, INT2CHAR(wGain, 0));
		//Write_SenReg(0x0204, INT2CHAR(wGain, 1));
		SetISPAEGain(fGain, fSnrGlbGain, 1);
	}
}
#endif//#ifdef _RTK_EXTENDED_CTL_

void SetOV9726ImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.
{

	WaitFrameSync(ISP_INT1,ISP_DATA_START_INT);

	bySnrImgDir ^= 0x01; 	// OV9726 output mirrored image at default, so need mirror the image for normal output
	Write_SenReg_Mask(0x101, bySnrImgDir, 0x03);

	WaitFrameSync(ISP_INT1,ISP_DATA_END_INT);

	if(bySnrImgDir & 0x01)
	{
#ifdef _MIPI_EXIST_
		SetBLCWindowStart(1, 1);
#else
		SetBkWindowStart(1, 1);
#endif
	}
	else
	{
#ifdef _MIPI_EXIST_
		SetBLCWindowStart(0, 1);
#else
		SetBkWindowStart(0, 1);
#endif
	}
}

void SetOV9726Exposuretime_Gain(float fExpTime, float fTotalGain)
{
	U16 wExposureRows_floor;
	U16  wGainRegSetting; //data
	float fSnrGlbGain;
	float fExposureRows;
	U16 wSetDummy;
	float fTemp;
	U16 wExposurePixels;

	//wGainRegSetting = MapSnrGlbGain2SnrRegSetting((U16)(fTotalGain*16.0));
	//wGainRegSetting = MapSnrGlbGain2SnrRegSetting((U16)(((fTotalGain-1.0)/1.27+1.0)*16.0));
	wGainRegSetting = MapSnrGlbGain2SnrRegSetting((U16)(((fTotalGain-1.0)/1.2+1.0)*16.0));
	if((wGainRegSetting != 0) && ((wGainRegSetting&0xf) == 0))
	{
		wGainRegSetting = ((wGainRegSetting>>1)|0xf);
	}
	
	fSnrGlbGain = SnrRegSetting2SnrGlbGain(wGainRegSetting);
	
	fExposureRows = fExpTime/g_fSensorRowTimes;	//unit : 1row
	wExposureRows_floor = (U16)(fExposureRows);
	fTemp = fExpTime-((float)wExposureRows_floor*(g_fSensorRowTimes)); //fine integration time;unit:  us
	wExposurePixels =(U16)(fTemp * (float)(g_dwPclk/10000)) ; //unit: pixels

	// write exposure, gain, dummy line
	if(fabs(g_fCurExpTime-fExpTime) > 0.0001)
	{
		// set dummy
		wSetDummy = g_wAFRInsertDummylines + g_wAEC_LineNumber;

		// group register hold
		Write_SenReg(0x0104, 0x01);

		//-----------Write  frame length or dummy lines------------
		if (wSetDummy%2 == 1)
		{
			wSetDummy++;
		}
		Write_SenReg(0x0341, INT2CHAR(wSetDummy, 0));
		Write_SenReg(0x0340, INT2CHAR(wSetDummy, 1));

		//-----------Write Exposuretime setting-----------
		Write_SenReg(0x0203, INT2CHAR(wExposureRows_floor, 0));	// change exposure value low
		Write_SenReg(0x0202, INT2CHAR(wExposureRows_floor, 1));	// change exposure value high

		//Reck added 20100625, fix OV9726 red edge bug
		if (wExposurePixels>1280)
		{
			wExposurePixels = 1280;
		}
		Write_SenReg(0x0201, INT2CHAR(wExposurePixels, 0));	// change exposure value low
		Write_SenReg(0x0200, INT2CHAR(wExposurePixels, 1));	// change exposure value high

		//----------- write gain setting-------------------
		Write_SenReg(0x0205, INT2CHAR(wGainRegSetting, 0));
		//Write_SenReg(0x0204, INT2CHAR(wGainRegSetting, 1));

		SetISPAEGain(fTotalGain, fSnrGlbGain, 1);

		// group register release
		Write_SenReg(0x0104, 0);
		g_fCurExpTime = fExpTime;
	}
	else
	{
		// group register hold
		Write_SenReg(0x0104, 1);
		wSetDummy = g_wAFRInsertDummylines + g_wAEC_LineNumber;

		//-----------Write  frame length or dummy lines------------
		if (wSetDummy%2 == 1)
		{
			wSetDummy++;
		}
		Write_SenReg(0x0341, INT2CHAR(wSetDummy, 0));
		Write_SenReg(0x0340, INT2CHAR(wSetDummy, 1));

		// write gain setting
		Write_SenReg(0x0205, INT2CHAR(wGainRegSetting, 0));
		//Write_SenReg(0x0204, INT2CHAR(wGainRegSetting, 1));

		SetISPAEGain(fTotalGain, fSnrGlbGain, 1);

		// group register release
		Write_SenReg(0x0104, 0);
	}
	
	return;
}

void OV9726_POR(void )
{
#ifdef _MIPI_EXIST_
	Init_MIPI();
#else
	Init_CCS();
#endif
	ENTER_SENSOR_RESET();
	// power on
	SensorPowerControl(SWITCH_ON,EN_DELAYTIME);
	uDelay(1);
	// Initialize I2C Controller Timing
	LEAVE_SENSOR_RESET();
	// clock select 24M
	if( g_bySSCEnable== TRUE)
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_SSC_24M);
	}
	else
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_96M|CCS_CLK_DIVIDER_4);
	}

	EnableSensorHCLK();
	g_bySensorIsOpen = SENSOR_OPEN;
	WaitTimeOut_Delay(1);  //OV9726 spec request 8192clk
}

void InitOV9726IspParams(void )
{
	// AWB initial gain
	//use D50 R/G/B gain as AWB initial value
	g_wAWBRGain_Last= 0x17C;
	g_wAWBGGain_Last= 0x100;
	g_wAWBBGain_Last= 0x226;
	g_wProjectGR = g_wAWBRGain_Last;
	g_wProjectGB = g_wAWBBGain_Last;

	// Special ISP
	g_bySaturation_Def =64;
	g_byContrast_Def = 32; //Neil Tuning at chicony
	g_byTgamma_rate_max=63;
	g_byTgamma_rate_min =20;

	//g_wDynamicISPEn = 0;
	g_wDynamicISPEn = DYNAMIC_LSC_CT_EN| DYNAMIC_LSC_EN | DYNAMIC_SHARPNESS_EN | DYNAMIC_SHARPPARAM_EN | DYNAMIC_GAMMA_EN | DYNAMIC_CCM_CT_EN;
}

void InitOV9726IspParamsByResolution(U16 wCurWidth, U16 wCurHeight)
{
	wCurWidth = wCurWidth;
	wCurHeight = wCurHeight;
}

void SetOV9726DynamicISP(U8 byAEC_Gain)
{
	byAEC_Gain = byAEC_Gain;
}

void SetOV9726DynamicISP_AWB(U16 wColorTempature)
{
	wColorTempature =wColorTempature;

	// hemonel 2011-07-04: Albert use only one LSC curve to pass MSOC premium test, so delete these code
	/*
	#ifdef _MSOC_TEST_
	U8 i;

	// LSC Curve dynamic
	if ( (g_wDynamicISPEn&DYNAMIC_LSC_EN) == DYNAMIC_LSC_EN)
	{
		if(wColorTempature < 4000)
		{
	          		for (i=0;i<6;i++)
	          		{
	          			g_awLensShadingCenter[i] = gc_wLSC_Center_Offset_OV9726_3500K[i];
	          		}

				for (i=0;i<48;i++)
				{
					XBYTE[ISP_NLSC_CURVE_R_BASE+i]  = gc_byLSC_RGB_Curve_OV9726_3500K[0][i];
					XBYTE[ISP_NLSC_CURVE_G_BASE+i]  = gc_byLSC_RGB_Curve_OV9726_3500K[1][i];
					XBYTE[ISP_NLSC_CURVE_B_BASE+i]  = gc_byLSC_RGB_Curve_OV9726_3500K[2][i];
				}
		}
		else if(wColorTempature > 4500)
		{
	          		for (i=0;i<6;i++)
	          		{
	          			g_awLensShadingCenter[i] = gc_wLSC_Center_Offset_OV9726_D65[i];
	          		}

				for (i=0;i<48;i++)
				{
					XBYTE[ISP_NLSC_CURVE_R_BASE+i]  = gc_byLSC_RGB_Curve_OV9726_D65[0][i];
					XBYTE[ISP_NLSC_CURVE_G_BASE+i]  = gc_byLSC_RGB_Curve_OV9726_D65[1][i];
					XBYTE[ISP_NLSC_CURVE_B_BASE+i]  = gc_byLSC_RGB_Curve_OV9726_D65[2][i];
				}
		}

	}
	#endif
	*/
}
#endif
