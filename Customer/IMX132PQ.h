#ifndef _IMX132PQ_H_
#define _IMX132PQ_H_

void IMX132PQSetFormatFps(U16 SetFormat, U8 Fps);
void CfgIMX132PQControlAttr(void);
void SetIMX132PQImgDir(U8 bySnrImgDir);
void SetIMX132PQIntegrationTime(U16 wEspline);
void SetIMX132PQExposuretime_Gain(float fExpTime, float fTotalGain);
void SetIMX132PQDynamicISP(U8 byAEC_Gain);
void IMX132PQ_POR();
void SetIMX132PQDefGain();
void SetIMX132PQGain(float fGain);
#endif // _IMX132PQ_H_

