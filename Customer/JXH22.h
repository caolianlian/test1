#ifndef _JXH22_H_
#define _JXH22_H_

void JXH22SetFormatFps(U16 SetFormat, U8 Fps);
void CfgJXH22ControlAttr(void);
void SetJXH22ImgDir(U8 bySnrImgDir);
void SetJXH22IntegrationTime(U16 wEspline);
void SetJXH22Exposuretime_Gain(float fExpTime, float fTotalGain);
void InitJXH22IspParams(void );
void JXH22_POR(void );
void SetJXH22DynamicISP(U8 bymode);
void SetJXH22DynamicISP_AWB(U16 wColorTempature);
void SetJXH22SoftReset(void );
void SetJXH22Gain(float fGain);
#endif // _OV9710_H_

