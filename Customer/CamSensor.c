#include "Inc.h"

void ReadSensorID(void)
{
#ifdef RTS58XX_SP_HM1055
	g_SnrRegAccessProp.byI2CID = 0x48;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_WA;;
	g_wSensor = SENSOR_HM1055;
	g_bySensor_YuvMode = RAW_MODE;
	return;
#endif

#ifdef RTS58XX_SP_SIW021A
	g_SnrRegAccessProp.byI2CID = 0x6A; // MICRON_ID_ADDR_1040_1;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_BA;
	g_wSensor = SENSOR_SIW021A;
	g_bySensor_YuvMode = RAW_MODE;
	return;
#endif //RTS58XX_SP_SIW021A

#ifdef RTS58XX_SP_ST171
	g_SnrRegAccessProp.byI2CID = 0x6A; // MICRON_ID_ADDR_1040_1;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_BA;
	g_wSensor = SENSOR_ST171;
	g_bySensor_YuvMode = RAW_MODE;
	return;
#endif //RTS58XX_SP_ST171
#ifdef RTS58XX_SP_MI1040
	g_SnrRegAccessProp.byI2CID = MICRON_ID_ADDR_1040_1;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_2|I2C_ADDR_MODE_WA;
	g_wSensor = SENSOR_MI1040;
	g_bySensor_YuvMode =RAW_MODE;
	return;
#endif


#ifdef RTS58XX_SP_S5K6AAFX
	g_SnrRegAccessProp.byI2CID = SAMSUNG_ID_ADDR_6AA_78;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_2|I2C_ADDR_MODE_WA;
	g_wSensor = SENSOR_S5K6AAFX;
	g_bySensor_YuvMode =RAW_MODE;
	return;
#endif //RTS58XX_SP_S5K6AAFX	

	// -----------------------------------
	// SONY
	//-----------------------------------
#ifdef RTS58XX_SP_IMX119PQH5
	g_SnrRegAccessProp.byI2CID = SONY_ID_ADDR_IMX119PQH5;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_WA;
	g_wSensor = SENSOR_IMX119PQH5;
	g_bySensor_YuvMode = RAW_MODE;
	return;
#endif //RTS58XX_SP_IMX119PQH5

#ifdef RTS58XX_SP_IMX188PQ
	g_SnrRegAccessProp.byI2CID = SONY_ID_ADDR_IMX188PQ;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_WA;
	g_wSensor = SENSOR_IMX188PQ;
	g_bySensor_YuvMode = RAW_MODE;
	return;
#endif //RTS58XX_SP_IMX188PQ


#ifdef RTS58XX_SP_IMX132PQ
	g_SnrRegAccessProp.byI2CID = SONY_ID_ADDR_IMX132PQ;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_WA;
	g_wSensor = SENSOR_IMX132PQ;
	g_bySensor_YuvMode = RAW_MODE;
	return;
#endif //RTS58XX_SP_IMX119PQH5


#ifdef RTS58XX_SP_IMX208PQH5
	g_SnrRegAccessProp.byI2CID = SONY_ID_ADDR_IMX208PQH5;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_WA;
	g_wSensor = SENSOR_IMX208PQH5;
	g_bySensor_YuvMode = RAW_MODE;
	return;
#endif //RTS58XX_SP_IMX208PQH5

#ifdef RTS58XX_SP_IMX189PSH5
	g_SnrRegAccessProp.byI2CID = SONY_ID_ADDR_IMX189PSH5;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_WA;
	g_wSensor = SENSOR_IMX189PSH5;
	g_bySensor_YuvMode = RAW_MODE;
	return;
#endif //RTS58XX_SP_IMX189PSH5

#ifdef RTS58XX_SP_IMX076LQZC
	g_SnrRegAccessProp.byI2CID = SONY_ID_ADDR_IMX076LQZC;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_WA;
	g_wSensor = SENSOR_IMX076LQZC;
	g_bySensor_YuvMode = RAW_MODE;
	return;
#endif //RTS58XX_SP_IMX076LQZC
	// -----------------------------------
	// MOBIEN, OMNIVISION
	//-----------------------------------
#if (defined(RTS58XX_SP_OV7725)||defined(RTS58XX_SP_OV7740)||defined(RTS58XX_SP_OV7670)||defined(RTS58XX_SP_OV7675) ||defined(RTS58XX_SP_OV7690))
	g_SnrRegAccessProp.byI2CID = OMNIVISION_ID_ADDR0;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_BA;
#ifdef RTS58XX_SP_OV7725
	g_wSensor = SENSOR_OV7725;
	return;
#endif
#ifdef RTS58XX_SP_OV7740
	g_wSensor = SENSOR_OV7740;
	return;
#endif
#ifdef RTS58XX_SP_OV7670
	g_wSensor = SENSOR_OV7670;
	return;
#endif
#ifdef RTS58XX_SP_OV7675
	g_wSensor = SENSOR_OV7675;
	g_bySensor_YuvMode = YUV422_MODE;
	return;
#endif
#ifdef RTS58XX_SP_OV7690
	g_wSensor = SENSOR_OV7690;
	g_bySensor_YuvMode = YUV422_MODE;	
	return;
#endif
#endif	// #if (defined(RTS58XX_SP_OV7725)||defined(RTS58XX_SP_OV7740)||defined(RTS58XX_SP_OV7670)||defined(RTS58XX_SP_OV7675) ||defined(RTS58XX_SP_OV7690))


#if (defined(RTS58XX_SP_OV9663)||defined(RTS58XX_SP_OV9710))
	g_SnrRegAccessProp.byI2CID = OMNIVISION_ID_ADDR1;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_BA;
#ifdef RTS58XX_SP_OV9663
	g_wSensor = SENSOR_OV9663;
	return;
#endif
#ifdef RTS58XX_SP_OV9710
	g_wSensor = SENSOR_OV9710;
	g_bySensor_YuvMode = RAW_MODE;
	return;
#endif //RTS58XX_SP_OV9710	
#endif	//#if (defined(RTS58XX_SP_OV9663)||defined(RTS58XX_SP_OV9710))

#ifdef RTS58XX_SP_OV2720
	g_SnrRegAccessProp.byI2CID = OMNIVISION_ID_ADDR3;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_WA;
	g_wSensor = SENSOR_OV2720;
	g_bySensor_YuvMode =RAW_MODE;
	return;
#endif

#ifdef RTS58XX_SP_OV2650
	g_SnrRegAccessProp.byI2CID = OMNIVISION_ID_ADDR1;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_WA;
	g_wSensor = SENSOR_OV2650;
	return;
#endif

#ifdef RTS58XX_SP_OV2710
	g_SnrRegAccessProp.byI2CID = OMNIVISION_ID_ADDR3;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_WA;
	g_wSensor = SENSOR_OV2710;
	g_bySensor_YuvMode =RAW_MODE;
	return;
#endif

#ifdef RTS58XX_SP_OV2680
	g_SnrRegAccessProp.byI2CID = OMNIVISION_ID_ADDR3;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_WA;
	g_wSensor = SENSOR_OV2680;
	g_bySensor_YuvMode =RAW_MODE;
	return;
#endif

#ifdef RTS58XX_SP_OV9726
	g_SnrRegAccessProp.byI2CID = OMNIVISION_ID_ADDR2;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_WA;;
	g_wSensor = SENSOR_OV9726;
	g_bySensor_YuvMode = RAW_MODE;
	return;
#endif

#ifdef RTS58XX_SP_OV9715
	g_SnrRegAccessProp.byI2CID = OMNIVISION_ID_ADDR1;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_BA;
	g_wSensor = SENSOR_OV9715;
	g_bySensor_YuvMode = RAW_MODE;
	return;
#endif


#ifdef RTS58XX_SP_OV9728
	g_SnrRegAccessProp.byI2CID = OMNIVISION_ID_ADDR2;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_WA;;
	g_wSensor = SENSOR_OV9728;
	g_bySensor_YuvMode = RAW_MODE;
	return;
#endif

#ifdef RTS58XX_SP_OV9724
	g_SnrRegAccessProp.byI2CID = OMNIVISION_ID_ADDR2;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_WA;;
	g_wSensor = SENSOR_OV9724;
	g_bySensor_YuvMode = RAW_MODE;
	return;
#endif

#ifdef RTS58XX_SP_OV5642
	g_SnrRegAccessProp.byI2CID = OMNIVISION_ID_ADDR4;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_WA;
	g_wSensor = SENSOR_OV5642;
	g_bySensor_YuvMode = YUV422_MODE;
	return;
#endif

#ifdef RTS58XX_SP_OV2722
	g_SnrRegAccessProp.byI2CID = OMNIVISION_ID_ADDR3;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_WA;
	g_wSensor = SENSOR_OV2722;
	g_bySensor_YuvMode =RAW_MODE;
	return;
#endif

#ifdef RTS58XX_SP_OV3642
	g_SnrRegAccessProp.byI2CID = OMNIVISION_ID_ADDR4;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_WA;
	g_wSensor = SENSOR_OV3642;
	g_bySensor_YuvMode =YUV422_MODE;
	return;
#endif

#ifdef RTS58XX_SP_OV3640
	g_SnrRegAccessProp.byI2CID = OMNIVISION_ID_ADDR4;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_WA;
	g_wSensor = SENSOR_OV3640;
	g_bySensor_YuvMode =YUV422_MODE;
	return;
#endif

#ifdef RTS58XX_SP_OV5640
	g_SnrRegAccessProp.byI2CID = OMNIVISION_ID_ADDR4;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_WA;	
	g_wSensor = SENSOR_OV5640;
	g_bySensor_YuvMode =YUV422_MODE;
	return;
#endif

#ifdef RTS58XX_SP_T4K71
	g_SnrRegAccessProp.byI2CID = TOSHIBA_ID_ADDR_T4K71;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_WA;
	g_wSensor = SENSOR_T4K71;
	g_bySensor_YuvMode =RAW_MODE;
	return;
#endif

#ifdef RTS58XX_SP_JXH22
	g_SnrRegAccessProp.byI2CID = JXH22_ADDR;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_BA;;
	g_wSensor = SENSOR_JXH22;
	g_bySensor_YuvMode = RAW_MODE;
	return;
#endif
	//--------------------------------
	//FT2  sensor model
	//--------------------------------
#ifdef RTS58XX_SP_FT2
	g_SnrRegAccessProp.byI2CID = FT2_SENSOR_MODEL_ID_ADDR;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_BA;
	g_wSensor = SENSOR_FT2_MODEL;
	g_bySensor_YuvMode = RAW_MODE;
	return;
#endif

	// -----------------------------------
	// MICRON
	//-----------------------------------
#ifdef RTS58XX_SP_MI0360
	g_SnrRegAccessProp.byI2CPageSelAddr= MICRON_PAGE_SEL_ADDR_360;
	g_SnrRegAccessProp.byI2CID = MICRON_ID_ADDR_360;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_2|I2C_ADDR_MODE_PA;
	g_wSensor = SENSOR_MI360;
	return;
#endif

#ifdef RTS58XX_SP_MI1320
	g_SnrRegAccessProp.byI2CPageSelAddr= MICRON_PAGE_SEL_ADDR_1320;
	g_SnrRegAccessProp.byI2CID = MICRON_ID_ADDR_1320_1;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_2|I2C_ADDR_MODE_PA;
	g_wSensor = SENSOR_MI1320;
	return;
#endif

#ifdef RTS58XX_SP_MI1330
	g_SnrRegAccessProp.byI2CID = MICRON_ID_ADDR_1330_1;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_2|I2C_ADDR_MODE_WA;
	g_wSensor = SENSOR_MI1330;
	return;
#endif

#ifdef RTS58XX_SP_MI2020
	g_SnrRegAccessProp.byI2CID = MICRON_ID_ADDR_2020_1;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_2|I2C_ADDR_MODE_WA;
	g_wSensor = SENSOR_MI2020;
	return;
#endif

	// -----------------------------------
	// SETI sensor
	//-----------------------------------
#ifdef RTS58XX_SP_SIV100B
	g_SnrRegAccessProp.byI2CID = SETI_ID_ADDR_1;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_BA;
	g_wSensor = SENSOR_SIV100B;
	return;
#endif

#ifdef RTS58XX_SP_SID130B
	g_SnrRegAccessProp.byI2CID = SETI_ID_ADDR_2;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_BA;
	g_wSensor = SENSOR_SID130B;
	return;
#endif

#ifdef RTS58XX_SP_SIV120B
	g_SnrRegAccessProp.byI2CID = SETI_ID_ADDR_1;
	g_SnrRegAccessProp.byI2CPageSelAddr= SETI_PAGE_SEL_ADDR;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_PA;
	g_wSensor = SENSOR_SIV120B;
	return;
#endif

#ifdef RTS58XX_SP_SIV120D
	g_SnrRegAccessProp.byI2CID = SETI_ID_ADDR_1;
	g_SnrRegAccessProp.byI2CPageSelAddr= SETI_PAGE_SEL_ADDR;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_PA;
	g_wSensor = SENSOR_SIV120D;
	return;
#endif

#ifdef RTS58XX_SP_SIM120C
	g_SnrRegAccessProp.byI2CID = SETI_ID_ADDR_3;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_BA;
	g_wSensor = SENSOR_SIM120C;
	return;
#endif

#ifdef RTS58XX_SP_GC0307
	g_SnrRegAccessProp.byI2CID = GALAXYCORE_ID_ADDR;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_BA;
	g_wSensor = SENSOR_GC0307;
	return;
#endif

#ifdef RTS58XX_SP_GC1036
	g_SnrRegAccessProp.byI2CID = GALAXYCORE_ID_ADDR_GC1036;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_BA;;
	g_wSensor = SENSOR_GC1036;
	g_bySensor_YuvMode = RAW_MODE;
	return;
#endif

#ifdef RTS58XX_SP_GC1136
	g_SnrRegAccessProp.byI2CID = GALAXYCORE_ID_ADDR_GC1036;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_BA;;
	g_wSensor = SENSOR_GC1136;
	g_bySensor_YuvMode = RAW_MODE;
	return;
#endif

#ifdef RTS58XX_SP_YACBAA0S
	g_SnrRegAccessProp.byI2CID = HYNIX_ID_ADDR1;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_BA;
	g_wSensor = SENSOR_YACBAA0S;
	return;
#endif

#ifdef RTS58XX_SP_YACC5A2S
	g_SnrRegAccessProp.byI2CID = HYNIX_ID_ADDR2;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_BA;
	g_wSensor = SENSOR_YACC5A2S;
	return;
#endif

#ifdef RTS58XX_SP_YACBAC1S
	g_SnrRegAccessProp.byI2CID = HYNIX_ID_ADDR1;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_BA;
	g_wSensor = SENSOR_YACBAC1S;
	return;
#endif

#ifdef RTS58XX_SP_YACC6A1S
	g_SnrRegAccessProp.byI2CID = HYNIX_ID_ADDR2;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_BA;
	g_wSensor = SENSOR_YACC6A1S;
	g_bySensor_YuvMode = RAW_MODE;
	return;
#endif

#ifdef RTS58XX_SP_YACC611
	g_SnrRegAccessProp.byI2CID = HYNIX_ID_ADDR3;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_WA;
	g_wSensor = SENSOR_YACC611;
	g_bySensor_YuvMode = RAW_MODE;
	return;
#endif

#ifdef RTS58XX_SP_YACY9A1
	g_SnrRegAccessProp.byI2CID = HYNIX_ID_ADDR4;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_WA;;
	g_wSensor = SENSOR_YACY9A1;
	g_bySensor_YuvMode = RAW_MODE;
	return;
#endif

#ifdef RTS58XX_SP_YACD6A1C
	g_SnrRegAccessProp.byI2CID = HYNIX_ID_ADDR2;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_WA;
	g_wSensor = SENSOR_YACD6A1C;
	g_bySensor_YuvMode = RAW_MODE;
	return;
#endif

#ifdef RTS58XX_SP_YACY6A1C9SBC
	g_SnrRegAccessProp.byI2CID = HYNIX_ID_ADDR1;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_BA;
	g_wSensor = SENSOR_YACY6A1C9SBC;
	g_bySensor_YuvMode = RAW_MODE;
	return;
#endif

#ifdef RTS58XX_SP_S5K6A1
	g_SnrRegAccessProp.byI2CID = SAMSUNG_ID_ADDR_6A1;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_WA;
	g_wSensor = SENSOR_S5K6A1;
	g_bySensor_YuvMode = RAW_MODE;
	return;
#endif //RT

#ifdef RTS58XX_SP_S5K8AA
	g_SnrRegAccessProp.byI2CID = SAMSUNG_ID_ADDR_8AA;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_2|I2C_ADDR_MODE_WA;
	g_wSensor = SENSOR_S5K8AA;
	g_bySensor_YuvMode = RAW_MODE;
	return;
#endif

#ifdef RTS58XX_SP_RS0509
	g_SnrRegAccessProp.byI2CID = FT2_SENSOR_MODEL_ID_ADDR;
	g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_WA;
	g_wSensor = SENSOR_RS0509;
	g_bySensor_YuvMode = RAW_MODE;
	return;
#endif

#ifdef RTS58XX_SP_RS0551C
    g_SnrRegAccessProp.byI2CID = FT2_SENSOR_MODEL_ID_ADDR;
    g_SnrRegAccessProp.byMode_DataWidth = I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_WA;
    g_wSensor = SENSOR_RS0551C;
    g_bySensor_YuvMode = RAW_MODE;
    return;
#endif

	//return;
}

void Sensor_SoftRST(void)
{
	U8 const bySensorSignalPolarity =PIXEL_SAMPLE_RISING| HSYNC_ACTIVE_HIGH |VSYNC_ACTIVE_HIGH;

	switch(g_wSensor)
	{
#ifdef RTS58XX_SP_OV7725
	case SENSOR_OV7725:
#endif
#ifdef RTS58XX_SP_OV7740
	case SENSOR_OV7740:
#endif
#ifdef RTS58XX_SP_OV7670
	case SENSOR_OV7670:
#endif
#ifdef RTS58XX_SP_OV7675
	case SENSOR_OV7675:
#endif
#ifdef RTS58XX_SP_OV7690
	case SENSOR_OV7690:    //cski_20090327
#endif
#if (defined(RTS58XX_SP_OV7725) || defined(RTS58XX_SP_OV7740) ||defined(RTS58XX_SP_OV7670)||defined(RTS58XX_SP_OV7675) || defined(RTS58XX_SP_OV7690))
		// sensor software reset
		Write_SenReg(0x12, 0x80);
		WaitTimeOut_Delay(1);
		break;
#endif
#ifdef RTS58XX_SP_OV9663
	case SENSOR_OV9663:
		// reset PLL twice
		Write_SenReg(0x3E, 0xD0);
		WaitTimeOut_Delay(1);
		Write_SenReg(0x3E, 0xD0);
		WaitTimeOut_Delay(1);
		// hemonel 2009-08-05: move to Resetsensor func() for still image time
		Write_SenReg(0x12, 0x80);
		WaitTimeOut_Delay(1);
		break;
#endif
#ifdef RTS58XX_SP_OV9710
	case SENSOR_OV9710:
		// sensor software reset
		Write_SenReg(0x12, 0x80);
		WaitTimeOut_Delay(1);
		break;
#endif //RTS5820_SP_OV9710
#ifdef RTS58XX_SP_OV2650
	case SENSOR_OV2650:
		Write_SenReg(0x3012, 0x80);	// Sensor Reset
		WaitTimeOut_Delay(1);
		break;
#endif
#ifdef RTS58XX_SP_OV2680
	case SENSOR_OV2680:
		Write_SenReg(0x0103, 0x01);	// Sensor Reset
		WaitTimeOut_Delay(1);
		break;
#endif
#ifdef RTS58XX_SP_OV9715
	case SENSOR_OV9715:
		// sensor software reset
		Write_SenReg(0x12, 0x80);
		WaitTimeOut_Delay(1);
		break;
#endif //RTS5820_SP_OV9715
#ifdef RTS58XX_SP_MI0360
	case SENSOR_MI360:
		Write_SenReg(0x0107, 0x0001);
		Write_SenReg(0x0107, 0x0000); //reset IFP
		WaitTimeOut_Delay(1);
		Write_SenReg(0x0106, 0x700E); //enable AE, defect correction, aperture correction knee, CCM, AWB.
		Write_SenReg(0x013A, 0x0002); //swap Y U Y V
		Write_SenReg(0x040D, 0x0001);
		WaitTimeOut_Delay(1);
		Write_SenReg(0x040D, 0x0000);
		break;
#endif
#ifdef RTS58XX_SP_MI1320
	case SENSOR_MI1320:
		Write_SenReg(0x000D, 0x0009);// assert reset
		WaitTimeOut_Delay(1);
		Write_SenReg(0x000D, 0x0008); //deassert reset
		WaitTimeOut_Delay(1);
		break;
#endif
#ifdef RTS58XX_SP_MI1330
	case SENSOR_MI1330:
		/////////////////////
		//software reset.
		/////////////////////
		//		Write_SenReg(0x001a, 0x0011); //SoC reset assert, parallel_disable
		//		WaitTimeOut_Delay(5);
		//		Write_SenReg(0x001a, 0x0010); //SoC reset deassert

		/////////////////////
		//MCU  reset.
		/////////////////////
		Write_SenReg(0x001c, 0x0001);//MCU reset  # :->0x5500
		Write_SenReg(0x001c, 0x0000);//MCU reset  # :->0x5500
		Write_SenReg(0x0018, 0x0008);// SOC not standby
		//Write_SenReg(0x0016, 0x01ff);// enable ext clk
		Write_SenReg(0x0016, 0x02ff);// enable ext clk
		WaitTimeOut_Delay(15);//this delay is necessary,cheney.cai 2008-11-20

		Write_SenReg(0x321c, 0x0003);	//SOC,fifo bypass
		Write_SenReg(0x3210, 0x01b8);	//SOC,fifo bypass
		WaitTimeOut_Delay(1);//this delay is necessary
		bySensorSignalPolarity = PIXEL_SAMPLE_FALLING| HSYNC_ACTIVE_HIGH |VSYNC_ACTIVE_HIGH;
		break;
#endif
#ifdef RTS58XX_SP_MI2020
	case SENSOR_MI2020:
		Write_SenReg(0x301a,0x02cd);//software reset.
		WaitTimeOut_Delay(10);
		Write_SenReg(0x3202,0x0008);// disable stand-by mode, stop MCU on Power up
		Write_SenReg(0x301a,0x0a00);//paraller disable, stop driving pin, stop streaming, unlock registers

		Write_SenReg(0x3044, 0x0542);//from MI2020 Wizard AP
		Write_SenReg(0x3216,0x02cf); //from MI2020 Wizard AP
		Write_SenReg(0x3214,0x0EE6);// slew rate control
		WaitTimeOut_Delay(10);
		break;
#endif
#ifdef RTS58XX_SP_SIV100B
	case SENSOR_SIV100B:
		Write_SenReg(0x03, 0x02);//pwrdn en
		WaitTimeOut_Delay(1);
		Write_SenReg(0x03, 0x04);//pwrdn dis
		WaitTimeOut_Delay(1);
		break;
#endif

#ifdef RTS58XX_SP_SIW021A
	case SENSOR_SIW021A:
		Write_SenReg(0x00, 0x01);//pwrdn en
		Write_SenReg(0x03, 0x00);//pwrdn en
		WaitTimeOut_Delay(1);
		Write_SenReg(0x03, 0x01);//pwrdn dis
		WaitTimeOut_Delay(1);
		break;
#endif

#ifdef RTS58XX_SP_ST171
	case SENSOR_ST171:
		Write_SenReg(0x00, 0x01);//pwrdn en
		Write_SenReg(0x03, 0x00);//pwrdn en
		WaitTimeOut_Delay(1);
		Write_SenReg(0x03, 0x01);//pwrdn dis
		WaitTimeOut_Delay(1);
		break;
#endif
#if defined( RTS58XX_SP_YACBAA0S) || defined(RTS58XX_SP_YACBAC1S )
	case SENSOR_YACBAA0S:
	case SENSOR_YACBAC1S:
#ifdef RTS58XX_SP_YACC5A2S
	case SENSOR_YACC5A2S:
#endif
		Write_SenReg(HYNIX_PAGE_SEL_ADDR, 0x00); //page 0
		Write_SenReg_Mask(0x01, 0x02, 0x02);//software reset accert
		WaitTimeOut_Delay(1);
		Write_SenReg_Mask(0x01, 0x00, 0x02);//software reset de-accert

		//_____for PCB signal integration____JQG_20090805
		bySensorSignalPolarity = PIXEL_SAMPLE_FALLING| HSYNC_ACTIVE_HIGH |VSYNC_ACTIVE_HIGH;
		break;
#endif
#ifdef RTS58XX_SP_S5K6A1
	case SENSOR_S5K6A1:
		// sensor software reset
		Write_SenReg(0x0103, 0x1);
		WaitTimeOut_Delay(1);
		break;
#endif
	default:
		break;
	}

	//SetSnrpinPolarity(bySensorSignalPolarity);// jqg20100826_for optimize code
	XBYTE[CCS_CONTROL] = bySensorSignalPolarity;
}

void Sensor_POR()
{
	switch(g_wSensor)
	{
#ifdef RTS58XX_SP_YACC611
	case SENSOR_YACC611:
		YACC611_POR();
		break;
#endif
#ifdef RTS58XX_SP_OV9710
	case SENSOR_OV9710:
		OV9710_POR();
		break;
#endif
#ifdef RTS58XX_SP_OV9715
	case SENSOR_OV9715:
		OV9715_POR();
		break;
#endif
#ifdef RTS58XX_SP_OV9726
	case SENSOR_OV9726:
		OV9726_POR();
		break;
#endif
#ifdef RTS58XX_SP_OV9728
	case SENSOR_OV9728:
		OV9728_POR();
		break;
#endif
#ifdef RTS58XX_SP_JXH22
	case SENSOR_JXH22:
		JXH22_POR();
		break;
#endif
#ifdef RTS58XX_SP_YACY9A1
	case SENSOR_YACY9A1:
		YACY9A1_POR();
		break;
#endif
#ifdef RTS58XX_SP_YACD6A1C
	case SENSOR_YACD6A1C:
		YACD6A1C_POR();
		break;
#endif
#ifdef RTS58XX_SP_OV9724
	case SENSOR_OV9724:
		OV9724_POR();
		break;
#endif
#ifdef RTS58XX_SP_S5K6A1
	case SENSOR_S5K6A1:
		S5K6A1_POR();
		break;
#endif
#ifdef RTS58XX_SP_S5K8AA
	case SENSOR_S5K8AA:
		S5K8AA_POR();
		break;
#endif
#ifdef RTS58XX_SP_OV2710
	case SENSOR_OV2710:
		OV2710_POR();
		break;
#endif
#ifdef RTS58XX_SP_OV2680
	case SENSOR_OV2680:
		OV2680_POR();
		break;
#endif
#ifdef RTS58XX_SP_TOSHIBA
	case SENSOR_T4K71:
		T4K71_POR();
		break;
#endif
#ifdef RTS58XX_SP_OV3642
	case SENSOR_OV3642:
		OV3642_POR();
		break;
#endif
#ifdef RTS58XX_SP_OV3640
	case SENSOR_OV3640:
		OV3640_POR();
		break;
#endif

#ifdef RTS58XX_SP_OV2720
	case SENSOR_OV2720:
		OV2720_POR();
		break;
#endif
#ifdef RTS58XX_SP_OV2722
	case SENSOR_OV2722:
		OV2722_POR();
		break;
#endif

#ifdef RTS58XX_SP_OV5642
	case SENSOR_OV5642:
		OV5642_POR();
		break;
#endif
#ifdef RTS58XX_SP_OV5640
	case SENSOR_OV5640:
		OV5640_POR();
		break;
#endif
#ifdef RTS58XX_SP_MI1040
	case SENSOR_MI1040:
		MI1040_POR();
		break;
#endif
#ifdef RTS58XX_SP_YACC6A1S
	case SENSOR_YACC6A1S:
		YACC6A1S_POR();
		break;
#endif
#ifdef RTS58XX_SP_YACY6A1C9SBC
	case SENSOR_YACY6A1C9SBC:
		YACY6A1C9SBC_POR();
		break;
#endif
#ifdef RTS58XX_SP_IMX119PQH5
	case SENSOR_IMX119PQH5:
		IMX119PQH5_POR();
		break;
#endif

#ifdef RTS58XX_SP_IMX188PQ
	case SENSOR_IMX188PQ:
		IMX188PQ_POR();
		break;
#endif

#ifdef RTS58XX_SP_IMX132PQ
	case SENSOR_IMX132PQ:
		IMX132PQ_POR();
		break;
#endif

#ifdef RTS58XX_SP_IMX208PQH5
	case SENSOR_IMX208PQH5:
		IMX208PQH5_POR();
		break;
#endif

#ifdef RTS58XX_SP_IMX189PSH5
	case SENSOR_IMX189PSH5:
		IMX189PSH5_POR();
		break;
#endif

#ifdef RTS58XX_SP_IMX076LQZC
	case SENSOR_IMX076LQZC:
		IMX076LQZC_POR();
		break;
#endif

#ifdef RTS58XX_SP_GC1036
	case SENSOR_GC1036:
		GC1036_POR();
		break;
#endif

#ifdef RTS58XX_SP_GC1136
	case SENSOR_GC1136:
		GC1136_POR();
		break;
#endif
#ifdef  RTS58XX_SP_RS0551C
	case SENSOR_RS0551C:
	    RS0551C_POR();
           break;
#endif
	default:
		Sensor_POR_Common();
		break;
	}
}

void Reset_Sensor(void)
{
	Sensor_POR();
	Sensor_SoftRST();
}

void SetSensorFormatFps(U16 SetFormat,U8 Fps)
{
	g_fCurExpTime =0;	// hemonel 2010-04-08: add for raw mode AE exposure time initial

	switch(g_wSensor)
	{
#ifdef RTS58XX_SP_HM1055
	case SENSOR_HM1055:
		HM1055SetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_MI1040
	case SENSOR_MI1040:
		MI1040SetFormatFps(SetFormat,Fps);
		break;
#endif
#ifdef RTS58XX_SP_S5K6AAFX
	case SENSOR_S5K6AAFX:
		S5K6AASetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_OV7725
	case SENSOR_OV7725:
		OV7725SetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_OV7740
	case SENSOR_OV7740:
		OV7740SetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_OV7670
	case SENSOR_OV7670:
		OV7670SetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_OV7675
	case SENSOR_OV7675:
		OV7675SetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_OV7690
	case SENSOR_OV7690: //cski_2009_03_27
		OV7690SetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_OV9663
	case SENSOR_OV9663:
		OV9663SetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_OV9710
	case SENSOR_OV9710:
		OV9710SetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_OV9715
	case SENSOR_OV9715:
		OV9715SetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_OV9726
	case SENSOR_OV9726:
		OV9726SetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_JXH22
	case SENSOR_JXH22:
		JXH22SetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_OV9728
	case SENSOR_OV9728:
		OV9728SetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_OV9724
	case SENSOR_OV9724:
		OV9724SetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_OV2650
	case SENSOR_OV2650:
		OV2650SetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_OV2710
	case SENSOR_OV2710:
		OV2710SetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_OV2680
	case SENSOR_OV2680:
		OV2680SetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_T4K71
	case SENSOR_T4K71:
		T4K71SetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_OV2720
	case SENSOR_OV2720:
		OV2720SetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_OV2722
	case SENSOR_OV2722:
		OV2722SetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_OV3642
	case SENSOR_OV3642:
		OV3642SetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_OV3640
	case SENSOR_OV3640:
		SetOV3640Fps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_OV5642
	case SENSOR_OV5642:
		OV5642SetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_OV5640
	case SENSOR_OV5640:
		OV5640SetFormatFps(SetFormat, Fps);			
		break;
#endif
#ifdef RTS58XX_SP_MI0360
	case SENSOR_MI360:
		Mi360SetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_MI1320
	case SENSOR_MI1320:
		Mi1320SetFormatFps(SetFormat,Fps);
		break;
#endif
#ifdef RTS58XX_SP_MI1330
	case SENSOR_MI1330:
		Mi1330SetFormatFps(SetFormat,Fps);
		break;
#endif
#ifdef RTS58XX_SP_MI2020
	case SENSOR_MI2020:
		Mi2020SetFormatFps(SetFormat,Fps);
		break;
#endif
#ifdef RTS58XX_SP_SIV100B
	case SENSOR_SIV100B:
		SIV100BSetFormatFps(SetFormat,Fps);
		break;
#endif
#ifdef RTS58XX_SP_SIV120B
	case SENSOR_SIV120B:
		SIV120BSetFormatFps(SetFormat,Fps);
		break;
#endif
#ifdef RTS58XX_SP_SIV120D
	case SENSOR_SIV120D:
		SIV120DSetFormatFps(SetFormat,Fps);
		break;
#endif
#ifdef RTS58XX_SP_SIM120C
	case SENSOR_SIM120C:
		SIM120CSetFormatFps(SetFormat,Fps);
		break;
#endif
#ifdef RTS58XX_SP_SID130B
	case SENSOR_SID130B:
		SID130BSetFormatFps(SetFormat,Fps);
		break;
#endif
#ifdef RTS58XX_SP_SIW021A
	case SENSOR_SIW021A:
		SIW021ASetFormatFps(SetFormat,Fps);
		break;
#endif
#ifdef RTS58XX_SP_ST171
	case SENSOR_ST171:
		ST171SetFormatFps(SetFormat,Fps);
		break;
#endif
#ifdef RTS58XX_SP_GC0307
	case SENSOR_GC0307:
		GC0307SetFormatFps(SetFormat,Fps);
		break;
#endif
#ifdef RTS58XX_SP_GC1036
	case SENSOR_GC1036:
		GC1036SetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_GC1136
	case SENSOR_GC1136:
		GC1136SetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_YACBAA0S
	case SENSOR_YACBAA0S:
		YACBAA0SSetFormatFps(SetFormat,Fps);
		break;
#endif
#ifdef RTS58XX_SP_YACBAC1S
	case SENSOR_YACBAC1S:
		YACBAC1SSetFormatFps(SetFormat,Fps);
		break;
#endif
#ifdef RTS58XX_SP_YACC5A2S
	case SENSOR_YACC5A2S:
		YACC5A2SSetFormatFps(SetFormat,Fps);
		break;
#endif
#ifdef RTS58XX_SP_YACC6A1S
	case SENSOR_YACC6A1S:
		YACC6A1SSetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_YACC611
	case SENSOR_YACC611:
		YACC611SetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_YACY9A1
	case SENSOR_YACY9A1:
		YACY9A1SetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_YACD6A1C
	case SENSOR_YACD6A1C:
		YACD6A1CSetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_YACY6A1C9SBC
	case SENSOR_YACY6A1C9SBC:
		YACY6A1C9SBCSetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_S5K6A1
	case SENSOR_S5K6A1:
		S5K6A1SetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_S5K8AA
	case SENSOR_S5K8AA:
		S5K8AASetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_IMX119PQH5
	case SENSOR_IMX119PQH5:
		IMX119PQH5SetFormatFps(SetFormat, Fps);
		break;
#endif

#ifdef RTS58XX_SP_IMX188PQ
	case SENSOR_IMX188PQ:
		IMX188PQSetFormatFps(SetFormat, Fps);
		break;
#endif

#ifdef RTS58XX_SP_IMX132PQ
	case SENSOR_IMX132PQ:
		IMX132PQSetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_IMX208PQH5
	case SENSOR_IMX208PQH5:
		IMX208PQH5SetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_IMX189PSH5
	case SENSOR_IMX189PSH5:
		IMX189PSH5SetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_IMX076LQZC
	case SENSOR_IMX076LQZC:
		IMX076LQZCSetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_RS0509
	case SENSOR_RS0509:
		RS0509SetFormatFps(SetFormat, Fps);
		break;
#endif
#ifdef RTS58XX_SP_RS0551C
	case SENSOR_RS0551C:
		RS0551CSetFormatFps(SetFormat, Fps);
		break;
#endif

	case SENSOR_FT2_MODEL:
		FT2ModelSetFormatFps(Fps);
		break;
	default:
		break;
	}
}



void	SetSensorIntegrationTime(U32 dwSetValue)
{
	U16 wEspline;

	// calculate exposure time and dummy line
	wEspline = (U16)(dwSetValue*(g_dwPclk/10000) / g_wSensorHsyncWidth);

	switch(g_wSensor)
	{
#ifdef RTS58XX_SP_HM1055
	case SENSOR_HM1055:
		SetHM1055IntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_S5K6AAFX
	case SENSOR_S5K6AAFX:
		SetS5K6AAIntegrationTime(dwSetValue);
		break;
#endif
#ifdef RTS58XX_SP_OV7725
	case SENSOR_OV7725:
		SetOV7725IntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_OV7740
	case SENSOR_OV7740:
		SetOV7740IntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_OV7670
	case SENSOR_OV7670:
		SetOV7670IntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_OV9710
	case SENSOR_OV9710:
		SetOV9710IntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_OV9715
	case SENSOR_OV9715:
		SetOV9715IntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_OV9726
	case SENSOR_OV9726:
		SetOV9726IntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_JXH22
	case SENSOR_JXH22:
		SetJXH22IntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_OV9728
	case SENSOR_OV9728:
		SetOV9728IntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_OV9724
	case SENSOR_OV9724:
		SetOV9724IntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_OV7675
	case SENSOR_OV7675:
		SetOV7675IntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_OV7690
	case SENSOR_OV7690:
		SetOV7690IntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_OV9663
	case SENSOR_OV9663:
		SetOV9663IntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_OV2650
	case SENSOR_OV2650:
		SetOV2650IntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_OV2710
	case SENSOR_OV2710:
		SetOV2710IntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_OV2680
	case SENSOR_OV2680:
		SetOV2680IntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_T4K71
	case SENSOR_T4K71:
		SetT4K71IntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_OV2720
	case SENSOR_OV2720:
		SetOV2720IntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_OV2722
	case SENSOR_OV2722:
		SetOV2722IntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_OV5642
	case SENSOR_OV5642:
		SetOV5642IntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_OV5640
	case SENSOR_OV5640:
		SetOV5640IntegrationTime(wEspline);
		break;
#endif	
#ifdef RTS58XX_SP_MI1320
	case SENSOR_MI1320:
		SetMI1320IntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_MI0360
	case SENSOR_MI360:
		SetMI360IntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_SIV120B
	case SENSOR_SIV120B:
		SetSIV120BIntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_SIV120D
	case SENSOR_SIV120D:
		SetSIV120DIntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_SIM120C
	case SENSOR_SIM120C:
		SetSIM120CIntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_SID130B
	case SENSOR_SID130B:
		SetSID130BIntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_SIV100B
	case SENSOR_SIV100B:
		SetSIV100BIntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_SIW021A
	case SENSOR_SIW021A:
		SetSIW021AIntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_ST171
	case SENSOR_ST171:
		SetST171IntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_GC0307
	case SENSOR_GC0307:
		SetGC0307IntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_GC1036
	case SENSOR_GC1036:
		SetGC1036IntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_GC1136
	case SENSOR_GC1136:
		SetGC1136IntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_YACBAA0S
	case SENSOR_YACBAA0S:
		SetYACBAA0SIntegrationTime(dwSetValue);
		break;
#endif
#ifdef RTS58XX_SP_YACBAC1S
	case SENSOR_YACBAC1S:
		SetYACBAC1SIntegrationTime(dwSetValue);
		break;
#endif
#ifdef RTS58XX_SP_YACC5A2S
	case SENSOR_YACC5A2S:
		SetYACC5A2SIntegrationTime(dwSetValue);
		break;
#endif
#ifdef RTS58XX_SP_YACC6A1S
	case SENSOR_YACC6A1S:
		SetYACC6A1SIntegrationTime(dwSetValue);
		break;
#endif
#ifdef RTS58XX_SP_YACC611
	case SENSOR_YACC611:
		SetYACC611IntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_YACY9A1
	case SENSOR_YACY9A1:
		SetYACY9A1IntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_YACD6A1C
	case SENSOR_YACD6A1C:
		SetYACD6A1CIntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_YACY6A1C9SBC
	case SENSOR_YACY6A1C9SBC:
		SetYACY6A1C9SBCIntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_S5K6A1
	case SENSOR_S5K6A1:
		SetS5K6A1IntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_S5K8AA
	case SENSOR_S5K8AA:
		SetS5K8AAIntegrationTime(dwSetValue);
		break;
#endif
#ifdef RTS58XX_SP_MI1040
	case SENSOR_MI1040:
		SetMI1040IntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_IMX119PQH5
	case SENSOR_IMX119PQH5:
		SetIMX119PQH5IntegrationTime(wEspline);
		break;
#endif

#ifdef RTS58XX_SP_IMX188PQ
	case SENSOR_IMX188PQ:
		SetIMX188PQIntegrationTime(wEspline);
		break;
#endif


#ifdef RTS58XX_SP_IMX132PQ
	case SENSOR_IMX132PQ:
		SetIMX132PQIntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_IMX208PQH5
	case SENSOR_IMX208PQH5:
		SetIMX208PQH5IntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_IMX189PSH5
	case SENSOR_IMX189PSH5:
		SetIMX189PSH5IntegrationTime(wEspline);
		break;
#endif
#ifdef RTS58XX_SP_IMX076LQZC
	case SENSOR_IMX076LQZC:
		SetIMX076LQZCIntegrationTime(wEspline);
		break;
#endif
	default:
		break;
	}
}

void SetSensorIntegrationTimeAuto(U8 bySetValue)
{
	if (g_bySensor_YuvMode==RAW_MODE)
	{
		if(bySetValue==EXPOSURE_TIME_AUTO_MOD_MANUAL)
		{
			SetDynamicISP(0);
#ifdef _RTK_EXTENDED_CTL_
			SetSensorGain(g_fAEC_ISO_gain_min);
#endif//#ifdef _RTK_EXTENDED_CTL_
		}
		else
		{
			g_fCurExpTime=0;	// hemonel 2010-04-08: add for raw mode AE exposure time initial
			SetExpTime_Gain(g_fAEC_EtGain);
			g_fAEC_SetEtgain = g_fAEC_EtGain;
			SetDynamicISP(ClipWord((U16)(g_fAEC_Gain*16.0),0,255));
		}
	}
	else
	{
		switch(g_wSensor)
		{
#ifdef RTS58XX_SP_S5K6AAFX
		case SENSOR_S5K6AAFX:
			SetS5K6AAIntegrationTimeAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_OV7725
		case SENSOR_OV7725:
			SetOV7725IntegrationTimeAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_OV7740
		case SENSOR_OV7740:
			SetOV7740IntegrationTimeAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_OV7670
		case SENSOR_OV7670:
			SetOV7670IntegrationTimeAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_OV7675
		case SENSOR_OV7675:
			SetOV7675IntegrationTimeAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_OV7690
		case SENSOR_OV7690:
			SetOV7690IntegrationTimeAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_OV9663
		case SENSOR_OV9663:
			SetOV9663IntegrationTimeAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_OV2650
		case SENSOR_OV2650:
			SetOV2650IntegrationTimeAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_OV5642
		case SENSOR_OV5642:
			SetOV5642IntegrationTimeAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_OV5640
		case SENSOR_OV5640:
			SetOV5640IntegrationTimeAuto(bySetValue);
			break;
#endif	
#ifdef RTS58XX_SP_MI1320
		case SENSOR_MI1320:
			SetMI1320IntegrationTimeAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_MI0360
		case SENSOR_MI360:
			SetMI360IntegrationTimeAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_SIV120B
		case SENSOR_SIV120B:
			SetSIV120BIntegrationTimeAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_SIV120D
		case SENSOR_SIV120D:
			SetSIV120DIntegrationTimeAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_SIM120C
		case SENSOR_SIM120C:
			SetSIM120CIntegrationTimeAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_SID130B
		case SENSOR_SID130B:
			SetSID130BIntegrationTimeAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_SIV100B
		case SENSOR_SIV100B:
			SetSIV100BIntegrationTimeAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_GC0307
		case SENSOR_GC0307:
			SetGC0307IntegrationTimeAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_YACBAA0S
		case SENSOR_YACBAA0S:
			SetYACBAA0SIntegrationTimeAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_YACBAC1S
		case SENSOR_YACBAC1S:
			SetYACBAC1SIntegrationTimeAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_YACC5A2S
		case SENSOR_YACC5A2S:
			SetYACC5A2SIntegrationTimeAuto(bySetValue);
			break;
#endif
		default:
			break;
		}
	}
}

#ifdef _RTK_EXTENDED_CTL_
void SetSensorGain(float fGain)
{
#ifdef RTS58XX_SP_HM1055
    SetHM1055Gain(fGain);
#elif defined RTS58XX_SP_S5K6AAFX
    SetS5K6AAGain(fGain);
#elif defined RTS58XX_SP_OV9772
    SetOV9772Gain(fGain);
#elif defined  RTS58XX_SP_OV7725
    SetOV7725Gain(fGain);
#elif defined  RTS58XX_SP_OV7740
    SetOV7740Gain(fGain);
#elif defined  RTS58XX_SP_OV7670
    SetOV7670Gain(fGain);
#elif defined  RTS58XX_SP_OV9710
    SetOV9710Gain(fGain);
#elif defined  RTS58XX_SP_OV9726
    SetOV9726Gain(fGain);
#elif defined  RTS58XX_SP_OV9728
    SetOV9728Gain(fGain);
#elif defined  RTS58XX_SP_OV9724
    SetOV9724Gain(fGain);
#elif defined  RTS58XX_SP_OV7675
    SetOV7675Gain(fGain);
#elif defined  RTS58XX_SP_OV7690
    SetOV7690Gain(fGain);
#elif defined  RTS58XX_SP_OV9663
    SetOV9663Gain(fGain);
#elif defined  RTS58XX_SP_OV2650
    SetOV2650Gain(fGain);
#elif defined  RTS58XX_SP_OV2710
    SetOV2710Gain(fGain);
#elif defined  RTS58XX_SP_OV2680
    SetOV2680Gain(fGain);    
#elif defined  RTS58XX_SP_OV2720
    SetOV2720Gain(fGain);
#elif defined  RTS58XX_SP_OV2722
    SetOV2722Gain(fGain);
#elif defined RTS58XX_SP_OV5642
    SetOV5642Gain(fGain);
#elif defined RTS58XX_SP_OV5640
    SetOV5640Gain(fGain);
#elif defined  RTS58XX_SP_JXH22
    SetJXH22Gain(fGain);
#elif defined	RTS58XX_SP_MI1320
    SetMI1320Gain(fGain);
#elif defined RTS58XX_SP_MI0360
    SetMI360Gain(fGain);
#elif defined RTS58XX_SP_SIV120B
    SetSIV120BGain(fGain);
#elif defined RTS58XX_SP_SIV120D
    SetSIV120DGain(fGain);
#elif defined RTS58XX_SP_SIM120C
    SetSIM120CGain(fGain);
#elif defined RTS58XX_SP_SID130B
    SetSID130BGain(fGain);
#elif defined RTS58XX_SP_SIV100B
    SetSIV100BGain(fGain);
#elif defined RTS58XX_SP_SIW021A
    SetSIW021AGain(fGain);
#elif defined RTS58XX_SP_ST171
    SetST171Gain(fGain);
#elif defined RTS58XX_SP_GC0307
    SetGC0307Gain(fGain);
#elif defined RTS58XX_SP_YACBAA0S
    SetYACBAA0SGain(fGain);
#elif defined RTS58XX_SP_YACBAC1S
    SetYACBAC1SGain(fGain);
#elif defined RTS58XX_SP_YACC5A2S
    SetYACC5A2SGain(fGain);
#elif defined RTS58XX_SP_YACC6A1S
    SetYACC6A1SGain(fGain);
#elif defined RTS58XX_SP_YACC611
    SetYACC611Gain(fGain);
#elif defined RTS58XX_SP_YACY9A1
    SetYACY9A1Gain(fGain);
#elif defined RTS58XX_SP_YACD6A1C
    SetYACD6A1CGain(fGain);
#elif defined  RTS58XX_SP_YACY6A1C9SBC
    SetYACY6A1C9SBCGain(fGain);
#elif defined RTS58XX_SP_S5K6A1
    SetS5K6A1Gain(fGain);
#elif defined RTS58XX_SP_S5K8AA
    SetS5K8AAGain(fGain);
#elif defined RTS58XX_SP_MI1040
    SetMI1040Gain(fGain);
#elif defined RTS58XX_SP_IMX119PQH5
    SetIMX119PQH5Gain(fGain);
#elif defined RTS58XX_SP_IMX188PQ
    SetIMX188PQGain(fGain);
#elif defined RTS58XX_SP_IMX132PQ
    SetIMX132PQGain(fGain);
#endif
}
#endif//#ifdef _RTK_EXTENDED_CTL_

void SetSensorLowLightComp(U8 bySetValue)
{
	bySetValue = bySetValue;

	switch(g_wSensor)
	{
		/*
		#ifdef RTS58XX_SP_S5K6AAFX
		case SENSOR_S5K6AAFX:
		//			SetS5K6AALowLightComp(bySetValue);
			break;
		#endif
		#ifdef RTS58XX_SP_OV7725
		case SENSOR_OV7725:
		//			SetOV7725LowLightComp(bySetValue);
			break;
		#endif
		#ifdef RTS58XX_SP_OV7740
		case SENSOR_OV7740:
		//			SetOV7740LowLightComp(bySetValue);
			break;
		#endif
		#ifdef RTS58XX_SP_OV7670
		case SENSOR_OV7670:
		//			SetOV7670LowLightComp(bySetValue);
			break;
		#endif
		#ifdef RTS58XX_SP_OV7675
		       case SENSOR_OV7675:
		//	            SetOV7675LowLightComp(bySetValue);
		           break;
		#endif
		#ifdef RTS58XX_SP_OV7690
		case SENSOR_OV7690:
		//			SetOV7690LowLightComp(bySetValue);
			break;
		#endif
		#ifdef RTS58XX_SP_OV9663
		case SENSOR_OV9663:
		//			SetOV9663LowLightComp(bySetValue);
			break;
		#endif
		*/

#ifdef RTS58XX_SP_OV2650
	case SENSOR_OV2650:
		SetOV2650LowLightComp(bySetValue);
		break;
#endif
#ifdef RTS58XX_SP_OV5640
	case SENSOR_OV5640:
		SetOV5640LowLightComp(bySetValue);
		break;
#endif	
		/*
		#ifdef RTS58XX_SP_MI2020
				case SENSOR_MI2020:
		//			SetMi2020LowLightComp(bySetValue);
					break;
		#endif
		#ifdef RTS58XX_SP_MI1330
				case SENSOR_MI1330:
		//			SetMi1330LowLightComp(bySetValue);
					break;
		#endif
		#ifdef RTS58XX_SP_MI1320
				case SENSOR_MI1320:
		//			SetMi1320LowLightComp(bySetValue);
					break;
		#endif
		#ifdef RTS58XX_SP_MI0360
				case SENSOR_MI360:
		//			SetMi360LowLightComp(bySetValue);
					break;
		#endif
		#ifdef RTS58XX_SP_SIV120B
				case SENSOR_SIV120B:
		//			SetSIV120BLowLightComp(bySetValue);
					break;
		#endif
		#ifdef RTS58XX_SP_SIM120C
				case SENSOR_SIM120C:
		//			SetSIM120CLowLightComp(bySetValue);
					break;
		#endif
		#ifdef RTS58XX_SP_SID130B
				case SENSOR_SID130B:
		//			SetSID130BLowLightComp(bySetValue);
					break;
		#endif
		#ifdef RTS58XX_SP_SIV100B
				case SENSOR_SIV100B:
		//			SetSIV100BLowLightComp(bySetValue);
					break;
		#endif
		#ifdef RTS58XX_SP_GC0307
				case SENSOR_GC0307:
		//			SetGC0307LowLightComp(bySetValue);
					break;
		#endif
		#ifdef RTS58XX_SP_YACBAA0S
				case SENSOR_YACBAA0S:
		//			SetYACBAA0SLowLightComp(bySetValue);
					break;
		#endif
		#ifdef RTS58XX_SP_YACC5A2S
				case SENSOR_YACC5A2S:
		//			SetYACC5A2SLowLightComp(bySetValue);
					break;
		#endif
		*/
	default:
		break;
	}
}

#ifndef _USE_BK_HSBC_ADJ_
void SetSensorBrightness(S8 sbySetValue)
{
    switch(g_wSensor)
    {
#ifdef RTS58XX_SP_OV5640
    case SENSOR_OV5640:
        SetOV5640Brightness(sbySetValue);
        break;
#endif
    default:
        break;
    }
}

void SetSensorContrast(U8 bySetValue)
{
    switch(g_wSensor)
    {
#ifdef RTS58XX_SP_OV5640
    case SENSOR_OV5640:
        SetOV5640Contrast(bySetValue);
        break;
#endif
    default:
        break;
    }
}

void SetSensorSaturation(U8 bySetValue)
{
    switch(g_wSensor)
    {
#ifdef RTS58XX_SP_OV5640
    case SENSOR_OV5640:
        SetOV5640Saturation(bySetValue);
        break;
#endif
    default:
        break;
    }
}

void SetSensorHue (U16 swSetValue)
{
    switch(g_wSensor)
    {
#ifdef RTS58XX_SP_OV5640
    case SENSOR_OV5640:
        SetOV5640Hue(swSetValue);
        break;
#endif
    default:
        break;
    }
}
#endif //_USE_BK_HSBC_ADJ_

void	SetSensorSharpness(U8 bySetValue)
{
	bySetValue = bySetValue;  // remove warning
	switch(g_wSensor)
	{
#ifdef RTS58XX_SP_S5K6AAFX
	case SENSOR_S5K6AAFX:
		SetS5K6AASharpness(bySetValue);
		break;
#endif
#ifdef RTS58XX_SP_OV7725
	case SENSOR_OV7725:
		SetOV7725Sharpness(bySetValue);
		break;
#endif
#ifdef RTS58XX_SP_OV7740
	case SENSOR_OV7740:
		SetOV7740Sharpness(bySetValue);
		break;
#endif
#ifdef RTS58XX_SP_OV7670
	case SENSOR_OV7670:
		SetOV7670Sharpness(bySetValue);
		break;
#endif
#ifdef RTS58XX_SP_OV7675
	case SENSOR_OV7675:
		SetOV7675Sharpness(bySetValue);
		break;
#endif
#ifdef RTS58XX_SP_OV7690
	case SENSOR_OV7690:
		SetOV7690Sharpness(bySetValue);
		break;
#endif
#ifdef RTS58XX_SP_OV9663
	case SENSOR_OV9663:
		SetOV9663Sharpness(bySetValue);
		break;
#endif
#ifdef RTS58XX_SP_OV2650
	case SENSOR_OV2650:
		SetOV2650Sharpness(bySetValue);
		break;
#endif
#ifdef RTS58XX_SP_OV5642
	case SENSOR_OV5642:
		SetOV5642Sharpness(bySetValue);
		break;
#endif
#ifdef RTS58XX_SP_OV5640
    case SENSOR_OV5640:
    	SetOV5640Sharpness(bySetValue);
		break;
#endif
#ifdef RTS58XX_SP_MI1320
	case SENSOR_MI1320:
		SetMI1320Sharpness(bySetValue);
		break;
#endif
#ifdef RTS58XX_SP_MI0360
	case SENSOR_MI360:
		SetMI360Sharpness(bySetValue);
		break;
#endif
#ifdef RTS58XX_SP_SIV120B
	case SENSOR_SIV120B:
		SetSIV120BSharpness(bySetValue);
		break;
#endif
#ifdef RTS58XX_SP_SIV120D
	case SENSOR_SIV120D:
		SetSIV120DSharpness(bySetValue);
		break;
#endif
#ifdef RTS58XX_SP_SIM120C
	case SENSOR_SIM120C:
		SetSIM120CSharpness(bySetValue);
		break;
#endif
#ifdef RTS58XX_SP_SID130B
	case SENSOR_SID130B:
		SetSID130BSharpness(bySetValue);
		break;
#endif
#ifdef RTS58XX_SP_SIV100B
	case SENSOR_SIV100B:
		SetSIV100BSharpness(bySetValue);
		break;
#endif
#ifdef RTS58XX_SP_GC0307
	case SENSOR_GC0307:
		SetGC0307Sharpness(bySetValue);
		break;
#endif
#ifdef RTS58XX_SP_YACBAA0S
	case SENSOR_YACBAA0S:
		SetYACBAA0SSharpness(bySetValue);
		break;
#endif
#ifdef RTS58XX_SP_YACBAC1S
	case SENSOR_YACBAC1S:
		SetYACBAC1SSharpness(bySetValue);
		break;
#endif
#ifdef RTS58XX_SP_YACC5A2S
	case SENSOR_YACC5A2S:
		SetYACC5A2SSharpness(bySetValue);
		break;
#endif
	default:
		break;
	}

}


void SetSensorBackLightComp(U8 bySetValue)
{
	// hemonel 2010-04-08: raw sensor do nothing at here, AE target will auto change
	if (g_bySensor_YuvMode==RAW_MODE)
	{
		if(BackLightCompItem.Last != bySetValue)
		{
			g_byAEForce_Adjust = 1;
		}
	}
	else
	{
		switch(g_wSensor)
		{
#ifdef RTS58XX_SP_S5K6AAFX
		case SENSOR_S5K6AAFX:
			SetS5K6AABackLightComp(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_OV7725
		case SENSOR_OV7725:
			SetOV7725BackLightComp(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_OV7740
		case SENSOR_OV7740:
			SetOV7740BackLightComp(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_OV7690
		case SENSOR_OV7690:
			SetOV7690BackLightComp(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_OV7670
		case SENSOR_OV7670:
			SetOV7670BackLightComp(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_OV7675
		case SENSOR_OV7675:
			SetOV7675BackLightComp(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_OV9663
		case SENSOR_OV9663:
			SetOV9663BackLightComp(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_OV2650
		case SENSOR_OV2650:
			SetOV2650BackLightComp(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_OV5642
		case SENSOR_OV5642:
			SetOV5642BackLightComp(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_OV5640
		case SENSOR_OV5640:
			SetOV5640BackLightComp(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_MI2020
		case SENSOR_MI2020:
			SetMi2020BackLightComp(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_MI1330
		case SENSOR_MI1330:
			SetMi1330BackLightComp(bySetValue);
			break;
#endif

#ifdef RTS58XX_SP_MI0360
		case SENSOR_MI360:
			SetMi360BackLightComp(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_MI1320
		case SENSOR_MI1320:
			SetMi1320BackLightComp(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_SIV120B
		case SENSOR_SIV120B:
			SetSIV120BBackLightComp(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_SIV120D
		case SENSOR_SIV120D:
			SetSIV120DBackLightComp(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_SIM120C
		case SENSOR_SIM120C:
			SetSIM120CBackLightComp(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_SID130B
		case SENSOR_SID130B:
			SetSID130BBackLightComp(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_SIV100B
		case SENSOR_SIV100B:
			SetSIV100BBackLightComp(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_GC0307
		case SENSOR_GC0307:
			SetGC0307BackLightComp(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_YACBAA0S
		case SENSOR_YACBAA0S:
			SetYACBAA0SBackLightComp(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_YACBAC1S
		case SENSOR_YACBAC1S:
			SetYACBAC1SBackLightComp(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_YACC5A2S
		case SENSOR_YACC5A2S:
			SetYACC5A2SBackLightComp(bySetValue);
			break;
#endif
		default:
			break;
		}
	}
}

void 	SetSensorPwrLineFreq(U8 byFps, U8 bySetValue)
{
	byFps = byFps;
	if(g_bySensor_YuvMode == RAW_MODE)
	{
		// hemonel 2010-03-15:  AE force to adjust
		if(PwrLineFreqItem.Last != bySetValue)
		{
			g_byAEForce_Adjust = 1;
		}
	}
	else
	{
		switch(g_wSensor)
		{
#ifdef RTS58XX_SP_S5K6AAFX
		case SENSOR_S5K6AAFX:
			SetS5K6AABandingFilter(byFps, bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_OV7725
		case SENSOR_OV7725:
			SetOV7725PwrLineFreq(byFps, bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_OV7740
		case SENSOR_OV7740:
			SetOV7740PwrLineFreq(byFps, bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_OV7670
		case SENSOR_OV7670:
			SetOV7670PwrLineFreq(byFps, bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_OV7675
		case SENSOR_OV7675:
			SetOV7675PwrLineFreq(byFps, bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_OV7690
		case SENSOR_OV7690:
			SetOV7690PwrLineFreq(byFps, bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_OV9663
		case SENSOR_OV9663:
			SetOV9663PwrLineFreq(byFps, bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_OV2650
		case SENSOR_OV2650:
			SetOV2650PwrLineFreq(byFps, bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_OV5642
		case SENSOR_OV5642:
			SetOV5642PwrLineFreq(byFps, bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_OV5640
		case SENSOR_OV5640:
			SetOV5640PwrLineFreq(byFps, bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_MI2020
		case SENSOR_MI2020:
			SetMi2020PwrLineFreq(byFps, bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_MI1330
		case SENSOR_MI1330:
			SetMi1330PwrLineFreq(byFps, bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_MI1320
		case SENSOR_MI1320:
			SetMi1320PwrLineFreq(byFps, bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_MI0360
		case SENSOR_MI360:
			SetMi360PwrLineFreq(byFps, bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_SIV120B
		case SENSOR_SIV120B:
			SetSIV120BPwrLineFreq(byFps, bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_SIV120D
		case SENSOR_SIV120D:
			SetSIV120DPwrLineFreq(byFps, bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_SIM120C
		case SENSOR_SIM120C:
			SetSIM120CPwrLineFreq(byFps, bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_SID130B
		case SENSOR_SID130B:
			SetSID130BPwrLineFreq(byFps, bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_SIV100B
		case SENSOR_SIV100B:
			SetSIV100BPwrLineFreq(byFps, bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_GC0307
		case SENSOR_GC0307:
			SetGC0307PwrLineFreq(byFps, bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_YACBAA0S
		case SENSOR_YACBAA0S:
			SetYACBAA0SPwrLineFreq(byFps, bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_YACBAC1S
		case SENSOR_YACBAC1S:
			SetYACBAC1SPwrLineFreq(byFps, bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_YACC5A2S
		case SENSOR_YACC5A2S:
			SetYACC5A2SPwrLineFreq(byFps, bySetValue);
			break;
#endif
		default:
			break;
		}
	}
}

void	SetSensorWBTemp(U16 wSetValue)
{
	OV_CTT_t ctt;
	U16 wRgain;
	U16 wGgain;
	U16 wBgain;

	// hemonel 2010-06-03 optimize code
	ctt = OV_SetColorTemperature(wSetValue);
	wRgain = ctt.wRgain;
	wGgain= ctt.wGgain;
	wBgain = ctt.wBgain;

	if (g_bySensor_YuvMode == RAW_MODE)
	{
		SetBKAWBGain(wRgain, wGgain, wBgain, 0);
	}
	else
	{
		switch(g_wSensor)
		{
#ifdef RTS58XX_SP_S5K6AAFX
		case SENSOR_S5K6AAFX:
			SetS5K6AAWBTemp(wRgain, wGgain, wBgain);
			break;
#endif
#ifdef RTS58XX_SP_OV7725
		case SENSOR_OV7725:
			SetOV7725WBTemp(wRgain, wGgain, wBgain);
			break;
#endif
#ifdef RTS58XX_SP_OV7740
		case SENSOR_OV7740:
			SetOV7740WBTemp(wRgain, wGgain, wBgain);
			break;
#endif
#ifdef RTS58XX_SP_OV7670
		case SENSOR_OV7670:
			SetOV7670WBTemp(wRgain, wGgain, wBgain);
			break;
#endif
#ifdef RTS58XX_SP_OV7675
		case SENSOR_OV7675:
			SetOV7675WBTemp(wRgain, wGgain, wBgain);
			break;
#endif
#ifdef RTS58XX_SP_OV7690
		case SENSOR_OV7690:
			SetOV7690WBTemp(wRgain, wGgain, wBgain);
			break;
#endif
#ifdef RTS58XX_SP_OV9663
		case SENSOR_OV9663:
			SetOV9663WBTemp(wRgain, wGgain, wBgain);
			break;
#endif
#ifdef RTS58XX_SP_OV2650
		case SENSOR_OV2650:
			SetOV2650WBTemp(wRgain, wGgain, wBgain);
			break;
#endif
#ifdef RTS58XX_SP_OV5642
		case SENSOR_OV5642:
			SetOV5642WBTemp(wRgain, wGgain, wBgain);
			break;
#endif
#ifdef RTS58XX_SP_OV5640
		case SENSOR_OV5640:
			SetOV5640WBTemp(wRgain, wGgain, wBgain);	
			break;
#endif
#ifdef RTS58XX_SP_MI0360
		case SENSOR_MI360:
			SetMi360WBTemp(wRgain, wGgain, wBgain);
			break;
#endif
#ifdef RTS58XX_SP_MI1330
		case SENSOR_MI1330:
			SetMi1330WBTemp(wRgain, wGgain, wBgain);
			break;
#endif
#ifdef RTS58XX_SP_MI2020
		case SENSOR_MI2020:
			SetMi2020WBTemp(wRgain, wGgain, wBgain);
			break;
#endif
#ifdef RTS58XX_SP_SIV120B
		case SENSOR_SIV120B:
			SetSIV120BWBTemp(wRgain, wGgain, wBgain);
			break;
#endif
#ifdef RTS58XX_SP_SIV120D
		case SENSOR_SIV120D:
			SetSIV120DWBTemp(wRgain, wGgain, wBgain);
			break;
#endif
#ifdef RTS58XX_SP_SIM120C
		case SENSOR_SIM120C:
			SetSIM120CWBTemp(wRgain, wGgain, wBgain);
			break;
#endif
#ifdef RTS58XX_SP_SID130B
		case SENSOR_SID130B:
			SetSID130BWBTemp(wRgain, wGgain, wBgain);
			break;
#endif
#ifdef RTS58XX_SP_SIV100B
		case SENSOR_SIV100B:
			SetSIV100BWBTemp(wRgain, wGgain, wBgain);
			break;
#endif
#ifdef RTS58XX_SP_GC0307
		case SENSOR_GC0307:
			SetGC0307WBTemp(wRgain, wGgain, wBgain);
			break;
#endif
#ifdef RTS58XX_SP_YACBAA0S
		case SENSOR_YACBAA0S:
			SetYACBAA0SWBTemp(wRgain, wGgain, wBgain);
			break;
#endif
#ifdef RTS58XX_SP_YACBAC1S
		case SENSOR_YACBAC1S:
			SetYACBAC1SWBTemp(wRgain, wGgain, wBgain);
			break;
#endif
#ifdef RTS58XX_SP_YACC5A2S
		case SENSOR_YACC5A2S:
			SetYACC5A2SWBTemp(wRgain, wGgain, wBgain);
			break;
#endif
		default:
			break;
		}
	}
}

void	SetSensorWBTempAuto(U8 bySetValue)
{
	if (g_bySensor_YuvMode == RAW_MODE)
	{
		if(bySetValue==1)
		{
			SetBKAWBGain(g_wAWBRGain_Last, g_wAWBGGain_Last, g_wAWBBGain_Last, 0);
		}
	}
	else
	{
		switch(g_wSensor)
		{
#ifdef RTS58XX_SP_S5K6AAFX
		case SENSOR_S5K6AAFX:
			SetS5K6AAWBTempAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_OV7725
		case SENSOR_OV7725:
			SetOV7725WBTempAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_OV7740
		case SENSOR_OV7740:
			SetOV7740WBTempAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_OV7670
		case SENSOR_OV7670:
			SetOV7670WBTempAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_OV7675
		case SENSOR_OV7675:
			SetOV7675WBTempAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_OV7690
		case SENSOR_OV7690:
			SetOV7690WBTempAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_OV9663
		case SENSOR_OV9663:
			SetOV9663WBTempAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_OV2650
		case SENSOR_OV2650:
			SetOV2650WBTempAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_OV5642
		case SENSOR_OV5642:
			SetOV5642WBTempAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_OV5640
		case SENSOR_OV5640:
			SetOV5640WBTempAuto(bySetValue);	
			break;
#endif
#ifdef RTS58XX_SP_MI0360
		case SENSOR_MI360:
			SetMi360WBTempAuto( bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_MI1330
		case SENSOR_MI1330:
			SetMi1330WBTempAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_MI2020
		case SENSOR_MI2020:
			SetMi2020WBTempAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_SIV120B
		case SENSOR_SIV120B:
			SetSIV120BWBTempAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_SIV120D
		case SENSOR_SIV120D:
			SetSIV120DWBTempAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_SIM120C
		case SENSOR_SIM120C:
			SetSIM120CWBTempAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_SID130B
		case SENSOR_SID130B:
			SetSID130BWBTempAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_SIV100B
		case SENSOR_SIV100B:
			SetSIV100BWBTempAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_GC0307
		case SENSOR_GC0307:
			SetGC0307WBTempAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_YACBAA0S
		case SENSOR_YACBAA0S:
			SetYACBAA0SWBTempAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_YACBAC1S
		case SENSOR_YACBAC1S:
			SetYACBAC1SWBTempAuto(bySetValue);
			break;
#endif
#ifdef RTS58XX_SP_YACC5A2S
		case SENSOR_YACC5A2S:
			SetYACC5A2SWBTempAuto(bySetValue);
			break;
#endif
		default:
			break;
		}
	}
}

void	SetSensorImgDir(U8 bySnrImgDir)
{
	switch(g_wSensor)
	{
#ifdef RTS58XX_SP_HM1055
	case SENSOR_HM1055:
		SetHM1055ImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_S5K6AAFX
	case SENSOR_S5K6AAFX:
		SetS5K6AAImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_OV7725
	case SENSOR_OV7725:
		SetOV7725ImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_OV7740
	case SENSOR_OV7740:
		SetOV7740ImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_OV7670
	case SENSOR_OV7670:
		SetOV7670ImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_OV7675
	case SENSOR_OV7675:
		SetOV7675ImgDir(bySnrImgDir);   //flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_OV7690
	case SENSOR_OV7690:
		SetOV7690ImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_OV9663
	case SENSOR_OV9663:
		SetOV9663ImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_OV9710
	case SENSOR_OV9710:
		SetOV9710ImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_OV9715
	case SENSOR_OV9715:
		SetOV9715ImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_OV9726
	case SENSOR_OV9726:
		SetOV9726ImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_OV9728
	case SENSOR_OV9728:
		SetOV9728ImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_OV9724
	case SENSOR_OV9724:
		SetOV9724ImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_OV2650
	case SENSOR_OV2650:
		SetOV2650ImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_OV2710
	case SENSOR_OV2710:
		SetOV2710ImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_OV2680
	case SENSOR_OV2680:
		SetOV2680ImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_T4K71
	case SENSOR_T4K71:
		SetT4K71ImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_JXH22
	case SENSOR_JXH22:
		SetJXH22ImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_OV2720
	case SENSOR_OV2720:
		SetOV2720ImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_OV2722
	case SENSOR_OV2722:
		SetOV2722ImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_OV5642
	case SENSOR_OV5642:
		SetOV5642ImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_OV5640
	case SENSOR_OV5640:
		SetOV5640ImgDir(bySnrImgDir);	
		break;
#endif
#ifdef RTS58XX_SP_MI2020
	case SENSOR_MI2020:
		SetMi2020ImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_MI1330
	case SENSOR_MI1330:
		SetMi1330ImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_MI1320
	case SENSOR_MI1320:
		SetMi1320ImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_MI0360
	case SENSOR_MI360:
		SetMi360ImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_SIV100B
	case SENSOR_SIV100B:
		SetSIV100BImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_SID130B
	case SENSOR_SID130B:
		SetSID130BImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_SIV120B
	case SENSOR_SIV120B:
		SetSIV120BImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_SIV120D
	case SENSOR_SIV120D:
		SetSIV120DImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_SIM120C
	case SENSOR_SIM120C:
		SetSIM120CImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_SIW021A
	case SENSOR_SIW021A:
		SetSIW021AImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_ST171
	case SENSOR_ST171:
		SetST171ImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_GC0307
	case SENSOR_GC0307:
		SetGC0307ImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_GC1036
	case SENSOR_GC1036:
		SetGC1036ImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_GC1136
	case SENSOR_GC1136:
		SetGC1136ImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_YACBAA0S
	case SENSOR_YACBAA0S:
		SetYACBAA0SImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_YACBAC1S
	case SENSOR_YACBAC1S:
		SetYACBAC1SImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_YACC5A2S
	case SENSOR_YACC5A2S:
		SetYACC5A2SImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_YACC6A1S
	case SENSOR_YACC6A1S:
		SetYACC6A1SImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_YACC611
	case SENSOR_YACC611:
		SetYACC611ImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_YACY9A1
	case SENSOR_YACY9A1:
		SetYACY9A1ImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_YACD6A1C
	case SENSOR_YACD6A1C:
		SetYACD6A1CImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_YACY6A1C9SBC
	case SENSOR_YACY6A1C9SBC:
		SetYACY6A1C9SBCImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_S5K6A1
	case SENSOR_S5K6A1:
		SetS5K6A1ImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_S5K8AA
	case SENSOR_S5K8AA:
		SetS5K8AAImgDir(bySnrImgDir);	//flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_MI1040
	case SENSOR_MI1040:
		SetMI1040ImgDir(bySnrImgDir);   //flip: bit1,mirror bit0.
		break;
#endif
#ifdef RTS58XX_SP_IMX119PQH5
	case SENSOR_IMX119PQH5:
		SetIMX119PQH5ImgDir(bySnrImgDir);
		break;
#endif

#ifdef RTS58XX_SP_IMX188PQ
	case SENSOR_IMX188PQ:
		SetIMX188PQImgDir(bySnrImgDir);
		break;
#endif


#ifdef RTS58XX_SP_IMX132PQ
	case SENSOR_IMX132PQ:
		SetIMX132PQImgDir(bySnrImgDir);
		break;
#endif
#ifdef RTS58XX_SP_IMX208PQH5
	case SENSOR_IMX208PQH5:
		SetIMX208PQH5ImgDir(bySnrImgDir);
		break;
#endif
#ifdef RTS58XX_SP_IMX189PSH5
	case SENSOR_IMX189PSH5:
		SetIMX189PSH5ImgDir(bySnrImgDir);
		break;
#endif
#ifdef RTS58XX_SP_IMX076LQZC
	case SENSOR_IMX076LQZC:
		SetIMX076LQZCImgDir(bySnrImgDir);
		break;
#endif
	default:
		break;
	}
}

void CfgSensorControlAttr(void )
{
	switch(g_wSensor)
	{
#ifdef RTS58XX_SP_HM1055
	case SENSOR_HM1055:
		CfgHM1055ControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_MI1040
	case SENSOR_MI1040:
		CfgMI1040ControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_S5K6AAFX
	case SENSOR_S5K6AAFX:
		CfgS5K6AAControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_S5K6A1
	case SENSOR_S5K6A1:
		CfgS5K6A1ControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_S5K8AA
	case SENSOR_S5K8AA:
		CfgS5K8AAControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_OV7725
	case SENSOR_OV7725:
		CfgOV7725ControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_OV7740
	case SENSOR_OV7740:
		CfgOV7740ControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_OV7670
	case SENSOR_OV7670:
		CfgOV7670ControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_OV7675
	case SENSOR_OV7675:
		CfgOV7675ControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_OV7690
	case SENSOR_OV7690:
		CfgOV7690ControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_OV9663
	case SENSOR_OV9663:
		CfgOV9663ControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_OV9710
	case SENSOR_OV9710:
		CfgOV9710ControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_OV9715
	case SENSOR_OV9715:
		CfgOV9715ControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_OV9726
	case SENSOR_OV9726:
		CfgOV9726ControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_OV9728
	case SENSOR_OV9728:
		CfgOV9728ControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_OV9724
	case SENSOR_OV9724:
		CfgOV9724ControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_OV2650
	case SENSOR_OV2650:
		CfgOV2650ControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_OV2710
	case SENSOR_OV2710:
		CfgOV2710ControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_OV2680
	case SENSOR_OV2680:
		CfgOV2680ControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_T4K71
	case SENSOR_T4K71:
		CfgT4K71ControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_OV2720
	case SENSOR_OV2720:
		CfgOV2720ControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_OV2722
	case SENSOR_OV2722:
		CfgOV2722ControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_OV3640
	case SENSOR_OV3640:
		CfgOV3640ControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_OV5642
	case SENSOR_OV5642:
		CfgOV5642ControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_OV5640
	case SENSOR_OV5640:
		CfgOV5640ControlAttr();
		break;			
#endif
#ifdef RTS58XX_SP_MI2020
	case SENSOR_MI2020:
		CfgMI2020ControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_MI1330
	case SENSOR_MI1330:
		CfgMI1330ControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_JXH22
	case SENSOR_JXH22:
		CfgJXH22ControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_MI0360
	case SENSOR_MI360:
		CfgMi360ControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_MI1320
	case SENSOR_MI1320:
		CfgMi1320ControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_SIV120B
	case SENSOR_SIV120B:
		CfgSIV120BControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_SIV120D
	case SENSOR_SIV120D:
		CfgSIV120DControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_SIV100B
	case SENSOR_SIV100B:
		CfgSIV100BControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_SIM120C
	case SENSOR_SIM120C:
		CfgSIM120CControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_SIW021A
	case SENSOR_SIW021A:
		CfgSIW021AControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_ST171
	case SENSOR_ST171:
		CfgST171ControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_GC0307
	case SENSOR_GC0307:
		CfgGC0307ControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_GC1036
	case SENSOR_GC1036:
		CfgGC1036ControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_GC1136
	case SENSOR_GC1136:
		CfgGC1136ControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_YACBAA0S
	case SENSOR_YACBAA0S:
		CfgYACBAA0SControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_YACBAC1S
	case SENSOR_YACBAC1S:
		CfgYACBAC1SControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_YACC5A2S
	case SENSOR_YACC5A2S:
		CfgYACC5A2SControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_YACC6A1S
	case SENSOR_YACC6A1S:
		CfgYACC6A1SControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_YACC611
	case SENSOR_YACC611:
		CfgYACC611ControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_YACY9A1
	case SENSOR_YACY9A1:
		CfgYACY9A1ControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_YACD6A1C
	case SENSOR_YACD6A1C:
		CfgYACD6A1CControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_YACY6A1C9SBC
	case SENSOR_YACY6A1C9SBC:
		CfgYACY6A1C9SBCControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_IMX119PQH5
	case SENSOR_IMX119PQH5:
		CfgIMX119PQH5ControlAttr();
		break;
#endif

#ifdef RTS58XX_SP_IMX188PQ
	case SENSOR_IMX188PQ:
		CfgIMX188PQControlAttr();
		break;
#endif


#ifdef RTS58XX_SP_IMX132PQ
	case SENSOR_IMX132PQ:
		CfgIMX132PQControlAttr();
		break;
#endif

#ifdef RTS58XX_SP_IMX208PQH5
	case SENSOR_IMX208PQH5:
		CfgIMX208PQH5ControlAttr();
		break;
#endif

#ifdef RTS58XX_SP_IMX189PSH5
	case SENSOR_IMX189PSH5:
		CfgIMX189PSH5ControlAttr();
		break;
#endif

#ifdef RTS58XX_SP_IMX076LQZC
	case SENSOR_IMX076LQZC:
		CfgIMX076LQZCControlAttr();
		break;
#endif

#ifdef RTS58XX_SP_RS0509
	case SENSOR_RS0509:
		CfgRS0509ControlAttr();
		break;
#endif
#ifdef RTS58XX_SP_RS0551C
	case SENSOR_RS0551C:
		CfgRS0551CControlAttr();
		break;
#endif
	case SENSOR_FT2_MODEL:
	default:
		CfgFT2ControlAttr();
		break;
	}
}

#ifdef _BLACK_SCREEN_
// unit : 1/16 gain
U16 GetSensorAEGain()
{
	U16 wResultGain =0;

	switch(g_wSensor)
	{
#ifdef RTS58XX_SP_OV7725
	case SENSOR_OV7725:
#endif
#ifdef RTS58XX_SP_OV7740
	case SENSOR_OV7740:
#endif
#ifdef RTS58XX_SP_OV7670
	case SENSOR_OV7670:
#endif
#ifdef RTS58XX_SP_OV7675
	case SENSOR_OV7675:
#endif
#ifdef RTS58XX_SP_OV7690
	case SENSOR_OV7690:
#endif
#ifdef RTS58XX_SP_OV9663
	case SENSOR_OV9663:
#endif
#ifdef RTS58XX_SP_OV2650
	case SENSOR_OV2650:
#endif
#if (defined(RTS58XX_SP_OV7725) || defined(RTS58XX_SP_OV7740) ||defined(RTS58XX_SP_OV7670)||defined(RTS58XX_SP_OV7675) || defined(RTS58XX_SP_OV7690) || defined(RTS58XX_SP_OV2650))
		{
			wResultGain=GetOVSensorAEGain();
			break;
		}
#endif
#ifdef RTS58XX_SP_MI0360
	case SENSOR_MI360:
		wResultGain = GetMi360AEGain();
		break;
#endif
#ifdef RTS58XX_SP_MI1330
	case SENSOR_MI1330:
		wResultGain = GetMi1330AEGain();
		break;
#endif
#ifdef RTS58XX_SP_MI2020
	case SENSOR_MI2020:
		wResultGain =	GetMi2020AEGain();
		break;
#endif
#ifdef RTS58XX_SP_SIV100B
	case SENSOR_SIV100B:
		wResultGain =	GetSIV100BAEGain();
		break;
#endif
#ifdef RTS58XX_SP_SIV120B
	case SENSOR_SIV120B:
		wResultGain =	GetSIV120BAEGain();
		break;
#endif
#ifdef RTS58XX_SP_SIV120D
	case SENSOR_SIV120D:
		wResultGain =	GetSIV120DAEGain();
		break;
#endif
#ifdef RTS58XX_SP_SIM120C
	case SENSOR_SIM120C:
		wResultGain =	GetSIM120CAEGain();
		break;
#endif
#ifdef RTS58XX_SP_GC0307
	case SENSOR_GC0307:
		wResultGain =	GetGC0307AEGain();
		break;
#endif
#ifdef RTS58XX_SP_YACBAA0S
	case SENSOR_YACBAA0S:
		wResultGain = GetYACBAA0SAEGain();
		break;
#endif
#ifdef RTS58XX_SP_YACBAC1S
	case SENSOR_YACBAC1S:
		wResultGain = GetYACBAC1SAEGain();
		break;
#endif
#ifdef RTS58XX_SP_YACC5A2S
	case SENSOR_YACC5A2S:
		wResultGain = GetYACC5A2SAEGain();
		break;
#endif
	default:

		break;
	}
	return wResultGain;
}

U8 GetSensorYavg(void)
{
	U8 byResult = 0xff;

	switch(g_wSensor)
	{
#ifdef RTS58XX_SP_OV7670
	case SENSOR_OV7670:
		byResult = GetOV7670Yavg();
		break;
#endif
		/*
		#ifdef RTS58XX_SP_OV7675
		        case SENSOR_OV7675:
		            byResult = GetOV7675Yavg();
		            break;
		#endif
		*/
	}

	return byResult;
}

U16 GetSensorExposureTime(void)
{
	U16 wResult = 0xffff;

	switch(g_wSensor)
	{
#ifdef RTS58XX_SP_OV7670
	case SENSOR_OV7670:
		wResult = GetOV7670Exposuretime();
		break;
#endif
		/*
		#ifdef RTS58XX_SP_OV7675
		        case SENSOR_OV7675:
		            wResult = GetOV7675Exposuretime();
		            break;
		#endif
		*/
	}

	return wResult;
}
#endif

#if (defined(RTS58XX_SP_MI1330)||defined(RTS58XX_SP_MI2020))
U8 Read_MI_Vairable ( U16 dWidth_driverID_offset,U16* pwData)
{
	if (SENSOR_MI2020 == g_wSensor)
	{
		Write_SenReg(MI2020_ADDR_MODE_REG, dWidth_driverID_offset);
		Read_SenReg(MI2020_DATA_INOUT_REG,pwData);
	}
	else if (SENSOR_MI1330 == g_wSensor)
	{
		Write_SenReg(MI1330_ADDR_MODE_REG, dWidth_driverID_offset);
		Read_SenReg(MI1330_DATA_INOUT_REG,pwData);
	}

	return TRUE;
}

U8 Write_MI_Vairable ( U16 dWidth_driverID_offset,U16 wData)
{
	//   DBG(("MIVAR_ADDR=%x\n ",dWidth_driverID_offset));
	if (SENSOR_MI2020 == g_wSensor)
	{
		Write_SenReg(MI2020_ADDR_MODE_REG, dWidth_driverID_offset);
		Write_SenReg(MI2020_DATA_INOUT_REG,wData);
	}
	else if (SENSOR_MI1330 == g_wSensor)
	{
		Write_SenReg(MI1330_ADDR_MODE_REG, dWidth_driverID_offset);
		Write_SenReg(MI1330_DATA_INOUT_REG,wData);
	}

	return TRUE;
}
#endif

#ifdef _BLACK_SCREEN_
#if (defined(RTS58XX_SP_OV7725) || defined(RTS58XX_SP_OV7740) ||defined(RTS58XX_SP_OV7670)||defined(RTS58XX_SP_OV7675) || defined(RTS58XX_SP_OV7690) || defined(RTS58XX_SP_OV2650))
U16 GetOVSensorAEGain(void)
{
	U8 i;
	U16 wAddr, wGain;
	U16 wResultGain;

	if (g_wSensor == SENSOR_OV2650 )
	{
		wAddr = 0x3000;
	}
	else
	{
		wAddr = 0x00;
	}
	Read_SenReg(wAddr, &wGain);

	wResultGain = (wGain&0x000F)+16;
	wGain >>= 4;
	for(i=0; i< 12; i++)
	{
		if(wGain&0x01)
		{
			wResultGain<<=1;
		}
		wGain >>=1;
	}

	return wResultGain;
}
#endif
#endif

#ifdef _STILL_IMG_BACKUP_SETTING_
// hemonel 2009-11-19: preview to still image backup AE and AWB
void PreviewToCapture(U16 wPreviewWidth, U16 wStillWidth)
{
#ifdef RTS58XX_SP_OV2650
	if(g_wSensor == SENSOR_OV2650)
	{
		OV2650PreviewToCapture(wPreviewWidth, wStillWidth);
		// hemonel 2009-11-18: add for fast WIA capture
		g_byIsAEAWBFixed = 1;
	}
#endif

	// hemonel 2010-01-13:for delete warning
	wPreviewWidth = wPreviewWidth;
	wStillWidth = wStillWidth;
}

// hemonel 2009-11-19: preview to still image backup AE and AWB
void CaptureToPreview()
{
#ifdef RTS58XX_SP_OV2650
	if(g_wSensor == SENSOR_OV2650)
	{
		OV2650CaptureToPreview();
	}
#endif
}
#endif

void SetSensorExposuretime_Gain(float fExpTime, float fTotalGain)
{
	switch(g_wSensor)
	{
#ifdef RTS58XX_SP_HM1055
	case SENSOR_HM1055:
		SetHM1055Exposuretime_Gain(fExpTime, fTotalGain);
		break;
#endif //RTS58XX_SP_YACC611			
#ifdef RTS58XX_SP_OV9710
	case SENSOR_OV9710:
		SetOV9710Exposuretime_Gain(fExpTime, fTotalGain);
		break;
#endif //RTS58XX_SP_OV9710
#ifdef RTS58XX_SP_OV9715
	case SENSOR_OV9715:
		SetOV9715Exposuretime_Gain(fExpTime, fTotalGain);
		break;
#endif //RTS58XX_SP_OV9710

#ifdef RTS58XX_SP_OV9726
	case SENSOR_OV9726:
		SetOV9726Exposuretime_Gain(fExpTime, fTotalGain);
		break;
#endif //RTS58XX_SP_OV9726
#ifdef RTS58XX_SP_JXH22
	case SENSOR_JXH22:
		SetJXH22Exposuretime_Gain(fExpTime, fTotalGain);
		break;
#endif //RTS58XX_SP_OV9726
#ifdef RTS58XX_SP_OV9728
	case SENSOR_OV9728:
		SetOV9728Exposuretime_Gain(fExpTime, fTotalGain);
		break;
#endif //RTS58XX_SP_OV9728
#ifdef RTS58XX_SP_OV9724
	case SENSOR_OV9724:
		SetOV9724Exposuretime_Gain(fExpTime, fTotalGain);
		break;
#endif //RTS58XX_SP_OV9724
#ifdef RTS58XX_SP_OV2710
	case SENSOR_OV2710:
		SetOV2710Exposuretime_Gain(fExpTime, fTotalGain);
		break;
#endif //RTS58XX_OV2710
#ifdef RTS58XX_SP_OV2680
	case SENSOR_OV2680:
		SetOV2680Exposuretime_Gain(fExpTime, fTotalGain);
		break;
#endif //RTS58XX_OV2680
#ifdef RTS58XX_SP_T4K71
	case SENSOR_T4K71:
		SetT4K71Exposuretime_Gain(fExpTime, fTotalGain);
		break;
#endif //RTS58XX_OV2710
#ifdef RTS58XX_SP_OV2720
	case SENSOR_OV2720:
		SetOV2720Exposuretime_Gain(fExpTime, fTotalGain);
		break;
#endif //RTS58XX_OV2720
#ifdef RTS58XX_SP_OV2722
	case SENSOR_OV2722:
		SetOV2722Exposuretime_Gain(fExpTime, fTotalGain);
		break;
#endif //RTS58XX_OV2710
#ifdef RTS58XX_SP_S5K6A1
	case SENSOR_S5K6A1:
		SetS5K6A1Exposuretime_Gain(fExpTime, fTotalGain);
		break;
#endif //RTS58XX_SP_S5K6A1
#ifdef RTS58XX_SP_S5K8AA
	case SENSOR_S5K8AA:
		SetS5K8AAExposuretime_Gain(fExpTime, fTotalGain);
		break;
#endif //RTS58XX_SP_S5K8AA
#ifdef RTS58XX_SP_YACC6A1S
	case SENSOR_YACC6A1S:
		SetYACC6A1SExposuretime_Gain(fExpTime, fTotalGain);
		break;
#endif
#ifdef RTS58XX_SP_SIW021A
	case SENSOR_SIW021A:
		SetSIW021AExposuretime_Gain(fExpTime, fTotalGain);
		break;
#endif //RTS58XX_SP_SIW021A
#ifdef RTS58XX_SP_ST171
	case SENSOR_ST171:
		SetST171Exposuretime_Gain(fExpTime, fTotalGain);
		break;
#endif //RTS58XX_SP_ST171
#ifdef RTS58XX_SP_MI1040
	case SENSOR_MI1040:
		SetMI1040Exposuretime_Gain(fExpTime, fTotalGain);
		break;
#endif //RTS58XX_SP_MI1040
#ifdef RTS58XX_SP_YACC611
	case SENSOR_YACC611:
		SetYACC611Exposuretime_Gain(fExpTime, fTotalGain);
		break;
#endif //RTS58XX_SP_YACC611
#ifdef RTS58XX_SP_YACY9A1
	case SENSOR_YACY9A1:
		SetYACY9A1Exposuretime_Gain(fExpTime, fTotalGain);
		break;
#endif //RTS58XX_SP_YACY9A1
#ifdef RTS58XX_SP_YACD6A1C
	case SENSOR_YACD6A1C:
		SetYACD6A1CExposuretime_Gain(fExpTime, fTotalGain);
		break;
#endif //RTS58XX_SP_YACD6A1C
#ifdef RTS58XX_SP_IMX119PQH5
	case SENSOR_IMX119PQH5:
		SetIMX119PQH5Exposuretime_Gain(fExpTime, fTotalGain);
		break;
#endif

#ifdef RTS58XX_SP_YACY6A1C9SBC
	case SENSOR_YACY6A1C9SBC:
		SetYACY6A1C9SBCExposuretime_Gain(fExpTime, fTotalGain);
		break;
#endif //RTS58XX_SP_YACY6A1C9SBC

#ifdef RTS58XX_SP_IMX188PQ
	case SENSOR_IMX188PQ:
		SetIMX188PQExposuretime_Gain(fExpTime, fTotalGain);
		break;
#endif

#ifdef RTS58XX_SP_IMX132PQ
	case SENSOR_IMX132PQ:
		SetIMX132PQExposuretime_Gain(fExpTime, fTotalGain);
		break;
#endif
#ifdef RTS58XX_SP_IMX208PQH5
	case SENSOR_IMX208PQH5:
		SetIMX208PQH5Exposuretime_Gain(fExpTime, fTotalGain);
		break;
#endif
#ifdef RTS58XX_SP_IMX189PSH5
	case SENSOR_IMX189PSH5:
		SetIMX189PSH5Exposuretime_Gain(fExpTime, fTotalGain);
		break;
#endif
#ifdef RTS58XX_SP_IMX076LQZC
	case SENSOR_IMX076LQZC:
		SetIMX076LQZCExposuretime_Gain(fExpTime, fTotalGain);
		break;
#endif
#ifdef RTS58XX_SP_GC1036
	case SENSOR_GC1036:
		SetGC1036Exposuretime_Gain(fExpTime, fTotalGain);
		break;
#endif //RTS58XX_SP_OV9726
#ifdef RTS58XX_SP_GC1136
		case SENSOR_GC1136:
			SetGC1136Exposuretime_Gain(fExpTime, fTotalGain);
			break;
#endif 
	default:
		break;
	}
	return;
}

void SetSensorDynamicISP(U8 byAEC_Gain)
{
	switch(g_wSensor)
	{
#ifdef  RTS58XX_SP_HM1055
	case SENSOR_HM1055:
		SetHM1055DynamicISP(byAEC_Gain);
		break;
#endif
#ifdef  RTS58XX_SP_SIW021A
	case SENSOR_SIW021A:
		SetSIW021ADynamicISP(byAEC_Gain);
		break;
#endif
#ifdef  RTS58XX_SP_ST171
	case SENSOR_ST171:
		SetST171DynamicISP(byAEC_Gain);
		break;
#endif
#ifdef RTS58XX_SP_MI1040
	case SENSOR_MI1040:
		SetMI1040DynamicISP(byAEC_Gain);
		break;
#endif
#ifdef RTS58XX_SP_OV9710
	case SENSOR_OV9710:
		SetOV9710DynamicISP(byAEC_Gain);
		break;
#endif
#ifdef RTS58XX_SP_OV9726
	case SENSOR_OV9726:
		SetOV9726DynamicISP(byAEC_Gain);
		break;
#endif
#ifdef RTS58XX_SP_JXH22
	case SENSOR_JXH22:
		SetJXH22DynamicISP(byAEC_Gain);
		break;
#endif
#ifdef RTS58XX_SP_OV9715
	case SENSOR_OV9715:
		SetOV9715DynamicISP(byAEC_Gain);
		break;
#endif
#ifdef RTS58XX_SP_OV9728
	case SENSOR_OV9728:
		SetOV9728DynamicISP(byAEC_Gain);
		break;
#endif
#ifdef RTS58XX_SP_OV9724
	case SENSOR_OV9724:
		SetOV9724DynamicISP(byAEC_Gain);
		break;
#endif
#ifdef RTS58XX_SP_OV2710
	case SENSOR_OV2710:
		SetOV2710DynamicISP(byAEC_Gain);
		break;
#endif
#ifdef RTS58XX_SP_OV2680
	case SENSOR_OV2680:
		SetOV2680DynamicISP(byAEC_Gain);
		break;
#endif
#ifdef RTS58XX_SP_T4K71
	case SENSOR_T4K71:
		SetT4K71DynamicISP(byAEC_Gain);
		break;
#endif
#ifdef RTS58XX_SP_OV2720
	case SENSOR_OV2710:
		SetOV2720DynamicISP(byAEC_Gain);
		break;
#endif
#ifdef RTS58XX_SP_OV2722
	case SENSOR_OV2722:
		SetOV2722DynamicISP(byAEC_Gain);
		break;
#endif
#ifdef  RTS58XX_SP_S5K6A1
	case SENSOR_S5K6A1:
		SetS5K6A1DynamicISP(byAEC_Gain);
		break;
#endif
#ifdef  RTS58XX_SP_S5K8AA
	case SENSOR_S5K8AA:
		SetS5K8AADynamicISP(byAEC_Gain);
		break;
#endif
#ifdef  RTS58XX_SP_YACC6A1S
	case SENSOR_YACC6A1S:
		SetYACC6A1SDynamicISP(byAEC_Gain);
		break;
#endif
#ifdef  RTS58XX_SP_YACC611
	case SENSOR_YACC611:
		SetYACC611DynamicISP(byAEC_Gain);
		break;
#endif
#ifdef RTS58XX_SP_YACY9A1
	case SENSOR_YACY9A1:
		SetYACY9A1DynamicISP(byAEC_Gain);
		break;
#endif
#ifdef RTS58XX_SP_YACD6A1C
	case SENSOR_YACD6A1C:
		SetYACD6A1CDynamicISP(byAEC_Gain);
		break;
#endif
#ifdef RTS58XX_SP_YACY6A1C9SBC
	case SENSOR_YACY6A1C9SBC:
		SetYACY6A1C9SBCDynamicISP(byAEC_Gain);
		break;
#endif
#ifdef  RTS58XX_SP_IMX119PQH5
	case SENSOR_IMX119PQH5:
		SetIMX119PQH5DynamicISP(byAEC_Gain);
		break;
#endif

#ifdef  RTS58XX_SP_IMX188PQ
	case SENSOR_IMX188PQ:
		SetIMX188PQDynamicISP(byAEC_Gain);
		break;
#endif


#ifdef  RTS58XX_SP_IMX132PQ
	case SENSOR_IMX132PQ:
		SetIMX132PQDynamicISP(byAEC_Gain);
		break;
#endif
#ifdef  RTS58XX_SP_IMX208PQH5
	case SENSOR_IMX208PQH5:
		SetIMX208PQH5DynamicISP(byAEC_Gain);
		break;
#endif
#ifdef  RTS58XX_SP_IMX189PSH5
	case SENSOR_IMX189PSH5:
		SetIMX189PSH5DynamicISP(byAEC_Gain);
		break;
#endif
#ifdef  RTS58XX_SP_IMX076LQZC
	case SENSOR_IMX076LQZC:
		SetIMX076LQZCDynamicISP(byAEC_Gain);
		break;
#endif
#ifdef RTS58XX_SP_GC1036
	case SENSOR_GC1036:
		SetGC1036DynamicISP(byAEC_Gain);
		break;
#endif
#ifdef RTS58XX_SP_GC1136
		case SENSOR_GC1136:
			SetGC1136DynamicISP(byAEC_Gain);
			break;
#endif
	default:
		break;
	}
}


void SetSensorDynamicISP_AWB(U16 wColorTempature)
{
	switch(g_wSensor)
	{
#ifdef  RTS58XX_SP_HM1055
	case SENSOR_HM1055:
		SetHM1055DynamicISP_AWB(wColorTempature);
		break;
#endif
#ifdef  RTS58XX_SP_SIW021A
	case SENSOR_SIW021A:
		SetSIW021ADynamicISP_AWB(wColorTempature);
		break;
#endif
#ifdef  RTS58XX_SP_ST171
	case SENSOR_ST171:
		SetST171DynamicISP_AWB(wColorTempature);
		break;
#endif
#ifdef RTS58XX_SP_MI1040
	case SENSOR_MI1040:
		SetMI1040DynamicISP_AWB(wColorTempature);
		break;
#endif
#ifdef RTS58XX_SP_OV9710
	case SENSOR_OV9710:
		SetOV9710DynamicISP_AWB(wColorTempature);
		break;
#endif
#ifdef RTS58XX_SP_OV9715
	case SENSOR_OV9715:
		SetOV9715DynamicISP_AWB(wColorTempature);
		break;
#endif
#ifdef RTS58XX_SP_OV9726
	case SENSOR_OV9726:
		SetOV9726DynamicISP_AWB(wColorTempature);
		break;
#endif
#ifdef RTS58XX_SP_OV9728
	case SENSOR_OV9728:
		SetOV9728DynamicISP_AWB(wColorTempature);
		break;
#endif
#ifdef RTS58XX_SP_OV9724
	case SENSOR_OV9724:
		SetOV9724DynamicISP_AWB(wColorTempature);
		break;
#endif
#ifdef RTS58XX_SP_OV2710
	case SENSOR_OV2710:
		SetOV2710DynamicISP_AWB(wColorTempature);
		break;
#endif
#ifdef RTS58XX_SP_OV2680
	case SENSOR_OV2680:
		SetOV2680DynamicISP_AWB(wColorTempature);
		break;
#endif
#ifdef RTS58XX_SP_JXH22
	case SENSOR_JXH22:
		SetJXH22DynamicISP_AWB(wColorTempature);
		break;
#endif
#ifdef RTS58XX_SP_T4K71
	case SENSOR_T4K71:
		SetT4K71DynamicISP_AWB(wColorTempature);
		break;
#endif
#ifdef RTS58XX_SP_OV2720
	case SENSOR_OV2720:
		SetOV2720DynamicISP_AWB(wColorTempature);
		break;
#endif
#ifdef RTS58XX_SP_OV2722
	case SENSOR_OV2722:
		SetOV2722DynamicISP_AWB(wColorTempature);
		break;
#endif
#ifdef  RTS58XX_SP_S5K6A1
	case SENSOR_S5K6A1:
		SetS5K6A1DynamicISP_AWB(wColorTempature);
		break;
#endif
#ifdef  RTS58XX_SP_S5K8AA
	case SENSOR_S5K8AA:
		SetS5K8AADynamicISP_AWB(wColorTempature);
		break;
#endif
#ifdef  RTS58XX_SP_YACC6A1S
	case SENSOR_YACC6A1S:
		SetYACC6A1SDynamicISP_AWB(wColorTempature);
		break;
#endif
#ifdef  RTS58XX_SP_YACC611
	case SENSOR_YACC611:
		SetYACC611DynamicISP_AWB(wColorTempature);
		break;
#endif
#ifdef RTS58XX_SP_YACY9A1
	case SENSOR_YACY9A1:
		SetYACY9A1DynamicISP_AWB(wColorTempature);
		break;
#endif
#ifdef RTS58XX_SP_YACD6A1C
	case SENSOR_YACD6A1C:
		SetYACD6A1CDynamicISP_AWB(wColorTempature);
		break;
#endif
#ifdef RTS58XX_SP_YACY6A1C9SBC
	case SENSOR_YACY6A1C9SBC:
		SetYACY6A1C9SBCDynamicISP_AWB(wColorTempature);
		break;
#endif
#ifdef RTS58XX_SP_GC1036
	case SENSOR_GC1036:
		SetGC1036DynamicISP_AWB(wColorTempature);
		break;
#endif
#ifdef RTS58XX_SP_GC1136
	case SENSOR_GC1136:
		SetGC1136DynamicISP_AWB(wColorTempature);
		break;
#endif
#ifdef  RTS58XX_SP_IMX208PQH5
	case SENSOR_IMX208PQH5:
		SetIMX208PQH5DynamicISP_AWB( wColorTempature);
		break;
#endif
#ifdef  RTS58XX_SP_IMX189PSH5
	case SENSOR_IMX189PSH5:
		SetIMX189PSH5DynamicISP_AWB( wColorTempature);
		break;
#endif
#ifdef  RTS58XX_SP_IMX076LQZC
	case SENSOR_IMX076LQZC:
		SetIMX076LQZCDynamicISP_AWB( wColorTempature);
		break;
#endif

	default:
		break;
	}
}
