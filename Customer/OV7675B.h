#ifndef _OV7675_H_
#define _OV7675_H_

#define OV7675_AEW 0x50 //---20090422
#define OV7675_AEB  0x40

void OV7675SetFormatFps(U8 SetFormat, U8 Fps);
void CfgOV7675ControlAttr(void);
void SetOV7675Brightness(S16 swSetValue);
void    SetOV7675Contrast(U16 wSetValue);
void    SetOV7675Saturation(U16 wSetValue);
void    SetOV7675Hue(S16 swSetValue);
void    SetOV7675Sharpness(U8 bySetValue);
void SetOV7675Effect(U8 byEffect);
void SetOV7675ImgDir(U8 bySnrImgDir);
//void SetOV7675WindowCrop(U16 wXStart,U16 wXEnd,U16 wYStart,U16 wYEnd );
void SetOV7675OutputDim(U16 wWidth,U16 wHeight);
void SetOV7675PwrLineFreq(U8 byFPS, U8 byLightFrq);
void SetOV7675WBTemp(U16 wRgain, U16 wGgain, U16 wBgain);
OV_CTT_t GetOV7675AwbGain(void);
void SetOV7675WBTempAuto(U8 bySetValue);
void SetOV7675BackLightComp(U8 bySetValue);
extern code OV_CTT_t gc_OV7675_CTT[3];
U8 GetOV7675Yavg();
U16 GetOV7675Exposuretime();
void SetOV7675IntegrationTimeAuto(U8 bySetValue);
void SetOV7675IntegrationTime(U16 wEspline);
#endif // _OV7675_H_
