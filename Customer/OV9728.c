#include "Inc.h"
#include "CamReg.h"
#ifdef RTS58XX_SP_OV9728

#ifdef _ENABLE_OUTDOOR_DETECT_
Dyanmic_AWB_Boundary_t code gc_AWBDynmaicBoundary={28,-8,16,8,50,30,2,21};
#endif

OV_CTT_t code gc_OV9728_CTT[3] =
{
	{2900,0x112,0x100,0x250},
	{4600,0x190,0x100,0x190},
	{6600,0x1D8,0x100,0x124},
};

code OV_FpsSetting_t  g_staOv9728HD720PFpsSetting[]=
{
//   FPS  ExtraDummyPixel       clkrc 	  	 pclk
	{5,		(11184),			(1), 		42946560},
	{9,		(6214),			(1), 		42946560},
	{10,		(5592),			(1), 		42946560},
	{15,		(3728),			(1),		42946560}, 
	{20,		(2796),			(1),		42946560}, 
	{25,		(2236),			(1),		42946560}, 
	{30,		(1864),			(1),		42946560}, 
};
code OV_FpsSetting_t  g_staOv9728HD800PFpsSetting[]=
{
//   FPS  ExtraDummyPixel       clkrc 	  	 pclk
	{5,		(8832),			(1), 		42923520},
	{9,		(4906),			(1), 		42923520},
	{10,		(4416),			(1), 		42923520},
	{15,		(2944),			(1),		42923520}, 
	{20,		(2208),			(1),		42923520}, 
	{25,		(1766),			(1),		42923520}, 
	{30,		(1472),			(1),		42923520}, 
};
// Sensor setting
t_RegSettingWB code gc_OV9728_HD800P_Setting[] =
{
	{0x103 ,0x01},
	{0x101 ,0x01},
	{0x202 ,0x03},
	{0x203 ,0xc4},
	{0x205 ,0x13},
	{0x301 ,0x0a},
	{0x303 ,0x01},
	{0x305 ,0x04},
	{0x307 ,0x46},
	{0x340 ,0x03},
	{0x341 ,0xcc},
	{0x342 ,0x05},
	{0x343 ,0xc0},
	{0x344 ,0x00},
	{0x345 ,0x04},
	{0x346 ,0x00},
	{0x347 ,0x00},
	{0x348 ,0x05},
	{0x349 ,0x1b},
	{0x34a ,0x03},
	{0x34b ,0x2f},
	{0x34c ,0x05}, // sensor width
	{0x34d ,0x10},
	{0x34e ,0x03}, // sensor height
	{0x34f ,0x2C},	
	{0x381 ,0x01},
	{0x383 ,0x01},
	{0x385 ,0x01},
	{0x387 ,0x01},
	{0x601 ,0x00},
	{0x3001 ,0x02},
	{0x3002 ,0x00},
	{0x3005 ,0x00},
	{0x3012 ,0x08},
	{0x3014 ,0x0d},
	{0x301e ,0x0a},
	{0x303c ,0x24},
	{0x303d ,0x92},
	{0x303e ,0x1a},
	{0x3040 ,0x0f},
	{0x3041 ,0x01},
	{0x3103 ,0x01},
	{0x3104 ,0x04},
	{0x3106 ,0x91},
	{0x3610 ,0x7c},
	{0x3621 ,0x65},
	{0x3622 ,0x9c},
	{0x3623 ,0x04},
	{0x3682 ,0x93},
	{0x3705 ,0x36},
	{0x370a ,0x23},
	{0x370f ,0x00},
	{0x3713 ,0x16},
	{0x3715 ,0x13},
	{0x3810 ,0x00},
	{0x3811 ,0x04},
	{0x3812 ,0x00},
	{0x3813 ,0x02},
	{0x3820 ,0xa0},
	{0x3821 ,0x00},
	{0x382a ,0x00},
	{0x3910 ,0xff},
	{0x3911 ,0xff},
	{0x3912 ,0x08},
	{0x3913 ,0x00},
	{0x3914 ,0x00},
	{0x3915 ,0x00},
	{0x3916 ,0x00},
	{0x3917 ,0x00},
	{0x3918 ,0x00},
	{0x3919 ,0x00},
	{0x391a ,0x00},
	{0x391b ,0xfc},
	{0x391c ,0x00},
	{0x3b00 ,0x00},
	{0x3b02 ,0x00},
	{0x3b03 ,0x00},
	{0x3b04 ,0x00},
	{0x3b05 ,0x00},
	{0x4001 ,0x00},
	{0x4008 ,0x02},
	{0x4009 ,0x05},
	{0x4010 ,0xf0},
	{0x4030 ,0x00},
	{0x4031 ,0x00},
	{0x4032 ,0x00},
	{0x4033 ,0x00},
	{0x4034 ,0x00},
	{0x4035 ,0x00},
	{0x4036 ,0x00},
	{0x4037 ,0x00},
	{0x4307 ,0x3a},
	{0x4400 ,0x00},
	{0x4402 ,0x00},
	{0x4403 ,0x00},
	{0x4404 ,0x00},
	{0x4405 ,0x00},
	{0x4501 ,0x08},
	{0x4601 ,0x16},
	{0x4602 ,0x00},
	{0x4603 ,0x00},
	{0x4605 ,0x00},
	{0x4606 ,0x00},
	{0x4607 ,0x00},
	{0x4608 ,0x00},
	{0x4609 ,0xa0},
	{0x4806 ,0x0f},
	{0x4837 ,0x2f},
	{0x4f02 ,0x00},
	{0x4f06 ,0x00},
	{0x4f07 ,0x23},
	{0x4f08 ,0x06},
	{0x4f0a ,0xaa},
	{0x4f0b ,0xaa},
	{0x5000 ,0x07},
	{0x5100 ,0x00},
	{0x100 ,0x01},
};

t_RegSettingWB code gc_OV9728_HD720P_Setting[] =
{
	{0x103, 0x01},
	{0x101, 0x01},
	{0x202, 0x03},
	{0x203, 0xc4},
	{0x205, 0x13},
	{0x301, 0x0a},
	{0x303, 0x01},
	{0x305, 0x04},
	{0x307, 0x46},
	{0x340, 0x03},
	{0x341, 0xcc},
	{0x342, 0x05},
	{0x343, 0xc0},
	{0x344, 0x00},
	{0x345, 0x04},
	{0x346, 0x00},
	{0x347, 0x00},
	{0x348, 0x05},
	{0x349, 0x1b},
	{0x34a, 0x03},
	{0x34b, 0x2f},
	{0x34c, 0x05},
	{0x34d, 0x10},
	{0x34e, 0x03},
	{0x34f, 0x2c},
	{0x381, 0x01},
	{0x383, 0x01},
	{0x385, 0x01},
	{0x387, 0x01},
	{0x601, 0x00},
	{0x3001, 0x02},
	{0x3002, 0x00},
	{0x3005, 0x00},
	{0x3012, 0x08},
	{0x3014, 0x0d},
	{0x301e, 0x0a},
	{0x303c, 0x24},
	{0x303d, 0x92},
	{0x303e, 0x1a},
	{0x3040, 0x0f},
	{0x3041, 0x01},
	{0x3103, 0x01},
	{0x3104, 0x04},
	{0x3106, 0x91},
	{0x3610, 0x7c},
	{0x3621, 0x45}, 
	{0x3622, 0x9c},
	{0x3623, 0x03},
	{0x3682, 0x92},
	{0x3705, 0x36},
	{0x370a, 0x23},
	{0x370d, 0xcc}, 
	{0x370f, 0x00},
	{0x3713, 0x16},
	{0x3715, 0x13},
	{0x3810, 0x00},
	{0x3811, 0x04},
	{0x3812, 0x00},
	{0x3813, 0x02},
	{0x3820, 0xa0},
	{0x3821, 0x00},
	{0x382a, 0x00},
	{0x3910, 0xff},
	{0x3911, 0xff},
	{0x3912, 0x08},
	{0x3913, 0x00},
	{0x3914, 0x00},
	{0x3915, 0x00},
	{0x3916, 0x00},
	{0x3917, 0x00},
	{0x3918, 0x00},
	{0x3919, 0x00},
	{0x391a, 0x00},
	{0x391b, 0xfc},
	{0x391c, 0x00},
	{0x3b00, 0x00},
	{0x3b02, 0x00},
	{0x3b03, 0x00},
	{0x3b04, 0x00},
	{0x3b05, 0x00},
	{0x4001, 0x00},
	{0x4008, 0x02},
	{0x4009, 0x05},
	{0x4010, 0xf0},
	{0x4017, 0x0f},
	{0x4030, 0x00},
	{0x4031, 0x00},
	{0x4032, 0x00},
	{0x4033, 0x00},
	{0x4034, 0x00},
	{0x4035, 0x00},
	{0x4036, 0x00},
	{0x4037, 0x00},
	{0x4307, 0x3a},
	{0x4400, 0x00},
	{0x4402, 0x00},
	{0x4403, 0x00},
	{0x4404, 0x00},
	{0x4405, 0x00},
	{0x4501, 0x08},
	{0x4601, 0x16},
	{0x4602, 0x00},
	{0x4603, 0x00},
	{0x4605, 0x00},
	{0x4606, 0x00},
	{0x4607, 0x00},
	{0x4608, 0x00},
	{0x4609, 0xa0},
	{0x4806, 0x0f},
	{0x4837, 0x2f},
	{0x4f02, 0x00},
	{0x4f06, 0x00},
	{0x4f07, 0x23},
	{0x4f08, 0x06},
	{0x4f0a, 0xaa},
	{0x4f0b, 0xaa},
	{0x5000, 0x07},
	{0x5100, 0x00},
	{0x340, 0x03},
	{0x341, 0x00},
	{0x342, 0x07},
	{0x343, 0x48},
	{0x344, 0x00},
	{0x345, 0x04},
	{0x346, 0x00},
	{0x347, 0x28},
	{0x348, 0x05},
	{0x349, 0x1b},
	{0x34a, 0x03},
	{0x34b, 0x07},
	{0x34c, 0x05},
	{0x34d, 0x08},
	{0x34e, 0x02},
	{0x34f, 0xd8},
	{0x3811, 0x08},
	{0x3813, 0x04},
	{0x100, 0x01},
};

static OV_FpsSetting_t*  GetOvFpsSetting(U8 Fps, OV_FpsSetting_t staOvFpsSetting[], U8 byArrayLen)
{
	U8 i;
	U8 Idx;

	ISP_MSG((" array size = %bd\n", byArrayLen));

	if(g_bIsHighSpeed)
	{
		Idx=2; //8fps for uxga, 10 fps for vga
	}
	else
	{
		Idx = 0;
	}

	for(i=0; i< byArrayLen; i++)
	{
		ISP_MSG((" fps i = %bd\n",i));
		if(staOvFpsSetting[i].byFps ==Fps)
		{
			Idx=i;
			break;
		}
	}
	ISP_MSG((" get fps idx = %bd\n",Idx));
	return &staOvFpsSetting[Idx];
}

static U16 MapSnrGlbGain2SnrRegSetting(U16 wGain)
{
	U16 wSnrRegGain = 0;
	U8 i;
	//QDBG(("byGain = %bd-> ",byGain));

	for(i=0; i<4; i++)
	{
		if(wGain >= 32)
		{
			wGain >>= 1;
			wSnrRegGain |=  (0x01<<(i+4));
		}
		else
		{
			wSnrRegGain |= (wGain-16);
			break;
		}
	}
	
	//QDBG(("sensorGain = %d    ",sensorGain));
	return wSnrRegGain;
}

float SnrRegSetting2SnrGlbGain(U16 wSnrRegGain)
{
	U16 wGain;
	float fGainOffset;
	U8 i;

	if(wSnrRegGain >= 0x70)
	{
		fGainOffset = 1.1; //0.99933; //0.5;
	}
	else if(wSnrRegGain >= 0x30)
	{
		fGainOffset = 0.360577; //0.32349; //0.2;
	}
	else if(wSnrRegGain >= 0x10)
	{
		fGainOffset = 0.107287; //0.11564; //0.0625;
	}
	else
	{
		fGainOffset = 0;
	}

	wGain = (wSnrRegGain&0x0f) + 16;
	wSnrRegGain >>= 4;

	for(i = 0; i < 4; i++)
	{
		if((wSnrRegGain&1) == 1)
		{
			wGain <<= 1;
		}

		wSnrRegGain >>= 1;
	}

	//return ((float)wGain)/16.0-fGainOffset;
	//return (((float)wGain)/16.0-fGainOffset-1.0)*1.27 + 1.0;
	return ((((float)wGain)/16.0-fGainOffset)-1.0)*1.2 + 1.0;
}

//this function is also called during Probe stage to calculate the transfer_size/LWM/HWM
void GetSensorPclkHsync(U16 wSensorSPFormat,U8 byFps)
{
	OV_FpsSetting_t *pOv9728_FpsSetting;

	wSensorSPFormat =wSensorSPFormat;
	if ((g_wSensorSPFormat & wSensorSPFormat) == HD720P_FRM)
	{
		pOv9728_FpsSetting=GetOvFpsSetting(byFps, g_staOv9728HD720PFpsSetting, sizeof(g_staOv9728HD720PFpsSetting)/sizeof(OV_FpsSetting_t));	
	}
	else
	{
		pOv9728_FpsSetting=GetOvFpsSetting(byFps, g_staOv9728HD800PFpsSetting, sizeof(g_staOv9728HD800PFpsSetting)/sizeof(OV_FpsSetting_t));
	}
	g_wSensorHsyncWidth = pOv9728_FpsSetting->wExtraDummyPixel; // this for manual exposure
	g_dwPclk = pOv9728_FpsSetting->dwPixelClk;		// this for scale speed
}

void OV9728SetFormatFps(U16 SetFormat, U8 Fps)
{
	U16 wTemp;
	OV_FpsSetting_t *pOv9728_FpsSetting;

	//write 720P and 800P common setting
	//WriteSensorSettingWB(sizeof(gc_OV9728_Common_Setting)/3, gc_OV9728_Common_Setting);
	
	if ((g_wSensorSPFormat & SetFormat) == HD720P_FRM)
	{
		pOv9728_FpsSetting=GetOvFpsSetting(Fps, g_staOv9728HD720PFpsSetting, sizeof(g_staOv9728HD720PFpsSetting)/sizeof(OV_FpsSetting_t));
		WriteSensorSettingWB(sizeof(gc_OV9728_HD720P_Setting)/3, gc_OV9728_HD720P_Setting);
		GetSensorPclkHsync(HD720P_FRM,Fps);
		g_wAECExposureRowMax = 764;	// this for max exposure time
		g_wAEC_LineNumber = 768;	// this for AE insert dummy line algothrim

		g_wSensorWidthBefBLC = 1280;
		g_wSensorHeightBefBLC = 720;
	}
	else
	{
		//1280*800
		pOv9728_FpsSetting=GetOvFpsSetting(Fps, g_staOv9728HD800PFpsSetting, sizeof(g_staOv9728HD800PFpsSetting)/sizeof(OV_FpsSetting_t));
		WriteSensorSettingWB(sizeof(gc_OV9728_HD800P_Setting)/3, gc_OV9728_HD800P_Setting);
		GetSensorPclkHsync(HD800P_FRM,Fps);
		g_wAECExposureRowMax = 968;	// this for max exposure time
		g_wAEC_LineNumber = 972;	// this for AE insert dummy line algothrim

		g_wSensorWidthBefBLC = 1280;
		g_wSensorHeightBefBLC = 800;
	}

	//write sensor PLL divider
	//Write_SenReg(0x0303, pOv9728_FpsSetting->byClkrc); // OV9728 will not change PLL when change fps
	
	wTemp = pOv9728_FpsSetting->wExtraDummyPixel;
	Write_SenReg(0x0343, INT2CHAR(wTemp, 0)); //Write dummy pixel LSB
	Write_SenReg(0x0342, INT2CHAR(wTemp, 1)); //Write dummy pixel MSB

	//_________________________________________________________
	//if 0x3503=07
	// (1) set settings (2) (3)gain work (4)exposure work (5)dummy work

	//if 0x3503=17
	// (1) set settings (2) (3) (4)exposure work&gain work  (5)dummy work
	Write_SenReg(0x3503, 0x17);
	Write_SenReg(0x3819, 0x6C); // fix image blink at insert dummy

#ifdef _MIPI_EXIST_
	//-----------------------------Dphy Setting--------------------------------
	SetMipiDphy(MIPI_DATA_LANE0_EN, MIPI_DATA_FORMAT_RAW10 , MIPI_DATA_TYPE_RAW10, HSTERM_EN_TIME_22);
#endif
}

void CfgOV9728ControlAttr(void)
{
	U8 i;

	//g_bySensorSize = SENSOR_SIZE_HD800P;
	//g_wSensorSPFormat = HD800P_FRM;
	g_bySensorSize = SENSOR_SIZE_HD720P;
	g_wSensorSPFormat = HD720P_FRM;
#ifdef _ENABLE_MJPEG_
	g_byQtableScale = 6;	//HD720P HD800P Setting=6 for 7 comp rate
#endif
	{
		memcpy(g_asOvCTT, gc_OV9728_CTT,sizeof(gc_OV9728_CTT));
	}

	{
		// hemonel 2010-09-08: support 1.8V sensor IO
#if (_CHIP_ID_ & _RTS5840_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V79;
#elif (_CHIP_ID_ & _RTS5832_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V85;
#elif (_CHIP_ID_ & _RTS5829B_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V82;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V80;
#endif
	}

	{
		//====== resolution  setting ===========
		// -- preview ---
		g_aVideoFormat[0].byaVideoFrameTbl[0] = 10;		// resolution number
		g_aVideoFormat[0].byaVideoFrameTbl[1]=F_SEL_640_480;
		g_aVideoFormat[0].byaVideoFrameTbl[2]=F_SEL_160_120;
		g_aVideoFormat[0].byaVideoFrameTbl[3]=F_SEL_176_144;
		g_aVideoFormat[0].byaVideoFrameTbl[4]=F_SEL_320_240;
		g_aVideoFormat[0].byaVideoFrameTbl[5]=F_SEL_352_288;
		g_aVideoFormat[0].byaVideoFrameTbl[6]=F_SEL_424_240;
		g_aVideoFormat[0].byaVideoFrameTbl[7]=F_SEL_640_360;
		g_aVideoFormat[0].byaVideoFrameTbl[8]=F_SEL_960_540;
		//g_aVideoFormat[0].byaVideoFrameTbl[6]=F_SEL_320_180;
		g_aVideoFormat[0].byaVideoFrameTbl[9]=F_SEL_1280_720;
		//g_aVideoFormat[0].byaVideoFrameTbl[8]=F_SEL_1280_800;
		
		//g_aVideoFormat[0].byaVideoFrameTbl[10]=F_SEL_848_480;
		
		
		g_aVideoFormat[0].byaVideoFrameTbl[10]=F_SEL_640_480;
		//--still image--
		g_aVideoFormat[0].byaStillFrameTbl[0] = 12;	// resolution number
		g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_1280_800;
		g_aVideoFormat[0].byaStillFrameTbl[2]=F_SEL_1280_720;
		g_aVideoFormat[0].byaStillFrameTbl[3]=F_SEL_160_120;
		g_aVideoFormat[0].byaStillFrameTbl[4]=F_SEL_176_144;
		g_aVideoFormat[0].byaStillFrameTbl[5]=F_SEL_320_240;
		g_aVideoFormat[0].byaStillFrameTbl[6]=F_SEL_352_288;
		g_aVideoFormat[0].byaStillFrameTbl[7]=F_SEL_640_480;
		g_aVideoFormat[0].byaStillFrameTbl[8]=F_SEL_960_540;
		g_aVideoFormat[0].byaStillFrameTbl[9]=F_SEL_848_480;
		g_aVideoFormat[0].byaStillFrameTbl[10]=F_SEL_640_360;
		g_aVideoFormat[0].byaStillFrameTbl[11]=F_SEL_424_240;
		g_aVideoFormat[0].byaStillFrameTbl[12]=F_SEL_320_180;
		//====== fps  setting ===========
		{
			for(i=0; i<10; i++)
			{
				/*
					F_SEL_640_480    (0)
					F_SEL_160_120    (1)
					F_SEL_176_144    (2)
					F_SEL_320_180	(3)
					F_SEL_320_200    (4)
					F_SEL_320_240    (5)
					F_SEL_352_288    (6)
					F_SEL_424_240	(7)
					F_SEL_640_360	(8)
					F_SEL_640_400    (9)
				*/
				g_aVideoFormat[0].waFrameFpsBitmap[i]=FPS_15|FPS_30;
			}
			for(i=10; i<18; i++)
			{
				/*
					F_SEL_800_600    (10)
					F_SEL_848_480	(11)
					F_SEL_960_540	 (12)
					F_SEL_1024_768   (13)
					F_SEL_1280_720   (14)
					F_SEL_1280_800   (15)
					F_SEL_1280_960   (16)
					F_SEL_1280_1024  (17)
				*/
				g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_9;
			}
		}
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_800_600]= FPS_25|FPS_20|FPS_15|FPS_10|FPS_5;
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_848_480]= FPS_20|FPS_15|FPS_10|FPS_5;
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_960_540]= FPS_15|FPS_10|FPS_5;

		//====== format type  setting ===========
		g_aVideoFormat[0].byFormatType = FORMAT_TYPE_YUY2;

#ifdef _ENABLE_MJPEG_
		// copy YUY2 setting to MJPEG setting
		memcpy(&(g_aVideoFormat[1]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

		// modify MJPEG FPS setting
		for(i=10; i<18; i++)
		{
			g_aVideoFormat[1].waFrameFpsBitmap[i] = FPS_15|FPS_30;
		}

		// modify MJPEG format type
		g_aVideoFormat[1].byFormatType = FORMAT_TYPE_MJPG;
#endif

#ifdef _ENABLE_M420_FMT_
		// copy YUY2 setting to M420 setting
		memcpy(&(g_aVideoFormat[2]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

		// modify M420 FPS setting
		{
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_800_600] =FPS_25|FPS_20|FPS_15|FPS_10|FPS_5;//|FPS_1;             //bit07 F_800_600
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_960_540] =FPS_25|FPS_20|FPS_15|FPS_10|FPS_5;//|FPS_5|FPS_3|FPS_1;                    //bit08 F_960_720
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1024_768] =FPS_15|FPS_10|FPS_5;//|FPS_1;                    //bit09 F_1024_768
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_720] =FPS_10|FPS_5;//FPS_9|FPS_8|FPS_1;    //bit10 F_1280_720
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_800] =FPS_10|FPS_5;

			// modify M420 format type
			g_aVideoFormat[2].byFormatType = FORMAT_TYPE_M420;
		}
#endif
	}
	InitOV9728IspParams();
}

void SetOV9728IntegrationTime(U16 wEspline)
{
	U16 wFrameLenLines;
	U16 wExpMax = g_wAECExposureRowMax-2;

	if(wEspline > wExpMax)
	{
		wFrameLenLines = wEspline + 20;
	}
	else
	{
		wFrameLenLines = g_wAEC_LineNumber;
	}

	if (wFrameLenLines%2==1)
	{
		wFrameLenLines++;
	}

	//-----------Write  frame length or dummy lines------------
	Write_SenReg(0x0341, INT2CHAR(wFrameLenLines, 0));
	Write_SenReg(0x0340, INT2CHAR(wFrameLenLines, 1));

	//-----------Write Exposuretime setting-----------
	Write_SenReg(0x0203, INT2CHAR(wEspline, 0));	// change exposure value low
	Write_SenReg(0x0202, INT2CHAR(wEspline, 1));	// change exposure value high

	//----------- write gain setting-------------------
	Write_SenReg(0x0205, 0x0f);	// fixed gain at manual exposure control
	Write_SenReg(0x0204, 0);
}

void SetOV9728Gain(float fGain)
{
}

void SetOV9728ImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.
{
	//WaitFrameSync(ISP_INT1,ISP_DATA_START_INT);
	SetBLCWindowStart(0, 1);

	bySnrImgDir ^= 0x01; 	// OV9728 output mirrored image at default, so need mirror the image for normal output
	Write_SenReg_Mask(0x101, bySnrImgDir, 0x03);
}

void SetOV9728Exposuretime_Gain(float fExpTime, float fTotalGain)
{
	U16 wExposureRows_floor;
	U16  wGainRegSetting; //data
	float fSnrGlbGain;
	float fExposureRows;
	U16 wSetDummy;
	float fTemp;
	U16 wExposurePixels;

	//wGainRegSetting = MapSnrGlbGain2SnrRegSetting((U16)(fTotalGain*16.0));
	//wGainRegSetting = MapSnrGlbGain2SnrRegSetting((U16)(((fTotalGain-1.0)/1.27+1.0)*16.0));
	wGainRegSetting = MapSnrGlbGain2SnrRegSetting((U16)(((fTotalGain-1.0)/1.2+1.0)*16.0));
	if((wGainRegSetting != 0) && ((wGainRegSetting&0xf) == 0))
	{
		wGainRegSetting = ((wGainRegSetting>>1)|0xf);
	}
	
	fSnrGlbGain = SnrRegSetting2SnrGlbGain(wGainRegSetting);
	
	fExposureRows = fExpTime/g_fSensorRowTimes;	//unit : 1row
	wExposureRows_floor = (U16)(fExposureRows);
	fTemp = fExpTime-((float)wExposureRows_floor*(g_fSensorRowTimes)); //fine integration time;unit:  us
	wExposurePixels =(U16)(fTemp * (float)(g_dwPclk/10000)) ; //unit: pixels

	// write exposure, gain, dummy line
	if(fabs(g_fCurExpTime-fExpTime) > 0.0001)
	{
		// set dummy
		wSetDummy = g_wAFRInsertDummylines + g_wAEC_LineNumber;

		// group register hold
		Write_SenReg(0x0104, 0x01);

		//-----------Write  frame length or dummy lines------------
		if (wSetDummy%2 == 1)
		{
			wSetDummy++;
		}
		Write_SenReg(0x0341, INT2CHAR(wSetDummy, 0));
		Write_SenReg(0x0340, INT2CHAR(wSetDummy, 1));

		//-----------Write Exposuretime setting-----------
		Write_SenReg(0x0203, INT2CHAR(wExposureRows_floor, 0));	// change exposure value low
		Write_SenReg(0x0202, INT2CHAR(wExposureRows_floor, 1));	// change exposure value high

		//Reck added 20100625, fix OV9728 red edge bug
		if (wExposurePixels>1280)
		{
			wExposurePixels = 1280;
		}
		Write_SenReg(0x0201, INT2CHAR(wExposurePixels, 0));	// change exposure value low
		Write_SenReg(0x0200, INT2CHAR(wExposurePixels, 1));	// change exposure value high

		//----------- write gain setting-------------------
		Write_SenReg(0x0205, INT2CHAR(wGainRegSetting, 0));
		Write_SenReg(0x0204, INT2CHAR(wGainRegSetting, 1));

		SetISPAEGain(fTotalGain, fSnrGlbGain, 1);

		// group register release
		Write_SenReg(0x0104, 0);
		g_fCurExpTime = fExpTime;
	}
	else
	{
		// group register hold
		Write_SenReg(0x0104, 1);
		wSetDummy = g_wAFRInsertDummylines + g_wAEC_LineNumber;

		//-----------Write  frame length or dummy lines------------
		if (wSetDummy%2 == 1)
		{
			wSetDummy++;
		}
		Write_SenReg(0x0341, INT2CHAR(wSetDummy, 0));
		Write_SenReg(0x0340, INT2CHAR(wSetDummy, 1));

		// write gain setting
		Write_SenReg(0x0205, INT2CHAR(wGainRegSetting, 0));
		Write_SenReg(0x0204, INT2CHAR(wGainRegSetting, 1));

		SetISPAEGain(fTotalGain, fSnrGlbGain, 1);

		// group register release
		Write_SenReg(0x0104, 0);
	}
	
	return;
}

void OV9728_POR(void )
{
#ifdef _MIPI_EXIST_
	Init_MIPI();
#else
	Init_CCS();
#endif
	ENTER_SENSOR_RESET();
	// power on
	SensorPowerControl(SWITCH_ON,EN_DELAYTIME);
	uDelay(1);
	// Initialize I2C Controller Timing
	LEAVE_SENSOR_RESET();
	// clock select 24M
	if( g_bySSCEnable== TRUE)
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_SSC_24M);
	}
	else
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_96M|CCS_CLK_DIVIDER_4);
	}

	EnableSensorHCLK();
	g_bySensorIsOpen = SENSOR_OPEN;
	WaitTimeOut_Delay(1);  //OV9728 spec request 8192clk
}

void InitOV9728IspParams(void )
{
	// AWB initial gain
	//use D50 R/G/B gain as AWB initial value
	g_wAWBRGain_Last= 0x17C;
	g_wAWBGGain_Last= 0x100;
	g_wAWBBGain_Last= 0x226;
	g_wProjectGR = g_wAWBRGain_Last;
	g_wProjectGB = g_wAWBBGain_Last;

	// Special ISP
	g_bySaturation_Def =64;
	g_byContrast_Def = 36;//33;//32; //Neil Tuning at chicony
	g_byTgamma_rate_max=63;
	g_byTgamma_rate_min =20;

	g_wDynamicISPEn = 0;
	//g_wDynamicISPEn = DYNAMIC_LSC_CT_EN| DYNAMIC_LSC_EN | DYNAMIC_SHARPNESS_EN | DYNAMIC_SHARPPARAM_EN | DYNAMIC_GAMMA_EN | DYNAMIC_CCM_CT_EN;
	g_wDynamicISPEn = DYNAMIC_SHARPPARAM_EN | DYNAMIC_LSC_CT_EN;// DYNAMIC_LSC_EN | DYNAMIC_CCM_CT_EN;

}

void SetOV9728DynamicISP(U8 byAEC_Gain)
{
	byAEC_Gain = byAEC_Gain;
	if( g_bySharpParamIndex_Last<=2)
	{
	XBYTE[ISP_NR_MMM_ENABLE]= 0x00; //0x852A
	XBYTE[ISP_LOC_ENABLE] = ISP_CRNR_EEH_DIS;	
	
}
	else
		{

			XBYTE[ISP_NR_MMM_ENABLE]= 0x01; //0x852A
			XBYTE[ISP_LOC_ENABLE] = ISP_CRNR_EEH_EN;	
		}
	if(g_wCurFrameHeight == 720 && ( (g_bySharpParamIndex_Last == 0) || (g_bySharpParamIndex_Last == 1)) )
	{
		XBYTE[0x853D]= 0x80;
		XBYTE[0x853C]= 0x80;
		
		XBYTE[ISP_EEH_SHARP_ARRAY0]= 0x04; //0x85E4
		XBYTE[ISP_EEH_SHARP_ARRAY10]= 0x12; //0x85EE
		XBYTE[ISP_EEH_SHARP_ARRAY11]= 0x16; //0x85EF
		XBYTE[ISP_EEH_SHARP_ARRAY1]= 0x08; //0x85E5
		XBYTE[ISP_EEH_SHARP_ARRAY2]= 0x0C; //0x85E6
		XBYTE[ISP_EEH_SHARP_ARRAY3]= 0x08; //0x85E7
		XBYTE[ISP_EEH_SHARP_ARRAY4]= 0x0E; //0x85E8
		XBYTE[ISP_EEH_SHARP_ARRAY5]= 0x10; //0x85E9
		XBYTE[ISP_EEH_SHARP_ARRAY6]= 0x0C; //0x85EA
		XBYTE[ISP_EEH_SHARP_ARRAY7]= 0x10; //0x85EB
		XBYTE[ISP_EEH_SHARP_ARRAY8]= 0x12; //0x85EC
		XBYTE[ISP_EEH_SHARP_ARRAY9]= 0x10; //0x85ED
/*
		XBYTE[ISP_EEH_SHARP_ARRAY0]= 0x04; //0x85E4
		XBYTE[ISP_EEH_SHARP_ARRAY10]= 0x20; //0x85EE
		XBYTE[ISP_EEH_SHARP_ARRAY11]= 0x20; //0x85EF
		XBYTE[ISP_EEH_SHARP_ARRAY1]= 0x10; //0x85E5
		XBYTE[ISP_EEH_SHARP_ARRAY2]= 0x16; //0x85E6
		XBYTE[ISP_EEH_SHARP_ARRAY3]= 0x10; //0x85E7
		XBYTE[ISP_EEH_SHARP_ARRAY4]= 0x18; //0x85E8
		XBYTE[ISP_EEH_SHARP_ARRAY5]= 0x18; //0x85E9
		XBYTE[ISP_EEH_SHARP_ARRAY6]= 0x16; //0x85EA
		XBYTE[ISP_EEH_SHARP_ARRAY7]= 0x18; //0x85EB
		XBYTE[ISP_EEH_SHARP_ARRAY8]= 0x20; //0x85EC
		XBYTE[ISP_EEH_SHARP_ARRAY9]= 0x18; //0x85ED
*/
		XBYTE[0x85FA]= 0x0A;//0x08;//0x04; //ISP_BRIGHT_RATE
		XBYTE[0x85F0]= 0x0A;//0x0F; //ISP_BRIGHT_TRM_B1
		XBYTE[0x85F1]= 0x19;//0x1E; //ISP_BRIGHT_TRM_B2
		XBYTE[0x85F4]= 0x0B; //ISP_BRIGHT_TRM_K
		XBYTE[0x85F2]= 0x0A; //ISP_BRIGHT_TRM_THD0
		XBYTE[0x85F3]= 0x1F; //ISP_BRIGHT_TRM_THD1

		XBYTE[0x85FB]= 0x0A;//0x08;//0x04;//0x10; //ISP_DARK_RATE
		XBYTE[0x85F5]= 0x0A;//0x0F; //ISP_DARK_TRM_B1
		XBYTE[0x85F6]= 0x19;//0x1E; //ISP_DARK_TRM_B2
		XBYTE[0x85F9]= 0x0B; //ISP_DARK_TRM_K
		XBYTE[0x85F7]= 0x0A; //ISP_DARK_TRM_THD0
		XBYTE[0x85F8]= 0x1F; //ISP_DARK_TRM_THD1
	}
	//else if(g_wCurFrameHeight == 720)
	else
	{
		XBYTE[0x853D]= 0x80;
		XBYTE[0x853C]= 0x80;

		XBYTE[0x85E4]= 0x08; //ISP_EEH_SHARP_ARRAY0
		XBYTE[0x85EE]= 0x0C; //ISP_EEH_SHARP_ARRAY10
		XBYTE[0x85EF]= 0x10; //ISP_EEH_SHARP_ARRAY11
		XBYTE[0x85E5]= 0x0C; //ISP_EEH_SHARP_ARRAY1
		XBYTE[0x85E6]= 0x10; //ISP_EEH_SHARP_ARRAY2
		XBYTE[0x85E7]= 0x0C; //ISP_EEH_SHARP_ARRAY3
		XBYTE[0x85E8]= 0x18; //ISP_EEH_SHARP_ARRAY4
		XBYTE[0x85E9]= 0x18; //ISP_EEH_SHARP_ARRAY5
		XBYTE[0x85EA]= 0x10; //ISP_EEH_SHARP_ARRAY6
		XBYTE[0x85EB]= 0x10; //ISP_EEH_SHARP_ARRAY7
		XBYTE[0x85EC]= 0x10; //ISP_EEH_SHARP_ARRAY8
		XBYTE[0x85ED]= 0x08; //ISP_EEH_SHARP_ARRAY9

		// Edge Enhance Bright Limit Value Transform
		XBYTE[0x85FA]= 0x04; //ISP_BRIGHT_RATE
		XBYTE[0x85F0] = 6; //ISP_BRIGHT_TRM_B1
		XBYTE[0x85F1] = 16; //ISP_BRIGHT_TRM_B2
		XBYTE[0x85F2] = 10; //ISP_BRIGHT_TRM_THD0
		XBYTE[0x85F3] = 24; //ISP_BRIGHT_TRM_THD1
		XBYTE[0x85F4] = 11;	//ISP_BRIGHT_TRM_K

		// Edge Enhance Dark Limit Value Transform
		XBYTE[0x85FB]= 0x04; //ISP_DARK_RATE
		XBYTE[0x85F5] = 15; //ISP_DARK_TRM_B1
		XBYTE[0x85F6] = 30; //ISP_DARK_TRM_B2
		XBYTE[0x85F7] = 6; //ISP_DARK_TRM_THD0
		XBYTE[0x85F8] = 24; //ISP_DARK_TRM_THD1
		XBYTE[0x85F9] = 13; //ISP_DARK_TRM_K
	}
	
}

void SetOV9728DynamicISP_AWB(U16 wColorTempature)
{
	wColorTempature =wColorTempature;
}
#endif
