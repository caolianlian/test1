#ifndef _SIW021A_H_
#define _SIW021A_H_

void SIW021ASetFormatFps(U16 SetFormat, U8 Fps);
void CfgSIW021AControlAttr(void);
void SetSIW021AImgDir(U8 bySnrImgDir);
void SetSIW021AIntegrationTime(U16 wEspline);
void SetSIW021AExposuretime_Gain(float fExpTime, float fTotalGain);
void InitSIW021AIspParams();
void SIW021A_POR();
void SetSIW021ADynamicISP(U8 byAEC_Gain);
void SetSIW021ADynamicISP_AWB(U16 wColorTempature);
void SetSIW021AGain(float fGain);
#endif // _SIW021A_H_

