#include "Inc.h"

#ifdef RTS58XX_SP_IMX076LQZC

#ifdef _ENABLE_OUTDOOR_DETECT_
Dyanmic_AWB_Boundary_t code gc_AWBDynmaicBoundary={28,-8,16,8,50,30,2,21};
#endif

OV_CTT_t code gc_IMX076LQZC_CTT[3] =
{
	{2900,0x112,0x100,0x250},
	{4600,0x190,0x100,0x190},
	{6600,0x1D8,0x100,0x124},
};

#if 0
code OV_FpsSetting_t  g_staIMX076LQZCFpsSetting[]=
{
// FPS ExtraDummyPixel clkrc 	   pclk
	{5,		(1650+841+1),	(2),		54000000},
	{7,		(1650),			(4),		54000000}, 	// adjust fps >=30fps for msoc jitter
	{8,		(1650+1473+1),	(2), 		54000000},
	{9,		(1650+1121+1),	(2), 		54000000},
	{10,		(2250+4500),		(0), 		54000000},
	{15,		(2250+2250),			(0), 		54000000},
	{20,		(2250+1123+1),		(0), 		54000000},
	{25,		(2250+450),		(0),		54000000},
	{30,		(2250),			(0),		54000000}, 	// adjust fps >=30fps for msoc jitter
	//{60,		(1123+1),		(0),		54000000}, 
	//{60,		(823+1),		(0),		54000000}, 
	{60,		(1650),		(0),		54648000}, 
};
#endif

code OV_FpsSetting_t  g_staIMX076LQZCFpsSetting_720P[]=
{
// FPS ExtraDummyPixel clkrc 	   pclk
	{5,		(1650+841+1),	(2),		54000000},
	{7,		(1650),			(4),		54000000}, 	// adjust fps >=30fps for msoc jitter
	{8,		(1650+1473+1),	(2), 		54000000},
	{9,		(1650+1121+1),	(2), 		54000000},
	{10,		(2250+4500),		(0), 		54000000},
	{15,		(2250+2250),			(0), 		54000000},
	{20,		(2250+1123+1),		(0), 		54000000},
	{25,		(2250+450),		(0),		54000000},
	{30,		(2250),			(0),		54000000}, 	// adjust fps >=30fps for msoc jitter
};

code OV_FpsSetting_t  g_staIMX076LQZCFpsSetting_VGA[]=
{
// FPS ExtraDummyPixel clkrc 	   pclk
	{30,		(1650+1650),		(0),		54648000}, 
	{60,		(1650),		(0),		54648000}, 
};


/*	// hemonel 2011-07-04: Albert use only one LSC curve to pass MSOC premium test, so delete these code
#ifdef _MSOC_TEST_
// bozhang 2011-07-01: use the different LSC curve at A and D65 for pass MSOC premium test
U8 code gc_byLSC_RGB_Curve_IMX076LQZC_3500K[3][48] =
{

{128, 0, 130, 0, 133, 0, 137, 0, 144, 0, 152, 0, 163, 0, 179, 0, 199, 0, 225, 0, 0, 1, 34, 1, 68, 1, 68, 1, 68, 1, 68, 1, 68, 1, 68, 1, 68, 1, 68, 1, 68, 1, 68, 1, 68, 1, 68, 1, },
{128, 0, 130, 0, 133, 0, 137, 0, 143, 0, 151, 0, 162, 0, 176, 0, 193, 0, 215, 0, 241, 0, 16, 1, 39, 1, 39, 1, 39, 1, 39, 1, 39, 1, 39, 1, 39, 1, 39, 1, 39, 1, 39, 1, 39, 1, 39, 1, },
{128, 0, 130, 0, 132, 0, 135, 0, 141, 0, 148, 0, 157, 0, 169, 0, 185, 0, 204, 0, 227, 0, 254, 0, 31, 1, 31, 1, 31, 1, 31, 1, 31, 1, 31, 1, 31, 1, 31, 1, 31, 1, 31, 1, 31, 1, 31, 1, },

};
U8 code gc_byLSC_RGB_Curve_IMX076LQZC_D65[3][48] =
{   // For Largan 9361H
   	//20110630
   	{128, 0, 129, 0, 134, 0, 141, 0, 151, 0, 165, 0, 183, 0, 206, 0, 234, 0, 12, 1, 47, 1, 85, 1, 134, 1, 134, 1, 134, 1, 134, 1, 134, 1, 134, 1, 134, 1, 134, 1, 134, 1, 134, 1, 134, 1, 134, 1, },
	{128, 0, 129, 0, 132, 0, 136, 0, 143, 0, 151, 0, 163, 0, 176, 0, 194, 0, 216, 0, 241, 0, 11, 1, 44, 1, 44, 1, 44, 1, 44, 1, 44, 1, 44, 1, 44, 1, 44, 1, 44, 1, 44, 1, 44, 1, 44, 1, },
	{128, 0, 128, 0, 131, 0, 135, 0, 141, 0, 149, 0, 158, 0, 171, 0, 187, 0, 207, 0, 228, 0, 252, 0, 25, 1, 25, 1, 25, 1, 25, 1, 25, 1, 25, 1, 25, 1, 25, 1, 25, 1, 25, 1, 25, 1, 25, 1, },

};

U16 code gc_wLSC_Center_Offset_IMX076LQZC_D65[6]={594 ,376,594 ,376,594, 376};
U16 code gc_wLSC_Center_Offset_IMX076LQZC_3500K[6]={622 ,388,622, 388,622 ,388};


#endif
*/
// Sensor setting
t_RegSettingWB code gc_IMX076LQZC_720P_30FPS_Setting[] =
{
	//1284x724@30fps 10bit
	//parallel CMOS SDR output
	//INCK=54MHz

	//ChipID: 0x02
	//address	data
	{0x0100,	0x00},//stand by
	{0x302C,	0x01},//XMSTA: master operation ready(default)

	//BLC
	{0x0009,	0x3F},
	//FRM_LENGTH: 320h->800
	{0x0340, 0x03},
	{0x0341, 0x20},
	//LINE_LENGTH: 8CAh->2250
	{0x0342, 0x08},
	{0x0343, 0xCA},
	//X_ADD_STA: 
	{0x0344,	0x00},
	{0x0345,	0x2A},
	//Y_ADD_STA: 
	{0x0346,	0x00},
	{0x0347,	0xA2},
	//cropping size in window cropping mode
	//X_OUT_SIZE
	{0x034C,	0x05},
	{0x034D,	0x10},
	//Y_OUT_SIZE
	{0x034E,	0x02},
	{0x034F,	0xD8},
	//register write: valid
	{0x3000,	0x31},
	//read out mode: window cropping mode
	{0x3002,	0x02},
	//output system: Others: Setting prohibited
	//output data rate: 324 Mbps
	{0x3011,	0x01},
	//fixed
	{0x3013,	0x40},
	{0x301F,	0x33},
	{0x3021,	0x30},
	//{0x3022,	0x40},
	{0x3022,	0x44},
	{0x3027,	0x20},
	{0x3040,	0x42},
	{0x3075,	0x07},
	{0x3076,	0x80},
	{0x3107,	0xFA},
	{0x310A,	0x30},
	{0x310B,	0x38},
	{0x3112,	0x10},
	{0x3116,	0x33},
	{0x3117,	0x4D},
	{0x3128,	0x05},
	{0x3173,	0x50},
	{0x317D,	0x8A},
	{0x317E,	0x60},
	{0x3185,	0x70},
	{0x3186,	0x42},
	{0x3187,	0x16},
	{0x3188,	0x00},
	{0x3189,	0x61},
	{0x318A,	0x0B},
	{0x318B,	0x29},
	{0x318C,	0x74},
	{0x318D,	0x81},
	{0x318E,	0x10},
	{0x318F,	0xBA},
	{0x31B1,	0xE8},
	{0x31B2,	0x82},

	//XMAST: master operation start
	{0x302C,	0x00},
	//standby control: Normal operation
	{0x0100,	0x01},
};

#if 0
t_RegSettingWB code gc_IMX076LQZC_QVGA_60FPS_Setting[] =
{
	//320x240@60fps 10bit
	//parallel CMOS SDR output
	//INCK=54MHz


	//address	data
	{0x0100,	0x00},//stand by
	{0x302C,	0x01},//XMSTA: master operation ready(default)

	//BLC
	{0x0009,	0x3F},
	//FRM_LENGTH: 320h->800
	//{0x0340, 0x04},
	//{0x0341, 0x42},
	//{0x0340, 0x03},
	//{0x0341, 0x20},
	{0x0340, 0x01},
	{0x0341, 0x90},
	//LINE_LENGTH: 465h->1125
	//{0x0342, 0x03},
	//{0x0343, 0x39},
	//{0x0342, 0x04},
	//{0x0343, 0x65},
	{0x0342, 0x08},
	{0x0343, 0xCA},
	//X_ADD_STA: 
	{0x0344,	0x02},
	{0x0345,	0x0C},
	//Y_ADD_STA: 
	{0x0346,	0x01},
	{0x0347,	0x94},
	//cropping size in window cropping mode
	//X_OUT_SIZE
	{0x034C,	0x01},
	{0x034D,	0x54},
	//Y_OUT_SIZE
	{0x034E,	0x01},
	{0x034F,	0x04},
	//register write: valid
	{0x3000,	0x31},
	//read out mode: window cropping mode
	{0x3002,	0x02},
	//output system: Others: Setting prohibited
	//output data rate: 324 Mbps
	//{0x3011,	0x00},
	{0x3011,	0x01},
	//fixed
	{0x3013,	0x40},
	{0x301F,	0x33},
	//{0x3021, 0x00},
	//{0x3022,	0x40},
	{0x3021, 0x30},
	{0x3022,	0x44},
	{0x3027,	0x20},
	{0x3040,	0x42},
	{0x3075,	0x07},
	{0x3076,	0x80},
	{0x3107,	0xFA},
	{0x310A,	0x30},
	{0x310B,	0x38},
	{0x3112,	0x10},
	{0x3116,	0x33},
	{0x3117,	0x4D},
	{0x3128,	0x05},
	{0x3173,	0x50},
	{0x317D,	0x8A},
	{0x317E,	0x60},
	{0x3185,	0x70},
	{0x3186,	0x42},
	{0x3187,	0x16},
	{0x3188,	0x00},
	{0x3189,	0x61},
	{0x318A,	0x0B},
	{0x318B,	0x29},
	{0x318C,	0x74},
	{0x318D,	0x81},
	{0x318E,	0x10},
	{0x318F,	0xBA},
	{0x31B1,	0xE8},
	{0x31B2,	0x82},

	//XMAST: master operation start
	{0x302C,	0x00},
	//standby control: Normal operation
	{0x0100,	0x01}
};
#endif

t_RegSettingWB code gc_IMX076LQZC_1368X524_60FPS_Setting[] =
{
	//1368x524@30fps 10bit
	//parallel CMOS SDR output
	//INCK=54MHz
	//vertical 1/2 elimination

	//address	data
	{0x0100,	0x00},
	{0x302C,	0x01},


	{0x0009,	0x3F},
	//552
	{0x0340,	0x02},
	{0x0341,	0x28},
	//1650
	{0x0342,	0x06},
	{0x0343,	0x72},
	{0x0344,	0x00},
	{0x0345,	0x00},
	{0x0346,	0x00},
	{0x0347,	0x00},
	//1368
	{0x034C,	0x05},
	{0x034D,	0x58},
	//1049
	{0x034E,	0x04},
	{0x034F,	0x19},
	{0x3000,	0x31},
	{0x3002,	0x04},
	{0x3011,	0x01},
	{0x3013,	0x40},
	{0x301F,	0x33},
	{0x3021,	0x30},
	{0x3022,	0x40},
	//{0x3022,	0x44},
	{0x3027,	0x20},
	//{0x302C,	0x00},
	{0x3040,	0x42},
	{0x3075,	0x07},
	{0x3076,	0x80},
	{0x3107,	0xFA},
	{0x310A,	0x30},
	{0x310B,	0x38},
	{0x3112,	0x10},
	{0x3116,	0x33},
	{0x3117,	0x4D},
	{0x3128,	0x05},
	{0x3173,	0x50},
	{0x317D,	0x8A},
	{0x317E,	0x60},
	{0x3185,	0x70},
	{0x3186,	0x42},
	{0x3187,	0x16},
	{0x3188,	0x00},
	{0x3189,	0x61},
	{0x318A,	0x0B},
	{0x318B,	0x29},
	{0x318C,	0x74},
	{0x318D,	0x81},
	{0x318E,	0x10},
	{0x318F,	0xBA},
	{0x31B1,	0xE8},
	{0x31B2,	0x82},


	{0x302C,	0x00},
	{0x0100,	0x01}
};

static OV_FpsSetting_t*  GetOvFpsSetting(U8 Fps, OV_FpsSetting_t staOvFpsSetting[], U8 byArrayLen)
{
	U8 i;
	U8 Idx;

	ISP_MSG((" array size = %bd\n", byArrayLen));

	if(g_bIsHighSpeed)
	{
		Idx=2; //8fps for uxga, 10 fps for vga
	}
	else
	{
		Idx = 0;
	}

	for(i=0; i< byArrayLen; i++)
	{
		ISP_MSG((" fps i = %bd\n",i));
		if(staOvFpsSetting[i].byFps ==Fps)
		{
			Idx=i;
			break;
		}
	}
	ISP_MSG((" get fps idx = %bd\n",Idx));
	return &staOvFpsSetting[Idx];
}

static U8 MapSnrGlbGain2SnrRegSetting(float fGain)
{
	U8 bySnrRegGain = 0;

	if(fGain<1.0)
	{
		bySnrRegGain = 0x00;
	}
	else if(fGain<=125.893)
	{
		//bySnrRegGain = (U16)(log10((double)fGain)/0.03);
		bySnrRegGain = (U16)(log10((double)fGain)/0.015);
	}
	else
	{
		bySnrRegGain = 0x8c;
	}
	
	return bySnrRegGain;
}

float SnrRegSetting2SnrGlbGain(U8 bySnrRegGain)
{
	//return (float)pow(10.0, 0.03*(double)bySnrRegGain);
	return (float)pow(10.0, 0.015*(double)bySnrRegGain);
}

//this function is also called during Probe stage to calculate the transfer_size/LWM/HWM
void GetSensorPclkHsync(U16 wSensorSPFormat,U8 byFps)
{
	OV_FpsSetting_t *pIMX076LQZC_FpsSetting;

	//wSensorSPFormat =wSensorSPFormat;

	if(VGA_FRM == wSensorSPFormat)
	{
		pIMX076LQZC_FpsSetting=GetOvFpsSetting(byFps, g_staIMX076LQZCFpsSetting_VGA, sizeof(g_staIMX076LQZCFpsSetting_VGA)/sizeof(OV_FpsSetting_t));
	}
	else	//HD720P_FRM
	{
		pIMX076LQZC_FpsSetting=GetOvFpsSetting(byFps, g_staIMX076LQZCFpsSetting_720P, sizeof(g_staIMX076LQZCFpsSetting_720P)/sizeof(OV_FpsSetting_t));
	}
	g_wSensorHsyncWidth = pIMX076LQZC_FpsSetting->wExtraDummyPixel; // this for manual exposure
	g_dwPclk = pIMX076LQZC_FpsSetting->dwPixelClk;		// this for scale speed
}

void IMX076LQZCSetFormatFps(U16 SetFormat, U8 Fps)
{
	U16 wTemp;
	OV_FpsSetting_t *pIMX076LQZC_FpsSetting;

	// hemonel 2010-04-13: for delete complier warning
	SetFormat = SetFormat;

	

	// initial all register setting
	if(VGA_FRM == SetFormat)
	{
		pIMX076LQZC_FpsSetting=GetOvFpsSetting(Fps, g_staIMX076LQZCFpsSetting_VGA, sizeof(g_staIMX076LQZCFpsSetting_VGA)/sizeof(OV_FpsSetting_t));
		WriteSensorSettingWB(sizeof(gc_IMX076LQZC_1368X524_60FPS_Setting)/3, gc_IMX076LQZC_1368X524_60FPS_Setting);
	}
	else	//HD720P_FRM
	{
		pIMX076LQZC_FpsSetting=GetOvFpsSetting(Fps, g_staIMX076LQZCFpsSetting_720P, sizeof(g_staIMX076LQZCFpsSetting_720P)/sizeof(OV_FpsSetting_t));
		WriteSensorSettingWB(sizeof(gc_IMX076LQZC_720P_30FPS_Setting)/3, gc_IMX076LQZC_720P_30FPS_Setting);
	}

	// 2) write sensor register
	//Write_SenReg(0x0305, pIMX076LQZC_FpsSetting->byClkrc);
	wTemp = pIMX076LQZC_FpsSetting->wExtraDummyPixel;
	Write_SenReg(0x0343, INT2CHAR(wTemp, 0)); //Write dummy pixel LSB
	Write_SenReg(0x0342, INT2CHAR(wTemp, 1)); //Write dummy pixel MSB

	//_________________________________________________________
	//if 0x3503=07
	// (1) set settings (2) (3)gain work (4)exposure work (5)dummy work

	//if 0x3503=17
	// (1) set settings (2) (3) (4)exposure work&gain work  (5)dummy work
	//Write_SenReg(0x3503, 0x17);
	//Write_SenReg(0x3819, 0x6C); // fix image blink at insert dummy

	// 3) update variable for AE
	//g_wSensorHsyncWidth = pIMX076LQZC_FpsSetting->wExtraDummyPixel; // this for manual exposure
	//g_dwPclk = pIMX076LQZC_FpsSetting->dwPixelClk;		// this for scale speed
	
	if(VGA_FRM == SetFormat)
	{
		GetSensorPclkHsync(VGA_FRM,Fps);
		g_wAECExposureRowMax = 523;	// this for max exposure time //-1
		g_wAEC_LineNumber =524;	// this for AE insert dummy line algothrim

		g_wSensorWidthBefBLC = 1280;
		g_wSensorHeightBefBLC = 480;
	}
	else	//HD720P_FRM
	{
		GetSensorPclkHsync(HD720P_FRM,Fps);
		g_wAECExposureRowMax = 799;	// this for max exposure time //-1
		g_wAEC_LineNumber = 800;	// this for AE insert dummy line algothrim

		g_wSensorWidthBefBLC = 1280;
		g_wSensorHeightBefBLC = 720;
	}

	InitIMX076LQZCIspParamsByResolution(g_wCurFrameWidth, g_wCurFrameHeight);

}

void CfgIMX076LQZCControlAttr(void)
{
	U8 i;

	g_bySensorSize = SENSOR_SIZE_HD720P;
	g_wSensorSPFormat =HD720P_FRM|VGA_FRM;//for IMX076, VGA_FRM is 1280x480
#ifdef _ENABLE_MJPEG_
	g_byQtableScale = 6;	//HD720P HD800P Setting=6 for 7 comp rate
#endif
	{
		memcpy(g_asOvCTT, gc_IMX076LQZC_CTT,sizeof(gc_IMX076LQZC_CTT));
	}

	{
		// hemonel 2010-09-08: support 1.8V sensor IO
#if (_CHIP_ID_ & _RTS5840_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V79;
#elif (_CHIP_ID_ & _RTS5832_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V85;
#elif (_CHIP_ID_ & _RTS5829B_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V82;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V80;
#endif

		g_bySnrPowerOnSeq = SNR_PWRCTL_SEQ_GPIO8|(SNR_PWRCTL_SEQ_SV18<<2| (SNR_PWRCTL_SEQ_SV28 <<4));
		g_bySnrPowerOffSeq = SNR_PWRCTL_SEQ_GPIO8| (SNR_PWRCTL_SEQ_SV18<<2)| (SNR_PWRCTL_SEQ_SV28 <<4);
	}

	{
		//====== resolution  setting ===========
		// -- preview ---
		g_aVideoFormat[0].byaVideoFrameTbl[0] = 7;		// resolution number
		g_aVideoFormat[0].byaVideoFrameTbl[1]=F_SEL_320_240;
		g_aVideoFormat[0].byaVideoFrameTbl[2]=F_SEL_320_180;
		//g_aVideoFormat[0].byaVideoFrameTbl[3]=F_SEL_424_240;
		g_aVideoFormat[0].byaVideoFrameTbl[3]=F_SEL_640_360;
		g_aVideoFormat[0].byaVideoFrameTbl[4]=F_SEL_640_480;
		g_aVideoFormat[0].byaVideoFrameTbl[5]=F_SEL_160_120;
		g_aVideoFormat[0].byaVideoFrameTbl[6]=F_SEL_160_90;
		g_aVideoFormat[0].byaVideoFrameTbl[7]=F_SEL_1280_720;
		
		//--still image--
#ifdef _ZERO_SHUTTLE_LAGGER_
		g_aVideoFormat[0].byaStillFrameTbl[0] = 5;	// resolution number
		g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_640_480;
		g_aVideoFormat[0].byaStillFrameTbl[2]=F_SEL_640_360;
		g_aVideoFormat[0].byaStillFrameTbl[3]=F_SEL_424_240;
		g_aVideoFormat[0].byaStillFrameTbl[4]=F_SEL_320_180;
		g_aVideoFormat[0].byaStillFrameTbl[5]=F_SEL_320_240;
#else
		g_aVideoFormat[0].byaStillFrameTbl[0] = 7;	// resolution number
		g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_320_240;
		g_aVideoFormat[0].byaStillFrameTbl[2]=F_SEL_320_180;
		g_aVideoFormat[0].byaStillFrameTbl[3]=F_SEL_640_360;
		g_aVideoFormat[0].byaStillFrameTbl[4]=F_SEL_640_480;
		g_aVideoFormat[0].byaStillFrameTbl[5]=F_SEL_160_120;
		g_aVideoFormat[0].byaStillFrameTbl[6]=F_SEL_160_90;
		g_aVideoFormat[0].byaStillFrameTbl[7]=F_SEL_1280_720;
		//g_aVideoFormat[0].byaStillFrameTbl[7]=F_SEL_424_240;
		//g_aVideoFormat[0].byaStillFrameTbl[8]=F_SEL_320_180;
#endif		



		//====== fps  setting ===========
		{
			for(i=0; i<10; i++)
			{
				/*
					F_SEL_640_480    (0)
					F_SEL_160_120    (1)
					F_SEL_176_144    (2)
					F_SEL_320_180	(3)
					F_SEL_320_200    (4)
					F_SEL_320_240    (5)
					F_SEL_352_288    (6)
					F_SEL_424_240	(7)
					F_SEL_640_360	(8)
					F_SEL_640_400    (9)
				*/
				g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_30;
			}
			for(i=10; i<18; i++)
			{
				/*
					F_SEL_800_600    (10)
					F_SEL_848_480	(11)
					F_SEL_960_540	 (12)
					F_SEL_1024_768   (13)
					F_SEL_1280_720   (14)
					F_SEL_1280_800   (15)
					F_SEL_1280_960   (16)
					F_SEL_1280_1024  (17)
				*/
				g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_10;
			}
		}
		//g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_800_600]= FPS_25;
		//g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_848_480]= FPS_25;
		//g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_960_540]= FPS_20;
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_320_240] |= FPS_60;
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_320_180] |= FPS_60;
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_160_120] |= FPS_60;		
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_160_90] = FPS_30|FPS_60;	

		//====== format type  setting ===========
		g_aVideoFormat[0].byFormatType = FORMAT_TYPE_YUY2;

#ifdef _ENABLE_MJPEG_
		// copy YUY2 setting to MJPEG setting
		memcpy(&(g_aVideoFormat[1]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

#ifdef _ZERO_SHUTTLE_LAGGER_
		g_aVideoFormat[1].byaStillFrameTbl[0] = 8;	// resolution number
		g_aVideoFormat[1].byaStillFrameTbl[1]=F_SEL_1280_720;
		g_aVideoFormat[1].byaStillFrameTbl[2]=F_SEL_320_240;
		g_aVideoFormat[1].byaStillFrameTbl[3]=F_SEL_640_480;
		g_aVideoFormat[1].byaStillFrameTbl[4]=F_SEL_960_540;
		g_aVideoFormat[1].byaStillFrameTbl[5]=F_SEL_848_480;
		g_aVideoFormat[1].byaStillFrameTbl[6]=F_SEL_640_360;
		g_aVideoFormat[1].byaStillFrameTbl[7]=F_SEL_424_240;
		g_aVideoFormat[1].byaStillFrameTbl[8]=F_SEL_320_180;
#endif

		// modify MJPEG FPS setting
		for(i=10; i<18; i++)
		{
			g_aVideoFormat[1].waFrameFpsBitmap[i] = (FPS_30);
		}
		g_aVideoFormat[1].waFrameFpsBitmap[F_SEL_640_480] |= (FPS_60);
		g_aVideoFormat[1].waFrameFpsBitmap[F_SEL_640_360] |= (FPS_60);

		// modify MJPEG format type
		g_aVideoFormat[1].byFormatType = FORMAT_TYPE_MJPG;
#endif

#ifdef _ENABLE_M420_FMT_
		// copy YUY2 setting to M420 setting
		memcpy(&(g_aVideoFormat[2]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

		// modify M420 FPS setting
		{
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_800_600] =FPS_25|FPS_20|FPS_15|FPS_10|FPS_5;//|FPS_1;             //bit07 F_800_600
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_960_540] =FPS_25|FPS_20|FPS_15|FPS_10|FPS_5;//|FPS_5|FPS_3|FPS_1;                    //bit08 F_960_720
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1024_768] =FPS_15|FPS_10|FPS_5;//|FPS_1;                    //bit09 F_1024_768
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_720] =FPS_10|FPS_5;//FPS_9|FPS_8|FPS_1;    //bit10 F_1280_720
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_800] =FPS_10|FPS_5;

			// modify M420 format type
			g_aVideoFormat[2].byFormatType = FORMAT_TYPE_M420;
		}
#endif
	}

	{
		//Brightness - BrightnessItem

		//Contrast - ContrastItem

		//Saturation - SaturationItem

		//Hue - 	HueItem

		//Sharpness - SharpnessItem
		//	SharpnessItem.Min = 0;
		//	SharpnessItem.Max = 8;
		//	SharpnessItem.Res = 1;
		//	SharpnessItem.Def = 3;
		//	SharpnessItem.Last  =SharpnessItem.Def;

		// gamma - 	GammaItem

		// gain - GainItem

		// whitebalance - WhiteBalanceTempItem

		// backlight - BackLightCompItem

		// powerline -PwrLineFreqItem
			PwrLineFreqItem.Def = PWR_LINE_FRQ_60;
			PwrLineFreqItem.Last = PwrLineFreqItem.Def;

		// exposure time -	ExposureTimeAbsolutItem

		// low light - LowLightCompItem
	}

	InitIMX076LQZCIspParams();
}

void SetIMX076LQZCIntegrationTime(U16 wEspline)
{
#if 1
	U16 wFrameLenLines;
	U16 wEspline_ceil;
	U16 wExpMax = g_wAECExposureRowMax;

	if(wEspline > wExpMax)
	{
		wFrameLenLines = wEspline + 1;
	}
	else
	{
		wFrameLenLines = g_wAEC_LineNumber;
	}

	if (wFrameLenLines%2==1)
	{
		wFrameLenLines++;
	}

	// group register hold
	Write_SenReg(0x0104, 0x01);

	//-----------Write  frame length or dummy lines------------
	Write_SenReg(0x0341, INT2CHAR(wFrameLenLines, 0));
	Write_SenReg(0x0340, INT2CHAR(wFrameLenLines, 1));

	//-----------Write Exposuretime setting-----------
	wEspline_ceil =  ceil(wFrameLenLines - 0.3 -wEspline);
	Write_SenReg(0x0203, INT2CHAR(wEspline_ceil, 0));	// change exposure value low
	Write_SenReg(0x0202, INT2CHAR(wEspline_ceil, 1));	// change exposure value high

	//----------- write gain setting-------------------
#ifdef _RTK_EXTENDED_CTL_
	if(RtkExtISOItem.Last == ISO_AUTO)
	{
		Write_SenReg(0x301e, 0);	// fixed gain at manual exposure control(*1)
		SetISPAEGain(1, 1, 1);	//ISP gain fixed at *1
	}
#else
	//----------- write gain setting-------------------
	Write_SenReg(0x301e, 0);	// fixed gain at manual exposure control(*1)
	SetISPAEGain(1, 1, 1);	//ISP gain fixed at *1
#endif//#ifdef _RTK_EXTENDED_CTL_

	// group register release
	Write_SenReg(0x0104, 0);
#endif
}

#ifdef _RTK_EXTENDED_CTL_
void SetIMX076LQZCGain(float fGain)
{
	U8 byGain;
	float fSnrGlbGain;

	if( fabs(fGain)<0.0001 )//ISO auto
	{
		//ISO_MSG(("SetIMX076LQZCGain: ISO auto,  wGain = %u \n",wGain));
		//do noting
	}
	else
	{
		byGain = MapSnrGlbGain2SnrRegSetting(fGain*16.0);
		fSnrGlbGain = SnrRegSetting2SnrGlbGain(byGain);

		//----------- write gain setting-------------------
		//ISO_MSG(("SetIMX076LQZCGain: wGain = %u \n",wGain));
		//only analog gain, no digital gain
		Write_SenReg(0x301e, byGain);
		//Write_SenReg(0x0204, INT2CHAR(wGain, 1));
		SetISPAEGain(fGain, fSnrGlbGain, 1);
	}
}
#endif//#ifdef _RTK_EXTENDED_CTL_

void SetIMX076LQZCImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.
{

	WaitFrameSync(ISP_INT1,ISP_DATA_START_INT);

	//bySnrImgDir ^= 0x01; 	// IMX076LQZC output mirrored image at default, so need mirror the image for normal output
	Write_SenReg_Mask(0x101, bySnrImgDir, 0x03);

	WaitFrameSync(ISP_INT1,ISP_DATA_END_INT);

	if(bySnrImgDir & 0x01)
	{
		if(VGA_FRM == g_wSensorCurFormat)
		{
			//SetBkWindowStart(0x4f, 0x12);
			SetBkWindowStart(0x67, 0x12);
		}
		else //720P_FRM
		{
			SetBkWindowStart(0x13, 0x1f);
			//SetBkWindowStart(0, 0);
		}
	}
	else
	{
		//SetBkWindowStart(0, 1);
		if(VGA_FRM == g_wSensorCurFormat)
		{
			//SetBkWindowStart(0x4f, 0x12);
			SetBkWindowStart(0x67, 0x12);
		}
		else //720P_FRM
		{
			SetBkWindowStart(0x13, 0x1f);
			//SetBkWindowStart(0, 0);
		}
	}
}

void SetIMX076LQZCExposuretime_Gain(float fExpTime, float fTotalGain)
{
#if 1
	U16 wExposureRows_ceil;
	U8  byGainRegSetting; //data
	float fSnrGlbGain;
	float fExposureRows;
	U16 wSetDummy;
	float fTemp;
	U16 wExposurePixels;

	byGainRegSetting = MapSnrGlbGain2SnrRegSetting(fTotalGain);
	ISO_MSG(("byGainRegSetting,%bx\n",byGainRegSetting));
	
	fSnrGlbGain = SnrRegSetting2SnrGlbGain(byGainRegSetting);
	
	fExposureRows = fExpTime/g_fSensorRowTimes;	//unit : 1row
	//wExposureRows_floor = (U16)(fExposureRows);
	
	//fTemp = fExpTime-((float)wExposureRows_floor*(g_fSensorRowTimes)); //fine integration time;unit:  us
	//wExposurePixels =(U16)(fTemp * (float)(g_dwPclk/10000)) ; //unit: pixels

	// write exposure, gain, dummy line
	if(fabs(g_fCurExpTime-fExpTime) > 0.0001)
	{
		// set dummy
		wSetDummy = g_wAFRInsertDummylines + g_wAEC_LineNumber;

		// group register hold
		Write_SenReg(0x0104, 0x01);

		//-----------Write  frame length or dummy lines------------
		if (wSetDummy%2 == 1)
		{
			wSetDummy++;
		}
		Write_SenReg(0x0341, INT2CHAR(wSetDummy, 0));
		Write_SenReg(0x0340, INT2CHAR(wSetDummy, 1));

		//-----------Write Exposuretime setting-----------
		wExposureRows_ceil =  ceil(wSetDummy - 0.3 -fExposureRows);
		Write_SenReg(0x0203, INT2CHAR(wExposureRows_ceil, 0));	// change exposure value low
		Write_SenReg(0x0202, INT2CHAR(wExposureRows_ceil, 1));	// change exposure value high
		ISO_MSG(("wSetDummy,%u,byGainRegSetting,%bx\n",wSetDummy,byGainRegSetting));
		ISO_MSG(("wExposureRows_ceil,%u\n",wExposureRows_ceil));
		
		//Reck added 20100625, fix IMX076LQZC red edge bug ?
		//if (wExposurePixels>1280)
		//{
		//	wExposurePixels = 1280;
		//}
		//Write_SenReg(0x0201, INT2CHAR(wExposurePixels, 0));	// change exposure value low
		//Write_SenReg(0x0200, INT2CHAR(wExposurePixels, 1));	// change exposure value high

		// group register release
		Write_SenReg(0x0104, 0);

		//----------- write gain setting-------------------
		WaitFrameSync(ISP_INT1,ISP_DATA_START_INT);
		Write_SenReg(0x301e, byGainRegSetting);

		SetISPAEGain(fTotalGain, fSnrGlbGain, 1);
		g_fCurExpTime = fExpTime;
	}
	else
	{
		// group register hold
		//Write_SenReg(0x0104, 1);
		wSetDummy = g_wAFRInsertDummylines + g_wAEC_LineNumber;

		//-----------Write  frame length or dummy lines------------
		if (wSetDummy%2 == 1)
		{
			wSetDummy++;
		}
		Write_SenReg(0x0341, INT2CHAR(wSetDummy, 0));
		Write_SenReg(0x0340, INT2CHAR(wSetDummy, 1));

		// write gain setting
		Write_SenReg(0x301e, byGainRegSetting);
		
		SetISPAEGain(fTotalGain, fSnrGlbGain, 1);

		// group register release
		//Write_SenReg(0x0104, 0);
	}
	//ISO_MSG(("wSetDummy,%u,byGainRegSetting,%bx\n",wSetDummy,byGainRegSetting));

	
	return;
#endif
}

void IMX076LQZC_POR(void )
{
	Init_CCS();

	ENTER_SENSOR_RESET();
	// power on
	SensorPowerControl(SWITCH_ON,EN_DELAYTIME);
	uDelay(1);

	// clock select 37.125M
	//CHANGE_CCS_CLK(CCS_CLK_SEL_SSC_24M);
	XBYTE[0xFF20] = 0x1F;
       XBYTE[0xFF21] = 0x39;
      // XBYTE[0xFF22] = 0x27;
       XBYTE[0xFF22] = 0x6A;
       //XBYTE[0xFF23] = 0x01;
       XBYTE[0xFF23] = 0x00;
       XBYTE[0xFF25] = 0x05;
       XBYTE[0xFF26] = 0x7A;
       XBYTE[0xFF27] = 0x43;
       XBYTE[0xFF24] = 0x07;
       XBYTE[0xFEC0] = 0x71;

	EnableSensorHCLK();
	Delay(1);
	// Initialize I2C Controller Timing
	LEAVE_SENSOR_RESET();
	
	g_bySensorIsOpen = SENSOR_OPEN;
	WaitTimeOut_Delay(1);  //IMX076LQZC spec request 8192clk
}

void InitIMX076LQZCIspParams(void )
{
	// AWB initial gain
	//use D50 R/G/B gain as AWB initial value
	g_wAWBRGain_Last= 0x17C;
	g_wAWBGGain_Last= 0x100;
	g_wAWBBGain_Last= 0x226;
	g_wProjectGR = g_wAWBRGain_Last;
	g_wProjectGB = g_wAWBBGain_Last;

	// Special ISP
	g_bySaturation_Def =64;
	g_byContrast_Def = 32; //Neil Tuning at chicony
	g_byTgamma_rate_max=63;
	g_byTgamma_rate_min =20;

	g_wDynamicISPEn = 0;
	//g_wDynamicISPEn = DYNAMIC_LSC_CT_EN| DYNAMIC_LSC_EN | DYNAMIC_SHARPNESS_EN | DYNAMIC_SHARPPARAM_EN | DYNAMIC_GAMMA_EN | DYNAMIC_CCM_CT_EN;
}

void InitIMX076LQZCIspParamsByResolution(U16 wCurWidth, U16 wCurHeight)
{
	wCurWidth = wCurWidth;
	wCurHeight = wCurHeight;
}

void SetIMX076LQZCDynamicISP(U8 byAEC_Gain)
{
	byAEC_Gain = byAEC_Gain;
}

void SetIMX076LQZCDynamicISP_AWB(U16 wColorTempature)
{
	wColorTempature =wColorTempature;

	// hemonel 2011-07-04: Albert use only one LSC curve to pass MSOC premium test, so delete these code
	/*
	#ifdef _MSOC_TEST_
	U8 i;

	// LSC Curve dynamic
	if ( (g_wDynamicISPEn&DYNAMIC_LSC_EN) == DYNAMIC_LSC_EN)
	{
		if(wColorTempature < 4000)
		{
	          		for (i=0;i<6;i++)
	          		{
	          			g_awLensShadingCenter[i] = gc_wLSC_Center_Offset_IMX076LQZC_3500K[i];
	          		}

				for (i=0;i<48;i++)
				{
					XBYTE[ISP_NLSC_CURVE_R_BASE+i]  = gc_byLSC_RGB_Curve_IMX076LQZC_3500K[0][i];
					XBYTE[ISP_NLSC_CURVE_G_BASE+i]  = gc_byLSC_RGB_Curve_IMX076LQZC_3500K[1][i];
					XBYTE[ISP_NLSC_CURVE_B_BASE+i]  = gc_byLSC_RGB_Curve_IMX076LQZC_3500K[2][i];
				}
		}
		else if(wColorTempature > 4500)
		{
	          		for (i=0;i<6;i++)
	          		{
	          			g_awLensShadingCenter[i] = gc_wLSC_Center_Offset_IMX076LQZC_D65[i];
	          		}

				for (i=0;i<48;i++)
				{
					XBYTE[ISP_NLSC_CURVE_R_BASE+i]  = gc_byLSC_RGB_Curve_IMX076LQZC_D65[0][i];
					XBYTE[ISP_NLSC_CURVE_G_BASE+i]  = gc_byLSC_RGB_Curve_IMX076LQZC_D65[1][i];
					XBYTE[ISP_NLSC_CURVE_B_BASE+i]  = gc_byLSC_RGB_Curve_IMX076LQZC_D65[2][i];
				}
		}

	}
	#endif
	*/
}
#endif
