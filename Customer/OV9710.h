#ifndef _OV9710_H_
#define _OV9710_H_

void OV9710SetFormatFps(U8 SetFormat, U8 Fps);
void CfgOV9710ControlAttr(void);
void SetOV9710ImgDir(U8 bySnrImgDir);
void SetOV9710IntegrationTime(U16 wEspline);
void SetOV9710Exposuretime_Gain(float fExpTime, float fTotalGain);
void OV9710_POR();
void InitOV9710IspParams();
void SetOV9710DynamicISP(U8 byAEC_Gain);
void SetOV9710DynamicISP_AWB(U16 wColorTempature);
void SetOV9710Gain(float fGain);
#endif // _OV9710_H_

