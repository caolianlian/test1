#include "Inc.h"

#ifdef RTS58XX_SP_IMX208PQH5

U8   gc_digital_gain_h;

#ifdef _ENABLE_OUTDOOR_DETECT_
Dyanmic_AWB_Boundary_t code gc_AWBDynmaicBoundary = {28, -8, 16, 8, 50, 30, 2, 21};
#endif

OV_CTT_t code gc_tIMX208PQH5_CTT[3] =
{
	{2800, 0x112, 0x100, 0x250},
	{4000, 0x190, 0x100, 0x190},
	{6500, 0x1D8, 0x100, 0x124},
};

code OV_FpsSetting_t  g_staIMX208PQH5FpsSetting[] =
{
//    FPS       ExtraDummyPixel   clkrc 	    pclk
//    {3,		(22500),	     (4),		81000000},
	{5,		(13498),	     (4),		81000000},
	{10,		(6748),	     (4), 	       81000000},
	{15,		(4500),	     (4), 		81000000},
	{20,		(3374),	     (4), 		81000000},
	{25,		(2668),	     (4),		81000000},
	{30,		(2248),	     (4),		81000000}, // adjust fps >=30fps for msoc jitter
};

// Sensor setting
t_RegSettingWB code gc_IMX208PQH5_mipi_Setting[] =
{	
	//----PLL----
	{0x0305, 0x04},
	{0x0307, 0x87},
	{0x303C, 0x4b},
	{0x30a4, 0x02},
      //MODE SETTING
	{0x0112, 0x0a},
	{0x0113, 0x0a},
	{0x0340, 0x04},
	{0x0341, 0xb0},
	{0x0342, 0x08},
	{0x0343, 0xC8},
	{0x0344, 0x00},
	{0x0345, 0x00},
	{0x0346, 0x00},
	{0x0347, 0x00},
	{0x0348, 0x07},
	{0x0349, 0x8f},
	{0x034a, 0x04},
	{0x034b, 0x47},
	{0x034c, 0x07},
	{0x034d, 0x90},
	{0x034e, 0x04},
	{0x034f, 0x48},
	{0x0381, 0x01},
	{0x0383, 0x01},
	{0x0385, 0x01},
	{0x0387, 0x01},
	{0x3048, 0x00},
	{0x304e, 0x0a},
	{0x3050, 0x02},
	{0x309b, 0x00},
	{0x30d5, 0x00},
	{0x3301, 0x01},
	{0x3318, 0x61},
	{0x20F, 0},
	{0x211, 0},
	{0x213, 0},
	{0x215, 0},
	{0x20F, 0},
	{0x211, 0},
	{0x213, 0},
	{0x215, 0},
	//shutter gain setting
	{0x0101, 0x01},
	{0x0202, 0x01},
	{0x0203, 0x90},
	{0x0205, 0x00},
	//streaming
	{0x0100, 0x01},
};

static OV_FpsSetting_t*  GetOvFpsSetting(U8 Fps, OV_FpsSetting_t staOvFpsSetting[], U8 byArrayLen)
{
	U8 i;
	U8 Idx;

	ISP_MSG((" array size = %bd\n", byArrayLen));

	if(g_bIsHighSpeed)
	{
		Idx=2; //Default FPS in Highspeed:15
	}
	else
	{
		Idx = 0; //Default FPS in FULL or LOW speed:5
	}

	for(i=0; i< byArrayLen; i++)
	{
		ISP_MSG((" fps i = %bd\n",i));
		
		if(staOvFpsSetting[i].byFps ==Fps)
		{
			Idx=i;
			break;
		}
	}
	
	ISP_MSG((" get fps idx = %bd\n",Idx));
	
	return &staOvFpsSetting[Idx];
}


static U8 MapSnrGlbGain2SnrRegSetting(float wGain)
{
	U8  bySensorGain=0;
		
	if(wGain <= 128) // MAX Analog Gain : 128=8*16
	{
		gc_digital_gain_h = 1;
	}
	else if(wGain <= 256)
	{
		gc_digital_gain_h = 2;
	}
	else if(wGain <= 512)
	{
		gc_digital_gain_h = 4;
	}
	else if(wGain <= 1024)
	{
		gc_digital_gain_h = 8;
	}
	else
	{
		gc_digital_gain_h = 16;
	}
		
	bySensorGain = 256 - (U8)(4096*gc_digital_gain_h/wGain);
			
	return bySensorGain;
}

static float SnrRegSetting2SnrGlbGain(U8 wSnrRegGain)
{
	return 256.0/(256.0-(float)wSnrRegGain)*(float)gc_digital_gain_h;
}

//this function is also called during Probe stage to calculate the transfer_size/LWM/HWM
void GetSensorPclkHsync(U16 wSensorSPFormat,U8 byFps)
{
	OV_FpsSetting_t *pIMX208PQH5_FpsSetting=NULL;
	
	// hemonel 2010-04-13: for delete complier warning
	wSensorSPFormat =wSensorSPFormat;
	
	pIMX208PQH5_FpsSetting=GetOvFpsSetting(byFps, g_staIMX208PQH5FpsSetting, sizeof(g_staIMX208PQH5FpsSetting)/sizeof(OV_FpsSetting_t));
	
	g_wSensorHsyncWidth = pIMX208PQH5_FpsSetting->wExtraDummyPixel; // this for manual exposure
	g_dwPclk = pIMX208PQH5_FpsSetting->dwPixelClk;		// this for scale speed
}

void IMX208PQH5SetFormatFps(U16 SetFormat, U8 Fps)
{
	U16 wTemp;
	OV_FpsSetting_t *pIMX208PQH5_FpsSetting;
	
	SetFormat = SetFormat;

	pIMX208PQH5_FpsSetting = GetOvFpsSetting(Fps, g_staIMX208PQH5FpsSetting, sizeof(g_staIMX208PQH5FpsSetting)/sizeof(OV_FpsSetting_t));

	// initial all register setting
	WriteSensorSettingWB(sizeof(gc_IMX208PQH5_mipi_Setting)/3, gc_IMX208PQH5_mipi_Setting);

	// 2) write sensor register for current FPS
	Write_SenReg(0x0305, pIMX208PQH5_FpsSetting->byClkrc);
	wTemp = pIMX208PQH5_FpsSetting->wExtraDummyPixel;
	Write_SenReg(0x0343, INT2CHAR(wTemp, 0)); //Write dummy pixel LSB
	Write_SenReg(0x0342, INT2CHAR(wTemp, 1)); //Write dummy pixel MSB

// 3) update variable for AE
       GetSensorPclkHsync(HD1080P_FRM,Fps);
       
	g_wAECExposureRowMax = 1200-5;	// this for max exposure time
	g_wAEC_LineNumber = 1200;	// this for AE insert dummy line algothrim

	g_wSensorWidthBefBLC = 1920;
	g_wSensorHeightBefBLC = 1080;
		
	InitIMX208PQH5IspParamsByResolution(g_wCurFrameWidth, g_wCurFrameHeight);

	//-----------------------------Dphy Setting--------------------------------
	SetMipiDphy(MIPI_DATA_LANE0_EN, \
	            MIPI_DATA_FORMAT_RAW10 , MIPI_DATA_TYPE_RAW10, \
	            HSTERM_EN_TIME_33);   
}

void CfgIMX208PQH5ControlAttr(void)
{
	U8 i;

	g_bySensorSize = SENSOR_SIZE_FHD;
	g_wSensorSPFormat = HD1080P_FRM;

	memcpy(g_asOvCTT, gc_tIMX208PQH5_CTT,sizeof(gc_tIMX208PQH5_CTT));//get manual white balance

	//  SVA:1.2V SVIO:1.8V
#if (_CHIP_ID_ & _RTS5840_)
	XBYTE[REG_TUNESVA] =  SVA_VOL_1V16;
	XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V79;
#elif (_CHIP_ID_ & _RTS5832_)
	XBYTE[REG_TUNESVA] =  SVA_VOL_1V20;
	XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V85;
#elif (_CHIP_ID_ & _RTS5829B_)
	XBYTE[REG_TUNESVA] =  SVA_VOL_1V17;
	XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V80;
#endif

	//====== resolution  setting ===========
	// -- preview ---
	g_aVideoFormat[0].byaVideoFrameTbl[0] = 9;		// resolution number
	g_aVideoFormat[0].byaVideoFrameTbl[1] = F_SEL_640_480;
	g_aVideoFormat[0].byaVideoFrameTbl[2] = F_SEL_320_180;
	g_aVideoFormat[0].byaVideoFrameTbl[3] = F_SEL_320_240;
	g_aVideoFormat[0].byaVideoFrameTbl[4] = F_SEL_424_240;
	g_aVideoFormat[0].byaVideoFrameTbl[5] = F_SEL_640_360;
	g_aVideoFormat[0].byaVideoFrameTbl[6] = F_SEL_848_480;
	g_aVideoFormat[0].byaVideoFrameTbl[7] = F_SEL_960_540;
	g_aVideoFormat[0].byaVideoFrameTbl[8] = F_SEL_1280_720;
	g_aVideoFormat[0].byaVideoFrameTbl[9] = F_SEL_1920_1080;
		//--still image--
	g_aVideoFormat[0].byaStillFrameTbl[0] = 9;	// resolution number
	g_aVideoFormat[0].byaStillFrameTbl[1] = F_SEL_640_480;
	g_aVideoFormat[0].byaStillFrameTbl[2] = F_SEL_320_180;
	g_aVideoFormat[0].byaStillFrameTbl[3] = F_SEL_320_240;
	g_aVideoFormat[0].byaStillFrameTbl[4] = F_SEL_424_240;
       g_aVideoFormat[0].byaStillFrameTbl[5] = F_SEL_640_360;
	g_aVideoFormat[0].byaStillFrameTbl[6] = F_SEL_848_480;
	g_aVideoFormat[0].byaStillFrameTbl[7] = F_SEL_960_540;
	g_aVideoFormat[0].byaStillFrameTbl[8] = F_SEL_1280_720;
	g_aVideoFormat[0].byaStillFrameTbl[9] = F_SEL_1920_1080;
	
	//====== fps  setting ===========		
	for(i=0; i<10; i++)
	{
		g_aVideoFormat[0].waFrameFpsBitmap[i] = FPS_15|FPS_20|FPS_25|FPS_30;
	}
	
	for(i=10; i<19; i++)
	{
		g_aVideoFormat[0].waFrameFpsBitmap[i] = FPS_10|FPS_5;
	}
	
	g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_1920_1080] = FPS_5;

	//====== format type  setting ===========
	g_aVideoFormat[0].byFormatType = FORMAT_TYPE_YUY2;

#ifdef _ENABLE_MJPEG_
	// copy YUY2 setting to MJPEG setting
	memcpy(&(g_aVideoFormat[1]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

	// modify MJPEG FPS setting
	for(i=10; i<20; i++)
	{
		g_aVideoFormat[1].waFrameFpsBitmap[i] = (FPS_15|FPS_20|FPS_25|FPS_30);
	}

	// modify MJPEG format type
	g_aVideoFormat[1].byFormatType = FORMAT_TYPE_MJPG;
#endif

#ifdef _ENABLE_M420_FMT_
	// copy YUY2 setting to M420 setting
	memcpy(&(g_aVideoFormat[2]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

	// modify M420 FPS setting
	g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_800_600] = FPS_25|FPS_20|FPS_15|FPS_10|FPS_5;        
	g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_960_540] = FPS_25|FPS_20|FPS_15|FPS_10|FPS_5;        
	g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1024_768] = FPS_15|FPS_10|FPS_5;                  
	g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_720] = FPS_10|FPS_5;  
	
	// modify M420 format type
	g_aVideoFormat[2].byFormatType = FORMAT_TYPE_M420;
	
#endif
	
	InitIMX208PQH5IspParams();
}

void SetIMX208PQH5IntegrationTime(U16 wEspline)
{
       Write_SenReg(0x0104, 1);
	//-----------Write Exposuretime setting-----------
	Write_SenReg(0x0203, INT2CHAR(wEspline, 0));	// change exposure value low
	Write_SenReg(0x0202, INT2CHAR(wEspline, 1));	// change exposure value high

	//----------- write gain setting-------------------
	Write_SenReg(0x0205, 0x0f);	// fixed gain at manual exposure control
	
       Write_SenReg(0x0104, 0);
}

void SetIMX208PQH5ImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.
{

	WaitFrameSync(ISP_INT1, ISP_DATA_START_INT);

	bySnrImgDir ^= 0x00;
	Write_SenReg_Mask(0x101, bySnrImgDir, 0x03);

	WaitFrameSync(ISP_INT1, ISP_DATA_END_INT);
	
	switch(bySnrImgDir)
	{
	       case 0:	
		SetBLCWindowStart(1, 0);
                             break;
              case 1:			  		
		SetBLCWindowStart(0, 0);	   	
                             break;
	}
}

void SetIMX208PQH5Exposuretime_Gain(float fExpTime, float fTotalGain)
{
	U16  wExposureRows_floor;
	U16  wGainRegSetting; //data
	float  fSnrGlbGain;
	U16   wSetDummy;
	//float fTemp;
	//U16 wExposurePixels;

	wGainRegSetting=MapSnrGlbGain2SnrRegSetting((U16)(fTotalGain*16.0));
	fSnrGlbGain = SnrRegSetting2SnrGlbGain(wGainRegSetting);

	wExposureRows_floor = (U16)(fExpTime/g_fSensorRowTimes);  //unit : 1row
//	fTemp = (float)wExpTime-((float)wExposureRows_floor*(g_fSensorRowTimes)); //fine integration time;unit:  us
//	wExposurePixels =(U16)(fTemp * (float)(g_dwPclk/10000)) ; //unit: pixels

	// set dummy
	wSetDummy = g_wAFRInsertDummylines + g_wAEC_LineNumber;
	//-----------Write  frame length or dummy lines------------
	if (wSetDummy%2 == 1)
	{
		wSetDummy++;
	}
	
	// write exposure, gain, dummy line
	if(fabs(g_fCurExpTime-fExpTime) > 0.0001)
	{
		// group register hold
		Write_SenReg(0x0104, 0x01);

		Write_SenReg(0x0341, INT2CHAR(wSetDummy, 0));
		Write_SenReg(0x0340, INT2CHAR(wSetDummy, 1));

		//Write Exposuretime setting
		Write_SenReg(0x0203, INT2CHAR(wExposureRows_floor, 0));	// coarse_integration_time  low
		Write_SenReg(0x0202, INT2CHAR(wExposureRows_floor, 1));	// coarse_integration_time  high
		
		//write analog gain setting
		Write_SenReg(0x0205, wGainRegSetting);

		//set digital gain
		Write_SenReg(0x20E, gc_digital_gain_h);
		Write_SenReg(0x210, gc_digital_gain_h);
		Write_SenReg(0x212, gc_digital_gain_h);
		Write_SenReg(0x214, gc_digital_gain_h);
	
		SetISPAEGain(fTotalGain, fSnrGlbGain, 1); //delay 1 frame

		//group register release
		Write_SenReg(0x0104, 0);
		
		g_fCurExpTime = fExpTime;	// back up exposure rows
	}
	else
	{
		// group register hold
		Write_SenReg(0x0104, 1);

		Write_SenReg(0x0341, INT2CHAR(wSetDummy, 0));
		Write_SenReg(0x0340, INT2CHAR(wSetDummy, 1));
		
		// write analog gain setting
		Write_SenReg(0x0205, wGainRegSetting);

		//set digital gain
		Write_SenReg(0x20E, gc_digital_gain_h);
		Write_SenReg(0x210, gc_digital_gain_h);
		Write_SenReg(0x212, gc_digital_gain_h);
		Write_SenReg(0x214, gc_digital_gain_h);

		SetISPAEGain(fTotalGain, fSnrGlbGain, 1);

		// group register release
		Write_SenReg(0x0104, 0);
	}
	
	return;
}

void IMX208PQH5_POR(void)
{
	Init_MIPI();

	ENTER_SENSOR_RESET();
	// power on
	SensorPowerControl(SWITCH_ON, EN_DELAYTIME);
	uDelay(1);
	// Initialize I2C Controller Timing
	LEAVE_SENSOR_RESET();
	// clock select 24M
	if( g_bySSCEnable== TRUE)
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_SSC_24M);
	}
	else
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_96M|CCS_CLK_DIVIDER_4);
	}

	EnableSensorHCLK();
	g_bySensorIsOpen = SENSOR_OPEN;
	WaitTimeOut_Delay(1);  //OV9726 spec request 8192clk
}

static void InitIMX208PQH5IspParams(void)
{
	// AWB initial gain
	//use D50 R/G/B gain as AWB initial value
	g_wAWBRGain_Last = 0x17C;
	g_wAWBGGain_Last = 0x100;
	g_wAWBBGain_Last = 0x226;
	g_wProjectGR = g_wAWBRGain_Last;
	g_wProjectGB = g_wAWBBGain_Last;

	// Special ISP
	g_bySaturation_Def = 64;
	g_byContrast_Def = 32; //Neil Tuning at chicony
	g_byTgamma_rate_max = 63;
	g_byTgamma_rate_min = 20;

	//g_wDynamicISPEn = 0;
	g_wDynamicISPEn = DYNAMIC_LSC_CT_EN| DYNAMIC_LSC_EN | DYNAMIC_SHARPNESS_EN | DYNAMIC_SHARPPARAM_EN | DYNAMIC_GAMMA_EN | DYNAMIC_CCM_CT_EN;

#ifdef _ENABLE_MJPEG_
	g_byQtableScale = 6;	//HD720P HD800P Setting=6 for 7 comp rate
#endif
}

void InitIMX208PQH5IspParamsByResolution(U16 wCurWidth, U16 wCurHeight)
{
	wCurWidth = wCurWidth;
	wCurHeight = wCurHeight;
}

void SetIMX208PQH5DynamicISP(U8 byAEC_Gain)
{
	byAEC_Gain = byAEC_Gain;
}

void SetIMX208PQH5DynamicISP_AWB(U16 wColorTempature)
{
	wColorTempature =wColorTempature;

	// hemonel 2011-07-04: Albert use only one LSC curve to pass MSOC premium test, so delete these code
	/*
	#ifdef _MSOC_TEST_
	U8 i;

	// LSC Curve dynamic
	if ( (g_wDynamicISPEn&DYNAMIC_LSC_EN) == DYNAMIC_LSC_EN)
	{
		if(wColorTempature < 4000)
		{
	          		for (i=0;i<6;i++)
	          		{
	          			g_awLensShadingCenter[i] = gc_wLSC_Center_Offset_OV9726_3500K[i];
	          		}

				for (i=0;i<48;i++)
				{
					XBYTE[ISP_NLSC_CURVE_R_BASE+i]  = gc_byLSC_RGB_Curve_OV9726_3500K[0][i];
					XBYTE[ISP_NLSC_CURVE_G_BASE+i]  = gc_byLSC_RGB_Curve_OV9726_3500K[1][i];
					XBYTE[ISP_NLSC_CURVE_B_BASE+i]  = gc_byLSC_RGB_Curve_OV9726_3500K[2][i];
				}
		}
		else if(wColorTempature > 4500)
		{
	          		for (i=0;i<6;i++)
	          		{
	          			g_awLensShadingCenter[i] = gc_wLSC_Center_Offset_OV9726_D65[i];
	          		}

				for (i=0;i<48;i++)
				{
					XBYTE[ISP_NLSC_CURVE_R_BASE+i]  = gc_byLSC_RGB_Curve_OV9726_D65[0][i];
					XBYTE[ISP_NLSC_CURVE_G_BASE+i]  = gc_byLSC_RGB_Curve_OV9726_D65[1][i];
					XBYTE[ISP_NLSC_CURVE_B_BASE+i]  = gc_byLSC_RGB_Curve_OV9726_D65[2][i];
				}
		}

	}
	#endif
	*/
}
#endif
