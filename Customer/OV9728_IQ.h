#ifndef _OV9728_IQ_H_
#define _OV9728_IQ_H_

IQTABLE_t code ct_IQ_Table=
{
	// IQ Header
	{
		IQ_TABLE_AP_VERSION,	// AP version
		sizeof(IQTABLE_t)+8,
		0x00,	// IQ version High
		0x00,	// IQ version Low
		0xFF,
		0xFF,
		0xFF
	},

	// BLC
	{
		// normal BLC:  offset_R,offsetG1,offsetG2,offsetB
		{12,12,12,12},
		//{6,0,0,6},
		// Low lux BLC:  offset_R,offsetG1,offsetG2,offsetB
		{12,12,12,12},
	},

	// LSC
	{
		// circle LSC
		{
			// circle LSC curve
			{
				//20130319_5634_CWF_85%
				//{128,   0, 135,   0, 142,   0, 153,   0, 165,   0, 183,   0, 206,   0, 235,   0,  15,   1,  58,   1, 107,   1, 155,   1, 155,   1, 155,   1, 155,   1, 155,   1, 155,   1, 155,   1, 155,   1, 155,   1, 155,   1, 155,   1, 155,   1, },
				//{128,   0, 132,   0, 139,   0, 147,   0, 157,   0, 170,   0, 187,   0, 208,   0, 234,   0,  11,   1,  47,   1,  82,   1,  82,   1,  82,   1,  82,   1,  82,   1,  82,   1,  82,   1,  82,   1,  82,   1,  82,   1,  82,   1,  82,   1, },
				//{128,   0, 133,   0, 138,   0, 145,   0, 154,   0, 165,   0, 178,   0, 196,   0, 217,   0, 245,   0,  25,   1,  60,   1,  60,   1,  60,   1,  60,   1,  60,   1,  60,   1,  60,   1,  60,   1,  60,   1,  60,   1,  60,   1,  60,   1, },
				// Original
				//{128,   0, 129,   0, 132,   0, 138,   0, 146,   0, 157,   0, 172,   0, 191,   0, 215,   0, 241,   0,  11,   1,  37,   1,  37,   1,  37,   1,  37,   1,  37,   1,  37,   1,  37,   1,  37,   1,  37,   1,  37,   1,  37,   1,  37,   1,  37,   1, },
				//{128,   0, 128,   0, 130,   0, 134,   0, 139,   0, 147,   0, 156,   0, 169,   0, 182,   0, 198,   0, 213,   0, 229,   0, 229,   0, 229,   0, 229,   0, 229,   0, 229,   0, 229,   0, 229,   0, 229,   0, 229,   0, 229,   0, 229,   0, 229,   0, },
				//{128,   0, 128,   0, 130,   0, 133,   0, 138,   0, 146,   0, 155,   0, 167,   0, 180,   0, 196,   0, 211,   0, 226,   0, 226,   0, 226,   0, 226,   0, 226,   0, 226,   0, 226,   0, 226,   0, 226,   0, 226,   0, 226,   0, 226,   0, 226,   0, },

				//Acer 80*
				{128,	0, 129,   0, 133,	0, 141,   0, 152,	0, 168,   0, 187,	0, 212,   0, 243,	0,	25,   1,  73,	1, 132,   1, 132,	1, 132,   1, 132,	1, 132,   1, 132,	1, 132,   1, 132,	1, 132,   1, 132,	1, 132,   1, 132,	1, 132,   1, },
				{128,	0, 129,   0, 132,	0, 137,   0, 146,	0, 157,   0, 172,	0, 189,   0, 211,	0, 240,   0,  19,	1,	70,   1,  70,	1,	70,   1,  70,	1,	70,   1,  70,	1,	70,   1,  70,	1,	70,   1,  70,	1,	70,   1,  70,	1,	70,   1, },
				{128,	0, 128,   0, 130,	0, 135,   0, 142,	0, 151,   0, 162,	0, 175,   0, 192,	0, 215,   0, 247,	0,	33,   1,  33,	1,	33,   1,  33,	1,	33,   1,  33,	1,	33,   1,  33,	1,	33,   1,  33,	1,	33,   1,  33,	1,	33,   1, },
				
			},
			// circle LSC center: R Center Hortizontal, R Center Vertical, G Center Hortizontal, G Center Vertical,B Center Hortizontal, B Center Vertical
			//{660,368,660,368,660,368},
			//Original
			//{648,374,648,374,648,374},
			{646,360,646,360,646,360},
		},

		// micro LSC
		{
			// micro LSC grid mode
			2,
			// micro LSC matrix
			{0},
		},

		// dynamic LSC
		{
			48,//80,   // Dynamic LSC Gain Threshold Low
			144,  // Dynamic LSC Gain Threshold High
			0x20,	// Dynamic LSC Adjust rate at Gain Threshold Low
			0x10,	// Dynamic LSC Adjust rate at Gain Threshold High
			{30,36,44,46,48,56},	// rough r gain before LSC, from A, U30, CWF, D50, D65, D75
			{66,58,58,48,36,30},	// rough b gain before LSC
			50,		// start threshold of dynamic LSC by CT, white pixel millesimal
			25,		// end threshold of dynamic LSC by CT, white pixel millesimal
			100,		// LSC switch color temperature threshold buffer
			//{3100,3800,4700,5800,7000},	// LSC switch color temperature threshold
			{3300,4200,5000,6500,7300}, // LSC switch color temperature threshold
			{	
				
				{0x20,0x20,0x20,},//a=2850k
				{0x20,0x20,0x20,},//3500k
				{0x20,0x20,0x20,},//cwf=4150k
				{0x20,0x20,0x20,},//d50=5000k
				{0x20,0x20,0x20,},//d65=6500k
				{0x20,0x20,0x20,},//d75=6500k
				
				/*
				{0x20,0x20,0x20,},//horizon=3382k
				{0x1f,0x21,0x20,},//U30=?k
				{0x20,0x20,0x20,},//a=4575k
				{0x20,0x20,0x20,},//cwf=5818k
				{0x20,0x20,0x20,},//d50=?k
				{0x20,0x20,0x20,},//d65=7833k
				*/
			},
		},
	},

	// CCM
	{
		// D65 light CCM
		//{0x163,-80,-19,-59,0x11d,30,2,-112,0x16e},	//For MSOC test, pass premium
		//{0x17c,-119,-7,-66,0x157,-19,8,-132,0x17a,},
		//{0x19e,-43,-116,-80,0x14b,7,-14,-282,0x223,}
		//{0x15b,-4,-87,-48,0x126,11,5,-195,0x1bb,},

		//{0x17a,5,-127,-66,0x129,26,16,-239,0x1db,},
		{0x17a,2,-124,-65,0x12a,24,14,-238,0x1dc,},


		//{0x1a4,-201,37,-87,0x16d,-21,-26,-207,0x1e9,},
		//{0x188,-174,38,-74,0x15a,-15,-18,-180,0x1c6,},

		//{0x17c, -92, -32, -64, 0x180, -64, 2 , -112, 0x16e},
		// A light CCM
		//{0x199,-116,-37,-80,0x18a,-56,-9,-91,0x164},	//For MSOC test, pass premium
		{0x19a,-18,-6,-90,0x170,28,-21,-198,0x1a8,},
		//{0x15c, -76, -16, -96, 0x1a0, -64, -16, -172, 0x1bc},
		// low lux CCM
		{0x100, 0, 0, 0, 0x100, 0, 0, 0, 0x100},
		80,	// dynamic CCM Gain Threshold Low
		144,	// dynamic CCM Gain Threshold High
		3000,	// dynamic CCM A light Color Temperature Switch Threshold
		3600	// dynamic CCM D65 light Color Temperature Switch Threshold
	},

	// Gamma
	{
		// normal light gamma
		//{ 0, 7, 16, 27, 38, 51, 63, 75, 89, 108, 123, 135, 144, 154, 162, 169, 177, 184, 190, 194, 199, 209, 218, 226, 234, 240, 244, 250, },
		//{0, 3, 11, 25, 41, 56, 69, 80, 90, 104, 116, 126, 136, 145, 152, 160, 167, 174, 180, 185, 190, 199, 207, 215, 222, 230, 238, 247},

		//{0, 11, 22, 33, 44, 53, 62, 71, 80, 95, 109, 122, 133, 143, 151, 159, 167, 173, 179, 184, 189, 198, 206, 214, 221, 229, 238, 247},
		{0, 12, 24, 35, 46, 57, 66, 74, 83, 98, 111, 122, 133, 142, 150, 158, 166, 172, 178, 183, 188, 197, 205, 213, 220, 228, 237, 246},

		// low light gamma
		//{0, 11, 19, 25, 31, 37, 43, 48, 53, 63, 72, 81, 90, 98, 106, 114, 122, 130, 137, 144, 152, 166, 179, 193, 206, 218, 231, 243}, //[Albert, 2011/06/09]
		{0, 8, 16, 24, 32, 40, 48, 54, 60, 70, 80, 92, 105, 117, 126, 135, 144, 152, 159, 166, 174, 186, 198, 210, 220, 230, 239, 247, },
		80,	// dynamic Gamma Gain Threshold Low
		255	// dynamic Gamma Gain Threshold High
	},

	// AE
	{
		// AE target
		{
			40,	// Histogram Ratio Low
			40,// Histogram Ratio High
			50, //45, //40,//60,	// YMean Target Low
			60, //55, //62,//66,	// YMean Target
			63, //58, //72,//80,	// YMean Target High
			10,	// Histogram Position Low
			190,// Histogram Position High
			3	// Dynamic AE Target decrease value
		},
		// AE limit
		{
			10,	// AE step Max value at 50Hz power line frequency
			12,	// AE step Max value at 60Hz power line frequency
			320,	// AE global gain Max value
			210,	// AE continous frame rate gain threshold
			280, //220,	// AE discrete frame rate 15fps gain threshold
			240, //210, //164,//168,	// AE discrete frame rate 30fps gain threshold
			120	,// AE HighLight mode threshold
		},
		// AE weight
		{
			{
				/*
				4,4,4,4,4,
				5,5,5,5,5,
				6,6,7,6,6,
				6,6,7,6,6,
				5,6,5,6,5,
				*/
				0,0,0,0,0,
				0,2,2,2,0,
				0,2,3,2,0,
				0,2,2,2,0,
				0,0,0,0,0,				
			}
		},
		// AE sensitivity
		{
			0.06,	// g_fAEC_Adjust_Th
			16,	// AE Latency time
			//20,	// Ymean diff threshold for judge AE same block
			8,	// hemonel 2011-07-14: modify Neil adjust parameter from 20 to 8
			22	// same block count threshold for judge AE scene variation
		},
	},

	// AWB
	{
		{
			// AWB simple
			//{33,36,50,54,56,34},	// g_aAWBRoughGain_R
			//{81,64,69,44,41,72},	// g_aAWBRoughGain_B
			//{31, 32, 49, 52, 54, 58},
			//{84, 82, 69, 46, 39, 36},
			//{32,35,36,54,55,56},	// g_aAWBRoughGain_R
			//{93,80,78,68,54,39},	// g_aAWBRoughGain_B
			{30,36,44,46,48,56},	// g_aAWBRoughGain_R
			{66,58,58,48,36,30},	// g_aAWBRoughGain_B
			9,	// K1
			86,	// B1
			60,	// B2
			8,	// sK3
			46,	// sB3
			13,	// sK4
			-30,	// sB4
			-45, //56,	// sK5
			140, //-50,	// sB5
			2,	// sK6
			21,	// sB6
			96,	// B_up
			50,	// B_down
			55,	// sB_left
			-40,	// sB_right
			230,	// Ymean range upper limit
			0,	// Ymean range low limit
			500,	// RGB sum threshold for AWB hold
		},
		// AWB advanced
		{
			5,	// white point ratio threshold
			38,	// g_byAWBFineMax_RG
			26,	// g_byAWBFineMin_RG
			38,	// g_byAWBFineMax_BG
			26,	// g_byAWBFineMin_BG
			225,	// g_byAWBFineMax_Bright
			20,	// g_byAWBFineMin_Bright
			25//16// g_byFtGainTh
		},
		// AWB sensitivity
		{
			4, // g_wAWBGainDiffTh
			1, // g_byAWBGainStep
			10,	// g_byAWBFixed_YmeanTh
			7,	// g_byAWBColorDiff_Th
			12	// g_byAWBDiffWindowsTh
		}
	},

	// Texture
	{
		// sharpness
		{
			60,//0,	// for CIF
			90,//48,	// for VGA
			128,//48,	// for HD
			0x20,// for low lux
			64,	// dynamic sharpness gain threshold low
			150	// dynamic sharpness gain threshold high
		},
		// sharpness & denoise paramter
		{
			// CIF
			{{
					0x0C,	//ISP_NR_EDGE_THD
					0x0A,	//ISP_NR_MM_THD1
					0x01,	//ISP_NR_MODE0_LPF
					0x01,	//ISP_NR_MODE1_LPF
					0x46,	//ISP_NR_MODE
					0x00,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x02,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD
					0x02,	//ISP_NR_CHAOS_CFG
					0x00,	//ISP_NR_DTHD_CFG
					0x81,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0
					0x0C,	//ISP_INTP_EDGE_THD1
					0x06,	//ISP_INTP_MODE
					0x02,	//ISP_INTP_CHAOS_MAX
					0x04,	//ISP_INTP_CHAOS_THD
					0x02,	//ISP_INTP_CHAOS_CFG
					0x00,	//ISP_INTP_DTHD_CFG
					0x07,	//ISP_RGB_IIR_CTRL
					0x60,	//ISP_RGB_VIIR_COEF
					0x88,	//ISP_RGB_HLPF_COEF
					0xA2,	//ISP_RGB_DIIR_MAX
					0x06,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x01,	//ISP_RGB_DIIR_COFF_CUT
					0x0A,	//ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG
				},
				{
						0x08,	//ISP_NR_EDGE_THD
						0x0A,	//ISP_NR_MM_THD1
						0x01,	//ISP_NR_MODE0_LPF
						0x01,	//ISP_NR_MODE1_LPF
						0x49,	//ISP_NR_MODE
						0x00,	//ISP_NR_COLOR_ENABLE_CTRL
						0x05,	//ISP_NR_COLOR_MAX
						0x02,	//ISP_NR_CHAOS_MAX
						0x04,	//ISP_NR_CHAOS_THD
						0x02,	//ISP_NR_CHAOS_CFG
						0x00,	//ISP_NR_DTHD_CFG
						0x81,	//ISP_NR_GRGB_CTRL
						0x06,	//ISP_INTP_EDGE_THD0
						0x0C,	//ISP_INTP_EDGE_THD1
						0x06,	//ISP_INTP_MODE
						0x02,	//ISP_INTP_CHAOS_MAX
						0x04,	//ISP_INTP_CHAOS_THD
						0x02,	//ISP_INTP_CHAOS_CFG
						0x00,	//ISP_INTP_DTHD_CFG
						0x07,	//ISP_RGB_IIR_CTRL
						0x60,	//ISP_RGB_VIIR_COEF
						0x77,	//ISP_RGB_HLPF_COEF
						0xA2,	//ISP_RGB_DIIR_MAX
						0x06,	//ISP_RGB_DIIR_COEF
						0x0C,	//ISP_RGB_BRIGHT_COEF
						0x01,	//ISP_RGB_DIIR_COFF_CUT
						0x0E,	//ISP_EDG_DCT_THD1
						0x03,	//ISP_EEH_DTHD_CFG
				},
				{
							0x08,	//ISP_NR_EDGE_THD
							0x0A,	//ISP_NR_MM_THD1
							0x01,	//ISP_NR_MODE0_LPF
							0x01,	//ISP_NR_MODE1_LPF
							0x57,	//ISP_NR_MODE
							0x01,	//ISP_NR_COLOR_ENABLE_CTRL
							0x05,	//ISP_NR_COLOR_MAX
							0x02,	//ISP_NR_CHAOS_MAX
							0x04,	//ISP_NR_CHAOS_THD
							0x02,	//ISP_NR_CHAOS_CFG
							0x00,	//ISP_NR_DTHD_CFG
							0x82,	//ISP_NR_GRGB_CTRL
							0x04,	//ISP_INTP_EDGE_THD0
							0x0C,	//ISP_INTP_EDGE_THD1
							0x06,	//ISP_INTP_MODE
							0x02,	//ISP_INTP_CHAOS_MAX
							0x04,	//ISP_INTP_CHAOS_THD
							0x02,	//ISP_INTP_CHAOS_CFG
							0x00,	//ISP_INTP_DTHD_CFG
							0x0F,	//ISP_RGB_IIR_CTRL
							0x90,	//ISP_RGB_VIIR_COEF
							0x88,	//ISP_RGB_HLPF_COEF
							0xA2,	//ISP_RGB_DIIR_MAX
							0x0D,	//ISP_RGB_DIIR_COEF
							0x0C,	//ISP_RGB_BRIGHT_COEF
							0x01,	//ISP_RGB_DIIR_COFF_CUT
							0x06,	//ISP_EDG_DCT_THD1
							0x03,	//ISP_EEH_DTHD_CFG
				},
				{
						0x0E,	//ISP_NR_EDGE_THD
						0x0A,	//ISP_NR_MM_THD1
						0x01,	//ISP_NR_MODE0_LPF
						0x01,	//ISP_NR_MODE1_LPF
						0xAA,	//ISP_NR_MODE
						0x01,	//ISP_NR_COLOR_ENABLE_CTRL
						0x05,	//ISP_NR_COLOR_MAX
						0x02,	//ISP_NR_CHAOS_MAX
						0x04,	//ISP_NR_CHAOS_THD
						0x02,	//ISP_NR_CHAOS_CFG
						0x00,	//ISP_NR_DTHD_CFG
						0x82,	//ISP_NR_GRGB_CTRL
						0x04,	//ISP_INTP_EDGE_THD0
						0x0C,	//ISP_INTP_EDGE_THD1
						0x16,	//ISP_INTP_MODE
						0x02,	//ISP_INTP_CHAOS_MAX
						0x04,	//ISP_INTP_CHAOS_THD
						0x02,	//ISP_INTP_CHAOS_CFG
						0x00,	//ISP_INTP_DTHD_CFG
						0x0F,	//ISP_RGB_IIR_CTRL
						0x70,	//ISP_RGB_VIIR_COEF
						0x77,	//ISP_RGB_HLPF_COEF
						0xA2,	//ISP_RGB_DIIR_MAX
						0x0D,	//ISP_RGB_DIIR_COEF
						0x0C,	//ISP_RGB_BRIGHT_COEF
						0x01,	//ISP_RGB_DIIR_COFF_CUT
						0x0C,	//ISP_EDG_DCT_THD1
						0x03,	//ISP_EEH_DTHD_CFG
				}
			},
			// VGA resolution
			{{
				0x0C,	//ISP_NR_EDGE_THD
				0x0A,	//ISP_NR_MM_THD1
				0x01,	//ISP_NR_MODE0_LPF
				0x01,	//ISP_NR_MODE1_LPF
				0x46,	//ISP_NR_MODE
				0x00,	//ISP_NR_COLOR_ENABLE_CTRL
				0x05,	//ISP_NR_COLOR_MAX
				0x02,	//ISP_NR_CHAOS_MAX
				0x04,	//ISP_NR_CHAOS_THD
				0x02,	//ISP_NR_CHAOS_CFG
				0x00,	//ISP_NR_DTHD_CFG
				0x81,	//ISP_NR_GRGB_CTRL
				0x04,	//ISP_INTP_EDGE_THD0
				0x0C,	//ISP_INTP_EDGE_THD1
				0x06,	//ISP_INTP_MODE
				0x02,	//ISP_INTP_CHAOS_MAX
				0x04,	//ISP_INTP_CHAOS_THD
				0x02,	//ISP_INTP_CHAOS_CFG
				0x00,	//ISP_INTP_DTHD_CFG
				0x07,	//ISP_RGB_IIR_CTRL
				0x60,	//ISP_RGB_VIIR_COEF
				0x88,	//ISP_RGB_HLPF_COEF
				0xA2,	//ISP_RGB_DIIR_MAX
				0x06,	//ISP_RGB_DIIR_COEF
				0x0C,	//ISP_RGB_BRIGHT_COEF
				0x01,	//ISP_RGB_DIIR_COFF_CUT
				0x0A,	//ISP_EDG_DCT_THD1
				0x03,	//ISP_EEH_DTHD_CFG
				},
				{
					0x08,	//ISP_NR_EDGE_THD
					0x0A,	//ISP_NR_MM_THD1
					0x01,	//ISP_NR_MODE0_LPF
					0x01,	//ISP_NR_MODE1_LPF
					0x49,	//ISP_NR_MODE
					0x00,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x02,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD
					0x02,	//ISP_NR_CHAOS_CFG
					0x00,	//ISP_NR_DTHD_CFG
					0x81,	//ISP_NR_GRGB_CTRL
					0x06,	//ISP_INTP_EDGE_THD0
					0x0C,	//ISP_INTP_EDGE_THD1
					0x06,	//ISP_INTP_MODE
					0x02,	//ISP_INTP_CHAOS_MAX
					0x04,	//ISP_INTP_CHAOS_THD
					0x02,	//ISP_INTP_CHAOS_CFG
					0x00,	//ISP_INTP_DTHD_CFG
					0x07,	//ISP_RGB_IIR_CTRL
					0x60,	//ISP_RGB_VIIR_COEF
					0x77,	//ISP_RGB_HLPF_COEF
					0xA2,	//ISP_RGB_DIIR_MAX
					0x06,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x01,	//ISP_RGB_DIIR_COFF_CUT
					0x0E,	//ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG
				},
				{
						0x08,	//ISP_NR_EDGE_THD
						0x0A,	//ISP_NR_MM_THD1
						0x01,	//ISP_NR_MODE0_LPF
						0x01,	//ISP_NR_MODE1_LPF
						0x57,	//ISP_NR_MODE
						0x01,	//ISP_NR_COLOR_ENABLE_CTRL
						0x05,	//ISP_NR_COLOR_MAX
						0x02,	//ISP_NR_CHAOS_MAX
						0x04,	//ISP_NR_CHAOS_THD
						0x02,	//ISP_NR_CHAOS_CFG
						0x00,	//ISP_NR_DTHD_CFG
						0x82,	//ISP_NR_GRGB_CTRL
						0x04,	//ISP_INTP_EDGE_THD0
						0x0C,	//ISP_INTP_EDGE_THD1
						0x06,	//ISP_INTP_MODE
						0x02,	//ISP_INTP_CHAOS_MAX
						0x04,	//ISP_INTP_CHAOS_THD
						0x02,	//ISP_INTP_CHAOS_CFG
						0x00,	//ISP_INTP_DTHD_CFG
						0x0F,	//ISP_RGB_IIR_CTRL
						0x90,	//ISP_RGB_VIIR_COEF
						0x88,	//ISP_RGB_HLPF_COEF
						0xA2,	//ISP_RGB_DIIR_MAX
						0x0D,	//ISP_RGB_DIIR_COEF
						0x0C,	//ISP_RGB_BRIGHT_COEF
						0x01,	//ISP_RGB_DIIR_COFF_CUT
						0x06,	//ISP_EDG_DCT_THD1
						0x03,	//ISP_EEH_DTHD_CFG
				},
				{
						0x0E,	//ISP_NR_EDGE_THD
						0x0A,	//ISP_NR_MM_THD1
						0x01,	//ISP_NR_MODE0_LPF
						0x01,	//ISP_NR_MODE1_LPF
						0xAA,	//ISP_NR_MODE
						0x01,	//ISP_NR_COLOR_ENABLE_CTRL
						0x05,	//ISP_NR_COLOR_MAX
						0x02,	//ISP_NR_CHAOS_MAX
						0x04,	//ISP_NR_CHAOS_THD
						0x02,	//ISP_NR_CHAOS_CFG
						0x00,	//ISP_NR_DTHD_CFG
						0x82,	//ISP_NR_GRGB_CTRL
						0x04,	//ISP_INTP_EDGE_THD0
						0x0C,	//ISP_INTP_EDGE_THD1
						0x16,	//ISP_INTP_MODE
						0x02,	//ISP_INTP_CHAOS_MAX
						0x04,	//ISP_INTP_CHAOS_THD
						0x02,	//ISP_INTP_CHAOS_CFG
						0x00,	//ISP_INTP_DTHD_CFG
						0x0F,	//ISP_RGB_IIR_CTRL
						0x70,	//ISP_RGB_VIIR_COEF
						0x77,	//ISP_RGB_HLPF_COEF
						0xA2,	//ISP_RGB_DIIR_MAX
						0x0D,	//ISP_RGB_DIIR_COEF
						0x0C,	//ISP_RGB_BRIGHT_COEF
						0x01,	//ISP_RGB_DIIR_COFF_CUT
						0x0C,	//ISP_EDG_DCT_THD1
						0x03,	//ISP_EEH_DTHD_CFG
				}
			},
			// HD resolution
			//[Albert, 2011/06/16]+++
			{{
				0x0C,	//ISP_NR_EDGE_THD
				0x0A,	//ISP_NR_MM_THD1
				0x01,	//ISP_NR_MODE0_LPF
				0x01,	//ISP_NR_MODE1_LPF
				0x46,	//ISP_NR_MODE
				0x00,	//ISP_NR_COLOR_ENABLE_CTRL
				0x05,	//ISP_NR_COLOR_MAX
				0x02,	//ISP_NR_CHAOS_MAX
				0x04,	//ISP_NR_CHAOS_THD
				0x02,	//ISP_NR_CHAOS_CFG
				0x00,	//ISP_NR_DTHD_CFG
				0x81,	//ISP_NR_GRGB_CTRL
				0x04,	//ISP_INTP_EDGE_THD0
				0x0C,	//ISP_INTP_EDGE_THD1
				0x06,	//ISP_INTP_MODE
				0x02,	//ISP_INTP_CHAOS_MAX
				0x04,	//ISP_INTP_CHAOS_THD
				0x02,	//ISP_INTP_CHAOS_CFG
				0x00,	//ISP_INTP_DTHD_CFG
				0x07,	//ISP_RGB_IIR_CTRL
				0x60,	//ISP_RGB_VIIR_COEF
				0x88,	//ISP_RGB_HLPF_COEF
				0xA2,	//ISP_RGB_DIIR_MAX
				0x06,	//ISP_RGB_DIIR_COEF
				0x0C,	//ISP_RGB_BRIGHT_COEF
				0x01,	//ISP_RGB_DIIR_COFF_CUT
				0x0A,	//ISP_EDG_DCT_THD1
				0x03,	//ISP_EEH_DTHD_CFG
				},
				{
					0x08,	//ISP_NR_EDGE_THD
					0x0A,	//ISP_NR_MM_THD1
					0x01,	//ISP_NR_MODE0_LPF
					0x01,	//ISP_NR_MODE1_LPF
					0x49,	//ISP_NR_MODE
					0x00,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x02,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD
					0x02,	//ISP_NR_CHAOS_CFG
					0x00,	//ISP_NR_DTHD_CFG
					0x81,	//ISP_NR_GRGB_CTRL
					0x06,	//ISP_INTP_EDGE_THD0
					0x0C,	//ISP_INTP_EDGE_THD1
					0x06,	//ISP_INTP_MODE
					0x02,	//ISP_INTP_CHAOS_MAX
					0x04,	//ISP_INTP_CHAOS_THD
					0x02,	//ISP_INTP_CHAOS_CFG
					0x00,	//ISP_INTP_DTHD_CFG
					0x07,	//ISP_RGB_IIR_CTRL
					0x60,	//ISP_RGB_VIIR_COEF
					0x77,	//ISP_RGB_HLPF_COEF
					0xA2,	//ISP_RGB_DIIR_MAX
					0x06,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x01,	//ISP_RGB_DIIR_COFF_CUT
					0x0E,	//ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG
				},
				{
						0x08,	//ISP_NR_EDGE_THD
						0x0A,	//ISP_NR_MM_THD1
						0x01,	//ISP_NR_MODE0_LPF
						0x01,	//ISP_NR_MODE1_LPF
						0x57,	//ISP_NR_MODE
						0x01,	//ISP_NR_COLOR_ENABLE_CTRL
						0x05,	//ISP_NR_COLOR_MAX
						0x02,	//ISP_NR_CHAOS_MAX
						0x04,	//ISP_NR_CHAOS_THD
						0x02,	//ISP_NR_CHAOS_CFG
						0x00,	//ISP_NR_DTHD_CFG
						0x82,	//ISP_NR_GRGB_CTRL
						0x04,	//ISP_INTP_EDGE_THD0
						0x0C,	//ISP_INTP_EDGE_THD1
						0x06,	//ISP_INTP_MODE
						0x02,	//ISP_INTP_CHAOS_MAX
						0x04,	//ISP_INTP_CHAOS_THD
						0x02,	//ISP_INTP_CHAOS_CFG
						0x00,	//ISP_INTP_DTHD_CFG
						0x0F,	//ISP_RGB_IIR_CTRL
						0x90,	//ISP_RGB_VIIR_COEF
						0x88,	//ISP_RGB_HLPF_COEF
						0xA2,	//ISP_RGB_DIIR_MAX
						0x0D,	//ISP_RGB_DIIR_COEF
						0x0C,	//ISP_RGB_BRIGHT_COEF
						0x01,	//ISP_RGB_DIIR_COFF_CUT
						0x06,	//ISP_EDG_DCT_THD1
						0x03,	//ISP_EEH_DTHD_CFG
				},
				{
						0x0E,	//ISP_NR_EDGE_THD
						0x0A,	//ISP_NR_MM_THD1
						0x01,	//ISP_NR_MODE0_LPF
						0x01,	//ISP_NR_MODE1_LPF
						0xAA,	//ISP_NR_MODE
						0x01,	//ISP_NR_COLOR_ENABLE_CTRL
						0x05,	//ISP_NR_COLOR_MAX
						0x02,	//ISP_NR_CHAOS_MAX
						0x04,	//ISP_NR_CHAOS_THD
						0x02,	//ISP_NR_CHAOS_CFG
						0x00,	//ISP_NR_DTHD_CFG
						0x82,	//ISP_NR_GRGB_CTRL
						0x04,	//ISP_INTP_EDGE_THD0
						0x0C,	//ISP_INTP_EDGE_THD1
						0x16,	//ISP_INTP_MODE
						0x02,	//ISP_INTP_CHAOS_MAX
						0x04,	//ISP_INTP_CHAOS_THD
						0x02,	//ISP_INTP_CHAOS_CFG
						0x00,	//ISP_INTP_DTHD_CFG
						0x0F,	//ISP_RGB_IIR_CTRL
						0x70,	//ISP_RGB_VIIR_COEF
						0x77,	//ISP_RGB_HLPF_COEF
						0xA2,	//ISP_RGB_DIIR_MAX
						0x0D,	//ISP_RGB_DIIR_COEF
						0x0C,	//ISP_RGB_BRIGHT_COEF
						0x01,	//ISP_RGB_DIIR_COFF_CUT
						0x0C,	//ISP_EDG_DCT_THD1
						0x03,	//ISP_EEH_DTHD_CFG
				}
			},
			3,		// dynamic denoise gain threshold 0
			9,		// dynamic denoise gain threshold 1
			11		// dynamic denoise gain threshold 2
		}
	},
	// UV offset
	{
		0,		// A light U offset
		0,		// A light V offset
		0,//0x24,		// D65 light U offset
		0,//0x22,		// D65 light V offset
		3300,	// Dynamic UV offset threshold for A light
		4000	// Dynamic UV offset threshold for D65 light
	},

	// gamma 2
	{
		// normal lux
		{
			// R - G diff gamma
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			// B - G diff gamma
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		},

		// low lux
		{
			// R - G diff gamma
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			// B - G diff gamma
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		},
	},

	// Texture 2
	{
		// corner denoise
		{
			0x100,	//d0(start) for RGB domain
			0x300,	//d1(end) for RGB domain
			0x100,	//d0(start) for YUV domain
			0x300,	//d1(end) for YUV domain
			{
				{
					0x08,	//ISP_RDLOC_MAX
					0x20,	//ISP_RDLOC_RATE
					0x08,	//ISP_GLOC_MAX
					0x20,	//ISP_GLOC_RATE
					0x02,	//ISP_ILOC_MAX
					0x04,	//ISP_ILOC_RATE
					0x10,	//ISP_LOC_MAX
					0x40,	//ISP_LOC_RATE
				},
				{
						0x10,	//ISP_RDLOC_MAX
						0x40,	//ISP_RDLOC_RATE
						0x10,	//ISP_GLOC_MAX
						0x40,	//ISP_GLOC_RATE
						0x02,	//ISP_ILOC_MAX
						0x04,	//ISP_ILOC_RATE
						0x10,	//ISP_LOC_MAX
						0x40,	//ISP_LOC_RATE
				},
				{
							0x10,	//ISP_RDLOC_MAX
							0x40,	//ISP_RDLOC_RATE
							0x0A,	//ISP_GLOC_MAX
							0x24,	//ISP_GLOC_RATE
							0x02,	//ISP_ILOC_MAX
							0x04,	//ISP_ILOC_RATE
							0x10,	//ISP_LOC_MAX
							0x40,	//ISP_LOC_RATE
				},
				{
						0x10,	//ISP_RDLOC_MAX
						0x40,	//ISP_RDLOC_RATE
						0x10,	//ISP_GLOC_MAX
						0x40,	//ISP_GLOC_RATE
						0x02,	//ISP_ILOC_MAX
						0x04,	//ISP_ILOC_RATE
						0x10,	//ISP_LOC_MAX
						0x40,	//ISP_LOC_RATE
				},
			},
		},
		// uv denoise
		{
			{
				{
					0x8,   //ISP_EEH_UVIIR_Y_CUTS
					4,   //ISP_EEH_UVIIR_Y_CMIN
					0xa,	 //ISP_EEH_IIR_COEF
					2,	 //ISP_EEH_IIR_CUTS
					1,  //ISP_EEh_IIR_STEP
				},
				{
						0x08,	//ISP_EEH_UVIIR_Y_CUTS
						0x04,	//ISP_EEH_UVIIR_Y_CMIN
						0x0A,	//ISP_EEH_IIR_COEF
						0x02,	//ISP_EEH_IIR_CUTS
						0x01,	//ISP_EEH_IIR_STEP
				},
				{
					0x8,   //ISP_EEH_UVIIR_Y_CUTS
					4,   //ISP_EEH_UVIIR_Y_CMIN
					0xb,	 //ISP_EEH_IIR_COEF
					2,	 //ISP_EEH_IIR_CUTS
					1, //ISP_EEh_IIR_STEP
				},
				{
					0x8,   //ISP_EEH_UVIIR_Y_CUTS
					4,   //ISP_EEH_UVIIR_Y_CMIN
					0xb,	 //ISP_EEH_IIR_COEF
					2,	 //ISP_EEH_IIR_CUTS
					1, //ISP_EEh_IIR_STEP
				},
			},
		},
		// noise reduction additional
		{
			{0x1E,0x1E,0x1E,0x1E,},	//ISP_EEH_CRC_RATE
			{2,2,1,1,},	//ISP_RD_MM2_RATE
			{
				{
					0x02,	//ISP_NR_MMM_D0
					0x22,	//ISP_NR_MMM_D1
					0xE0,	//ISP_NR_MMM_RATE
					0x10,	//ISP_NR_MMM_MIN
					0x80,	//ISP_NR_MMM_MAX
				},
				{
						0x02,	//ISP_NR_MMM_D0
						0x22,	//ISP_NR_MMM_D1
						0x44,	//ISP_NR_MMM_RATE
						0x02,	//ISP_NR_MMM_MIN
						0xA4,	//ISP_NR_MMM_MAX
				},
				{
							0x08,	//ISP_NR_MMM_D0
							0x04,	//ISP_NR_MMM_D1
							0x10,	//ISP_NR_MMM_RATE
							0x02,	//ISP_NR_MMM_MIN
							0x01,	//ISP_NR_MMM_MAX
				},
				{
								0x02,	//ISP_NR_MMM_D0
								0x22,	//ISP_NR_MMM_D1
								0xFF,	//ISP_NR_MMM_RATE
								0x02,	//ISP_NR_MMM_MIN
								0xA4,	//ISP_NR_MMM_MAX
				},
			},
		},
	},

	// Edge enhance
	{
		0x01, 			//ISP_BRIGHT_RATE
		0x02, 			//ISP_BRIGHT_TRM_B1
		0x10, 			//ISP_BRIGHT_TRM_B2
		0x0a, 			//ISP_BRIGHT_TRM_K
		0x03, 			//ISP_BRIGHT_TRM_THD0
		0x18, 			//ISP_BRIGHT_TRM_THD1
		0x01, 			//ISP_DARK_RATE
		0x02, 			//ISP_DARK_TRM_B1
		0x10, 			//ISP_DARK_TRM_B2
		0x0a, 			//ISP_DARK_TRM_K
		0x03, 			//ISP_DARK_TRM_THD0
		0x18, 			//ISP_DARK_TRM_THD1
		0x28,  			//ISP_EDG_DIFF_C0
		0x1e, 			//ISP_EDG_DIFF_C1
		0x1e, 			//ISP_EDG_DIFF_C2
		0x1e, 	    	//ISP_EDG_DIFF_C3
		0x0e, 			//ISP_EDG_DIFF_C4
		0x08, 			//ISP_EEH_SHARP_ARRAY 0
		0x0C, 			//ISP_EEH_SHARP_ARRAY10
		0x10, 			//ISP_EEH_SHARP_ARRAY11
		0x0C, 			//ISP_EEH_SHARP_ARRAY1
		0x10, 			//ISP_EEH_SHARP_ARRAY2
		0x0C, 			//ISP_EEH_SHARP_ARRAY3
		0x18, 			//ISP_EEH_SHARP_ARRAY4
		0x18, 			//ISP_EEH_SHARP_ARRAY5
		0x10, 			//ISP_EEH_SHARP_ARRAY6
		0x10, 			//ISP_EEH_SHARP_ARRAY7
		0x10, 			//ISP_EEH_SHARP_ARRAY8
		0x08, 			//ISP_EEH_SHARP_ARRAY9

	},

	// flase color and morie
	{
		{
			{
				0x02, 			//ISP_FCRD_MIN
				0x12, 			//ISP_FCRD_YCUTS
				0x90, 			//ISP_FCRD_CFG
				0x1f, 			//ISP_FCRD_MAX
				0x20,			//ISP_MOIRE_RATE
				0x02,			//MOIRE detect threshold
			},
			{
				0x02, 			//ISP_FCRD_MIN
				0x12, 			//ISP_FCRD_YCUTS
				0x90, 			//ISP_FCRD_CFG
				0x1f, 			//ISP_FCRD_MAX
				0x20,			//ISP_MOIRE_RATE
				0x02,			//MOIRE detect threshold
			},
			{
				0x02, 			//ISP_FCRD_MIN
				0x12, 			//ISP_FCRD_YCUTS
				0x90, 			//ISP_FCRD_CFG
				0x1f, 			//ISP_FCRD_MAX
				0x10,			//ISP_MOIRE_RATE
				0x04,			//MOIRE detect threshold
			},
			{
				0x02, 			//ISP_FCRD_MIN
				0x12, 			//ISP_FCRD_YCUTS
				0x90, 			//ISP_FCRD_CFG
				0x1f, 			//ISP_FCRD_MAX
				0x10,			//ISP_MOIRE_RATE
				0x04,			//MOIRE detect threshold
			},
		}

	},

	// dead pixel cancel
	{
		0x01,	//  ISP_DDP_CTRL
		0x01, 	// ISP_DDP_SEL
		0x04,	//	ISP_DP_THD_D1
		0x02, 	//	ISP_DP_BRIGHT_THD_MIN
		0x0A,	//  ISP_DP_BRIGHT_THD_MAX
		0x03, 	//  ISP_DP_DARK_THD_MIN
		0x10,	//  ISP_DP_DARK_THD_MAX
		0x02,	//  ISP_DDP_BRIGHT_RATE
		0x02,	//  ISP_DDP_DARK_RATE
	},

	// UV color tune
	{
		//A light UV color tune
		{
			0, //ISP_UVT_UCENTER
			0, //ISP_UVT_VCENTER
			0, //ISP_UVT_UINC
			0, //ISP_UVT_VINC
		},
		//D65 light UV color tune
		{
			0,  //ISP_UVT_UCENTER
			0,    //ISP_UVT_VINC
			0,   //ISP_UVT_UINC
			0,   //ISP_UVT_VINC
		},
		3300, // Dynamic UV color tune threshold for A light
		4000,  // Dynamic UV color tune threshold for D65 light
	},

	// NLC
	{
		// G NLC
		{0, 16, 32, 48, 64, 80, 96, 112, 128, 144, 160, 176, 192, 208, 224, 240},
		// R - G diff
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		// B - G diff
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
	},

	// HDR
	{
		// HDR threshold
		{
			10,		// Histogram dark pixel threshold for start HDR function
			10,     // Histogram bright pixel threshold for start HDR function
			40, 	// Histogram dark pixel max number
			100,		// Hdr tune max value

		},

		// FW HDR
		{
			// HDR gamma
			{0,23,43,58,73,81,88,94,99,109,117,125,132,139,145,151,157,163,168,173,177,184,190,195,201,210,222,237},
		},

		// HW HDR
		{
			// tgamma threshold
			0x8,
			// tgamma rate
			0x10,
			// HDR LPF COEF
			{0, 1, 3, 5, 6, 6, 5, 4, 2},
			// HDR halo thd
			0x10,
			// HDR curver
			{115, 115, 114, 112, 111, 109, 108, 106, 104, 100, 96, 91, 85, 79, 72, 64, 54, 45, 35, 26, 16, 10, 6, 3},
			// HDR max curver
			{192, 144, 112, 80, 61, 52, 42, 36, 32},
			//HDR step
			0x4,
			//local constrast curver
			{24, 26, 26, 24, 20, 20, 20,20, 20, 20, 20, 20, 20, 23, 26, 26},
			//local constrast rate min
			0xd,
			//local constrast rate max
			0xd,
			//local constrast step
			0x10
		},
	}

};
#endif // _OV9728_IQ_H_
