#ifndef _OV5642_H_
#define _OV5642_H_

#define OV5642_AEW	0x70// 0x78 ---jqg 20090422
#define OV5642_AEB	0x68// 0x68
#define OV5642_VPT	0xd4

typedef struct OV5642_FpsSetting
{
	U8 byFps;
	U8  byClkrc; // register 0x11: clock pre-scalar
	U8 byPLL;
	U8 byExtraDummyPixel;   // register 0x2a,0x2b: dummy pixel adjustment
	U32 dwPixelClk;
} OV5642_FpsSetting_t;
void OV5642_POR();
void OV5642SetFormatFps(U16 SetFormat, U8 Fps);
void CfgOV5642ControlAttr(void);
void SetOV5642Brightness(S16 swSetValue);
void	SetOV5642Contrast(U16 wSetValue);
void	SetOV5642Saturation(U16 wSetValue);
void	SetOV5642Hue(S16 swSetValue);
void	SetOV5642Sharpness(U8 bySetValue);
void SetOV5642Effect(U8 byEffect);
void SetOV5642ImgDir(U8 bySnrImgDir);
void SetOV5642WindowCrop(U16 wXStart,U16 wXEnd,U16 wYStart,U16 wYEnd );
void SetOV5642OutputDim(U16 wWidth,U16 wHeight);
void SetOV5642PwrLineFreq(U8 byFPS, U8 byLightFrq);
void SetOV5642WBTemp(U16 wRgain, U16 wGgain, U16 wBgain);
void SetOV5642WBTempAuto(U8 bySetValue);
void SetOV5642BackLightComp(U8 bySetValue);
extern code OV_CTT_t gc_OV5642_CTT[3];
void SetOV5642IntegrationTimeAuto(U8 bySetValue);
void SetOV5642IntegrationTime(U16 wEspline);
void SetOV5642LowLightComp(U8 bySetValue);
void OV5642PreviewToCapture(U16 wPreviewWidth, U16 wStillWidth);
void OV5642CaptureToPreview();
void SetOV5642Gain(float fGain);
#endif // _OV775_H_

