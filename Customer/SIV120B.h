#ifndef _SIV120B_H_
#define _SIV120B_H_

#define SIV120B_AE_TARGET	0x78

void SIV120BSetFormatFps(U8 SetFormat, U8 Fps);
void CfgSIV120BControlAttr(void);
void	SetSIV120BSharpness(U8 bySetValue);
void SetSIV120BImgDir(U8 bySnrImgDir);
void SetSIV120BOutputDim(U16 wWidth,U16 wHeight);
void SetSIV120BPwrLineFreq(U8 byFPS, U8 byLightFrq);
void SetSIV120BWBTemp(U16 wSetValue);
//OV_CTT_t GetSIV120BAwbGain(void);
void SetSIV120BWBTempAuto(U8 bySetValue);
void SetSIV120BBackLightComp(U8 bySetValue);
extern code OV_CTT_t gc_SIV120B_CTT[3];
U16 GetSIV120BAEGain(void);
void SetSIV120BIntegrationTimeAuto(U8 bySetValue);
void SetSIV120BIntegrationTime(U16 wEspline);
void SetSIV120BGain(float fGain);
#endif // _OV7670_H_
