#ifndef _IMX132PQ_IQ_H_
#define _IMX132PQ_IQ_H_

IQTABLE_t code ct_IQ_Table=
{
	// IQ Header
	{
		IQ_TABLE_AP_VERSION,	// AP version
		sizeof(IQTABLE_t)+8,
		0x00,	// IQ version High
		0x00,	// IQ version Low
		0xFF,
		0xFF,
		0xFF
	},

	// BLC
	{
		// normal BLC:  offset_R,offsetG1,offsetG2,offsetB
		{64,64,64,64},
		//{0,0,0,0},
		// Low lux BLC:  offset_R,offsetG1,offsetG2,offsetB
		{64,64,64,64},
		//{0,0,0,0},
	},

	// LSC
	{
		// circle LSC
		{
			// circle LSC curve
			{
				//:3500k
				/*	{128, 0, 128, 0, 129, 0, 132, 0, 136, 0, 141, 0, 148, 0, 156, 0, 165, 0, 176, 0, 186, 0, 196, 0, 203, 0, 203, 0, 203, 0, 203, 0, 203, 0, 203, 0, 203, 0, 203, 0, 203, 0, 203, 0, 203, 0, 203, 0, },
					{128, 0, 128, 0, 130, 0, 133, 0, 137, 0, 143, 0, 150, 0, 158, 0, 168, 0, 178, 0, 189, 0, 201, 0, 211, 0, 211, 0, 211, 0, 211, 0, 211, 0, 211, 0, 211, 0, 211, 0, 211, 0, 211, 0, 211, 0, 211, 0, },
					{128, 0, 128, 0, 129, 0, 132, 0, 135, 0, 139, 0, 144, 0, 150, 0, 156, 0, 163, 0, 170, 0, 178, 0, 183, 0, 183, 0, 183, 0, 183, 0, 183, 0, 183, 0, 183, 0, 183, 0, 183, 0, 183, 0, 183, 0, 183, 0, },
				*/
				//d65
				/*	{128, 0, 129, 0, 132, 0, 137, 0, 146, 0, 157, 0, 171, 0, 190, 0, 212, 0, 240, 0, 9, 1, 38, 1, 63, 1, 63, 1, 63, 1, 63, 1, 63, 1, 63, 1, 63, 1, 63, 1, 63, 1, 63, 1, 63, 1, 63, 1, },
					{128, 0, 128, 0, 131, 0, 136, 0, 142, 0, 151, 0, 162, 0, 177, 0, 194, 0, 215, 0, 235, 0, 4, 1, 30, 1, 30, 1, 30, 1, 30, 1, 30, 1, 30, 1, 30, 1, 30, 1, 30, 1, 30, 1, 30, 1, 30, 1, },
					{128, 0, 129, 0, 131, 0, 136, 0, 142, 0, 151, 0, 162, 0, 176, 0, 194, 0, 214, 0, 234, 0, 3, 1, 24, 1, 24, 1, 24, 1, 24, 1, 24, 1, 24, 1, 24, 1, 24, 1, 24, 1, 24, 1, 24, 1, 24, 1, },
				*/
				//d50
				/*		{128, 0, 128, 0, 132, 0, 137, 0, 141, 0, 148, 0, 158, 0, 177, 0, 195, 0, 207, 0, 235, 0, 5, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, },
						{128, 0, 128, 0, 131, 0, 136, 0, 141, 0, 148, 0, 158, 0, 177, 0, 194, 0, 207, 0, 236, 0, 5, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, },
						{128, 0, 128, 0, 131, 0, 135, 0, 141, 0, 148, 0, 158, 0, 175, 0, 192, 0, 207, 0, 233, 0, 3, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, },
				*/
				//d50 60%
				/*		{128, 0, 128, 0, 130, 0, 133, 0, 137, 0, 142, 0, 150, 0, 158, 0, 168, 0, 181, 0, 193, 0, 215, 0, 231, 0, 231, 0, 231, 0, 231, 0, 231, 0, 231, 0, 231, 0, 231, 0, 231, 0, 231, 0, 231, 0, 231, 0,},
						{128, 0, 128, 0, 130, 0, 132, 0, 136, 0, 142, 0, 149, 0, 157, 0, 168, 0, 180, 0, 193, 0, 209, 0, 225, 0, 225, 0, 225, 0, 225, 0, 225, 0, 225, 0, 225, 0, 225, 0, 225, 0, 225, 0, 225, 0, 225, 0, },
						{128, 0, 128, 0, 129, 0, 132, 0, 136, 0, 141, 0, 148, 0, 156, 0, 166, 0, 178, 0, 190, 0, 205, 0, 224, 0, 224, 0, 224, 0, 224, 0, 224, 0, 224, 0, 224, 0, 224, 0, 224, 0, 224, 0, 224, 0, 224, 0, },
				*/
				/*
						//1012 Leo++ 80%
						{128,   0, 129,   0, 134,   0, 144,   0, 157,   0, 178,   0, 206,   0, 242,   0,  31,   1,  86,   1, 144,   1, 201,   1,  22,   2,  22,   2,  22,   2,  22,   2,  22,   2,  22,   2,  22,   2,  22,   2,  22,   2,  22,   2,  22,   2, },
						{128,   0, 131,   0, 136,   0, 146,   0, 159,   0, 178,   0, 203,   0, 234,   0,  16,   1,  58,   1, 104,   1, 147,   1, 192,   1, 192,   1, 192,   1, 192,   1, 192,   1, 192,   1, 192,   1, 192,   1, 192,   1, 192,   1, 192,   1, },
						{128,   0, 129,   0, 133,   0, 140,   0, 152,   0, 168,   0, 190,   0, 216,   0, 250,   0,  33,   1,  74,   1, 110,   1, 159,   1, 159,   1, 159,   1, 159,   1, 159,   1, 159,   1, 159,   1, 159,   1, 159,   1, 159,   1, 159,   1, },
				*/
				/*
						//1021 Leo++ 70%
						{128,   0, 130,   0, 134,   0, 141,   0, 153,   0, 170,   0, 194,   0, 224,   0,   6,   1,  51,   1, 100,   1, 148,   1, 213,   1, 213,   1, 213,   1, 213,   1, 213,   1, 213,   1, 213,   1, 213,   1, 213,   1, 213,   1, 213,   1, },
						{128,   0, 130,   0, 134,   0, 142,   0, 153,   0, 169,   0, 190,   0, 215,   0, 246,   0,  25,   1,  62,   1,  98,   1, 136,   1, 136,   1, 136,   1, 136,   1, 136,   1, 136,   1, 136,   1, 136,   1, 136,   1, 136,   1, 136,   1, },
						{128,   0, 130,   0, 133,   0, 139,   0, 149,   0, 162,   0, 181,   0, 203,   0, 232,   0,   8,   1,  43,   1,  73,   1, 109,   1, 109,   1, 109,   1, 109,   1, 109,   1, 109,   1, 109,   1, 109,   1, 109,   1, 109,   1, 109,   1, },
				*/
				/*
				//1021 Leo++ 60%
				{128,   0, 129,   0, 132,   0, 138,   0, 148,   0, 161,   0, 180,   0, 204,   0, 234,   0,  14,   1,  53,   1,  91,   1, 142,   1, 142,   1, 142,   1, 142,   1, 142,   1, 142,   1, 142,   1, 142,   1, 142,   1, 142,   1, 142,   1, },
				{128,   0, 129,   0, 133,   0, 139,   0, 148,   0, 160,   0, 177,   0, 197,   0, 222,   0, 249,   0,  23,   1,  51,   1,  81,   1,  81,   1,  81,   1,  81,   1,  81,   1,  81,   1,  81,   1,  81,   1,  81,   1,  81,   1,  81,   1, },
				{128,   0, 129,   0, 132,   0, 136,   0, 144,   0, 155,   0, 170,   0, 187,   0, 210,   0, 235,   0,   7,   1,  31,   1,  59,   1,  59,   1,  59,   1,  59,   1,  59,   1,  59,   1,  59,   1,  59,   1,  59,   1,  59,   1,  59,   1, },
				*/
				/*
						//1021 Leo++ 50%
						{128,   0, 129,   0, 131,   0, 136,   0, 142,   0, 152,   0, 166,   0, 184,   0, 206,   0, 232,   0,   5,   1,  33,   1,  71,   1,  71,   1,  71,   1,  71,   1,  71,   1,  71,   1,  71,   1,  71,   1,  71,   1,  71,   1,  71,   1, },
						{128,   0, 129,   0, 131,   0, 136,   0, 142,   0, 152,   0, 164,   0, 179,   0, 197,   0, 217,   0, 239,   0,   4,   1,  26,   1,  26,   1,  26,   1,  26,   1,  26,   1,  26,   1,  26,   1,  26,   1,  26,   1,  26,   1,  26,   1, },
						{128,   0, 129,   0, 131,   0, 134,   0, 140,   0, 148,   0, 159,   0, 172,   0, 188,   0, 207,   0, 228,   0, 245,   0,  10,   1,  10,   1,  10,   1,  10,   1,  10,   1,  10,   1,  10,   1,  10,   1,  10,   1,  10,   1,  10,   1, },
				*/

				//R_Gain =
				{128, 0, 129, 0, 130, 0, 133, 0, 138, 0, 143, 0, 151, 0, 160, 0, 171, 0, 184, 0, 199, 0, 216, 0, 234, 0, 255, 0, 18, 1, 38, 1, 55, 1, 69, 1, 86, 1, 86, 1, 86, 1, 86, 1, 86, 1, 86, 1, },
				//G_Gain =
				{128, 0, 128, 0, 129, 0, 131, 0, 135, 0, 139, 0, 144, 0, 150, 0, 157, 0, 165, 0, 174, 0, 184, 0, 194, 0, 205, 0, 214, 0, 223, 0, 231, 0, 238, 0, 246, 0, 246, 0, 246, 0, 246, 0, 246, 0, 246, 0, },
				//B_Gain =
				{128, 0, 128, 0, 129, 0, 132, 0, 135, 0, 139, 0, 144, 0, 150, 0, 157, 0, 165, 0, 174, 0, 183, 0, 193, 0, 203, 0, 211, 0, 219, 0, 226, 0, 233, 0, 240, 0, 240, 0, 240, 0, 240, 0, 240, 0, 240, 0, },

			},
			// circle LSC center: R Center Hortizontal, R Center Vertical, G Center Hortizontal, G Center Vertical,B Center Hortizontal, B Center Vertical
			//	{672 ,402,672 ,402,672 ,402}, //For3500k
			{1004, 618, 1004, 618, 1004, 618,}, //For5000k
		},

		// micro LSC
		{
			// micro LSC grid mode
			2,
			// micro LSC matrix
			{0},
		},

		// dynamic LSC
		{
			80,   // Dynamic LSC Gain Threshold Low
			192,  // Dynamic LSC Gain Threshold High
			0x20,	// Dynamic LSC Adjust rate at Gain Threshold Low
			0x10,	// Dynamic LSC Adjust rate at Gain Threshold High
			{33,35,38,54,56,34},	// rough r gain before LSC, from A, U30, CWF, D50, D65, D75
			{78,70,61,44,38,72},	// rough b gain before LSC
			50,		// start threshold of dynamic LSC by CT, white pixel millesimal
			25,		// end threshold of dynamic LSC by CT, white pixel millesimal
			100,		// LSC switch color temperature threshold buffer
			{3100,3800,4700,5800,7000},	// LSC switch color temperature threshold
			{
				{0x20,0x20,0x20,},//a=2850k
				{0x20,0x20,0x20,},//3500k
				{0x1A,0x20,0x20,},//cwf=4150k
				{0x20,0x20,0x20,},//d50=5000k
				{0x20,0x20,0x20,},//d65=6500k
				{0x20,0x20,0x20,},//d75=6500k
			},
		},
	},

	// CCM
	{

		// D65 light CCM
		//	{121, 1,  115, 8,  6, 8,  89, 8,  153, 1,  64, 8,  32, 8,  166, 8,  198, 1,  },//25%
		//{128, 1,  115, 8,  12, 8,  89, 8,  179, 1,  89, 8,  25, 8,  166, 8,  192, 1,  },
		//	{64,1,51,8,11,8,59,8,132,1,72,8,7,8,92,8,101,1, 0,0,0},//111014_PaoChi
		//    {90,1,77,8,11,8,71,8,150,1,78,8,65,8,40,8,108,1, 0,0,0},//111014_PaoChi
		//    {103,1,83,8,17,8,71,8,150,1,78,8,65,8,40,8,108,1, 0,0,0},//111014_PaoChi
		//	{110,1,104,8,7,8,77,8,147,1,70,8,13,8,118,8,131,1, 0,0,0},//111018_Leo
		//	{185,1,178,8,9,8,113,8,200,1,87,8,33,8,196,8,228,1, 0,0,0},//111021_LeoChou
		{0x181,-122,-8,-89,0x1a0,-71,-1,-138,0x18b},//120402_LeoChou
		//	{121, 1,  115, 8,  6, 8,  64, 8,  128, 1,  64, 8,  6, 8,  140, 8,  147, 1,  },//15%
		//	{121, 1,  101, 8,  20, 8,  100, 8,  182, 1,  82, 8,  20, 8,  146, 8,  166, 1,  },//rgb
		//	{80, 1,  80, 8,  0, 0,  33, 8,  101, 1,  67, 8,  0, 0,  80, 8,  80, 1,  },//delta c min
		//D35
		{0x160,  -76, -19, -166, 0x1f3, -76, -6, -179, 0x1b9},
		// low lux CCM
		//{253,0,19,0,12,8,48,8,93,1,43,8,27,8,47,0,239,0, 0,0,0},
		//{0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1, 0,0,0},
		{0x142,-59,-7,-58,0x173,-57,13,-72,0x13b},


		//	{0, 1,  0, 0,  0, 0, 0, 0,  0, 1,  0, 0,  0, 0,  0, 0,  0, 1, 0,0,0 },
		90,	// dynamic CCM Gain Threshold Low
		144,	// dynamic CCM Gain Threshold High
		3000,	// dynamic CCM A light Color Temperature Switch Threshold
		3600	// dynamic CCM D65 light Color Temperature Switch Threshold
	},

	// Gamma
	{
		// normal light gamma
		//  {0, 8, 15, 23, 31, 41, 50, 59, 70, 85, 100, 116, 130, 142, 152, 161, 169, 176, 182, 188, 192, 202, 210, 218, 225, 232, 240, 248, },
		//{0, 6, 13, 20, 27, 34, 42, 49, 58, 77, 96, 115, 131, 144, 153, 162, 169, 175, 181, 186, 191, 200, 209, 217, 225, 233, 241, 248, },
		//{0, 6, 12, 18, 24, 30, 36, 43, 51, 69, 87, 107, 126, 141, 154, 165, 174, 182, 188, 194, 199, 208, 216, 222, 227, 234, 241, 248, },
		//	{0, 4, 8, 13, 17, 21, 27, 35, 44, 56, 71, 84, 97, 111, 124, 135, 144, 152, 161, 169, 177, 191, 202, 213, 222, 231, 240, 248, },
		//	{0, 8, 15, 23, 30, 37, 45, 52, 60, 74, 87, 99, 112, 124, 133, 144, 153, 162, 169, 177, 183, 195, 205, 214, 223, 231, 239, 247},
		//	{0, 5, 13, 22, 32, 42, 52, 62, 71, 87, 102, 116, 129, 141, 152, 161, 169, 176, 182, 187, 193, 202, 210, 218, 225, 232, 240, 248},//111013
		//	{0, 6, 13, 20, 27, 34, 42, 50, 58, 77, 96, 115, 131, 144, 153, 162, 169, 175, 181, 186, 191, 200, 209, 217, 225, 233, 241, 248},//111019
		//	{0, 7, 15, 23, 31, 39, 47, 55, 63, 79, 96, 115, 131, 144, 153, 162, 169, 175, 181, 186, 191, 200, 209, 217, 225, 233, 241, 248},//111020_orig
		//		{0, 9, 18, 28, 38, 48, 58, 68, 78, 95, 111, 126, 137, 147, 155, 162, 168, 174, 180, 185, 190, 200, 209, 217, 224, 232, 240, 248},
		//		{0, 20, 37, 53, 66, 78, 88, 96, 104, 118, 128, 138, 147, 154, 160, 166, 172, 176, 181, 185, 189, 198, 207, 216, 223, 232, 240, 248},
		{0, 10, 20, 30, 39, 48, 57, 66, 74, 90, 103, 116, 127, 137, 146, 155, 164, 171, 178, 184, 190, 201, 211, 220, 228, 235, 242, 249},
		// low light gamma
		{0, 6, 13, 20, 27, 34, 42, 49, 58, 77, 96, 115, 131, 144, 153, 162, 169, 175, 181, 186, 191, 200, 209, 217, 225, 233, 241, 248, },
		80,	// dynamic Gamma Gain Threshold Low
		144	// dynamic Gamma Gain Threshold High
	},

	// AE
	{
		// AE target
		{
			40,// Histogram Ratio Low
			40,// Histogram Ratio High
			60,// YMean Target Low
			66,// YMean Target
			80,// YMean Target High
			10,// Histogram Position Low
			190,// Histogram Position High
			3// Dynamic AE Target decrease value
		},
		// AE limit
		{
			13,	// AE step Max value at 50Hz power line frequency
			16,	// AE step Max value at 60Hz power line frequency
			196,	// AE global gain Max value
			64,	// AE continous frame rate gain threshold
			144,	// AE discrete frame rate 15fps gain threshold
			112,	// AE discrete frame rate 30fps gain threshold
			120	// AE HighLight mode threshold
		},
		// AE weight
		{
			{
				1,1,1,1,1,
				1,3,3,3,1,
				1,6,6,6,1,
				2,5,5,5,2,
				3,4,4,4,3,
			}
		},
		// AE sensitivity
		{
			0.05,// g_fAEC_Adjust_Th
			16,	// AE Latency time
			//20,	// Ymean diff threshold for judge AE same block
			8,	// hemonel 2011-07-14: modify Neil adjust parameter from 20 to 8
			22	// same block count threshold for judge AE scene variation
		},
	},

	// AWB
	{
		{

			{37,37,49,54,61,65},	// g_aAWBRoughGain_R
			{81,72,65,52,43,41},	// g_aAWBRoughGain_B
			13,	// K1
			115,	// B1
			90,	// B2
			16,	// sK3
			25,	// sB3
			16,	// sK4
			-60,	// sB4
			16,	// sK5
			77,	// sB5
			15,	// sK6
			-80,	// sB6
			125,	// B_up
			80,	// B_down
			35,	// sB_left
			-70,	// sB_right
			230,	// Ymean range upper limit
			0,	// Ymean range low limit
			500,	// RGB sum threshold for AWB hold
		},
		// AWB advanced
		{
			5,	// white point ratio threshold
			38,	// g_byAWBFineMax_RG
			26,	// g_byAWBFineMin_RG
			38,	// g_byAWBFineMax_BG
			26,	// g_byAWBFineMin_BG
			225,	// g_byAWBFineMax_Bright
			20,	// g_byAWBFineMin_Bright
			30	// g_byFtGainTh
		},
		// AWB sensitivity
		{
			4, // g_wAWBGainDiffTh
			1, // g_byAWBGainStep
			10,	// g_byAWBFixed_YmeanTh
			7,	// g_byAWBColorDiff_Th
			12	// g_byAWBDiffWindowsTh
		}
	},

	// Texture
	{
		// sharpness
		{
			0,	// for CIF
			48,	// for VGA
			48,	// for HD
			0x20,// for low lux
			64,	// dynamic sharpness gain threshold low
			150	// dynamic sharpness gain threshold high
		},
		// sharpness & denoise paramter
		{
			// CIF
			{{
					0x0c,  	//ISP_NR_EDGE_THD,
					0x0a,	//ISP_NR_MM_THD1
					0x0, 	//ISP_NR_MODE0_LPF,
					0x0, 	//ISP_NR_MODE1_LPF,
					0xff,	//ISP_NR_MODE,
					0x00,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x02,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x02,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x86,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x0c,	//ISP_INTP_EDGE_THD1,
					0x3f,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x07,	//ISP_RGB_IIR_CTRL
					0x60,	//ISP_RGB_VIIR_COEF
					0xCC,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x06,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x01, 	//ISP_RGB_DIIR_COFF_CUT
					0x0a,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG
				},
				{
					0x0c,  	//ISP_NR_EDGE_THD,
					0x0a,	//ISP_NR_MM_THD1
					0x0, 	//ISP_NR_MODE0_LPF,
					0x0, 	//ISP_NR_MODE1_LPF,
					0xff,	//ISP_NR_MODE,
					0x00,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x02,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x02,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x86,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x0c,	//ISP_INTP_EDGE_THD1,
					0x3f,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x07,	//ISP_RGB_IIR_CTRL
					0x60,	//ISP_RGB_VIIR_COEF
					0xCC,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x06,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x01, 	//ISP_RGB_DIIR_COFF_CUT
					0x0a,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG

				},
				{
					0x08,  	//ISP_NR_EDGE_THD,
					0x0a,	//ISP_NR_MM_THD1
					0x0, 	//ISP_NR_MODE0_LPF,
					0x0, 	//ISP_NR_MODE1_LPF,
					0xff,	//ISP_NR_MODE,
					0x01,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x02,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x02,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x86,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x0c,	//ISP_INTP_EDGE_THD1,
					0x3f,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x0F,	//ISP_RGB_IIR_CTRL
					0xC0,	//ISP_RGB_VIIR_COEF
					0xCC,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x0d,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x01, 	//ISP_RGB_DIIR_COFF_CUT
					0x06,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG

				},
				{
					0x0e,  	//ISP_NR_EDGE_THD,
					0x0a,	//ISP_NR_MM_THD1
					0x0, 	//ISP_NR_MODE0_LPF,
					0x0, 	//ISP_NR_MODE1_LPF,
					0xff,	//ISP_NR_MODE,
					0x01,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x02,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x02,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x86,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x0c,	//ISP_INTP_EDGE_THD1,
					0x3f,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x0F,	//ISP_RGB_IIR_CTRL
					0xC0,	//ISP_RGB_VIIR_COEF
					0xCC,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x0d,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x01, 	//ISP_RGB_DIIR_COFF_CUT
					0x0c,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG

				}
			},
			// VGA resolution
			{{
					0x0c,  	//ISP_NR_EDGE_THD,
					0x0a,	//ISP_NR_MM_THD1
					0x01, 	//ISP_NR_MODE0_LPF,
					0x01, 	//ISP_NR_MODE1_LPF,
					0x57,	//ISP_NR_MODE,
					0x00,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x02,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x02,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x81,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x0c,	//ISP_INTP_EDGE_THD1,
					0x16,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x07,	//ISP_RGB_IIR_CTRL
					0x60,	//ISP_RGB_VIIR_COEF
					0x88,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x06,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x01, 	//ISP_RGB_DIIR_COFF_CUT
					0x0a,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG
				},
				{
					0x0c,  	//ISP_NR_EDGE_THD,
					0x0a,	//ISP_NR_MM_THD1
					0x01, 	//ISP_NR_MODE0_LPF,
					0x01, 	//ISP_NR_MODE1_LPF,
					0x57,	//ISP_NR_MODE,
					0x00,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x02,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x02,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x81,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x0c,	//ISP_INTP_EDGE_THD1,
					0x16,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x07,	//ISP_RGB_IIR_CTRL
					0x60,	//ISP_RGB_VIIR_COEF
					0x88,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x06,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x01, 	//ISP_RGB_DIIR_COFF_CUT
					0x0a,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG
				},
				{
					0x08,  	//ISP_NR_EDGE_THD,
					0x0a,	//ISP_NR_MM_THD1
					0x01, 	//ISP_NR_MODE0_LPF,
					0x01, 	//ISP_NR_MODE1_LPF,
					0xAB,	//ISP_NR_MODE,
					0x01,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x02,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x02,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x82,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x0c,	//ISP_INTP_EDGE_THD1,
					0x16,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x0F,	//ISP_RGB_IIR_CTRL
					0xC0,	//ISP_RGB_VIIR_COEF
					0xaa,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x0d,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x01, 	//ISP_RGB_DIIR_COFF_CUT
					0x06,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG

				},
				{
					0x0e,  	//ISP_NR_EDGE_THD,
					0x0a,	//ISP_NR_MM_THD1
					0x01, 	//ISP_NR_MODE0_LPF,
					0x01, 	//ISP_NR_MODE1_LPF,
					0xbb,	//ISP_NR_MODE,
					0x01,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x02,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x02,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x82,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x0c,	//ISP_INTP_EDGE_THD1,
					0x16,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x0F,	//ISP_RGB_IIR_CTRL
					0xC0,	//ISP_RGB_VIIR_COEF
					0xcc,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x0d,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x01, 	//ISP_RGB_DIIR_COFF_CUT
					0x0c,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG

				}
			},
			// HD resolution
			//[Albert, 2011/06/16]+++
			{{
					0x0c,  	//ISP_NR_EDGE_THD,
					0x0a,	//ISP_NR_MM_THD1
					0x01, 	//ISP_NR_MODE0_LPF,
					0x01, 	//ISP_NR_MODE1_LPF,
					0x57,	//ISP_NR_MODE,
					0x00,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x02,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x02,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x81,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x0c,	//ISP_INTP_EDGE_THD1,
					0x16,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x07,	//ISP_RGB_IIR_CTRL
					0x60,	//ISP_RGB_VIIR_COEF
					0x88,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x06,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x01, 	//ISP_RGB_DIIR_COFF_CUT
					0x0a,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG
				},
				{
					0x0c,  	//ISP_NR_EDGE_THD,
					0x0a,	//ISP_NR_MM_THD1
					0x01, 	//ISP_NR_MODE0_LPF,
					0x01, 	//ISP_NR_MODE1_LPF,
					0x57,	//ISP_NR_MODE,
					0x00,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x02,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x02,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x81,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x0c,	//ISP_INTP_EDGE_THD1,
					0x16,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x07,	//ISP_RGB_IIR_CTRL
					0x60,	//ISP_RGB_VIIR_COEF
					0x88,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x06,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x01, 	//ISP_RGB_DIIR_COFF_CUT
					0x0a,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG

				},
				{
					0x08,  	//ISP_NR_EDGE_THD,
					0x0a,	//ISP_NR_MM_THD1
					0x01, 	//ISP_NR_MODE0_LPF,
					0x01, 	//ISP_NR_MODE1_LPF,
					0xAB,	//ISP_NR_MODE,
					0x01,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x02,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x02,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x82,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x0c,	//ISP_INTP_EDGE_THD1,
					0x16,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x0F,	//ISP_RGB_IIR_CTRL
					0xC0,	//ISP_RGB_VIIR_COEF
					0xaa,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x0d,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x01, 	//ISP_RGB_DIIR_COFF_CUT
					0x06,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG

				},
				{
					0x0e,  	//ISP_NR_EDGE_THD,
					0x0a,	//ISP_NR_MM_THD1
					0x01, 	//ISP_NR_MODE0_LPF,
					0x01, 	//ISP_NR_MODE1_LPF,
					0xbb,	//ISP_NR_MODE,
					0x01,	//ISP_NR_COLOR_ENABLE_CTRL
					0x05,	//ISP_NR_COLOR_MAX
					0x02,	//ISP_NR_CHAOS_MAX
					0x04,	//ISP_NR_CHAOS_THD,
					0x02,	//ISP_NR_CHAOS_CFG,
					0x00,	//ISP_NR_DTHD_CFG,
					0x82,	//ISP_NR_GRGB_CTRL
					0x04,	//ISP_INTP_EDGE_THD0,
					0x0c,	//ISP_INTP_EDGE_THD1,
					0x16,	//ISP_INTP_MODE,
					0x02,	//ISP_INTP_CHAOS_MAX,
					0x04,	//ISP_INTP_CHAOS_THD,
					0x02,	//ISP_INTP_CHAOS_CFG,
					0x00,	//ISP_INTP_DTHD_CFG,
					0x0F,	//ISP_RGB_IIR_CTRL
					0xC0,	//ISP_RGB_VIIR_COEF
					0xcc,	//ISP_RGB_HLPF_COEF
					0xa2,	//ISP_RGB_DIIR_MAX
					0x0d,	//ISP_RGB_DIIR_COEF
					0x0C,	//ISP_RGB_BRIGHT_COEF
					0x01, 	//ISP_RGB_DIIR_COFF_CUT
					0x0c,   //ISP_EDG_DCT_THD1
					0x03,	//ISP_EEH_DTHD_CFG

				}
			},
			48,		// dynamic denoise gain threshold 0
			80,		// dynamic denoise gain threshold 1
			112		// dynamic denoise gain threshold 2
		}
	},
	// UV offset
	{
		0,		// A light U offset
		0,		// A light V offset
		0,//0x24,		// D65 light U offset
		0,//0x22,		// D65 light V offset
		3300,	// Dynamic UV offset threshold for A light
		4000	// Dynamic UV offset threshold for D65 light
	},

	// gamma 2
	{
		// normal lux
		{
			// R - G diff gamma
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			// B - G diff gamma
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		},

		// low lux
		{
			// R - G diff gamma
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			// B - G diff gamma
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		},
	},

	// Texture 2
	{
		// corner denoise
		{
			0x100,	//d0(start) for RGB domain
			0x300,	//d1(end) for RGB domain
			0x100,	//d0(start) for YUV domain
			0x300,	//d1(end) for YUV domain
			{
				{
					0x08,	//ISP_RDLOC_MAX
					0x20,	//ISP_RDLOC_RATE
					0x08,	//ISP_GLOC_MAX
					0x20,	//ISP_GLOC_RATE
					0x02,	//ISP_ILOC_MAX
					0x04,	//ISP_ILOC_RATE
					0x10,	//ISP_LOC_MAX
					0x40,	//ISP_LOC_RATE
				},
				{
					0x08,	//ISP_RDLOC_MAX
					0x20,	//ISP_RDLOC_RATE
					0x08,	//ISP_GLOC_MAX
					0x20,	//ISP_GLOC_RATE
					0x02,	//ISP_ILOC_MAX
					0x04,	//ISP_ILOC_RATE
					0x10,	//ISP_LOC_MAX
					0x40,	//ISP_LOC_RATE
				},
				{
					0x10,	//ISP_RDLOC_MAX
					0x40,	//ISP_RDLOC_RATE
					0x10,	//ISP_GLOC_MAX
					0x40,	//ISP_GLOC_RATE
					0x02,	//ISP_ILOC_MAX
					0x04,	//ISP_ILOC_RATE
					0x10,		//ISP_LOC_MAX
					0x40,	//ISP_LOC_RATE
				},
				{
					0x10,	//ISP_RDLOC_MAX
					0x40,	//ISP_RDLOC_RATE
					0x10,	//ISP_GLOC_MAX
					0x40,	//ISP_GLOC_RATE
					0x02,	//ISP_ILOC_MAX
					0x04,	//ISP_ILOC_RATE
					0x10,	//ISP_LOC_MAX
					0x40,	//ISP_LOC_RATE
				},
			},
		},
		// uv denoise
		{
			{
				{
					0x8,   //ISP_EEH_UVIIR_Y_CUTS
					4,   //ISP_EEH_UVIIR_Y_CMIN
					0xa,	 //ISP_EEH_IIR_COEF
					2,	 //ISP_EEH_IIR_CUTS
					1,  //ISP_EEh_IIR_STEP
				},
				{
					0x8,   //ISP_EEH_UVIIR_Y_CUTS
					4,   //ISP_EEH_UVIIR_Y_CMIN
					0xa,	 //ISP_EEH_IIR_COEF
					2,	 //ISP_EEH_IIR_CUTS
					1,  //ISP_EEh_IIR_STEP
				},
				{
					0x8,   //ISP_EEH_UVIIR_Y_CUTS
					4,   //ISP_EEH_UVIIR_Y_CMIN
					0xb,	 //ISP_EEH_IIR_COEF
					2,	 //ISP_EEH_IIR_CUTS
					1, //ISP_EEh_IIR_STEP
				},
				{
					0x8,   //ISP_EEH_UVIIR_Y_CUTS
					4,   //ISP_EEH_UVIIR_Y_CMIN
					0xb,	 //ISP_EEH_IIR_COEF
					2,	 //ISP_EEH_IIR_CUTS
					1, //ISP_EEh_IIR_STEP
				},
			},
		},
		// noise reduction additional
		{
			{0x1E,0x1E,0x1E,0x1E,},	//ISP_EEH_CRC_RATE
			{2,2,1,1,},	//ISP_RD_MM2_RATE
			{
				{
					0x02,	//ISP_NR_MMM_D0
					0x22,	//ISP_NR_MMM_D1
					0xE0,	//ISP_NR_MMM_RATE
					0x10,	//ISP_NR_MMM_MIN
					0x80,	//ISP_NR_MMM_MAX
				},
				{
					0x02,	//ISP_NR_MMM_D0
					0x22,	//ISP_NR_MMM_D1
					0xE0,	//ISP_NR_MMM_RATE
					0x10,	//ISP_NR_MMM_MIN
					0x80,	//ISP_NR_MMM_MAX
				},
				{
					0x02,	//ISP_NR_MMM_D0
					0x22,	//ISP_NR_MMM_D1
					0xF0,	//ISP_NR_MMM_RATE
					0x02,	//ISP_NR_MMM_MIN
					0xA4,	//ISP_NR_MMM_MAX
				},
				{
					0x02,	//ISP_NR_MMM_D0
					0x22,	//ISP_NR_MMM_D1
					0x78,	//ISP_NR_MMM_RATE
					0x10,	//ISP_NR_MMM_MIN
					0x40,	//ISP_NR_MMM_MAX
				},
			},
		},
	},

	// Edge enhance
	{
		0x01, 			//ISP_BRIGHT_RATE
		0x02, 			//ISP_BRIGHT_TRM_B1
		0x10, 			//ISP_BRIGHT_TRM_B2
		0x0a, 			//ISP_BRIGHT_TRM_K
		0x03, 			//ISP_BRIGHT_TRM_THD0
		0x18, 			//ISP_BRIGHT_TRM_THD1
		0x01, 			//ISP_DARK_RATE
		0x02, 			//ISP_DARK_TRM_B1
		0x10, 			//ISP_DARK_TRM_B2
		0x0a, 			//ISP_DARK_TRM_K
		0x03, 			//ISP_DARK_TRM_THD0
		0x18, 			//ISP_DARK_TRM_THD1
		20,  				//ISP_EDG_DIFF_C0
		3, 			//ISP_EDG_DIFF_C1
		-1, 			//ISP_EDG_DIFF_C2
		-3, 	    		//ISP_EDG_DIFF_C3
		-2, 			//ISP_EDG_DIFF_C4
		0x08, 			//ISP_EEH_SHARP_ARRAY 0
		0x0C, 			//ISP_EEH_SHARP_ARRAY10
		0x10, 			//ISP_EEH_SHARP_ARRAY11
		0x0C, 			//ISP_EEH_SHARP_ARRAY1
		0x10, 			//ISP_EEH_SHARP_ARRAY2
		0x0C, 			//ISP_EEH_SHARP_ARRAY3
		0x18, 			//ISP_EEH_SHARP_ARRAY4
		0x18, 			//ISP_EEH_SHARP_ARRAY5
		0x10, 			//ISP_EEH_SHARP_ARRAY6
		0x10, 			//ISP_EEH_SHARP_ARRAY7
		0x10, 			//ISP_EEH_SHARP_ARRAY8
		0x08, 			//ISP_EEH_SHARP_ARRAY9

	},

	// flase color and morie
	{
		{
			{
				0x02, 			//ISP_FCRD_MIN
				0x12, 			//ISP_FCRD_YCUTS
				0x90, 			//ISP_FCRD_CFG
				0x1f, 			//ISP_FCRD_MAX
				0x20,			//ISP_MOIRE_RATE
				0x02,			//MOIRE detect threshold
			},
			{
				0x02, 			//ISP_FCRD_MIN
				0x12, 			//ISP_FCRD_YCUTS
				0x90, 			//ISP_FCRD_CFG
				0x1f, 			//ISP_FCRD_MAX
				0x20,			//ISP_MOIRE_RATE
				0x02,			//MOIRE detect threshold
			},
			{
				0x02, 			//ISP_FCRD_MIN
				0x12, 			//ISP_FCRD_YCUTS
				0x90, 			//ISP_FCRD_CFG
				0x1f, 			//ISP_FCRD_MAX
				0x10,			//ISP_MOIRE_RATE
				0x04,			//MOIRE detect threshold
			},
			{
				0x02, 			//ISP_FCRD_MIN
				0x12, 			//ISP_FCRD_YCUTS
				0x90, 			//ISP_FCRD_CFG
				0x1f, 			//ISP_FCRD_MAX
				0x10,			//ISP_MOIRE_RATE
				0x04,			//MOIRE detect threshold
			},
		}

	},

	// dead pixel cancel
	{
		0x01,	//  ISP_DDP_CTRL
		0x00, 	// ISP_DDP_SEL
		0x04,	//	ISP_DP_THD_D1
		0x02, 	//	ISP_DP_BRIGHT_THD_MIN
		0x0A,	//  ISP_DP_BRIGHT_THD_MAX
		0x03, 	//  ISP_DP_DARK_THD_MIN
		0x10,	//  ISP_DP_DARK_THD_MAX
		0x02,	//  ISP_DDP_BRIGHT_RATE
		0x02,	//  ISP_DDP_DARK_RATE
	},

	// UV color tune
	{
		//A light UV color tune
		{
			0, //ISP_UVT_UCENTER
			0, //ISP_UVT_VCENTER
			0, //ISP_UVT_UINC
			0, //ISP_UVT_VINC
		},
		//D65 light UV color tune
		{
			0,  //ISP_UVT_UCENTER
			0,    //ISP_UVT_VINC
			0,   //ISP_UVT_UINC
			0,   //ISP_UVT_VINC
		},
		3300, // Dynamic UV color tune threshold for A light
		4000,  // Dynamic UV color tune threshold for D65 light
	},

	// NLC
	{
		// G NLC
		{0, 16, 32, 48, 64, 80, 96, 112, 128, 144, 160, 176, 192, 208, 224, 240},
		// R - G diff
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		// B - G diff
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
	},

	// HDR
	{
		// HDR threshold
		{
			10,		// Histogram dark pixel threshold for start HDR function
			10,     // Histogram bright pixel threshold for start HDR function
			60, 	// Histogram dark pixel max number
			64,		// Hdr tune max value

		},

		// FW HDR
		{
			// HDR gamma
			{0,23,43,58,73,81,88,94,99,109,117,125,132,139,145,151,157,163,168,173,177,184,190,195,201,210,222,237},
		},

		// HW HDR
		{
			// tgamma threshold
			0x8,
			// tgamma rate
			0x10,
			// HDR LPF COEF
			{0, 1, 3, 5, 6, 6, 5, 4, 2},
			// HDR halo thd
			0x10,
			// HDR curver
			{115, 115, 114, 112, 111, 109, 108, 106, 104, 100, 96, 91, 85, 79, 72, 64, 54, 45, 35, 26, 16, 10, 6, 3},
			// HDR max curver
			{192, 144, 112, 80, 61, 52, 42, 36, 32},
			//HDR step
			0x4,
			//local constrast curver
			{24, 26, 26, 24, 20, 20, 20,20, 20, 20, 20, 20, 20, 23, 26, 26},
			//local constrast rate min
			0xd,
			//local constrast rate max
			0xd,
			//local constrast step
			0x10
		},
	}

};
#endif // _OV9726_IQ_H_
