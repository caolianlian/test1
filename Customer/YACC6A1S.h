#ifndef _YACC6A1S_H_
#define _YACC6A1S_H_

#define YACC6A1S_AE_TARGET 0x3f

extern OV_CTT_t code gc_YACC6A1S_CTT[3];
void YACC6A1SSetFormatFps(U8 SetFormat, U8 byFps);
void SetYACC6A1SImgDir(U8 bySnrImgDir) ;
void CfgYACC6A1SControlAttr(void);
#ifndef _USE_BK_HSBC_ADJ_
void SetYACC6A1SBrightness(S16 swSetValue);
void	SetYACC6A1SContrast(U16 wSetValue);
void	SetYACC6A1SSaturation(U16 wSetValue);
void	SetYACC6A1SHue(S16 swSetValue);
#endif
void	SetYACC6A1SSharpness(U8 bySetValue);
void SetYACC6A1SOutputDim(U16 wWidth,U16 wHeight);
void SetYACC6A1SPwrLineFreq(U8 byFPS, U8 byLightFrq);
void SetYACC6A1SWBTemp(U16 wSetValue);
OV_CTT_t GetYACC6A1SAwbGain(void);
void SetYACC6A1SWBTempAuto(U8 bySetValue);
void SetYACC6A1SBackLightComp(U8 bySetValue);
U16  GetYACC6A1SAEGain();
//void SetYACC6A1SISPMisc();
void SetYACC6A1SIntegrationTimeAuto(U8 bySetValue);
void SetYACC6A1SIntegrationTime(U32 dwSetValue);
void SetYACC6A1SGain(float fGain);
#endif
