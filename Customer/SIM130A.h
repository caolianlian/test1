#ifndef _SIM130A_H_
#define _SIM130A_H_

#define SIM130A_AE_TARGET	0x78

void SIM130ASetFormatFps(U8 SetFormat, U8 Fps);
void CfgSIM130AControlAttr(void);
void	SetSIM130ASharpness(U8 bySetValue);
void SetSIM130AImgDir(U8 bySnrImgDir);
void SetSIM130AOutputDim(U16 wWidth,U16 wHeight);
void SetSIM130APwrLineFreq(U8 byFPS, U8 byLightFrq);
void SetSIM130AWBTemp(U16 wSetValue);
//OV_CTT_t GetSIM120CAwbGain(void);
void SetSIM130AWBTempAuto(U8 bySetValue);
void SetSIM130ABackLightComp(U8 bySetValue);
void SetSIM130AISPMisc();
extern code OV_CTT_t gc_SIM130A_CTT[3];
U16 GetSIM130AAEGain(void);
void SetSIM130AIntegrationTimeAuto(U8 bySetValue);
void SetSIM130AIntegrationTime(U16 wEspline);
#endif // _OV7670_H_

