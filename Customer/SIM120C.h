#ifndef _SIM120C_H_
#define _SIM120C_H_

#define SIM120C_AE_TARGET	0x78

void SIM120CSetFormatFps(U8 SetFormat, U8 Fps);
void CfgSIM120CControlAttr(void);
void	SetSIM120CSharpness(U8 bySetValue);
void SetSIM120CImgDir(U8 bySnrImgDir);
void SetSIM120COutputDim(U16 wWidth,U16 wHeight);
void SetSIM120CPwrLineFreq(U8 byFPS, U8 byLightFrq);
void SetSIM120CWBTemp(U16 wSetValue);
//OV_CTT_t GetSIM120CAwbGain(void);
void SetSIM120CWBTempAuto(U8 bySetValue);
void SetSIM120CBackLightComp(U8 bySetValue);
extern code OV_CTT_t gc_SIM120C_CTT[3];
U16 GetSIM120CAEGain(void);
void SetSIM120CIntegrationTimeAuto(U8 bySetValue);
void SetSIM120CIntegrationTime(U16 wEspline);
void SetSIM120CGain(float fGain);
#endif // _OV7670_H_
