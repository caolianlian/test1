#ifndef _IMX188PQ_H_
#define _IMX188PQ_H_

void IMX188PQSetFormatFps(U16 SetFormat, U8 Fps);
void CfgIMX188PQControlAttr(void);
void SetIMX188PQImgDir(U8 bySnrImgDir);
void SetIMX188PQIntegrationTime(U16 wEspline);
void SetIMX188PQExposuretime_Gain(float fExpTime, float wGain);
void SetIMX188PQDynamicISP(U8 byAEC_Gain);
void IMX188PQ_POR();
void SetIMX188PQDefGain();
void SetIMX188PQGain(float fGain);
#endif // _IMX188PQ_H_

