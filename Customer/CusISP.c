#include "Inc.h"
#include "CusISP.h"

extern S16 g_aCCM[9];

void SetBKAWBGain(U16 wRGain,U16 wGGain,U16 wBGain,U8 byflag);
void SetExpTime_Gain(float fAE_Value);
U8 VCM_Write(U16 wData);
void SetCCM(void);

#ifdef _CUSTOMER_ISP_

// This function will be called first after system init finished
void CusISPInit(void)
{

}

// Start AWB statistics, this function will be loop called in main()
void CusAWBStaticsStart(void)
{
	if(g_byISPAWBStaticsEn == 1)
	{
		g_byISPAWBStaticsEn = 0;
		XBYTE[ISP_AWB_STATIS_CTRL] = ISP_AWB_STAT_START|ISP_AWB_ROUGH_EN;
	}
}

// Start AE statistics, this function will be loop called in main()
void CusAEStaticsStart(void)
{
	if(g_byISPAEStaticsEn == 1)
	{
		g_byISPAEStaticsEn = 0;	
		XBYTE[ISP_AE_CTRL] |= ISP_AE_STAT_START;
	}
}

// Get AWB 25 windows statistics
static void CusGetAWBStat(void)
{
	U8 i,j;

	for(i=0; i<5; i++)
	{
		for(j=0; j<5; j++)
		{
			XBYTE[ISP_AWB_WIN_ADDR]=i*5+j;
			g_byAWB_WinMeanR[i][j] =(r_dwISP_AWB_WIN_SUM_R>>2)/(U32)g_wAWBSTnum;
			g_byAWB_WinMeanG[i][j] =(r_dwISP_AWB_WIN_SUM_G>>2)/(U32)g_wAWBSTnum;
			g_byAWB_WinMeanB[i][j] =(r_dwISP_AWB_WIN_SUM_B>>2)/(U32)g_wAWBSTnum;
		}
	}
}

// This function implement AWB Algorithm, and it will be called after AWB complete the statistics
void CusAWBAlgorithm(void)
{
	U16 wRGain, wGGain, wBGain;

	// Get AWB statistics
	CusGetAWBStat();

	// At here, write your AWB algorithm code, change R/G/B gain, the real gain set is wXGain*256
	wRGain=256;
	wGGain=256;
	wBGain=256;

	SetBKAWBGain(wRGain, wGGain, wBGain, 1);

	// Save the last value
	g_wAWBRGain_Last = wRGain;
	g_wAWBGGain_Last = wGGain;
	g_wAWBBGain_Last = wBGain;

	// start next AWB statistics
	g_byISPAWBStaticsEn=1;
}

static void CusGetAEStat(void)
{
	S8 i;
	U16 wSTnum;
	U16 wYsum;

	wSTnum = g_wSTnum*25;

	//Get Historgram statistics
	for (i = 0; i < 64; i++)
	{
		XBYTE[ISP_AE_ADDR]= (U8)i;
		ASSIGN_INT(g_wAE_Hist_StNum[i], XBYTE[ISP_AE_SUM_H], XBYTE[ISP_AE_SUM_L]);
	}

	//use AE window statics calculate Y average
	for(i=0; i<25; i++)
	{
		XBYTE[ISP_AE_ADDR] = 64+(U8)i;
		ASSIGN_INT(wYsum, XBYTE[ISP_AE_SUM_H], XBYTE[ISP_AE_SUM_L]);
		g_byAE_WinYMean[i] = (((U32)wYsum)<<8)/(U32)g_wSTnum;
	}
}

// This function implement AE Algorithm, and it will be called after AE complete the statistics
void CusAEAlgorithm(void)
{
	float fExpTime;

	CusGetAEStat();

	//At here, write your AE algorithm code
	fExpTime = 100;

	//The parameter of SetExpTime_Gain is based on 0.1 ms
	SetExpTime_Gain(fExpTime);

	// save the last value
	g_fAEC_EtGain= fExpTime;

	// start next AE statistics
	g_byISPAEStaticsEn=1;	
}

void CusAFStaticsStart(void)
{
	if (FocusAutoItem.Last == CTL_DEF_CT_FOCUS_AUTO)
	{
		XBYTE[ISP_AF_CTRL] = ISP_AF_STAT_START;
	}
}

// This function implement AF Algorithm, and it will be called after AF complete the statistics
void CusAFAlgorithm(void)
{
	U16 wCamPos;
	U32 dwAFSharp0, dwAFSharp1;

	// dwAFSharp0 is perpheral 16 windows sharp value, dwAFSharp1 is internal 9 windows sharp value
	dwAFSharp0 = r_dwISP_AF_SUM0;
	dwAFSharp1 = r_dwISP_AF_SUM1;

	//At here, write your AF algorithm code
	wCamPos = 100;

	//Set the camera focus position, the range is mostly 0~1023
	VCM_Write(wCamPos);

	// save the last value
	g_wAF_CurPosition = 	wCamPos;

	// start next AF statistics
	CusAFStaticsStart();
}

// This function called per frame
void CusCalledPerFrame(void)
{
	// set ccm parameters
	memset(g_aCCM, 0 , 18);
	g_aCCM[0] = 0x100;
	g_aCCM[4] = 0x100;
	g_aCCM[8] = 0x100;

	// load ccm parameters
	SetCCM();

}

#endif

