#ifndef _GC1036_H_
#define _GC1036_H_

void GC1036SetFormatFps(U16 SetFormat, U8 Fps);
void CfgGC1036ControlAttr(void);
void SetGC1036ImgDir(U8 bySnrImgDir);
void SetGC1036IntegrationTime(U16 wEspline);
void SetGC1036Exposuretime_Gain(float fExpTime, float fTotalGain);
void InitGC1036IspParams(void );
void InitGC1036IspParamsByResolution(U16 wCurWidth, U16 wCurHeight);
void GC1036_POR(void );
void SetGC1036DynamicISP(U8 byAEC_Gain);
void SetGC1036DynamicISP_AWB(U16 wColorTempature);
#endif // _OV9710_H_

