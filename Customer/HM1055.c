#include "Inc.h"

#ifdef RTS58XX_SP_HM1055
//_________________________
#define DYNAMIC_DarkMode_CCM_EN		0x0800
//__________________________
U8 CCM_frame_count =0;
U8 g_byCCM2State  = 0x00;
U16 MapSnrGlbGain2SnrRegSetting(U16 wGain);
void WriteHM1055Exposuretime(U16 wExposureRows);
void WriteHM1055Gain(U16 wGain);
void WriteHM1055DummyLine(U16 wDummy);


U8 Digital_fractional_sensorGain;
OV_CTT_t code gc_HM1055_CTT[3] =
{
	{2800,256,384,544},
	{4050,259,256,328},
	{6700,341,288,256},
};

code OV_FpsSetting_t  g_staHM1055FpsSetting[]=
{
	{10,	(1296+306),		(0x81), 		12000000},
	{15,		(1296+306),		(0x80), 		24000000},
	{20,		(1296+306),		(0x80), 		24000000},
	{25,		(1296+306),		(0x00),		36000000},
	{30,		(1296+306),		(0x00),		36000000},   //extra_dummy_pixel = 1296 + dummy_pixel  ?n����?��???A��@|C��`?��??
};

t_RegSettingWB code gc_HM1055_Setting[] =
{
	{ 0x0022, 0x00},	// RESET
	{ 0x0015, 0x01},	//
	{ 0x0016, 0xF4},	//
	{ 0x0018, 0x01},	//
	{ 0x0020, 0x08},	//
	{ 0x0027, 0x23},	// FPGA 24
	{ 0x0004, 0x10},	//
	{ 0x0006, 0x00},	// Modified by Brandon. No Flip
	{ 0x0012, 0x0F},	//
	{ 0x0025, 0x80},	//
	{ 0x0026, 0xF7},	// For 30fps @ 48MHz MCLK(4/19, K.Kim)
	{ 0x002A, 0x44},	// Modified by Wilson
	{ 0x002B, 0x00},	//
	{ 0x002C, 0x13},	// Turn on DCC (4/19, K.Kim)
	{ 0x004A, 0x0A},	//
	{ 0x004B, 0x72},	//
	{ 0x0070, 0x2A},	//
	{ 0x0071, 0x46},	//
	{ 0x0072, 0x55},	//
	{ 0x0080, 0xC2},	//
	{ 0x0082, 0xA2},	//
	{ 0x0083, 0xF0},	//
	{ 0x0085, 0x10},	// Set ADC power to 75% and Increase Comparator power and reduce ADC swing range(4/19, K.Kim)
	{ 0x0086, 0x22},	//
	{ 0x0087, 0x08},	//
	{ 0x0088, 0xAD},	// Temperature config 0x60:62, Minimize CTIA & PGA power and set VCMI to 200mV(4/19, K.Kim)
	{ 0x0089, 0x2A},	//
	{ 0x008A, 0x2F},	// Set BLC power to 50%(4/19, K.Kim)
	{ 0x008D, 0x20},	//
	{ 0x0090, 0x01},	//
	{ 0x0091, 0x02},	//
	{ 0x0092, 0x03},	//
	{ 0x0093, 0x04},	//
	{ 0x0094, 0x14},	//
	{ 0x0095, 0x09},	//
	{ 0x0096, 0x0A},	//
	{ 0x0097, 0x0B},	//
	{ 0x0098, 0x0C},	//
	{ 0x0099, 0x04},	//
	{ 0x009A, 0x14},	//
	{ 0x009B, 0x34},	//
	{ 0x00A0, 0x00},	//
	{ 0x00A1, 0x00},	//
	{ 0x0B3B, 0x0B},	//
	{ 0x0040, 0x0A},	// BLC Phase1
	{ 0x0053, 0x0A},	// BLC Phase2
	{ 0x0120, 0x37},	// 36:50Hz, 37:60Hz
	{ 0x0121, 0x80},
	{ 0x0122, 0xE9},	// 122[3] is temperature_control_bit2
	{ 0x0123, 0xCC},	// 123[3:0]maskTH
	{ 0x0124, 0xDE},
	{ 0x0125, 0xDF},
	{ 0x0126, 0x70},
	{ 0x0128, 0x1F},
	{ 0x0132, 0xF8},
	{ 0x011F, 0x88},        // BPC COLD PIXEL ENABLE if 11F[0] = 1 then on else 0 off
	{ 0x0144, 0x0C},        // Hot pixel ratio
	{ 0x0145, 0x00},        // Hot pixel ratio alpha
	{ 0x0146, 0x1E},        // Vertical Line TH
	{ 0x0147, 0x1E},        // Vertical Line TH alpha
	{ 0x0148, 0x14},        // HD_2line_BPC_TH
	{ 0x0149, 0x14},        // HD_2line_BPC_TH alpha
	{ 0x0156, 0x0C},        // BPC RATIO FOR COLD PIXEL
	{ 0x0157, 0x0C},        // BPC RATIO FOR COLD PIXEL
	{ 0x0158, 0x0A},        // 2 LINE BPC RATIO
	{ 0x0159, 0x0A},        // 2 LINE BPC RATIO
	{ 0x015A, 0x03},        // BPC RATIO FOR COLD PIXEL
	{ 0x015B, 0x40},        // Corner TH
	{ 0x015C, 0x21},        // Corner TH  alpha
	{ 0x015E, 0x0F},        // Debug_mode
	{ 0x0168, 0xC8},        // BPC T1
	{ 0x0169, 0xC8},        // BPC T1 ALPHA
	{ 0x016A, 0x96},        // BPC T2
	{ 0x016B, 0x96},        // BPC T2 ALPHA
	{ 0x016C, 0x64},        // BPC T3
	{ 0x016D, 0x64},        // BPC T3 ALPHA
	{ 0x016E, 0x32},        // BPC T4
	{ 0x016F, 0x32},        // BPC T4 ALPHA
	{ 0x01EF, 0x31},        // BPC Control Byte
	{ 0x0131, 0x48},  	//[6] DS_EN, [5] VOFF, [4] EDGEON, [3] SBPI_MODE, [2] RAW_SAHRPNESS_EN, [1]G1G2B_EN
	{ 0x014C, 0x60},	//VSUB
	{ 0x014D, 0x24},  	//VSUB ALPHA0
	{ 0x015D, 0x90},  	//[7] TG_en// [6:0]:min Tgain
	{ 0x01D8, 0x40},  	//NOISE TH
	{ 0x01D9, 0x20},  	//NL_V1
	{ 0x01DA, 0x23},  	//NL_V2
	{ 0x0150, 0x05},  	//NL_INC1
	{ 0x0155, 0x07},  	//NL_INC2
	{ 0x0178, 0x10},  	//EVDIV
	{ 0x017A, 0x10},  	//EVDIVD
	{ 0x01BA, 0x10},  	//EVSUB
	{ 0x0176, 0x00},  	//EDGE2W
	{ 0x0179, 0x10},  	//EVDIV ALPHA0
	{ 0x017B, 0x10},  	//EVDIVD ALPHA0
	{ 0x01BB, 0x10},  	//EVSUB ALPHA0
	{ 0x0177, 0x00},  	//EDGE2W ALPHA0
	{ 0x01E7, 0x20},  	//TLTH1
	{ 0x01E8, 0x30},  	//TLTH2
	{ 0x01E9, 0x50},  	//TLTH3
	{ 0x01E4, 0x0C},  	//BD STRENGTH
	{ 0x01E5, 0x20},  	//BD STRENGTH ALPHA0
	{ 0x01E6, 0x04},  	//BD STRENGTH OUTDOOR
	{ 0x0210, 0x20},  	//STD
	{ 0x0211, 0x08},  	//STD ALPHA0
	{ 0x0212, 0x20},	//STD OUTDOOR
	{ 0x01DB, 0x04},  	//G1G2 STRENGTH
	{ 0x015E, 0x0F},  	//DEBUG MODE (>8 OFF)
	{ 0x01DC, 0x14},  	//SBPI_OFFSET
	{ 0x0151, 0x08},
	{ 0x0152, 0x08},
	{ 0x015D, 0x0C},
	{ 0x01F2, 0x18},
	{ 0x01F3, 0x18},
	{ 0x01F8, 0x3C},
	{ 0x01F9, 0x3C},
	{ 0x01FE, 0x24},
	{ 0x01FF, 0x24},
	{ 0x0213, 0x03},  	// BPC_Control_BYTE_2 // BPC_line_edge_th 3rd bpc
	{ 0x0214, 0x03},  	// BPC_Control_BYTE_3 // 3rd bpc strength[4:0] , [7:5] is for 2line bpc temp_control_bit
	{ 0x0215, 0x10},  	// BPC_Control_BYTE_4 // 1st bpc strength[4:0]
	{ 0x0216, 0x08},  	// BPC_Control_BYTE_5 // 1st br pulse strength[4:0]
	{ 0x0217, 0x05},  	// BPC_Control_BYTE_6 // 1st gbgr pulse strengh[4:0]
	{ 0x0218, 0x40},
	{ 0x0219, 0x02},
	{ 0x021A, 0x40},
	{ 0x021B, 0x02},
	{ 0x021C, 0x40},
	{ 0x021D, 0x02},
	{ 0x021E, 0x40},
	{ 0x021F, 0x02},
	{ 0x0220, 0x01},
	{ 0x0221, 0x80},
	{ 0x0222, 0x06},
	{ 0x0223, 0x80},
	{ 0x0224, 0x6E},
	{ 0x0225, 0x00},
	{ 0x0226, 0x80},
	{ 0x022A, 0x74},
	{ 0x022B, 0x00},
	{ 0x022C, 0x80},
	{ 0x022D, 0x11},
	{ 0x022E, 0x0B},
	{ 0x022F, 0x11},
	{ 0x0230, 0x0B},
	{ 0x0233, 0x11},
	{ 0x0234, 0x0B},
	{ 0x0235, 0x88},
	{ 0x0236, 0x02},
	{ 0x0237, 0x88},
	{ 0x0238, 0x02},
	{ 0x023B, 0x80},
	{ 0x023C, 0x02},
	{ 0x023D, 0x6E},
	{ 0x023E, 0x01},
	{ 0x023F, 0x6E},
	{ 0x0240, 0x01},
	{ 0x0243, 0x6E},
	{ 0x0244, 0x01},
	{ 0x0251, 0x06},
	{ 0x0252, 0x00},
	{ 0x0260, 0x00},
	{ 0x0261, 0x68},
	{ 0x0262, 0x06},
	{ 0x0263, 0x80},
	{ 0x0264, 0x64},
	{ 0x0265, 0x00},
	{ 0x0266, 0x80},
	{ 0x026A, 0x5C},
	{ 0x026B, 0x00},
	{ 0x026C, 0x80},
	{ 0x0278, 0xA0},	// C8
	{ 0x0279, 0x40},
	{ 0x027A, 0x80},
	{ 0x027B, 0x80},	// 98
	{ 0x027C, 0x08},
	{ 0x027D, 0x80},
	{ 0x0280, 0x0A},    	// Gamma
	{ 0x0282, 0x17},
	{ 0x0284, 0x2D},
	{ 0x0286, 0x53},
	{ 0x0288, 0x62},
	{ 0x028A, 0x6F},
	{ 0x028C, 0x7A},
	{ 0x028E, 0x83},
	{ 0x0290, 0x8B},
	{ 0x0292, 0x93},
	{ 0x0294, 0x9F},
	{ 0x0296, 0xA9},
	{ 0x0298, 0xBC},
	{ 0x029A, 0xCE},
	{ 0x029C, 0xE2},
	{ 0x029E, 0x2A},
	{ 0x02A0, 0x02},
	{ 0x02C0, 0xAE},	// CCM
	{ 0x02C1, 0x01},
	{ 0x02C2, 0xAC},
	{ 0x02C3, 0x04},
	{ 0x02C4, 0x02},
	{ 0x02C5, 0x04},
	{ 0x02C6, 0x20},
	{ 0x02C7, 0x04},
	{ 0x02C8, 0x52},
	{ 0x02C9, 0x01},
	{ 0x02CA, 0x32},
	{ 0x02CB, 0x04},
	{ 0x02CC, 0x08},
	{ 0x02CD, 0x04},
	{ 0x02CE, 0xBC},
	{ 0x02CF, 0x04},
	{ 0x02D0, 0xC4},
	{ 0x02D1, 0x01},
	{ 0x0302, 0x00},
	{ 0x0303, 0x0A},
	{ 0x0304, 0x00},
	{ 0x02E0, 0x00},
	{ 0x02F0, 0x3d},	// CCM
	{ 0x02F1, 0x04},
	{ 0x02F2, 0x03},
	{ 0x02F3, 0x01},
	{ 0x02F4, 0xc6},
	{ 0x02F5, 0x04},
	{ 0x02F6, 0x3c},
	{ 0x02F7, 0x04},
	{ 0x02F8, 0x19},
	{ 0x02F9, 0x00},
	{ 0x02FA, 0x23},
	{ 0x02FB, 0x00},
	{ 0x02FC, 0x4f},
	{ 0x02FD, 0x04},
	{ 0x02FE, 0x51},
	{ 0x02FF, 0x00},
	{ 0x0300, 0x02},
	{ 0x0301, 0x04},
	{ 0x0305, 0x14},
	{ 0x0306, 0x0e},
	{ 0x0307, 0x00},
	{ 0x032D, 0x70},
	{ 0x032E, 0x01},
	{ 0x032F, 0x00},
	{ 0x0330, 0x01},
	{ 0x0331, 0x70},
	{ 0x0332, 0x01},
	{ 0x0333, 0x88},	// AWB channel offset
	{ 0x0334, 0x00},
	{ 0x0335, 0x8A},
	{ 0x033E, 0x04},
	{ 0x033F, 0x86},
	{ 0x0340, 0x30},	// AWB
	{ 0x0341, 0x44},
	{ 0x0342, 0x4A},
	{ 0x0343, 0x43},	// CT1
	{ 0x0344, 0x83},  	//
	{ 0x0345, 0x4D},	// CT2
	{ 0x0346, 0x75},  	//
	{ 0x0347, 0x5B},	// CT3
	{ 0x0348, 0x68},	//
	{ 0x0349, 0x68},	// CT4
	{ 0x034A, 0x60},	//
	{ 0x034B, 0x70},	// CT5
	{ 0x034C, 0x55},	//
	{ 0x0350, 0x88},
	{ 0x0351, 0x80},
	{ 0x0352, 0x18},
	{ 0x0353, 0x18},
	{ 0x0354, 0x80},
	{ 0x0355, 0x4A},
	{ 0x0356, 0x80},
	{ 0x0357, 0xE0},
	{ 0x0358, 0x06},
	{ 0x035A, 0x06},
	{ 0x035B, 0xAC},
	{ 0x035C, 0x6C},
	{ 0x035D, 0x78},
	{ 0x035E, 0xBC},
	{ 0x035F, 0xAC},
	{ 0x0360, 0x02},
	{ 0x0361, 0x18},
	{ 0x0362, 0x50},
	{ 0x0363, 0x6C},
	{ 0x0364, 0x00},
	{ 0x0365, 0xF0},
	{ 0x0366, 0x08},
	{ 0x0367, 0x00},
	{ 0x0369, 0x00},
	{ 0x036A, 0x10},
	{ 0x036B, 0x18},
	{ 0x036E, 0x10},
	{ 0x036F, 0x00},
	{ 0x0370, 0x10},
	{ 0x0371, 0x18},
	{ 0x0372, 0x0C},
	{ 0x0373, 0x38},
	{ 0x0374, 0x3A},
	{ 0x0375, 0x12},
	{ 0x0376, 0x20},
	{ 0x0380, 0xFC},
	{ 0x0381, 0x4A},
	{ 0x0382, 0x3A},
	{ 0x038A, 0x80},
	{ 0x038B, 0x10},
	{ 0x038C, 0xC1},
	{ 0x038E, 0x42},
	{ 0x038F, 0x09},
	{ 0x0390, 0xE0},
	{ 0x0391, 0x01},
	{ 0x0393, 0x80},
	{ 0x0395, 0x22},
	{ 0x0398, 0x01},	// AE Frame Control
	{ 0x0399, 0xF4},
	{ 0x039A, 0x02},
	{ 0x039B, 0x71},
	{ 0x039C, 0x02},
	{ 0x039D, 0xEE},
	{ 0x039E, 0x03},
	{ 0x039F, 0xE8},
	{ 0x03A0, 0x04},
	{ 0x03A1, 0xE2},
	{ 0x03A2, 0x05},
	{ 0x03A3, 0xDC},
	{ 0x03A4, 0x07},
	{ 0x03A5, 0xD0},
	{ 0x03A6, 0x20},
	{ 0x03A7, 0x24},
	{ 0x03A8, 0x28},
	{ 0x03A9, 0x30},
	{ 0x03AA, 0x38},
	{ 0x03AB, 0x40},
	{ 0x03AC, 0x30},
	{ 0x03AD, 0x2e},
	{ 0x03AE, 0x26},
	{ 0x03AF, 0x1e},
	{ 0x03B0, 0x1e},
	{ 0x03B1, 0x19},
	{ 0x03B7, 0x64},	// AEAWB Windowing Step for 7x7
	{ 0x03B8, 0x00},
	{ 0x03B9, 0xB4},
	{ 0x03BA, 0x00},
	{ 0x03BB, 0xFF},
	{ 0x03BC, 0xFF},
	{ 0x03BD, 0xFF},
	{ 0x03BE, 0xFF},
	{ 0x03BF, 0xFF},
	{ 0x03C0, 0xFF},
	{ 0x03C1, 0x01},
	{ 0x03E0, 0x44},
	{ 0x03E1, 0x10},
	{ 0x03E2, 0x40},
	{ 0x03E3, 0x04},
	{ 0x03E4, 0x04},
	{ 0x03E5, 0x11},
	{ 0x03E6, 0x01},
	{ 0x03E7, 0x04},
	{ 0x03E8, 0x10},
	{ 0x03E9, 0x32},
	{ 0x03EA, 0x12},
	{ 0x03EB, 0x00},
	{ 0x03EC, 0x10},
	{ 0x03ED, 0x33},
	{ 0x03EE, 0x13},
	{ 0x03EF, 0x00},
	{ 0x03F0, 0x10},
	{ 0x03F1, 0x32},
	{ 0x03F2, 0x12},
	{ 0x03F3, 0x00},
	{ 0x03F4, 0x10},
	{ 0x03F5, 0x22},
	{ 0x03F6, 0x12},
	{ 0x03F7, 0x00},
	{ 0x03F8, 0x10},
	{ 0x03F9, 0x11},
	{ 0x03FA, 0x11},
	{ 0x03FB, 0x00},
	{ 0x03DC, 0x47},	// SubWinAE setting, min_dark_cnt=7, exit_dark_cnt=4
	{ 0x03DD, 0x5A},	// AEbl_th=70%
	{ 0x03DE, 0x41},	// enable SubWinAE, bottom 3x5, DM_tol=12.5%
	{ 0x03DF, 0x53},	// limit_ratio=65%
	{ 0x0420, 0x81},	// Digital Gain offset
	{ 0x0421, 0x00},
	{ 0x0422, 0x00},
	{ 0x0423, 0x81},
	{ 0x0430, 0x08},	// ABLC
	{ 0x0431, 0x40},
	{ 0x0432, 0x10},
	{ 0x0433, 0x04}, 	// 0A
	{ 0x0435, 0x08}, 	// 10
	{ 0x0450, 0xFF},	// Alpha
	{ 0x0451, 0xD0},
	{ 0x0452, 0xB8},
	{ 0x0453, 0x88},
	{ 0x0454, 0x00},
	{ 0x0458, 0x80},
	{ 0x0459, 0x03},
	{ 0x045A, 0x00},
	{ 0x045B, 0x80},
	{ 0x045C, 0x00},
	{ 0x045D, 0xC0},
	{ 0x0460, 0xFF},
	{ 0x0461, 0xFF},
	{ 0x0463, 0xFF},
	{ 0x0465, 0x02},
	{ 0x0466, 0x14},
	{ 0x0478, 0xFF},
	{ 0x047A, 0x00},	// ELOFFNRB
	{ 0x047B, 0x00},	// ELOFFNRY
	{ 0x047C, 0x04},
	{ 0x047D, 0x80},
	{ 0x047E, 0x04},
	{ 0x047F, 0xC0},
	{ 0x0480, 0x58},
	{ 0x0481, 0x08},
	{ 0x0482, 0x08},	// Sat outdoor
	{ 0x04B0, 0x48},	// Contrast
	{ 0x04B6, 0x38},
	{ 0x04B9, 0x10},
	{ 0x04B3, 0x18},
	{ 0x04B1, 0x86},
	{ 0x04B4, 0x20},
	{ 0x0540, 0x00},	//
	{ 0x0541, 0x7D},	// 60Hz Flicker
	{ 0x0542, 0x00},	//
	{ 0x0543, 0x96},	// 50Hz Flicker
	{ 0x0580, 0x04},  	//BLUR WEIGHT
	{ 0x0581, 0x0F},  	//BLUR WEIGHT ALPHA0
	{ 0x0582, 0x04},  	//BLUR WEIGHT OUTDOOR
	{ 0x05A1, 0x0A},  	//SHARPNESS STRENGTH
	{ 0x05A2, 0x21},  	//[6:4] SHARP_ORE_NEG, [1:0] FIL SELECTION
	{ 0x05A3, 0x84},  	//[7]EA_EN, [6] EE_CENTER, [3:0] EDGEDIV
	{ 0x05A4, 0x24},  	//SHARP_ORE
	{ 0x05A5, 0xFF},  	//ORE_MAX
	{ 0x05A6, 0x00},  	//ORE_LOW
	{ 0x05A7, 0x24},  	//OUGB_HOT
	{ 0x05A8, 0x24},  	//OUGB_COLD
	{ 0x05A9, 0x02},  	//EDGE RANGE
	{ 0x05B1, 0x24},  	//EDGE SUB
	{ 0x05B2, 0x0C},  	//[3] Y_ADJC, [2]CMODE
	{ 0x05B4, 0x1F},  	//FIX WEIGHT
	{ 0x05AE, 0x75},		//OAR START
	{ 0x05AF, 0x78},		//[7:5]OAR STEP, [4:0]OAR DECSTR
	{ 0x05B6, 0x00},  	//SHARPNESS STRENGTH ALPHA0
	{ 0x05B7, 0x10},  	//SHARP_ORE ALPHA0
	{ 0x05BF, 0x20},  	//EDGESUB ALPHA0
	{ 0x05CD, 0x00},  	//Raw sharpness strength outdoor
	{ 0x05C1, 0x06},  	//SHARPNESS STRENGTH OUTDOOR
	{ 0x05C2, 0x18},  	//SHARP ORE OUTDOOR
	{ 0x05C7, 0x0A},  	//TFBPI
	{ 0x05CC, 0x04},  	//Raw sharpness strength
	{ 0x05CD, 0x00},  	//Raw sharpness strength ALPHA0
	{ 0x05CE, 0x03},  	//Raw sharpness strength outdoor
	{ 0x05E4, 0x00},	//
	{ 0x05E5, 0x00},	//
	{ 0x05E6, 0x0F},	//
	{ 0x05E7, 0x05},	//
	{ 0x05E8, 0x01},	//
	{ 0x05E9, 0x00},	//
	{ 0x05EA, 0x30},	//
	{ 0x05EB, 0x03},	//
	{ 0x0660, 0x00},  //
	{ 0x0661, 0x16},  //
	{ 0x0662, 0xff},  //
	{ 0x0663, 0xf1},  //
	{ 0x0664, 0xff},  //
	{ 0x0665, 0xde},  //
	{ 0x0666, 0xff},  //
	{ 0x0667, 0xe7},  //
	{ 0x0668, 0x00},  //
	{ 0x0669, 0x35},  //
	{ 0x066a, 0xff},  //
	{ 0x066b, 0xf9},  //
	{ 0x066c, 0xff},  //
	{ 0x066d, 0xb7},  //
	{ 0x066e, 0x00},  //
	{ 0x066f, 0x27},  //
	{ 0x0670, 0xff},  //
	{ 0x0671, 0xf3},  //
	{ 0x0672, 0xff},  //
	{ 0x0673, 0xc5},  //
	{ 0x0674, 0xff},  //
	{ 0x0675, 0xee},  //
	{ 0x0676, 0x00},  //
	{ 0x0677, 0x16},  //
	{ 0x0678, 0x01},  //
	{ 0x0679, 0x80},  //
	{ 0x067a, 0x00},  //
	{ 0x067b, 0x85},  //
	{ 0x067c, 0xff},  //
	{ 0x067d, 0xe1},  //
	{ 0x067e, 0xff},  //
	{ 0x067f, 0xf5},  //
	{ 0x0680, 0xff},  //
	{ 0x0681, 0xb9},  //
	{ 0x0682, 0x00},  //
	{ 0x0683, 0x31},  //
	{ 0x0684, 0xff},  //
	{ 0x0685, 0xe6},  //
	{ 0x0686, 0xff},  //
	{ 0x0687, 0xd3},  //
	{ 0x0688, 0x00},  //
	{ 0x0689, 0x18},  //
	{ 0x068a, 0xff},  //
	{ 0x068b, 0xfa},  //
	{ 0x068c, 0xff},  //
	{ 0x068d, 0xd2},  //
	{ 0x068e, 0x00},  //
	{ 0x068f, 0x08},  //
	{ 0x0690, 0x00},  //
	{ 0x0691, 0x02},  //
	{ 0xAFD0, 0x03},	// Auto Flicker detection off
	{ 0xAFD3, 0x18},	//
	{ 0xAFD4, 0x04},	//
	{ 0xAFD5, 0x17},	// auto flicker samp time 3196
	{ 0xAFD6, 0x04},	//
	{ 0xAFD7, 0x68},	//
	{ 0xAFD8, 0x03},	//
	{ 0x0000, 0x01},	//
	{ 0x0100, 0x01},	//
	{ 0x0101, 0x01},	//
	{ 0x0005, 0x01},	// Turn on rolling shutter
};
static OV_FpsSetting_t*  GetOvFpsSetting(U8 Fps, OV_FpsSetting_t staOvFpsSetting[], U8 byArrayLen)
{
	U8 i;
	U8 Idx;
	ISP_MSG((" array size = %bd\n", byArrayLen));
	if(g_bIsHighSpeed)
	{
		Idx=2; //8fps for uxga, 10 fps for vga
	}
	else
	{
		Idx = 0;
	}
	for(i=0; i< byArrayLen; i++)
	{
		ISP_MSG((" fps i = %bd\n",i));
		if(staOvFpsSetting[i].byFps ==Fps)
		{
			Idx=i;
			break;
		}
	}
	ISP_MSG((" get fps idx = %bd\n",Idx));
	return &staOvFpsSetting[Idx];
}

static U16 MapSnrGlbGain2SnrRegSetting(U16 wGain)
{
	U16 sensorGain = 0;
	U8 k =0;
	U8 Digital_fractional_sensorGain_b5_b0;

	if(wGain>= 128)
	{
		sensorGain =3;
	}
	else if(wGain>= 64)
	{
		sensorGain =2;
	}
	else if(wGain>= 32)
	{
		sensorGain =1;
	}
	else
	{
		sensorGain =0;
	}

	if(sensorGain ==0)  //<=16 <byGain<32,  Again= 2^0 = 1,  sensor formula = 1*(1+n/16) = 1*(1+n*4/64)
	{
		Digital_fractional_sensorGain_b5_b0 =(wGain%16)*4; //ex: 17/16 = 1+1/16 = 1+1*4/16*4
	}
	if(sensorGain ==1)// 32<=byGain<64,    Again= 2^1 = 2,  sensor formula = 2*(1+n/32)= 2*(1+n*2/64)
	{
		Digital_fractional_sensorGain_b5_b0 =(wGain%32)*2;  //ex: 33/32 = 1+1/32 = 1+1*2/32*2
	}
	if(sensorGain ==2)// 64<=byGain<128  Again= 2^2 = 4,  sensor formula = 4*(1+n/64)= 4*(1+n/64)
	{
		Digital_fractional_sensorGain_b5_b0 =(wGain%64)*1;  //ex: 65/64 = 1+1/64 = 1+1*//*64
	}

	Digital_fractional_sensorGain = 0x40|Digital_fractional_sensorGain_b5_b0;
	return sensorGain;
}

float SnrRegSetting2SnrGlbGain(U16 wSnrAnaRegGain)
{
	float fAnalogGain;

	switch(wSnrAnaRegGain)
	{
	case 7:
		fAnalogGain = 128.0;
		break;
	case 6:
		fAnalogGain = 64.0;
		break;
	case 5:
		fAnalogGain = 32.0;
		break;
	case 4:
		fAnalogGain = 16.0;
		break;
	case 3:
		fAnalogGain = 8.0;
		break;
	case 2:
		fAnalogGain = 4.0;
		break;
	case 1:
		fAnalogGain = 2.0;
		break;
	case 0:
		fAnalogGain = 1.0;
		break;
	default:
		fAnalogGain = 1.0;
		break;
	}

	return fAnalogGain*(float)Digital_fractional_sensorGain/64.0;
}

//this function is also called during Probe stage to calculate the transfer_size/LWM/HWM
void GetSensorPclkHsync(U16 wSensorSPFormat,U8 byFps)
{
	OV_FpsSetting_t *pHM1055_FpsSetting;

	wSensorSPFormat =wSensorSPFormat;
	
	pHM1055_FpsSetting=GetOvFpsSetting(byFps, g_staHM1055FpsSetting, sizeof(g_staHM1055FpsSetting)/sizeof(OV_FpsSetting_t));
	g_wSensorHsyncWidth = pHM1055_FpsSetting->wExtraDummyPixel; // this for manual exposure
	g_dwPclk = pHM1055_FpsSetting->dwPixelClk;		// this for scale speed
}

void HM1055SetFormatFps(U16 SetFormat, U8 Fps)
{
	U16 wTemp;
	//U8 i;
	OV_FpsSetting_t *pHM1055_FpsSetting;
	SetFormat = SetFormat;

	pHM1055_FpsSetting=GetOvFpsSetting(Fps, g_staHM1055FpsSetting, sizeof(g_staHM1055FpsSetting)/sizeof(OV_FpsSetting_t));
	WriteSensorSettingWB(sizeof(gc_HM1055_Setting)/3, gc_HM1055_Setting);
	Write_SenReg(0x25, pHM1055_FpsSetting->byClkrc);
	wTemp = pHM1055_FpsSetting->wExtraDummyPixel;
	//g_wSensorHsyncWidth = pHM1055_FpsSetting->wExtraDummyPixel;
	//g_dwPclk = pHM1055_FpsSetting->dwPixelClk;		// this for scale speed
	GetSensorPclkHsync(SetFormat,Fps);
	
	if(Fps ==30)
	{
		g_wAEC_LineNumber = 731;	// this for AE insert dummy line algothrim  (scope measured) //?D-??I
		g_wAECExposureRowMax = 748;	// this for max exposure timev            (scope measured)  //3o��~?O-??I?C30fps?����13\ao?n����3��|hhsync|C??(��]��tVsyncao High(��??qao)?M Low duty(��?oaao))
	}
	if(Fps ==15)
	{
		Write_SenReg(0x10,0x01);
		Write_SenReg(0x11,0x02);
		Write_SenReg(0x0100,0x01);
		g_wAEC_LineNumber = 731;
		g_wAECExposureRowMax = 996;  //when 15fps, low duty increase, so guess that exposureRowMax will still increase with the time ratio.
	}
	if(Fps ==25)
	{
		Write_SenReg(0x10,0x00);
		Write_SenReg(0x11,0x9a);
		Write_SenReg(0x0100,0x01);
		g_wAEC_LineNumber = 731;
		g_wAECExposureRowMax = 896;  //when 15fps, low duty increase, so guess that exposureRowMax will still increase with the time ratio.
	}
	if(Fps ==20)
	{
		g_wAEC_LineNumber = 731;
		g_wAECExposureRowMax = 748;  //when 15fps, low duty increase, so guess that exposureRowMax will still increase with the time ratio.
	}
	if(Fps ==10)
	{
		Write_SenReg(0x10,0x00);
		Write_SenReg(0x11,0x02);
		Write_SenReg(0x0100,0x01);
		g_wAEC_LineNumber = 731;
		g_wAECExposureRowMax = 748;  //when 15fps, low duty increase, so guess that exposureRowMax will still increase with the time ratio.
	}
	g_wSensorWidthBefBLC = 1280;
	g_wSensorHeightBefBLC = 720;
#ifndef _MIPI_EXIST_
	SetBkWindowStart(1, 0);
#endif
	DBG_SENSOR(("Fps=%bu\n",Fps));
#if 0
	{
		for (i=0; i<6; i++)
		{
			g_awLensShadingCenter[i] = gc_wLSC_Center_Offset_HM1055[i];
		}
	}
#endif

	return;
}
void CfgHM1055ControlAttr(void)
{
	U8 i;
	g_bySensorSize = SENSOR_SIZE_HD720P;
	g_wSensorSPFormat = HD720P_FRM;
	{
		memcpy(g_asOvCTT, gc_HM1055_CTT,sizeof(gc_HM1055_CTT));
	}
	{
#ifdef SP_HM1055_IO18
		XBYTE[REG_TUNES18_28] = SV28_VOL_1V8|SV18_VOL_2V9;	// SV18 and SV28 voltage control register
#else

#if (_CHIP_ID_ & _RTS5840_)
	XBYTE[REG_TUNESVA] =  SVA_VOL_3V04;
	XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V70;
#elif (_CHIP_ID_ & _RTS5832_)
	XBYTE[REG_TUNESVA] =  SVA_VOL_3V00;
	XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V70;
#elif (_CHIP_ID_ & _RTS5829B_)
	XBYTE[REG_TUNESVA] =  SVA_VOL_3V00;
	XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V71;
#endif
#endif
	}
//	if(g_byFormatCfgExist==0) // format config not exist, use default setting.
	{
		g_aVideoFormat[0].byaVideoFrameTbl[0] = 8;
		g_aVideoFormat[0].byaVideoFrameTbl[1]=F_SEL_640_480;
		g_aVideoFormat[0].byaVideoFrameTbl[2]=F_SEL_176_144;
		g_aVideoFormat[0].byaVideoFrameTbl[3]=F_SEL_160_120;
		g_aVideoFormat[0].byaVideoFrameTbl[4]=F_SEL_352_288;
		g_aVideoFormat[0].byaVideoFrameTbl[5]=F_SEL_320_240;
		g_aVideoFormat[0].byaVideoFrameTbl[6]=F_SEL_800_600;
		g_aVideoFormat[0].byaVideoFrameTbl[7]=F_SEL_960_540;
		g_aVideoFormat[0].byaVideoFrameTbl[8]=F_SEL_1280_720;
		g_aVideoFormat[0].byaStillFrameTbl[0] = 5;
		g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_1280_720;
		g_aVideoFormat[0].byaStillFrameTbl[2]=F_SEL_160_120;
		g_aVideoFormat[0].byaStillFrameTbl[3]=F_SEL_320_240;
		g_aVideoFormat[0].byaStillFrameTbl[4]=F_SEL_640_480;
		g_aVideoFormat[0].byaStillFrameTbl[5]=F_SEL_1280_720;
		for(i=0; i<7; i++)
		{
			g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_15|FPS_30;
		}
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_800_600] =FPS_10;//|FPS_10;//FPS_9|FPS_8|FPS_5|FPS_1;                             //bit10 F_1280_720
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_960_540] =FPS_10;//|FPS_10;//FPS_9|FPS_8|FPS_5|FPS_1;                             //bit10 F_1280_720
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_1280_720] =FPS_10;//|FPS_10;//FPS_9|FPS_8|FPS_5|FPS_1;                             //bit10 F_1280_720
		g_aVideoFormat[0].byFormatType = FORMAT_TYPE_YUY2;
#ifdef _ENABLE_MJPEG_
		memcpy(&(g_aVideoFormat[1]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));
		for(i=7; i<12; i++) //bit0-6 VGA and smaller size, 30fps max
		{
			g_aVideoFormat[1].waFrameFpsBitmap[i] =FPS_15|FPS_30;//|FPS_1|FPS_3|FPS_5|FPS_10;
		}
		g_aVideoFormat[1].byFormatType = FORMAT_TYPE_MJPG;
#endif
#ifdef _ENABLE_M420_FMT_
		memcpy(&(g_aVideoFormat[2]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));
		g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_720] =FPS_11|FPS_5;
		g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_800] =FPS_11|FPS_5;
		g_aVideoFormat[2].byFormatType = FORMAT_TYPE_M420;
#endif
	}

	InitHM1055IspParams();
}
void SetHM1055IntegrationTime(U16 wEspline)
{
	U16 wFrameLenLines;
	U16 wExpMax = g_wAECExposureRowMax-2;
	if(wEspline > wExpMax)
	{
		wFrameLenLines = wEspline + 20;
	}
	else
	{
		wFrameLenLines = g_wAEC_LineNumber;
	}
	if (wFrameLenLines%2==1)
	{
		wFrameLenLines++;
	}
	Write_SenReg(0x0016, INT2CHAR(wEspline, 0));	// change Coarse integration time low
	Write_SenReg(0x0015, INT2CHAR(wEspline, 1));	// change Coarse integration time high
	Write_SenReg(0x0100, 0x01);	// AE function work latch.
}

void SetHM1055Gain(float fGain)
{
}

void SetHM1055ImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.
{
	bySnrImgDir =bySnrImgDir;
	bySnrImgDir ^= 0x01; 	// HM1055 output mirrored image at default, so need mirror the image for normal output
	if(bySnrImgDir & 0x01)
	{
#ifndef _MIPI_EXIST_
		SetBkWindowStart(0, 4);
#endif
	}
	else
	{
#ifndef _MIPI_EXIST_
		SetBkWindowStart(0, 1);
#endif
	}
//___________________________________a good timing to set sharpness to avoid to be overwrited
//__________________________________________________________________________________Edge
	XBYTE[0x8575]= 0x80; //ISP_SHARPNESS
	XBYTE[0x85E4]= 0x04; //ISP_EEH_SHARP_ARRAY0
	XBYTE[0x85E5]= 0x10; //ISP_EEH_SHARP_ARRAY1
	XBYTE[0x85E6]= 0x14; //ISP_EEH_SHARP_ARRAY2
	XBYTE[0x85E7]= 0x12; //ISP_EEH_SHARP_ARRAY3
	XBYTE[0x85E8]= 0x18; //ISP_EEH_SHARP_ARRAY4
	XBYTE[0x85E9]= 0x1C; //ISP_EEH_SHARP_ARRAY5
	XBYTE[0x85EA]= 0x18; //ISP_EEH_SHARP_ARRAY6
	XBYTE[0x85EB]= 0x1A; //ISP_EEH_SHARP_ARRAY7
	XBYTE[0x85EC]= 0x1E; //ISP_EEH_SHARP_ARRAY8
	XBYTE[0x85ED]= 0x1C; //ISP_EEH_SHARP_ARRAY9
	XBYTE[0x85EE]= 0x1E; //ISP_EEH_SHARP_ARRAY10
	XBYTE[0x85EF]= 0x22; //ISP_EEH_SHARP_ARRAY11
	XBYTE[0x85F0]= 0x0A; //ISP_BRIGHT_TRM_B1
	XBYTE[0x85F1]= 0x1F; //ISP_BRIGHT_TRM_B2
	XBYTE[0x85F2]= 0x06; //ISP_BRIGHT_TRM_THD0
	XBYTE[0x85F3]= 0x1C; //ISP_BRIGHT_TRM_THD1
	XBYTE[0x85F4]= 0x0F; //ISP_BRIGHT_TRM_K
	XBYTE[0x85F5]= 0x0E; //ISP_DARK_TRM_B1
	XBYTE[0x85F6]= 0x1F; //ISP_DARK_TRM_B2
	XBYTE[0x85F7]= 0x06; //ISP_DARK_TRM_THD0
	XBYTE[0x85F8]= 0x18; //ISP_DARK_TRM_THD1
	XBYTE[0x85F9]= 0x0F; //ISP_DARK_TRM_K
	XBYTE[0x85FA]= 0x10; //ISP_BRIGHT_RATE
	XBYTE[0x85FB]= 0x10; //ISP_DARK_RATE
//__________________________________________________________________________________Edge
//_____________________________________

}


void WaitFrameStart(void)
{
	if(g_byStartVideo)
	{
		XBYTE[0x8007/*ISP_INT_FLAG0*/] = 0x40/*ISP_FRAMESTART_INT*/;	// clear ISP Statictis interrupt
		WaitTimeOut(0x8007/*ISP_INT_FLAG0*/,0x40/* ISP_FRAMESTART_INT*/, 1, 30);
	}
}


void SetHM1055Exposuretime_Gain(float fExpTime, float fTotalGain)
{
	U16 wExposureRows_floor;
	U16  wGainRegSetting; //data
	float fSnrGlbGain;
	float fExposureRows;
	U16 wSetDummy;
	float fTemp;
	U16 wExposurePixels;

	wGainRegSetting = MapSnrGlbGain2SnrRegSetting((U16)(fTotalGain*16.0));
	fSnrGlbGain = SnrRegSetting2SnrGlbGain(wGainRegSetting);
	
	fExposureRows = fExpTime/g_fSensorRowTimes;	//unit : 1row  //____��Y((float)g_wSensorHsyncWidth*(float)10000)/(float)g_dwPclk;     // unit microsecond
	wExposureRows_floor = (U16)(fExposureRows);
	fTemp = fExpTime-((float)wExposureRows_floor*(g_fSensorRowTimes)); //fine integration time;unit:  us
	wExposurePixels =(U16)(fTemp * (float)(g_dwPclk/10000)) ; //unit: pixels

	if(fabs(g_fCurExpTime-fExpTime) > 0.0001)
	{
		wSetDummy = g_wAFRInsertDummylines + g_wAEC_LineNumber;
		if (wSetDummy%2 == 1)
		{
			wSetDummy++;
		}
		Write_SenReg(0x016, INT2CHAR(wExposureRows_floor, 0));	// change exposure value low
		Write_SenReg(0x015, INT2CHAR(wExposureRows_floor, 1));	// change exposure value high
		Write_SenReg(0x001D, Digital_fractional_sensorGain);
		Write_SenReg(0x0018, wGainRegSetting);
		Write_SenReg(0x0100, 0x01);
		WaitFrameSync(ISP_INT1, ISP_DATA_START_INT);		

		SetISPAEGain(fTotalGain, fSnrGlbGain, 1);
		
		g_fCurExpTime = fExpTime;	// back up exposure rows
	}
	else
	{
		Write_SenReg(0x0018, wGainRegSetting);
		Write_SenReg(0x001D, Digital_fractional_sensorGain);
		Write_SenReg(0x0100, 0x01);

		SetISPAEGain(fTotalGain, fSnrGlbGain, 1);
	}

	WaitFrameStart();
}

void InitHM1055IspParams()
{
	// AWB initial gain
	//use D50 R/G/B gain as AWB initial value
	g_wAWBRGain_Last= 0x100;
	g_wAWBGGain_Last= 0x108;
	g_wAWBBGain_Last= 0x118;
	g_wProjectGR = g_wAWBRGain_Last;
	g_wProjectGB = g_wAWBBGain_Last;

	g_bySaturation_Def = 64;
	g_byContrast_Def =32;
	g_wDynamicISPEn = DYNAMIC_DarkMode_CCM_EN|DYNAMIC_SHARPPARAM_EN|
		DYNAMIC_SHARPNESS_EN|DYNAMIC_GAMMA_EN;//|DYNAMIC_CCM_CT_EN;


	//U8 i;

}
void SetHM1055DynamicISP(U8 byAEC_Gain)
{
////_______________Dark mode
	if  ( (g_wDynamicISPEn&DYNAMIC_DarkMode_CCM_EN) == DYNAMIC_DarkMode_CCM_EN)
	{
		if(CCM_frame_count == 5)
		{
			CCM_frame_count = 0;
			if((byAEC_Gain ==g_wAECGlobalgainmax)&&(g_byAEMeanValue <= 20 ) &&(XBYTE[0x8011]==0x05))	//only for HD720
			{
				if(g_byCCM2State != 0x03)
				{
					XBYTE[0x8361] = 0; //0x8361
					XBYTE[0x8369] = 0; //0x8369
					XBYTE[0x8367] = 0; //0x8367
					XBYTE[0x8375] = 0x01;
					g_byCCM2State = 0x03;  //0x03 = Dark_CCM, but here, use define will result error.
				}
			}
			else// if(wColorTempature > 2900)
			{
				if(g_byCCM2State == 0x03) //only do once, for DarkMode come back condition.
				{
					XBYTE[0x8361] = 1;
					XBYTE[0x8369] = 1;
					XBYTE[0x8367] = 1;
					XBYTE[0x8375] = 0x01;
					g_byCCM2State = 0x00;  //0x00 = D65_CCM,
				}
			}
		}
		else
		{
			CCM_frame_count++;
		}
	}
///_________________________

}
void SetHM1055DynamicISP_AWB(U16 wColorTempature)
{
	wColorTempature =wColorTempature;


}
#endif
