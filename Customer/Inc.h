#ifndef _INC_H_
#define _INC_H_

// Misc header files
#include "pc_cam.h"
#include "device.h"
#include "camutil.h"
#include "camUvc.h"
#include "CamCtl.h"
#include "CamSensor.h"
#include "CamI2C.h"
#include "Global_vars.h"
#include "ISP_vars.h"
#include "camusb.h"

#include "CamSensorUtil.h"
#include "camvdcfg.h"
#include "CamProcess.h"
#include "CamIsp.h"
#include "CamRtkExtIsp.h"
#include "CamReg2.h"

#include "ISP_TuningTable.h"
#include "ISP_lib.h"
#include "CUSXU.h"


// sensor header files
#include "HM1055.h"
#include "OV7725.h"
#include "OV7740.h"
#include "OV7670.h"
#include "OV7690.h"
#include "OV9710.h"
#include "OV9715.h"
#include "OV7675.h"
#include "OV7690.h"
#include "OV9663.h"
#include "OV2650.h"
#include "OV2710.h"
#include "OV2680.h"
#include "OV2722.h"
#include "OV5642.h"
#include "OV5640.h"
#include "OV3642.h"
#include "OV3640.h"
#include"mi360.h"
#include"mi1320.h"
#include"mi1330.h"
#include"mi2020.h"
#include"Sam6AA.h"
#include "SIV100B.h"
#include "SIV120B.h"
#include "SIV120D.h"
#include "SIM120C.h"
#include "SIW021A.h"
#include "ST171.h"
#include "SID130B.h"
#include "GC0307.h"
#include "GC1036.h"
#include "GC1136.h"
#include "YACBAA0S.h"
#include "YACBAC1S.h"
#include "YACC5A2S.h"
#include "OV9726.h"
#include "OV9728.h"
#include "OV9724.h"
#include "S5K6A1.h"
#include "S5K8AA.h"
#include "YACC611.h"
#include "mi1040.h"
#include "YACC6A1S_RAW.h"
#include "IMX119PQH5.h"
#include "IMX188PQ.h"
#include "IMX132PQ.h"
#include "IMX208PQH5.h"
#include "IMX189PSH5.h"
#include "IMX076LQZC.h"
#include "RS0509.h"
#include "RS0551C.h"
#include "OV2720.h"
#include "YACY9A1.h"
#include "YACD6A1C.h"
#include "T4K71.h"
#include "YACY6A1C9SBC.h"
#include "JXH22.h"
#endif // _INC_H_
