#ifndef _GC0307_H_
#define _GC0307_H_

#define GC0307_AE_TARGET 0x50

void GC0307SetFormatFps(U8 SetFormat, U8 Fps);
void CfgGC0307ControlAttr(void);
void SetGC0307Brightness(S16 swSetValue);
void	SetGC0307Contrast(U16 wSetValue);
void	SetGC0307Saturation(U16 wSetValue);
void	SetGC0307Hue(S16 swSetValue);
void	SetGC0307Sharpness(U8 bySetValue);
void SetGC0307Effect(U8 byEffect);
void SetGC0307ImgDir(U8 bySnrImgDir);
//void SetGC0307WindowCrop(U16 wXStart,U16 wXEnd,U16 wYStart,U16 wYEnd );
void SetGC0307OutputDim(U16 wWidth,U16 wHeight);
void SetGC0307PwrLineFreq(U8 byFPS, U8 byLightFrq);
void SetGC0307WBTemp(U16 wRgain, U16 wGgain, U16 wBgain);
void SetGC0307WBTempAuto(U8 bySetValue);
void SetGC0307BackLightComp(U8 bySetValue);
U16 GetGC0307AEGain();
extern code OV_CTT_t gc_GC0307_CTT[3];
void SetGC0307IntegrationTimeAuto(U8 bySetValue);
void SetGC0307IntegrationTime(U16 wEspline);
void SetGC0307Gain(float fGain);
#endif // _GC0307_H_

