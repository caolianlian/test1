#include "Inc.h"

#ifdef RTS58XX_SP_OV9710
OV_CTT_t code gc_OV9710_CTT[3] =
{
	{3000,0x132,0x100,0x181},
	{4800,0x140,0x100,0x162},
	{6500,0x150,0x100,0x127},
};

code OV_FpsSetting_t  g_staOv9710FpsSetting[]=
{
	// FPS ExtraDummyPixel clkrc 	   pclk
	{1,		(1691),	(29), 	  140000},
	{3,		(1691),	(9),  	  4200000},	// 2.999
	{5,		(1691),	(5),  	  7000000},	// 4.999
	//	{8,		(2114),	(2), 		14000000},
	//	{9,		(1879),	(2), 		14000000},
	{10,		(1691),	(2), 		14000000},	// 9.999
	{15,		(1691),	(1), 		21000000},	// 14.999
	{20,		(2537),	(0), 		42000000},	// 19.995
	{25,		(2029),	(0),		42000000},	// 25.001
	{30,		(1691),	(0),		42000000},	//29.998 // vista/xp capture ok for 1 hour.
};

code t_RegSettingBB gc_OV9710_Setting[] =
{
	// reset
//	{0x12, 0x80},
	//---------------------------
	// core settings
	//---------------------------
	{0x1E, 0x07},
	{0x5F, 0x18},
	{0x69, 0x04},
	{0x65, 0x2A},
	{0x68, 0x0A},
	{0x39, 0x28},
	{0x4D, 0x90},
	{0xC1, 0x80},
	//---------------------------
	//DSP
	//---------------------------
	{0x96, 0xF1},
	{0xBC, 0x68},
	//---------------------------
	// resolution and format
	//---------------------------
	{0x12, 0x00},
	{0x3B, 0x00},
	{0x97, 0x80},
	//---------------------------
	// place generated setting here
	//---------------------------
	// sensor output window: 1296:810, Start: (303,6)

	{0x17, 0x25},//0x23},// 0x25}, for BLC _JQG_20100311_	delete BLC hemonel_20110622
	{0x18, 0xA2},//0xA4},// 0xA2}, for BLC _JQG_20100311_	delete BLC hemonel_20110622
	{0x19,  0x01},//0x00},// 0x01}, for BLC _JQG_20100311_	delete BLC hemonel_20110622
	{0x1A, 0xCA},//0xCB},// 0xCA}, for BLC _JQG_20100311_	delete BLC hemonel_20110622
	{0x03, 0x00},	// 0x02,  0x0A	// hemonel 2010-02-26: modify from 0x0A to 0x02 for image flip and mirror issue
	{0x32, 0x07},


	/*
		//1304*816
		{0x17,0x24},
		{0x18, 0xA3},
		{0x19, 0x00},
		{0x1A, 0xCC},
		{0x03, 0x00},
		{0x32, 0x07},
	*/
	// DSP control for pre_win output offset
	{0x98, 0x00},
	{0x99, 0x00},
	{0x9A, 0x00},
	// DSP output window size: 1280*801
	{0x57, 0x01},		// vertical + 1; 0x00, defualt,
	{0x58, 0xC9},// 0xC8}, for BLC _JQG_20100311_
	{0x59, 0xA2},// 0xA0}, for BLC _JQG_20100311_
	//
	{0x4C, 0x13},
	{0x4B, 0x36},
	// REND = 828
	{0x3D, 0x3C},
	{0x3E, 0x03},
	{0xBD, 0xA0},
	{0xBE, 0xC8},

	// AWB
	{0x38, 0x00},	// manual white balance control
	{0x01, 0x40},	// blue gain
	{0x02, 0x40},	// red gain
	{0x05, 0x40},	// green gain

	// Lens Correction


	// YAVG

	// 16-zone exposure Windows weight
	{0x4E, 0x55},
	{0x4F, 0x55},
	{0x50, 0x55},
	{0x51, 0x55},

	{0x24, 0x55},		// WPT, exposure windows high
	{0x25, 0x40},		//BPT, exposure windows low
	{0x26, 0xA1},		// VPT, expsoure windows fast adjust

	// clock
	{0x5C, 0x59},
	{0x5D, 0x10},		// hemonel 2008-08-18: adust 3x	// 0x00
	{0x11, 0x01},
	// horizontal Tp counter End point = 1688
	{0x2A, 0x98},
	{0x2B, 0x06},
	// dummy lines = 0
	{0x2D, 0x00},
	{0x2E, 0x00},

	// General
	{0x13, 0x00},		// disableAGC/AEC, 0x85: enable AEC fast,enable AGC/AEC
	{0x14, 0x40},
};

static OV_FpsSetting_t*  GetOvFpsSetting(U8 Fps, OV_FpsSetting_t staOvFpsSetting[], U8 byArrayLen)
{
	U8 i;
	U8 Idx;

	ISP_MSG((" array size = %bd\n", byArrayLen));

	if(g_bIsHighSpeed)
	{
		Idx=2; //8fps for uxga, 10 fps for vga
	}
	else
	{
		Idx = 0;
	}

	for(i=0; i< byArrayLen; i++)
	{
		ISP_MSG((" fps i = %bd\n",i));
		if(staOvFpsSetting[i].byFps ==Fps)
		{
			Idx=i;
			break;
		}
	}
	ISP_MSG((" get fps idx = %bd\n",Idx));
	return &staOvFpsSetting[Idx];
}

void GetSensorPclkHsync(U16 wSensorSPFormat, U8 byFps)
{
	OV_FpsSetting_t *pOv9710_FpsSetting;
	wSensorSPFormat = wSensorSPFormat;
	pOv9710_FpsSetting=GetOvFpsSetting(byFps, g_staOv9710FpsSetting, sizeof(g_staOv9710FpsSetting)/sizeof(OV_FpsSetting_t));
	g_wSensorHsyncWidth = pOv9710_FpsSetting->wExtraDummyPixel;
	g_dwPclk = pOv9710_FpsSetting->dwPixelClk;	
}

void OV9710SetFormatFps(U8 SetFormat, U8 Fps)
{
	U16 wTemp;
	OV_FpsSetting_t *pOv9710_FpsSetting;

	// hemonel 2010-04-13: for delete complier warning
	SetFormat = SetFormat;

	pOv9710_FpsSetting=GetOvFpsSetting(Fps, g_staOv9710FpsSetting, sizeof(g_staOv9710FpsSetting)/sizeof(OV_FpsSetting_t));
	// 1) change hclk if necessary
	// ov9710 hclk at 24M ok

	// initial all register setting
	WriteSensorSettingBB(sizeof(gc_OV9710_Setting)>>1, gc_OV9710_Setting);

	// 2) write sensor register
	Write_SenReg(0x11, pOv9710_FpsSetting->byClkrc);
	wTemp = pOv9710_FpsSetting->wExtraDummyPixel;
	Write_SenReg(0x2A, INT2CHAR(wTemp, 0));	//Write dummy pixel LSB
	Write_SenReg(0x2B, INT2CHAR(wTemp, 1));	//Write dummy pixel MSB

	// 3) update variable for AE
	//g_wSensorHsyncWidth = pOv9710_FpsSetting->wExtraDummyPixel;
	//g_dwPclk = pOv9710_FpsSetting->dwPixelClk;
	GetSensorPclkHsync(HD800P_FRM,Fps);
	g_wAECExposureRowMax = 826;
//	g_wAEC_LineNumber = 826;		// ov9710 not used frame line length, this variable for insert dummy lines to frame line length

	g_wSensorWidthBefBLC = 1280;
	g_wSensorHeightBefBLC = 800;
}

void CfgOV9710ControlAttr(void)
{
	U8 i;

	g_bySensorSize = SENSOR_SIZE_HD800P;
	g_wSensorSPFormat =HD800P_FRM;

	{
		memcpy(g_asOvCTT,gc_OV9710_CTT,sizeof(gc_OV9710_CTT));
	}

	{
#if (_CHIP_ID_ & _RTS5840_)
	XBYTE[REG_TUNESVA] =  SVA_VOL_3V25;
	XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V79;
#elif (_CHIP_ID_ & _RTS5832_)
	XBYTE[REG_TUNESVA] =  SVA_VOL_3V19;
	XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V85;
#elif (_CHIP_ID_ & _RTS5829B_)
	XBYTE[REG_TUNESVA] =  SVA_VOL_3V28;
	XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V80;
#endif
	}

//	if(g_byFormatCfgExist==0) // format config not exist, use default setting.
	{
		//====== resolution  setting ===========
		// -- preview ---
		g_aVideoFormat[0].byaVideoFrameTbl[0] = 8;		// resolution number
		g_aVideoFormat[0].byaVideoFrameTbl[1]=F_SEL_640_480;
		g_aVideoFormat[0].byaVideoFrameTbl[2]=F_SEL_160_120;
		g_aVideoFormat[0].byaVideoFrameTbl[3]=F_SEL_176_144;
		g_aVideoFormat[0].byaVideoFrameTbl[4]=F_SEL_320_240;
		g_aVideoFormat[0].byaVideoFrameTbl[5]=F_SEL_352_288;
		g_aVideoFormat[0].byaVideoFrameTbl[6]=F_SEL_640_400;
		g_aVideoFormat[0].byaVideoFrameTbl[7]=F_SEL_1280_720;
		g_aVideoFormat[0].byaVideoFrameTbl[8]=F_SEL_1280_800;
		//--still image--
		g_aVideoFormat[0].byaStillFrameTbl[0] = 4;	// resolution number
		g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_1280_800;
		g_aVideoFormat[0].byaStillFrameTbl[2]=F_SEL_160_120;
		g_aVideoFormat[0].byaStillFrameTbl[3]=F_SEL_320_240;
		g_aVideoFormat[0].byaStillFrameTbl[4]=F_SEL_640_480;

		//====== fps  setting ===========
		for(i=0; i<7; i++)
		{
			/*640_480,
			160_120,
			176_144,
			320_200,
			320_240,
			352_288,
			640_400*/
			g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_15|FPS_20|FPS_25|FPS_30;
		}
		for(i=7; i<12; i++)
		{
			/*800_600,
			960_720,
			1024_768,
			1280_720,
			1280_800,*/
			g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_10|FPS_5;
		}

		//====== format type  setting ===========
		g_aVideoFormat[0].byFormatType = FORMAT_TYPE_YUY2;
#ifdef _ENABLE_MJPEG_
		// copy YUY2 setting to MJPEG setting
		memcpy(&g_aVideoFormat[1], &g_aVideoFormat[0], sizeof(g_aVideoFormat[0]));

		// modify MJPEG FPS setting
		for(i=7; i<12; i++)
		{
			g_aVideoFormat[1].waFrameFpsBitmap[i] = (FPS_15|FPS_20|FPS_25|FPS_30);
		}

		// modify MJPEG format type
		g_aVideoFormat[1].byFormatType = FORMAT_TYPE_MJPG;
#endif

#ifdef _ENABLE_M420_FMT_
		// copy YUY2 setting to M420 setting
		memcpy(&(g_aVideoFormat[2]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

		// modify M420 FPS setting
		g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_800_600] =FPS_15|FPS_10|FPS_5;//|FPS_1;             //bit07 F_800_600
		g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_960_720] =FPS_15|FPS_10|FPS_5;//|FPS_5|FPS_3|FPS_1;                    //bit08 F_960_720
		g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1024_768] =FPS_15|FPS_10|FPS_5;//|FPS_1;                    //bit09 F_1024_768
		g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_720] =FPS_11|FPS_5;//FPS_9|FPS_8|FPS_1;    //bit10 F_1280_720
		g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_800] =FPS_11|FPS_5;

		// modify M420 format type
		g_aVideoFormat[2].byFormatType = FORMAT_TYPE_M420;
#endif

	}

	{
		//Brightness - BrightnessItem

		//Contrast - ContrastItem

		//Saturation - SaturationItem

		//Hue - 	HueItem

		//Sharpness - SharpnessItem
		//	SharpnessItem.Min = 0;
		//	SharpnessItem.Max = 8;
		//	SharpnessItem.Res = 1;
		//	SharpnessItem.Def = 3;
		//	SharpnessItem.Last  =SharpnessItem.Def;

		// gamma - 	GammaItem

		// gain - GainItem

		// whitebalance - WhiteBalanceTempItem

		// backlight - BackLightCompItem

		// powerline -PwrLineFreqItem

		// exposure time -	ExposureTimeAbsolutItem

		// low light - LowLightCompItem
	}

	InitOV9710IspParams();
}

void SetOV9710IntegrationTime(U16 wEspline)
{
	U16 wDummyline = 0;
	U16 wExpMax = g_wAECExposureRowMax-2;

	if(wEspline > wExpMax)
	{
		wDummyline = wEspline - wExpMax;
		wEspline = wExpMax;
	}

	// write exposure time and dummy line to register
	//Write_SenReg_Mask(0x13, 0x00, 0x01);
	Write_SenReg(0x10, INT2CHAR(wEspline, 0));
	Write_SenReg(0x16, INT2CHAR(wEspline, 1));
	Write_SenReg(0x2D, INT2CHAR(wDummyline, 0));
	Write_SenReg(0x2E, INT2CHAR(wDummyline, 1));
}

void SetOV9710Gain(float fGain)
{
}

void SetOV9710ImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.
{
	SetBkWindowStart(0, 1);
	Write_SenReg_Mask(0x04, ((bySnrImgDir&0x01)<<7)|((bySnrImgDir&0x02)<<5), 0xC0);
}


static U16 MapSnrGlbGain2SnrRegSetting(U16 wGain)
{
	U16  sensorGain=0;
	U8  i;
	//QDBG(("byGain = %bd-> ",byGain));

	for(i=0; i<4; i++)
	{
		if(wGain >= 32)
		{
			wGain >>= 1;
			sensorGain |=  (0x01<<(i+4));
		}
		else
		{
			sensorGain |= (wGain-16);
			break;
		}
	}

	//QDBG(("sensorGain = %d    ",sensorGain));
	return sensorGain;
}

float SnrRegSetting2SnrGlbGain(U16 wSnrRegGain)
{
	U16 wGain;
	U8 i;

	wGain = (wSnrRegGain&0x0f) + 16;
	wSnrRegGain >>= 4;

	for(i = 0; i < 4; i++)
	{
		if((wSnrRegGain&1) == 1)
		{
			wGain <<= 1;
		}

		wSnrRegGain >>= 1;
	}

	return (float)wGain/16.0;
}

void SetOV9710Exposuretime_Gain(float fExpTime, float fTotalGain)
{
	U16 wExposureRows;
	U16  wGainRegSetting; //data
	float fSnrGlbGain;
	float fExposureRows;
	U8  byOV_FirstInsertDummy;
	static U16 swAFRInsertDummyLines = 0;

	wGainRegSetting=MapSnrGlbGain2SnrRegSetting((U16)(fTotalGain*16.0));
	fSnrGlbGain = SnrRegSetting2SnrGlbGain(wGainRegSetting);

	fExposureRows = fExpTime/g_fSensorRowTimes;	//unit : 1row
	wExposureRows =(U16)(fExposureRows+0.5);

	// fix 60Hz highlight mode bug
	if (wExposureRows <= g_wAFRInsertDummylines)
	{
		g_wAFRInsertDummylines = 0;
	}
	else
	{
		wExposureRows = wExposureRows - g_wAFRInsertDummylines;
	}

	if(fabs(g_fCurExpTime-fExpTime) > 0.0001)
	{
		if((swAFRInsertDummyLines==0)&&(g_wAFRInsertDummylines!=0))
		{
			byOV_FirstInsertDummy = 1;
		}
		else
		{
			byOV_FirstInsertDummy = 0;
		}
		swAFRInsertDummyLines = g_wAFRInsertDummylines;

		Write_SenReg(0x10, INT2CHAR(wExposureRows, 0));	// change exposure value low
		Write_SenReg(0x16, INT2CHAR(wExposureRows, 1));	// change exposure value high
		if(byOV_FirstInsertDummy==1)
		{
			Write_SenReg(0x2D, INT2CHAR(g_wAFRInsertDummylines, 0));
			Write_SenReg(0x2E, INT2CHAR(g_wAFRInsertDummylines, 1));
		}

		// delay one frame for sync
		WaitFrameSync(ISP_INT1, ISP_DATA_START_INT);

		if(byOV_FirstInsertDummy==0)
		{
			Write_SenReg(0x2D, INT2CHAR(g_wAFRInsertDummylines, 0));
			Write_SenReg(0x2E, INT2CHAR(g_wAFRInsertDummylines, 1));
		}

		Write_SenReg(0x00, wGainRegSetting);

		SetISPAEGain(fTotalGain, fSnrGlbGain, 1);

		g_fCurExpTime = fExpTime;	// back up exposure rows
	}
	else
	{
		Write_SenReg(0x00, wGainRegSetting);

		SetISPAEGain(fTotalGain, fSnrGlbGain, 1);
	}


	return;
}

void OV9710_POR()
{
#ifdef _MIPI_EXIST_
	Init_MIPI();
#else
	Init_CCS();
#endif
	ENTER_SENSOR_RESET();
	// power on
	SensorPowerControl(SWITCH_ON,EN_DELAYTIME);
	uDelay(1);
	// Initialize I2C Controller Timing
	LEAVE_SENSOR_RESET();
	// clock select 24M
	if( g_bySSCEnable== TRUE)
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_SSC_24M);
	}
	else
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_96M|CCS_CLK_DIVIDER_4);
	}

	EnableSensorHCLK();
	g_bySensorIsOpen = SENSOR_OPEN;
	WaitTimeOut_Delay(1);  //OV9726 spec request 8192clk
}

void InitOV9710IspParams()
{
	// D50 R/G/B gain
	g_wAWBRGain_Last= 0x17C;
	g_wAWBGGain_Last= 0x100;
	g_wAWBBGain_Last= 0x226;
	g_wProjectGR = g_wAWBRGain_Last;
	g_wProjectGB = g_wAWBBGain_Last;

	// Special ISP
//	g_bySaturation_Def =64;
//	g_byContrast_Def = 32;

	g_wDynamicISPEn = DYNAMIC_LSC_EN | DYNAMIC_SHARPPARAM_EN 	|
		DYNAMIC_GAMMA_EN | DYNAMIC_CCM_CT_EN | DYNAMIC_SHARPNESS_EN;
}

void SetOV9710DynamicISP(U8 byAEC_Gain)
{
	byAEC_Gain = byAEC_Gain;
}

void SetOV9710DynamicISP_AWB(U16 wColorTempature)
{
	wColorTempature = wColorTempature;
}
#endif
