#include "Inc.h"

#ifdef RTS58XX_SP_OV2722
/*
*********************************************************************************************************
*                                 manual white balance control parameter
*
*	1st column: color-temperature. color-temperature must be from little to big
*	2nd column: Rgain
*	3rd column: Ggain
*	4th column: Bgain
*
*	this array used for manual white balance control, these data are get from Judge II light box
*
*********************************************************************************************************
*/

OV_CTT_t code gc_OV2722_CTT[6] =
{
	{2800,0x0130,0x100,0x220},
	{3700,0x0160,0x100,0x1c0},
	{4000,0x1b0,0x100,0x01d0},
	{4600,0x1a4,0x100,0x0198},
	{5000,0x1c0,0x100,0x0178},
	{6500,0x1f0,0x100,0x120},

};

/*
*********************************************************************************************************
*                                 	Sensor FPS Parameter setting
*
*	1st column: FPS. FPS order must be from little to big
*	2nd column: Dummy Pixel. sensor extra dummy pixel or frame lines
*	3rd column: PLL.	adjust sensor PLL
*	4th column: PCLK. sensor one-pixel output clock frequency, unit of Hz. If YUYV data output, this clock is the half of PCLK
*********************************************************************************************************
*/

#ifdef _MIPI_EXIST_

code OV_FpsSetting_t  g_staOV2722FHDFpsSetting[]=
{
	// FPS ExtraDummyPixel clkrc 	   pclk
	{5,		(3214),			(0xf1), 		17976000},
	{8,		(4017),			(0xd1), 		35952000},
	{9,		(3696),			(0xd1), 		35952000},
	{10,		(3214),			(0xd1), 		35952000},
	{15,		(2140),			(0xd1), 		35952000},
	{20,		(3214),			(0xa1), 		71904000},
	{25,		(2572),			(0xa1),		71904000},
	{30,		(2140),			(0xa1),		71904000},
};


#else

code OV_FpsSetting_t  g_staOV2722FHDFpsSetting[]=
{
	// FPS ExtraDummyPixel clkrc 	   pclk
	{5,		(3210),			(0x71), 		17976000},
	{8,		(4018),			(0x51), 		35952000},
	{9,		(3696),			(0x51), 		35952000},
	{10,		(3210),			(0x51), 		35952000},
	{15,		(2140),			(0x51), 		35952000},
	{20,		(3214),			(0x21), 		71904000},
	{25,		(2572),			(0x21),		71904000},
	{30,		(2140),			(0x21),		71904000},
};

#endif


/*
*********************************************************************************************************
*                                 	Sensor Register setting
* param:
*	the 1st column: sensor register address
*	the 2nd column: sensor register value
*********************************************************************************************************
*/
#ifdef _MIPI_EXIST_

// 1080P MIPI 1 LANE 30 fps

t_RegSettingWB code gc_OV2722_MIPI_FHD_Setting[] =
{
	{0x0103, 0x01},
	{0x3718, 0x10},
	{0x3702, 0x24},
	{0x373a, 0x60},
	{0x3715, 0x01},
	{0x3703, 0x2e},
	{0x3705, 0x10},
	{0x3730, 0x30},
	{0x3704, 0x62},
	{0x3f06, 0x3a},
	{0x371c, 0x00},
	{0x371d, 0xc4},
	{0x371e, 0x01},
	{0x371f, 0x0d},
	{0x3708, 0x61},
	{0x3709, 0x12},
	{0x3800, 0x00},
	{0x3801, 0x08},  // 8
	{0x3802, 0x00},
	{0x3803, 0x02},	// 2
	{0x3804, 0x07},
	{0x3805, 0x9b},	// 1947
	{0x3806, 0x04},
	{0x3807, 0x45},   //1093
	{0x3808, 0x07},
	{0x3809, 0x84}, //1920
	{0x380a, 0x04},
	{0x380b, 0x39}, //1080
	{0x380c, 0x08},
	{0x380d, 0x5c},//2140
	{0x380e, 0x04},
	{0x380f, 0x60}, //1120
	{0x3810, 0x00},
	{0x3811, 0x09}, //9
	{0x3812, 0x00},
	{0x3813, 0x06}, // 6
	{0x3820, 0x80},
	{0x3821, 0x06},
	{0x3814, 0x11},
	{0x3815, 0x11},
	{0x3612, 0x4b},
	{0x3618, 0x04},
	{0x3a08, 0x01},
	{0x3a09, 0x50},
	{0x3a0a, 0x01},
	{0x3a0b, 0x18},
	{0x3a0d, 0x03},
	{0x3a0e, 0x03},
	{0x4520, 0x00},
	{0x4837, 0x1b},
	{0x3000, 0xff},
	{0x3001, 0xff},
	{0x3002, 0xf0},
	{0x3600, 0x08},
	{0x3621, 0xc0},
	{0x3632, 0xe3},
	{0x3633, 0x23},
	{0x3634, 0x24},
	{0x3f01, 0x0c},
	{0x5001, 0xc1}, // disable BLC
	{0x3614, 0xf0},
	{0x3630, 0x2d},
	{0x370b, 0x62},
	{0x3706, 0x61},
	{0x4000, 0x02},
	{0x4002, 0xc5},
	{0x4005, 0x08},
	{0x404f, 0x84},
	{0x4051, 0x00},
	{0x5000, 0xCF}, // disable awb, lsc
	{0x3a18, 0x00},
	{0x3a19, 0x80},
	{0x3503, 0x07}, //disable sensor AE, AG
	{0x4521, 0x00},
	{0x5183, 0xb0},
	{0x5184, 0xb0},
	{0x5185, 0xb0},
	{0x370c, 0x0c},
	{0x3035, 0x00},
	{0x3036, 0x1e},
	{0x3037, 0xa1},
	{0x303e, 0x19},
	{0x3038, 0x06},
	{0x3018, 0x04},
	{0x3000, 0x00},
	{0x3001, 0x00},
	{0x3002, 0x00},
	{0x3a0f, 0x40},
	{0x3a10, 0x38},
	{0x3a1b, 0x48},
	{0x3a1e, 0x30},
	{0x3a11, 0x90},
	{0x3a1f, 0x10},
	{0x3011, 0x22},
	//{0x5040, 0x80}, // test pattern
	{0x0100, 0x01},
};

#else

t_RegSettingWB code gc_OV2722_FHD_Setting[] =
{
	{0x0103, 0x01},
	{0x3718, 0x10},
	{0x3702, 0x24},
	{0x373a, 0x60},
	{0x3715, 0x01},
	{0x3703, 0x2e},
	{0x3705, 0x10},
	{0x3730, 0x30},
	{0x3704, 0x62},
	{0x3f06, 0x3a},
	{0x371c, 0x00},
	{0x371d, 0xc4},
	{0x371e, 0x01},
	{0x371f, 0x0d},
	{0x3708, 0x61},
	{0x3709, 0x12},
	//resolution_1920x1080
	{0x3800, 0x00},
	{0x3801, 0x08}, //8
	{0x3802, 0x00},
	{0x3803, 0x02}, // 2
	{0x3804, 0x07},
	{0x3805, 0x9b}, // 1947
	{0x3806, 0x04},
	{0x3807, 0x45},  // 1093
	{0x3808, 0x07},
	{0x3809, 0x82},	// 1962
	{0x380a, 0x04},
	{0x380b, 0x3A},	//1082
	{0x380c, 0x08},
	{0x380d, 0x5C},// 2140
	{0x380e, 0x04},
	{0x380f, 0x60},//1120
	{0x3810, 0x00},
	{0x3811, 0x09}, // 9
	{0x3812, 0x00},
	{0x3813, 0x06}, //6
	{0x3820, 0x80},
	{0x3821, 0x06},
	{0x3814, 0x11},
	{0x3815, 0x11},
	{0x3612, 0x4b},
	{0x3618, 0x04},
	{0x3a08, 0x01},
	{0x3a09, 0x50},
	{0x3a0a, 0x01},
	{0x3a0b, 0x18},
	{0x3a0d, 0x03},
	{0x3a0e, 0x03},
	{0x4520, 0x00},
	{0x4837, 0x1b},
	{0x3000, 0xff},
	{0x3001, 0xff},
	{0x3002, 0xf0},
	{0x3600, 0x08},
	{0x3621, 0xc0},
	{0x3632, 0xe3},
	{0x3633, 0x23},
	{0x3634, 0x24},
	{0x3f01, 0x0c},
	{0x5001, 0xc1}, // disable BLC
	{0x3614, 0xf0},
	{0x3630, 0x2d},
	{0x370b, 0x62},
	{0x3706, 0x61},
	{0x4000, 0x02},
	{0x4002, 0xc5},
	{0x4005, 0x08},
	{0x404f, 0x84},
	{0x4051, 0x00},
	{0x5000, 0xCF}, // disable awb, lsc
	{0x3a18, 0x00},
	{0x3a19, 0x80},
	{0x3503, 0x07}, //disable sensor AE, AG, delay 1 frame
	{0x4521, 0x00},
	{0x5183, 0xb0},
	{0x5184, 0xb0},
	{0x5185, 0xb0},
	{0x370c, 0x0c},
	{0x3035, 0x30},
	{0x3036, 0x1e},
	{0x3037, 0x21},
	{0x303e, 0x19},
	{0x3038, 0x06},
	{0x3020, 0x93},
	{0x3106, 0xf1},
	{0x3a0f, 0x40},
	{0x3a10, 0x38},
	{0x3a1b, 0x48},
	{0x3a1e, 0x30},
	{0x3a11, 0x90},
	{0x3a1f, 0x10},
	{0x3011, 0x22},
	{0x3035, 0x30}, // 72M pclk
	//{0x5040, 0x80}, // test pattern
	{0x3011, 0x62}, // drive strength adjustment
	{0x0100, 0x01},
};

#endif


/*
*********************************************************************************************************
*                                         Find FPS Setting pointer
* FUNCTION GetOvFpsSetting
*********************************************************************************************************
*/
/**
  Find FPS setting pointer in FPS setting array. These setting will be writen to sensor register so that sensor will preview with the
  FPS. This function is called by OV2722SetFormatFps() function.

  \param
  	Fps 		- the frame rate to be found
  	staOvFpsSetting        - the FPS setting array pointer
  	byArrayLen        - the length of the FPS setting array

  \retval
  	If find, return	the expected FPS setting pointer.
  	If not find, use default FPS setting pointer.
 *********************************************************************************************************
*/
static OV_FpsSetting_t*  GetOvFpsSetting(U8 Fps, OV_FpsSetting_t staOvFpsSetting[], U8 byArrayLen)
{
	U8 i;
	U8 Idx;

	ISP_MSG((" array size = %bd\n", byArrayLen));

	if(g_bIsHighSpeed)
	{
		Idx=2; //8fps for uxga, 10 fps for vga
	}
	else
	{
		Idx = 0;
	}

	for(i=0; i< byArrayLen; i++)
	{
		ISP_MSG((" fps i = %bd\n",i));
		if(staOvFpsSetting[i].byFps ==Fps)
		{
			Idx=i;
			break;
		}
	}
	ISP_MSG((" get fps idx = %bd\n",Idx));
	return &staOvFpsSetting[Idx];
}

void GetSensorPclkHsync(U16 wSensorSPFormat, U8 byFps)
{
	OV_FpsSetting_t *pOv2722_FpsSetting;
#ifdef OV2722_720P_VIDEO_FORMAT	
	if (wSensorSPFormat == HD720P_FRM)
	{
		pOv2722_FpsSetting=GetOvFpsSetting(byFps, g_staOV2722HD720FpsSetting, sizeof(g_staOV2722HD720FpsSetting)/sizeof(OV_FpsSetting_t));		
	}
	else
#endif
	{
		pOv2722_FpsSetting=GetOvFpsSetting(byFps, g_staOV2722FHDFpsSetting, sizeof(g_staOV2722FHDFpsSetting)/sizeof(OV_FpsSetting_t));		
	}
	g_wSensorHsyncWidth =  pOv2722_FpsSetting->wExtraDummyPixel; // this for manual exposure
	g_dwPclk = pOv2722_FpsSetting->dwPixelClk;		// this for scale speed
}

void OV2722SetFormatFps(U16 SetFormat, U8 Fps)
{
	OV_FpsSetting_t *pOv2722_FpsSetting;
	U16 wTemp;
	SetFormat = SetFormat;

#ifdef OV2722_720P_VIDEO_FORMAT
	if((g_wSensorSPFormat & SetFormat) == HD720P_FRM)
	{
		// initial all register setting
#ifdef _MIPI_EXIST_
		WriteSensorSettingWB(sizeof(gc_OV2722_MIPI_HD720_Setting)/3, gc_OV2722_MIPI_HD720_Setting);
#else
		WriteSensorSettingWB(sizeof(gc_OV2722_HD720_Setting)/3, gc_OV2722_HD720_Setting);
#endif

		// write fps setting
		pOv2722_FpsSetting=GetOvFpsSetting(Fps, g_staOV2722HD720FpsSetting, sizeof(g_staOV2722HD720FpsSetting)/sizeof(OV_FpsSetting_t));
		Write_SenReg(0x3037, pOv2722_FpsSetting->byClkrc);
		wTemp = pOv2722_FpsSetting->wExtraDummyPixel;
		Write_SenReg(0x380C, INT2CHAR(wTemp, 1)); //Write dummy pixel MSB
		Write_SenReg(0x380D, INT2CHAR(wTemp, 0)); //Write dummy pixel LSB

		// update firmware variable
		//g_wSensorHsyncWidth =  pOv2722_FpsSetting->wExtraDummyPixel; // this for manual exposure
		//g_dwPclk = pOv2722_FpsSetting->dwPixelClk;		// this for scale speed
		GetSensorPclkHsync(HD720P_FRM, Fps);
		g_wAECExposureRowMax = 760;	//hemonel 2010-12-09: We use 7. At OV2722 When porting using manual exp control, the max exposure, at least should subtract 6
		g_wAEC_LineNumber = 754;	// this for AE insert dummy line algothrim

		g_wSensorWidthBefBLC = 1280;
		g_wSensorHeightBefBLC = 720;

	}
	else
#endif
	{
		// initial all register setting
#ifdef _MIPI_EXIST_
		WriteSensorSettingWB(sizeof(gc_OV2722_MIPI_FHD_Setting)/3, gc_OV2722_MIPI_FHD_Setting);
#else
		WriteSensorSettingWB(sizeof(gc_OV2722_FHD_Setting)/3, gc_OV2722_FHD_Setting);
#endif

		// write fps setting
		pOv2722_FpsSetting=GetOvFpsSetting(Fps, g_staOV2722FHDFpsSetting, sizeof(g_staOV2722FHDFpsSetting)/sizeof(OV_FpsSetting_t));
		Write_SenReg(0x3037, pOv2722_FpsSetting->byClkrc);
		wTemp = pOv2722_FpsSetting->wExtraDummyPixel;
		Write_SenReg(0x380C, INT2CHAR(wTemp, 1)); //Write dummy pixel MSB
		Write_SenReg(0x380D, INT2CHAR(wTemp, 0)); //Write dummy pixel LSB

		// update firmware variable
		//g_wSensorHsyncWidth =  pOv2722_FpsSetting->wExtraDummyPixel; // this for manual exposure
		//g_dwPclk = pOv2722_FpsSetting->dwPixelClk;		// this for scale speed
		GetSensorPclkHsync(HD1080P_FRM, Fps);
		g_wAECExposureRowMax = 1114;	//hemonel 2010-12-09: We use 7. At OV2722 When porting using manual exp control, the max exposure, at least should subtract 6
		g_wAEC_LineNumber = 1120;	// this for AE insert dummy line algothrim

		g_wSensorWidthBefBLC = 1920;
		g_wSensorHeightBefBLC = 1080;

	}

#ifdef _MIPI_EXIST_
	SetMipiDphy( MIPI_DATA_LANE0_EN , MIPI_DATA_FORMAT_RAW10 , \
	             MIPI_DATA_TYPE_RAW10,HSTERM_EN_TIME_22);
#endif

}


void CfgOV2722ControlAttr(void)
{
	U8 i;

	g_bySensorSize = SENSOR_SIZE_FHD;

#ifdef OV2722_720P_VIDEO_FORMAT
	g_wSensorSPFormat = HD1080P_FRM | HD720P_FRM;
#else
	g_wSensorSPFormat = HD1080P_FRM;
#endif

	memcpy(g_asOvCTT,gc_OV2722_CTT,sizeof(gc_OV2722_CTT));

	//if(g_byBKSensorPwrSettingExist==0)  //sensor power setting
	{
#if (_CHIP_ID_ & _RTS5840_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V79;
#elif (_CHIP_ID_ & _RTS5832_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V85;
#elif (_CHIP_ID_ & _RTS5829B_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V82;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V80;  //modify for VCM
#endif									
	}

	//if(g_byFormatCfgExist==0) // format config not exist, use default setting.
	{
		//====== resolution  setting ===========
		// -- preview ---
		g_aVideoFormat[0].byaVideoFrameTbl[0] = 12;		// resolution number
		g_aVideoFormat[0].byaVideoFrameTbl[1]=F_SEL_640_480;
		g_aVideoFormat[0].byaVideoFrameTbl[2]=F_SEL_320_180;
		g_aVideoFormat[0].byaVideoFrameTbl[3]=F_SEL_320_240;
		g_aVideoFormat[0].byaVideoFrameTbl[4]=F_SEL_352_288;
		g_aVideoFormat[0].byaVideoFrameTbl[5]=F_SEL_424_240;
		g_aVideoFormat[0].byaVideoFrameTbl[6]=F_SEL_640_360;
		g_aVideoFormat[0].byaVideoFrameTbl[7]=F_SEL_640_400;
		g_aVideoFormat[0].byaVideoFrameTbl[8]=F_SEL_848_480;
		g_aVideoFormat[0].byaVideoFrameTbl[9]=F_SEL_960_540;
		g_aVideoFormat[0].byaVideoFrameTbl[10]=F_SEL_1280_720;
		g_aVideoFormat[0].byaVideoFrameTbl[11]=F_SEL_1280_800;
		g_aVideoFormat[0].byaVideoFrameTbl[12]=F_SEL_1920_1080;
		//--still image--
		g_aVideoFormat[0].byaStillFrameTbl[0] = 5;	// resolution number
		g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_1920_1080;
		g_aVideoFormat[0].byaStillFrameTbl[2]=F_SEL_1280_720;
		g_aVideoFormat[0].byaStillFrameTbl[3]=F_SEL_320_240;
		g_aVideoFormat[0].byaStillFrameTbl[4]=F_SEL_352_288;
		g_aVideoFormat[0].byaStillFrameTbl[5]=F_SEL_640_480;

		//====== fps  setting ===========
		for(i=0; i<7; i++) //below VGA resolution
		{
			/*640_480,
			160_120,
			176_144,
			320_200,
			320_240,
			352_288,
			640_400*/
			g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_15|FPS_20|FPS_25|FPS_30;
		}
		for(i=7; i<16; i++)
		{

			g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_20|FPS_15|FPS_10|FPS_5;
		}

		for(i=16; i<20; i++)
		{
			g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_3 |FPS_5|FPS_10;
		}

		//====== format type  setting ===========
		g_aVideoFormat[0].byFormatType = FORMAT_TYPE_YUY2;

#ifdef _ENABLE_MJPEG_
		// copy YUY2 setting to MJPEG setting
		memcpy(&(g_aVideoFormat[1]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

		// modify MJPEG FPS setting
		for(i=0;i<20;i++)
		{
			/*
			 F_SEL_800_600    (7)
			 F_SEL_960_720    (8)
			 F_SEL_1024_768   (9)
			 F_SEL_1280_720   (10)
			 F_SEL_1280_800   (11)
			 F_SEL_1280_960   (12)
			 F_SEL_1280_1024  (13)
			 F_SEL_1600_1200  (14)
			 F_SEL_1920_1080  (15)
			 F_SEL_1600_900	 (16)
			 */
			g_aVideoFormat[1].waFrameFpsBitmap[i] = (FPS_10|FPS_15|FPS_20|FPS_25|FPS_30);
		}

		// modify MJPEG format type
		g_aVideoFormat[1].byFormatType = FORMAT_TYPE_MJPG;

#endif

#ifdef _ENABLE_M420_FMT_
		// copy YUY2 setting to M420 setting
		memcpy(&(g_aVideoFormat[2]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

		// modify M420 FPS setting
		{
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_640_480] =FPS_25|FPS_20|FPS_15|FPS_10|FPS_5;//|FPS_1;             //bit07 F_800_600
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_960_540] =FPS_25|FPS_20|FPS_15|FPS_10|FPS_5;//|FPS_5|FPS_3|FPS_1;                    //bit08 F_960_720
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1024_768] =FPS_15|FPS_10|FPS_5;//|FPS_1;                    //bit09 F_1024_768
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_720] =FPS_10|FPS_5;//FPS_9|FPS_8|FPS_1;    //bit10 F_1280_720
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_800] =FPS_10|FPS_5;
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1920_1080] =FPS_5;
			// modify M420 format type
			g_aVideoFormat[2].byFormatType = FORMAT_TYPE_M420;
		}
#endif

	}	

	InitOV2722IspParams();
}

void OV2722_POR()
{
#ifdef _MIPI_EXIST_
	Init_MIPI();
#else
	Init_CCS();
#endif

	ENTER_SENSOR_RESET();

	// power on
	SensorPowerControl(SWITCH_ON,EN_DELAYTIME);
	uDelay(1);

	// Initialize I2C Controller Timing
	LEAVE_SENSOR_RESET();

	// clock select 24M
	if( g_bySSCEnable== TRUE)
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_SSC_24M);
	}
	else
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_96M|CCS_CLK_DIVIDER_4);
	}

	EnableSensorHCLK();
	g_bySensorIsOpen = SENSOR_OPEN;
	WaitTimeOut_Delay(1);  //OV2722 spec request 8192clk
}


void SetOV2722IntegrationTime(U16 wEspline)   //Manual exposure
{
	U16 wAECVTS;
	U32 dwExposureTime;

	if(wEspline > g_wAECExposureRowMax)
	{
		wAECVTS = wEspline - g_wAECExposureRowMax;
		if (wAECVTS%2==1)
		{
			wAECVTS++;
		}
	}
	else
	{
		wAECVTS = 0;
	}
	dwExposureTime = ((U32)wEspline)<<4;

	//-----------Write  frame length/dummy lines------------
	Write_SenReg(0x350C, INT2CHAR(wAECVTS, 1));
	Write_SenReg(0x350D, INT2CHAR(wAECVTS, 0));

	//-----------Write Exposuretime setting-----------
	Write_SenReg(0x3500, LONG2CHAR(dwExposureTime, 2));	// change exposure value high
	Write_SenReg(0x3501, LONG2CHAR(dwExposureTime, 1));	// change exposure value
	Write_SenReg(0x3502, LONG2CHAR(dwExposureTime, 0));	// change exposure value low

	//----------- write gain setting-------------------
	Write_SenReg(0x350A, 0);	// fixed gain at manual exposure control
	Write_SenReg(0x350B, 0);
}


void SetOV2722Exposuretime_Gain(float fExpTime, float fTotalGain)
{
	U16  wGainRegSetting; //data
	float fSnrGlbGain;
	U32 dwExposureTime;
	U8 byTemp;

	wGainRegSetting = (U16)(fTotalGain*16.0);
	fSnrGlbGain = (float)wGainRegSetting/16.0;

	dwExposureTime = (U32)(fExpTime*16/g_fSensorRowTimes);
	// hemonel 2010-12-09: image occur white point issue
	//					OV note that exposure tline need less than one line then can use sub-line
	if(dwExposureTime> 16)
	{
		byTemp = dwExposureTime&0x0F;
		dwExposureTime = dwExposureTime&0xFFFFFFF0;	// cut sub-line
		if(byTemp >= 8)	// round sub-line to tline
		{
			dwExposureTime+=16;
		}
	}

	// write exposure, gain, dummy line
	if(fabs(g_fCurExpTime-fExpTime) > 0.0001)
	{
		Write_SenReg(0x3208, 0);

		//-----------Write  frame length/dummy lines------------
		Write_SenReg(0x350C, INT2CHAR(g_wAFRInsertDummylines, 1));
		Write_SenReg(0x350D, INT2CHAR(g_wAFRInsertDummylines, 0));

		//-----------Write Exposuretime setting-----------
		Write_SenReg(0x3500, LONG2CHAR(dwExposureTime, 2));	// change exposure value high
		Write_SenReg(0x3501, LONG2CHAR(dwExposureTime, 1));	// change exposure value
		Write_SenReg(0x3502, LONG2CHAR(dwExposureTime, 0));	// change exposure value low

		//----------- write gain setting-------------------
		Write_SenReg(0x3508, INT2CHAR(wGainRegSetting, 1));
		Write_SenReg(0x3509, INT2CHAR(wGainRegSetting, 0));

		SetISPAEGain(fTotalGain, fSnrGlbGain, 1);

		Write_SenReg(0x3208, 0x10);
		Write_SenReg(0x3208, 0xA0);


		g_fCurExpTime = fExpTime;	// back up exposure rows

	}
	else
	{
		Write_SenReg(0x3208, 0);

		//-----------Write  frame length/dummy lines------------
		Write_SenReg(0x350C, INT2CHAR(g_wAFRInsertDummylines, 1));
		Write_SenReg(0x350D, INT2CHAR(g_wAFRInsertDummylines, 0));
		
		Write_SenReg(0x3508, INT2CHAR(wGainRegSetting, 1));
		Write_SenReg(0x3509, INT2CHAR(wGainRegSetting, 0));

		SetISPAEGain(fTotalGain, fSnrGlbGain, 1);

		Write_SenReg(0x3208, 0x10);
		Write_SenReg(0x3208, 0xA0);

	}

	return;
}

void SetOV2722Gain(float fGain)
{
}

void SetOV2722ImgDir(U8 bySnrImgDir)
{
	WaitFrameSync(ISP_INT1,ISP_DATA_END_INT);//zouxiaozhi 2011-12-31:this wait is very differnt from other sensor

	// OV2722 set the mirrored as normal output,0x3820 bit[2:1]->flip,0x3821 bit[2:1] ->mirror
	switch(bySnrImgDir & 0x03)
	{
	case SNR_IMG_DIR_MIRROR:
		Write_SenReg(0x3821, 0x00);
		Write_SenReg(0x3820, 0x80);
#ifdef _MIPI_EXIST_
		SetBLCWindowStart(1, 1);
#else
		SetBkWindowStart(1, 1);
#endif
		break;
	case SNR_IMG_DIR_FLIP:
#ifdef _MIPI_EXIST_
		Write_SenReg(0x3821, 0x06);
		Write_SenReg(0x3820, 0x86);
#else
		Write_SenReg(0x3821, 0x02);
		Write_SenReg(0x3820, 0x82);
#endif
#ifdef _MIPI_EXIST_
		SetBLCWindowStart(0, 1);
#else
		SetBkWindowStart(0, 0);
#endif

		break;
	case SNR_IMG_DIR_FLIP_MIRROR:
		Write_SenReg(0x3821, 0x00);
#ifdef _MIPI_EXIST_
		Write_SenReg(0x3820, 0x86);
#else
		Write_SenReg(0x3820, 0x82);
#endif
#ifdef _MIPI_EXIST_
		SetBLCWindowStart(1, 1);
#else
		SetBkWindowStart(1, 0);
#endif

		break;
	default://normal
		Write_SenReg(0x3821, 0x02);
		Write_SenReg(0x3820, 0x80);
#ifdef _MIPI_EXIST_
		SetBLCWindowStart(0, 1);
#else
		SetBkWindowStart(0, 1);
#endif

		break;
	}
}


void InitOV2722IspParams()
{
	// AWB initial gain
	//use D50 R/G/B gain as AWB initial value
	g_wAWBRGain_Last= 0x116;
	g_wAWBGGain_Last= 0x100;
	g_wAWBBGain_Last= 0x182;
	g_wProjectGR = g_wAWBRGain_Last;
	g_wProjectGB = g_wAWBBGain_Last;
}

void SetOV2722DynamicISP(U8 byAEC_Gain)
{
	byAEC_Gain = byAEC_Gain;
}

void SetOV2722DynamicISP_AWB(U16 wColorTempature)
{
	wColorTempature =wColorTempature;
}


#endif

