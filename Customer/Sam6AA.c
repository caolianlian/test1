#include "Inc.h"

//#define VD_DBGP3(x)        printf x

#ifdef RTS58XX_SP_S5K6AAFX
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<<<new
U16  xdata samsung6AA_0x0400;
U16  samsung6AA_0x021C;

code OV_CTT_t gc_S5K6AA_CTT[3] =
{
	{3000,0x40,0x4C,0xd0},
	{4050,0x4d,0x40,0x8a},
	{6500,0x54,0x40,0x52},
};

typedef struct Sam6AA_FpsSetting
{
	U8 byFps;
	U16 wFrameratelmax;   // samsung6AA framerate
	U16 wFrameratelmin;
	U8  byClk; // samsung6AA ISPclk
	U32 dwPixelClk;
} Sam6AA_FpsSetting_t;

code Sam6AA_FpsSetting_t  g_staSam6AAFpsSetting[]=
{
	//  FPS	framerate	x x
	{3,		(0xd05),		(0xd05),		(0),		  (0x00)},	// SXGA
	{5,		(0x7d0),		(0xd05),		(0),		  (0x00)},	// SAGA
	{7,		(0x566),	    	(0xd05),   	(0),		  (0x00)},	       // SXGA  7.5fps
	{8,		(0x535),	    	(0xd05),   	(0),		  (0x00)},	       // SXGA  7.5fps
	{10,		(0x3E8),		(0x535),		(0),		  (0x00)},	// VGA
	{15,		(0x29A),		(0x535),		(0),		  (0x00)},	// VGA
	{20,		(0x1f4),		(0x535),		(0),		  (0x00)},	// VGA
	{25,		(0x190),		(0x535),    	(0),		  (0x00)},	// VGA
	{30,		(0x14d),   	(0x535),    	(0),           (0x00)},	       // VGA
};


t_RegSettingWW code gc_Sam6AA_PLL_Setting[] =
{

	{0x1030,  0x0000},
	{0x0014,  0x0001},
	{0x0028,  0x7000},
	{0x002a,  0x1d60},    // ????  SETTING ?
	{0x002a,  0x10da},
	{0x0f12,  0x0001},
	{0x002a,  0x1102},
	{0x0f12,  0x0000},
	{0x002a,  0x1108},
	{0x0f12,  0x0090},
	{0x002a,  0x110c},
	{0x0f12,  0x0003},
	{0x002a,  0x110e},
	{0x0f12,  0x0003},
	{0xf40c,  0x0060},
	{0xf40e,  0x0003},
	{0xf41e,  0x0039},
	{0xf42a,  0x0090},
	{0xf452,  0x0001},
	{0xf424,  0x0030},
	{0xf4ce,  0x0000},
	{0x1000,  0x0001},


	{0x0028,0x7000},//same as 0xfcfc,0x7000 (direct adress, But may something wrong),//0x0028 (indirect address, 0x002a =adress, 0x0f12=value)
	{0x002a,0x0400},
	{0x0F12,0x007F}, //Auto 7Fh, manual:5Fh
	{0x002A,0x03Dc}, //REG_SF_USER_FlickerQuant
	{0x0F12,0x0000}, // 0-disable 1-50 2-60
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//{0x0F12,0x005F}, //Auto 7Fh, manual:5Fh
//{0x002A,0x03Dc}, //REG_SF_USER_FlickerQuant
//{0x0F12,0x0001}, // 0-disable 1-50 2-60
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	{0x002A,0x03DE}, //REG_SF_USER_FlickerQuantChanged
	{0x0F12,0x0000},
	{0x002A,0x092c}, //Noise Reduction
	{0x0F12,0xafe2},
	{0x002A,0x09aa}, //Noise Reduction
	{0x0F12,0xafe2},
	{0x002A,0x0928}, //Noise Reduction
	{0x0F12,0xafe2},
///////////////////////////////
//clk Settings---------------------------2 clock   24M or 26M
	{0x002A,0x01b8},	//REG_TC_IPRM_InClockLSBs
	{0x0F12,0x5DC0},        //input=24MHz
	{0x002A,0x01BA},	//REG_TC_IPRM_InClockMSBs
	{0x0F12,0x0000},
	{0x002A,0x01C6}, 	//REG_TC_IPRM_UseNPviClocks	//1 PLL configurations
	{0x0F12,0x0002},

	{0x002A,0x01CC}, 	//REG_TC_IPRM_OpClk4KHz_0	//1st system CLK 26MHz for VGA
	{0x0F12,0x1964},
	{0x002A,0x01CE},	//REG_TC_IPRM_MinOutRate4KHz_0
	{0x0F12,0x1964},
	{0x002A,0x01D0},	//REG_TC_IPRM_MaxOutRate4KHz_0
	{0x0F12,0x1964},

	{0x002A,0x01D2},       //REG_TC_IPRM_OpClk4KHz_1	//2nd system CLK 24MHz for SXGA
	{0x0F12,0x1770},
	{0x002A,0x01D4},	//REG_TC_IPRM_MinOutRate4KHz_1
	{0x0F12,0x1770},
	{0x002A,0x01D6},	//REG_TC_IPRM_MaxOutRate4KHz_1
	{0x0F12,0x1770},
	{0x002A,0x01E0}, 	//REG_TC_IPRM_InitParamsUpdated
	{0x0F12,0x0001},


	{0x002a,  0x08a0},  //
	{0x0f12,  0x8039},
	{0x002a,  0x091e},  //
	{0x0f12,  0x8055},
	{0x002a,  0x099c},  //
	{0x0f12,  0x8055},
	{0x002a,  0x0a1a},  //
	{0x0f12,  0x8055},
	{0x002a,  0x0a98},  //
	{0x0f12,  0x8055},
	{0x002a,  0x0c48},  //
	{0x0f12,  0x0000},
	{0x0f12,  0x0000},
	{0x0f12,  0x0000},
};
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
t_RegSettingWW code gc_Sam6AA_PLL_Setting_FS[] =
{
	{0x1030,  0x0000},
	{0x0014,  0x0001},
	{0x0028,  0x7000},
	{0x002a,  0x1d60},    // ????  SETTING ?
	{0x002a,  0x10da},
	{0x0f12,  0x0001},
	{0x002a,  0x1102},
	{0x0f12,  0x0000},
	{0x002a,  0x1108},
	{0x0f12,  0x0090},
	{0x002a,  0x110c},
	{0x0f12,  0x0003},
	{0x002a,  0x110e},
	{0x0f12,  0x0003},
	{0xf40c,  0x0060},
	{0xf40e,  0x0003},
	{0xf41e,  0x0039},
	{0xf42a,  0x0090},
	{0xf452,  0x0001},
	{0xf424,  0x0030},
	{0xf4ce,  0x0000},
	{0x1000,  0x0001},

	{0x0028,0x7000},//same as 0xfcfc,0x7000 (direct adress, But may something wrong),//0x0028 (indirect address, 0x002a =adress, 0x0f12=value)
	{0x002a,0x0400},
	{0x0F12,0x007F}, //Auto 7Fh, manual:5Fh
	{0x002A,0x03Dc}, //REG_SF_USER_FlickerQuant
	{0x0F12,0x0000}, // 0-disable 1-50 2-60
	{0x002A,0x03DE}, //REG_SF_USER_FlickerQuantChanged
	{0x0F12,0x0000},
	{0x002A,0x092c}, //Noise Reduction
	{0x0F12,0xafe2},
	{0x002A,0x09aa}, //Noise Reduction
	{0x0F12,0xafe2},
	{0x002A,0x0928}, //Noise Reduction
	{0x0F12,0xafe2},
///////////////////////////////
//clk Settings---------------------------2 clock   24M or 26M
	{0x002A,0x01b8},	//REG_TC_IPRM_InClockLSBs
	{0x0F12,0x5DC0},        //input=24MHz
	{0x002A,0x01BA},	//REG_TC_IPRM_InClockMSBs
	{0x0F12,0x0000},
	{0x002A,0x01C6}, 	//REG_TC_IPRM_UseNPviClocks	//1 PLL configurations  number of PLL(n)
	{0x0F12,0x0002},

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//PCLK=12MHz
	{0x002A,0x01CC}, 	//REG_TC_IPRM_OpClk4KHz_0	//1st system CLK 26MHz for VGA
	{0x0F12,0x0bb8}, //12Mhz
	{0x002A,0x01CE},	//REG_TC_IPRM_MinOutRate4KHz_0
	{0x0F12,0x0bb8},    //12MHz
	{0x002A,0x01D0},	//REG_TC_IPRM_MaxOutRate4KHz_0
	{0x0F12,0x0bb8},  //12Mhz
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ full speed~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	{0x002A,0x01D2},       //REG_TC_IPRM_OpClk4KHz_1	//2nd system CLK 24MHz for SXGA
	{0x0F12,0x1770},
	{0x002A,0x01D4},	//REG_TC_IPRM_MinOutRate4KHz_1
	{0x0F12,0x1770},
	{0x002A,0x01D6},	//REG_TC_IPRM_MaxOutRate4KHz_1
	{0x0F12,0x1770},
	{0x002A,0x01E0}, 	//REG_TC_IPRM_InitParamsUpdated
	{0x0F12,0x0001},

	{0x002a,  0x08a0},  //
	{0x0f12,  0x8039},
	{0x002a,  0x091e},  //
	{0x0f12,  0x8055},
	{0x002a,  0x099c},  //
	{0x0f12,  0x8055},
	{0x002a,  0x0a1a},  //
	{0x0f12,  0x8055},
	{0x002a,  0x0a98},  //
	{0x0f12,  0x8055},
	{0x002a,  0x0c48},  //
	{0x0f12,  0x0000},
	{0x0f12,  0x0000},
	{0x0f12,  0x0000},
};
t_RegSettingWW code gc_Sam6AA_Setting[] =
{
	{0x0028,0x7000}, //FPS up from 10 to 15
	{0x002a,0x0488}, //FPS up from 10 to 15
	{0x0f12,0x61A8}, //FPS up from 10 to 15
	{0x002A,0x0242},	//REG_0TC_PCFG_usWidth
	{0x0F12,0x0500},	// 0320 					//1280
	{0x002A,0x0244},	//REG_0TC_PCFG_usHeight
	{0x0F12,0x0400},	//0258					//1024
	{0x002A,0x0246},	//REG_0TC_PCFG_Format
	{0x0F12,0x0005},	//YUV
	{0x002A,0x024E},	//REG_0TC_PCFG_uClockInd 		//PLL config
	{0x0F12,0x0001},
	{0x002A,0x0248},	//REG_0TC_PCFG_usMaxOut4KHzRate		//PCLK max
	{0x0F12,0x1770},
	{0x002A,0x024A},	//REG_0TC_PCFG_usMinOut4KHzRate		//PCLK min
	{0x0F12,0x1770},
	{0x002A,0x024C},	//REG_0TC_PCFG_PVIMask
	{0x0F12,0x0042},
	{0x002A,0x0252},	//REG_0TC_PCFG_FrRateQualityType		//1b: Avg S.S 2b: SXGA
	{0x0F12,0x0002},
	{0x002A,0x0250},	//REG_0TC_PCFG_usFrTimeType
	{0x0F12,0x0002},
	{0x002A,0x0254},	//REG_0TC_PCFG_usMaxFrTimeMsecMult10	//max frame time : 15fps 029a
//{0x0F12,0x069E}, 	//7.5fps 0535 //9 fps 0457//5 fps 07d0//3fps 0d05
	{0x0F12,0x0d05}, 	//7.5fps 0535 //9 fps 0457//5 fps 07d0//3fps 0d05
//{0x0F12,0x0535}, 	//7.5fps 0535 //9 fps 0457//5 fps 07d0//3fps 0d05

	{0x002A,0x0256},	//REG_0TC_PCFG_usMinFrTimeMsecMult10
	{0x0F12,0x0594},  //7 Fps
//{0x0F12,0x0535},  //7.5Fps
//{0x0F12,0x0d05},

//--------------------------------------PREVIEW CONFIGURATION 1 (SXGA, YUV, 4-7.5fps)
	{0x002A,0x0268}, //#REG_1TC_PCFG_usWidth
	{0x0F12,0x0500},  					//1280
	{0x002A,0x026A}, //#REG_1TC_PCFG_usHeight
	{0x0F12,0x0400}, 				//1024
	{0x002A,0x026C}, //#REG_1TC_PCFG_Format
	{0x0F12,0x0005},					//YUV
	{0x002A,0x0274}, //#REG_1TC_PCFG_uClockInd 		//PLL config
//{0x0F12,0x0000},
	{0x0F12,0x0001}, //cski debug**********************
	{0x002A,0x026E}, //#REG_1TC_PCFG_usMaxOut4KHzRate		//PCLK max
	{0x0F12,0x1770},
	{0x002A,0x0270}, //#REG_1TC_PCFG_usMinOut4KHzRate		//PCLK min
	{0x0F12,0x1770},
	{0x002A,0x0272}, //#REG_1TC_PCFG_PVIMask
	{0x0F12,0x0042},
	{0x002A,0x0278}, //#REG_1TC_PCFG_FrRateQualityType		//1b: Avg S.S 2b: SXGA
	{0x0F12,0x0002},
	{0x002A,0x0276}, //#REG_1TC_PCFG_usFrTimeType
	{0x0F12,0x0000},
	{0x002A,0x027A}, //#REG_1TC_PCFG_usMaxFrTimeMsecMult10	//max frame time : 15fps 029a
//{0x0F12,0x0B29},
	{0x0F12,0x0d05},
	{0x002A,0x027C}, //#REG_1TC_PCFG_usMinFrTimeMsecMult10
//{0x0F12,0x029a},
	{0x0F12,0x0535},
//PREVIEW ---------------------------------------CONFIGURATION 2 (VGA, YUV, 30fps)
	{0x002A,0x028E}, //#REG_2TC_PCFG_usWidth
	{0x0F12,0x0280}, 					//640
	{0x002A,0x0292}, //#REG_2TC_PCFG_usHeight
	{0x0F12,0x01E0}, 					//480
	{0x002A,0x029A}, //#REG_2TC_PCFG_Format
	{0x0F12,0x0005},					//YUV
	{0x002A,0x029C}, //#REG_2TC_PCFG_uClockInd 		//PLL config
	{0x0F12,0x0001},
	{0x002A,0x0294}, //#REG_2TC_PCFG_usMaxOut4KHzRate		//PCLK max
	{0x0F12,0x1770},
	{0x002A,0x0296}, //#REG_2TC_PCFG_usMinOut4KHzRate		//PCLK min
	{0x0F12,0x1770},
	{0x002A,0x0A9E}, //#REG_2TC_PCFG_PVIMask
	{0x0F12,0x0042},
	{0x002A,0x029E}, //#REG_2TC_PCFG_FrRateQualityType		//1b: Avg S.S 2b: SXGA
	{0x0F12,0x0001},
	{0x002A,0x029C}, //#REG_2TC_PCFG_usFrTimeType
	{0x0F12,0x0002},
	{0x002A,0x02A0}, //#REG_2TC_PCFG_usMaxFrTimeMsecMult10	//max frame time : 15fps 029a
	{0x0F12,0x0159},
	{0x002A,0x02A2}, //#REG_2TC_PCFG_usMinFrTimeMsecMult10
	{0x0F12,0x0000},
//PREVIEW------------------------------------CONFIGURATION 3 (VGA, YUV, 7.5-30fps)
#if 0
	{0x002A,0x02B4},	//REG_3TC_PCFG_usWidth
//{0x0F12,0x0280}, 	// 0320 			//640
	{0x0F12,0x00a0}, 	//  			//160
	{0x002A,0x02B6},	//REG_3TC_PCFG_usHeight
//{0x0F12,0x01E0}, 	//0258				//480
	{0x0F12,0x0078}, 	//				//120
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	{0x002A,0x02B8},	//REG_3TC_PCFG_Format
	{0x0F12,0x0005},					//YUV
	{0x002A,0x02C0},	//REG_3TC_PCFG_uClockInd 	//PLL config
	{0x0F12,0x0000},
	{0x002A,0x02BA},	//REG_3TC_PCFG_usMaxOut4KHzRate	//PCLK max
	{0x0F12,0x1770},
	{0x002A,0x02BC},	//REG_3TC_PCFG_usMinOut4KHzRate	//PCLK min
	{0x0F12,0x1770},
	{0x002A,0x02BE},	//REG_3TC_PCFG_PVIMask
	{0x0F12,0x0042},
	{0x002A,0x02C4},	//REG_3TC_PCFG_FrRateQualityType //1b: Avg S.S 2b: SXGA
	{0x0F12,0x0001},
	{0x002A,0x02C2}, 	//REG_3TC_PCFG_usFrTimeType
	{0x0F12,0x0000},
	{0x002A,0x02C6},	//REG_3TC_PCFG_usMaxFrTimeMsecMult10	//max frame time : 15fps 029a
	{0x0F12,0x0535}, 	// 7.5fps Ban setting.
//{0x0F12,0x0000}, 	// 0fps //cski try **************************************
	{0x002A,0x02C8}, 	//REG_3TC_PCFG_usMinFrTimeMsecMult10
	{0x0F12,0x014d},
#endif
};

t_RegSettingWW code config3_size_VGA_Setting[] =
{
//PREVIEW------------------------------------CONFIGURATION 3 (VGA, YUV, 7.5-30fps)
	{0x002A,0x02B4},	//REG_3TC_PCFG_usWidth
	{0x0F12,0x0280}, //640
	{0x002A,0x02B6},	//REG_3TC_PCFG_usHeight
	{0x0F12,0x01E0}, 	//480
};

t_RegSettingWW code config3_size_QVGA_Setting[] =
{
//PREVIEW------------------------------------CONFIGURATION 3 (VGA, YUV, 7.5-30fps)
	{0x002A,0x02B4},	//REG_3TC_PCFG_usWidth
	{0x0F12,0x0140}, //320
	{0x002A,0x02B6},	//REG_3TC_PCFG_usHeight
	{0x0F12,0x00F0}, 	//240
};

t_RegSettingWW code config3_size_CIF_Setting[] =
{
//PREVIEW------------------------------------CONFIGURATION 3 (VGA, YUV, 7.5-30fps)
	{0x002A,0x02B4},	//REG_3TC_PCFG_usWidth
	{0x0F12,0x0160}, //352
	{0x002A,0x02B6},	//REG_3TC_PCFG_usHeight
	{0x0F12,0x0120}, 	//288
};

t_RegSettingWW code config3_size_QCIF_Setting[] =
{
//PREVIEW------------------------------------CONFIGURATION 3 (VGA, YUV, 7.5-30fps)
	{0x002A,0x02B4},	//REG_3TC_PCFG_usWidth
	{0x0F12,0x00B0}, //176
	{0x002A,0x02B6},	//REG_3TC_PCFG_usHeight
	{0x0F12,0x0090}, 	//144
};

t_RegSettingWW code config3_size_QQVGA_Setting[] =
{
//PREVIEW------------------------------------CONFIGURATION 3 (VGA, YUV, 7.5-30fps)
	{0x002A,0x02B4},	//REG_3TC_PCFG_usWidth
	{0x0F12,0x00A0}, //160
	{0x002A,0x02B6},	//REG_3TC_PCFG_usHeight
	{0x0F12,0x0078}, 	//120
};

t_RegSettingWW code config3_size_Setting[] =
{
	{0x002A,0x02B8},	//REG_3TC_PCFG_Format
	{0x0F12,0x0005},					//YUV
	{0x002A,0x02C0},	//REG_3TC_PCFG_uClockInd 	//PLL config
	{0x0F12,0x0000},
	{0x002A,0x02BA},	//REG_3TC_PCFG_usMaxOut4KHzRate	//PCLK max
	{0x0F12,0x1770},
	{0x002A,0x02BC},	//REG_3TC_PCFG_usMinOut4KHzRate	//PCLK min
	{0x0F12,0x1770},
	{0x002A,0x02BE},	//REG_3TC_PCFG_PVIMask
	{0x0F12,0x0042},
	{0x002A,0x02C4},	//REG_3TC_PCFG_FrRateQualityType //1b: Avg S.S 2b: SXGA
	{0x0F12,0x0001},
	{0x002A,0x02C2}, 	//REG_3TC_PCFG_usFrTimeType
	{0x0F12,0x0000},
////{0x002A,0x02C6},	//REG_3TC_PCFG_usMaxFrTimeMsecMult10	//max frame time : 15fps 029a
////{0x0F12,0x0535}, 	// 7.5fps Ban setting.
//{0x0F12,0x0000}, 	// 0fps //cski try **************************************
////{0x002A,0x02C8}, 	//REG_3TC_PCFG_usMinFrTimeMsecMult10
////{0x0F12,0x014d},
};

t_RegSettingWW code gc_Sam6AA_FS_Setting[] =
{
	{0x002A,0x02BA},	//REG_3TC_PCFG_usMaxOut4KHzRate	//PCLK max
	{0x0F12,0x0BB8}, // 12M
	{0x002A,0x02BC},	//REG_3TC_PCFG_usMinOut4KHzRate	//PCLK min
	{0x0F12,0x0BB8}, // 12M
};
t_RegSettingWW code gc_Sam6AA_VGA_Setting[] =
{
	{0x002A,0x021C},	//REG_TC_GP_ActivePrevConfig
	{0x0F12,0x0003},   	//-------------------------> 0: fixed sxga 1: dynamic sxga 2: fixed vga 3: dynamic vga
	{0x002A,0x0220},	//REG_TC_GP_PrevOpenAfterChange
	{0x0F12,0x0001},
	{0x002A,0x01F8},	//REG_TC_GP_NewConfigSync
	{0x0F12,0x0001},
	{0x002A,0x021E},	//REG_TC_GP_PrevConfigChanged
	{0x0F12,0x0001},
	{0x002A,0x01F0},	//REG_TC_GP_EnablePreview
	{0x0F12,0x0001},
	{0x002A,0x01F2},	//REG_TC_GP_EnablePreviewChanged
	{0x0F12,0x0001},
};
t_RegSettingWW code gc_Sam6AA_SXGA_Setting[] =
{
	{0x002A,0x021C},	//REG_TC_GP_ActivePrevConfig
//{0x0F12,0x0000},   	//-------------------------> 0: fixed sxga 1: dynamic sxga 2: fixed vga 3: dynamic vga
	{0x0F12,0x0001}, //using config-1*************************=====!!!!!!!!!
	{0x002A,0x0220},	//REG_TC_GP_PrevOpenAfterChange
	{0x0F12,0x0001},
	{0x002A,0x01F8},	//REG_TC_GP_NewConfigSync
	{0x0F12,0x0001},
	{0x002A,0x021E},	//REG_TC_GP_PrevConfigChanged
	{0x0F12,0x0001},
	{0x002A,0x01F0},	//REG_TC_GP_EnablePreview
	{0x0F12,0x0001},
	{0x002A,0x01F2},	//REG_TC_GP_EnablePreviewChanged
	{0x0F12,0x0001},
};

t_RegSettingWW code Bison_AWB_Setting[] =
{
	{0x002A,0x100e},
	{0x0f12,0x0101},
	{0x0f12,0x0101},
	{0x0f12,0x0101},
	{0x0f12,0x0101},
	{0x0f12,0x0101},
	{0x0f12,0x0101},
	{0x0f12,0x0101},
	{0x0f12,0x0101},
	{0x0f12,0x0101},
	{0x0f12,0x0301},
	{0x0f12,0x0103},
	{0x0f12,0x0101},
	{0x0f12,0x0101},
	{0x0f12,0x0503},
	{0x0f12,0x0305},
	{0x0f12,0x0101},
	{0x0f12,0x0201},
	{0x0f12,0x0505},
	{0x0f12,0x0505},
	{0x0f12,0x0102},
	{0x0f12,0x0201},
	{0x0f12,0x0505},
	{0x0f12,0x0505},
	{0x0f12,0x0102},
	{0x0f12,0x0201},
	{0x0f12,0x0504},
	{0x0f12,0x0405},
	{0x0f12,0x0102},
	{0x0f12,0x0201},
	{0x0f12,0x0303},
	{0x0f12,0x0303},
	{0x0f12,0x0102},
};


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>new

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<<new
static void SetSensor6AAFormat(U8 SetFormat)
{
	SetFormat = SetFormat; // for delete warning
	samsung6AA_0x0400 = 0x007F;

	// initial all register setting
	if(g_bIsHighSpeed)
	{
		WriteSensorSettingWW((sizeof(gc_Sam6AA_PLL_Setting)/sizeof(t_RegSettingWW)), gc_Sam6AA_PLL_Setting);
		//WriteSensorSettingWW((sizeof(gc_Sam6AA_Setting)/sizeof(t_RegSettingWW)), gc_Sam6AA_Setting);
	}
	if(!g_bIsHighSpeed)
	{
		//VD_DBGP0(("\n--vd code full  speed = --------****************************"));
		WriteSensorSettingWW((sizeof(gc_Sam6AA_PLL_Setting_FS)/sizeof(t_RegSettingWW)), gc_Sam6AA_PLL_Setting_FS);
		//WriteSensorSettingWW((sizeof(gc_Sam6AA_Setting)/sizeof(t_RegSettingWW)), gc_Sam6AA_Setting);
	}
	WriteSensorSettingWW((sizeof(gc_Sam6AA_Setting)/sizeof(t_RegSettingWW)), gc_Sam6AA_Setting);
}

Sam6AA_FpsSetting_t*  GetSam6AAFpsSetting(U8 Fps, Sam6AA_FpsSetting_t staSam6AAFpsSetting[], U8 byArrayLen)
{
	U8 i;
	U8 Idx;


	for(i=0; i< byArrayLen; i++)
	{
		//ISP_MSG((" fps i = %bd\n",i));
		if(staSam6AAFpsSetting[i].byFps ==Fps)
		{
			Idx=i;
			break;
		}
	}
	//ISP_MSG((" get fps idx = %bd\n",Idx));
	return &staSam6AAFpsSetting[Idx];
}

void SetS5K6AAFps(U8 SetFormat, U8 Fps)
{
	Sam6AA_FpsSetting_t *pSam6AA_FpsSetting;

	pSam6AA_FpsSetting=GetSam6AAFpsSetting(Fps, g_staSam6AAFpsSetting, sizeof(g_staSam6AAFpsSetting)/sizeof(Sam6AA_FpsSetting_t));
	if(SetFormat == VGA_FRM)
	{
		samsung6AA_0x021C = 0x03;
		// VD_DBGP3(("\n------------------------SetFormat == VGA_FRM-----------------------"));
		Write_SenReg(0x002A, 0x02C6);
		Write_SenReg(0x0F12, (pSam6AA_FpsSetting->wFrameratelmin));
		//VD_DBGP3(("\n--0x2c6 = wFrameratelmin) = %x------------------------------------",pSam6AA_FpsSetting->wFrameratelmin));
		Write_SenReg(0x002A, 0x02C8);
		Write_SenReg(0x0F12, (pSam6AA_FpsSetting->wFrameratelmax));
		//VD_DBGP3(("\n--0x2c8 = wFrameratelmax) = %x------------------------------------",pSam6AA_FpsSetting->wFrameratelmax));
	}
	else
	{
		samsung6AA_0x021C = 0x01;
		Write_SenReg(0x002A, 0x027a);	//config-1
		Write_SenReg(0x0F12, (pSam6AA_FpsSetting->wFrameratelmin));
		Write_SenReg(0x002A, 0x027c);	//config-1
		Write_SenReg(0x0F12, (pSam6AA_FpsSetting->wFrameratelmax));
	}
}


void S5K6AASetFormatFps(U8 SetFormat,U8 Fps)
{
//	XBYTE[0xfecd] = 0x07;  // 0x07(Normal Mode)

	SetSensor6AAFormat(SetFormat);
	SetS5K6AAFps(SetFormat,Fps);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>new

void	SetS5K6AASharpness(U8 bySetValue)
{
	U16 bySharpValueU16;

	bySharpValueU16 =  0xff81 + 32*bySetValue;

	Write_SenReg(0x0028,0x7000);
	Write_SenReg(0x002A,0x01EA);
	Write_SenReg(0x0F12,bySharpValueU16);
}

void SetS5K6AABandingFilter(U8 byFPS, U8 byLightFrq)
{
	byFPS = byFPS; 	// for delete warning

	samsung6AA_0x0400 = (samsung6AA_0x0400 & 0xdf); //manual flicker 0x0400[5]=0
	if (byLightFrq == PWR_LINE_FRQ_50)
	{


		Write_SenReg(0x0028,0x7000);
		Write_SenReg(0x002A,0x0400);
		Write_SenReg(0x0F12,samsung6AA_0x0400);//try
		Write_SenReg(0x002A,0x03DC);
		Write_SenReg(0x0F12,0x0001);// 0-disable 1-50 2-60
		Write_SenReg(0x002A,0x03DE);//REG_SF_USER_FlickerQuantChanged
		Write_SenReg(0x0F12,0x0001);
	}

	else if (byLightFrq == PWR_LINE_FRQ_60)
	{
		Write_SenReg(0x0028,0x7000);
		Write_SenReg(0x002A,0x0400);
		Write_SenReg(0x0F12,samsung6AA_0x0400);//try
		Write_SenReg(0x002A,0x03DC);
		Write_SenReg(0x0F12,0x0002);// 0-disable 1-50 2-60
		Write_SenReg(0x002A,0x03DE);//REG_SF_USER_FlickerQuantChanged
		Write_SenReg(0x0F12,0x0001);
	}
	else
	{
		//for disable mode
		Write_SenReg(0x0028,0x7000);
		Write_SenReg(0x002A,0x0400);
		Write_SenReg(0x0F12,samsung6AA_0x0400);//try
		Write_SenReg(0x002A,0x03DC);
		Write_SenReg(0x0F12,0x0000);// 0-disable 1-50 2-60
		Write_SenReg(0x002A,0x03DE);//REG_SF_USER_FlickerQuantChanged
		Write_SenReg(0x0F12,0x0001);
	}
}

void SetS5K6AABackLightComp(U8 bySetValue)
{
	U8 byAE_BackLight_Target;

	if(bySetValue==2)
	{
		byAE_BackLight_Target = 0x96;
	}
	else if(bySetValue==1)
	{
		byAE_BackLight_Target = 0x6B;
	}
	else
	{
		byAE_BackLight_Target = 0x47;
	}

	Write_SenReg(0x0028,0x7000);
	Write_SenReg(0x002A,0x1000);
	Write_SenReg(0x0F12,byAE_BackLight_Target);
}

// hemonel 2009-08-17: delete these code for optimize.
/*
OV_CTT_t S5K6AA_SetColorTemperature(U16 wSetValue)
{
	U8 data byTableIndex0,byTableIndex1;
	U8 i;
	float colordiff;
	OV_CTT_t ctt;
	OV_CTT_t * pCtt;

	pCtt = gc_Sensor_CTT;

	for(i = 1; i < CTT_INDEX_MAX; i++)
	{
		if(pCtt[i].wCT== 0)
		{
			// color temperature table end
			i--;
			break;
		}

		if(pCtt[i].wCT >  wSetValue)
		{
			break;
		}
	}

	if(i==CTT_INDEX_MAX)
	{
		byTableIndex0 = CTT_INDEX_MAX-2;
		byTableIndex1 = CTT_INDEX_MAX-1;
	}
	else
	{
		byTableIndex0 = i-1;
		byTableIndex1 = i;
	}

	// interpolation current color temperature gain value accroding to near color temperature value in color temperature table
	colordiff = ((float)wSetValue-(float)pCtt[byTableIndex0].wCT)/((float)pCtt[byTableIndex1].wCT-(float)pCtt[byTableIndex0].wCT);
	ctt.wRgain= pCtt[byTableIndex0].wRgain+((float)pCtt[byTableIndex1].wRgain - (float)pCtt[byTableIndex0].wRgain)*colordiff;
	ctt.wGgain = pCtt[byTableIndex0].wGgain+((float)pCtt[byTableIndex1].wGgain - (float)pCtt[byTableIndex0].wGgain)*colordiff;
	ctt.wBgain = pCtt[byTableIndex0].wBgain+((float)pCtt[byTableIndex1].wBgain - (float)pCtt[byTableIndex0].wBgain)*colordiff;

	return ctt;
}
*/

void SetS5K6AAWBTemp(U16 wRgain, U16 wGgain, U16 wBgain)
{
//	OV_CTT_t ctt;

//	ctt = OV_SetColorTemperature(wSetValue);

	Write_SenReg(0x0028,0x7000);
	Write_SenReg(0x002A,0x03D0);
	Write_SenReg(0x0F12,wRgain);
	Write_SenReg(0x002A,0x03D2);
	Write_SenReg(0x0F12,0x0001);
	Write_SenReg(0x002A,0x03D4);
	Write_SenReg(0x0F12,wGgain);

	Write_SenReg(0x002A,0x03D6);
	Write_SenReg(0x0F12,0x0001);
	Write_SenReg(0x002A,0x03D8);
	Write_SenReg(0x0F12,wBgain);
	Write_SenReg(0x002A,0x03DA);
	Write_SenReg(0x0F12,0x0001);
}

void SetS5K6AAWBTempAuto(U8 bySetValue)
{
	if(bySetValue)
	{
		samsung6AA_0x0400 = (samsung6AA_0x0400 |  0x08);
		// atuo white balance
		Write_SenReg(0x0028,0x7000);
		Write_SenReg(0x002A,0x0400);
		Write_SenReg(0x0F12,samsung6AA_0x0400);
	}
	else
	{
		samsung6AA_0x0400 = (samsung6AA_0x0400 &  0xf7);
		// manual white balance
		Write_SenReg(0x0028,0x7000);
		Write_SenReg(0x002A,0x0400);
		Write_SenReg(0x0F12,samsung6AA_0x0400);
	}
}
void SetS5K6AAIntegrationTimeAuto(U8 bySetValue)
{
	if(bySetValue == EXPOSURE_TIME_AUTO_MOD_APERTPRO)
	{
		// auto exposure
		Write_SenReg(0x0028,0x7000);
		samsung6AA_0x0400 |= 0x06;
		Write_SenReg(0x002a,0x0400);
		Write_SenReg(0x0f12,samsung6AA_0x0400);

	}
	else
	{
		// manual exposure
		Write_SenReg(0x0028,0x7000);
		samsung6AA_0x0400 &= 0xF9;
		Write_SenReg(0x002a,0x0400);
		Write_SenReg(0x0f12,samsung6AA_0x0400);
	}
}

void SetS5K6AAIntegrationTime(U32 dwSetValue)
{
	U16 wEspline;

	wEspline =dwSetValue;

	Write_SenReg(0x0028,0x7000);
	Write_SenReg(0x002a,0x03C6);
	Write_SenReg(0x0f12,wEspline);

	Write_SenReg(0x002a,0x03CA);
	Write_SenReg(0x0f12,0x0001);
}

void SetS5K6AAGain(float fGain)
{
}
// hemonel 2009-08-11: delete this function becasue it is not used.
/*
void SetS5K6AAEffect(U8 byEffect)
{
	if(byEffect&SNR_EFFECT_NEGATIVE)
	{
		// negative image
		Write_SenReg(0x0028,0x7000);
		Write_SenReg(0x002a,0x01EE);
		Write_SenReg(0x0f12,0x0002);
	}
	else if(byEffect&SNR_EFFECT_MONOCHROME)
	{
		//
		Write_SenReg(0x0028,0x7000);
		Write_SenReg(0x002a,0x01EE);
		Write_SenReg(0x0f12,0x0001);
	}

	else if(byEffect&SNR_EFFECT_SEPIA)
	{
		//
		Write_SenReg(0x0028,0x7000);
		Write_SenReg(0x002a,0x01EE);
		Write_SenReg(0x0f12,0x0003);
	}

	else if(byEffect&SNR_EFFECT_AQUA)
	{
		//
		Write_SenReg(0x0028,0x7000);
		Write_SenReg(0x002a,0x01EE);
		Write_SenReg(0x0f12,0x0004);
	}
	else
	{
		//
		//Write_SenReg(0x0028,0x7000);
		//Write_SenReg(0x002a,0x01EE);
		//Write_SenReg(0x0f12,0x0000);
	}
}
*/

void SetS5K6AAImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.
{

	U16 UsingConfig;
	if(samsung6AA_0x021C == 0x03)   //VGA condition
	{
		UsingConfig = 0x02d4;
	}
	else     //SXGA
		UsingConfig = 0x0288;
	if(bySnrImgDir == 1)
	{
		// negative image
		Write_SenReg(0x0028,0x7000);
		Write_SenReg(0x002a,UsingConfig);
		Write_SenReg(0x0f12,0x0001);
		Write_SenReg(0x002a,0x021e);
		Write_SenReg(0x0f12,0x0001);
	}
	//else if(bySnrImgDir&0x02)
	else if (bySnrImgDir == 2)
	{
		//
		Write_SenReg(0x0028,0x7000);
		Write_SenReg(0x002a,UsingConfig);
		Write_SenReg(0x0f12,0x0002);
		Write_SenReg(0x002a,0x021e);
		Write_SenReg(0x0f12,0x0001);
	}
	else if (bySnrImgDir == 3)
	{
		//
		Write_SenReg(0x0028,0x7000);
		Write_SenReg(0x002a,UsingConfig);
		Write_SenReg(0x0f12,0x0003);
		Write_SenReg(0x002a,0x021e);
		Write_SenReg(0x0f12,0x0001);
	}
	else
	{
		Write_SenReg(0x0028,0x7000);
		Write_SenReg(0x002a,UsingConfig);
		Write_SenReg(0x0f12,0x0000);
		Write_SenReg(0x002a,0x021e);
		Write_SenReg(0x0f12,0x0001);
	}
}
void SetS5K6AAOutputDim(U16 wWidth,U16 wHeight)
{
	wHeight = wHeight; // for delete warning

	switch(wWidth)
	{
	case 640:
		WriteSensorSettingWW((sizeof(config3_size_VGA_Setting)/sizeof(t_RegSettingWW)), config3_size_VGA_Setting);
		WriteSensorSettingWW((sizeof(config3_size_Setting)/sizeof(t_RegSettingWW)), config3_size_Setting);
		WriteSensorSettingWW((sizeof(gc_Sam6AA_VGA_Setting)/sizeof(t_RegSettingWW)), gc_Sam6AA_VGA_Setting);
		WriteSensorSettingWW((sizeof(Bison_AWB_Setting)/sizeof(t_RegSettingWW)), Bison_AWB_Setting); //only for Bison 2009-07-01
		break;
	case 320:
		WriteSensorSettingWW((sizeof(config3_size_QVGA_Setting)/sizeof(t_RegSettingWW)), config3_size_QVGA_Setting);
		WriteSensorSettingWW((sizeof(config3_size_Setting)/sizeof(t_RegSettingWW)), config3_size_Setting);
		WriteSensorSettingWW((sizeof(gc_Sam6AA_VGA_Setting)/sizeof(t_RegSettingWW)), gc_Sam6AA_VGA_Setting);
		WriteSensorSettingWW((sizeof(Bison_AWB_Setting)/sizeof(t_RegSettingWW)), Bison_AWB_Setting); //only for Bison 2009-07-01
		break;
	case 352:
		WriteSensorSettingWW((sizeof(config3_size_CIF_Setting)/sizeof(t_RegSettingWW)), config3_size_CIF_Setting);
		WriteSensorSettingWW((sizeof(config3_size_Setting)/sizeof(t_RegSettingWW)), config3_size_Setting);
		WriteSensorSettingWW((sizeof(gc_Sam6AA_VGA_Setting)/sizeof(t_RegSettingWW)), gc_Sam6AA_VGA_Setting);
		WriteSensorSettingWW((sizeof(Bison_AWB_Setting)/sizeof(t_RegSettingWW)), Bison_AWB_Setting); //only for Bison 2009-07-01
		break;
	case 176:
		WriteSensorSettingWW((sizeof(config3_size_QCIF_Setting)/sizeof(t_RegSettingWW)), config3_size_QCIF_Setting);
		WriteSensorSettingWW((sizeof(config3_size_Setting)/sizeof(t_RegSettingWW)), config3_size_Setting);
		WriteSensorSettingWW((sizeof(gc_Sam6AA_VGA_Setting)/sizeof(t_RegSettingWW)), gc_Sam6AA_VGA_Setting);
		WriteSensorSettingWW((sizeof(Bison_AWB_Setting)/sizeof(t_RegSettingWW)), Bison_AWB_Setting); //only for Bison 2009-07-01

		break;
	case 160:
		WriteSensorSettingWW((sizeof(config3_size_QQVGA_Setting)/sizeof(t_RegSettingWW)), config3_size_QQVGA_Setting);
		WriteSensorSettingWW((sizeof(config3_size_Setting)/sizeof(t_RegSettingWW)), config3_size_Setting);
		if(g_bIsHighSpeed)
		{
			//VD_DBGP0(("\n--vd code high  speed = ------------------------------------------"));
		}
		else //full speed
		{
			//VD_DBGP0(("\n--vd code full  speed = --------****************************"));
			WriteSensorSettingWW((sizeof(gc_Sam6AA_FS_Setting)/sizeof(t_RegSettingWW)), gc_Sam6AA_FS_Setting);
		}
		WriteSensorSettingWW((sizeof(gc_Sam6AA_VGA_Setting)/sizeof(t_RegSettingWW)), gc_Sam6AA_VGA_Setting);
		WriteSensorSettingWW((sizeof(Bison_AWB_Setting)/sizeof(t_RegSettingWW)), Bison_AWB_Setting); //only for Bison 2009-07-01
		break;

	case 1280:
		WriteSensorSettingWW((sizeof(gc_Sam6AA_SXGA_Setting)/sizeof(t_RegSettingWW)), gc_Sam6AA_SXGA_Setting);
		WriteSensorSettingWW((sizeof(Bison_AWB_Setting)/sizeof(t_RegSettingWW)), Bison_AWB_Setting);//only for Bison 2009-07-01

		break;
	default:
		WriteSensorSettingWW((sizeof(config3_size_VGA_Setting)/sizeof(t_RegSettingWW)), config3_size_VGA_Setting);
		WriteSensorSettingWW((sizeof(config3_size_Setting)/sizeof(t_RegSettingWW)), config3_size_Setting);
		WriteSensorSettingWW((sizeof(gc_Sam6AA_VGA_Setting)/sizeof(t_RegSettingWW)), gc_Sam6AA_VGA_Setting);
		WriteSensorSettingWW((sizeof(Bison_AWB_Setting)/sizeof(t_RegSettingWW)), Bison_AWB_Setting); //only for Bison 2009-07-01
		break;

	}
}

void CfgS5K6AAControlAttr(void)
{
	g_bySensorSize = SENSOR_SIZE_SXGA;
	g_bySensorSPFormat = SXGA_FRM | VGA_FRM;

	{
		memcpy(g_asOvCTT,gc_S5K6AA_CTT,sizeof(gc_S5K6AA_CTT));
	}

	{
		// AE backlight compensation parameter
//		g_byOVAEW_Normal = S5K6AA_AEW;
//		g_byOVAEB_Normal = S5K6AA_AEB;

//		g_byOVAEW_BLC = g_byOVAEW_Normal + 0x20;
//		g_byOVAEB_BLC = g_byOVAEB_Normal + 0x20;
	}

	{
		//g_bySV28VoltSel =  SV28_VOL_2V9;
		//	XBYTE[REG_TUNESVIO_SVA] =  SV28_VOL_2V9|SV18_VOL_1V8;
		// hemonel 2010-09-08: support 1.8V sensor IO
#if (_CHIP_ID_ & _RTS5840_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V79;
#elif (_CHIP_ID_ & _RTS5832_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V85;
#elif (_CHIP_ID_ & _RTS5829B_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V82;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V80;
#endif
	}

	{
		InitFormatFrameFps();
	}


}
#endif

