#include "Inc.h"

#ifdef RTS58XX_SP_YACD6A1C
OV_CTT_t code gc_YACD6A1C_CTT[3] =
{
	{2700,234,256,541},
	{5000,379,256,352},
	{7500,443,256,292},
};

code OV_FpsSetting_t  g_staYACD6A1CFpsSetting[]=
{
// FPS     ExtraDummyPixel clkrc 	   pclk

	{5,		(3514),		(0x19), 		20000000},
	{10,		(3514),		(0x32), 		40000000},
	{15,		(2344),		(0x32), 		40000000},
	{20,		(3514),		(0x64), 		80000000},
	{25,		(2812),		(0x64),		80000000},
	{30,		(2344),		(0x64),		80000000},
};

//1920*1080 MIPI 1lane
t_RegSettingWB code gc_YACD6A1C_Setting[] =
{
	{0x4200, 0x74}, // bit2:Grouped parameter hold mode enable
	{0x6100, 0x00}, // soft-standby mode 
	{0x6103, 0x01}, // soft-reset assertion
	{0x4042, 0xa0}, // [7]:otp disable, [5]:default init disable
	{0x5002, 0x56}, // pll bypass 
	{0x5005, 0x08}, // pll clkgen reset assertion

	{0x6200,0x00},  // fine integration time
	{0x6201,0x90},  // fine integration time
	{0x6202,0x04},  // coarse integration time
	{0x6203,0x6E},  // coarse integration time
	{0x6205,0xe0},  // analog gain
	{0x6340,0x04},  // frame length lines
	{0x6341,0x72},
	{0x6342,0x09},  // line length pck
	{0x6343,0x24},
	{0x6344,0x00},
	{0x6345,0x1E},
	{0x6346,0x00},
	{0x6347,0x20},
	{0x6348,0x07},
	{0x6349,0xB1},
	{0x634A,0x04},
	{0x634B,0x61},
	{0x634C,0x07},  // x output size 1936
	{0x634D,0x90},
	{0x634E,0x04},  // y output size 1090
	{0x634F,0x42},
	{0x6381,0x01},
	{0x6383,0x01},
	{0x6385,0x01},
	{0x6387,0x01},
	{0x6900,0x00},  // binning enable
	{0x6901,0x22},  // binning type
	{0x6B04,0x01},  // frame blc enable
	{0x8000,0x0C},  // tg_ctl1
	{0x8001,0x02},
	{0x8002,0x01},
	{0x8003,0x04},
	{0x8004,0x00},
	{0x8007,0x00},
	{0x800A,0x00},
	{0x800B,0x07},
	{0x800C,0x43},
	{0x800D,0x00},
	{0x800E,0x37},
	{0x8033,0x10},
	{0x8034,0x01},
	{0x8035,0x20},
	{0x8036,0x80},
	{0x8039,0x03},
	{0x8041,0x0B},
	{0x8042,0x79},
	{0x8043,0x6F},
	{0x8078,0x28},
	{0x8044,0xFF},
	{0x8045,0x7F},
	{0x8046,0x00},
	{0x8048,0x08},
	{0x8049,0xC8},
	{0x804A,0x39},
	{0x8079,0x05},
	{0x804B,0x00},
	{0x804E,0x55},
	{0x8051,0x33},
	{0x8053,0x03},
	{0x8054,0x50},
	{0x8058,0xB1},
	{0x8059,0x77},
	{0x805A,0x00},
	{0x805B,0xCC},
	{0x805C,0x09},
	{0x805D,0xFF},
	{0x807C,0x00},
	{0x805E,0x55},
	{0x8072,0x01},
	{0x8073,0x5E},
	{0x8074,0x01},
	{0x8075,0x5E},
	{0x8098,0x80},
	{0x8099,0x00},
	{0x809A,0x08},
	{0x809B,0x00},
	{0x80A0,0x01},  // line blc enable
	{0x80A1,0x5D},  //  blc_ctl2 ([6:4]:adp_dead_pxl_th_wgt => x0.375)
	{0x80A2,0x80},
	{0x80A3,0x00},
	{0x80A4,0x06},
	{0x80A5,0x00},
	{0x80A6,0x46},
	{0x80A7,0x20},  // blc_ctl8 ([7:5]:row_obp_dpc_rjt_opt)
	{0x80AC,0x02},
	{0x80AD,0xff},
	{0x80AE,0x08},  // col_blc_dead_pxl_th
	//{0x80B0,0x40},  // digital gb offset
	//{0x80B1,0x40},  // digital gr offset
	//{0x80B2,0x40},  // digital blue offset
	//{0x80B3,0x40},  // digital red offset
	{0x80C0,0x10},
	{0x80C1,0x66},  // curr_frm_obp_avg_wgt (x0.4)
	{0x80C2,0x10},  // frm_obp_avg_pga_wgt (x0.5)
	{0x8100,0x02},
	{0x8101,0x90},
	{0x8102,0x00},
	{0x8103,0x50},
	{0x8104,0x01},  // ramp_preset1_ful_pos
	{0x8105,0x10},
	{0x8106,0x00},  // ramp_preset1_ful_neg
	{0x8107,0x8A},
	{0x8108,0x04},  // ramp_preset2_ful_pos
	{0x8109,0x86},
	{0x810A,0x01},  // ramp_preset2_ful_neg
	{0x810B,0x80},
	{0x810C,0x01},  // ramp_clk_msk1_ful_pos
	{0x810D,0x0A},
	{0x810E,0x00},  // ramp_clk_msk1_ful_neg
	{0x810F,0x8A},
	{0x8110,0x04},  // ramp_clk_msk2_ful_pos
	{0x8111,0x80},
	{0x8112,0x01},  // ramp_clk_msk2_ful_neg
	{0x8113,0x80},
	{0x8130,0x00},  // ramp_rst_ofs_ful_pos
	{0x8131,0x74},
	{0x8132,0x01},  // ramp_rst_ofs_ful_neg
	{0x8133,0x10},
	{0x8134,0x04},  // ramp_sig_ofs_ful_pos
	{0x8135,0x86},
	{0x8136,0x00},  // ramp_sig_ofs_ful_neg
	{0x8137,0x74},
	{0x8138,0x01},  // ramp_rst_flg_ful_neg
	{0x8139,0x10},
	{0x813A,0x04},
	{0x813B,0x04},
	{0x813E,0x04},
	{0x8140,0x00},  // int_addr_ful_neg
	{0x8141,0x3E},
	{0x8142,0x06},
	{0x8144,0x00},  // int_en_ful_neg
	{0x8145,0x3C},
	{0x8146,0x00},
	{0x8148,0x00},
	{0x8149,0x00},
	{0x814A,0x0A},                             
	{0x814C,0x00},  // int_rx1_ful_neg
	{0x814D,0x38},
	{0x814E,0x00},
	{0x814F,0x0E},
	{0x8150,0x00},
	{0x8151,0x0E},
	{0x8152,0x00},
	{0x8153,0x0E},
	{0x8154,0x00},
	{0x8155,0x0E},
	{0x8156,0x0E},
	{0x8158,0x00},  // int_tx1_ful_neg
	{0x8159,0x34},
	{0x815A,0x00},
	{0x815B,0x12},
	{0x815C,0x00},
	{0x815D,0x12},
	{0x815E,0x01},
	{0x8160,0x04},
	{0x8161,0x8C},
	{0x8162,0x06},
	{0x8163,0x1A},
	{0x8166,0x02},
	{0x8168,0x04},  // scn_en_ful_neg
	{0x8169,0x88},  
	{0x816A,0x24},
	{0x816C,0x04},
	{0x816D,0x86},
	{0x816E,0x01},
	{0x816F,0x10},
	{0x8170,0x01},  // scn_tx1_ful_neg
	{0x8171,0x36},
	{0x8172,0x01},
	{0x8173,0xA2},
	{0x8174,0x01},
	{0x8175,0xA2},
	{0x8176,0x04},  // scn_tx3_ful_pos
	{0x8177,0x86},
	{0x8178,0x04},  // scn_tx3_ful_neg
	{0x8179,0x8A},
	{0x817A,0x08},  // scn_rx1_ful_neg
	{0x817C,0x00},
	{0x817D,0x26},
	{0x817E,0x00},
	{0x817F,0x08},
	{0x8180,0x00},
	{0x8181,0x08},
	{0x8182,0x04},  // scn_rx3_ful_pos
	{0x8183,0x86},
	{0x8184,0x04},  // scn_rx3_ful_neg
	{0x8185,0x8A},
	{0x8186,0x00},  // cds_rst_clp_ful_pos
	{0x8187,0x32},
	{0x8188,0x00},  // cds_rst_clp_ful_neg
	{0x8189,0x68},
	{0x818A,0x00},
	{0x818B,0x08},
	{0x818C,0x01},  // cds_rst_ful_neg
	{0x818D,0x0E},
	{0x818E,0x01},  // cds_sig_ful_pos
	{0x818F,0x12},
	{0x8190,0x04},  // cds_sig_ful_neg
	{0x8191,0x86},
	{0x8192,0x00},
	{0x8193,0x34},
	{0x8194,0x00},
	{0x8195,0x66},
	{0x8196,0x01},
	{0x8197,0x42},
	{0x8198,0x01},
	{0x8199,0x7E},
	{0x819A,0x00},  // cds_s2_ful_pos
	{0x819B,0x34},
	{0x819C,0x00},  // cds_s2_ful_neg
	{0x819D,0x52},
	{0x819E,0x00},  // cds_s3_ful_pos
	{0x819F,0x34},
	{0x81A0,0x00},  // cds_s3_ful_neg
	{0x81A1,0x66},
	{0x81A2,0x00},  // cds_s4_ful_pos
	{0x81A3,0x7C},
	{0x81A4,0x04},  // cds_s4_ful_neg
	{0x81A5,0x86},
	{0x81A6,0x00},
	{0x81A7,0x18},
	{0x81A8,0x00},
	{0x81A9,0x02},
	{0x81AA,0x00},
	{0x81AB,0x18},
	{0x81AC,0x00},
	{0x81AD,0x02},
	{0x81AE,0x00},
	{0x81AF,0x0A},
	{0x81B0,0x00},  // col_init_ful_neg
	{0x81B1,0x74},
	{0x81B2,0x04},  // col_load_ful_pos
	{0x81B3,0x86},
	{0x81B4,0x04},  // col_load_ful_neg
	{0x81B5,0x8E},
	{0x81B6,0x04},
	{0x81B7,0x86},
	{0x81B8,0x00},
	{0x81B9,0x04},
	{0x81BA,0x00},
	{0x81BB,0x0C},
	{0x81BC,0x04},
	{0x81BD,0x20},
	{0x81BE,0x00},
	{0x81BF,0x18},
	{0x81C0,0x00},
	{0x81C1,0x02},
	{0x81C2,0x00},  // ramp_dn1_ful_pos
	{0x81C3,0xC8},
	{0x81C4,0x00},  // ramp_dn1_ful_neg
	{0x81C5,0xA8},
	{0x81C6,0x01},  // ramp_dn2_ful_pos
	{0x81C7,0x10},
	{0x81C8,0x00},  // ramp_dn2_ful_neg
	{0x81C9,0xE8},
	{0x81CA,0x02},  // ramp_dn3_ful_pos
	{0x81CB,0xFE},
	{0x81CC,0x02},  // ramp_dn3_ful_neg
	{0x81CD,0x3E},
	{0x81CE,0x04},  // ramp_dn4_ful_pos
	{0x81CF,0x86},
	{0x81D0,0x03},  // ramp_dn4_ful_neg
	{0x81D1,0xBE},
	{0x81D2,0x00},  // ramp_lsb1c1_ful_pos
	{0x81D3,0xA8},
	{0x81D4,0x01},  // ramp_lsb1c1_ful_neg
	{0x81D5,0x10},
	{0x81D6,0x02},  // ramp_lsb1c2_ful_pos
	{0x81D7,0x3E},
	{0x81D8,0x04},  // ramp_lsb1c2_ful_neg
	{0x81D9,0x86},
	{0x81DA,0x00},  // ramp_lsb2c1_ful_pos
	{0x81DB,0xC8},
	{0x81DC,0x01},  // ramp_lsb2c1_ful_neg
	{0x81DD,0x10},
	{0x81DE,0x02},  // ramp_lsb2c2_ful_pos
	{0x81DF,0xFE},
	{0x81E0,0x04},  // ramp_lsb2c2_ful_neg
	{0x81E1,0x86},
	{0x81E2,0x00},  // ramp_lsb3c1_ful_pos
	{0x81E3,0xE8},
	{0x81E4,0x01},  // ramp_lsb3c1_ful_neg
	{0x81E5,0x10},
	{0x81E6,0x03},  // ramp_lsb3c2_ful_pos
	{0x81E7,0xBE},
	{0x81E8,0x04},  // ramp_lsb3c2_ful_neg
	{0x81E9,0x86},
	{0x81EA,0x00},
	{0x81EB,0xCE},
	{0x81EC,0x00},
	{0x81ED,0xD2},
	{0x8202,0x01},
	{0x8203,0x10},
	{0x8204,0x01},
	{0x8205,0x36},
	{0x8208,0x00},
	{0x8209,0x26},
	{0x820C,0x00},
	{0x820D,0x38},
	{0x8210,0x00},
	{0x8211,0x34},
	{0x8200,0x06},
	{0x8201,0x1A},
	{0x8206,0x08},
	{0x820A,0x0A},
	{0x820E,0x0E},

	{0x5000,0x03},  //clk_con      // parallel port disable : 0x0F=>0x03  
	{0x5003,0x64},  //Mode_pll_2   
	{0x5004,0xCF},  //Mode_pll_3   // PLL disable : 0x80,    PLL enable : 0xC3, [3:2]:ramp_clk_div(4)
	{0x5005,0x18},  //Mode_pll_4   
	{0x5006,0x33},  //Mode_pll_5   
	{0x5007,0x22},  //Mode_pll_6   
	{0x5008,0x18},  //Mode_pll_7   
	{0x500E,0x00},  //mode_io       // parallel port disable : 0x80=>0x00
	{0xA051,0x00},  //Fmt_ctrl2    
	{0xA054,0x00},  //FMT_x_start_h
	{0xA055,0x00},  //FMT_x_start_l
	{0xA056,0x00},  //FMT_y_start_h
	{0xA057,0x00},  //FMT_y_start_l
	{0xA000,0x00},  //Isp_mas_en - disable all isp block
	{0xA031,0x02},  //adpc_ctl2 - disable scan otp    
	{0xA096,0x88},  // ratio adjust of blue data and red data (x1)

	// MIPI settings
	{0x6800,0x80},  // CLK_Post(150ns) 
	{0x6801,0x30},  // HS_Prepare (66ns}
	{0x6802,0x3a},  // HS_Zero (140ns)
	{0x6803,0x4a},  // HS_Trail (80ns)
	{0x6804,0x4a},  // CLK_Trail (80ns)
	{0x6805,0x30},  // CLK_Prepare (65ns)
	{0x6806,0xca},  // CLK Zero (250ns)
	{0x6807,0x30},  // CLK,DATA LPX (60ns)
	{0xB030,0x00},  // hs_delay_h
	{0xB031,0x03},  // hs_delay_m
	{0xB032,0x66},  // hs_delay_l (7.4us)

	// Analog Setting Adjustment
	{0x8039,0x0F},  //          bias_sample_off
	{0x803C,0x0C},  // 20121204 pxl_pwr_ctl_off
	//{0x8041,0x00},  //          adc_input_range  - 600mv (actually 640mv, offset 40mv)
	//{0x8041,0x03},  //          adc_input_range  - 700mv (actually 750mv, offset 50mv)
	{0x8041,0x02},  //          adc_input_range  - 636mv (actually 680mv, offset 50mv)
	{0x8043,0x5F},  //          ramp_dc_offset
	{0x8047,0x00},  //          ramp signal offset
	{0x804B,0x03},  //          bias_ctl_en
	{0x804E,0x00},  //          pixel bias
	{0x8051,0x03},  // 20121204 amp1 bias
	{0x8053,0x00},  //          amp2 bias
	{0x8054,0x50},  // 20121215 clamp level
	{0x8078,0x20},  //          ramp_filter bypass
	//{0x819D,0x52},  // 20121204 s2_neg           - black sun char. enhancement,  adc range error enhancement
	{0x81A1,0x57},  // 20121204 s3_neg 

	{0x5002,0x57},  //Mode_pll_1   // PLL disable : 0x56,    PLL enable : 0x57
	{0x6100,0x01},  //mode_select 


};

static OV_FpsSetting_t*  GetOvFpsSetting(U8 Fps, OV_FpsSetting_t staOvFpsSetting[], U8 byArrayLen)
{
	U8 i;
	U8 Idx;

	ISP_MSG((" array size = %bd\n", byArrayLen));

	if(g_bIsHighSpeed)
	{
		Idx=2; //8fps for uxga, 10 fps for vga
	}
	else
	{
		Idx = 0;
	}

	for(i=0; i< byArrayLen; i++)
	{
		ISP_MSG((" fps i = %bd\n",i));
		if(staOvFpsSetting[i].byFps ==Fps)
		{
			Idx=i;
			break;
		}
	}
	ISP_MSG((" get fps idx = %bd\n",Idx));
	return &staOvFpsSetting[Idx];
}

static U8 MapSnrGlbGain2SnrRegSetting(float fGain)
{
	U8  bySensorGain=0;
	
	if(fGain>(float)8.0)    //Use Analog Gain here max=8
	{
		bySensorGain = 0;	//=256/wGain-32
	}
	else//max analog gain is 8 times
	{
		bySensorGain = (U8)((float)256/fGain)-32;
	}
	
	return bySensorGain;
}

float SnrRegSetting2SnrGlbGain(U8 bySnrRegGain)
{
	return 256.0/(32.0+(float)bySnrRegGain);
}

//this function is also called during Probe stage to calculate the transfer_size/LWM/HWM
void GetSensorPclkHsync(U16 wSensorSPFormat,U8 byFps)
{
	OV_FpsSetting_t *pYACD6A1C_FpsSetting;

	wSensorSPFormat =wSensorSPFormat;
	
	pYACD6A1C_FpsSetting=GetOvFpsSetting(byFps, g_staYACD6A1CFpsSetting, sizeof(g_staYACD6A1CFpsSetting)/sizeof(OV_FpsSetting_t));
	g_wSensorHsyncWidth = pYACD6A1C_FpsSetting->wExtraDummyPixel; // this for manual exposure
	g_dwPclk = pYACD6A1C_FpsSetting->dwPixelClk;		// this for scale speed
}

void YACD6A1CSetFormatFps(U16 SetFormat, U8 Fps)
{
	U16 wTemp;
	OV_FpsSetting_t *pYACD6A1C_FpsSetting;

	// hemonel 2010-04-13: for delete complier warning
	SetFormat = SetFormat;

	pYACD6A1C_FpsSetting=GetOvFpsSetting(Fps, g_staYACD6A1CFpsSetting, sizeof(g_staYACD6A1CFpsSetting)/sizeof(OV_FpsSetting_t));

	// initial all register setting
	WriteSensorSettingWB(sizeof(gc_YACD6A1C_Setting)/3, gc_YACD6A1C_Setting);

	Write_SenReg(0x5003, pYACD6A1C_FpsSetting->byClkrc);

	wTemp = pYACD6A1C_FpsSetting->wExtraDummyPixel;
	Write_SenReg(0x6343, INT2CHAR(wTemp, 0)); //Total  pixel Value  LSB
	Write_SenReg(0x6342, INT2CHAR(wTemp, 1)); //Total  pixel Value  MSB

	//g_dwPclk = pYACD6A1C_FpsSetting->dwPixelClk;		// this for scale speed
	GetSensorPclkHsync(HD1080P_FRM,Fps);

	g_wAEC_LineNumber = 1138;	// this for AE insert dummy line algothrim
	g_wAECExposureRowMax = 1138 -4;	// this for max exposure time //20110524
	g_wSensorWidthBefBLC = 1920;
	g_wSensorHeightBefBLC = 1080;

#ifdef _MIPI_EXIST_
	//-----------------------------Dphy Setting--------------------------------
	SetMipiDphy(MIPI_DATA_LANE0_EN, MIPI_DATA_FORMAT_RAW10 , MIPI_DATA_TYPE_RAW10, HSTERM_EN_TIME_33);
#endif
	return;

}

void CfgYACD6A1CControlAttr(void)
{
	U8 i;

	g_bySensorSize = SENSOR_SIZE_FHD;
	g_wSensorSPFormat = HD1080P_FRM;

	{
		memcpy(g_asOvCTT, gc_YACD6A1C_CTT,sizeof(gc_YACD6A1C_CTT));
	}

	{
		// hemonel 2010-09-08: support 1.8V sensor IO
#if (_CHIP_ID_ & _RTS5840_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V79;
#elif (_CHIP_ID_ & _RTS5832_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V85;
#elif (_CHIP_ID_ & _RTS5829B_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V82;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V80;
#endif
	}

	{
		//====== resolution  setting ===========
		// -- preview ---
		g_aVideoFormat[0].byaVideoFrameTbl[0] = 12;		// resolution number
		g_aVideoFormat[0].byaVideoFrameTbl[1]=F_SEL_640_480;
		g_aVideoFormat[0].byaVideoFrameTbl[2]=F_SEL_320_180;
		g_aVideoFormat[0].byaVideoFrameTbl[3]=F_SEL_320_240;
		g_aVideoFormat[0].byaVideoFrameTbl[4]=F_SEL_352_288;
		g_aVideoFormat[0].byaVideoFrameTbl[5]=F_SEL_424_240;
		g_aVideoFormat[0].byaVideoFrameTbl[6]=F_SEL_640_360;
		g_aVideoFormat[0].byaVideoFrameTbl[7]=F_SEL_640_400;
		g_aVideoFormat[0].byaVideoFrameTbl[8]=F_SEL_848_480;
		g_aVideoFormat[0].byaVideoFrameTbl[9]=F_SEL_960_540;
		g_aVideoFormat[0].byaVideoFrameTbl[10]=F_SEL_1280_720;
		g_aVideoFormat[0].byaVideoFrameTbl[11]=F_SEL_1280_800;
		g_aVideoFormat[0].byaVideoFrameTbl[12]=F_SEL_1920_1080;
		//--still image--
		g_aVideoFormat[0].byaStillFrameTbl[0] = 5;	// resolution number
		g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_1920_1080;
		g_aVideoFormat[0].byaStillFrameTbl[2]=F_SEL_1280_720;
		g_aVideoFormat[0].byaStillFrameTbl[3]=F_SEL_320_240;
		g_aVideoFormat[0].byaStillFrameTbl[4]=F_SEL_352_288;
		g_aVideoFormat[0].byaStillFrameTbl[5]=F_SEL_640_480;

		//====== fps  setting ===========
		for(i=0; i<10; i++)
			{
				/*
					F_SEL_640_480    (0)
					F_SEL_160_120    (1)
					F_SEL_176_144    (2)
					F_SEL_320_180	(3)
					F_SEL_320_200    (4)
					F_SEL_320_240    (5)
					F_SEL_352_288    (6)
					F_SEL_424_240	(7)
					F_SEL_640_360	(8)
					F_SEL_640_400    (9)
				*/
				g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_15|FPS_20|FPS_25|FPS_30;
			}
			for(i=10; i<20; i++)
			{
				/*
					F_SEL_800_600    (10)
					F_SEL_848_480	(11)
					F_SEL_960_540	 (12)
					F_SEL_1024_768   (13)
					F_SEL_1280_720   (14)
					F_SEL_1280_800   (15)
					F_SEL_1280_960   (16)
					F_SEL_1280_1024  (17)
				*/
				g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_10;
			}

		//====== format type  setting ===========
		g_aVideoFormat[0].byFormatType = FORMAT_TYPE_YUY2;

#ifdef _ENABLE_MJPEG_
		// copy YUY2 setting to MJPEG setting
		memcpy(&(g_aVideoFormat[1]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

		// modify MJPEG FPS setting
		for(i=0;i<20;i++)
		{
			/*
			 F_SEL_800_600    (7)
			 F_SEL_960_720    (8)
			 F_SEL_1024_768   (9)
			 F_SEL_1280_720   (10)
			 F_SEL_1280_800   (11)
			 F_SEL_1280_960   (12)
			 F_SEL_1280_1024  (13)
			 F_SEL_1600_1200  (14)
			 F_SEL_1920_1080  (15)
			 F_SEL_1600_900	 (16)
			 */
			g_aVideoFormat[1].waFrameFpsBitmap[i] = (FPS_10|FPS_15|FPS_20|FPS_25|FPS_30);
		}	
		
		// modify MJPEG format type
		g_aVideoFormat[1].byFormatType = FORMAT_TYPE_MJPG;

#endif

#ifdef ENABLE_M420_FMT
		memcpy(&(g_aVideoFormat[2]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));
		g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_720] =FPS_11|FPS_5;
		g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1920_1080] =FPS_11|FPS_5;
		g_aVideoFormat[2].byFormatType = FORMAT_TYPE_M420;
#endif
	}

	InitYACD6A1CIspParams();
}

void SetYACD6A1CIntegrationTime(U16 wEspline)
{
	U16 wFrameLenLines;
	U16 wExpMax = g_wAECExposureRowMax-2;

	if(wEspline > wExpMax)
	{
		wFrameLenLines = wEspline + 20;
	}
	else
	{
		wFrameLenLines = g_wAEC_LineNumber;
	}

	if (wFrameLenLines%2==1)
	{
		wFrameLenLines++;
	}

	//-----------Write  frame length or dummy lines------------
	Write_SenReg(0x6341, INT2CHAR(wFrameLenLines, 0));
	Write_SenReg(0x6340, INT2CHAR(wFrameLenLines, 1));

	//-----------Write Exposuretime setting-----------
	Write_SenReg(0x6203, INT2CHAR(wEspline, 0));	// change exposure value low
	Write_SenReg(0x6202, INT2CHAR(wEspline, 1));	// change exposure value high

	//----------- write gain setting-------------------
	Write_SenReg(0x6205, 0x0f);	// fixed gain at manual exposure control
	Write_SenReg(0x6204, 0);

}

void SetYACD6A1CGain(float fGain)
{
}

void SetYACD6A1CImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.
{
	WaitFrameSync(ISP_INT1,ISP_DATA_START_INT);

	bySnrImgDir ^= 0x01; 	// OV9726 output mirrored image at default, so need mirror the image for normal output
	Write_SenReg_Mask(0x6101, bySnrImgDir, 0x03);

	WaitFrameSync(ISP_INT1,ISP_DATA_END_INT);

	switch(bySnrImgDir)
	{
		case 0:
#ifdef _MIPI_EXIST_		
		SetBLCWindowStart(0, 1);
#endif			
		break;
	case 1:
#ifdef _MIPI_EXIST_			
		SetBLCWindowStart(1, 1);
#endif			
		break;
	case 2:
#ifdef _MIPI_EXIST_			
		SetBLCWindowStart(0, 0);
#endif			
		break;
	default:
	case 3:
#ifdef _MIPI_EXIST_			
		SetBLCWindowStart(1, 0);
#endif
		break;
	}
}

void SetYACD6A1CExposuretime_Gain(float fExpTime, float fTotalGain)
{

	U16 wExposureRows_floor;
	U8  byGainRegSetting; //data
	float fSnrGlbGain;
	float fExposureRows;
	U16 wSetDummy;
	float fTemp;
	U16 wExposurePixels;

	byGainRegSetting = MapSnrGlbGain2SnrRegSetting(fTotalGain);
	
	fSnrGlbGain = SnrRegSetting2SnrGlbGain(byGainRegSetting);
	
	fExposureRows = fExpTime/g_fSensorRowTimes;	//unit : 1row
	wExposureRows_floor = (U16)(fExposureRows);
	fTemp = fExpTime-((float)wExposureRows_floor*(g_fSensorRowTimes)); //fine integration time;unit:  us
	wExposurePixels =(U16)(fTemp * (float)(g_dwPclk/10000)) ; //unit: pixels

	// write exposure, gain, dummy line
	if(fabs(g_fCurExpTime-fExpTime) > 0.0001)
	{
		// set dummy
		wSetDummy = g_wAFRInsertDummylines + g_wAEC_LineNumber;

		// group register hold
		Write_SenReg(0x6104, 0x01);

		//-----------Write  frame length or dummy lines------------
		Write_SenReg(0x6341, INT2CHAR(wSetDummy, 0));
		Write_SenReg(0x6340, INT2CHAR(wSetDummy, 1));

		//-----------Write Exposuretime setting-----------
		Write_SenReg(0x6203, INT2CHAR(wExposureRows_floor, 0));	// change exposure value low
		Write_SenReg(0x6202, INT2CHAR(wExposureRows_floor, 1));	// change exposure value high

		Write_SenReg(0x6201, INT2CHAR(wExposurePixels, 0));	// change exposure value low
		Write_SenReg(0x6200, INT2CHAR(wExposurePixels, 1));	// change exposure value high

		//----------- write gain setting-------------------
		Write_SenReg(0x6205,byGainRegSetting);		//Only support 8x gain one byte
		//Write_SenReg(0x0205, INT2CHAR(wGainRegSetting, 0));
		//Write_SenReg(0x0204, INT2CHAR(wGainRegSetting, 1));

		SetISPAEGain(fTotalGain, fSnrGlbGain, 1);

		// group register release
		Write_SenReg(0x6104, 0);
		g_fCurExpTime = fExpTime;
	}
	else
	{
		// group register hold
		Write_SenReg(0x6104, 1);
		wSetDummy = g_wAFRInsertDummylines + g_wAEC_LineNumber;

		//-----------Write  frame length or dummy lines------------
		Write_SenReg(0x6341, INT2CHAR(wSetDummy, 0));
		Write_SenReg(0x6340, INT2CHAR(wSetDummy, 1));

		// write gain setting
		Write_SenReg(0x6205,byGainRegSetting);		//Only support 8x gain one byte
		//Write_SenReg(0x0205, INT2CHAR(wGainRegSetting, 0));
		//Write_SenReg(0x0204, INT2CHAR(wGainRegSetting, 1));

		SetISPAEGain(fTotalGain, fSnrGlbGain, 1);

		// group register release
		Write_SenReg(0x6104, 0);
	}

}

void YACD6A1C_POR(void)
{
#ifdef _MIPI_EXIST_
	Init_MIPI();
#else
	Init_CCS();
#endif
	ENTER_SENSOR_RESET();
	// power on
	SensorPowerControl(SWITCH_ON,EN_DELAYTIME);
	uDelay(2);

	// clock select 24M
	if( g_bySSCEnable== TRUE)
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_SSC_24M);
	}
	else
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_96M|CCS_CLK_DIVIDER_4);
	}

	EnableSensorHCLK();
	
	WaitTimeOut_Delay(2);	
	LEAVE_SENSOR_RESET();
	
	g_bySensorIsOpen = SENSOR_OPEN;
	WaitTimeOut_Delay(1);
}

void InitYACD6A1CIspParams()
{
	// AWB initial gain
	//use D50 R/G/B gain as AWB initial value
	g_wAWBRGain_Last= 0x17C;
	g_wAWBGGain_Last= 0x100;
	g_wAWBBGain_Last= 0x226;
	g_wProjectGR = g_wAWBRGain_Last;
	g_wProjectGB = g_wAWBBGain_Last;

	// Special ISP
	g_bySaturation_Def =64;
	g_byContrast_Def = 32; //Neil Tuning at chicony
	g_byTgamma_rate_max=63;
	g_byTgamma_rate_min =20;

	//g_wDynamicISPEn = 0;
	g_wDynamicISPEn = DYNAMIC_LSC_CT_EN| DYNAMIC_LSC_EN | DYNAMIC_SHARPNESS_EN | DYNAMIC_SHARPPARAM_EN | DYNAMIC_GAMMA_EN | DYNAMIC_CCM_CT_EN;
}

void SetYACD6A1CDynamicISP(U8 byAEC_Gain)
{
	byAEC_Gain = byAEC_Gain;
}

void SetYACD6A1CDynamicISP_AWB(U16 wColorTempature)
{
	wColorTempature = wColorTempature;
}
#endif
