#ifndef _YACBAC1S_H_
#define _YACBAC1S_H_

#define YACBAC1S_AE_TARGET 0x38

extern OV_CTT_t code gc_YACBAC1S_CTT[3];
void YACBAC1SSetFormatFps(U8 SetFormat, U8 byFps);
void SetYACBAC1SImgDir(U8 bySnrImgDir) ;
void CfgYACBAC1SControlAttr(void);
#ifndef _USE_BK_HSBC_ADJ_
void SetYACBAC1SBrightness(S16 swSetValue);
void	SetYACBAC1SContrast(U16 wSetValue);
void	SetYACBAC1SSaturation(U16 wSetValue);
void	SetYACBAC1SHue(S16 swSetValue);
#endif
void	SetYACBAC1SSharpness(U8 bySetValue);
void SetYACBAC1SOutputDim(U16 wWidth,U16 wHeight);
void SetYACBAC1SPwrLineFreq(U8 byFPS, U8 byLightFrq);
void SetYACBAC1SWBTemp(U16 wRgain, U16 wGgain, U16 wBgain);
OV_CTT_t GetYACBAC1SAwbGain(void);
void SetYACBAC1SWBTempAuto(U8 bySetValue);
void SetYACBAC1SBackLightComp(U8 bySetValue);
U16  GetYACBAC1SAEGain();
void SetYACBAC1SIntegrationTimeAuto(U8 bySetValue);
void SetYACBAC1SIntegrationTime(U32 dwSetValue);
void SetYACBAC1SGain(float fGain);
#endif

