#include "Inc.h"

#ifdef RTS58XX_SP_MI1040

OV_CTT_t code gc_MI1040_CTT[3] =
{
	{2886,280,276,571},
	{4141,346,256,449},
	{6892,386,256,272},
};

code MI1040_FpsSetting_t  g_staMI1040SXGAFpsSetting[]=
{
//    FPS,    	M_N,     	   P, HorizWidth,VertHeight,fine_integ_time_min_max,      PixelClk,    max_frame_rate,min
	{  5,   0x0110, 0x0F00,     0x0951,     0x03EE,     0x00DB,     0x08CE,     (  0xB71B00),   0x0500 , 0x0500},
	{  9,   0x0110, 0x0B00,     0x06E7,     0x03EE,     0x00DB,     0x0664,     (  0xF42400),   0x0900 , 0x0780},
	{10,   0x0110, 0x0B00,     0x0636,     0x03EE,     0x00DB,     0x05B3,     (  0xF42400),   0x0A01 , 0x0780},
	{15,   0x0110, 0x0700,     0x0636,     0x03EE,     0x00DB,     0x05B3,     (0x16E3600),   0x0F01 , 0x0780},
	{20,   0x0110, 0x0500,     0x0636,     0x03EE,     0x00DB,     0x05B3,     (0x1E84800),   0x1401 , 0x0780},
	{25,   0x0114, 0x0500,     0x0636,     0x03EE,     0x00DB,     0x05B3,     (0x2625A00),   0x1902 , 0x0780},
	{30,   0x0110, 0x0300,     0x0636,     0x03EE,     0x00DB,     0x05B3,     (0x2DC6C00),   0x1E02 , 0x0780},
};

/*
// CCM
U8 code g_MI1040_CCM_D65[21] =
{ 15,2, 202,8, 69,8,
   79,8, 84,1, 5,8,
   59,8, 131,8,189,1,
                 0,0,0 };

U8 code g_MI1040_CCM_A[21] =
{ 15,2, 202,8, 69,8,
   79,8, 84,1, 5,8,
   59,8, 131,8,189,1,
                 0,0,0 };

// LSC
U8 code gc_byLSC_RGB_Curve_MI1040[3][48] =
{
{128,   0, 130,   0, 134,   0, 140,   0, 148,   0, 159,   0, 173,   0, 188,   0, 205,   0, 225,   0, 250,   0,  29,   1,  94,   1,  94,   1,  94,   1,  94,   1,  94,   1,  94,   1,  94,   1,  94,   1,  94,   1,  94,   1,  94,   1, },
{129,   0, 131,   0, 136,   0, 142,   0, 151,   0, 162,   0, 176,   0, 189,   0, 204,   0, 224,   0, 252,   0,  34,   1, 107,   1, 107,   1, 107,   1, 107,   1, 107,   1, 107,   1, 107,   1, 107,   1, 107,   1, 107,   1, 107,   1, },
{128,   0, 130,   0, 133,   0, 138,   0, 144,   0, 152,   0, 162,   0, 172,   0, 184,   0, 199,   0, 218,   0, 245,   0,  50,   1,  50,   1,  50,   1,  50,   1,  50,   1,  50,   1,  50,   1,  50,   1,  50,   1,  50,   1,  50,   1, },
};
U16 code gc_wLSC_Center_Offset_MI1040[6]={592,489,592,489,592,489};
U8 code gc_bymLSC_Grid_MI1040 = 2;
U8 code gc_bymLSC_Matrix_MI1040[3][224] ={0};

// Gamma
U8 code g_byGamma_MI1040_nor[28] =   {0, 12, 25, 37, 48, 57, 65, 74, 82, 94, 105, 115, 124, 132, 139, 147, 154, 161, 167, 173, 179, 190, 200, 210, 221, 230, 239, 247}; //[Albert, 2011/06/09]
U8 code g_byGamma_MI1040_lowlux[28] =   {0, 11, 19, 25, 31, 37, 43, 48, 53, 63, 72, 81, 90, 98, 106, 114, 122, 130, 137, 144, 152, 166, 179, 193, 206, 218, 231, 243}; //[Albert, 2011/06/09]

// AWB gain
U8 code g_MI1040_AWB_Rough_GainR[6] ={33,37,43,45,48,52};
U8 code g_MI1040_AWB_Rough_GainB[6] ={66,56,56,45,34,23};


*/

t_VarSettingWW code gc_MI1040_Step3_Recommended[] =
{
//[**********Step3*************]
//[Step3-Recommended]
//LOAD=Sensor optimization
//LOAD=Errata item 1
//LOAD=Errata item 2


//[Sensor optimization]
	{0x316A, 0x8270, 2},
	{0x316C, 0x8270, 2},
	{0x3ED0, 0x3605, 2},
	{0x3ED2, 0x77CF, 2},
	{0x316E, 0x8233, 2},
	{0x3180, 0x87FF, 2},
	{0x30D4, 0x6080,2},
	{0xA802, 0x0008, 2},

//[Errata item y]
//[Errata item z]

//[Errata item 1]
	{0x3E14, 0xFF39, 2},
	{0x301a, 0x0234, 2},

//[Errata item 2]
//LOAD=Load Patch 02FF
//LOAD=Apply Patch 02FF
};

U16 code gc_MI1040_Patch_02FF[] =
{
//REG_BURST= 0xd000,
	0x70cf, 0xffff, 0xc5d4, 0x903a, 0x2144, 0x0c00, 0x2186, 0x0ff3, 0xb844, 0xb948, 0xe082, 0x20cc, 0x80e2, 0x21cc, 0x80a2, 0x21cc, 0x80e2, 0xf404, 0xd801, 0xf003, 0xd800, 0x7ee0, 0xc0f1, 0x08ba,
//REG_BURST= 0xd030,
	0x0600, 0xc1a1, 0x76cf, 0xffff, 0xc130, 0x6e04, 0xc040, 0x71cf, 0xffff, 0xc790, 0x8103, 0x77cf, 0xffff, 0xc7c0, 0xe001, 0xa103, 0xd800, 0x0c6a, 0x04e0, 0xb89e, 0x7508, 0x8e1c, 0x0809, 0x0191,
//REG_BURST= 0xd060,
	0xd801, 0xae1d, 0xe580, 0x20ca, 0x0022, 0x20cf, 0x0522, 0x0c5c, 0x04e2, 0x21ca, 0x0062, 0xe580, 0xd901, 0x79c0, 0xd800, 0x0be6, 0x04e0, 0xb89e, 0x70cf, 0xffff, 0xc8d4, 0x9002, 0x0857, 0x025e,
//REG_BURST= 0xd090,
	0xffdc, 0xe080, 0x25cc, 0x9022, 0xf225, 0x1700, 0x108a, 0x73cf, 0xff00, 0x3174, 0x9307, 0x2a04, 0x103e, 0x9328, 0x2942, 0x7140, 0x2a04, 0x107e, 0x9349, 0x2942, 0x7141, 0x2a04, 0x10be, 0x934a,
//REG_BURST= 0xd0c0,
	0x2942, 0x714b, 0x2a04, 0x10be, 0x130c, 0x010a, 0x2942, 0x7142, 0x2250, 0x13ca, 0x1b0c, 0x0284, 0xb307, 0xb328, 0x1b12, 0x02c4, 0xb34a, 0xed88, 0x71cf, 0xff00, 0x3174, 0x9106, 0xb88f, 0xb106,
//REG_BURST= 0xd0f0,
	0x210a, 0x8340, 0xc000, 0x21ca, 0x0062, 0x20f0, 0x0040, 0x0b02, 0x0320, 0xd901, 0x07f1, 0x05e0, 0xc0a1, 0x78e0, 0xc0f1, 0x71cf, 0xffff, 0xc7c0, 0xd840, 0xa900, 0x71cf, 0xffff, 0xd02c, 0xd81e,
//REG_BURST= 0xd120,
	0x0a5a, 0x04e0, 0xda00, 0xd800, 0xc0d1, 0x7ee0

};
/*
U16 code gc_MI1040_Patch_03FF[] ={
//REG_BURST= 0xd12c,
0x70cf, 0xffff, 0xc5d4, 0x903a, 0x2144, 0x0c00, 0x2186, 0x0ff3, 0xb844, 0x262f, 0xf008, 0xb948, 0x21cc, 0x8021, 0xd801, 0xf203, 0xd800, 0x7ee0, 0xc0f1, 0x71cf, 0xffff, 0xc610, 0x910e, 0x208c,
//REG_BURST=0xd15c,
0x8014, 0xf418, 0x910f, 0x208c, 0x800f, 0xf414, 0x9116, 0x208c, 0x800a, 0xf410, 0x9117, 0x208c, 0x8807, 0xf40c, 0x9118, 0x2086, 0x0ff3, 0xb848, 0x080d, 0x0090, 0xffea, 0xe081, 0xd801, 0xf203,
//REG_BURST=0xd18c,
0xd800, 0xc0d1, 0x7ee0, 0x78e0, 0xc0f1, 0x71cf, 0xffff, 0xc610, 0x910e, 0x208c, 0x800a, 0xf418, 0x910f, 0x208c, 0x8807, 0xf414, 0x9116, 0x208c, 0x800a, 0xf410, 0x9117, 0x208c, 0x8807, 0xf40c,
//REG_BURST=0xd1bc,
0x9118, 0x2086, 0x0ff3, 0xb848, 0x080d, 0x0090, 0xffd9, 0xe080, 0xd801, 0xf203, 0xd800, 0xf1df, 0x9040, 0x71cf, 0xffff, 0xc5d4, 0xb15a, 0x9041, 0x73cf, 0xffff, 0xc7d0, 0xb140, 0x9042, 0xb141,
//REG_BURST=0xd1ec,
0x9043, 0xb142, 0x9044, 0xb143, 0x9045, 0xb147, 0x9046, 0xb148, 0x9047, 0xb14b, 0x9048, 0xb14c, 0x9049, 0x1958, 0x0084, 0x904a, 0x195a, 0x0084, 0x8856, 0x1b36, 0x8082, 0x8857, 0x1b37, 0x8082,
//REG_BURST=0xd21c,
0x904c, 0x19a7, 0x009c, 0x881a, 0x7fe0, 0x1b54, 0x8002, 0x78e0, 0x71cf, 0xffff, 0xc350, 0xd828, 0xa90b, 0x8100, 0x01c5, 0x0320, 0xd900, 0x78e0, 0x220a, 0x1f80, 0xffff, 0xd4e0, 0xc0f1, 0x0811,
//REG_BURST=0xd24c,
0x0051, 0x2240, 0x1200, 0xffe1, 0xd801, 0xf006, 0x2240, 0x1900, 0xffde, 0xd802, 0x1a05, 0x1002, 0xfff2, 0xf195, 0xc0f1, 0x0e7e, 0x05c0, 0x75cf, 0xffff, 0xc84c, 0x9502, 0x77cf, 0xffff, 0xc344,
//REG_BURST=0xd27c,
0x2044, 0x008e, 0xb8a1, 0x0926, 0x03e0, 0xb502, 0x9502, 0x952e, 0x7e05, 0xb5c2, 0x70cf, 0xffff, 0xc610, 0x099a, 0x04a0, 0xb026, 0x0e02, 0x0560, 0xde00, 0x0a12, 0x0320, 0xb7c4, 0x0b36, 0x03a0,
//REG_BURST=0xd2ac,
0x70c9, 0x9502, 0x7608, 0xb8a8, 0xb502, 0x70cf, 0x0000, 0x5536, 0x7860, 0x2686, 0x1ffb, 0x9502, 0x78c5, 0x0631, 0x05e0, 0xb502, 0x72cf, 0xffff, 0xc5d4, 0x923a, 0x73cf, 0xffff, 0xc7d0, 0xb020,
//REG_BURST=0xd2dc,
0x9220, 0xb021, 0x9221, 0xb022, 0x9222, 0xb023, 0x9223, 0xb024, 0x9227, 0xb025, 0x9228, 0xb026, 0x922b, 0xb027, 0x922c, 0xb028, 0x1258, 0x0101, 0xb029, 0x125a, 0x0101, 0xb02a, 0x1336, 0x8081,
//REG_BURST=0xd30c,
0xa836, 0x1337, 0x8081, 0xa837, 0x12a7, 0x0701, 0xb02c, 0x1354, 0x8081, 0x7fe0, 0xa83a, 0x78e0, 0xc0f1, 0x0dc2, 0x05c0, 0x7608, 0x09bb, 0x0010, 0x75cf, 0xffff, 0xd4e0, 0x8d21, 0x8d00, 0x2153,
//REG_BURST=0xd33c,
0x0003, 0xb8c0, 0x8d45, 0x0b23, 0x0000, 0xea8f, 0x0915, 0x001e, 0xff81, 0xe808, 0x2540, 0x1900, 0xffde, 0x8d00, 0xb880, 0xf004, 0x8d00, 0xb8a0, 0xad00, 0x8d05, 0xe081, 0x20cc, 0x80a2, 0xdf00,
//REG_BURST=0xd36c,
0xf40a, 0x71cf, 0xffff, 0xc84c, 0x9102, 0x7708, 0xb8a6, 0x2786, 0x1ffe, 0xb102, 0x0b42, 0x0180, 0x0e3e, 0x0180, 0x0f4a, 0x0160, 0x70c9, 0x8d05, 0xe081, 0x20cc, 0x80a2, 0xf429, 0x76cf, 0xffff,
//REG_BURST=0xd39c,
0xc84c, 0x082d, 0x0051, 0x70cf, 0xffff, 0xc90c, 0x8805, 0x09b6, 0x0360, 0xd908, 0x2099, 0x0802, 0x9634, 0xb503, 0x7902, 0x1523, 0x1080, 0xb634, 0xe001, 0x1d23, 0x1002, 0xf00b, 0x9634, 0x9503,
//REG_BURST=0xd3cc,
0x6038, 0xb614, 0x153f, 0x1080, 0xe001, 0x1d3f, 0x1002, 0xffa4, 0x9602, 0x7f05, 0xd800, 0xb6e2, 0xad05, 0x0511, 0x05e0, 0xd800, 0xc0f1, 0x0cfe, 0x05c0, 0x0a96, 0x05a0, 0x7608, 0x0c22, 0x0240,
//REG_BURST=0xd3fc,
0xe080, 0x20ca, 0x0f82, 0x0000, 0x190b, 0x0c60, 0x05a2, 0x21ca, 0x0022, 0x0c56, 0x0240, 0xe806, 0x0e0e, 0x0220, 0x70c9, 0xf048, 0x0896, 0x0440, 0x0e96, 0x0400, 0x0966, 0x0380, 0x75cf, 0xffff,
//REG_BURST=0xd42c,
0xd4e0, 0x8d00, 0x084d, 0x001e, 0xff47, 0x080d, 0x0050, 0xff57, 0x0841, 0x0051, 0x8d04, 0x9521, 0xe064, 0x790c, 0x702f, 0x0ce2, 0x05e0, 0xd964, 0x72cf, 0xffff, 0xc700, 0x9235, 0x0811, 0x0043,
//REG_BURST=0xd45c,
0xff3d, 0x080d, 0x0051, 0xd801, 0xff77, 0xf025, 0x9501, 0x9235, 0x0911, 0x0003, 0xff49, 0x080d, 0x0051, 0xd800, 0xff72, 0xf01b, 0x0886, 0x03e0, 0xd801, 0x0ef6, 0x03c0, 0x0f52, 0x0340, 0x0dba,
//REG_BURST=0xd48c,
0x0200, 0x0af6, 0x0440, 0x0c22, 0x0400, 0x0d72, 0x0440, 0x0dc2, 0x0200, 0x0972, 0x0440, 0x0d3a, 0x0220, 0xd820, 0x0bfa, 0x0260, 0x70c9, 0x0451, 0x05c0, 0x78e0, 0xd900, 0xf00a, 0x70cf, 0xffff,
//REG_BURST=0xd4bc,
0xd520, 0x7835, 0x8041, 0x8000, 0xe102, 0xa040, 0x09f1, 0x8114, 0x71cf, 0xffff, 0xd4e0, 0x70cf, 0xffff, 0xc594, 0xb03a, 0x7fe0, 0xd800, 0x0000, 0x0000, 0x0500, 0x0500, 0x0200, 0x0330, 0x0000,
//REG_BURST=0xd4ec,
0x0000, 0x03cd, 0x050d, 0x01c5, 0x03b3, 0x00e0, 0x01e3, 0x0280, 0x01e0, 0x0109, 0x0080, 0x0500, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
//REG_BURST=0xd51c,
0x0000, 0x0000, 0xffff, 0xc9b4, 0xffff, 0xd324, 0xffff, 0xca34, 0xffff, 0xd3ec
};
*/
t_VarSettingWW code gc_MI1040_Step7_CPIPE_Preference[] =
{
// hemonel 2010-2-2: output YCbYCr
	{0xC86C, 0x0210 , 2},       // swap YC
	{0xA804, 0x0000 , 2},       // swap YC
// hemonel end
};


t_VarSettingWW code gc_MI1040_Step8_Features[] =
{
//[**********Step8*************]
// A preset for additional settings, if any, to ensure optimal out-of-box experience
//[Step8-Features]
	{0x98E, 0, 2}, // set XDMA to logical addressing
//{0xC984, 0x8040, 2},     //cam_port_output_control = 32840 selects parallel port and slowdown pixelclock
	{0x001E, 0x0777 , 2},   //PAD SLEW CONTROL
// hemonel 2010-2-2: add

//{0x0032, 0x3CD0, 2},  // PAD output enable
// hemonel end
};

static MI1040_FpsSetting_t*  GetMI1040FpsSetting(U8 Format,U8 Fps)
{
	U8 i;
	U8 Idx=0;
	Format = Format;
	if(g_bIsHighSpeed)
	{
		Idx=3; //8fps for uxga, 10 fps for vga
	}
	else
	{
		Idx = 1;
	}

	{
		for(i=0; i< (sizeof(g_staMI1040SXGAFpsSetting)/sizeof(MI1040_FpsSetting_t)); i++)
		{
			if(g_staMI1040SXGAFpsSetting[i].byFps ==Fps)
			{
				Idx=i;
				DBG(("find the config , idx is %bd, fps is %bd\n", Idx, Fps));
				break;
			}
		}
		return &g_staMI1040SXGAFpsSetting[Idx];
	}
}

static void WriteMI1040VarSettingWW(U16 length, t_VarSettingWW pSetting[])
{
	U16 i;

	for ( i = 0; i < length; i++)
	{
		I2C_WriteWord(g_SnrRegAccessProp.byI2CID,pSetting[i].wAddress,pSetting[i].wData,pSetting[i].byDataWidth);
	}
}
static U8 MI1040SendHostCmd(U16 wHostCmd)
{
	U16 wTemp;
	U8 i;

	Write_SenReg(MI1040REG_HOSTCMD, wHostCmd|MI1040_HOSTCMD_OK);//FIELD_WR= COMMAND_REGISTER, 0x8002        // Issue the SetState command

	do
	{
		Read_SenReg(MI1040REG_HOSTCMD, &wTemp); //POLL_FIELD= COMMAND_REGISTER, HOST_COMMAND_1, !=0, DELAY=10, TIMEOUT=100
		if(wTemp & wHostCmd)
		{
			i++;
			WaitTimeOut_Delay(1);
		}
		else
		{
			break;
		}
	}
	while (i<10);

	if(wTemp & wHostCmd)    // timeout
	{
		return FALSE;
	}

	if(wTemp & MI1040_HOSTCMD_OK)   //ERROR_IF= COMMAND_REGISTER, HOST_COMMAND_OK, !=1, "Change-Config failed"
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

static void MI1040_BurstWrite_SenReg(U16 wStartAddr, U16 wLen, U16 pSetting[])
{
	U16 i;

	for(i=0; i< wLen; i++)
	{
		Write_SenReg(wStartAddr+i*2, pSetting[i]);
	}
}

static void MI1040Step1Reset(void)
{
	// Soft-Reset
	Write_SenReg(0x001A, 0x0001);
	Write_SenReg(0x001A, 0x0000);

	// Errata item 3
	Write_SenReg_Mask(0x16, 0x0020, 0x0020);

	// Delay 20ms
	WaitTimeOut_Delay(2);
}


static U8 MI1040LoadApply_Patch(void)
{
	//[Load Patch 02FF]
	//FIELD_WR= ACCESS_CTL_STAT, 0x0001    // REG= 0x0982, 0x0001
	//FIELD_WR= PHYSICAL_ADDRESS_ACCESS, 0x51e8  // // REG= 0x098A, 0x51E8
	Write_SenReg(0x0982, 0x0001);
	Write_SenReg(0x098A, 0x5000);
	MI1040_BurstWrite_SenReg(0xd000, sizeof(gc_MI1040_Patch_02FF)/2, gc_MI1040_Patch_02FF);
	Write_SenReg(0x0982, 0x0001);
	Write_SenReg(0x098A, 0x512c);
	//MI1040_BurstWrite_SenReg(0xd12C, sizeof(gc_MI1040_Patch_03FF)/2, gc_MI1040_Patch_03FF);
	//FIELD_WR= LOGICAL_ADDRESS_ACCESS, 0x0000   // REG= 0x098E, 0x0000
	Write_SenReg(0x098E, 0x0000);

	//[Apply Patch 02FF]
	//FIELD_WR= PATCHLDR_LOADER_ADDRESS, 0x02b0
	//REG= 0x098E, 0x6000   // LOGICAL_ADDRESS_ACCESS [PATCHLDR_LOADER_ADDRESS]
	//REG= 0xE000, 0x02B0   // PATCHLDR_LOADER_ADDRESS
	Write_SenReg(0x098E, 0x6000);
	Write_SenReg(0xE000, 0x010C);
	//FIELD_WR= PATCHLDR_PATCH_ID, 0x02FF
	//REG= 0xE002, 0x02FF   // PATCHLDR_PATCH_ID
	Write_SenReg(0xE002, 0x0201);
	//FIELD_WR= PATCHLDR_FIRMWARE_ID, 0x40030106
	//REG= 0xE004, 0x4003   // PATCHLDR_FIRMWARE_ID
	//REG= 0xE006, 0x0106   // PATCHLDR_FIRMWARE_ID
	Write_SenReg(0xE004, 0x4103);
	Write_SenReg(0xE006, 0x0202);


	//FIELD_WR= COMMAND_REGISTER, HOST_COMMAND_OK, 1
	// REG= 0x0080, 0xFFF0
	//POLL_FIELD= COMMAND_REGISTER, HOST_COMMAND_0, !=0, DELAY=10, TIMEOUT=100
	// poll 0x0080[bit0] if equal 0, jump to next command

	//FIELD_WR= COMMAND_REGISTER, HOST_COMMAND_0, 1
	// REG= 0x0080, 0xFFF1

	//POLL_FIELD= COMMAND_REGISTER, HOST_COMMAND_0, !=0, DELAY=10, TIMEOUT=100
	// poll 0x0080[bit0] if equal 0, jump to next command


	//ERROR_IF= COMMAND_REGISTER, HOST_COMMAND_OK, !=1, "Couldn't apply patch",
	//ERROR_IF= PATCHLDR_APPLY_STATUS, !=0, "Apply status non-zero",
	return MI1040SendHostCmd(MI1040_HOSTCMD_0);
}

static U8 MI1040Change_Config(void)
{
	//do nothing //ERROR_IF= COMMAND_REGISTER, HOST_COMMAND_1, !=0, "State change cmd bit is already set"
	Write_SenReg_Mask(0xDC00, 0x2800, 0xFF00);  //FIELD_WR= SYSMGR_NEXT_STATE, 0x28     // Set the desired next state

	return MI1040SendHostCmd(MI1040_HOSTCMD_1);
}

/*
static U8 MI1040Refresh(void)
{
	 return MI1040SendHostCmd(MI1040_HOSTCMD_2);
}
*/

/*static U16 MapSnrGlbGain2SnrRegSetting(U16 wGain)
{
	U16 data sensorGain=0;

	if (wGain < 32 )
	{
		sensorGain = (wGain<<1);
	}
	else if (wGain >=32 && wGain < 64)
	{
		sensorGain = 64 + wGain;
	}
	else if (wGain >=64 && wGain < 128)
	{
		sensorGain = 128 + (wGain/2);
	}
	else if (wGain >=128 && wGain < 256)
	{
		sensorGain = 192 + (wGain/4);
	}

	return sensorGain;
}*/

/*float SnrRegSetting2SnrGlbGain(U16 wSnrRegGain)
{
	switch(wSnrRegGain&0x00C0)
	{
	case 0x00:
		return ((float)(wSnrRegGain&0x003f))/32.0;
		break;
		
	case 0x40:
		return 2.0*((float)(wSnrRegGain&0x003f))/32.0;
		break;
		
	case 0x80:
		return 4.0*((float)(wSnrRegGain&0x003f))/32.0;
		break;
		
	case 0xC0:
		return 8.0*((float)(wSnrRegGain&0x003f))/32.0;
		break;
	}
}*/

static U16 MapSnrGlbGain2SnrRegSetting(float fTotalGain)
{
	U16 wGain = (U16)(((fTotalGain-1.0)/1.2334 + 1.0)*32.0);
	U16 data sensorGain=0;

	if (wGain < 64)
	{
		sensorGain = wGain;
	}
	else if (wGain >= 64 && wGain < 128)
	{
		sensorGain = 64 + (wGain>>1);
	}
	else if (wGain >= 128 && wGain < 256)
	{
		sensorGain = 128 + (wGain>>2);
	}
	else if (wGain >= 256 && wGain < 512)
	{
		sensorGain = 192 + (wGain>>3);
	}

	return sensorGain;
}

float SnrRegSetting2SnrGlbGain(U16 wSnrRegGain)
{
	switch(wSnrRegGain&0x00C0)
	{
	case 0x00:
		return 1.2334*((float)(wSnrRegGain&0x003f))/32.0 - 0.251779;
		break;
		
	case 0x40:
		return 1.14732*2.0*((float)(wSnrRegGain&0x003f))/32.0 - 0.168967;
		break;
		
	case 0x80:
		return 1.13007*4.0*((float)(wSnrRegGain&0x003f))/32.0 - 0.250216;
		break;
		
	case 0xC0:
		return 1.07421*8.0*((float)(wSnrRegGain&0x003f))/32.0 - 0.139098;
		break;
	}
}

//this function is also called during Probe stage to calculate the transfer_size/LWM/HWM
void GetSensorPclkHsync(U16 wSensorSPFormat,U8 byFps)
{
	MI1040_FpsSetting_t *pMI1040_FpsSetting;

	wSensorSPFormat =wSensorSPFormat;
	
	pMI1040_FpsSetting=GetMI1040FpsSetting((U8)wSensorSPFormat, byFps);
	g_wSensorHsyncWidth = pMI1040_FpsSetting->wHorizWidth;
	g_dwPclk= pMI1040_FpsSetting->dwPixelClk;
}

void MI1040SetFormatFps(U16 SetFormat, U8 Fps)
{
	MI1040_FpsSetting_t *pMI1040_FpsSetting;

	pMI1040_FpsSetting = GetMI1040FpsSetting(SetFormat, Fps);


	// write sensor setting
	//LOAD = Step1-Reset            //Reset
	//LOAD = Step2-PLL_Timing           //PLL, Timing  and Frame Rate, 720p_30fps
	//#//LOAD = Step2-PLL_Timing VGA Scaling
	//LOAD = Step3-Recommended      //Patch,Errata and Sensor optimization Setting
	//#//LOAD = Step4-PGA           //PGA, Lens shading tuning can be tuned later, don't need apply now..
	//LOAD = Step5-AWB_CCM          //AWB & CCM
	//LOAD = Step7-CPIPE_Preference //Color Pipe preference settings, if any
	//LOAD = Step8-Features     //Ports, special features, etc.
	//LOAD = Change-Config

	MI1040Step1Reset();

	Write_SenReg(0xC886, 0x48);
	Write_SenReg(0xC890, 0x40);
	Write_SenReg_Mask(0xC878, 0x0000, 0xff00);
	Write_SenReg_Mask(0xC87a, 0x3700, 0xff00);
	Write_SenReg_Mask(0xC87b, 0x2700, 0xff00);
	Write_SenReg(0xC87C, 0x005A);

	//LOAD = Step2-PLL_Timing
	Write_SenReg(0x098E, 0x1000);
	Write_SenReg_Mask(0xC97E, 0x0100, 0x0100);
	Write_SenReg(0xC984, 0x8040);

	Write_SenReg(0xC980,    pMI1040_FpsSetting->wPLLCtlM_N); //cam_sysctl_pll_divider_m_n
	Write_SenReg(0xC982,    pMI1040_FpsSetting->wPLLPara_P);    //cam_sysctl_pll_divider_p
	Write_SenReg(0xC800,    4);//sensor_cfg_y_start);    //cam_sensor_cfg_y_addr_start
	Write_SenReg(0xC802,    4);//sensor_cfg_x_start);    //cam_sensor_cfg_x_addr_start
	Write_SenReg(0xC804,    971);//sensor_cfg_y_end);  //cam_sensor_cfg_y_addr_end
	Write_SenReg(0xC806,    1291);//sensor_cfg_x_end);  //cam_sensor_cfg_x_addr_end
	Write_SenReg(0xC808,    (pMI1040_FpsSetting->dwPixelClk) >> 16);
	Write_SenReg(0xC80A,    (pMI1040_FpsSetting->dwPixelClk) & 0xFFFF);
	Write_SenReg(0xC80C,    0x0001);    //cam_sensor_cfg_row_speed
	Write_SenReg(0xC80E,    pMI1040_FpsSetting->wfine_integ_time_min);  //cam_sensor_cfg_fine_integ_time_min
	Write_SenReg(0xC810,    pMI1040_FpsSetting->wfine_integ_time_max);  //cam_sensor_cfg_fine_integ_time_max
	Write_SenReg(0xC812,    pMI1040_FpsSetting->wVertHeight);   //cam_sensor_cfg_frame_length_lines
	Write_SenReg(0xC814,    pMI1040_FpsSetting->wHorizWidth);   //cam_sensor_cfg_line_length_pck
	Write_SenReg(0xC816,    0x0060);    //cam_sensor_cfg_fine_correction
	Write_SenReg(0xC818,    963);//sensor_cfg_cpipe_last_row); //cam_sensor_cfg_cpipe_last_row
	Write_SenReg(0xC826,    0x0020);	//cam_sensor_cfg_reg_0_data = 32
	Write_SenReg(0xC834,    3);//cam_read_mode); //cam_sensor_control_read_mode
	Write_SenReg(0xC854,    0x0000);    //cam_crop_window_xoffset
	Write_SenReg(0xC856,    0x0000);    //cam_crop_window_yoffset
	Write_SenReg(0xC858,    1280);//crop_win_width);    //cam_crop_window_width
	Write_SenReg(0xC85A,    960);//crop_win_height);   //cam_crop_window_height
	Write_SenReg(0xC85C,    0x0342);    //cam_crop_cropmode
	Write_SenReg(0xC868,    1280);//output_width);  //cam_output_width
	Write_SenReg(0xC86A,    960);//output_height); //cam_output_height
	Write_SenReg(0xC88C,    pMI1040_FpsSetting->waet_max_frame_rate);    //cam_aet_max_frame_rate
	Write_SenReg(0xC88E,    pMI1040_FpsSetting->waet_min_frame_rate); //cam_aet_min_frame_rate
	Write_SenReg(0xC914,    0x0000);    //cam_stat_awb_clip_window_xstart
	Write_SenReg(0xC916,    0x0000);    //cam_stat_awb_clip_window_ystart
	Write_SenReg(0xC918,    1279);//(output_width -1)); //cam_stat_awb_clip_window_xend
	Write_SenReg(0xC91A,    959);//(output_height -1));    //cam_stat_awb_clip_window_yend
	Write_SenReg(0xC91C,    0x0000);    //cam_stat_ae_initial_window_xstart
	Write_SenReg(0xC91E,    0x0000);    //cam_stat_ae_initial_window_ystart
	Write_SenReg(0xC920,    255);//(output_width -1)/5);   //cam_stat_ae_initial_window_xend
	Write_SenReg(0xC922,    191);//(output_height -1)/5);  //cam_stat_ae_initial_window_yend

	WriteMI1040VarSettingWW(sizeof(gc_MI1040_Step3_Recommended)/5, gc_MI1040_Step3_Recommended);
	Write_SenReg_Mask(0x301A, 0x0200, 0x0200);
	MI1040LoadApply_Patch();
	//WriteMI1040VarSettingWW(sizeof(gc_MI1040_Step4_APGA)/5, gc_MI1040_Step4_APGA);  //LSC

	//WriteMI1040VarSettingWW(sizeof(gc_MI1040_Step5_AWB_CCM)/5, gc_MI1040_Step5_AWB_CCM);
	WriteMI1040VarSettingWW(sizeof(gc_MI1040_Step7_CPIPE_Preference)/5, gc_MI1040_Step7_CPIPE_Preference);
	WriteMI1040VarSettingWW(sizeof(gc_MI1040_Step8_Features)/5, gc_MI1040_Step8_Features);
	//WriteMI1040VarSettingWW(sizeof(gc_MI1040_AE_SPEEDUP)/5, gc_MI1040_AE_SPEEDUP);

	MI1040Change_Config();

	Write_SenReg_Mask(0xCC21, 0x0000, 0x0100);

	g_wAECExposureRowMax = (pMI1040_FpsSetting->wVertHeight)-2;
	//g_wSensorHsyncWidth = pMI1040_FpsSetting->wHorizWidth;
	//g_dwPclk= pMI1040_FpsSetting->dwPixelClk;
	GetSensorPclkHsync(SetFormat,Fps);
	g_wAEC_LineNumber =  pMI1040_FpsSetting->wVertHeight;   // this for AE insert dummy line algothrim

	g_wSensorWidthBefBLC = 1280;
	g_wSensorHeightBefBLC = 960;
#ifndef _MIPI_EXIST_
	SetBkWindowStart(1, 1);
#endif
}


void CfgMI1040ControlAttr(void)
{
	U8 i;

	g_bySensorSize = SENSOR_SIZE_XVGA;
	g_wSensorSPFormat = XVGA_FRM;

	memcpy(g_asOvCTT,gc_MI1040_CTT,sizeof(gc_MI1040_CTT));

	{
		// SVA connect sensor VDDA
		// SVIO connect sensor VDDIO and VDDC
#if (_CHIP_ID_ & _RTS5840_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V79;
#elif (_CHIP_ID_ & _RTS5832_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V85;
#elif (_CHIP_ID_ & _RTS5829B_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V82;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V80;
#endif
	}

//	if(g_byFormatCfgExist==0) // format config not exist, use default setting.
	{
		//-------- configure high speed frame size-----------
		//video
		g_aVideoFormat[0].byaVideoFrameTbl[0] = 12;		// resolution number
		g_aVideoFormat[0].byaVideoFrameTbl[1]=F_SEL_640_480;
		g_aVideoFormat[0].byaVideoFrameTbl[2]=F_SEL_160_120;
		g_aVideoFormat[0].byaVideoFrameTbl[3]=F_SEL_176_144;
		g_aVideoFormat[0].byaVideoFrameTbl[4]=F_SEL_320_240;
		g_aVideoFormat[0].byaVideoFrameTbl[5]=F_SEL_352_288;
		g_aVideoFormat[0].byaVideoFrameTbl[6]=F_SEL_320_180;
		g_aVideoFormat[0].byaVideoFrameTbl[7]=F_SEL_1280_720;
		g_aVideoFormat[0].byaVideoFrameTbl[8]=F_SEL_1280_800;
		g_aVideoFormat[0].byaVideoFrameTbl[9]=F_SEL_960_540;
		g_aVideoFormat[0].byaVideoFrameTbl[10]=F_SEL_848_480;
		g_aVideoFormat[0].byaVideoFrameTbl[11]=F_SEL_640_360;
		g_aVideoFormat[0].byaVideoFrameTbl[12]=F_SEL_424_240;
		//still image
		g_aVideoFormat[0].byaStillFrameTbl[0] = 12;	// resolution number
		g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_1280_800;
		g_aVideoFormat[0].byaStillFrameTbl[2]=F_SEL_1280_720;
		g_aVideoFormat[0].byaStillFrameTbl[3]=F_SEL_160_120;
		g_aVideoFormat[0].byaStillFrameTbl[4]=F_SEL_176_144;
		g_aVideoFormat[0].byaStillFrameTbl[5]=F_SEL_320_240;
		g_aVideoFormat[0].byaStillFrameTbl[6]=F_SEL_352_288;
		g_aVideoFormat[0].byaStillFrameTbl[7]=F_SEL_640_480;
		g_aVideoFormat[0].byaStillFrameTbl[8]=F_SEL_960_540;
		g_aVideoFormat[0].byaStillFrameTbl[9]=F_SEL_848_480;
		g_aVideoFormat[0].byaStillFrameTbl[10]=F_SEL_640_360;
		g_aVideoFormat[0].byaStillFrameTbl[11]=F_SEL_424_240;
		g_aVideoFormat[0].byaStillFrameTbl[12]=F_SEL_320_180;

		//====== fps  setting ===========
		{
			for(i=0; i<10; i++)
			{
				/*
					F_SEL_640_480    (0)
					F_SEL_160_120    (1)
					F_SEL_176_144    (2)
					F_SEL_320_180	(3)
					F_SEL_320_200    (4)
					F_SEL_320_240    (5)
					F_SEL_352_288    (6)
					F_SEL_424_240	(7)
					F_SEL_640_360	(8)
					F_SEL_640_400    (9)
				*/
				g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_15|FPS_20|FPS_25|FPS_30;
			}
			for(i=10; i<18; i++)
			{
				/*
					F_SEL_800_600    (10)
					F_SEL_848_480	(11)
					F_SEL_960_540	 (12)
					F_SEL_1024_768   (13)
					F_SEL_1280_720   (14)
					F_SEL_1280_800   (15)
					F_SEL_1280_960   (16)
					F_SEL_1280_1024  (17)
				*/
				g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_10|FPS_5;
			}
		}
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_800_600]= FPS_25|FPS_20|FPS_15|FPS_10|FPS_5;
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_848_480]= FPS_25|FPS_20|FPS_15|FPS_10|FPS_5;
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_960_540]= FPS_20|FPS_15|FPS_10|FPS_5;

		//====== format type  setting ===========
		g_aVideoFormat[0].byFormatType = FORMAT_TYPE_YUY2;

#ifdef _ENABLE_MJPEG_
		// copy YUY2 setting to MJPEG setting
		memcpy(&(g_aVideoFormat[1]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

		// modify MJPEG FPS setting
		for(i=10; i<18; i++)
		{
			g_aVideoFormat[1].waFrameFpsBitmap[i] = (FPS_15|FPS_20|FPS_25|FPS_30);
		}

		// modify MJPEG format type
		g_aVideoFormat[1].byFormatType = FORMAT_TYPE_MJPG;
#endif

#ifdef _ENABLE_M420_FMT_
		// copy YUY2 setting to M420 setting
		memcpy(&(g_aVideoFormat[2]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

		// modify M420 FPS setting
		{
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_800_600] =FPS_25|FPS_20|FPS_15|FPS_10|FPS_5;//|FPS_1;             //bit07 F_800_600
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_960_540] =FPS_25|FPS_20|FPS_15|FPS_10|FPS_5;//|FPS_5|FPS_3|FPS_1;                    //bit08 F_960_720
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1024_768] =FPS_15|FPS_10|FPS_5;//|FPS_1;                    //bit09 F_1024_768
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_720] =FPS_10|FPS_5;//FPS_9|FPS_8|FPS_1;    //bit10 F_1280_720
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_800] =FPS_10|FPS_5;

			// modify M420 format type
			g_aVideoFormat[2].byFormatType = FORMAT_TYPE_M420;
		}
#endif
	}

	InitMI1040IspParams();
}

void SetMI1040IntegrationTime(U16 wEspline)
{
	U16 wFrameLenLines;
	U16 wExpMax = g_wAECExposureRowMax-2;

	if(wEspline > wExpMax)
	{
		wFrameLenLines = wEspline + 20;
	}
	else
	{
		wFrameLenLines = g_wAEC_LineNumber;
	}

	if (wFrameLenLines%2==1)
	{
		wFrameLenLines++;
	}

	// write exposure time and dummy line to register
	Write_SenReg(0x300a, wFrameLenLines);
	Write_SenReg(0x3012, wEspline);	// change exposure value
	Write_SenReg(0x305E,0x1020);		// fix sensor gain 1x
}

void SetMI1040Gain(float fGain)
{
}

void SetMI1040ImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.
{
	bySnrImgDir ^= 0x03; 	// MI1040 output mirrored image at default, so need mirror the image for normal output
	// hemonel 2011-07-04: flip and mirror register changes take effect after a Change-Config command.
	// 					  But Change-Config command will reset exposure time to default value in order
	//					  that the image illumination will blink. So far we don't implement this function.
	/*
	Write_SenReg_Mask(0xC834, bySnrImgDir, 0x03);
	MI1040Change_Config();
	SetBkWindowStart(bySnrImgDir & 0x01, (bySnrImgDir & 0x02)>>1);
	*/
}

void SetMI1040Exposuretime_Gain(float fExpTime, float fTotalGain)
{
	U16 wExposureRows_floor;
	U16  wGainRegSetting; //data
	float fSnrGlbGain;
	float fExposureRows;
	U16 wSetDummy;
	float fTemp;
	U16 wExposurePixels;
	
	wGainRegSetting=MapSnrGlbGain2SnrRegSetting(fTotalGain);
	fSnrGlbGain = SnrRegSetting2SnrGlbGain(wGainRegSetting);

	wSetDummy = g_wAFRInsertDummylines + g_wAEC_LineNumber;
	
	if(fabs(g_fCurExpTime-fExpTime) > 0.0001)
	{
		fExposureRows = fExpTime/g_fSensorRowTimes;  //unit : 1row
		wExposureRows_floor = (U16)(fExposureRows);
		fTemp = fExpTime-((float)wExposureRows_floor*(g_fSensorRowTimes)); //fine integration time;unit:  us
		wExposurePixels =(U16)(fTemp * (float)(g_dwPclk/10000)) ; //unit: pixels
		wExposurePixels = ClipWord(wExposurePixels, 0xDB, g_wSensorHsyncWidth-0x83);	// sensor limit

		//-----------Write  frame length------------
		Write_SenReg(0x300a, wSetDummy);

		//-----------Write Exposuretime setting-----------
		Write_SenReg(0x3012,wExposureRows_floor);   // change exposure value low//PaoChi
		Write_SenReg(0x3014,wExposurePixels);

		//-----------Write gain setting-----------
		Write_SenReg(0x305E,(wGainRegSetting|0x1000));

		SetISPAEGain(fTotalGain, fSnrGlbGain, 0);
		
		g_fCurExpTime = fExpTime;  // back up exposure rows
	}
	else
	{	
		//-----------Write  frame length------------
		Write_SenReg(0x300a, wSetDummy);
		
		//-----------Write gain setting-----------
		Write_SenReg(0x305E, (wGainRegSetting|0x1000));

		SetISPAEGain(fTotalGain, fSnrGlbGain, 0);
	}

}

void MI1040_POR()
{
#ifdef _MIPI_EXIST_
	Init_MIPI();
#else
	Init_CCS();
#endif
	ENTER_SENSOR_RESET();
	// power on
	SensorPowerControl(SWITCH_ON,EN_DELAYTIME);
	uDelay(1);
	// Initialize I2C Controller Timing
	LEAVE_SENSOR_RESET();
	// clock select 24M
	if( g_bySSCEnable== TRUE)
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_SSC_24M);
	}
	else
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_96M|CCS_CLK_DIVIDER_4);
	}

	EnableSensorHCLK();
	g_bySensorIsOpen = SENSOR_OPEN;

	WaitTimeOut_Delay(5);  //need 44ms
}

void InitMI1040IspParams()
{
	// AWB initial gain
	//use D50 R/G/B gain as AWB initial value
	g_wAWBRGain_Last= 0x17C;
	g_wAWBGGain_Last= 0x100;
	g_wAWBBGain_Last= 0x226;
	g_wProjectGR = g_wAWBRGain_Last;
	g_wProjectGB = g_wAWBBGain_Last;

	// Special ISP
	g_bySaturation_Def =64;
	g_byContrast_Def = 32; //Neil Tuning at chicony

	g_wDynamicISPEn =DYNAMIC_SHARPPARAM_EN | DYNAMIC_SHARPNESS_EN;
	//g_wDynamicISPEn = 0;
}

void SetMI1040DynamicISP(U8 byAEC_Gain)
{
	byAEC_Gain = byAEC_Gain;
}

void SetMI1040DynamicISP_AWB(U16 wColorTempature)
{
	wColorTempature = wColorTempature;
}
#endif
