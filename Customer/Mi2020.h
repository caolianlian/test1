#ifndef _MI2020_H_
#define _MI2020_H_

#define MI2020_AEW    (72+10)
#define MI2020_AEB     (72 -10)


typedef struct MI2020_FpsSetting
{
	U8 byFps;
	//U8 byHClk; //Host clock frequency
	U16 wHorizClks;   // 0x300c lines_length_pck
	U16 wVertRows;   //0x300A frame_length_lines
	U16 wExtraDelay; //0x3018 extra delay
	U16 wPLLCtlReg; // 0x341c register
	U32 dwPixelClk;

} MI2020_FpsSetting_t;

void Mi2020SetFormatFps(U8 SetFormat,U8 Fps);
void Mi2020Refresh();
void 	CfgMI2020ControlAttr();
void SetMi2020OutputDim(U16 wWidth,U16 wHeight);
void SetMi2020WindowCrop(U16 wXStart,U16 wXEnd,U16 wYStart,U16 wYEnd );
void SetMi2020SensorEffect(U8 byEffect);
void SetMi2020ImgDir(U8 bySnrImgDir); //flip: bit1,mirror bit0.
void SetMI2020Brightness(S16 swSetValue)	;
void SetMI2020Contrast(U16 wSetValue);

void SetMi2020PwrLineFreq(U8 byFps, U8 bySetValue);

void SetMi2020ISPMisc();
void 	SetMi2020BackLightComp(U8 bySetValue);
void SetMi2020WBTempAuto(U8 bySetValue);
void SetMi2020WBTemp(U16 wSetValue);

U16 GetMi2020AEGain();
void SetMi2020IntegrationTimeAuto(U8 bySetValue);
void SetMi2020IntegrationTime(U16 wEspline);
void SetMI2020SoftReset(void );
#endif
