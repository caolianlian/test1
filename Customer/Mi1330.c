#include "Inc.h"

#ifdef RTS58XX_SP_MI1330
U8 g_byMi1330SOCSeqMode=0x0F;


code MI1330_FpsSetting_t  g_staMi1330VGAFpsSetting[]=
{
	{1,/*CCS_HCLK_24M,*/(648+8192),	(520+1572),	6720,	0x0525,	18500000},
	{3,/*CCS_HCLK_24M,*/(648+8192),	(520+177),	5186,	0x0525,	18500000},
	{5,/*CCS_HCLK_24M,*/(648+2577),	(520+100),	500,	0x0514,	10000000},
	{10,/*CCS_HCLK_24M,*/(648+964),	(520+100),	560,	0x0514,	10000000},
	{15,/*CCS_HCLK_24M,*/(648+580),	(520+100),	416,	0x0a2a,	11455000},

	{20,/*CCS_HCLK_24M,*/(648+580),	(520+90),	919,	0x0A37,	15000000},
	{25,/*CCS_HCLK_24M,*/(648+581),	(520+95),   	165,	0x093f,	18900000},
	{30,/*CCS_HCLK_24M,*/(648+580),	(520+95),	335,	0x0844,	22666667},
};

code MI1330_FpsSetting_t  g_staMi1330SXGAFpsSetting[]=
{
	{1,(1288+538),(1032+4444),       824,	0x0514,	10000000},
	{3,(1288+538),(1032+793), 	       883,	0x0514,	10000000},
	{5,(1288+558),(1032+120), 	       680,	0x0A27,	10636000},

	{8,(1288+543),(1032+120), 	       63,	0x072d,	16875000},

	{9,(1288+544),(1032+120), 	       647, 0x0213,	19000000},
	{10,(1288+556),(1032+120),      712, 0x0b55,	21250000},

	{11,(1288+538),(1032+120),      17,    0x0b5b,	22750000},

	{12,(1288+539),(1032+100),      17,    0x0a5b,	24818000},

	{15,(1288+478),(1032+100),      888,    0x0114,	30000000},
};


t_RegSettingWW code gc_MI1330_SOC_LSC_Setting[]=
{
	/*
		//[Lens Correction 11/18/08 17:56:06 RS]
		{ 0x3658, 0x0150 },	// P_RD_P0Q0
		{ 0x365A, 0xD30A },	// P_RD_P0Q1
		{ 0x365C, 0x60F0 },	// P_RD_P0Q2
		{ 0x365E, 0x8ACD },	// P_RD_P0Q3
		{ 0x3660, 0x1EAE },	// P_RD_P0Q4
		{ 0x3680, 0x9BAD },	// P_RD_P1Q0
		{ 0x3682, 0x4A4C },	// P_RD_P1Q1
		{ 0x3684, 0x6E8F },	// P_RD_P1Q2
		{ 0x3686, 0x7050 },	// P_RD_P1Q3
		{ 0x3688, 0xFCCD },	// P_RD_P1Q4
		{ 0x36A8, 0x1FB0 },	// P_RD_P2Q0
		{ 0x36AA, 0xCBAB },	// P_RD_P2Q1
		{ 0x36AC, 0xD091 },	// P_RD_P2Q2
		{ 0x36AE, 0x2A4A },	// P_RD_P2Q3
		{ 0x36B0, 0x3BD3 },	// P_RD_P2Q4
		{ 0x36D0, 0x20AF },	// P_RD_P3Q0
		{ 0x36D2, 0x33B1 },	// P_RD_P3Q1
		{ 0x36D4, 0x0511 },	// P_RD_P3Q2
		{ 0x36D6, 0x8314 },	// P_RD_P3Q3
		{ 0x36D8, 0xC493 },	// P_RD_P3Q4
		{ 0x36F8, 0x6C2E },	// P_RD_P4Q0
		{ 0x36FA, 0xB992 },	// P_RD_P4Q1
		{ 0x36FC, 0x4334 },	// P_RD_P4Q2
		{ 0x36FE, 0x0A14 },	// P_RD_P4Q3
		{ 0x3700, 0xADB6 },	// P_RD_P4Q4
		{ 0x364E, 0x0690 },	// P_GR_P0Q0
		{ 0x3650, 0x6CCE },	// P_GR_P0Q1
		{ 0x3652, 0x79B0 },	// P_GR_P0Q2
		{ 0x3654, 0xF3CB },	// P_GR_P0Q3
		{ 0x3656, 0xCD0C },	// P_GR_P0Q4
		{ 0x3676, 0xCCAD },	// P_GR_P1Q0
		{ 0x3678, 0x6EAD },	// P_GR_P1Q1
		{ 0x367A, 0x1550 },	// P_GR_P1Q2
		{ 0x367C, 0x0331 },	// P_GR_P1Q3
		{ 0x367E, 0x6BA9 },	// P_GR_P1Q4
		{ 0x369E, 0x0510 },	// P_GR_P2Q0
		{ 0x36A0, 0x2D0A },	// P_GR_P2Q1
		{ 0x36A2, 0xD3F1 },	// P_GR_P2Q2
		{ 0x36A4, 0x0A8E },	// P_GR_P2Q3
		{ 0x36A6, 0x19F3 },	// P_GR_P2Q4
		{ 0x36C6, 0x0C2F },	// P_GR_P3Q0
		{ 0x36C8, 0x5A11 },	// P_GR_P3Q1
		{ 0x36CA, 0x1D11 },	// P_GR_P3Q2
		{ 0x36CC, 0x9C74 },	// P_GR_P3Q3
		{ 0x36CE, 0x86B4 },	// P_GR_P3Q4
		{ 0x36EE, 0x006F },	// P_GR_P4Q0
		{ 0x36F0, 0xF211 },	// P_GR_P4Q1
		{ 0x36F2, 0x0D34 },	// P_GR_P4Q2
		{ 0x36F4, 0x0193 },	// P_GR_P4Q3
		{ 0x36F6, 0xE075 },	// P_GR_P4Q4
		{ 0x3662, 0x0930 },	// P_BL_P0Q0
		{ 0x3664, 0x6CAE },	// P_BL_P0Q1
		{ 0x3666, 0x49B0 },	// P_BL_P0Q2
		{ 0x3668, 0xC94D },	// P_BL_P0Q3
		{ 0x366A, 0x202E },	// P_BL_P0Q4
		{ 0x368A, 0x87EE },	// P_BL_P1Q0
		{ 0x368C, 0x1EEB },	// P_BL_P1Q1
		{ 0x368E, 0x0FF0 },	// P_BL_P1Q2
		{ 0x3690, 0x05D1 },	// P_BL_P1Q3
		{ 0x3692, 0xDDCE },	// P_BL_P1Q4
		{ 0x36B2, 0x770F },	// P_BL_P2Q0
		{ 0x36B4, 0x5788 },	// P_BL_P2Q1
		{ 0x36B6, 0x80F2 },	// P_BL_P2Q2
		{ 0x36B8, 0x024E },	// P_BL_P2Q3
		{ 0x36BA, 0x4673 },	// P_BL_P2Q4
		{ 0x36DA, 0x536E },	// P_BL_P3Q0
		{ 0x36DC, 0x5271 },	// P_BL_P3Q1
		{ 0x36DE, 0x4AAF },	// P_BL_P3Q2
		{ 0x36E0, 0x8E74 },	// P_BL_P3Q3
		{ 0x36E2, 0xEF32 },	// P_BL_P3Q4
		{ 0x3702, 0x57AF },	// P_BL_P4Q0
		{ 0x3704, 0x9192 },	// P_BL_P4Q1
		{ 0x3706, 0x1EF4 },	// P_BL_P4Q2
		{ 0x3708, 0x2173 },	// P_BL_P4Q3
		{ 0x370A, 0xF735 },	// P_BL_P4Q4
		{ 0x366C, 0x0790 },	// P_GB_P0Q0
		{ 0x366E, 0xF5EB },	// P_GB_P0Q1
		{ 0x3670, 0x5D50 },	// P_GB_P0Q2
		{ 0x3672, 0xE3AD },	// P_GB_P0Q3
		{ 0x3674, 0x622B },	// P_GB_P0Q4
		{ 0x3694, 0xEFCD },	// P_GB_P1Q0
		{ 0x3696, 0x19CE },	// P_GB_P1Q1
		{ 0x3698, 0x5BCF },	// P_GB_P1Q2
		{ 0x369A, 0x63B0 },	// P_GB_P1Q3
		{ 0x369C, 0xBF2F },	// P_GB_P1Q4
		{ 0x36BC, 0x0870 },	// P_GB_P2Q0
		{ 0x36BE, 0x902E },	// P_GB_P2Q1
		{ 0x36C0, 0xDC71 },	// P_GB_P2Q2
		{ 0x36C2, 0x194F },	// P_GB_P2Q3
		{ 0x36C4, 0x1D53 },	// P_GB_P2Q4
		{ 0x36E4, 0x5A0E },	// P_GB_P3Q0
		{ 0x36E6, 0x3731 },	// P_GB_P3Q1
		{ 0x36E8, 0xA84C },	// P_GB_P3Q2
		{ 0x36EA, 0x8834 },	// P_GB_P3Q3
		{ 0x36EC, 0xFA30 },	// P_GB_P3Q4
		{ 0x370C, 0x210F },	// P_GB_P4Q0
		{ 0x370E, 0xACD2 },	// P_GB_P4Q1
		{ 0x3710, 0x0894 },	// P_GB_P4Q2
		{ 0x3712, 0x00D4 },	// P_GB_P4Q3
		{ 0x3714, 0xCCF5 },	// P_GB_P4Q4
		{ 0x3644, 0x0280 },	// POLY_ORIGIN_C
		{ 0x3642, 0x01F4 }	// POLY_ORIGIN_R
	*/
//STATE= Lens Correction Falloff, 100
//STATE= Lens Correction Center X, 544
//STATE= Lens Correction Center Y, 620
//BITFIELD= 0x3210, 0x0008, 1 //PGA_ENABLE


//[Lens Correction 02/06/09 19:49:37]
	{ 0x3658, 0x0190},   // P_RD_P0Q0
	{ 0x365A, 0xAF6A},   // P_RD_P0Q1
	{ 0x365C, 0x0DF1},   // P_RD_P0Q2
	{ 0x365E, 0xC8E6},   // P_RD_P0Q3
	{ 0x3660, 0xACAE},   // P_RD_P0Q4
	{ 0x3680, 0xB52D},   // P_RD_P1Q0
	{ 0x3682, 0x5D0A},   // P_RD_P1Q1
	{ 0x3684, 0xAE0E},   // P_RD_P1Q2
	{ 0x3686, 0xE76E},   // P_RD_P1Q3
	{ 0x3688, 0x2C70},   // P_RD_P1Q4
	{ 0x36A8, 0x42B0},   // P_RD_P2Q0
	{ 0x36AA, 0xA8CF},   // P_RD_P2Q1
	{ 0x36AC, 0x0D91},   // P_RD_P2Q2
	{ 0x36AE, 0x1391},   // P_RD_P2Q3
	{ 0x36B0, 0xDB72},   // P_RD_P2Q4
	{ 0x36D0, 0x208D},   // P_RD_P3Q0
	{ 0x36D2, 0xDAEE},   // P_RD_P3Q1
	{ 0x36D4, 0x0AF2},   // P_RD_P3Q2
	{ 0x36D6, 0x4891},   // P_RD_P3Q3
	{ 0x36D8, 0xE7D0},   // P_RD_P3Q4
	{ 0x36F8, 0x68CE},   // P_RD_P4Q0
	{ 0x36FA, 0x4890},   // P_RD_P4Q1
	{ 0x36FC, 0x8673},   // P_RD_P4Q2
	{ 0x36FE, 0x94D3},   // P_RD_P4Q3
	{ 0x3700, 0xFD11},   // P_RD_P4Q4
	{ 0x364E, 0x04D0},   // P_GR_P0Q0
	{ 0x3650, 0x77EE},   // P_GR_P0Q1
	{ 0x3652, 0x7E10},   // P_GR_P0Q2
	{ 0x3654, 0x428D},   // P_GR_P0Q3
	{ 0x3656, 0x8E6F},   // P_GR_P0Q4
	{ 0x3676, 0xA20C},   // P_GR_P1Q0
	{ 0x3678, 0x394B},   // P_GR_P1Q1
	{ 0x367A, 0x9F0E},   // P_GR_P1Q2
	{ 0x367C, 0x32C9},   // P_GR_P1Q3
	{ 0x367E, 0x1250},   // P_GR_P1Q4
	{ 0x369E, 0x18F0},   // P_GR_P2Q0
	{ 0x36A0, 0xC1CB},   // P_GR_P2Q1
	{ 0x36A2, 0x6B30},   // P_GR_P2Q2
	{ 0x36A4, 0x1ACF},   // P_GR_P2Q3
	{ 0x36A6, 0xB5D2},   // P_GR_P2Q4
	{ 0x36C6, 0x206C},   // P_GR_P3Q0
	{ 0x36C8, 0x4F2E},   // P_GR_P3Q1
	{ 0x36CA, 0x0DD2},   // P_GR_P3Q2
	{ 0x36CC, 0x2630},   // P_GR_P3Q3
	{ 0x36CE, 0xDCB1},   // P_GR_P3Q4
	{ 0x36EE, 0x6EED},   // P_GR_P4Q0
	{ 0x36F0, 0x082F},   // P_GR_P4Q1
	{ 0x36F2, 0xA3B3},   // P_GR_P4Q2
	{ 0x36F4, 0xEED2},   // P_GR_P4Q3
	{ 0x36F6, 0x5D31},   // P_GR_P4Q4
	{ 0x3662, 0x0510},   // P_BL_P0Q0
	{ 0x3664, 0x598E},   // P_BL_P0Q1
	{ 0x3666, 0x4190},   // P_BL_P0Q2
	{ 0x3668, 0x0CEE},   // P_BL_P0Q3
	{ 0x366A, 0xDECB},   // P_BL_P0Q4
	{ 0x368A, 0xFD2C},   // P_BL_P1Q0
	{ 0x368C, 0xC94C},   // P_BL_P1Q1
	{ 0x368E, 0x888F},   // P_BL_P1Q2
	{ 0x3690, 0xF8EE},   // P_BL_P1Q3
	{ 0x3692, 0x2FEF},   // P_BL_P1Q4
	{ 0x36B2, 0x7FEF},   // P_BL_P2Q0
	{ 0x36B4, 0xD02D},   // P_BL_P2Q1
	{ 0x36B6, 0x2CD0},   // P_BL_P2Q2
	{ 0x36B8, 0x1490},   // P_BL_P2Q3
	{ 0x36BA, 0xCFF1},   // P_BL_P2Q4
	{ 0x36DA, 0xAA2E},   // P_BL_P3Q0
	{ 0x36DC, 0xB6CC},   // P_BL_P3Q1
	{ 0x36DE, 0x67F1},   // P_BL_P3Q2
	{ 0x36E0, 0x0E92},   // P_BL_P3Q3
	{ 0x36E2, 0x0271},   // P_BL_P3Q4
	{ 0x3702, 0x402F},   // P_BL_P4Q0
	{ 0x3704, 0x1590},   // P_BL_P4Q1
	{ 0x3706, 0xCB12},   // P_BL_P4Q2
	{ 0x3708, 0xA753},   // P_BL_P4Q3
	{ 0x370A, 0xFE52},   // P_BL_P4Q4
	{ 0x366C, 0x02B0},   // P_GB_P0Q0
	{ 0x366E, 0xAECA},   // P_GB_P0Q1
	{ 0x3670, 0x6970},   // P_GB_P0Q2
	{ 0x3672, 0xACEA},   // P_GB_P0Q3
	{ 0x3674, 0x93AF},   // P_GB_P0Q4
	{ 0x3694, 0x88ED},   // P_GB_P1Q0
	{ 0x3696, 0x084D},   // P_GB_P1Q1
	{ 0x3698, 0x834F},   // P_GB_P1Q2
	{ 0x369A, 0x820D},   // P_GB_P1Q3
	{ 0x369C, 0x75AF},   // P_GB_P1Q4
	{ 0x36BC, 0x1CF0},   // P_GB_P2Q0
	{ 0x36BE, 0xA6AF},   // P_GB_P2Q1
	{ 0x36C0, 0x04B1},   // P_GB_P2Q2
	{ 0x36C2, 0x2C50},   // P_GB_P2Q3
	{ 0x36C4, 0xBF52},   // P_GB_P2Q4
	{ 0x36E4, 0xFD6D},   // P_GB_P3Q0
	{ 0x36E6, 0x344E},   // P_GB_P3Q1
	{ 0x36E8, 0x0272},   // P_GB_P3Q2
	{ 0x36EA, 0x0AEF},   // P_GB_P3Q3
	{ 0x36EC, 0xAD50},   // P_GB_P3Q4
	{ 0x370C, 0x57CE},   // P_GB_P4Q0
	{ 0x370E, 0x16CE},   // P_GB_P4Q1
	{ 0x3710, 0xA6D3},   // P_GB_P4Q2
	{ 0x3712, 0xB551},   // P_GB_P4Q3
	{ 0x3714, 0x29F2},   // P_GB_P4Q4
	{ 0x3644, 0x0298},   // POLY_ORIGIN_C
	{ 0x3642, 0x01A8},   // POLY_ORIGIN_R
//BITFIELD= 0x3210, 0x0008, 1 //PGA_ENABLE

};

void Mi1330Refresh()
{
#if 1
	Write_MI_Vairable(MI_DRV_ID_SEQ|0X03|MIVAR_DATA_WIDTH_8_LOG, 6);
//	Write_MI_Vairable(MI_DRV_ID_SEQ|0X03|MIVAR_DATA_WIDTH_8_LOG, 5);
//	Write_MI_Vairable(MI_DRV_ID_SEQ|0X02|MIVAR_DATA_WIDTH_8_LOG,g_byMi1330SOCSeqMode);
	MI_PollWaitRefreshDone(200);
	return;
#else
	if(g_bySensorCurFormat==VGA_FRM)
	{
		Write_MI_Vairable(MI_DRV_ID_SEQ|0X03|MIVAR_DATA_WIDTH_8_LOG, 6);
	}
	else
	{
		Write_MI_Vairable(MI_DRV_ID_SEQ|0X03|MIVAR_DATA_WIDTH_8_LOG, 6);
		WaitTimeOut_Delay(5);
		Write_MI_Vairable(MI_DRV_ID_SEQ|0X03|MIVAR_DATA_WIDTH_8_LOG, 1);
		WaitTimeOut_Delay(10);
		Write_MI_Vairable(MI_DRV_ID_SEQ|0X03|MIVAR_DATA_WIDTH_8_LOG, 2);

	}
#endif

}




//find propriety setting
MI1330_FpsSetting_t*  GetMi1330FpsSetting(U8 Format,U8 Fps)
{
	U8 i;
	U8 Idx=0;
	if(g_bIsHighSpeed)
	{
		Idx=3; //8fps for uxga, 10 fps for vga
	}
	else
	{
		Idx = 1;
	}
	if(Format ==VGA_FRM)
	{
		for(i=0; i< (sizeof(g_staMi1330VGAFpsSetting)/sizeof(MI1330_FpsSetting_t)); i++)
		{
			if(g_staMi1330VGAFpsSetting[i].byFps ==Fps)
			{
				Idx=i;
				break;
			}
		}
		return &g_staMi1330VGAFpsSetting[Idx];

	}
	else //if(Format ==UXGA_FRM)
	{
		for(i=0; i< (sizeof(g_staMi1330SXGAFpsSetting)/sizeof(MI1330_FpsSetting_t)); i++)
		{
			if(g_staMi1330SXGAFpsSetting[i].byFps ==Fps)
			{
				Idx=i;
				break;
			}
		}
		return &g_staMi1330SXGAFpsSetting[Idx];
	}
}


void	SetMi1330OutputDim(U16 wWidth,U16 wHeight)
{
	if(g_bySensorCurFormat==VGA_FRM)
	{
		Write_MI_Vairable(MI_DRV_ID_MODE|0X03|MIVAR_DATA_WIDTH_16_LOG, wWidth);//output_Width_A;
		Write_MI_Vairable(MI_DRV_ID_MODE|0X05|MIVAR_DATA_WIDTH_16_LOG, wHeight);//output_ Height_A;
		Write_MI_Vairable(MI_DRV_ID_SEQ|0x03|MIVAR_DATA_WIDTH_8_LOG, 0x0001); //go to preview mode
		Write_MI_Vairable(MI_DRV_ID_SEQ|0x15|MIVAR_DATA_WIDTH_8_LOG, 0x00f0); //go to preview mode
		Write_MI_Vairable(MI_DRV_ID_SEQ|0x03|MIVAR_DATA_WIDTH_8_LOG, 0x0006); //refresh mode
		WaitTimeOut_Delay(2);
		Write_MI_Vairable(MI_DRV_ID_SEQ|0x03|MIVAR_DATA_WIDTH_8_LOG, 0x0005); //refresh
	}
	else
	{
		Write_MI_Vairable(MI_DRV_ID_MODE|0X07|MIVAR_DATA_WIDTH_16_LOG, wWidth);//output_Width_b;
		Write_MI_Vairable(MI_DRV_ID_MODE|0X09|MIVAR_DATA_WIDTH_16_LOG, wHeight);//output_ Height_b;
		Write_MI_Vairable(MI_DRV_ID_SEQ|0x03|MIVAR_DATA_WIDTH_8_LOG, 0x0002); //go to Capture mode
		Write_MI_Vairable(MI_DRV_ID_SEQ|0x15|MIVAR_DATA_WIDTH_8_LOG, 0x00f2); //go to capture mode,enable AE,AWB,and anti-flick
	}
	MI_PollWaitRefreshDone(200);
	//Mi1330Refresh();
	WaitTimeOut_Delay(2);
}


t_RegSettingWW code g_cMi1330VarSetting[]=
{
	////////////////////////
	//mode A
	////////////////////////
	//MISC settings 1
	{MI_DRV_ID_MODE|0x15|MIVAR_DATA_WIDTH_16_LOG,0x2111 }, //row speed A

	{MI_DRV_ID_MODE|0x17|MIVAR_DATA_WIDTH_16_LOG,0x046c}, //Read mode A

	{MI_DRV_ID_MODE|0x55|MIVAR_DATA_WIDTH_16_LOG, 0x0002}, //output_format_A, swap chrominance byte with luminance.
	//row start ,col start,row end ,col end.
	{MI_DRV_ID_MODE|0x0d|MIVAR_DATA_WIDTH_16_LOG, 0x0000},
	{MI_DRV_ID_MODE|0x0f|MIVAR_DATA_WIDTH_16_LOG, 0x0000},
//	{MI_DRV_ID_MODE|0x11|MIVAR_DATA_WIDTH_16_LOG, 973},
	{MI_DRV_ID_MODE|0x11|MIVAR_DATA_WIDTH_16_LOG, 1037},
	{MI_DRV_ID_MODE|0x13|MIVAR_DATA_WIDTH_16_LOG, 1293},

	//timing
	{MI_DRV_ID_MODE|0x19|MIVAR_DATA_WIDTH_16_LOG, 0x0ac }, //fine_correction
	{MI_DRV_ID_MODE|0x1b|MIVAR_DATA_WIDTH_16_LOG, 0x1f1}, //Fine integration time min
	{MI_DRV_ID_MODE|0x1d|MIVAR_DATA_WIDTH_16_LOG, 0x13f}, //Fine integration time max margin
	////////////////////
	//mode B
	////////////////////
	//misc setting
	{MI_DRV_ID_MODE|0x2B|MIVAR_DATA_WIDTH_16_LOG,0x2111 }, //row speed b
	{MI_DRV_ID_MODE|0x2D|MIVAR_DATA_WIDTH_16_LOG,0x0024}, //Read mode b
	{MI_DRV_ID_MODE|0x57|MIVAR_DATA_WIDTH_16_LOG, 0x0002}, //output_format_B, swap chrominance byte with luminance.
	//row start ,col start,row end ,col end.
	{MI_DRV_ID_MODE|0x23|MIVAR_DATA_WIDTH_16_LOG, 4},
	{MI_DRV_ID_MODE|0x25|MIVAR_DATA_WIDTH_16_LOG, 4},
	{MI_DRV_ID_MODE|0x27|MIVAR_DATA_WIDTH_16_LOG, 1035},
	{MI_DRV_ID_MODE|0x29|MIVAR_DATA_WIDTH_16_LOG, 1291},
	//timing
	{MI_DRV_ID_MODE|0x2f|MIVAR_DATA_WIDTH_16_LOG, 0x004C}, //fine_correction
	{MI_DRV_ID_MODE|0x31|MIVAR_DATA_WIDTH_16_LOG, 0xF9}, //Fine integration time min
	{MI_DRV_ID_MODE|0x33|MIVAR_DATA_WIDTH_16_LOG, 0xA7} //Fine integration time max margin
};


void Mi1330SetFormatFps(U8 SetFormat,U8 Fps)
{
	U16 wTmp1;
	U32 dwTmp1,dwTmp2;

	MI1330_FpsSetting_t *pMi1330_FpsSetting;
	DBG(("Mi1330 set format fps \n"));
	pMi1330_FpsSetting=GetMi1330FpsSetting( SetFormat, Fps);
	////////////////////////////////////////////////
	////PLL setting
	////////////////////////////////////////////////
	/*
	BITFIELD= 0x14, 1, 1         // Bypass PLL
	BITFIELD= 0X14, 2, 0         // Power-down PLL
	REG = 0x0014, 0x2145        //PLL control: BYPASS PLL = 8517
	REG = 0x0010, 0x011E        //PLL Dividers = 286
	REG = 0x0012, 0x0031        //PLL P Dividers = 49
	REG = 0x0014, 0x2545        //PLL control: TEST_BYPASS on = 9541
	REG = 0x0014, 0x2547        //PLL control: PLL_ENABLE on = 9543
	REG = 0x0014, 0x3447        //PLL control: SEL_LOCK_DET on = 13383
	DELAY = 1               // Allow PLL to lock
	REG = 0x0014, 0x3047        //PLL control: TEST_BYPASS off = 12359
	REG = 0x0014, 0x3046        //PLL control: PLL_BYPASS off = 12358
	*/
	Write_SenReg_Mask(0x0014, 0x0001, 0x0001);// Bypass PLL
	Write_SenReg_Mask(0x0014, 0x0000, 0x0002);// Power-down PLL
	Write_SenReg(0x0010, pMi1330_FpsSetting->wPLLCtlReg);
	Write_SenReg(0x0012, 0x0031);    //PLL divider
	Write_SenReg(0x0014, 0x2545);
	Write_SenReg(0x0014, 0x2547);
	Write_SenReg(0x0014, 0x3447);
	WaitTimeOut_Delay(1); //Allow PLL to lock
	Write_SenReg(0x0014, 0x3047);
	Write_SenReg(0x0014, 0x3046);
	WaitTimeOut_Delay(1); // now use PLL
///////////////////////////////////
	//YUV_Ycbcr_control
	//sRGB color space ,ITU 601 Range.
	Write_SenReg(0x337C, 0x000d);

	/////////////////////////////////////
	//antiflick setting
	dwTmp1   =  pMi1330_FpsSetting->wHorizClks*100000 ;
	dwTmp1 /= (pMi1330_FpsSetting->dwPixelClk/10); //line time, unit: 1us.
	//60Hz
	wTmp1     =  (1000000/120) / dwTmp1;                           //60Hz lines
	dwTmp2   =  (1000000/120) -  (dwTmp1*wTmp1);       //remainder
	if(dwTmp2 > (dwTmp1/2))
	{
		wTmp1+=1;
	}
	Write_MI_Vairable(MI_DRV_ID_ANTF|0x11|MIVAR_DATA_WIDTH_16_LOG,wTmp1 ); //60Hz R9 step
	Write_MI_Vairable(MI_DRV_ID_ANTF|0x15|MIVAR_DATA_WIDTH_16_LOG,wTmp1 ); //60Hz R9 step
	//50Hz
	wTmp1     =  (1000000/100) / dwTmp1;                           //50Hz lines
	dwTmp2   =  (1000000/100) -  (dwTmp1*wTmp1);       //remainder
	if(dwTmp2 > (dwTmp1/2))
	{
		wTmp1+=1;
	}
	Write_MI_Vairable(MI_DRV_ID_ANTF|0x13|MIVAR_DATA_WIDTH_16_LOG,wTmp1); //50Hz R9 step
	Write_MI_Vairable(MI_DRV_ID_ANTF|0x17|MIVAR_DATA_WIDTH_16_LOG,wTmp1); //50Hz R9 step
	wTmp1  /=5;

	Write_MI_Vairable(MI_DRV_ID_ANTF|0x8|MIVAR_DATA_WIDTH_8_LOG, wTmp1 ); //search_f1_50
	Write_MI_Vairable(MI_DRV_ID_ANTF|0x9|MIVAR_DATA_WIDTH_8_LOG, (wTmp1+2) ); //search_f2_50

	Write_MI_Vairable(MI_DRV_ID_ANTF|0xa|MIVAR_DATA_WIDTH_8_LOG, (wTmp1+3) ); //search_f1_60
	Write_MI_Vairable(MI_DRV_ID_ANTF|0xb|MIVAR_DATA_WIDTH_8_LOG, (wTmp1+5) ); //search_f2_60

	WriteMIVarSettingWW((sizeof(g_cMi1330VarSetting)/sizeof(t_RegSettingWW)), g_cMi1330VarSetting);


	if((g_bySensorSPFormat & SetFormat) == VGA_FRM)
	{
		//frame rate

		Write_MI_Vairable(MI_DRV_ID_MODE|0x1f|MIVAR_DATA_WIDTH_16_LOG, pMi1330_FpsSetting->wVertRows); //sensor Frame length lines A
		Write_MI_Vairable(MI_DRV_ID_MODE|0x21|MIVAR_DATA_WIDTH_16_LOG, pMi1330_FpsSetting->wHorizClks); //sensor line length pck A
	}
	else //SXGA
	{
		//frame rate
		Write_MI_Vairable(MI_DRV_ID_MODE|0x35|MIVAR_DATA_WIDTH_16_LOG, pMi1330_FpsSetting->wVertRows); //sensor Frame length lines B
		Write_MI_Vairable(MI_DRV_ID_MODE|0x37|MIVAR_DATA_WIDTH_16_LOG, pMi1330_FpsSetting->wHorizClks); //sensor line length pck B
	}


	Write_SenReg_Mask(0x3210, 0x0100,0x0100);//enable scaler.


	Write_SenReg(0x001e, 0x0203);// pixel clock slew rate:
	Write_SenReg(0x001a, 0x0210);//  parallel output enable
//	g_dwPixelClk =pMi1330_FpsSetting->dwPixelClk;
	g_wAECExposureRowMax =pMi1330_FpsSetting->wVertRows;

	SetMi1330ISPMisc();
}









void SetMi1330WindowCrop(U16 wXStart,U16 wXEnd,U16 wYStart,U16 wYEnd )
{

	//DBG(("@12-->%d,%d,%d,%d\n",wXStart, wXEnd, wYStart, wYEnd));
	if(g_bySensorCurFormat==VGA_FRM)
	{
		Write_MI_Vairable(MI_DRV_ID_MODE|0X39|MIVAR_DATA_WIDTH_16_LOG, wXStart);//crop_x0_A;
		Write_MI_Vairable(MI_DRV_ID_MODE|0X3b|MIVAR_DATA_WIDTH_16_LOG, wXEnd-1);//crop_x1_A;
		Write_MI_Vairable(MI_DRV_ID_MODE|0X3d|MIVAR_DATA_WIDTH_16_LOG, wYStart);//crop_Y0_A;
		Write_MI_Vairable(MI_DRV_ID_MODE|0X3f|MIVAR_DATA_WIDTH_16_LOG, wYEnd-1);//crop_Y1_A;
	}
	else
	{
		Write_MI_Vairable(MI_DRV_ID_MODE|0X47|MIVAR_DATA_WIDTH_16_LOG, wXStart);//crop_x0_B;
		Write_MI_Vairable(MI_DRV_ID_MODE|0X49|MIVAR_DATA_WIDTH_16_LOG, wXEnd-1);//crop_x1_B;
		Write_MI_Vairable(MI_DRV_ID_MODE|0X4b|MIVAR_DATA_WIDTH_16_LOG, wYStart);//crop_Y0_B;
		Write_MI_Vairable(MI_DRV_ID_MODE|0X4d|MIVAR_DATA_WIDTH_16_LOG, wYEnd-1);//crop_Y1_B;
	}
}

#ifndef _USE_BK_SPECIAL_EFFECT_
void SetMi1330SensorEffect(U8 byEffect)
{
	//switch(g_bySensorCurFormat)
	switch(byEffect)
	{
	case SNR_EFFECT_NEGATIVE:
		Write_MI_Vairable(MI_DRV_ID_MODE|0X59|MIVAR_DATA_WIDTH_16_LOG, 0x6443);
		Write_MI_Vairable(MI_DRV_ID_MODE|0X5B|MIVAR_DATA_WIDTH_16_LOG, 0x6443);
		break;
	case SNR_EFFECT_MONOCHROME:
		Write_MI_Vairable(MI_DRV_ID_MODE|0X59|MIVAR_DATA_WIDTH_16_LOG, 0x6441);
		Write_MI_Vairable(MI_DRV_ID_MODE|0X5B|MIVAR_DATA_WIDTH_16_LOG, 0x6441);
		break;
	case SNR_EFFECT_SEPIA:
		Write_MI_Vairable(MI_DRV_ID_MODE|0X59|MIVAR_DATA_WIDTH_16_LOG, 0x6442);
		Write_MI_Vairable(MI_DRV_ID_MODE|0X5B|MIVAR_DATA_WIDTH_16_LOG, 0x6442);
		break;
	default:
		Write_MI_Vairable(MI_DRV_ID_MODE|0X59|MIVAR_DATA_WIDTH_16_LOG, 0x6440);
		Write_MI_Vairable(MI_DRV_ID_MODE|0X5B|MIVAR_DATA_WIDTH_16_LOG, 0x6440);
		break;
	}
	Mi1330Refresh();
}
#endif
void SetMi1330ImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.
{
	Write_MI_Vairable(MI_DRV_ID_MODE|0X17|MIVAR_DATA_WIDTH_16_LOG, 0x046C|(U16)bySnrImgDir);//mode_sensor_mode_A
	Write_MI_Vairable(MI_DRV_ID_MODE|0X2d|MIVAR_DATA_WIDTH_16_LOG, 0x0024|(U16)bySnrImgDir);//mode_sensor_mode_B
	Mi1330Refresh();
}

void SetMi1330PwrLineFreq(U8 byFps, U8 bySetValue)
{
	U8 byTmp;

	byFps = byFps; // for delete warning
	if(bySetValue == PWR_LINE_FRQ_DIS)
	{
		Write_MI_Vairable(MI_DRV_ID_SEQ|0x18|MIVAR_DATA_WIDTH_8_LOG,1); //PreviewParEnter.fd
		Write_MI_Vairable(MI_DRV_ID_SEQ|0x1e|MIVAR_DATA_WIDTH_8_LOG,1); //PreviewPar.fd
		Write_MI_Vairable(MI_DRV_ID_SEQ|0x24|MIVAR_DATA_WIDTH_8_LOG,1); //PreviewParLeave.fd
		Write_MI_Vairable(MI_DRV_ID_SEQ|0x2a|MIVAR_DATA_WIDTH_8_LOG,1); //CapParEnter.fd
		Write_MI_Vairable(MI_DRV_ID_ANTF|0x04|MIVAR_DATA_WIDTH_8_LOG,0x00); //CapParEnter.fd

	}
	else
	{
		Write_MI_Vairable(MI_DRV_ID_SEQ|0x18|MIVAR_DATA_WIDTH_8_LOG,2); //PreviewParEnter.fd
		Write_MI_Vairable(MI_DRV_ID_SEQ|0x1e|MIVAR_DATA_WIDTH_8_LOG,2); //PreviewPar.fd
		Write_MI_Vairable(MI_DRV_ID_SEQ|0x24|MIVAR_DATA_WIDTH_8_LOG,2); //PreviewParLeave.fd
		Write_MI_Vairable(MI_DRV_ID_SEQ|0x2a|MIVAR_DATA_WIDTH_8_LOG,2); //CapParEnter.fd
		if(bySetValue == PWR_LINE_FRQ_50)//50Hz
		{

			byTmp = 0xc0;
		}
		else//60Hz
		{
			byTmp = 0x80;
		}
		Write_MI_Vairable(MI_DRV_ID_ANTF|0x04|MIVAR_DATA_WIDTH_8_LOG,byTmp); //CapParEnter.fd
	}

	Write_MI_Vairable(MI_DRV_ID_SEQ|0x3|MIVAR_DATA_WIDTH_8_LOG,0x06);

}

void SetMi1330LSC()
{

	//lens shading correction setting
	//cheney.cai 2009-06-24,do not select setting ,just replace setting when EEPROM configuration exist
	//if((g_byVdSOCIspSettingExistBitmap&VD_SNRISP_REGSETTING_LSC) == 0)
	{
		//LoadVdSensorSetting((U8*)gc_MI1330_SOC_LSC_Setting,sizeof(gc_MI1330_SOC_LSC_Setting)/5);
		WriteSensorSettingWW(sizeof(gc_MI1330_SOC_LSC_Setting)/4, gc_MI1330_SOC_LSC_Setting);
		Write_SenReg_Mask(0x3210, 0x0008, 0x0008);
	}

}

void SetMi1330CCM()
{

}
void SetMi1330AE()
{
	U8 byTmp;
	Write_MI_Vairable(MI_DRV_ID_SEQ|0x9|MIVAR_DATA_WIDTH_8_LOG,0x01); //AE fast buff,slowest AE speed
	Write_MI_Vairable(MI_DRV_ID_SEQ|0xa|MIVAR_DATA_WIDTH_8_LOG,0x01); //AE fast step,smooth AE transition
	byTmp= (g_byOVAEW_Normal+g_byOVAEB_Normal)/2;
	Write_MI_Vairable(MI_DRV_ID_AE|0x4f|MIVAR_DATA_WIDTH_8_LOG,byTmp);//AE_BASE_TARGET.
	byTmp =(g_byOVAEW_Normal-g_byOVAEB_Normal)/2;
	Write_MI_Vairable(MI_DRV_ID_AE|0x7|MIVAR_DATA_WIDTH_8_LOG,byTmp);
}

void SetMi1330AWB()
{
	Write_MI_Vairable(MI_DRV_ID_SEQ|0xb|MIVAR_DATA_WIDTH_8_LOG,0x20); //Awb cont buff,slowest AWB speed
	Write_MI_Vairable(MI_DRV_ID_SEQ|0xc|MIVAR_DATA_WIDTH_8_LOG,0x02); //Awb cont step,smooth AWB transition
	Write_MI_Vairable(MI_DRV_ID_AWB|0x66|MIVAR_DATA_WIDTH_8_LOG,0x96); //Awb KR_L, A light R gain
	Write_MI_Vairable(MI_DRV_ID_AWB|0x67|MIVAR_DATA_WIDTH_8_LOG,0x92); //Awb KG_L, A light G gain
	Write_MI_Vairable(MI_DRV_ID_AWB|0x68|MIVAR_DATA_WIDTH_8_LOG,0x96); //Awb KB_L, A light B gain
	Write_MI_Vairable(MI_DRV_ID_AWB|0x69|MIVAR_DATA_WIDTH_8_LOG,0x85); //Awb KR_R, Day light R gain
	Write_MI_Vairable(MI_DRV_ID_AWB|0x6A|MIVAR_DATA_WIDTH_8_LOG,0x80); //Awb KG_R, Day light G gain
	Write_MI_Vairable(MI_DRV_ID_AWB|0x6B|MIVAR_DATA_WIDTH_8_LOG,0x77); //Awb KB_R, Day light B gain
}
void SetMi1330WBTempAuto(U8 bySetValue)
{
	if(bySetValue)
	{
		Write_MI_Vairable_Mask(MI_DRV_ID_SEQ|0x2|MIVAR_DATA_WIDTH_8_LOG, 0x04, 0x04);

		DBG(("Mi1330 AWB ON\n"));
	}
	else
	{
		Write_MI_Vairable_Mask(MI_DRV_ID_SEQ|0x2|MIVAR_DATA_WIDTH_8_LOG, 0x0, 0x04);
		DBG(("Mi1330 AWB OFF\n"));
	}
}



void SetMi1330WBTemp(U16 wSetValue)
{
	Write_MI_Vairable(MI_DRV_ID_AWB|0x53|MIVAR_DATA_WIDTH_8_LOG,MapWBTemp2MicronSensorWBPositon(wSetValue) );
}

void SetMi1330DPC()
{

}


void SetMi1330NRC()
{

}

void SetMi1330MISC()
{

}

void SetMi1330BackLightComp(U8 bySetValue)
{
	U8 byTmp1,byTmp2;

	if(bySetValue)
	{
		byTmp1 =(g_byOVAEW_BLC+ g_byOVAEB_BLC)/2;
		byTmp2 =(g_byOVAEW_BLC -g_byOVAEB_BLC)/2;
	}
	else
	{
		byTmp1 =(g_byOVAEW_Normal+ g_byOVAEB_Normal)/2;
		byTmp2 =(g_byOVAEW_Normal- g_byOVAEB_Normal)/2;
	}
	Write_MI_Vairable(MI_DRV_ID_AE|0x4f|MIVAR_DATA_WIDTH_8_LOG,byTmp1);//AE_BASE_TARGET.
	Write_MI_Vairable(MI_DRV_ID_AE|0x07|MIVAR_DATA_WIDTH_8_LOG,byTmp2);//AE_BASE_TARGET.
}

void CfgMi1330ControlAttr()
{
	g_bySensorSize = SENSOR_SIZE_SXGA;
	g_bySensorSPFormat = SXGA_FRM | VGA_FRM;

	{
//		memcpy(g_asOvCTT,gc_MI1330_CTT,sizeof(gc_MI1330_CTT));
	}

	{
		// AE backlight compensation parameter
		g_byOVAEW_Normal = MI1330_AEW;
		g_byOVAEB_Normal = MI1330_AEB;

		g_byOVAEW_BLC = g_byOVAEW_Normal + 0x20;
		g_byOVAEB_BLC = g_byOVAEB_Normal + 0x20;
	}

	{
		//g_bySV28VoltSel =  SV28_VOL_2V9;
		//g_bySV18VoltSel=  SV18_VOL_1V9;
		// hemonel 2010-09-08: support 1.8V sensor IO
#if (_CHIP_ID_ & _RTS5840_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V79;
#elif (_CHIP_ID_ & _RTS5832_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V85;
#elif (_CHIP_ID_ & _RTS5829B_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V82;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V80;
#endif
	}

//	if(g_byFormatCfgExist==0) // format config not exist, use default setting.
	{
		InitFormatFrameFps();
	}


}

#ifndef _USE_BK_HSBC_ADJ_
void SetMI1330Brightness(S16 swSetValue)
{
	S16 swTmp;
	//find  setting  first.
	swTmp= swSetValue;

	if(swTmp<0)
	{
		swTmp=0;
	}
	Write_MI_Vairable(MI_DRV_ID_MODE|0x5D|MIVAR_DATA_WIDTH_8_LOG,swTmp );
	Write_MI_Vairable(MI_DRV_ID_MODE|0x5E|MIVAR_DATA_WIDTH_8_LOG,swTmp);
	//Write_MI_Vairable(MI_DRV_ID_SEQ|0X03|MIVAR_DATA_WIDTH_8_LOG, 6);
	//Write_MI_Vairable(MI_DRV_ID_SEQ|0X03|MIVAR_DATA_WIDTH_8_LOG, 5);
	Mi1330Refresh();
}
#endif


void SetMi1330ISPMisc()
{
	SetMi1330LSC();
	SetMi1330CCM();
	SetMi1330AE();
	SetMi1330AWB();
	SetMi1330DPC();
	SetMi1330NRC();
	SetMi1330MISC();
}
U16 GetMi1330AEGain()
{
	U16 wRegValue;
	Read_SenReg(0x3028, &wRegValue);
	return (wRegValue<<1);
}

void SetMi1330IntegrationTimeAuto(U8 bySetValue)
{
}

void SetMi1330IntegrationTime(U16 wEspline)
{
}
#endif
