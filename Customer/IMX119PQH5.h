#ifndef _IMX119PQH5_H_
#define _IMX119PQH5_H_

void IMX119PQH5SetFormatFps(U16 SetFormat, U8 Fps);
void CfgIMX119PQH5ControlAttr(void);
void SetIMX119PQH5ImgDir(U8 bySnrImgDir);
void SetIMX119PQH5IntegrationTime(U16 wEspline);
void SetIMX119PQH5Exposuretime_Gain(float fExpTime, float fTotalGain);
void SetIMX119PQH5DynamicISP(U8 byAEC_Gain);
void IMX119PQH5_POR();
void SetIMX119PQH5DefGain();
void SetIMX119PQH5Gain(float fGain);
#endif // _IMX119PQH5_H_

