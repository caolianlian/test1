#include "Inc.h"


#ifdef RTS58XX_SP_S5K6A1

typedef struct S6A1_FpsSetting
{
	U8 byFps;
	U16 wSysClkDiv; // register 0x0302 0x0303
	U16 wExtraDummyPixel;
	U32 dwPixelClk;
} S6A1_FpsSetting_t;


OV_CTT_t code gc_S5K6A1_CTT[3] =
{

	{3000,0xEF,0x100,0x2AC},
	{4150,0x17C,0x100,0x226},
	{6500,0x1BF,0x100,0x122},

};

code S6A1_FpsSetting_t  g_staS5K6A1FpsSetting[]=
{
//  FPS     FrameLength	SysClkDiv	ExtraDummyPixel	pclk
	{3,		(4),			(3809),			12000000},
	{5,		(4),			(2285),			12000000},
	{7,		(2),			(3265),			24000000},
	{8,		(2),			(2788),			24000000},
	{9,		(2),			(2539),			24000000},
	{10,		(2),			(2285),			24000000},
	{15,		(2),			(1523),			24000000},
	{20,		(1),			(2285),			48000000},
	{25,		(1),			(1828),			48000000},
	{30,		(1),			(1523),			48000000},

};


t_RegSettingWB code gc_S5K6A1_Setting[] =
{
	{0x0100,0x00}, // streaming off
	{0x0103,0x01}, // sw reset
	{0x0305,0x08}, // clk divder
	{0x0101,0x03}, //Vfilp + H mirror
	{0x301C,0x35}, //APS
	{0x3016,0x05}, //Analog
	{0x3034,0x73}, //Analog
	{0x3037,0x01}, //Analog
	{0x3035,0x05}, //Analog
	{0x301E,0x00}, //Analog
	{0x301B,0xC0}, //Analog
	{0x3013,0x28}, //Analog
	{0x310C,0x50}, //dclk inv
	{0x3115,0x02}, //pclk delay
	{0x3117,0x0A}, //v&h sync strength
	{0x3118,0xAA}, //Data&pclk strength
	{0x0204,0x01}, //AGC

//	{0x0100,0x01}, //stream ON

	{0x034C,0x05},
	{0x034D,0x08},
	{0x034E,0x04},
	{0x034F,0x08},
	{0x0344,0x00},
	{0x0345,0x04},
	{0x0346,0x00},
	{0x0347,0x04},
	{0x0348,0x05},
	{0x0349,0x0B},
	{0x034A,0x04},
	{0x034B,0x0B},
	{0x0383,0x01},
	{0x0387,0x01},
	{0x0100,0x01},
};


static S6A1_FpsSetting_t*  GetS5K6A1FpsSetting(U8 Fps, S6A1_FpsSetting_t staS6A1FpsSetting[], U8 byArrayLen)
{
	U8 i;
	U8 Idx;


	if(g_bIsHighSpeed)
	{
		Idx=2; //8fps for uxga, 10 fps for vga
	}
	else
	{
		Idx = 0;
	}

	for(i=0; i< byArrayLen; i++)
	{
		if(staS6A1FpsSetting[i].byFps ==Fps)
		{
			Idx=i;
			break;
		}
	}
	return &staS6A1FpsSetting[Idx];
}

//this function is also called during Probe stage to calculate the transfer_size/LWM/HWM
void GetSensorPclkHsync(U16 wSensorSPFormat,U8 byFps)
{
	S6A1_FpsSetting_t *pS5K6A1_FpsSetting;

	wSensorSPFormat = wSensorSPFormat;
	pS5K6A1_FpsSetting=GetS5K6A1FpsSetting(byFps, g_staS5K6A1FpsSetting, sizeof(g_staS5K6A1FpsSetting)/sizeof(S6A1_FpsSetting_t));
	
	g_wSensorHsyncWidth = pS5K6A1_FpsSetting->wExtraDummyPixel; // this for manual exposure
	g_dwPclk = pS5K6A1_FpsSetting->dwPixelClk;		// this for scale speed
}

void S5K6A1SetFormatFps(U16 SetFormat, U8 Fps)
{
	U16 wTemp;
	S6A1_FpsSetting_t *pS5K6A1_FpsSetting;

	SetFormat = SetFormat;
	pS5K6A1_FpsSetting=GetS5K6A1FpsSetting(Fps, g_staS5K6A1FpsSetting, sizeof(g_staS5K6A1FpsSetting)/sizeof(S6A1_FpsSetting_t));

	WriteSensorSettingWB(sizeof(gc_S5K6A1_Setting)/3, gc_S5K6A1_Setting);

	wTemp = 1050;
	Write_SenReg(0x0340, INT2CHAR(wTemp, 1)); //Write frame length lines MSB
	Write_SenReg(0x0341, INT2CHAR(wTemp, 0)); //Write frame length lines LSB

	wTemp = pS5K6A1_FpsSetting->wExtraDummyPixel;
	Write_SenReg(0x0343, INT2CHAR(wTemp, 0)); //Write dummy pixel LSB
	Write_SenReg(0x0342, INT2CHAR(wTemp, 1)); //Write dummy pixel MSB

	wTemp = pS5K6A1_FpsSetting->wSysClkDiv;
	Write_SenReg(0x0302, INT2CHAR(wTemp, 1)); //Write sys clk div MSB
	Write_SenReg(0x0303, INT2CHAR(wTemp, 0)); //Write sys clk div LSB

	//g_wSensorHsyncWidth = pS5K6A1_FpsSetting->wExtraDummyPixel; // this for manual exposure
	//g_dwPclk = pS5K6A1_FpsSetting->dwPixelClk;		// this for scale speed
	GetSensorPclkHsync(SXGA_FRM,Fps);
	g_wAECExposureRowMax =  1044;	// this for max exposure time
	g_wAEC_LineNumber =  1050;	// this for AE insert dummy line algothrim

	g_wSensorWidthBefBLC = 1280;
	g_wSensorHeightBefBLC = 1024;
}

void S5K6A1_POR()
{
#ifdef _MIPI_EXIST_
	Init_MIPI();
#else
	Init_CCS();
#endif
	ENTER_SENSOR_RESET();

	// power on
	SensorPowerControl(SWITCH_ON,EN_DELAYTIME);
	uDelay(1);

	// Initialize I2C Controller Timing
	LEAVE_SENSOR_RESET();

	// clock select 24M
	if( g_bySSCEnable== TRUE)
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_SSC_24M);
	}
	else
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_96M|CCS_CLK_DIVIDER_4);
	}

	EnableSensorHCLK();
	g_bySensorIsOpen = SENSOR_OPEN;
//	WaitTimeOut_Delay(1);  //OV9726 spec request 8192clk


}


void CfgS5K6A1ControlAttr(void)
{
	U8 i;

	g_bySensorSize = SENSOR_SIZE_SXGA;
	g_wSensorSPFormat = SXGA_FRM;

	memcpy(g_asOvCTT,gc_S5K6A1_CTT,sizeof(gc_S5K6A1_CTT));


	{
#if (_CHIP_ID_ & _RTS5840_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V79;
#elif (_CHIP_ID_ & _RTS5832_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V85;
#elif (_CHIP_ID_ & _RTS5829B_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V82;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V80;
#endif
	}

	{
		//-------- configure high speed frame size-----------
		//video
		g_aVideoFormat[0].byaVideoFrameTbl[0] = 9;
		g_aVideoFormat[0].byaVideoFrameTbl[1]=F_SEL_640_480;
		g_aVideoFormat[0].byaVideoFrameTbl[2]=F_SEL_160_120;
		g_aVideoFormat[0].byaVideoFrameTbl[3]=F_SEL_176_144;
		g_aVideoFormat[0].byaVideoFrameTbl[4]=F_SEL_320_240;
		g_aVideoFormat[0].byaVideoFrameTbl[5]=F_SEL_352_288;
		g_aVideoFormat[0].byaVideoFrameTbl[6]=F_SEL_640_400;
		g_aVideoFormat[0].byaVideoFrameTbl[7]=F_SEL_1280_720;
		g_aVideoFormat[0].byaVideoFrameTbl[8]=F_SEL_1280_800;
		g_aVideoFormat[0].byaVideoFrameTbl[9]=F_SEL_1280_1024;

		//still image
		g_aVideoFormat[0].byaStillFrameTbl[0] = 9;
		g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_1280_1024;
		g_aVideoFormat[0].byaStillFrameTbl[2]=F_SEL_160_120;
		g_aVideoFormat[0].byaStillFrameTbl[3]=F_SEL_176_144;
		g_aVideoFormat[0].byaStillFrameTbl[4]=F_SEL_320_240;
		g_aVideoFormat[0].byaStillFrameTbl[5]=F_SEL_352_288;
		g_aVideoFormat[0].byaStillFrameTbl[6]=F_SEL_640_400;
		g_aVideoFormat[0].byaStillFrameTbl[7]=F_SEL_640_480;
		g_aVideoFormat[0].byaStillFrameTbl[8]=F_SEL_1280_720;
		g_aVideoFormat[0].byaStillFrameTbl[9]=F_SEL_1280_800;

		//-------- configure high speed FPS setting-----------

		for(i=0; i<10; i++)
		{
			/*640_480,
			160_120,
			176_144,
			320_200,
			320_240,
			352_288,
			640_400*/
			g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_15|FPS_20|FPS_25|FPS_30;
		}
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_1280_720] =FPS_7|FPS_5;//FPS_9|FPS_8|FPS_1;                             //bit10 F_1280_720
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_1280_800] =FPS_7|FPS_5;//FPS_9|FPS_8|FPS_1;                        //bit11 F_1280_800
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_1280_1024] =FPS_7|FPS_5;//FPS_9|FPS_8|FPS_1;                        //bit11 F_1280_1024

		//====== format type  setting ===========
		g_aVideoFormat[0].byFormatType = FORMAT_TYPE_YUY2;

#ifdef _ENABLE_MJPEG_
		memcpy(&(g_aVideoFormat[1]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));
		for(i=7; i<12; i++) //bit0-6 VGA and smaller size, 30fps max
		{
			g_aVideoFormat[1].waFrameFpsBitmap[i] =FPS_15|FPS_20|FPS_25|FPS_30;//|FPS_1|FPS_3|FPS_5|FPS_10;
		}
		g_aVideoFormat[1].byFormatType = FORMAT_TYPE_MJPG;
#endif

#ifdef _ENABLE_M420_FMT_
		memcpy(&(g_aVideoFormat[2]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

		g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_720] =FPS_11|FPS_5;
		g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_800] =FPS_11|FPS_5;
		g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_1024] =FPS_11|FPS_5;
		g_aVideoFormat[2].byFormatType = FORMAT_TYPE_M420;
#endif

	}

	{
		//Brightness - BrightnessItem

		//Contrast - ContrastItem

		//Saturation - SaturationItem

		//Hue - 	HueItem

		//Sharpness - SharpnessItem
		//	SharpnessItem.Min = 0;
		//	SharpnessItem.Max = 8;
		//	SharpnessItem.Res = 1;
		//	SharpnessItem.Def = 3;
		//	SharpnessItem.Last  =SharpnessItem.Def;

		// gamma - 	GammaItem

		// gain - GainItem

		// whitebalance - WhiteBalanceTempItem

		// backlight - BackLightCompItem

		// powerline -PwrLineFreqItem

		// exposure time -	ExposureTimeAbsolutItem

		// low light - LowLightCompItem
	}

	InitS5K6A1IspParams();
}


void SetS5K6A1IntegrationTime(U16 wEspline)
{
	U16 wCurDummy;

	Write_SenReg(0x0104, 1);

	wCurDummy = g_wAFRInsertDummylines + g_wAEC_LineNumber;

		//-----------Write  frame length------------
	if (wCurDummy%2 == 1)
	{
			wCurDummy++;
	}

	if (wEspline > wCurDummy)
	{
		wCurDummy= wEspline + 8;

	}

	Write_SenReg(0x0341, INT2CHAR(wCurDummy, 0));
	Write_SenReg(0x0340, INT2CHAR(wCurDummy, 1));
	WaitFrameSync(ISP_INT1, ISP_DATA_START_INT);

	Write_SenReg(0x0203, INT2CHAR(wEspline, 0));	// change exposure value low
	Write_SenReg(0x0202, INT2CHAR(wEspline, 1));	// change exposure value high
	Write_SenReg(0x0104, 0);
}

void SetS5K6A1Gain(float fGain)
{
}

void SetS5K6A1ImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.
{
	WaitFrameSync(ISP_INT1, ISP_DATA_START_INT);
	Write_SenReg_Mask(0x0101, bySnrImgDir, 0x03);
	WaitFrameSync(ISP_INT1, ISP_DATA_START_INT);

	bySnrImgDir &= 0x3;

	if (bySnrImgDir == 0x0)
	{
		SetBkWindowStart(0, 0);
	}
	else if (bySnrImgDir == 0x01)
	{
		SetBkWindowStart(1, 0);
	}
	else if (bySnrImgDir == 0x02)
	{
		SetBkWindowStart(0, 1);
	}
	else if (bySnrImgDir == 0x03)
	{
		SetBkWindowStart(1, 1);
	}

}

static U16 MapSnrGlbGain2SnrRegSetting(U16 wGain)
{
	U16  sensorGain=0;  

     if(wGain>128) {
        g_wDigitalGain = wGain -128;
        wGain =128;
     }
     else g_wDigitalGain =0;

    
    return wGain*2;
}

float SnrRegSetting2SnrGlbGain(U16 wSnrRegGain)
{
	return ((float)wSnrRegGain)/32.0;
}

//??? It is need making certain
void SetS5K6A1Exposuretime_Gain(float fExpTime, float fTotalGain)
{
	U16 wExposureRows_floor;
	U16  wGainRegSetting; //data
	float fSnrGlbGain;
	float fExposureRows;
	U16 wSetDummy;
	float fTemp;
	U16 wExposurePixels;
	U16 wDigitalGain;

	//wGainRegSetting=MapSnrGlbGain2SnrRegSetting2((U16)(fTotalGain*16.0));
	//fSnrGlbGain = SnrRegSetting2SnrGlbGain(wGainRegSetting);

	wGainRegSetting = (U16)(fTotalGain*32.0);
	fSnrGlbGain = ((float)wGainRegSetting)/32.0;

	fExposureRows = fExpTime/g_fSensorRowTimes;	//unit : 1row
	wExposureRows_floor = (U16)(fExposureRows);
	fTemp = fExpTime-((float)wExposureRows_floor*(g_fSensorRowTimes)); //fine integration time;unit:  us
	wExposurePixels =(U16)(fTemp * (float)(g_dwPclk/10000)) ; //unit: pixels

	if(fabs(g_fCurExpTime-fExpTime) > 0.0001)
	{
		wSetDummy = g_wAFRInsertDummylines + g_wAEC_LineNumber;

		//-----------Write  frame length------------
		if (wSetDummy%2 == 1)
		{
			wSetDummy++;
		}

		//dummy line take effect earlier than exposure time so
		//we move dummy register setting outside group register hold
		//and add one frame delay
		Write_SenReg(0x0341, INT2CHAR(wSetDummy, 0));
		Write_SenReg(0x0340, INT2CHAR(wSetDummy, 1));

		WaitFrameSync(ISP_INT1, ISP_DATA_START_INT);

		// group register hold
		Write_SenReg(0x0104, 1);

		//zhangbo :add  fine integration time min and max limit 2011/4/25
		wExposurePixels= ClipWord(wExposurePixels, 0x17E, 1280);

		//-----------Write Exposuretime setting-----------
		Write_SenReg(0x0203, INT2CHAR(wExposureRows_floor, 0));	// change exposure value low
		Write_SenReg(0x0202, INT2CHAR(wExposureRows_floor, 1));	// change exposure value high
		Write_SenReg(0x0201, INT2CHAR(wExposurePixels, 0));	// change exposure value low
		Write_SenReg(0x0200, INT2CHAR(wExposurePixels, 1));	// change exposure value high

		//----------- write gain setting-------------------
		Write_SenReg(0x0205, INT2CHAR(wGainRegSetting, 0));
		Write_SenReg(0x0204, INT2CHAR(wGainRegSetting, 1));

		//----------- write digital gain setting-------------------
		wDigitalGain = 0x100 + g_wDigitalGain;
		// Write_SenReg(0x0204, wGainRegSetting);
		Write_SenReg(0x020e, INT2CHAR(wDigitalGain, 1));  
		Write_SenReg(0x020f, INT2CHAR(wDigitalGain, 0));	
		Write_SenReg(0x0210, INT2CHAR(wDigitalGain, 1));  
		Write_SenReg(0x0211, INT2CHAR(wDigitalGain, 0));
		Write_SenReg(0x0212, INT2CHAR(wDigitalGain, 1));  
		Write_SenReg(0x0213, INT2CHAR(wDigitalGain, 0));
		Write_SenReg(0x0214, INT2CHAR(wDigitalGain, 1));  
		Write_SenReg(0x0215, INT2CHAR(wDigitalGain, 0));

		SetISPAEGain(fTotalGain, fSnrGlbGain, 1);

		// group register release
		Write_SenReg(0x0104, 0);
		g_fCurExpTime-fExpTime;	// back up exposure rows
	}
	else
	{
		// group register hold
		Write_SenReg(0x0104, 1);

		wSetDummy = g_wAFRInsertDummylines + g_wAEC_LineNumber;

		if (wSetDummy%2 == 1)
		{
			wSetDummy++;
		}
		Write_SenReg(0x0341, INT2CHAR(wSetDummy, 0));
		Write_SenReg(0x0340, INT2CHAR(wSetDummy, 1));
		
		// write gain setting
		Write_SenReg(0x0205, INT2CHAR(wGainRegSetting, 0));
		Write_SenReg(0x0204, INT2CHAR(wGainRegSetting, 1));

		//----------- write digital gain setting-------------------
		wDigitalGain = 0x100 + g_wDigitalGain;
		// Write_SenReg(0x0204, wGainRegSetting);
		Write_SenReg(0x020e, INT2CHAR(wDigitalGain, 1));  
		Write_SenReg(0x020f, INT2CHAR(wDigitalGain, 0));	
		Write_SenReg(0x0210, INT2CHAR(wDigitalGain, 1));  
		Write_SenReg(0x0211, INT2CHAR(wDigitalGain, 0));
		Write_SenReg(0x0212, INT2CHAR(wDigitalGain, 1));  
		Write_SenReg(0x0213, INT2CHAR(wDigitalGain, 0));
		Write_SenReg(0x0214, INT2CHAR(wDigitalGain, 1));  
		Write_SenReg(0x0215, INT2CHAR(wDigitalGain, 0));

		SetISPAEGain(fTotalGain, fSnrGlbGain, 0);

		// group register release
		Write_SenReg(0x0104, 0);
	}
	return;

}

void InitS5K6A1IspParams()
{
	g_wAWBRGain_Last= 0x127;
	g_wAWBGGain_Last= 0x100;
	g_wAWBBGain_Last= 0x210;

	g_wProjectGR = g_wAWBRGain_Last;
	g_wProjectGB = g_wAWBBGain_Last;

	// Special ISP
	g_bySaturation_Def =64;
	g_byContrast_Def = 32;

	//g_wDynamicISPEn = DYNAMIC_LSC_EN | DYNAMIC_SHARPPARAM_EN | DYNAMIC_GAMMA_EN | DYNAMIC_CCM_CT_EN;
}

void SetS5K6A1DynamicISP(U8 byAEC_Gain)
{
	byAEC_Gain = byAEC_Gain;
}

void SetS5K6A1DynamicISP_AWB(U16 wColorTempature)
{
	wColorTempature = wColorTempature;
}
#endif
