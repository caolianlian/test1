
//#include "CamReg.h"
#include "Inc.h"

#ifdef RTS58XX_SP_IMX188PQ

U8   gc_digital_gain_h;
U8  gc_digital_gain_l;

//#define IMX188PQ_SIZE_720P	//Jimmy.20111214.default close it.
//#define SXGA_RAW8	//Jimmy.20111214.default close it.used for RAW8 debug only.

void InitIMX188PQIspParams(void );

#ifdef _ENABLE_OUTDOOR_DETECT_
Dyanmic_AWB_Boundary_t code gc_AWBDynmaicBoundary={28,-8,16,8,50,30,2,21};
#endif


code OV_FpsSetting_t  g_staIMX188PQHD720PFpsSetting[]=
{
//  FPS         ExtraDummyPixel  clkrc 	    pclk
	{5,		(18000),		(2),		81000000},
	{10,		(9000),		(2), 		81000000},	
	{15,		(6000),		(2), 		81000000},
	{20,		(4500),		(2), 		81000000},
	{25,		(3600),		(2),		81000000},
	{30,		(3000),		(2),		81000000},
};


OV_CTT_t code gc_IMX188PQ_CTT[3] =
{
	{3000,0x120,0x100,0x280},
	{4150,0x1a0,0x100,0x210},
	{6500,0x1e6,0x100,0x160},
};




t_RegSettingWB code gc_IMX188PQ_Global_Setting[] =
{
	{0x0103,0x01},
	{0x0101,0x03},

	//Global Setting
	{0x3094,0x32},
	{0x309A,0xA3},
	{0x309E,0x00},
	{0x3166,0x1C},
	{0x3167,0x1B},
	{0x3168,0x32},
	{0x3169,0x31},
	{0x316A,0x1C},
	{0x316B,0x1B},
	{0x316C,0x32},
	{0x316D,0x31},
};

t_RegSettingWB code gc_IMX188PQ_PLL_Setting[] =
{	
	//PLL Setting
	{0x0305,0x04}, //pre_pll_clk_div[7:0]
	{0x0307,0x87}, //pll_multiplier[7:0] 
	{0x303C,0x4B},
	{0x30A4,0x02},
};

t_RegSettingWB code gc_IMX188PQ_HD800P_Mode_Setting[] =
{
	//Mode Setting
	{0x0112,0x0A}, //CCP_DT_FMT[15:8]        
	{0x0113,0x0A}, //CCP_DT_FMT[7:0]         
	{0x0340,0x03}, //frame_length_lines[15:8]
	{0x0341,0x84}, //frame_length_lines[7:0] 
	{0x0342,0x0B}, //line_length_pck[15:8]   
	{0x0343,0xB8}, //line_length_pck[7:0]    
	{0x0344,0x00}, //x_addr_start[15:8]      
	{0x0345,0x00}, //x_addr_start[7:0]       
	{0x0346,0x00}, //y_addr_start[15:8]      
	{0x0347,0x00}, //y_addr_start[7:0]       
	{0x0348,0x05}, //x_addr_end[15:8]        
	{0x0349,0x1F}, //x_addr_end[7:0]         
	{0x034A,0x03}, //y_addr_end[15:8]        
	{0x034B,0x30}, //y_addr_end[7:0]         
	{0x034C,0x05}, //x_output_size[15:8]     
	{0x034D,0x20}, //x_output_size[7:0]      
	{0x034E,0x03}, //y_output_size[15:8]     
	{0x034F,0x31}, //y_output_size[7:0]      
	{0x0381,0x01}, //x_even_inc[3:0]         
	{0x0383,0x01}, //x_odd_inc[3:0]          
	{0x0385,0x01}, //y_even_inc[7:0]         
	{0x0387,0x01}, //y_odd_inc[7:0]          
	{0x3040,0x08}, //Y_OPBADD_STA[6:0]       
	{0x3041,0x97}, //Y_OPBADD_END[6:0]       
	{0x3048,0x00},
	{0x304E,0x0A},
	{0x3050,0x02},
	//{0x309B,0x08},//Change Normal input range to Widen input range of AD converter table : add by Shao-Hsiang
	{0x309B,0x00},
	{0x30D5,0x00},
	{0x31A1,0x07}, //LowPower
	{0x31A2,0x65}, //LowPower
	{0x31A3,0x04}, //LowPower
	{0x31A4,0xFC}, //LowPower
	{0x31A5,0x08}, //LowPower
	{0x31AA,0x65}, //LowPower
	{0x31AB,0x04}, //LowPower
	{0x31AC,0x60}, //LowPower
	{0x31AD,0x09}, //LowPower
	{0x31B0,0x00}, //LowPower                
	{0x3301,0x05}, //MIPI CLK free running   
	{0x3318,0x66}, //MIPI Global Timing Table
	
	//Streaming
	{0x0100,0x01},
};

static OV_FpsSetting_t*  GetOvFpsSetting(U8 Fps, OV_FpsSetting_t staOvFpsSetting[], U8 byArrayLen)
{
	U8 i;
	U8 Idx;

	ISP_MSG((" array size = %bd\n", byArrayLen));

	if(g_bIsHighSpeed)
	{
		Idx=2; //8fps for uxga, 10 fps for vga
	}
	else
	{
		Idx = 0;
	}

	for(i=0; i< byArrayLen; i++)
	{
		ISP_MSG((" fps i = %bd\n",i));
		if(staOvFpsSetting[i].byFps ==Fps)
		{
			Idx=i;
			break;
		}
	}
	ISP_MSG((" get fps idx = %bd\n",Idx));
	return &staOvFpsSetting[Idx];
}

static U16 MapSnrGlbGain2SnrRegSetting(U16 wGain)
{
	U16  sensorGain=0;
	float temp;

       if(wGain>128)    //Use Analog Gain here 128=8*16
       {
           	sensorGain = 256 - (U16)(4096/(U16)128);

		//set digital gain of sensor	
		temp = (float)wGain /(float)128;
		gc_digital_gain_h = (U8)temp;
		gc_digital_gain_l = (temp-(float)gc_digital_gain_h)*(float)256;
		//XBYTE[ISP_AE_GAIN_CTRL] |= 0X80;		
       }
	else//max analog gain is 8 times
	{
		sensorGain = 256 - (U16)(4096/(U16)wGain);
		gc_digital_gain_h=1;
		gc_digital_gain_l = 0;
		//XBYTE[ISP_AE_GAIN_CTRL] &= 0X7F;
	}

	return sensorGain;
}

float SnrRegSetting2SnrGlbGain(U16 wSnrRegGain)
{
	return 256.0/(256.0-(float)wSnrRegGain)*(float)((gc_digital_gain_h<<8)|gc_digital_gain_l)/256.0;
}

//this function is also called during Probe stage to calculate the transfer_size/LWM/HWM
void GetSensorPclkHsync(U16 wSensorSPFormat,U8 byFps)
{
	OV_FpsSetting_t *pIMX188PQ_FpsSetting= NULL;

	pIMX188PQ_FpsSetting=GetOvFpsSetting(byFps, g_staIMX188PQHD720PFpsSetting, sizeof(g_staIMX188PQHD720PFpsSetting)/sizeof(OV_FpsSetting_t));			

	g_wSensorHsyncWidth = pIMX188PQ_FpsSetting->wExtraDummyPixel; // this for manual exposure
	g_dwPclk = pIMX188PQ_FpsSetting->dwPixelClk;		// this for scale speed
}

void IMX188PQSetFormatFps(U16 SetFormat, U8 Fps)
{	
	U16 wTemp;
	OV_FpsSetting_t *pIMX188PQ_FpsSetting;			
	
	// hemonel 2010-04-13: for delete complier warning
	SetFormat = SetFormat;	
	//byFormatType = byFormatType;

	// initial all register setting
	WriteSensorSettingWB(sizeof(gc_IMX188PQ_Global_Setting)/3, gc_IMX188PQ_Global_Setting);
	//WaitTimeOut_Delay(20);
	WriteSensorSettingWB(sizeof(gc_IMX188PQ_PLL_Setting)/3, gc_IMX188PQ_PLL_Setting);
	WriteSensorSettingWB(sizeof(gc_IMX188PQ_HD800P_Mode_Setting)/3, gc_IMX188PQ_HD800P_Mode_Setting);
	//g_wMipiActiveWidth= 1296;

	// write fps setting
	pIMX188PQ_FpsSetting=GetOvFpsSetting(Fps, g_staIMX188PQHD720PFpsSetting, sizeof(g_staIMX188PQHD720PFpsSetting)/sizeof(OV_FpsSetting_t));
	Write_SenReg(0x0350, pIMX188PQ_FpsSetting->byClkrc);
	wTemp = pIMX188PQ_FpsSetting->wExtraDummyPixel;
	Write_SenReg(0x0343, INT2CHAR(wTemp, 0)); //Write dummy pixel MSB
	Write_SenReg(0x0342, INT2CHAR(wTemp, 1)); //Write dummy pixel LSB

	// update firmware variable	
	g_wSensorHsyncWidth =  pIMX188PQ_FpsSetting->wExtraDummyPixel; // this for manual exposure
	g_dwPclk = pIMX188PQ_FpsSetting->dwPixelClk;		// this for scale speed
	g_wAECExposureRowMax = 900-5;	//hemonel 2010-12-09: We use 7. At OV2710 When porting using manual exp control, the max exposure, at least should subtract 6
	g_wAEC_LineNumber = 900;   // this for AE insert dummy line algothrim	

	g_wSensorWidthBefBLC = 1280;
	g_wSensorHeightBefBLC = 800;     

	//g_byISPBLCSTART_X = 1;
	//g_byISPBLCSTART_Y = 1;

    //-----------------------------Other Setting--------------------------------
#ifdef _MIPI_EXIST_
    //-----------------------------Dphy Setting--------------------------------
	SetMipiDphy(MIPI_DATA_LANE0_EN, MIPI_DATA_FORMAT_RAW10, MIPI_DATA_TYPE_RAW10,HSTERM_EN_TIME_22);
#endif	

   
	return;
}

void CfgIMX188PQControlAttr(void)
{
	U8 i;

	g_bySensorSize =SENSOR_SIZE_HD800P;
	g_wSensorSPFormat = HD800P_FRM;
#ifdef _ENABLE_MJPEG_
	g_byQtableScale = 6;	//HD720P HD800P Setting=6 for 7 comp rate
#endif

	{
		memcpy(g_asOvCTT, gc_IMX188PQ_CTT,sizeof(gc_IMX188PQ_CTT));
	}

	{
#if (_CHIP_ID_ & _RTS5840_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_1V28;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V79;
#elif (_CHIP_ID_ & _RTS5832_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_1V31;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V85;
#elif (_CHIP_ID_ & _RTS5829B_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_1V29;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V80;
#endif
	}

	{
		//====== resolution  setting ===========

		// -- preview ---
		g_aVideoFormat[0].byaVideoFrameTbl[0] = 7;		// resolution number
		g_aVideoFormat[0].byaVideoFrameTbl[1]=F_SEL_640_480;
		g_aVideoFormat[0].byaVideoFrameTbl[2]=F_SEL_160_120;
		g_aVideoFormat[0].byaVideoFrameTbl[3]=F_SEL_176_144;
		g_aVideoFormat[0].byaVideoFrameTbl[4]=F_SEL_320_240;
//		g_aVideoFormat[0].byaVideoFrameTbl[5]=F_SEL_640_400;
		g_aVideoFormat[0].byaVideoFrameTbl[5]=F_SEL_352_288;
		g_aVideoFormat[0].byaVideoFrameTbl[6]=F_SEL_1280_720;
		g_aVideoFormat[0].byaVideoFrameTbl[7]=F_SEL_1280_800;

		//--still image--
		g_aVideoFormat[0].byaStillFrameTbl[0] = 1;	// resolution number
		g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_640_480;


		//====== fps  setting ===========
		{
			for(i=0; i<10; i++)
			{
				/*
					F_SEL_640_480    (0)
					F_SEL_160_120    (1)
					F_SEL_176_144    (2)
					F_SEL_320_180	(3)
					F_SEL_320_200    (4)
					F_SEL_320_240    (5)
					F_SEL_352_288    (6)
					F_SEL_424_240	(7)
					F_SEL_640_360	(8)
					F_SEL_640_400    (9)
				*/
				g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_30;
			}
			for(i=10; i<18; i++)
			{
				/*
					F_SEL_800_600    (10)
					F_SEL_848_480	(11)
					F_SEL_960_540	 (12)
					F_SEL_1024_768   (13)
					F_SEL_1280_720   (14)
					F_SEL_1280_800   (15)
					F_SEL_1280_960   (16)
					F_SEL_1280_1024  (17)
				*/
				g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_10;
			}
		}
		//====== format type  setting ===========
		g_aVideoFormat[0].byFormatType = FORMAT_TYPE_YUY2;

#ifdef _ENABLE_MJPEG_
		// copy YUY2 setting to MJPEG setting
		memcpy(&(g_aVideoFormat[1]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

		// modify MJPEG FPS setting
		for(i=10; i<18; i++)
		{
			g_aVideoFormat[1].waFrameFpsBitmap[i] = (FPS_30);
		}

		// modify MJPEG format type
		g_aVideoFormat[1].byFormatType = FORMAT_TYPE_MJPG;

#endif

#ifdef _ENABLE_M420_FMT_
		// copy YUY2 setting to M420 setting
		memcpy(&(g_aVideoFormat[2]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

		// modify M420 FPS setting
		g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_800_600] =FPS_15|FPS_10|FPS_5;//|FPS_1;             //bit07 F_800_600
		g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_720] =FPS_15|FPS_10|FPS_5;//|FPS_5|FPS_3|FPS_1;                    //bit08 F_960_720
		g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1024_768] =FPS_15|FPS_10|FPS_5;//|FPS_1;                    //bit09 F_1024_768
		g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_720] =FPS_10|FPS_5;//FPS_9|FPS_8|FPS_1;    //bit10 F_1280_720

		// modify M420 format type
		g_aVideoFormat[2].byFormatType = FORMAT_TYPE_M420;

#endif
	}

	InitIMX188PQIspParams();
}

void SetIMX188PQIntegrationTime(U16 wEspline)
{
	//-----------Write Exposuretime setting-----------
	Write_SenReg(0x0203, INT2CHAR(wEspline, 0));	// change exposure value low
	Write_SenReg(0x0202, INT2CHAR(wEspline, 1));	// change exposure value high 

	//----------- write gain setting-------------------	
	Write_SenReg(0x0205, 0x0f);	// fixed gain at manual exposure control
	Write_SenReg(0x0204, 0);	
}

void SetIMX188PQGain(float fGain)
{
}

void SetIMX188PQImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.
{
	Write_SenReg_Mask(0x0101, bySnrImgDir, 0x03);

	WaitFrameSync(ISP_INT1,ISP_DATA_END_INT);
   	switch(bySnrImgDir)
   	{
   	    case 0:
#ifdef _MIPI_EXIST_   	    
   	        SetBLCWindowStart(1, 0);
#endif       
   	        break;
   	    case 1:
#ifdef _MIPI_EXIST_  
   	        SetBLCWindowStart(1, 0);
#endif
   	        break;
   	    case 2:
#ifdef _MIPI_EXIST_  
   	        SetBLCWindowStart(0, 1);
#endif
   	        break;
   	    default:
   	    case 3:
#ifdef _MIPI_EXIST_  
   	        SetBLCWindowStart(0, 1);
#endif
               break;
   	}
	
	XBYTE[0x8520]= 0xC8;//ISP_NR_GRGB_CTRL

}

// wExpTime: unit with 1/10000 seconds
// byGain: unit with 1/16
void SetIMX188PQExposuretime_Gain(float fExpTime, float fTotalGain)
{
	U16 wExposureRows_floor;
	U16  wGainRegSetting; //data
	float fSnrGlbGain;
	float fExposureRows;
	U16 wSetDummy;
	//wGainRegSetting=MapSnrGlbGain2SnrRegSetting(wGain);

	wGainRegSetting=MapSnrGlbGain2SnrRegSetting((U16)(fTotalGain*16.0));
	fSnrGlbGain = SnrRegSetting2SnrGlbGain(wGainRegSetting);


	fExposureRows = fExpTime/g_fSensorRowTimes;	//unit : 1row
	wExposureRows_floor = (U16)(fExposureRows);

	// set dummy
	wSetDummy = g_wAFRInsertDummylines + g_wAEC_LineNumber;	
	
	if(fabs(g_fCurExpTime-fExpTime) > 0.0001)
	{	
		// group register hold
		Write_SenReg(0x0104, 0x01);
	
		//-----------Write  frame length or dummy lines------------	
		Write_SenReg(0x0341, INT2CHAR(wSetDummy, 0));	// change frame length value low
		Write_SenReg(0x0340, INT2CHAR(wSetDummy, 1));	// change frame length value high 	

		//-----------Write Exposuretime setting-----------		
		Write_SenReg(0x0203, INT2CHAR(wExposureRows_floor, 0));	// change exposure value low
		Write_SenReg(0x0202, INT2CHAR(wExposureRows_floor, 1));	// change exposure value high 	
		
		//----------- write gain setting-------------------	
		Write_SenReg(0x0205, INT2CHAR(wGainRegSetting, 0));

		//set digital gain
		Write_SenReg(0x020E,gc_digital_gain_h);
		Write_SenReg(0x020F,gc_digital_gain_l);
		
		Write_SenReg(0x0210,gc_digital_gain_h);
		Write_SenReg(0x0211,gc_digital_gain_l);
		
		Write_SenReg(0x0212,gc_digital_gain_h);
		Write_SenReg(0x0213,gc_digital_gain_l);

		Write_SenReg(0x0214,gc_digital_gain_h);
		Write_SenReg(0x0215,gc_digital_gain_l);

		SetISPAEGain(fTotalGain, fSnrGlbGain, 1);
		

		// group register release
		Write_SenReg(0x0104, 0x00);		
		g_fCurExpTime = fExpTime;	// back up exposure rows			
	}
	else
	{
		// group register hold
		Write_SenReg(0x0104, 0x01);

		//-----------Write  frame length or dummy lines------------	
		Write_SenReg(0x0341, INT2CHAR(wSetDummy, 0));	// change frame length value low
		Write_SenReg(0x0340, INT2CHAR(wSetDummy, 1));	// change frame length value high 
		
		//----------- write gain setting-------------------	
		Write_SenReg(0x0205, INT2CHAR(wGainRegSetting, 0));

		//set digital gain
		Write_SenReg(0x020E,gc_digital_gain_h);
		Write_SenReg(0x020F,gc_digital_gain_l);
		
		Write_SenReg(0x0210,gc_digital_gain_h);
		Write_SenReg(0x0211,gc_digital_gain_l);
		
		Write_SenReg(0x0212,gc_digital_gain_h);
		Write_SenReg(0x0213,gc_digital_gain_l);

		Write_SenReg(0x0214,gc_digital_gain_h);
		Write_SenReg(0x0215,gc_digital_gain_l);

		// group register release
		Write_SenReg(0x0104, 0x00);
	}
	return;
	
}

void InitIMX188PQIspParams(void )
{

	g_wAWBRGain_Last= 330;
	g_wAWBGGain_Last= 0x100;
	g_wAWBBGain_Last= 610;
	g_wProjectGR = g_wAWBRGain_Last;
	g_wProjectGB = g_wAWBBGain_Last;
//__________________________________
//	g_fAEC_EtGain = 1093.75;
//	g_wAEC_Gain = 47;
//	g_byAEC_Control = 0x1f;
//___________________________________

	// Special ISP
	//g_bySaturation_Def =68;
	//g_byContrast_Def = 33; //Neil Tuning at chicony
	g_byContrast_Def = 35; //Neil Tuning at chicony
//	g_byContrast_Def = 32; //Neil Tuning at chicony


	//g_wDynamicISPEn = DYNAMIC_SHARPPARAM_EN|DYNAMIC_SHARPNESS_EN|DYNAMIC_CCM_BRIGHT_EN|DYNAMIC_HDR_EN;
	    g_wDynamicISPEn = DYNAMIC_SHARPPARAM_EN | DYNAMIC_LSC_CT_EN;	


}

void IMX188PQ_POR()
{

#ifdef _MIPI_EXIST_
	Init_MIPI();
#else
	Init_CCS();
#endif
	ENTER_SENSOR_RESET();

	XBYTE[0xFEA1] |= 0x01;//GPIO output
	XBYTE[0xFEA7] |= 0x01;

	// power on
	SensorPowerControl(SWITCH_ON,EN_DELAYTIME);
	uDelay(1);

	// Initialize I2C Controller Timing
	LEAVE_SENSOR_RESET();

	// clock select 24M
	if( g_bySSCEnable== TRUE)
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_SSC_24M);
	}
	else
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_96M|CCS_CLK_DIVIDER_4);
	}

	EnableSensorHCLK();
	g_bySensorIsOpen = SENSOR_OPEN;
	WaitTimeOut_Delay(1);  //IMX188PQ spec request 8192clk
}

void SetIMX188PQDynamicISP(U8 byAEC_Gain)
{
	
	g_fAEC_HistPos_LH_Target = pow(g_byAEC_HistPos_Th_L, g_fAEC_HighContrast_Exp_L) * pow(g_byAEC_HistPos_Th_H, 1.0-g_fAEC_HighContrast_Exp_L);
	g_fAEC_HistPos_LH_Target_Th = pow(2, g_fAEC_HighContrast_Exp_L) * pow(10, 1.0-g_fAEC_HighContrast_Exp_L);


	byAEC_Gain = byAEC_Gain;
}

#endif
