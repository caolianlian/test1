
#include "Inc.h"

#ifdef RTS58XX_SP_OV7675



code OV_CTT_t gc_OV7675_CTT[3] =
{
	{3000,0x40,0x4C,0xAA},
	{4050,0x4C,0x40,0x7E},
	{6500,0x50,0x40,0x54},
};

// hemonel 2009-03-17: copy ov7725 fps
code OV_FpsSetting_t  g_staOV7675FpsSetting[]=
{
	//  FPS    extradummypixel clkrc   pclk = 24M/(clkrc+1)
	//  {1,     (1176+1),   (23),     1000000}, // 1M
	{3,         (0),        (9),          2000000}, // 2M   // 2.998
	{5,         (0),        (5),          2000000}, // 2M
	{8,         (0),        (3),          4000000}, // 4M   // 7.995
	{9,      (87+1),        (2),          4000000}, // 4M   // 8.995
	{10,        (0),        (2),          4000000}, // 4M
	{15,        (0),        (1),          6000000}, // 6M
	{20,        (392),      (0),         12000000}, // 12M
	{25,        (156),      (0),         12000000},  // 12M  // hemonel 2009-04-20: fix scale down abnormal to change 157 to 156
	{30,        (0),        (0),         12000000},  // 12M
};

code OV_FpsSetting_t  g_staOV7675FpsSetting_QVGA[]=
{
	//  FPS    extradummypixel clkrc   pclk = 24M/(clkrc+1)
	//  {1,     (1176+1),   (23),     1000000}, // 1M
	{3,         (0),       (28),         750000}, // 2M   // 2.998
	{5,         (0),       (17),         750000}, // 2M
	//  {8,         (196+1),    (5),        4000000}, // 4M   // 7.995
	//  {9,         (87+1),     (5),        4000000}, // 4M   // 8.995
	{10,        (64),       (7),        1325000}, // 10.1
	{15,        (0),        (5),        2000000}, // 14.99
	{20,        (60),       (3),        2650000}, // 20.1
	{25,        (96),       (2),        3400000}, // 25.01
	{30,        (0),        (1),        4000000}, // 29.98
};

code OV_FpsSetting_t  g_staOV7675FpsSetting_QQVGA[]=
{
	//  FPS    extradummypixel clkrc   pclk = 24M/(clkrc+1)
	//  {1,     (1176+1),   (23),     1000000}, // 1M
	{3,       (336),       (31),     2000000}, // 2M   // 2.998
	{5,         (9),       (31),     2000000}, // 2M
	//  {8,         (196+1),    (5),          4000000}, // 4M   // 7.995
	//  {9,         (87+1), (5),          4000000}, // 4M   // 8.995
	{10,        (32),       (14),      4000000}, // 4M
	{15,        (42),       (9),        650000}, // 15.4
	{20,        (0),        (7),        750000}, //
	{25,        (32),       (5),        915000}, // 24.86
	{30,        (0),        (4),       1000000}, //
};


t_RegSettingBB code gc_OV7675_Setting[] =
// ov7675.ovd VGA setting
{
	{0x12, 0x80},
	{0x09, 0x10},
	{0x11, 0x80},
	{0x6B, 0x0A},
	{0x3a, 0x04},

	{0xc1, 0x7f},
	{0x11, 0x80},
	{0x3d, 0xc3},
	{0x12, 0x00},
	{0x15, 0x00},

// resolution
	{0x17, 0x13},
	{0x18, 0x01},
	{0x32, 0xbf},
	{0x19, 0x03},
	{0x1a, 0x7b},
	{0x13, 0x0a},
	{0x0c, 0x00},
	{0x3e, 0x00},
	{0x70, 0x3a},
	{0x71, 0x35},
	{0x72, 0x11},
	{0x73, 0xf0},
	{0xa2, 0x02},


	//// AWB
	{0x43, 0x0a},
	{0x44, 0xf2},
	{0x45, 0x39},
	{0x46, 0x62},
	{0x47, 0x3d},
	{0x48, 0x55},
	{0x59, 0x83},
	{0x5a, 0x0d},
	{0x5b, 0xcd},
	{0x5c, 0x8c},
	{0x5d, 0x77},
	{0x5e, 0x16},
	{0x6c, 0x0a},
	{0x6d, 0x65},
	{0x6e, 0x11},
	{0x6f, 0x9f}, //9e,	//97 simple, 9e advanced



	{0x13, 0xe0},
	{0x00, 0x00},
	{0x10, 0x00},
	{0x0d, 0x40},
	{0x14, 0x18},

//;;AE target
//{0x24, 0x48},
//{0x25, 0x38},
	{0x26, 0xc2},//0x93},

	{0x9f, 0x78},
	{0xa0, 0x68},
	{0xa1, 0x03},
	{0xa6, 0xd8},
	{0xa7, 0xd8},
	{0xa8, 0xf0},
	{0xa9, 0x90},
	{0xaa, 0x14},
	{0x13, 0xe5},
	{0x0e, 0x61},
	{0x0f, 0x4b},
	{0x16, 0x02},
	{0x1e, 0x37},//mirror and flip
//{0x1e, 0x1F},
	{0x21, 0x02},
	{0x22, 0x91},
	{0x29, 0x07},
	{0x33, 0x0b},
	{0x35, 0x0b},
	{0x37, 0x1d},
	{0x38, 0x71},
	{0x39, 0x2a},
	{0x3c, 0x78},
	{0x4d, 0x40},
	{0x4e, 0x20},
	{0x69, 0x00},
	{0x6b, 0x0a},
	{0x74, 0x10},
	{0x8d, 0x4f},
	{0x8e, 0x00},
	{0x8f, 0x00},
	{0x90, 0x00},
	{0x91, 0x00},
	{0x96, 0x00},
	{0x9a, 0x80},
	{0xb0, 0x84},
	{0xb1, 0x0c},
	{0xb2, 0x0e},
	{0xb3, 0x82},
	{0xb8, 0x0a},

///DMax gamma
////Gamma
# if 0
	{0x7a, 0x09},
	{0x7b, 0x0c},
	{0x7c, 0x16},
	{0x7d, 0x28},
	{0x7e, 0x48},
	{0x7f, 0x57},
	{0x80, 0x64},
	{0x81, 0x71},
	{0x82, 0x7e},
	{0x83, 0x89},
	{0x84, 0x94},
	{0x85, 0xa8},
	{0x86, 0xba},
	{0x87, 0xd7},
	{0x88, 0xec},
	{0x89, 0xf9},
#else
	{0x7a, 0x20},
	{0x7b, 0x04},
	{0x7c, 0x0a},
	{0x7d, 0x1a},
	{0x7e, 0x3f},
	{0x7f, 0x4e},
	{0x80, 0x5b},
	{0x81, 0x68},
	{0x82, 0x75},
	{0x83, 0x7f},
	{0x84, 0x89},
	{0x85, 0x9a},
	{0x86, 0xab},
	{0x87, 0xbd},
	{0x88, 0xd3},
	{0x89, 0xe8},
#endif

	{0x6a, 0x40},
	{0x01, 0x56},
	{0x02, 0x44},
	{0x13, 0xe7},

//color matrix

//{0x4f, 0x60}, //80  20091224 Color matrix Final setting in pegatron//TEST2
//{0x50, 0x56}, //84
//{0x51, 0x0a}, //4
//{0x52, 0x0B}, //22
//{0x53, 0x6A}, //73
//{0x54, 0x75}, //94
//{0x58, 0x1e}, //1A Color matrix Final setting in pegatron
#if 1//realtek setting
	{0x4f, 0x87},//0x87 default
	{0x50, 0x68},
	{0x51, 0x1e},
	{0x52, 0x15},
	{0x53, 0x7c},
	{0x54, 0x91},
	{0x58, 0x1e},
#else//sunplus setting
	{0x4f, 0x5A},
	{0x50, 0xFA},
	{0x51, 0xFB},
	{0x52, 0xF8},
	{0x53, 0x3A},
	{0x54, 0xf4},
	{0x58, 0x1a},
#endif

	{0x55, 0x00},
	{0x56, 0x40},
	{0x57, 0x80},

	{0x41, 0x38}, //sharpness and de-noise
	{0x3f, 0x03},
	{0x75, 0x04}, // 20091224 Final seting in pegatron
	{0x76, 0x60},
	{0x4c, 0x00},
	{0x77, 0x06},
	{0x4b, 0x09},
	{0xc9, 0x30},


	{0x34, 0x11},
	{0x3b, 0x12},
	{0xa4, 0x88},//
	{0x96, 0x00},
	{0x97, 0x30},
	{0x98, 0x20},
	{0x99, 0x30},
	{0x9a, 0x84},
	{0x9b, 0x29},
	{0x9c, 0x03},

	{0x9d, 0x99},   //banding filter
	{0x9e, 0x7f},
	{0xa5, 0x02},
	{0xab, 0x02},


	{0x66, 0x05},
	{0x62, 0x00},
	{0x63, 0x00},
	{0x65, 0x00},
	{0x64, 0x14},
	{0x94, 0x14},
	{0x95, 0x16},

	{0x78, 0x04},
	{0x79, 0x01},
	{0xc8, 0xf0},
	{0x79, 0x0f},
	{0xc8, 0x00},
	{0x79, 0x10},
	{0xc8, 0x7e},
	{0x79, 0x0a},
	{0xc8, 0x80},
	{0x79, 0x0b},
	{0xc8, 0x01},
	{0x79, 0x0c},
	{0xc8, 0x0f},
	{0x79, 0x0d},
	{0xc8, 0x20},
	{0x79, 0x09},
	{0xc8, 0x80},
	{0x79, 0x02},
	{0xc8, 0xc0},
	{0x79, 0x03},
	{0xc8, 0x40},
	{0x79, 0x05},
	{0xc8, 0x30},
	{0x79, 0x26},
	{0x76, 0xe0},

//General control
	{0xa4, 0x82},
	{0x14, 0x38},
	{0x42, 0x40},
	{0x0d, 0x50},
	{0x56, 0x40},
	{0x55, 0x8f},


	{0x3b, 0xe2},
	{0xcf, 0x84}, //7.5fps


	{0x09, 0x00},   //end initial setting

};

t_RegSettingBB code gc_OV7675_Raw_Setting[] =
{
//  {0x12, 0x80},

	{0x11, 0x01},    //CLKRC
	{0x3a, 0x04},    //YUYV
	{0x12, 0x01},    //Raw Bayer RGB
	{0x17, 0x12},    // window
	{0x18, 0x00},    // window
	{0x32, 0xb6},    // window
	{0x19, 0x02},    // window
	{0x1a, 0x7a},    // window
	//{0x03, 0x00},    // window
	{0x03, 0x04},    // hemonel 2009-03-06:add 1 line for RTS5820 GRGR pattern alignment
	{0x0c, 0x00},    // scale disable
	{0x3e, 0x00},    // pclk divider: normal
	{0x70, 0x3a},    // scale
	{0x71, 0x35},    // scale
	{0x72, 0x11},    // scale
	{0x73, 0xf0},    // scale
	{0xa2, 0x02},    // scale

	// AEC configure
	{0x13, 0xe0},
	{0x00, 0x00},
	{0x10, 0x00},
	{0x0d, 0x40},
	{0x14, 0x18},
	{0xa5, 0x05},
	{0xab, 0x07},
	{0x24, 0x95},
	{0x25, 0x33},
	{0x26, 0xe3},
	{0x9f, 0x78},
	{0xa0, 0x68},
	{0xa1, 0x03},
	{0xa6, 0xd8},
	{0xa7, 0xd8},
	{0xa8, 0xf0},
	{0xa9, 0x90},
	{0xaa, 0x94},
	{0x13, 0xe5},

	{0x0e, 0x61},
	{0x0f, 0x4b},
	{0x16, 0x02},
	{0x1e, 0x07},
	{0x21, 0x02},
	{0x22, 0x91},
	{0x29, 0x07},
	{0x33, 0x0b},
	{0x35, 0x0b},
	{0x37, 0x1d},
	{0x38, 0x71},
	{0x39, 0x2a},
	{0x3c, 0x78},
	{0x3d, 0x08},
	{0x41, 0x3a},
	{0x4d, 0x40},
	{0x4e, 0x20},
	{0x69, 0x00},
	{0x6b, 0x4a},   // PLL x4
	{0x74, 0x10},
	{0x76, 0xe1},
	{0x8d, 0x4f},
	{0x8e, 0x00},
	{0x8f, 0x00},
	{0x90, 0x00},
	{0x91, 0x00},
	{0x96, 0x00},
	{0x9a, 0x80},
	{0xb0, 0x84},
	{0xb1, 0x0c},
	{0xb2, 0x0e},
	{0xb3, 0x82},
	{0xb8, 0x0a},

	// AWB control
	{0x43, 0x14},
	{0x44, 0xf0},
	{0x45, 0x34},
	{0x46, 0x58},
	{0x47, 0x28},
	{0x48, 0x3a},
	{0x59, 0x88},
	{0x5a, 0x88},
	{0x5b, 0x44},
	{0x5c, 0x67},
	{0x5d, 0x49},
	{0x5e, 0x0e},
	{0x6c, 0x0a},
	{0x6d, 0x55},
	{0x6e, 0x11},
	{0x6f, 0x9f},
	{0x6a, 0x40},
	{0x01, 0x40},
	{0x02, 0x40},
	{0x13, 0xE7},//0x00},   // hemonel 2009-6-30: enable AWB and AE

	//
	{0x34, 0x11},
	{0x3b, 0x02},
	{0xa4, 0x88},
	{0x96, 0x00},
	{0x97, 0x30},
	{0x98, 0x20},
	{0x99, 0x20},
	{0x9a, 0x84},
	{0x9b, 0x29},
	{0x9c, 0x03},
	{0x9d, 0x4c},
	{0x9e, 0x3f},
	{0x78, 0x04},

	{0x79, 0x01},
	{0xc8, 0xf0},
	{0x79, 0x0f},
	{0xc8, 0x00},
	{0x79, 0x10},
	{0xc8, 0x7e},
	{0x79, 0x0a},
	{0xc8, 0x80},
	{0x79, 0x0b},
	{0xc8, 0x01},
	{0x79, 0x0c},
	{0xc8, 0x0f},
	{0x79, 0x0d},
	{0xc8, 0x20},
	{0x79, 0x09},
	{0xc8, 0x80},
	{0x79, 0x02},
	{0xc8, 0xc0},
	{0x79, 0x03},
	{0xc8, 0x40},
	{0x79, 0x05},
	{0xc8, 0x30},
	{0x79, 0x26},
	// Created by Tools
	// 99 640 480
	// 98 0 0

	//find propriety setting
};

static OV_FpsSetting_t*  GetOvFpsSetting(U8 Fps, OV_FpsSetting_t staOvFpsSetting[], U8 byArrayLen)
{
	U8 i;
	U8 Idx;

	ISP_MSG((" array size = %bd\n", byArrayLen));

	if(g_bIsHighSpeed)
	{
		Idx=2; //8fps for uxga, 10 fps for vga
	}
	else
	{
		Idx = 0;
	}

	for(i=0; i< byArrayLen; i++)
	{
		ISP_MSG((" fps i = %bd\n",i));
		if(staOvFpsSetting[i].byFps ==Fps)
		{
			Idx=i;
			break;
		}
	}
	ISP_MSG((" get fps idx = %bd\n",Idx));
	return &staOvFpsSetting[Idx];
}

void GetSensorPclkHsync(U16 wSensorSPFormat, U8 byFps)
{
	OV_FpsSetting_t *pOv7675_FpsSetting;
	pOv7675_FpsSetting=GetOvFpsSetting(byFps, g_staOV7675FpsSetting, sizeof(g_staOV7675FpsSetting)/sizeof(OV_FpsSetting_t));
	g_wSensorHsyncWidth = 784+pOv7675_FpsSetting->wExtraDummyPixel;
	g_dwPclk= pOv7675_FpsSetting->dwPixelClk;	
}

void OV7675SetFormatFps(U8 SetFormat, U8 Fps)
{
	OV_FpsSetting_t *pOv7675_FpsSetting;
	SetFormat = SetFormat; // for delete warning
	Fps = Fps;

	pOv7675_FpsSetting=GetOvFpsSetting(Fps, g_staOV7675FpsSetting, sizeof(g_staOV7675FpsSetting)/sizeof(OV_FpsSetting_t));
	// 1) change hclk if necessary
	// ov7675 hclk at 24M ok

	// initial all register setting
	if(g_bySensor_YuvMode == RAW_MODE)
	{
		WriteSensorSettingBB(sizeof(gc_OV7675_Raw_Setting)>>1, gc_OV7675_Raw_Setting);
	}
	else
	{
		WriteSensorSettingBB(sizeof(gc_OV7675_Setting)>>1, gc_OV7675_Setting);
	}
	// 2) write sensor register for fps
	Write_SenReg_Mask(0x11, pOv7675_FpsSetting->byClkrc, 0x3F); // write CLKRC
	Write_SenReg(0x2B, INT2CHAR(pOv7675_FpsSetting->wExtraDummyPixel,0));   //Write dummy pixel LSB
	Write_SenReg_Mask(0x2A, (pOv7675_FpsSetting->wExtraDummyPixel)>>4, 0xF0); // Writedummy pixel MSB
	// 3) update variable for AE
	g_wAECExposureRowMax = (510-2);
	//g_wSensorHsyncWidth = 784+pOv7675_FpsSetting->wExtraDummyPixel;
	//g_dwPclk= pOv7675_FpsSetting->dwPixelClk;
	GetSensorPclkHsync(VGA_FRM, Fps);
	
}

void CfgOV7675ControlAttr(void)
{
	U8 i;

	g_bySensorSize = SENSOR_SIZE_VGA;
	g_wSensorSPFormat = VGA_FRM;

	{
		memcpy(g_asOvCTT,gc_OV7675_CTT,sizeof(gc_OV7675_CTT));
	}

	{
		// AE backlight compensation parameter
		g_byOVAEW_Normal = OV7675_AEW;
		g_byOVAEB_Normal = OV7675_AEB;

		g_byOVAEW_BLC = g_byOVAEW_Normal + 0x20;
		g_byOVAEB_BLC = g_byOVAEB_Normal + 0x20;
	}

	{
		//	XBYTE[REG_TUNESVIO_SVA] =  SV28_VOL_2V9|SV18_VOL_1V8;
		// hemonel 2010-09-08: support 1.8V sensor IO
#if (_CHIP_ID_ & _RTS5840_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V79;
#elif (_CHIP_ID_ & _RTS5832_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V85;
#elif (_CHIP_ID_ & _RTS5829B_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V82;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V80;
#endif
	}

//	if(g_byFormatCfgExist==0) // format config not exist, use default setting.
	{
		//====== resolution  setting ===========
		// -- preview ---
		g_aVideoFormat[0].byaVideoFrameTbl[0] = 6;		// resolution number
		g_aVideoFormat[0].byaVideoFrameTbl[1]=F_SEL_640_480;
		g_aVideoFormat[0].byaVideoFrameTbl[2]=F_SEL_160_120;
		g_aVideoFormat[0].byaVideoFrameTbl[3]=F_SEL_176_144;
		g_aVideoFormat[0].byaVideoFrameTbl[4]=F_SEL_320_240;
		g_aVideoFormat[0].byaVideoFrameTbl[5]=F_SEL_352_288;
		g_aVideoFormat[0].byaVideoFrameTbl[6]=F_SEL_640_480;
		//--still image--
		g_aVideoFormat[0].byaStillFrameTbl[0] = 6;	// resolution number
		g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_640_480;
		g_aVideoFormat[0].byaStillFrameTbl[2]=F_SEL_160_120;
		g_aVideoFormat[0].byaStillFrameTbl[3]=F_SEL_176_144;
		g_aVideoFormat[0].byaStillFrameTbl[4]=F_SEL_320_240;
		g_aVideoFormat[0].byaStillFrameTbl[5]=F_SEL_352_288;
		g_aVideoFormat[0].byaStillFrameTbl[6]=F_SEL_640_480;

		//====== fps  setting ===========
		for(i=0; i<7; i++)
		{
			/*640_480,
			160_120,
			176_144,
			320_200,
			320_240,
			352_288,
			640_400*/
			g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_15|FPS_20|FPS_25|FPS_30;
		}

		//====== format type  setting ===========
		g_aVideoFormat[0].byFormatType = FORMAT_TYPE_YUY2;

#ifdef _ENABLE_MJPEG_
		// copy YUY2 setting to MJPEG setting
		memcpy(&(g_aVideoFormat[1]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

		// modify MJPEG format type
		g_aVideoFormat[1].byFormatType = FORMAT_TYPE_MJPG;
#endif

#ifdef _ENABLE_M420_FMT_
		// copy YUY2 setting to M420 setting
		memcpy(&(g_aVideoFormat[2]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

		// modify M420 format type
		g_aVideoFormat[2].byFormatType = FORMAT_TYPE_M420;
#endif
	}


}

#ifndef _USE_BK_HSBC_ADJ_
void SetOV7675Brightness(S16 swSetValue)
{
	U16 wBrightSign;
	U16 wBrightVal;

	wBrightVal = abs(swSetValue);
	if(swSetValue<0)
	{
		wBrightVal += 0x80; // Brightness sign negative
	}
	Write_SenReg(0x55, wBrightVal); // write Brightness vale

	return;
}

void    SetOV7675Contrast(U16 wSetValue)
{
	Write_SenReg(0x56, wSetValue);

	return;
}

void    SetOV7675Saturation(U16 wSetValue)
{
	return;
}

void    SetOV7675Hue(S16 swSetValue)
{
	return;
}
#endif

// manual sharpness mode
void    SetOV7675Sharpness(U8 bySetValue)
{
	U8 bySharpLowerLimit, bySharpUpperLimit;

	switch(bySetValue)
	{
	case 0:
		bySharpLowerLimit = 0x01;
		bySharpUpperLimit = 0x02;
		break;
	case 1:
		bySharpLowerLimit = 0x01;
		bySharpUpperLimit = 0x02;
		break;
	case 2:
		bySharpLowerLimit = 0x01;
		bySharpUpperLimit = 0x03;
		break;
	case 3:
		bySharpLowerLimit = 0x02;
		bySharpUpperLimit = 0x04;
		break;
	case 4:
		bySharpLowerLimit = 0x02;
		bySharpUpperLimit = 0x06;
		break;
	case 5:
		bySharpLowerLimit = 0x02;
		bySharpUpperLimit = 0x08;
		break;
	case 6:
		bySharpLowerLimit = 0x02;
		bySharpUpperLimit = 0x0a;
		break;
	default:
		bySharpLowerLimit = 0x04;
		bySharpUpperLimit = 0x0f;
		break;
	}

	Write_SenReg_Mask(0x76, bySharpLowerLimit, 0x1F);
	Write_SenReg_Mask(0x75, bySharpUpperLimit, 0x1F);
}

#ifndef _USE_BK_SPECIAL_EFFECT_
void SetOV7675Effect(U8 byEffect)
{
	if(byEffect&SNR_EFFECT_NEGATIVE)
	{
		// negative image
		Write_SenReg_Mask(0x3A, 0x20, 0x20);
	}
	else
	{
		// not negative image
		Write_SenReg_Mask(0x3A, 0x00, 0x20);
	}

	if(byEffect&SNR_EFFECT_MONOCHROME)
	{
		// gray
	}
}
#endif

void SetOV7675ImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.
{
	Write_SenReg_Mask(0x1E, ((bySnrImgDir&0x01)<<5)|((bySnrImgDir&0x02)<<3), 0x30);
}
/*
void SetOV7675WindowCrop(U16 wXStart,U16 wXEnd,U16 wYStart,U16 wYEnd )
{


    DBG(("@12-->%d,%d,%d,%d\n",wXStart, wXEnd, wYStart, wYEnd));

    // recalculate window
    wXEnd = wXEnd - wXStart +16;    // width
    wYEnd = wYEnd - wYStart;    // height
    wXStart += 136;
    wYStart += 14;

    // update window
    Read_SenReg(0x32, &wTmp);
    Write_SenReg(0x17, wXStart>>2);
    Write_SenReg(0x18, wXEnd>>2);
    Write_SenReg(0x19, wYStart>>1);
    Write_SenReg(0x1A, wYEnd>>1);
    Write_SenReg(0x32, (wTmp&0x88)|((wYStart&0x01)<<6)|((wXStart&0x03)<<4)|((wYEnd&0x01)<<2)|(wXEnd&0x03));

    return;
}
*/

#define CCS_HSYNC_TCTL0 0xFECE
#define CCS_VSYNC_TCTL0 0xFED0

/*
void SetOV7675OutputDim(U16 wWidth,U16 wHeight)
{
    U8 COM3,COM14;
    U8 HSTART,HSTOP,HREF, VSTART;
    OV_FpsSetting_t *pOv7675_FpsSetting;
    U16 wAECExposureRowMax, wSensorHsyncWidth;

    wWidth = wWidth; // for delete warning

    XBYTE[CCS_HSYNC_TCTL0] = 0;
    XBYTE[CCS_VSYNC_TCTL0] = 0;
    Write_SenReg(0x09, 0x10);
    switch(wHeight)
    {
        case 240:   // QVGA: 320x240
            HSTART = 0x13;
            HSTOP = 0x04;
            HREF = 0x8a;
            COM3 = 0x04;
            COM14 = 0x18;
            VSTART = 0x03;
            //Write_SenReg(0x6b, 0xea);
            Write_SenReg(0xbc, 0x12);
            Write_SenReg(0xe6, 0x05);
            Write_SenReg(0xb9, 0x30);


            pOv7675_FpsSetting=GetOvFpsSetting(g_byCommitFPS, g_staOV7675FpsSetting_QVGA, sizeof(g_staOV7675FpsSetting_QVGA)/sizeof(OV_FpsSetting_t));
            wAECExposureRowMax = (270-2);
            wSensorHsyncWidth = 492+pOv7675_FpsSetting->wExtraDummyPixel;

            break;
        case 120:   // QQVGA: 160x120
            HSTART = 0x13;
            HSTOP = 0x04;
            HREF = 0xa5;        // hemonel 2009-02-09: get from ov QQVGA setting
            COM3 = 0x04;
            COM14 = 0x19;
            VSTART = 0x01;
            //Write_SenReg(0x6b, 0xea);
            Write_SenReg(0xbc, 0x12);
            Write_SenReg(0xe6, 0x09);
            Write_SenReg(0xbe, 0x01);
            //Write_SenReg(0x3e, 0x11);
            Write_SenReg(0xd2, 0x5c);
            Write_SenReg(0xb9, 0x30);
            Write_SenReg(0x6f, 0x97);


            pOv7675_FpsSetting=GetOvFpsSetting(g_byCommitFPS, g_staOV7675FpsSetting_QQVGA, sizeof(g_staOV7675FpsSetting_QQVGA)/sizeof(OV_FpsSetting_t));
            wAECExposureRowMax = (150-2);
            wSensorHsyncWidth = 332+pOv7675_FpsSetting->wExtraDummyPixel;

            break;

        case 288:   // CIF: 352x288
            HSTART = 0x13;
            HSTOP = 0x01;
            HREF = 0xB6;
            COM3 = 0x00;
            COM14 = 0x00;
            VSTART = 0x03;
            pOv7675_FpsSetting=GetOvFpsSetting(g_byCommitFPS, g_staOV7675FpsSetting, sizeof(g_staOV7675FpsSetting)/sizeof(OV_FpsSetting_t));
            wAECExposureRowMax = (510-2);
            wSensorHsyncWidth = 784+pOv7675_FpsSetting->wExtraDummyPixel;

            XBYTE[CCS_HSYNC_TCTL0] = (480 -288)/2;
            XBYTE[CCS_VSYNC_TCTL0] = (640 -352)/2;

            break;

        case 144:   // QCIF: 176x144
            HSTART = 0x13;
            HSTOP = 0x04;
            HREF = 0x8a;
            COM3 = 0x04;
            COM14 = 0x18;
            VSTART = 0x03;
            //Write_SenReg(0x6b, 0xea);
            Write_SenReg(0xbc, 0x12);
            Write_SenReg(0xe6, 0x05);
            Write_SenReg(0xb9, 0x30);


            pOv7675_FpsSetting=GetOvFpsSetting(g_byCommitFPS, g_staOV7675FpsSetting_QVGA, sizeof(g_staOV7675FpsSetting_QVGA)/sizeof(OV_FpsSetting_t));
            wAECExposureRowMax = (270-2);
            wSensorHsyncWidth = 492+pOv7675_FpsSetting->wExtraDummyPixel;

            XBYTE[CCS_HSYNC_TCTL0] = (240 -144)/2;
            XBYTE[CCS_VSYNC_TCTL0] = (320 -176)/2;
            break;



        default:        // VGA: 640x480
            HSTART = 0x13;
            HSTOP = 0x01;
            HREF = 0xB6;
            COM3 = 0x00;
            COM14 = 0x00;
            VSTART = 0x03;
            pOv7675_FpsSetting=GetOvFpsSetting(g_byCommitFPS, g_staOV7675FpsSetting, sizeof(g_staOV7675FpsSetting)/sizeof(OV_FpsSetting_t));
            wAECExposureRowMax = (510-2);
            wSensorHsyncWidth = 784+pOv7675_FpsSetting->wExtraDummyPixel;


            break;
    }


    Write_SenReg(0x17, HSTART);
    Write_SenReg(0x18, HSTOP);
    Write_SenReg(0x32, HREF);
    Write_SenReg(0x0C, COM3);
    Write_SenReg(0x3E, COM14);
    Write_SenReg(0x19, VSTART);


    //write sensor register for fps
    Write_SenReg_Mask(0x11, pOv7675_FpsSetting->byClkrc, 0x3F); // write CLKRC
    Write_SenReg(0x2B, INT2CHAR(pOv7675_FpsSetting->wExtraDummyPixel,0));   //Write dummy pixel LSB
    Write_SenReg_Mask(0x2A, (pOv7675_FpsSetting->wExtraDummyPixel)>>4, 0xF0); // Writedummy pixel MSB

    g_wAECExposureRowMax = wAECExposureRowMax;
    g_wSensorHsyncWidth = wSensorHsyncWidth;
    g_dwPclk= pOv7675_FpsSetting->dwPixelClk;

    Write_SenReg(0x09, 0x00);
}
*/

U16 CalAntiflickerStep(U8 byFps, U8 byFrq, U16 wRowNumber)
{
	return ((U16)byFps * wRowNumber/(U16)byFrq+1)/2; // round to integer
}

OV_BandingFilter_t  OV_SetBandingFilter(U8 byFps)
{
	OV_BandingFilter_t BdFilter;

	BdFilter.wD50Base= CalAntiflickerStep(byFps, 50, g_wAECExposureRowMax);//((U16)byFps*g_wAECExposureRowMax/(U16)50+1)/2;
	BdFilter.byD50Step= 100/byFps -1;

	BdFilter.wD60Base= CalAntiflickerStep(byFps, 60, g_wAECExposureRowMax);//((U16)byFps*g_wAECExposureRowMax/(U16)60+1)/2;
	BdFilter.byD60Step= 120/byFps -1;

	if(BdFilter.wD50Base<0x10)
	{
		BdFilter.wD50Base= CalAntiflickerStep(byFps, 25, g_wAECExposureRowMax);//(U16)byFps*g_wAECExposureRowMax/(U16)(100/2);
		BdFilter.byD50Step= (100/2)/byFps -1;
	}

	if(BdFilter.wD60Base<0x10)
	{
		BdFilter.wD60Base= CalAntiflickerStep(byFps, 30, g_wAECExposureRowMax);//(U16)byFps*g_wAECExposureRowMax/(U16)(120/2);
		BdFilter.byD60Step= (120/2)/byFps -1;
	}

	return BdFilter;
}
// anti-banding lines = (1/120)/ Tline for 60Hz, (1/100)/Tline for 50Hz
// Tline = 1/(fps*Exposure row max)
// anti-banding lines = (fps*exposure row max) /120 for 60Hz, (fps*exposure row max) /100 for 50Hz
void SetOV7675PwrLineFreq(U8 byFPS, U8 byLightFrq)
{
	U8 byCOM11;
	OV_BandingFilter_t BdFilter;

	// hemonel 2009-07-06: optimize code
	BdFilter = OV_SetBandingFilter(byFPS);

	// D50
	Write_SenReg(0x9D, BdFilter.wD50Base);
	Write_SenReg(0xA5, BdFilter.byD50Step);

	//D60
	Write_SenReg(0x9E, BdFilter.wD60Base);
	Write_SenReg(0xAB, BdFilter.byD60Step);

	if(PWR_LINE_FRQ_DIS == byLightFrq)
	{
		// disable banding filter, auto banding filter
		byCOM11 = 0x10;
	}
	else if(PWR_LINE_FRQ_50 == byLightFrq)
	{
		// select BD50 as banding filter
		byCOM11 = 0x08;
	}
	else
	{
		// select BD60 as banding filter
		byCOM11 = 0x00;
	}
	Write_SenReg_Mask(0x3B, byCOM11, 0x18);
}

void SetOV7675WBTemp(U16 wRgain, U16 wGgain, U16 wBgain)
{
//  OV_CTT_t ctt;

//  ctt = OV_SetColorTemperature(wSetValue);

	Write_SenReg(0x02, wRgain);
	Write_SenReg(0x6A, wGgain);
	Write_SenReg(0x01, wBgain);
}
/*
OV_CTT_t GetOV7675AwbGain(void)
{
    OV_CTT_t ctt;

    Read_SenReg(0x02, &ctt.wRgain);
    Read_SenReg(0x6A, &ctt.wGgain);
    Read_SenReg(0x01, &ctt.wBgain);

    return ctt;
}
*/
void SetOV7675WBTempAuto(U8 bySetValue)
{
	if(bySetValue)
	{
		// atuo white balance
		Write_SenReg_Mask(0x13, 0x02,0x02);
	}
	else
	{
		// manual white balance
		Write_SenReg_Mask(0x13, 0x00,0x02);
	}
}

void SetOV7675BackLightComp(U8 bySetValue)
{
	U8 byAEW,byAEB;

	if(bySetValue)
	{
		byAEW = g_byOVAEW_BLC;
		byAEB = g_byOVAEB_BLC;
	}
	else
	{
		byAEW = g_byOVAEW_Normal;
		byAEB = g_byOVAEB_Normal;
	}
	Write_SenReg(0x24, byAEW);
	Write_SenReg(0x25, byAEB);
}

void SetOV7675IntegrationTimeAuto(U8 bySetValue)
{
	if(bySetValue == EXPOSURE_TIME_AUTO_MOD_APERTPRO)
	{
		// auto exposure
		Write_SenReg_Mask(0x3B, 0x80, 0x80);    // enable night mode
		Write_SenReg_Mask(0x13, 0x05,  0x05);
	}
	else
	{
		// manual expsoure
		Write_SenReg_Mask(0x3B, 0x00, 0x80);    // close night mode
		Write_SenReg_Mask(0x13, 0x00,  0x05);
	}
}

void SetOV7675IntegrationTime(U16 wEspline)
{
	U16 wDummyline = 0;
	U16 wExpMax = g_wAECExposureRowMax-2;

	if(wEspline > wExpMax)
	{
		wDummyline = wEspline - wExpMax;
		wEspline = wExpMax;
	}

	DBG_SENSOR(("\n wEspline value = %u\n", wEspline));
	DBG_SENSOR(("wDummyline value = %u\n", wDummyline));
	// write exposure time and dummy line to register
	Write_SenReg_Mask(0x04, (wEspline&0x03),0x03);
	Write_SenReg(0x10, (wEspline>>2));
	Write_SenReg_Mask(0x07, (wEspline>>10),0x3F);
	Write_SenReg(0x2D, INT2CHAR(wDummyline, 0));
	Write_SenReg(0x2E, INT2CHAR(wDummyline, 1));
}
#endif

