#include "Inc.h"

#ifdef RTS58XX_SP_OV9724

OV_CTT_t code gc_OV9724_CTT[3] =
{
	{2900,0x112,0x100,0x250},
	{4600,0x190,0x100,0x190},
	{6600,0x1D8,0x100,0x124},
};

typedef struct OV_FpsSetting2
{
	U8 byFps;
	U16 wExtraDummyPixel;   // register 0x2a,0x2b: dummy pixel adjustment
	U8  byClkrc; // register 0x11: clock pre-scalar
	U8 byPclk_Period;
	U32 dwPixelClk;
} OV_FpsSetting2_t;

code OV_FpsSetting2_t  g_staOv9724FpsSetting[]=
{
// 	FPS ExtraDummyPixel clkrc	byPclk_Period		pclk
	{10,		(1576),		(6),		0xc6, 		12000000},
	{15,		(1576),		(4), 		0x4f,		18000000},
	{20,		(1576),		(3), 		0x43,		24000000},
	{30,		(1576),		(2),		0x36,		36000000}, 
};

//@@ MIPI10bit_1280x720_30fps
//;OmniVision Technologies Inc.
//;Sensor Setting Release Log
//;=============================================================
//;Sensor 	: OV9724
//;Sensor Rev	: Rev1a
//;
//;010912 v01		1) preliminary setting for wafer test
//;012312 v02		1) Set BLC_En as Default define
//;020212 v03		1) Add BLC_MAN define
//;          		2) Change r370a value for SUB formats
//;          		3) Add SW AEC/AGC
//;022112 v04		1) Adjust stxwidth by r3705 to be 0x67
//;          		2) Change 320x240 crop format for stxwidth
//;
//;=============================================================
t_RegSettingWB code gc_OV9724_mipi_Setting[] =
{
	{0x0103, 0x01},
	{0x3210, 0x43},
	{0x3705, 0x67},
	{0x0340, 0x02},
	{0x0341, 0xf8},
	{0x0342, 0x06},
	{0x0343, 0x28},
	{0x0202, 0x02},
	{0x0203, 0xf0},
	{0x4801, 0x0f},
	{0x4801, 0x8f},
	{0x4814, 0x2b},
	{0x4307, 0x3a},
	{0x5000, 0x06},
	{0x5001, 0x73},
	{0x0205, 0x3f},
	{0x0100, 0x01},

	//{0x5003, 0x01},
	{0x4009, 0xe7},  //BLC trigger by gain --> trigger by frame

	{0x034c, 0x05},  //x_output_size_High-->1284
	{0x034d, 0x04},  //x_output_size_Low         //Liany.0x00->0x04 for 5828 BLC
	{0x034e, 0x02},  //y_output_size_High-->721
	{0x034f, 0xd1},  //y_output_size_Low         //Liany.0xd0->0xd1 for 5828 BLC

	{0x4819, 0x09},    // manual set hs_zero_min value
	{0x4802, 0x04},    // bit 2 set hs_zero_min 1: manual 0: Auto

};

static OV_FpsSetting2_t*  GetOvFpsSetting(U8 Fps, OV_FpsSetting2_t staOvFpsSetting[], U8 byArrayLen)
{
	U8 i;
	U8 Idx;

	ISP_MSG((" array size = %bd\n", byArrayLen));

	if(g_bIsHighSpeed)
	{
		Idx=2; //8fps for uxga, 10 fps for vga
	}
	else
	{
		Idx = 0;
	}

	for(i=0; i< byArrayLen; i++)
	{
		ISP_MSG((" fps i = %bd\n",i));
		if(staOvFpsSetting[i].byFps ==Fps)
		{
			Idx=i;
			break;
		}
	}
	ISP_MSG((" get fps idx = %bd\n",Idx));
	return &staOvFpsSetting[Idx];
}

static U16 MapSnrGlbGain2SnrRegSetting(U16 wGain)
{
	U16  sensorGain=0;
	U16  GainTemp =0;
	U8  i;
	//QDBG(("byGain = %bd-> ",byGain));

	GainTemp = (U16)wGain;

	if(GainTemp>=256)
	{
		sensorGain = (GainTemp>>4);
	}
	else if(GainTemp>=128)
	{
		sensorGain = (GainTemp>>3);
	}
	else if(GainTemp>=64)
	{
		sensorGain = (GainTemp>>2);
	}
	else if(GainTemp>=32)
	{
		sensorGain = (GainTemp>>1);
	}
	else if(GainTemp < 32)
	{
		sensorGain = (GainTemp -16);
	}

	for(i=0; i<3; i++)
	{
		if(GainTemp >= 64)
		{
			GainTemp >>= 1;
			sensorGain |=  (0x01<<(i+5));
		}
	}

	//QDBG(("sensorGain = %d    ",sensorGain));
	return sensorGain;
}

float SnrRegSetting2SnrGlbGain(U16 wSnrRegGain)
{
	U16 wGain;
	U8 i;

	wGain = (wSnrRegGain&0x0f) + 16;
	wSnrRegGain >>= 4;

	for(i = 0; i < 4; i++)
	{
		if((wSnrRegGain&1) == 1)
		{
			wGain <<= 1;
		}

		wSnrRegGain >>= 1;
	}

	return (float)wGain/16.0;
}

void GetSensorPclkHsync(U16 wSensorSPFormat, U8 byFps)
{
	OV_FpsSetting2_t *pOv9724_FpsSetting;
	wSensorSPFormat = wSensorSPFormat;
	pOv9724_FpsSetting=GetOvFpsSetting(byFps, g_staOv9724FpsSetting, sizeof(g_staOv9724FpsSetting)/sizeof(OV_FpsSetting2_t));
	g_wSensorHsyncWidth = pOv9724_FpsSetting->wExtraDummyPixel; // this for manual exposure
	g_dwPclk = pOv9724_FpsSetting->dwPixelClk;		// this for scale speed
}

void OV9724SetFormatFps(U16 SetFormat, U8 Fps)
{
	U16 wTemp;
	OV_FpsSetting2_t *pOv9724_FpsSetting;

	// hemonel 2010-04-13: for delete complier warning
	SetFormat = SetFormat;

	pOv9724_FpsSetting=GetOvFpsSetting(Fps, g_staOv9724FpsSetting, sizeof(g_staOv9724FpsSetting)/sizeof(OV_FpsSetting2_t));
	// 1) change hclk if necessary

#ifdef _MIPI_EXIST_
	WriteSensorSettingWB(sizeof(gc_OV9724_mipi_Setting)/3, gc_OV9724_mipi_Setting);
#endif

	// 2) write sensor register
	//Write_SenReg(0x0305, 0x06);
	//wTemp = pOv9724_FpsSetting->wExtraDummyPixel;
	//Write_SenReg(0x0343, INT2CHAR(wTemp, 0)); //Write dummy pixel LSB
	//Write_SenReg(0x0342, INT2CHAR(wTemp, 1)); //Write dummy pixel MSB

	//WaitTimeOut_Delay(10);
	Write_SenReg(0x0303, pOv9724_FpsSetting->byClkrc);
	Write_SenReg(0x4837, pOv9724_FpsSetting->byPclk_Period);	// Add MIPI delay to fix PLL change bug.
	//WaitTimeOut_Delay(10);
	wTemp = pOv9724_FpsSetting->wExtraDummyPixel;
	Write_SenReg(0x0343, INT2CHAR(wTemp, 0)); //Write dummy pixel LSB
	//WaitTimeOut_Delay(10);
	Write_SenReg(0x0342, INT2CHAR(wTemp, 1)); //Write dummy pixel MSB

	//_________________________________________________________
	//if 0x3503=07
	// (1) set settings (2) (3)gain work (4)exposure work (5)dummy work

	//if 0x3503=17
	// (1) set settings (2) (3) (4)exposure work&gain work  (5)dummy work
	//Write_SenReg(0x3503, 0x17);
	//Write_SenReg(0x3819, 0x6C); // fix image blink at insert dummy

	// 3) update variable for AE
	//g_wSensorHsyncWidth = pOv9724_FpsSetting->wExtraDummyPixel; // this for manual exposure
	g_wAECExposureRowMax = 755;//835;	// this for max exposure time
	g_wAEC_LineNumber = 760;//760;	// this for AE insert dummy line algothrim

	g_wSensorWidthBefBLC = 1280;
	g_wSensorHeightBefBLC = 720;
	GetSensorPclkHsync(HD720P_FRM,Fps);
	//g_dwPclk = pOv9724_FpsSetting->dwPixelClk;		// this for scale speed


	//if (Fps == 5)
	//{
	//	Write_SenReg(0x340, 0x06);
	//	Write_SenReg(0x341, 0xA1);
	//	g_wAECExposureRowMax = 1692;
	//	g_wAEC_LineNumber = 1697;
	//}

	InitOV9724IspParamsByResolution(g_wCurFrameWidth, g_wCurFrameHeight);

#ifdef _MIPI_EXIST_
	//-----------------------------Dphy Setting--------------------------------

	SetMipiDphy(MIPI_DATA_LANE0_EN, \
	            MIPI_DATA_FORMAT_RAW10, MIPI_DATA_TYPE_RAW10, \
	            HSTERM_EN_TIME_22);

	/*
	if(byFormatType==JPEG_IMAGE)
	{
		SetMipiDphy(MIPI_BIT_SWITCH | MIPI_DATA_LANE0_EN, \
					MIPI_DATA_FORMAT_RAW10 | MIPI_YUV_TYPE_NULL, \
					MIPI_DATA_TYPE_RAW10,
					0x11);
	}
	else
	{
		if(Fps==10)
		{
			SetMipiDphy(MIPI_BIT_SWITCH | MIPI_DATA_LANE0_EN, \
						MIPI_DATA_FORMAT_RAW10 | MIPI_YUV_TYPE_NULL, \
						MIPI_DATA_TYPE_RAW10,
						0x13);
		}
		else
		{
			SetMipiDphy(MIPI_BIT_SWITCH | MIPI_DATA_LANE0_EN, \
						MIPI_DATA_FORMAT_RAW10 | MIPI_YUV_TYPE_NULL, \
						MIPI_DATA_TYPE_RAW10,
						0x11);
		}
	}*/

#endif

}

void CfgOV9724ControlAttr(void)
{
	U8 i;

	g_bySensorSize = SENSOR_SIZE_HD720P;
	g_wSensorSPFormat =HD720P_FRM;

	{
		memcpy(g_asOvCTT, gc_OV9724_CTT,sizeof(gc_OV9724_CTT));
	}

	{
		// hemonel 2010-09-08: support 1.8V sensor IO
#if (_CHIP_ID_ & _RTS5840_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V79;
#elif (_CHIP_ID_ & _RTS5832_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V85;
#elif (_CHIP_ID_ & _RTS5829B_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V82;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V80;
#endif
	}

	{
		//====== resolution  setting ===========

		// -- preview ---
		g_aVideoFormat[0].byaVideoFrameTbl[0] = 5;		// resolution number
		g_aVideoFormat[0].byaVideoFrameTbl[1]=F_SEL_640_480;
		g_aVideoFormat[0].byaVideoFrameTbl[2]=F_SEL_160_120;
		g_aVideoFormat[0].byaVideoFrameTbl[3]=F_SEL_320_240;
		g_aVideoFormat[0].byaVideoFrameTbl[4]=F_SEL_1280_720;
		g_aVideoFormat[0].byaVideoFrameTbl[5]=F_SEL_640_480;
		//--still image--
		g_aVideoFormat[0].byaStillFrameTbl[0] = 4;	// resolution number
		g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_160_120;
		g_aVideoFormat[0].byaStillFrameTbl[2]=F_SEL_320_240;
		g_aVideoFormat[0].byaStillFrameTbl[3]=F_SEL_640_480;
		g_aVideoFormat[0].byaStillFrameTbl[4]=F_SEL_1280_720;

		//g_aVideoFormat[0].byaVideoFrameTbl[0] = 1;
		//g_aVideoFormat[0].byaVideoFrameTbl[1]=F_SEL_1280_720;
		//g_aVideoFormat[0].byaStillFrameTbl[0] = 1;
		//g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_1280_720;

		//====== fps  setting ===========
		for(i=0; i<7; i++)
		{
			/*640_480,
			160_120,
			176_144,
			320_200,
			320_240,
			352_288,
			640_400*/
			//g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_15|FPS_20|FPS_25|FPS_30;
			g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_30|FPS_20|FPS_15|FPS_10;
		}
		for(i=7; i<15; i++)
		{
			/*800_600,
			960_720,
			1024_768,
			1280_720,
			1280_800,*/
			g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_10;//|FPS_5;
		}

		//====== format type  setting ===========
		g_aVideoFormat[0].byFormatType = FORMAT_TYPE_YUY2;

#ifdef ENABLE_MJPEG
		// copy YUY2 setting to MJPEG setting
		//memcpy(&(g_aVideoFormat[1]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

		// modify MJPEG FPS setting
		//for(i=7;i<12;i++)
		//{
		//g_aVideoFormat[1].waFrameFpsBitmap[i] = (FPS_15|FPS_20|FPS_25|FPS_30);
		//	g_aVideoFormat[1].waFrameFpsBitmap[i] = FPS_30;
		//}

		// -- preview ---
		g_aVideoFormat[1].byaVideoFrameTbl[0] = 5;		// resolution number
		g_aVideoFormat[1].byaVideoFrameTbl[1]=F_SEL_640_480;
		g_aVideoFormat[1].byaVideoFrameTbl[2]=F_SEL_160_120;
		g_aVideoFormat[1].byaVideoFrameTbl[3]=F_SEL_320_240;
		g_aVideoFormat[1].byaVideoFrameTbl[4]=F_SEL_1280_720;
		g_aVideoFormat[1].byaVideoFrameTbl[5]=F_SEL_640_480;
		//--still image--
		g_aVideoFormat[1].byaStillFrameTbl[0] = 4;	// resolution number
		g_aVideoFormat[1].byaStillFrameTbl[1]=F_SEL_160_120;
		g_aVideoFormat[1].byaStillFrameTbl[2]=F_SEL_320_240;
		g_aVideoFormat[1].byaStillFrameTbl[3]=F_SEL_640_480;
		g_aVideoFormat[1].byaStillFrameTbl[4]=F_SEL_1280_720;


		// modify MJPEG FPS setting
		for(i=0; i<12; i++)
		{
			g_aVideoFormat[1].waFrameFpsBitmap[i] = FPS_30;
		}


		// modify MJPEG format type
		g_aVideoFormat[1].byFormatType = FORMAT_TYPE_MJPG;

#endif

#ifdef ENABLE_M420_FMT
		// copy YUY2 setting to M420 setting
		memcpy(&(g_aVideoFormat[2]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

		// modify M420 FPS setting
		{
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_800_600] =FPS_15|FPS_10|FPS_5;//|FPS_1;             //bit07 F_800_600
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_960_720] =FPS_15|FPS_10|FPS_5;//|FPS_5|FPS_3|FPS_1;                    //bit08 F_960_720
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1024_768] =FPS_15|FPS_10|FPS_5;//|FPS_1;                    //bit09 F_1024_768
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_720] =FPS_11|FPS_5;//FPS_9|FPS_8|FPS_1;    //bit10 F_1280_720
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_800] =FPS_11|FPS_5;

			// modify M420 format type
			g_aVideoFormat[2].byFormatType = FORMAT_TYPE_M420;
		}
#endif

	}

	{
		//Brightness - BrightnessItem

		//Contrast - ContrastItem

		//Saturation - SaturationItem

		//Hue - 	HueItem

		//Sharpness - SharpnessItem
		//	SharpnessItem.Min = 0;
		//	SharpnessItem.Max = 8;
		//	SharpnessItem.Res = 1;
		//	SharpnessItem.Def = 3;
		//	SharpnessItem.Last  =SharpnessItem.Def;

		// gamma - 	GammaItem

		// gain - GainItem

		// whitebalance - WhiteBalanceTempItem

		// backlight - BackLightCompItem

		// powerline -PwrLineFreqItem
		//	PwrLineFreqItem.Def = PWR_LINE_FRQ_60;
		//	PwrLineFreqItem.Last = PwrLineFreqItem.Def;

		// exposure time -	ExposureTimeAbsolutItem

		// low light - LowLightCompItem
	}

	InitOV9724IspParams();
}

void SetOV9724IntegrationTime(U16 wEspline)
{
	U16 wFrameLenLines;
	U16 wExpMax = g_wAECExposureRowMax-2;

	if(wEspline > wExpMax)
	{
		wFrameLenLines = wEspline + 20;
	}
	else
	{
		wFrameLenLines = g_wAEC_LineNumber;
	}

	if (wFrameLenLines%2==1)
	{
		wFrameLenLines++;
	}

	//-----------Write  frame length or dummy lines------------
	Write_SenReg(0x0341, INT2CHAR(wFrameLenLines, 0));
	Write_SenReg(0x0340, INT2CHAR(wFrameLenLines, 1));

	//-----------Write Exposuretime setting-----------
	Write_SenReg(0x0203, INT2CHAR(wEspline, 0));	// change exposure value low
	Write_SenReg(0x0202, INT2CHAR(wEspline, 1));	// change exposure value high

	//----------- write gain setting-------------------
	Write_SenReg(0x0205, 0x0f);	// fixed gain at manual exposure control
	Write_SenReg(0x0204, 0);

}

void SetOV9724Gain(float fGain)
{
}

void SetOV9724ImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.
{

	WaitFrameSync(ISP_INT1,ISP_DATA_START_INT);

	bySnrImgDir ^= 0x01; 	// OV9724 output mirrored image at default, so need mirror the image for normal output
	Write_SenReg_Mask(0x101, bySnrImgDir, 0x03);

	WaitFrameSync(ISP_INT1,ISP_DATA_END_INT);


	if(bySnrImgDir & 0x01)
	{
#ifdef _MIPI_EXIST_
		SetBLCWindowStart(1, 1);
		//SetBLCWindowStart(0, 0);
#else
		SetBkWindowStart(1, 1);
#endif
	}
	else
	{
#ifdef _MIPI_EXIST_
		SetBLCWindowStart(0, 1);
#else
		SetBkWindowStart(0, 1);
#endif
	}

}

void SetOV9724Exposuretime_Gain(float fExpTime, float fTotalGain)
{
	U16 wExposureRows_floor;
	U16  wGainRegSetting; //data
	float fSnrGlbGain;
	float fExposureRows;
	U16 wSetDummy;
	float fTemp;
	U16 wExposurePixels;

	wGainRegSetting=MapSnrGlbGain2SnrRegSetting((U16)(fTotalGain*16.0));
	fSnrGlbGain = SnrRegSetting2SnrGlbGain(wGainRegSetting);

	fExposureRows = fExpTime/g_fSensorRowTimes;	//unit : 1row
	wExposureRows_floor = (U16)(fExposureRows);
	fTemp = fExpTime-((float)wExposureRows_floor*(g_fSensorRowTimes)); //fine integration time;unit:  us
	wExposurePixels =(U16)(fTemp * (float)(g_dwPclk/10000)) ; //unit: pixels

	//WaitFrameSync(ISP_INT1,ISP_DATA_START_INT);

	// write exposure, gain, dummy line
	if(fabs(g_fCurExpTime-fExpTime) > 0.0001)
	{
		// set dummy
		wSetDummy = g_wAFRInsertDummylines + g_wAEC_LineNumber;

		// group register hold
		Write_SenReg(0x0104, 0x01);

		//-----------Write  frame length or dummy lines------------
		if (wSetDummy%2 == 1)
		{
			wSetDummy++;
		}
		Write_SenReg(0x0341, INT2CHAR(wSetDummy, 0));
		Write_SenReg(0x0340, INT2CHAR(wSetDummy, 1));

		//-----------Write Exposuretime setting-----------
		Write_SenReg(0x0203, INT2CHAR(wExposureRows_floor, 0));	// change exposure value low
		Write_SenReg(0x0202, INT2CHAR(wExposureRows_floor, 1));	// change exposure value high

		//----------- write gain setting-------------------
		Write_SenReg(0x0205, INT2CHAR(wGainRegSetting, 0));

		SetISPAEGain(fTotalGain, fSnrGlbGain, 1);

		// group register release
		Write_SenReg(0x0104, 0);
		g_fCurExpTime = fExpTime;	// back up exposure rows
	}
	else
	{
		// group register hold
		Write_SenReg(0x0104, 1);
		wSetDummy = g_wAFRInsertDummylines + g_wAEC_LineNumber;

		//-----------Write  frame length or dummy lines------------
		if (wSetDummy%2 == 1)
		{
			wSetDummy++;
		}
		Write_SenReg(0x0341, INT2CHAR(wSetDummy, 0));
		Write_SenReg(0x0340, INT2CHAR(wSetDummy, 1));

		// write gain setting
		Write_SenReg(0x0205, INT2CHAR(wGainRegSetting, 0));

		SetISPAEGain(fTotalGain, fSnrGlbGain, 1);

		// group register release
		Write_SenReg(0x0104, 0);
	}

	return;

}

void OV9724_POR()
{
	Init_MIPI();
	ENTER_SENSOR_RESET();
	// power on
	SensorPowerControl(SWITCH_ON,EN_DELAYTIME);
	uDelay(1);
	// Initialize I2C Controller Timing
	LEAVE_SENSOR_RESET();
	// clock select 24M
	if( g_bySSCEnable== TRUE)
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_SSC_24M);
	}
	else
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_96M|CCS_CLK_DIVIDER_4);
	}

	EnableSensorHCLK();
	g_bySensorIsOpen = SENSOR_OPEN;
	WaitTimeOut_Delay(1);  //OV9726 spec request 8192clk
}

void InitOV9724IspParams(void)
{
	// AWB initial gain
	//use D50 R/G/B gain as AWB initial value
	g_wAWBRGain_Last= 0x17C;
	g_wAWBGGain_Last= 0x100;
	g_wAWBBGain_Last= 0x226;
	g_wProjectGR = g_wAWBRGain_Last;
	g_wProjectGB = g_wAWBBGain_Last;

	// Special ISP
	g_bySaturation_Def =64;//64;
	g_byContrast_Def = 48; //Neil Tuning at chicony

	//g_wDynamicISPEn = 0;//DYNAMIC_LSC_EN | DYNAMIC_SHARPPARAM_EN | DYNAMIC_GAMMA_EN | DYNAMIC_CCM_CT_EN;
	g_wDynamicISPEn = DYNAMIC_CCM_CT_EN| DYNAMIC_SHARPPARAM_EN | DYNAMIC_SHARPNESS_EN;//DYNAMIC_LSC_EN | DYNAMIC_GAMMA_EN | DYNAMIC_CCM_CT_EN;
}

void InitOV9724IspParamsByResolution(U16 wCurWidth, U16 wCurHeight)
{
	wCurWidth = wCurWidth;
	wCurHeight = wCurHeight;
}

void SetOV9724DynamicISP(U8 byAEC_Gain)
{
	byAEC_Gain = byAEC_Gain;
}

void SetOV9724DynamicISP_AWB(U16 wColorTempature)
{
	wColorTempature =wColorTempature;

	// hemonel 2011-07-04: Albert use only one LSC curve to pass MSOC premium test, so delete these code
	/*
	#ifdef _MSOC_TEST_
	U8 i;

	// LSC Curve dynamic
	if ( (g_wDynamicISPEn&DYNAMIC_LSC_EN) == DYNAMIC_LSC_EN)
	{
		if(wColorTempature < 4000)
		{
	          		for (i=0;i<6;i++)
	          		{
	          			g_awLensShadingCenter[i] = gc_wLSC_Center_Offset_OV9726_3500K[i];
	          		}

				for (i=0;i<48;i++)
				{
					XBYTE[ISP_NLSC_CURVE_R_BASE+i]  = gc_byLSC_RGB_Curve_OV9726_3500K[0][i];
					XBYTE[ISP_NLSC_CURVE_G_BASE+i]  = gc_byLSC_RGB_Curve_OV9726_3500K[1][i];
					XBYTE[ISP_NLSC_CURVE_B_BASE+i]  = gc_byLSC_RGB_Curve_OV9726_3500K[2][i];
				}
		}
		else if(wColorTempature > 4500)
		{
	          		for (i=0;i<6;i++)
	          		{
	          			g_awLensShadingCenter[i] = gc_wLSC_Center_Offset_OV9726_D65[i];
	          		}

				for (i=0;i<48;i++)
				{
					XBYTE[ISP_NLSC_CURVE_R_BASE+i]  = gc_byLSC_RGB_Curve_OV9726_D65[0][i];
					XBYTE[ISP_NLSC_CURVE_G_BASE+i]  = gc_byLSC_RGB_Curve_OV9726_D65[1][i];
					XBYTE[ISP_NLSC_CURVE_B_BASE+i]  = gc_byLSC_RGB_Curve_OV9726_D65[2][i];
				}
		}

	}
	#endif
	*/
}
#endif
