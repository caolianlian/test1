#ifndef _YACY6A1C9SBC_H_
#define _YACY6A1C9SBC_H_

void YACY6A1C9SBCSetFormatFps(U16 SetFormat, U8 Fps);
void CfgYACY6A1C9SBCControlAttr(void);
void SetYACY6A1C9SBCImgDir(U8 bySnrImgDir);
void SetYACY6A1C9SBCIntegrationTime(U16 wEspline);
void SetYACY6A1C9SBCExposuretime_Gain(float fExpTime, float fTotalGain);
void InitYACY6A1C9SBCIspParams(void );
void InitYACY6A1C9SBCIspParamsByResolution(U16 wCurWidth, U16 wCurHeight);
void YACY6A1C9SBC_POR(void );
void SetYACY6A1C9SBCDynamicISP(U8 byAEC_Gain);
void SetYACY6A1C9SBCDynamicISP_AWB(U16 wColorTempature);
#endif // _YACY6A1C9SBC_H_

