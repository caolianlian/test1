#ifndef _IMX076LQZC_H_
#define _IMX076LQZC_H_

void IMX076LQZCSetFormatFps(U16 SetFormat, U8 Fps);
void CfgIMX076LQZCControlAttr(void);
void SetIMX076LQZCImgDir(U8 bySnrImgDir);
void SetIMX076LQZCIntegrationTime(U16 wEspline);
void SetIMX076LQZCExposuretime_Gain(float fExpTime, float fTotalGain);
void InitIMX076LQZCIspParams(void );
void InitIMX076LQZCIspParamsByResolution(U16 wCurWidth, U16 wCurHeight);
void IMX076LQZC_POR(void );
void SetIMX076LQZCDynamicISP(U8 byAEC_Gain);
void SetIMX076LQZCDynamicISP_AWB(U16 wColorTempature);
void SetIMX076LQZCGain(float fGain);
#endif // _OV9710_H_

