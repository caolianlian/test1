#ifndef _CUSXU_H_
#define _CUSXU_H_

/************control attributes************/
#ifdef _LENOVO_JAPAN_PROPERTY_PAGE_

//CONTROL EXPOSURE TIME ABSOLUTE
#define CTL_MIN_CT_EXPOSURE_TIME_ABSOLUTE     (2)
#define CTL_MAX_CT_EXPOSURE_TIME_ABSOLUTE    (1250)
#define CTL_RES_CT_EXPOSURE_TIME_ABSOLUTE     (1)
#define CTL_DEF_CT_EXPOSURE_TIME_ABSOLUTE     (156)	// hemonel 2009-08-25: fix vista/7/xp show different value 

//#define CTL_MIN_CT_EXPOSURE_TIME_ABSOLUTE     (100)
//#define CTL_MAX_CT_EXPOSURE_TIME_ABSOLUTE    (10000)
//#define CTL_RES_CT_EXPOSURE_TIME_ABSOLUTE     (100)
//#define CTL_DEF_CT_EXPOSURE_TIME_ABSOLUTE     (300)

// CONTROL LOW LIGHT COMPENSATION
#define  CTL_MIN_PU_LOWLIGHTCOMP  (0)
#define  CTL_MAX_PU_LOWLIGHTCOMP  (2)
#define  CTL_RES_PU_LOWLIGHTCOMP  (1)
#define  CTL_DEF_PU_LOWLIGHTCOMP  (1)

//CONTROL WHITEBALANCE TERMPERATURE
#define CTL_MIN_PU_WHITE_BALANCE_TEMPERATURE   (2800)
#define CTL_MAX_PU_WHITE_BALANCE_TEMPERATURE  (6500)
#define CTL_RES_PU_WHITE_BALANCE_TEMPERATURE   (10)
#define CTL_DEF_PU_WHITE_BALANCE_TEMPERATURE   (4600)	//heonel 2010-06-22 follow Dell V0.11  //(4650)

//CONTROL WHITEBALANCE COMPONENT
#define CTL_MIN_PU_WHITE_BALANCE_COMPONENT   (0x00010001)
#define CTL_MAX_PU_WHITE_BALANCE_COMPONENT  (0x00FF00FF)
#define CTL_RES_PU_WHITE_BALANCE_COMPONENT   (0X00010001)
#define CTL_DEF_PU_WHITE_BALANCE_COMPONENT   (0x00400040)



//CONTROL WHITEBALANCE TERMPERATURE AUTO
#define CTL_MIN_PU_WHITE_BALANCE_TEMP_AUTO_CONTROL   (0)
#define CTL_MAX_PU_WHITE_BALANCE_TEMP_AUTO_CONTROL  (1)
#define CTL_RES_PU_WHITE_BALANCE_TEMP_AUTO_CONTROL   (1)
#define CTL_DEF_PU_WHITE_BALANCE_TEMP_AUTO_CONTROL   (1)
//



/************control attributes************/
//CONTROL  brightness
#ifdef _TCORRECTION_TEST_BY_BRIGHTNESS_
#define CTL_MIN_PU_BRIGHTNESS   (-10)	// 
#define CTL_MAX_PU_BRIGHTNESS   (10)	// 
#else
#define CTL_MIN_PU_BRIGHTNESS   (0)	// HP commercial
#define CTL_MAX_PU_BRIGHTNESS   (255)	// HP commercial
#define CTL_MIN_PU_BRIGHTNESS_STANDARD  (-64)	// HP commercial
#define CTL_MAX_PU_BRIGHTNESS_STANDARD  (64)	// HP commercial
#define CTL_DEF_PU_BRIGHTNESS_STANDARD  (0)
#endif
#define CTL_RES_PU_BRIGHTNESS   (1)
#define CTL_DEF_PU_BRIGHTNESS   (128)
#define  CTL_MIN_PU_CONTRAST    (0)
#define  CTL_MAX_PU_CONTRAST    (255) //heonel 2010-06-22 follow Dell V0.11  //(64)
#define  CTL_RES_PU_CONTRAST    (1)
#define  CTL_DEF_PU_CONTRAST    (32)	//heonel 2010-06-22 follow Dell V0.11  //(32) 	
#define  CTL_MIN_PU_CONTRAST_STANDARD (0)
#define  CTL_MAX_PU_CONTRAST_STANDARD (100) //heonel 2010-06-22 follow Dell V0.11  //(64)
#define  CTL_RES_PU_CONTRAST_STANDARD (1)
#define  CTL_DEF_PU_CONTRAST_STANDARD (50)	//heonel 2010-06-22 follow Dell V0.11  //(32) 	
#define  CTL_MIN_PU_HUE    (-180)	//heonel 2010-06-22 follow Dell V0.11  //(-180)      
#define  CTL_MAX_PU_HUE    (180)		//heonel 2010-06-22 follow Dell V0.11  //(180) 
#define  CTL_RES_PU_HUE    (1)
#define  CTL_DEF_PU_HUE    (0)
#define  CTL_MIN_PU_SATURATION  0
#define  CTL_MAX_PU_SATURATION  (100)	//heonel 2010-06-22 follow Dell V0.11  //(128) 	// HP commercial
#define  CTL_RES_PU_SATURATION  (1)
#define  CTL_DEF_PU_SATURATION  (64) 	// HP commercial
#define  CTL_MIN_PU_SHARPNESS   (0)	//heonel 2010-06-22 follow Dell V0.11  //(0)       
#define  CTL_MAX_PU_SHARPNESS   (7)	//heonel 2010-06-22 follow Dell V0.11  //(8)     
#define  CTL_RES_PU_SHARPNESS   (1)
#define  CTL_DEF_PU_SHARPNESS   (0)	//heonel 2010-06-22 follow Dell V0.11  //(4)   	
#define  CTL_MIN_PU_SHARPNESS_STANDARD  (0)	//heonel 2010-06-22 follow Dell V0.11  //(0)       
#define  CTL_MAX_PU_SHARPNESS_STANDARD  (100)	//heonel 2010-06-22 follow Dell V0.11  //(8)     
#define  CTL_RES_PU_SHARPNESS_STANDARD  (1)
#define  CTL_DEF_PU_SHARPNESS_STANDARD  (50)	//heonel 2010-06-22 follow Dell V0.11  //(4)   	
#define  CTL_MIN_PU_GAMMA       (90)   // 1.5
#define  CTL_MAX_PU_GAMMA       (150)	//heonel 2010-06-22 follow Dell V0.11  //(500)  // 2.2*2.2
#define  CTL_RES_PU_GAMMA       (1)
#define  CTL_DEF_PU_GAMMA       (120)	//heonel 2010-06-22 follow Dell V0.11  //(220) 
#define  CTL_MIN_PU_GAMMA_STANDARD       (100)   // 1.5
#define  CTL_MAX_PU_GAMMA_STANDARD       (500)	//heonel 2010-06-22 follow Dell V0.11  //(500)  // 2.2*2.2
#define  CTL_RES_PU_GAMMA_STANDARD       (1)
#define  CTL_DEF_PU_GAMMA_STANDARD       (300)	//heonel 2010-06-22 follow Dell V0.11  //(220) 
#define  CTL_MIN_PU_BACKLIGHTCOMP  (0)
#define  CTL_MAX_PU_BACKLIGHTCOMP  (3)	//heonel 2010-06-22 follow Dell V0.11  //(1) 
#define  CTL_RES_PU_BACKLIGHTCOMP  (1)
#define  CTL_DEF_PU_BACKLIGHTCOMP  (0)	//Reck 2010-08-23 BackLigjt Set to 0 //heonel 2010-06-22 follow Dell V0.11  //(0) 
#define  CTL_MIN_PU_GAIN  (0)
#define  CTL_MAX_PU_GAIN  (128)
#define  CTL_RES_PU_GAIN  (1)
#define  CTL_DEF_PU_GAIN  (64)
#define  CTL_MIN_PU_PWRLINEFRQ  (PWR_LINE_FRQ_DIS)
#define  CTL_MAX_PU_PWRLINEFRQ  (PWR_LINE_FRQ_OUTDOOR)
#define  CTL_RES_PU_PWRLINEFRQ  (1)
#define  CTL_DEF_PU_PWRLINEFRQ  (PWR_LINE_FRQ_60) //heonel 2010-06-22 follow Dell V0.11
#define  CTL_MIN_PU_PAN  (-16)
#define  CTL_MAX_PU_PAN  (16)
#define  CTL_RES_PU_PAN  (1)
#define  CTL_DEF_PU_PAN  (0)
#define  CTL_MIN_PU_TILT  (-16)
#define  CTL_MAX_PU_TILT  (16)
#define  CTL_RES_PU_TILT  (1)
#define  CTL_DEF_PU_TILT  (0)
#define  CTL_MIN_PU_ZOOM  (100)
#define  CTL_MAX_PU_ZOOM  (400)
#define  CTL_RES_PU_ZOOM  (100)
#define  CTL_DEF_PU_ZOOM  (100)
#define  CTL_MIN_PU_ROLL  (0)
#define  CTL_MAX_PU_ROLL  (3)
#define  CTL_RES_PU_ROLL  (1)
#define  CTL_DEF_PU_ROLL  (0)
#define CTL_MIN_CT_FOCUS_ABSOLUTE     (0)
#define CTL_MAX_CT_FOCUS_ABSOLUTE    (255)
#define CTL_RES_CT_FOCUS_ABSOLUTE     (4)
#define CTL_DEF_CT_FOCUS_ABSOLUTE     (68)
#define CTL_MIN_CT_FOCUS_AUTO   (0)
#define CTL_MAX_CT_FOCUS_AUTO  (1)
#define CTL_RES_CT_FOCUS_AUTO   (1)
#define CTL_DEF_CT_FOCUS_AUTO   (1)
#define CTL_MIN_EXTU_TRAPEZIUMCORRECTION   (-10)
#define CTL_MAX_EXTU_TRAPEZIUMCORRECTION   (10)
#define CTL_RES_EXTU_TRAPEZIUMCORRECTION   (1)
#define CTL_DEF_EXTU_TRAPEZIUMCORRECTION   (0)
#else
#define CTL_MIN_CT_EXPOSURE_TIME_ABSOLUTE     (50)
#define CTL_MAX_CT_EXPOSURE_TIME_ABSOLUTE    (10000)
#define CTL_RES_CT_EXPOSURE_TIME_ABSOLUTE     (1)
#define CTL_DEF_CT_EXPOSURE_TIME_ABSOLUTE     (166)	// hemonel 2009-08-25: fix vista/7/xp show different value 
#define  CTL_MIN_PU_LOWLIGHTCOMP  (0)
#define  CTL_MAX_PU_LOWLIGHTCOMP  (1)
#define  CTL_RES_PU_LOWLIGHTCOMP  (1)
#define  CTL_DEF_PU_LOWLIGHTCOMP  (1)
#define CTL_MIN_PU_WHITE_BALANCE_TEMPERATURE   (2800)
#define CTL_MAX_PU_WHITE_BALANCE_TEMPERATURE  (6500)
#define CTL_RES_PU_WHITE_BALANCE_TEMPERATURE   (10)
#define CTL_DEF_PU_WHITE_BALANCE_TEMPERATURE   (4600)	//heonel 2010-06-22 follow Dell V0.11  //(4650)
#define CTL_MIN_PU_WHITE_BALANCE_COMPONENT   (0x00010001)
#define CTL_MAX_PU_WHITE_BALANCE_COMPONENT  (0x00FF00FF)
#define CTL_RES_PU_WHITE_BALANCE_COMPONENT   (0X00010001)
#define CTL_DEF_PU_WHITE_BALANCE_COMPONENT   (0x00400040)
#define CTL_MIN_PU_WHITE_BALANCE_TEMP_AUTO_CONTROL   (0)
#define CTL_MAX_PU_WHITE_BALANCE_TEMP_AUTO_CONTROL  (1)
#define CTL_RES_PU_WHITE_BALANCE_TEMP_AUTO_CONTROL   (1)
#define CTL_DEF_PU_WHITE_BALANCE_TEMP_AUTO_CONTROL   (1)
#ifdef _TCORRECTION_TEST_BY_BRIGHTNESS_
#define CTL_MIN_PU_BRIGHTNESS   (-10)	// 
#define CTL_MAX_PU_BRIGHTNESS   (10)	// 
#else
#define CTL_MIN_PU_BRIGHTNESS   (-64)	// HP commercial
#define CTL_MAX_PU_BRIGHTNESS   (64)	// HP commercial
#endif
#define CTL_RES_PU_BRIGHTNESS   (1)
#define CTL_DEF_PU_BRIGHTNESS   (0)
//CONTRAST
#define  CTL_MIN_PU_CONTRAST    (0)
#define  CTL_MAX_PU_CONTRAST    (100) //heonel 2010-06-22 follow Dell V0.11  //(64)
#define  CTL_RES_PU_CONTRAST    (1)
#define  CTL_DEF_PU_CONTRAST    (50)	//heonel 2010-06-22 follow Dell V0.11  //(32) 	
//HUE
#define  CTL_MIN_PU_HUE    (-180)	//heonel 2010-06-22 follow Dell V0.11  //(-180)      
#define  CTL_MAX_PU_HUE    (180)		//heonel 2010-06-22 follow Dell V0.11  //(180) 
#define  CTL_RES_PU_HUE    (1)
#define  CTL_DEF_PU_HUE    (0)
//SATURATIONT
#define  CTL_MIN_PU_SATURATION  0
#define  CTL_MAX_PU_SATURATION  (100)	//heonel 2010-06-22 follow Dell V0.11  //(128) 	// HP commercial
#define  CTL_RES_PU_SATURATION  (1)
#define  CTL_DEF_PU_SATURATION  (64) 	// HP commercial

//SHARPNESS
#define  CTL_MIN_PU_SHARPNESS   (0)	//heonel 2010-06-22 follow Dell V0.11  //(0)       
#define  CTL_MAX_PU_SHARPNESS   (100)	//heonel 2010-06-22 follow Dell V0.11  //(8)     
#define  CTL_RES_PU_SHARPNESS   (1)
#define  CTL_DEF_PU_SHARPNESS   (50)	//heonel 2010-06-22 follow Dell V0.11  //(4)   	

/*
//Reck Add for Test (More Step for Sharpness)
#define  CTL_MIN_PU_SHARPNESS   (0)
#define  CTL_MAX_PU_SHARPNESS   (108)
#define  CTL_RES_PU_SHARPNESS   (1)
#define  CTL_DEF_PU_SHARPNESS   (30)
*/


//GAMMA
// hemonel 2008-03-04: change Gamma value
#define  CTL_MIN_PU_GAMMA       (100)   // 1.5
#define  CTL_MAX_PU_GAMMA       (500)	//heonel 2010-06-22 follow Dell V0.11  //(500)  // 2.2*2.2
#define  CTL_RES_PU_GAMMA       (1)
#define  CTL_DEF_PU_GAMMA       (300)	//heonel 2010-06-22 follow Dell V0.11  //(220) 
//BACKLIGHTCOMP
#define  CTL_MIN_PU_BACKLIGHTCOMP  (0)
#define  CTL_MAX_PU_BACKLIGHTCOMP  (1)	//heonel 2010-06-22 follow Dell V0.11  //(1) 
#define  CTL_RES_PU_BACKLIGHTCOMP  (1)
#define  CTL_DEF_PU_BACKLIGHTCOMP  (0)	//Reck 2010-08-23 BackLigjt Set to 0 //heonel 2010-06-22 follow Dell V0.11  //(0) 
//GAIN
#define  CTL_MIN_PU_GAIN  (0)
#define  CTL_MAX_PU_GAIN  (128)
#define  CTL_RES_PU_GAIN  (1)
#define  CTL_DEF_PU_GAIN  (64)
//PWR_LINE_FRQ
#define  CTL_MIN_PU_PWRLINEFRQ  (PWR_LINE_FRQ_DIS)
#define  CTL_MAX_PU_PWRLINEFRQ  (PWR_LINE_FRQ_60)
#define  CTL_RES_PU_PWRLINEFRQ  (1)
#define  CTL_DEF_PU_PWRLINEFRQ  (PWR_LINE_FRQ_50) //heonel 2010-06-22 follow Dell V0.11

//Pan
#define  CTL_MIN_PU_PAN  (-16)
#define  CTL_MAX_PU_PAN  (16)
#define  CTL_RES_PU_PAN  (1)
#define  CTL_DEF_PU_PAN  (0)

//Tilt
#define  CTL_MIN_PU_TILT  (-12)
#define  CTL_MAX_PU_TILT  (12)
#define  CTL_RES_PU_TILT  (1)
#define  CTL_DEF_PU_TILT  (0)

//Zoom
#define  CTL_MIN_PU_ZOOM  (0)
#define  CTL_MAX_PU_ZOOM  (3)
#define  CTL_RES_PU_ZOOM  (1)
#define  CTL_DEF_PU_ZOOM  (0)

// Roll
#define  CTL_MIN_PU_ROLL  (0)
#define  CTL_MAX_PU_ROLL  (3)
#define  CTL_RES_PU_ROLL  (1)
#define  CTL_DEF_PU_ROLL  (0)
//Focus Absoulte
#define CTL_MIN_CT_FOCUS_ABSOLUTE     (0)
#define CTL_MAX_CT_FOCUS_ABSOLUTE    (255)
#define CTL_RES_CT_FOCUS_ABSOLUTE     (4)
#define CTL_DEF_CT_FOCUS_ABSOLUTE     (68)

// Focus Auto
#define CTL_MIN_CT_FOCUS_AUTO   (0)
#define CTL_MAX_CT_FOCUS_AUTO  (1)
#define CTL_RES_CT_FOCUS_AUTO   (1)
#define CTL_DEF_CT_FOCUS_AUTO   (1)
//Trapezium  Correction
#define CTL_MIN_EXTU_TRAPEZIUMCORRECTION   (-10)
#define CTL_MAX_EXTU_TRAPEZIUMCORRECTION   (10)
#define CTL_RES_EXTU_TRAPEZIUMCORRECTION   (1)
#define CTL_DEF_EXTU_TRAPEZIUMCORRECTION   (0)

#ifdef _RTK_EXTENDED_CTL_
#define CTL_MAX_RTKEXT_SPECIALEFFECT		(ISP_SPECIAL_EFFECT_NORMAL		\
											|ISP_SPECIAL_EFFECT_MONOCHROME	\
											|ISP_SPECIAL_EFFECT_GRAY		\
											|ISP_SPECIAL_EFFECT_NEGATIVE	\
											|ISP_SPECIAL_EFFECT_SEPIA		\
											|ISP_SPECIAL_EFFECT_GREENISH	\
											|ISP_SPECIAL_EFFECT_REDDISH		\
											|ISP_SPECIAL_EFFECT_BLUISH)
#define CTL_DEF_RTKEXT_SPECIALEFFECT		(ISP_SPECIAL_EFFECT_NORMAL)

#define CTL_MIN_RTKEXT_EVCOMP				(-6)	// the max value should be divided by the resolution
#define CTL_MAX_RTKEXT_EVCOMP				(6)		// the min value should be divided by the resolution
#define CTL_RES_RTKEXT_EVCOMP				(EVCOMP_THIRDSTEP)
#define CTL_DEF_RTKEXT_EVCOMP				(0)

#define CTL_MIN_RTKEXT_ISO					(ISO_AUTO)
#define CTL_MAX_RTKEXT_ISO					(ISO_6400)
#define CTL_RES_RTKEXT_ISO					(1)
#define CTL_DEF_RTKEXT_ISO					(ISO_AUTO)	

#ifdef _AF_ENABLE_
#define CTL_MAX_RTKEXT_ROI_AUTOFUNCTION		(ROI_AE_SUPPORT|ROI_AF_SUPPORT)
#else
#define CTL_MAX_RTKEXT_ROI_AUTOFUNCTION		(ROI_AE_SUPPORT)
#endif

#endif

#endif
#endif
