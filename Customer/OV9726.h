#ifndef _OV9726_H_
#define _OV9726_H_

void OV9726SetFormatFps(U16 SetFormat, U8 Fps);
void CfgOV9726ControlAttr(void);
void SetOV9726ImgDir(U8 bySnrImgDir);
void SetOV9726IntegrationTime(U16 wEspline);
void SetOV9726Exposuretime_Gain(float fExpTime, float fTotalGain);
void InitOV9726IspParams(void );
void InitOV9726IspParamsByResolution(U16 wCurWidth, U16 wCurHeight);
void OV9726_POR(void );
void SetOV9726DynamicISP(U8 byAEC_Gain);
void SetOV9726DynamicISP_AWB(U16 wColorTempature);
void SetOV9726Gain(float fGain);
#endif // _OV9710_H_

