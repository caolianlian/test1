#ifndef _MI360_H_
#define _MI360_H_




#define MI360_AEW    (90+10)
#define MI360_AEB     (90-10)



typedef struct MI360_FpsSetting
{
	U8 byFps;
	//U8 byHclk;
	U16 wHorizBlanks;   // 0x5
	U16 wVertBlanks;   //0x6
	U8  byDiv;
	//U32 dwPixelClk;
} MI360_FpsSetting_t;

void  Mi360SetFormatFps(U8 SetFormat, U8 Fps);
void SetMi360OutputDim(U16 wWidth,U16 wHeight);

void SetMi360SensorEffect(U8 byEffect);
void SetMi360ImgDir(U8 bySnrImgDir); //flip: bit1,mirror bit0.
void SetMI360Brightness(S16 swSetValue);
void SetMI360Saturation(U16 wSetValue);
void  SetMI360Sharpness(U8 bySetValue);
void SetMi360WindowCrop(U16 wXStart,U16 wXEnd,U16 wYStart,U16 wYEnd );
void SetMi360SensorEffect(U8 byEffect);
void SetMi360SensorImgDir(U8 bySnrImgDir); //flip: bit1,mirror bit0.
void SetMi360PwrLineFreq(U8 byFps, U8 bySetValue);
void SetMi360ISPMisc();
void SetMi360BackLightComp(U8 bySetValue);


void SetMi360WBTempAuto(U8 bySetValue);
void SetMi360WBTemp(U16 wSetValue);
void CfgMi360ControlAttr();
U16 GetMi360AEGain();
void SetMi360IntegrationTimeAuto(U8 bySetValue);
void SetMi360IntegrationTime(U16 wEspline);
void SetMI360Gain(float fGain);
#endif
