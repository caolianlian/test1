#include "Inc.h"



#ifdef RTS58XX_SP_MI0360
t_RegSettingWW code gc_MI360_SOC_LSC_Setting[]=
{

//[Lens Correction 100% 05/11/09 19:34:30]
	{  0x180,0x0003 }, 	// LENS_CORR_CONTROL
	{  0x181,0xb017 }, 	// LENS_ADJ_VERT_RED_0
	{  0x182,0x01cf }, 	// LENS_ADJ_VERT_RED_1_2
	{  0x183,0x682b }, 	// LENS_ADJ_VERT_RED_3_4
	{  0x184,0xcb0c }, 	// LENS_ADJ_VERT_GREEN_0
	{  0x185,0x0ae0 }, 	// LENS_ADJ_VERT_GREEN_1_2
	{  0x186,0x430e }, 	// LENS_ADJ_VERT_GREEN_3_4
	{  0x187,0xC70f }, 	// LENS_ADJ_VERT_BLUE_0
	{  0x188,0x0Add }, 	// LENS_ADJ_VERT_BLUE_1_2
	{  0x189,0x3e0e }, 	// LENS_ADJ_VERT_BLUE_3_4
	{  0x18A,0x8e23 }, 	// LENS_ADJ_HORIZ_RED_0
	{  0x18B,0xe7ac }, 	// LENS_ADJ_HORIZ_RED_1_2
	{  0x18C,0x4810 }, 	// LENS_ADJ_HORIZ_RED_3_4
	{  0x18D,0x0073 }, 	// LENS_ADJ_HORIZ_RED_5
	{  0x18E,0xc115 }, 	// LENS_ADJ_HORIZ_GREEN_0
	{  0x18F,0xedcb }, 	// LENS_ADJ_HORIZ_GREEN_1_2
	{  0x190,0x260a }, 	// LENS_ADJ_HORIZ_GREEN_3_4
	{  0x191,0x0036 }, 	// LENS_ADJ_HORIZ_GREEN_5
	{  0x192,0xBb13 }, 	// LENS_ADJ_HORIZ_BLUE_0
	{  0x193,0xeed2 }, 	// LENS_ADJ_HORIZ_BLUE_1_2
	{  0x194,0x230a }, 	// LENS_ADJ_HORIZ_BLUE_3_4
	{  0x195,0x003a }, 	// LENS_ADJ_HORIZ_BLUE_5


//BITFIELD= 1, 0x08, 0x0100, 1 //LENS_CORRECTION
};


code t_RegSettingWW  gc_MI360_GAMMA_Setting[]=
//[Register Log 04/15/09 17:36:35] //GAMMA=0.45 , black=8.
{
	{  0X158, 0x0000}, 	// GAMMA_Y0
	{  0X153, 0x1708}, 	// GAMMA_Y1_Y2
	{  0X154, 0x5535}, 	// GAMMA_Y3_Y4
	{  0X155, 0x967B}, 	// GAMMA_Y5_Y6
	{  0X156, 0xBFAC}, 	// GAMMA_Y7_Y8
	{  0X157, 0xE0D0} 	// GAMMA_Y9_Y10
};

code t_RegSettingWW gc_MI360_AWB_CCM_Setting[]=
{
//[AWB and CCMs 05/12/09 15:40:15]
	{   0x120, 0xC814}, 	// LUM_LIMITS_WB_STATS
	{   0x121, 0x0000}, 	// R_B_GAIN_MANUAL_WB
	{   0x122, 0xD960}, 	// AWB_RED_LIMIT
	{   0x123, 0xD960}, 	// AWB_BLUE_LIMIT
	{   0x124, 0x7F00}, 	// MATRIX_ADJ_LIMITS
	{   0x15E, 0x7B4F}, 	// RATIO_BASE_REG
	{   0x15F, 0x522E}, 	// RATIO_DELTA_REG
	{   0x160, 0x0002}, 	// SIGNS_DELTA_REG


	{   0x102, 0x00EE}, 	// BASE_MATRIX_SIGNS
	{   0x103, 0x3923}, 	// BASE_MATRIX_SCALE_K1_K5
	{   0x104, 0x0524}, 	// BASE_MATRIX_SCALE_K6_K9
	{   0x109, 0x00C5}, 	// BASE_MATRIX_COEF_K1
	{   0x10A, 0x0046}, 	// BASE_MATRIX_COEF_K2
	{   0x10B, 0x0041}, 	// BASE_MATRIX_COEF_K3
	{   0x10C, 0x0063}, 	// BASE_MATRIX_COEF_K4
	{   0x10D, 0x00B5}, 	// BASE_MATRIX_COEF_K5
	{   0x10E, 0x0005}, 	// BASE_MATRIX_COEF_K6
	{   0x10F, 0x001F}, 	// BASE_MATRIX_COEF_K7
	{   0x110, 0x00B2}, 	// BASE_MATRIX_COEF_K8
	{   0x111, 0x0074}, 	// BASE_MATRIX_COEF_K9
	{   0x115, 0x0199}, 	// DELTA_COEFS_SIGNS
	{   0x116, 0x0011}, 	// DELTA_MATRIX_COEF_D1
	{   0x117, 0x0031 },	// DELTA_MATRIX_COEF_D2
	{   0x118, 0x005A },	// DELTA_MATRIX_COEF_D3
	{   0x119, 0x0060 },	// DELTA_MATRIX_COEF_D4
	{   0x11A, 0x0013 },	// DELTA_MATRIX_COEF_D5
	{   0x11B, 0x0039 },	// DELTA_MATRIX_COEF_D6
	{   0x11C, 0x0044 },	// DELTA_MATRIX_COEF_D7
	{   0x11D, 0x0050 },	// DELTA_MATRIX_COEF_D8
	{   0x11E, 0x0025 }	// DELTA_MATRIX_COEF_D9

};










code MI360_FpsSetting_t  g_staMi360VGAFpsSetting[]=
{
	//{1,	1023,2202,3},
	//{3,	1023,408,3},
	//{5,	1143,15,	3},
	//{8,	429,	15,	3},
	//	{1,	1023,1753,4},
	//{3,	1020,1757,0},
	{3,	1023,1753,0},
	{5,	1020,	859,	0},
	{8,	1019,	354,	0},
	{9,	1022,	259,	0},
	{10,	1022,	184,	0},
	{15,	845,	9,	0},
	{20,	444,	9,	0},
	{25,	203,	9,	0},
	{30,	33,	(14+1),	0}
};

/*
SnrFpsSetting_t g_staMi360FpsSetting[] =
{
	{3, &g_staMi360VGAFpsSetting[0]}
};



U8* GetSnrFpsSetting(SnrFpsSetting_t* pSnrFpsSettingArray,U8 SettingArrayLength,U8 Fps)
{
	U8 i;
	U8 Idx=1;
	for(i=0;i< (SettingArrayLength)/sizeof(SnrFpsSetting_t);i++)
	{
		if(pSnrFpsSettingArray[i].fps ==Fps)
		{
			Idx=i;
			break;
		}
	}
	return (U8*)&pSnrFpsSettingArray[Idx];
}

*/



MI360_FpsSetting_t*  GetMi360FpsSetting(U8 Fps)
{
	U8 i;
	U8 Idx=0;
	if(g_bIsHighSpeed)
	{
		Idx=3; //8fps for uxga, 10 fps for vga
	}
	else
	{
		Idx = 1;
	}

	for(i=0; i< (sizeof(g_staMi360VGAFpsSetting)/sizeof(MI360_FpsSetting_t)); i++)
	{
		if(g_staMi360VGAFpsSetting[i].byFps ==Fps)
		{
			Idx=i;
			break;
		}
	}
	return &g_staMi360VGAFpsSetting[Idx];
}


void  Mi360SetFormatFps(U8 SetFormat, U8 Fps)
{
	U8 byPixelClkMHz;
	float fTmp;
	U16 wTmp;
	MI360_FpsSetting_t  *pMI360_FpsSetting;

	SetFormat = SetFormat;// for delete warning

	pMI360_FpsSetting=GetMi360FpsSetting(Fps);


	Write_SenReg(0x0108, 0xC800); //output YUYV; enable flicker detection, lens shading.
	Write_SenReg(0x015B, 0x0001); //flicker detect
	Write_SenReg(0x106 ,   0x700E);// enable AWB, Aperture correction knee, on-the-fly defect detection
	//enable AE and AE measurement is a weighted sum of largewindow and center window
	Write_SenReg(0x135,0xF010);//yuv Control ITU601
	Write_SenReg(0x405,pMI360_FpsSetting->wHorizBlanks  );
	Write_SenReg(0x406,pMI360_FpsSetting->wVertBlanks);
	Write_SenReg(0x40a,pMI360_FpsSetting->byDiv);
	if((pMI360_FpsSetting->byDiv) ==2)
	{
		byPixelClkMHz = (24>>2);
	}
	else if ((pMI360_FpsSetting->byDiv) ==4)
	{
		byPixelClkMHz = (24/6);
	}
	else
	{
		byPixelClkMHz = (24>>1);
	}
//	g_wDumpxlPerline=pMI360_FpsSetting->wHorizBlanks+121;


	//set anti-flicker registers.
	//row time ,unit:1us
	//    ((float)(pMI360_FpsSetting->wHorizBlanks+121+640))/(float)byPixelClkMHz;     // unit microsecond
	//overhead time unit:1us (4*57)/24;
	//   1/30 s shutter width for 60Hz AC
	//Int_Time = shutter_width*row_time - over_head_time-Reset_delay.
	//
	// shutter_width =[Int_Time+over_head_time+Reset_delay(0)]/row_time
	fTmp = (float)((33333+((4*57)/24))*(float)byPixelClkMHz) / (float)((pMI360_FpsSetting->wHorizBlanks+121+640));
	//60Hz
	wTmp = (U16)fTmp;
	Write_SenReg(0x159, wTmp); //60Hz shutter width

	wTmp = wTmp/15;

	wTmp=((wTmp+2)<<8)|wTmp;
	Write_SenReg(0x15c, wTmp);
	//50Hz
	wTmp = (U16)(fTmp*1.2);
	Write_SenReg(0x15a, wTmp); //50Hz shutter width

	wTmp = wTmp/15;

	wTmp=((wTmp+2)<<8)|wTmp;

	Write_SenReg(0x15d, wTmp);


	g_wAECExposureRowMax = pMI360_FpsSetting->wVertBlanks+488;

	SetMi360ISPMisc();
}



void SetMi360WindowCrop(U16 wXStart,U16 wXEnd,U16 wYStart,U16 wYEnd )
{
	U16 wTmp_width,wTmp_height;

	wTmp_width = wXEnd-wXStart;
	wTmp_height = wYEnd-wYStart;

	Write_SenReg(0x1A5, wXStart); //pan width
	Write_SenReg(0x1A8, wYStart); //pan height
	Write_SenReg(0x1A6, wTmp_width); //zoom width
	Write_SenReg(0x1A9, wTmp_height); //zoon heigth
}


void SetMi360OutputDim(U16 wWidth,U16 wHeight)
{
	Write_SenReg(0x1A7,wWidth); //horizontal
	Write_SenReg(0x1AA,wHeight); //vertical
}

#ifndef _USE_BK_SPECIAL_EFFECT_
void SetMi360SensorEffect(U8 byEffect)
{
	U16 wTmp= 0;
	switch(byEffect)
	{
	case SNR_EFFECT_MONOCHROME:
		wTmp = 0x0020;//
		break;
	default:
		wTmp =0;//no effect;
		break;
	}
	Write_SenReg_Mask(0x108,wTmp,0x0020);
}
#endif

void SetMI360Gain(float fGain)
{
}

void SetMi360ImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.
{
	U16 wTmp;
	wTmp = 0x1000;
	if(bySnrImgDir&0x01) //mirror
	{
		wTmp |= 0x4020;
	}

	if(bySnrImgDir&0x02) // flip
	{
		wTmp |= 0x8080;
	}
	Write_SenReg(0x420, wTmp);//Read_mode.
	return ;
}
void SetMi360PwrLineFreq(U8 byFps, U8 bySetValue)
{
	U8 byTmp;

	byFps = byFps; // for delete warning
	if(bySetValue == PWR_LINE_FRQ_DIS)
	{
		byTmp=0x0000;//auto flicker detection
	}
	else //	manual mode.
	{
		if(bySetValue == PWR_LINE_FRQ_50)//50Hz
		{
			byTmp = 0x0001;
		}
		else//60Hz
		{
			byTmp = 0x0003;
		}
	}
	Write_SenReg(0x15b,byTmp);
}
void SetMi360LSC()
{
	//cheney.cai 2009-06-24,do not select setting ,just replace setting when EEPROM configuration exist
	//if((g_byVdSOCIspSettingExistBitmap&VD_SNRISP_REGSETTING_LSC) == 0)
	{
		//LoadVdSensorSetting((U8*)gc_MI360_SOC_LSC_Setting,sizeof(gc_MI360_SOC_LSC_Setting)/5);
		WriteSensorSettingWW(sizeof(gc_MI360_SOC_LSC_Setting)/4, gc_MI360_SOC_LSC_Setting);

		Write_SenReg_Mask(0x108,0x0100,0x0100);//enble lens shading.
	}
}
void SetMi360CCM()
{


}
void SetMi360AE()
{
	U8 byTmp1,byTmp2;
	byTmp1 = (g_byOVAEW_Normal+g_byOVAEB_Normal)/2;
	byTmp2 = (g_byOVAEW_Normal-g_byOVAEB_Normal)/2;
	//set sensor AE target and tracking stability
	Write_SenReg(0x12E, ((((U16)byTmp2)<<8)|byTmp1));
	//set sensor AE speed and  sensitivity
	Write_SenReg(0x12F, 0x44);//AE reaction delay "100" ,AE speed "100", step size setting '01'(medium speed )
	//use default AE window setting

}


void SetMi360AWB()
{
	//Write_SenReg(0x125, 0x4524);//AWB reaction delay "100",AWB speed "100",saturatin 100% enable automatic color saturation control in low light.
	//Write_SenReg(0x125, 0x4524);//AWB reaction delay "100",AWB speed "100",saturatin 100% enable automatic color saturation control in low light.
	Write_SenReg(0x125, 0x6d24);//AWB reaction delay "100",AWB speed "100",saturatin 150% enable automatic color saturation control in low light.
	//use default AWB window setting.

	//data 090219
	//Write_SenReg(0x15e ,0x9625);//base gain ratio  //tune at 2009-02-19.
	//Write_SenReg(0x15f ,0x5d03 );//Delta gain ratio
	//data 090320
	//Write_SenReg(0x15e ,0x7d4f);//base gain ratio  //tune at 2009-02-19.
	//Write_SenReg(0x15f ,0x5128 );//Delta gain ratio
	//data 090330
	//Write_SenReg(0x15e ,0x3f52);//base gain ratio  //tune at 2009-02-19.
	//Write_SenReg(0x15f ,0x1612 );//Delta gain ratio
	//Write_SenReg(0x15e ,0x7030);//base gain ratio  //tune at 2009-04-17.
	//Write_SenReg(0x15f ,0x3e14 );//Delta gain ratio

//	Write_SenReg(0x15e ,0x6a2e);//base gain ratio  //tune at 2009-04-17.
//	Write_SenReg(0x15f ,0x380e );//Delta gain ratio

//	Write_SenReg(0x15e ,0x8235);//base gain ratio  //tune at 2009-04-17.
//	Write_SenReg(0x15f ,0x4212 );//Delta gain ratio
	//cheney.cai 2009-06-24,do not select setting ,just replace setting when EEPROM configuration exist
	//if((g_byVdSOCIspSettingExistBitmap&VD_SNRISP_REGSETTING_CCM) == 0)
	{
		//LoadVdSensorSetting((U8*)gc_MI360_AWB_CCM_Setting,sizeof(gc_MI360_AWB_CCM_Setting)/5);
		WriteSensorSettingWW(sizeof(gc_MI360_AWB_CCM_Setting)/4, gc_MI360_AWB_CCM_Setting);
		Write_SenReg_Mask(0x106, 0x8000, 0x8000);
		WaitTimeOut_Delay(50);
		Write_SenReg_Mask(0x106, 0x0000, 0x8000);
	}



}

void SetMi360WBTempAuto(U8 bySetValue)
{
	U16 wTmp;
	if(bySetValue)
	{
		Write_SenReg(0x124,(127<<8)|0); //AWB upper limit and lower limit
		Write_SenReg_Mask(0x106, 0x02, 0x02); //enable AWB
		Write_SenReg_Mask(0x125,0x7f,0x07f);//slowest AWB speed;
		//DBG(("Mi360 AWB ON\n"));
	}
	else
	{
		Write_SenReg_Mask(0x106, 0x00, 0x02);//Stop AWB at current values
		Read_SenReg(0x112, &wTmp);
		Ctl_WhiteBalanceTemp.pLast->CurU16=MapMicronSensorWBPositon2WBTemp((U8)wTmp);
		Write_SenReg_Mask(0x125,0x00,0x07f);//fastest AWB speed;
		//DBG(("Mi360 AWB OFF\n"));
	}
}



void SetMi360WBTemp(U16 wSetValue)
{
	U8 byTmp;
	//0x112
	//0x124
	//0x106
	Write_SenReg_Mask(0x106, 0x80, 0x82); //switch to manual WB mode R0x106[1]=0 R0x106[15]=1;
	byTmp=MapWBTemp2MicronSensorWBPositon(wSetValue);
	Write_SenReg(0x124, (byTmp<<8)|byTmp);//matrix positon constraints lower , upper limit
	Write_SenReg_Mask(0x106, 0x02, 0x82); //return to  AWB mode R0x106[1]=1 R0x106[15]=0;

}



void SetMi360DPC()
{

}
void SetMi360NRC()
{

}

void SetMi360MISC()
{
	//cheney.cai 2009-06-24,do not select setting ,just replace setting when EEPROM configuration exist
	//if((g_byVdSOCIspSettingExistBitmap&VD_SNRISP_REGSETTING_GAMMA) == 0)
	{
		//LoadVdSensorSetting((U8*)gc_MI360_GAMMA_Setting,sizeof(gc_MI360_GAMMA_Setting)/5);
		WriteSensorSettingWW(sizeof(gc_MI360_GAMMA_Setting)/4, gc_MI360_GAMMA_Setting);
	}
}
void SetMi360BackLightComp(U8 bySetValue)
{

	if(bySetValue==0)
	{
		Write_SenReg_Mask(0x106, 0x000c, 0x000c); //auto exposure sampling window is specified by the weighted sum of the full window.
		Write_SenReg_Mask(0x12E, ((g_byOVAEW_Normal+g_byOVAEB_Normal)/2),0x00FF);
		//center window TWICE times more heavily
	}
	else
	{
		Write_SenReg_Mask(0x106, 0x0004, 0x000c);//auto exposure sampling window is specified by the center window.
		Write_SenReg_Mask(0x12E, ((g_byOVAEW_BLC+g_byOVAEB_BLC)/2),0x00FF);
	}
}

void CfgMi360ControlAttr()
{
	g_bySensorSize = SENSOR_SIZE_VGA;
	g_bySensorSPFormat = VGA_FRM;

	{
//		memcpy(g_asOvCTT,gc_MI360_CTT,sizeof(gc_MI360_CTT));
	}

	{
		// AE backlight compensation parameter
		g_byOVAEW_Normal = MI360_AEW;
		g_byOVAEB_Normal = MI360_AEB;

		g_byOVAEW_BLC = g_byOVAEW_Normal + 0x20;
		g_byOVAEB_BLC = g_byOVAEB_Normal + 0x20;
	}

	{
		// hemonel 2010-09-08: support 1.8V sensor IO
#if (_CHIP_ID_ & _RTS5840_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V79;
#elif (_CHIP_ID_ & _RTS5832_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V85;
#elif (_CHIP_ID_ & _RTS5829B_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V82;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V80;
#endif
	}

//	if(g_byFormatCfgExist==0) // format config not exist, use default setting.
	{
		InitFormatFrameFps();
	}


}

#ifndef _USE_BK_HSBC_ADJ_
void  SetMI360Brightness(S16 swSetValue)
{
	S16 swTmp;
	//find  setting  first.
	swTmp = swSetValue;

	if(swTmp<0)
	{
		swTmp=0;
	}
	Write_SenReg(0x134,swTmp&0x00ff);

}

U8 code g_cMi360SaturationTable[]=
{
	0x6,//0%, black and white
	0x4,//25%   of full saturatoin
	0x3,//37,5% ~~
	0x2,//50%
	0x1,//75%
	0X0,//100%
	0X5//150%
};

void  SetMI360Saturation(U16 wSetValue)
{
	U16 wTmp1;
	U16 wTmp2;
	//find saturation setting index first.
	wTmp1=wSetValue;

	wTmp2=  g_cMi360SaturationTable[wTmp1];
	Read_SenReg(0X125,&wTmp1);
	wTmp1 &= (~0x3800);
	Write_SenReg(0x125,(wTmp2<< 11)|wTmp1);
}
#endif
void  SetMi360Sharpness(U8 bySetValue)
{
	U16 wTmp;
	//find  setting  first.

	wTmp=((U16)bySetValue * 8)/(CTL_MAX_PU_SHARPNESS-CTL_MIN_PU_SHARPNESS+1);

	Write_SenReg(0x105,wTmp&0x0007);
}

void SetMi360ISPMisc()
{
	SetMi360LSC();
	SetMi360CCM();
	SetMi360AE();
	SetMi360AWB();
	SetMi360DPC();
	SetMi360NRC();
	SetMi360MISC();
}
U16 GetMi360AEGain()
{
	U8 i;
	U16 wRegValue;
	U16 wAnalogGain;
	U16 wDigitalGain;
	//0x435 global gain
	/*	Global Gain default = 0x0020 (32) = 1x gain. This register can be used to set all four gains at once.
	When read, it will return the value stored in R0x2B.
	6:0 Initial Gain = bits (6:0) x 0.03125
	7, 8 Analog Gain = (bit 8 + 1) x (bit 7 + 1) x initial gain (each bit gives 2x gain).
	9,10 Total Gain = (bit 9 + 1) x (bit 10 + 1) x analog gain (each bit gives 2x gain).
	*/
	// calculate analog gain Unit:1/32
	Read_SenReg(0x435, &wRegValue);
	wAnalogGain = wRegValue&0x007f;
	for(i=7; i<11; i++)
	{
		if(wRegValue&(0x0001<<i))
		{
			wAnalogGain <<=1;
		}
	}
	//0x162 digital gain
	//Digital gain  unit:1/(16*16)
	Read_SenReg(0x162, &wRegValue);
	wDigitalGain = wRegValue&0x00ff;
	wDigitalGain *= ((wRegValue&0xff00)>>8);
	//total gain
	return (((U32)wDigitalGain *(U32)wAnalogGain)>>9);
}

void SetMi360IntegrationTimeAuto(U8 bySetValue)
{
}

void SetMi360IntegrationTime(U16 wEspline)
{
}
#endif
