#ifndef _OV2710_H_
#define _OV2710_H_

void OV2710SetFormatFps(U16 SetFormat, U8 Fps);
void CfgOV2710ControlAttr(void);
void SetOV2710ImgDir(U8 bySnrImgDir);
void SetOV2710IntegrationTime(U16 wEspline);
void SetOV2710Exposuretime_Gain(float fExpTime, float fTotalGain);
void InitOV2710IspParams();
void OV2710_POR();
void InitOV2710IspParams();
void SetOV2710DynamicISP(U8 bymode);
void SetOV2710DynamicISP_AWB(U16 wColorTempature);
void SetOV2710Gain(float fGain);
#endif // _OV9710_H_


