#include "Inc.h"
#include "CamReg.h"

#ifdef RTS58XX_SP_GC1136
//#define DLINK_SHORT_PWDN
U8 bybyGC1136AGain = 0;
U8 byGC1136GlobalGain = 0;
U16 wGC1136AutoPreGain = 0;

#ifdef _ENABLE_OUTDOOR_DETECT_
Dyanmic_AWB_Boundary_t code gc_AWBDynmaicBoundary={28,-8,16,8,50,30,2,21};
#endif

OV_CTT_t code gc_GC1136_CTT[3] =
{
	{2900,0x112,0x100,0x250},
	{4600,0x190,0x100,0x190},
	{6600,0x1D8,0x100,0x124},
};
/*
code GC_FpsSetting_t  g_staGC1136FpsSetting[]=
{
// FPS ExtraDummyPixel byExtraDummyLine clkrc 	   pclk
//	{5,		(1682+1683+1),	         (3),		14135400},		//GC1036 clk too low
	{5,		(2041),	             (53),	    (0x83),	      24000000},
	{10,		(857),	             (53),          (0x83), 		24000000},
	{15,		(1529),		       (90),          (0x88), 		60000000},
	{20,		(1023),	             (90),          (0x88), 		60000000},
	{25,		(671),	             (90),          (0x88),		60000000},
	{30,		(338),			 (90),         (0x88),		60000000}, 	// adjust fps >=30fps for msoc jitter
};
*/

code GC_FpsSetting_t  g_staGC1136FpsSetting[]=
{
// FPS ExtraDummyPixel byExtraDummyLine clkrc	   pclk
//	{5, 	(1682+1683+1),			 (3),		14135400},		//GC1036 clk too low
	{5, 	(2311), 			 (53),		(0x01), 	  24000000},
	{10,		(816),				 (53),			(0x01), 		24000000},
	{15,		(1696), 		   (90),		  (0x04),		60000000},
	{20,		(1104), 			 (90),			(0x04), 		60000000},
	{25,		(736),				 (90),			(0x04), 	60000000},
	//{30,		(764),			 (8),		   (0x06),		84000000},	// adjust fps >=30fps for msoc jitter
	{30,		(511),			 (90),		   (0x04),		60000000},	// adjust fps >=30fps for msoc jitter
};

// GC Calculate setting, but FPS is wrong
/*
code GC_FpsSetting_t  g_staGC1136FpsSetting[]=
{
// FPS ExtraDummyPixel byExtraDummyLine clkrc	   pclk
	{5, 		(2600), 		(148),			(0x04), 	60000000},	 //FPS 9.997
	{10,		(1800), 		(442),			(0x04), 	60000000},	 //FPS 10.005
	{15,		(1200), 		(298),			(0x04), 	60000000},	 //FPS 15.001
	{20,		(800),			(248),			(0x04), 	60000000},	 //FPS 20.005
	{25,		(500),			(252),			(0x04), 	60000000},	 //FPS 25.01
	{30,		(373),			(185),			(0x04), 	60000000},	 // FPS 30.009 adjust fps >=30fps for msoc jitter

};
*/


t_RegSettingBB code gc_GC1136_Setting[] =
{
	/////////////////////////////////////////////////////
	//////////////////////	 SYS   //////////////////////
	/////////////////////////////////////////////////////
	{0xfe, 0xf0},
	{0xfe, 0xf0},
	{0xfe, 0xf0},
	{0xf2, 0x00}, //sync_pad_io_ebi
	{0xf6, 0x00}, //up down
	{0xf7, 0x29},  //19//pll enable
	{0xf8, 0x04}, // //30fps 03 //Pll mode 2
	{0xf9, 0x4e}, //de //[0] pll enable
	{0xfa, 0x00}, //div
	{0xfc, 0x06},
	{0xfe, 0x00},

	/////////////////////////////////////////////////////
	//////////////////////	 MIPI	/////////////////////
	/////////////////////////////////////////////////////
	{0xfe, 0x03},
	{0x01, 0x00},
	{0x02, 0x00},
	{0x03, 0x00},
	{0x06, 0x20},
	{0x10, 0x00},
	{0x15, 0x00},
	{0x41, 0x00},
	{0x42, 0x40},
	{0x43, 0x00},
	{0x40, 0x00},
	{0xfe, 0x00},

	/////////////////////////////////////////////////////
	////////////////   ANALOG & CISCTL	 ////////////////
	/////////////////////////////////////////////////////
	{0x03, 0x05},	
	{0x04, 0xa0}, //max 30fps
	{0x05, 0x02},
	{0x06, 0x3e},
	{0x07, 0x00},
	{0x08, 0x28},
	//{0x0a, 0x00}, //row start
	{0x0a, 0xf0}, //row start
	//{0x0c, 0x04}, //0c//col start
	{0x0c, 0xa0}, //0c//col start
	{0x0d, 0x02},
	{0x0e, 0xe0},	
	{0x0f, 0x05},
	//{0x10, 0x10}, //Window setting 1296x736
	{0x10, 0x30}, //20140905 GC recommand for module window setting //Window setting 1296x736
	{0x17, 0x14},
	{0x19, 0x0b}, //09
	{0x1b, 0x48}, //49
	{0x1c, 0x12},
	{0x1d, 0x10}, //double reset
	{0x1e, 0xbc}, //a8//col_r/rowclk_mode/rsthigh_en FPN
	{0x1f, 0xc8}, //08//rsgl_s_mode/vpix_s_mode �ƹܺ�����
	{0x20, 0x71},
	{0x21, 0x20}, //rsg
	{0x22, 0xa0},
	{0x23, 0x51}, //01
	{0x24, 0x19}, //0b //55
	{0x27, 0x20}, //�ƹܺ�����
	{0x28, 0x00},

	//20140926 Modify senser setting by GC
	//{0x2b, 0x81}, //80 //00 sf_s_mode FPN
	{0x2b, 0x80}, //80 //00 sf_s_mode FPN
	//----------------------------------//
	
	{0x2c, 0x38}, //50 //5c ispg FPN //ȥ��̫��
	{0x2e, 0x16}, //05//eq width 
	{0x2f, 0x14}, //[3:0]tx_width д0�ܸ����
	{0x30, 0x00},
	{0x31, 0x01},
	{0x32, 0x02},
	{0x33, 0x03},
	{0x34, 0x07},
	{0x35, 0x0b},
	{0x36, 0x0f},

	/////////////////////////////////////////////////////
	//////////////////////	 ISP	/////////////////////
	/////////////////////////////////////////////////////
	{0x8c, 0x02}, //06

	/////////////////////////////////////////////////////
	//////////////////////	 gain	/////////////////////
	/////////////////////////////////////////////////////
	{0xb0, 0x50}, //1.25x
	{0xb1, 0x05}, //02
	{0xb2, 0xb6}, //e0 //5.71x
	{0xb3, 0x40},
	{0xb4, 0x40},
	{0xb5, 0x40},
	//{0xb6, 0x03}, //2.8x
	{0xb6, 0x00}, //1x A gain

	/////////////////////////////////////////////////////
	//////////////////////	 crop	/////////////////////
	/////////////////////////////////////////////////////
	{0x92, 0x02},
	{0x94, 0x04},
	{0x95, 0x02},
	{0x96, 0xd0},
	{0x97, 0x05},
	//{0x98, 0x00}, //out window set 1280x720
	{0x98, 0x20}, //20140905 GC recommand for module window setting //out window set 1280x720

	/////////////////////////////////////////////////////
	//////////////////////	BLK /////////////////////
	/////////////////////////////////////////////////////
	{0x18, 0x02},
	{0x1a, 0x01},
	{0x40, 0x42},
	{0x41, 0x00},

	{0x44, 0x00},
	{0x45, 0x00},
	{0x46, 0x00},
	{0x47, 0x00},
	{0x48, 0x00},
	{0x49, 0x00},
	{0x4a, 0x00},
	{0x4b, 0x00}, //clear offset

	{0x4e, 0x3c}, //BLK select
	{0x4f, 0x00}, 
	{0x5c, 0x00}, //20140905 GC recommand for module BLK setting
	{0x5e, 0x00}, //offset ratio
	{0x66, 0x20}, //dark ratio
/*
	{0x6a, 0x02},
	{0x6b, 0x02},
	{0x6c, 0x02},
	{0x6d, 0x02},
	{0x6e, 0x02},
	{0x6f, 0x02},
	{0x70, 0x02},
	{0x71, 0x02}, //manual offset
*/
	//20140905 GC recommand for module BLK setting
	{0x6a, 0x00},
	{0x6b, 0x00},
	{0x6c, 0x00},
	{0x6d, 0x00},
	{0x6e, 0x00},
	{0x6f, 0x00},
	{0x70, 0x00},
	{0x71, 0x00}, //manual offset
	//------------------------------------------//
	
	/////////////////////////////////////////////////////
	//////////////////	 Dark sun	/////////////////////
	/////////////////////////////////////////////////////
	{0x87, 0x03}, //
	{0xe0, 0xe7}, //dark sun en/extend mode
	{0xe3, 0xc0}, //clamp


	{0xfe, 0x03},
	{0x40, 0x09},
	{0xfe, 0x00},
	//sleep 10
	{0xf2, 0x0f},

};


static GC_FpsSetting_t*  GetOvFpsSetting(U8 Fps, GC_FpsSetting_t staOvFpsSetting[], U8 byArrayLen)
{
	U8 i;
	U8 Idx;

	ISP_MSG((" array size = %bd\n", byArrayLen));

	if(g_bIsHighSpeed)
	{
		Idx=2; //8fps for uxga, 10 fps for vga
	}
	else
	{
		Idx = 0;
	}

	for(i=0; i< byArrayLen; i++)
	{
		ISP_MSG((" fps i = %bd\n",i));
		if(staOvFpsSetting[i].byFps ==Fps)
		{
			Idx=i;
			break;
		}
	}
	ISP_MSG((" get fps idx = %bd\n",Idx));
	return &staOvFpsSetting[Idx];
}

static U16 MapSnrGlbGain2SnrRegSetting(U16 wGain)
{
	U16 wSnrRegGain = 0;
	U16 bySnrRegGain_Low=0;
	U16 bySnrRegGain_High=0;

	wGC1136AutoPreGain = 0;
	//QDBG(("byGain = %bd-> ",byGain));

		if(wGain < 64 )
		{
			wGain <<= 2;

			/*
			bySnrRegGain_Low=wGain;
			
			if(bySnrRegGain_Low < 0x50)
			{
				bySnrRegGain_Low = 0x50;
			}
			
			bySnrRegGain_High=0x40;
			*/
			bybyGC1136AGain = 0x00; //1x A gain
			byGC1136GlobalGain = wGain;
			wGC1136AutoPreGain = 0x0100;
			//bySnrRegGain_Low = 0x00;
			//bySnrRegGain_High = 0x01;
		}	
		//else if(wGain < 1024)
		else if(wGain < 256)
		{   
			/*
		    bySnrRegGain_Low=0xff;
			bySnrRegGain_High=wGain;
			*/
			bybyGC1136AGain = 0x00; //1x A gain
			byGC1136GlobalGain = 0xff;
			wGC1136AutoPreGain = wGain >> 2;
			wGC1136AutoPreGain = wGC1136AutoPreGain << 4;		
		}
		else 
		{
			bybyGC1136AGain = 0x02;
			//wGain = (U16)(wGain/7.5294);			
			wGain = (U16)(wGain/1.5366);

			byGC1136GlobalGain = 0xff;
			wGC1136AutoPreGain = wGain >> 2;
			wGC1136AutoPreGain = wGC1136AutoPreGain << 4;
		}

		/*
		wGC1136AutoPreGain = bySnrRegGain_High;
		//wGC1136AutoPreGain<<=2;
		wGC1136AutoPreGain<<=4;
			
		bySnrRegGain_High<<=8;
		
		wSnrRegGain=bySnrRegGain_High|bySnrRegGain_Low;
		*/
	
	//QDBG(("sensorGain = %d    ",sensorGain));
	//return wSnrRegGain;
	return 1;
}

float SnrRegSetting2SnrGlbGain(U16 wSnrRegGain)
{
	U16 wGain;
	float fGainOffset;
	U8 i;

	if(wSnrRegGain >= 0x70)
	{
		fGainOffset = 1.1; //0.99933; //0.5;
	}
	else if(wSnrRegGain >= 0x30)
	{
		fGainOffset = 0.360577; //0.32349; //0.2;
	}
	else if(wSnrRegGain >= 0x10)
	{
		fGainOffset = 0.107287; //0.11564; //0.0625;
	}
	else
	{
		fGainOffset = 0;
	}

	wGain = (wSnrRegGain&0x0f) + 16;
	wSnrRegGain >>= 4;

	for(i = 0; i < 4; i++)
	{
		if((wSnrRegGain&1) == 1)
		{
			wGain <<= 1;
		}

		wSnrRegGain >>= 1;
	}

	//return ((float)wGain)/16.0-fGainOffset;
	//return (((float)wGain)/16.0-fGainOffset-1.0)*1.27 + 1.0;
	return ((((float)wGain)/16.0-fGainOffset)-1.0)*1.2 + 1.0;
}

//this function is also called during Probe stage to calculate the transfer_size/LWM/HWM
void GetSensorPclkHsync(U16 wSensorSPFormat,U8 byFps)
{
	GC_FpsSetting_t *pGC1136_FpsSetting;

	wSensorSPFormat =wSensorSPFormat;
	
	pGC1136_FpsSetting=GetOvFpsSetting(byFps, g_staGC1136FpsSetting, sizeof(g_staGC1136FpsSetting)/sizeof(GC_FpsSetting_t));
	g_wSensorHsyncWidth = pGC1136_FpsSetting->wExtraDummyPixel; // this for manual exposure
	g_dwPclk = pGC1136_FpsSetting->dwPixelClk;		// this for scale speed
}

void GC1136SetFormatFps(U16 SetFormat, U8 Fps)
{
	U16 wTemp;
	GC_FpsSetting_t *pGC1136_FpsSetting;

	// hemonel 2010-04-13: for delete complier warning
	SetFormat = SetFormat;

	pGC1136_FpsSetting=GetOvFpsSetting(Fps, g_staGC1136FpsSetting, sizeof(g_staGC1136FpsSetting)/sizeof(GC_FpsSetting_t));
	// 1) change hclk if necessary

	// initial all register setting
	WriteSensorSettingBB(sizeof(gc_GC1136_Setting)/2, gc_GC1136_Setting);

	// 2) write sensor register
	//Write_SenReg(0x0305, pGC1036_FpsSetting->byClkrc);
	
	wTemp = pGC1136_FpsSetting->wExtraDummyPixel;
	Write_SenReg(0x06, INT2CHAR(wTemp, 0)); //Write dummy pixel LSB
	Write_SenReg(0x05, INT2CHAR(wTemp, 1)); //Write dummy pixel MSB
	Write_SenReg(0x08, pGC1136_FpsSetting->byExtraDummyLine); //Write Dummy line
	Write_SenReg(0xf8, pGC1136_FpsSetting->byClkrc);//Write PLL
	
	g_wSensorHsyncWidth= 1296+pGC1136_FpsSetting->wExtraDummyPixel+570;
	//g_wSensorHsyncWidth= (1296/2+pGC1136_FpsSetting->wExtraDummyPixel+18+4);
	g_wAECExposureRowMax = 736+pGC1136_FpsSetting->byExtraDummyLine-6;//730;//1095;//970;	// this for max exposure time
	g_wAEC_LineNumber = 736+pGC1136_FpsSetting->byExtraDummyLine;//736;//1100;//975;	// this for AE insert dummy line algothrim
    g_dwPclk = pGC1136_FpsSetting->dwPixelClk;	
	g_wSensorWidthBefBLC = 1280;
	g_wSensorHeightBefBLC = 720;

	InitGC1136IspParamsByResolution(g_wCurFrameWidth, g_wCurFrameHeight);

#ifdef _MIPI_EXIST_
	//-----------------------------Dphy Setting--------------------------------
	SetMipiDphy(MIPI_DATA_LANE0_EN, \
	            MIPI_DATA_FORMAT_RAW10 , MIPI_DATA_TYPE_RAW10, \
	            HSTERM_EN_TIME_33);
#endif
}

void CfgGC1136ControlAttr(void)
{
	U8 i;

	g_bySensorSize = SENSOR_SIZE_HD720P;
	g_wSensorSPFormat =HD720P_FRM;
#ifdef _ENABLE_MJPEG_
	g_byQtableScale = 6;	//HD720P HD800P Setting=6 for 7 comp rate
#endif
	{
		memcpy(g_asOvCTT, gc_GC1136_CTT,sizeof(gc_GC1136_CTT));
	}

	{
		// hemonel 2010-09-08: support 1.8V sensor IO
#if (_CHIP_ID_ & _RTS5840_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V79;
#elif (_CHIP_ID_ & _RTS5832_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V85;
#elif (_CHIP_ID_ & _RTS5829B_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V82;
   		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V80;
#endif
	}

	{
		//====== resolution  setting ===========
		// -- preview ---
		g_aVideoFormat[0].byaVideoFrameTbl[0] = 8;		// resolution number
		g_aVideoFormat[0].byaVideoFrameTbl[1]=F_SEL_640_480;
		g_aVideoFormat[0].byaVideoFrameTbl[2]=F_SEL_320_180;
		g_aVideoFormat[0].byaVideoFrameTbl[3]=F_SEL_320_240;
		g_aVideoFormat[0].byaVideoFrameTbl[4]=F_SEL_424_240;
		g_aVideoFormat[0].byaVideoFrameTbl[5]=F_SEL_640_360;
		g_aVideoFormat[0].byaVideoFrameTbl[6]=F_SEL_848_480;	
		g_aVideoFormat[0].byaVideoFrameTbl[7]=F_SEL_960_540;
		g_aVideoFormat[0].byaVideoFrameTbl[8]=F_SEL_1280_720;		

		//--still image--
		g_aVideoFormat[0].byaStillFrameTbl[0] = 6;	// resolution number
		//g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_1280_800;
		g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_1280_720;
		g_aVideoFormat[0].byaStillFrameTbl[2]=F_SEL_160_120;
		g_aVideoFormat[0].byaStillFrameTbl[3]=F_SEL_176_144;
		g_aVideoFormat[0].byaStillFrameTbl[4]=F_SEL_320_240;
		g_aVideoFormat[0].byaStillFrameTbl[5]=F_SEL_352_288;
		g_aVideoFormat[0].byaStillFrameTbl[6]=F_SEL_640_480;

		
		//====== fps  setting ===========
		{
			for(i=0; i<10; i++)
			{
				/*
					F_SEL_640_480    (0)
					F_SEL_160_120    (1)
					F_SEL_176_144    (2)
					F_SEL_320_180	(3)
					F_SEL_320_200    (4)
					F_SEL_320_240    (5)
					F_SEL_352_288    (6)
					F_SEL_424_240	(7)
					F_SEL_640_360	(8)
					F_SEL_640_400    (9)
				*/
				g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_15|FPS_30;
			}
			for(i=10; i<18; i++)
			{
				/*
					F_SEL_800_600    (10)
					F_SEL_848_480	(11)
					F_SEL_960_540	 (12)
					F_SEL_1024_768   (13)
					F_SEL_1280_720   (14)
					F_SEL_1280_800   (15)
					F_SEL_1280_960   (16)
					F_SEL_1280_1024  (17)
				*/
				g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_10;
			}
		}
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_800_600]= FPS_10;
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_848_480]= FPS_15;
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_960_540]= FPS_10;

		//====== format type  setting ===========
		g_aVideoFormat[0].byFormatType = FORMAT_TYPE_YUY2;

#ifdef _ENABLE_MJPEG_
		// copy YUY2 setting to MJPEG setting
		memcpy(&(g_aVideoFormat[1]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

		// modify MJPEG FPS setting
		for(i=10; i<18; i++)
		{
			g_aVideoFormat[1].waFrameFpsBitmap[i] = (FPS_15|FPS_30);
		}

		// modify MJPEG format type
		g_aVideoFormat[1].byFormatType = FORMAT_TYPE_MJPG;
#endif

#ifdef _ENABLE_M420_FMT_
		// copy YUY2 setting to M420 setting
		memcpy(&(g_aVideoFormat[2]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

		// modify M420 FPS setting
		{
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_800_600] =FPS_25|FPS_20|FPS_15|FPS_10|FPS_5;//|FPS_1;             //bit07 F_800_600
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_960_540] =FPS_25|FPS_20|FPS_15|FPS_10|FPS_5;//|FPS_5|FPS_3|FPS_1;                    //bit08 F_960_720
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1024_768] =FPS_15|FPS_10|FPS_5;//|FPS_1;                    //bit09 F_1024_768
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_720] =FPS_10|FPS_5;//FPS_9|FPS_8|FPS_1;    //bit10 F_1280_720
			g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_800] =FPS_10|FPS_5;

			// modify M420 format type
			g_aVideoFormat[2].byFormatType = FORMAT_TYPE_M420;
		}
#endif
	}

	{
		//Brightness - BrightnessItem

		//Contrast - ContrastItem

		//Saturation - SaturationItem

		//Hue - 	HueItem

		//Sharpness - SharpnessItem
		//	SharpnessItem.Min = 0;
		//	SharpnessItem.Max = 8;
		//	SharpnessItem.Res = 1;
		//	SharpnessItem.Def = 3;
		//	SharpnessItem.Last  =SharpnessItem.Def;

		// gamma - 	GammaItem

		// gain - GainItem

		// whitebalance - WhiteBalanceTempItem

		// backlight - BackLightCompItem

		// powerline -PwrLineFreqItem
		//	PwrLineFreqItem.Def = PWR_LINE_FRQ_60;
		//	PwrLineFreqItem.Last = PwrLineFreqItem.Def;

		// exposure time -	ExposureTimeAbsolutItem

		// low light - LowLightCompItem
	}

	InitGC1136IspParams();
}

void SetGC1136IntegrationTime(U16 wEspline)
{
	U16 wFrameLenLines;
	U16 wExpMax = g_wAECExposureRowMax-2;

	if(wEspline > wExpMax)
	{
		wFrameLenLines = wEspline + 20;
	}
	else
	{
		wFrameLenLines = g_wAEC_LineNumber;
	}

	if (wFrameLenLines%2==1)
	{
		wFrameLenLines++;
	}
	Write_SenReg(0x04, INT2CHAR(wEspline, 0));	// change exposure value low
	Write_SenReg(0x03, INT2CHAR(wEspline, 1));	// change exposure value high
/*
	//-----------Write  frame length or dummy lines------------
	Write_SenReg(0x0341, INT2CHAR(wFrameLenLines, 0));
	Write_SenReg(0x0340, INT2CHAR(wFrameLenLines, 1));

	//-----------Write Exposuretime setting-----------
	Write_SenReg(0x0203, INT2CHAR(wEspline, 0));	// change exposure value low
	Write_SenReg(0x0202, INT2CHAR(wEspline, 1));	// change exposure value high

	//----------- write gain setting-------------------
	Write_SenReg(0x0205, 0x0f);	// fixed gain at manual exposure control
	Write_SenReg(0x0204, 0);
	*/
}

void SetGC1136ImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.
{

	WaitFrameSync(ISP_INT1,ISP_DATA_START_INT);

	//bySnrImgDir ^= 0x02; 	// GC1036 output mirrored image at default, so need mirror the image for normal output
	Write_SenReg_Mask(0x17, bySnrImgDir, 0x03);
	if(bySnrImgDir==0x02)
	{
	
      Write_SenReg(0x92, 0x03);
	Write_SenReg(0x94, 0x05); 
	}
	else if(bySnrImgDir==3)
	{
      Write_SenReg(0x92, 0x03);
	Write_SenReg(0x94, 0x06); 
	}
	else if(bySnrImgDir==0)
	{
      Write_SenReg(0x92, 0x02);
	Write_SenReg(0x94, 0x05); 
	}	
	else
	{
       Write_SenReg(0x92, 0x02);
	 Write_SenReg(0x94, 0x06); 
	}
	WaitFrameSync(ISP_INT1,ISP_DATA_END_INT);
     
/*
	if(bySnrImgDir & 0x01)
	{
#ifdef _MIPI_EXIST_
		SetBLCWindowStart(1, 1);
#else
		SetBkWindowStart(1, 1);
#endif
	}
	else
	{
#ifdef _MIPI_EXIST_
		SetBLCWindowStart(0, 1);
#else
		SetBkWindowStart(0, 1);
#endif
	}
	*/

}

void SetGC1136Exposuretime_Gain(float fExpTime, float fTotalGain)
{

	U16 wExposureRows_floor;
	U16  wGainRegSetting; //data
	float fSnrGlbGain;
	float fExposureRows;
	U16 wSetDummy;
	float fTemp;
	U16 wExposurePixels;
	U8 bySnrAnalogGain;
	U8 bywGC1136AutoPreGain_LB;
	U8 bywGC1136AutoPreGain_HB;


	//bySnrAnalogGain = 0x06;		
	//wGainRegSetting = MapSnrGlbGain2SnrRegSetting((U16)( (fTotalGain)*23.52) );

	wGainRegSetting = MapSnrGlbGain2SnrRegSetting((U16)(fTotalGain*16.0));
	//wGainRegSetting = MapSnrGlbGain2SnrRegSetting((U16)(fTotalGain*20.0));

	//wGainRegSetting = byGC1136GlobalGain;
	bywGC1136AutoPreGain_LB = INT2CHAR(wGC1136AutoPreGain, 0);
	bywGC1136AutoPreGain_HB = INT2CHAR(wGC1136AutoPreGain, 1);

	fSnrGlbGain = SnrRegSetting2SnrGlbGain(wGainRegSetting);
	
	fExposureRows = fExpTime/g_fSensorRowTimes;	//unit : 1row
	wExposureRows_floor = (U16)(fExposureRows);
	fTemp = fExpTime-((float)wExposureRows_floor*(g_fSensorRowTimes)); //fine integration time;unit:  us
	wExposurePixels =(U16)(fTemp * (float)(g_dwPclk/10000)) ; //unit: pixels

	// write exposure, gain, dummy line
	if(fabs(g_fCurExpTime-fExpTime) > 0.0001)
	{
		// set dummy
		wSetDummy = g_wAFRInsertDummylines + g_wAEC_LineNumber;

		// group register hold
		//Write_SenReg(0x0104, 0x01);
  
		//-----------Write  frame length or dummy lines------------
		if (wSetDummy%2 == 1)
		{
			wSetDummy++;
		}

              //wSetDummy=wSetDummy-1296/2-832-4;
		Write_SenReg(0x08, INT2CHAR(wSetDummy, 0));
		Write_SenReg(0x07, INT2CHAR(wSetDummy, 1));

            wExposureRows_floor=wExposureRows_floor;

		if(wExposureRows_floor<1) wExposureRows_floor=1;
		if(wExposureRows_floor>8192)wExposureRows_floor=8192;

			  
		//-----------Write Exposuretime setting-----------
		Write_SenReg(0x04, INT2CHAR(wExposureRows_floor, 0));	// change exposure value low
		Write_SenReg(0x03, INT2CHAR(wExposureRows_floor, 1));	// change exposure value high
		
		//WaitFrameSync(ISP_INT1, ISP_DATA_START_INT);
		
		//----------- write gain setting-------------------
		Write_SenReg(0xb6, bybyGC1136AGain);
		Write_SenReg(0xb0, byGC1136GlobalGain);
		Write_SenReg_Mask(0xb1, bywGC1136AutoPreGain_HB, 0x0F);
		Write_SenReg_Mask(0xb2, bywGC1136AutoPreGain_LB, 0xFC);
             
		//SetISPAEGain(fTotalGain, fSnrGlbGain, 1);

		// group register release
		//Write_SenReg(0x0104, 0);
		g_fCurExpTime = fExpTime;
	}
	else
	{
		// group register hold
		//Write_SenReg(0x0104, 1);
		wSetDummy = g_wAFRInsertDummylines + g_wAEC_LineNumber;

		//-----------Write  frame length or dummy lines------------
		if (wSetDummy%2 == 1)
		{
			wSetDummy++;
		}
		Write_SenReg(0x06, INT2CHAR(wSetDummy, 0));
		Write_SenReg(0x05, INT2CHAR(wSetDummy, 1));

		// write gain setting
		Write_SenReg(0xb6, bybyGC1136AGain);
		Write_SenReg(0xb0, byGC1136GlobalGain);
		Write_SenReg_Mask(0xb1, bywGC1136AutoPreGain_HB, 0x0F);
		Write_SenReg_Mask(0xb2, bywGC1136AutoPreGain_LB, 0xFC);

		//SetISPAEGain(fTotalGain, fSnrGlbGain, 1);

		// group register release
		//Write_SenReg(0x0104, 0);
	}


	return;

}

void GC1136_POR(void )
{
	g_bySnrPowerOnSeq =SNR_PWRCTL_SEQ_GPIO8|(SNR_PWRCTL_SEQ_SV28<<2) | (SNR_PWRCTL_SEQ_SV18<<4); 
	g_bySnrPowerOffSeq = SNR_PWRCTL_SEQ_SV18| (SNR_PWRCTL_SEQ_SV28<<2)| (SNR_PWRCTL_SEQ_GPIO8 <<4);
#ifdef DLINK_SHORT_PWDN
	XBYTE[PG_DELINK_CTRL] = DELINK_HIGH_ACTIVE;
#endif
#ifdef _MIPI_EXIST_
	Init_MIPI();
#else
	Init_CCS();
#endif
	ENTER_SENSOR_RESET();
	// power on
	SensorPowerControl(SWITCH_ON,EN_DELAYTIME);
	uDelay(1);
	// Initialize I2C Controller Timing
	//LEAVE_SENSOR_RESET();
	
	// clock select 24M
	if( g_bySSCEnable== TRUE)
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_SSC_24M);
	}
	else
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_96M|CCS_CLK_DIVIDER_4);
	}
	
	EnableSensorHCLK();
	g_bySensorIsOpen = SENSOR_OPEN;
	WaitTimeOut_Delay(1);  //GC1136 spec request 8192clk
#ifdef DLINK_SHORT_PWDN
	XBYTE[PG_DELINK_CTRL] = PAD_DELINK_OUT;
#endif

	LEAVE_SENSOR_RESET();

	WaitTimeOut_Delay(10);
}

void InitGC1136IspParams(void )
{
	// AWB initial gain
	//use D50 R/G/B gain as AWB initial value
	g_wAWBRGain_Last= 0x17C;
	g_wAWBGGain_Last= 0x100;
	g_wAWBBGain_Last= 0x226;
	g_wProjectGR = g_wAWBRGain_Last;
	g_wProjectGB = g_wAWBBGain_Last;

	// Special ISP
	g_bySaturation_Def =64;
	g_byContrast_Def = 32; //Neil Tuning at chicony
	g_byTgamma_rate_max=63;
	g_byTgamma_rate_min =20;

	//g_wDynamicISPEn = 0;
	g_wDynamicISPEn = DYNAMIC_SHARPPARAM_EN|DYNAMIC_CCM_CT_EN;
}

void InitGC1136IspParamsByResolution(U16 wCurWidth, U16 wCurHeight)
{
	wCurWidth = wCurWidth;
	wCurHeight = wCurHeight;
}

void SetGC1136DynamicISP(U8 byAEC_Gain)
{
	byAEC_Gain = byAEC_Gain;
}

void SetGC1136DynamicISP_AWB(U16 wColorTempature)
{
	wColorTempature =wColorTempature;

	// hemonel 2011-07-04: Albert use only one LSC curve to pass MSOC premium test, so delete these code
	/*
	#ifdef _MSOC_TEST_
	U8 i;

	// LSC Curve dynamic
	if ( (g_wDynamicISPEn&DYNAMIC_LSC_EN) == DYNAMIC_LSC_EN)
	{
		if(wColorTempature < 4000)
		{
	          		for (i=0;i<6;i++)
	          		{
	          			g_awLensShadingCenter[i] = gc_wLSC_Center_Offset_GC1036_3500K[i];
	          		}

				for (i=0;i<48;i++)
				{
					XBYTE[ISP_NLSC_CURVE_R_BASE+i]  = gc_byLSC_RGB_Curve_GC1036_3500K[0][i];
					XBYTE[ISP_NLSC_CURVE_G_BASE+i]  = gc_byLSC_RGB_Curve_GC1036_3500K[1][i];
					XBYTE[ISP_NLSC_CURVE_B_BASE+i]  = gc_byLSC_RGB_Curve_GC1036_3500K[2][i];
				}
		}
		else if(wColorTempature > 4500)
		{
	          		for (i=0;i<6;i++)
	          		{
	          			g_awLensShadingCenter[i] = gc_wLSC_Center_Offset_GC1036_D65[i];
	          		}

				for (i=0;i<48;i++)
				{
					XBYTE[ISP_NLSC_CURVE_R_BASE+i]  = gc_byLSC_RGB_Curve_GC1036_D65[0][i];
					XBYTE[ISP_NLSC_CURVE_G_BASE+i]  = gc_byLSC_RGB_Curve_GC1036_D65[1][i];
					XBYTE[ISP_NLSC_CURVE_B_BASE+i]  = gc_byLSC_RGB_Curve_GC1036_D65[2][i];
				}
		}

	}
	#endif
	*/
}
#endif
