#include "Inc.h"

#ifdef RTS58XX_SP_OV7740

OV_CTT_t code gc_OV7740_CTT[3]=
{
	{2800,0x40,0x4B,0xC2},
	{4050,0x4E,0x40,0x97},
	{6500,0x5F,0x40,0x5E},
};

OV_FpsSetting_t  code g_staOV7740FpsSetting[]=
{
	//FPS      dummypixel       clkrc      pclk = 24M/(clkrc+1)
//	{1,		(791+208-8),	(47),	  1000000},
	{3,		(791+541-8),	(11),	  2000000},
	{5, 		(791+0),		(11),	  2000000},
	{8,  		(791+208-8),	(5),		  4000000},
	{9,  		(791+97-8),	(5),		  4000000},
	{10,		(791+0),		(5),		  4000000},
	{15,		(791+0),		(3),		6000000},
	{20,		(791+0),		(2),		8000000},
	{25,		(791+168-8),	(1),		12000000},
	{30,		(791+0),		(1),		12000000},
};


t_RegSettingBB code gc_OV7740_Setting[] =
{
	{0x11, 0x01},//clk divider
	{0x12, 0x00},//yuv out
	{0xd5, 0x10},//scale zoom out mode
	{0x0c, 0x12},//UYVY... mode
	{0x0d, 0x34},//rsvd
	{0x17, 0x25},//horizontal start 8 msb /10bits
	{0x18, 0xa0},//horizontal size 8 msb /10bits
	{0x19, 0x03},//vertical start 8 msb /9bits
	{0x1a, 0xf0},//vertical size 8 msb /9bits
	{0x1b, 0x81},//pixel shift
	{0x29, 0x17},//horizontal tp counter end point lsb, 0x2a msb
	{0x2b, 0xf8},//row counter end point lsb
	{0x2c, 0x01},//row counter end point msb
	{0x31, 0xa0},//dsp H out size 8msb /10bits
	{0x32, 0xf0},//dsp V out size 8msb /9bits
	{0x33, 0x44},//rsvd
	{0x36, 0x3f},//rsvd

	{0x04, 0x60},//rsvd
	{0x27, 0x80},//black sun cancellation en
	{0x3a, 0xb4},
	{0x3d, 0x0f},
	{0x3e, 0x80},
	{0x3f, 0x40},
	{0x40, 0x7f},
	{0x41, 0x6a},
	{0x42, 0x29},
	{0x44, 0x11},
	{0x45, 0x41},
	{0x47, 0x02},
	{0x49, 0x64},
	{0x4a, 0xa1},
	{0x4b, 0x40},
	{0x4c, 0x1a},
	{0x4d, 0x50},
	{0x4e, 0x13},
	{0x64, 0x00},//rsvd
	{0x67, 0x88},//blc target
	{0x68, 0x1a},//rsvd

	{0x14, 0x38},//AGC gain ceiling 16x
	//{0x24, 0x3c},//Aec agc high range
	//{0x25, 0x30},//Aec agc low range
	//{0x26, 0x72},//vpt
	{0x50, 0x97},//banding 50Hz step lsb
	{0x51, 0x7e},//banding 60Hz step lsb
	{0x52, 0x00},//banding 50/60Hz step msb
	{0x53, 0x00},//rsvd
	{0x20, 0x00},
	{0x21, 0x23},
	{0x38, 0x14},//monitor i2c reg sub-addr |>
	{0xe9, 0x00},//threshold for blackness
	{0x56, 0x55},//16-zone Y average select
	{0x57, 0xff},
	{0x58, 0xff},
	{0x59, 0xff},
	{0x5f, 0x04},//rsvd
	{0xec, 0x00},//BD50/60auto, MBand50/60
	{0x13, 0xff},//misc sensor

	{0x15, 0x90},//night mode, 1/2 frame rate_jqg_20090427_

	{0x80, 0x7f},//misc isp
	{0x81, 0x3f},//misc isp, color interplation
	{0x82, 0x32},//misc isp, scale, window
	{0x83, 0x01},//misc isp
	{0x38, 0x11},//monitor i2c reg sub-addr <|
	{0x84, 0x70},//bist ctrl
	{0x85, 0x00},//AGC offset, for dsn_th auto value
	{0x86, 0x03},
	{0x87, 0x01},
	{0x88, 0x05},//AGC ctrl
	//{0x89, 0x30},//lenc ctrl
	//{0x8d, 0x30},
	//{0x8f, 0x85},
	//{0x93, 0x30},
	//{0x95, 0x85},
	//{0x99, 0x30},
	//{0x9b, 0x85},

	//{0x9c, 0x08},//GMA
	//{0x9d, 0x12},
	//{0x9e, 0x23},
	//{0x9f, 0x45},
	//{0xa0, 0x55},
	//{0xa1, 0x64},
	//{0xa2, 0x72},
	//{0xa3, 0x7f},
	//{0xa4, 0x8b},
	//{0xa5, 0x95},
	//{0xa6, 0xa7},
	//{0xa7, 0xb5},
	//{0xa8, 0xcb},
	//{0xa9, 0xdd},
	//{0xaa, 0xec},
	//{0xab, 0x1a},

	//{0xce, 0x78},//CMX
	//{0xcf, 0x6e},
	//{0xd0, 0x0a},
	//{0xd1, 0x0c},
	//{0xd2, 0x84},
	//{0xd3, 0x90},
	//{0xd4, 0x1e},

	{0x5a, 0x24},//rsvd
	{0x5b, 0x1f},
	{0x5c, 0x88},
	{0x5d, 0x60},

	{0xac, 0x6e},//rsvd White Balance
	{0xbe, 0xff},
	{0xbf, 0x00},
};


t_RegSettingBB code gc_OV7740_ISP_Setting[] =
{
	//---OV7740 LSC---
	{0x89, 0x30}, //LSC ctrl
	/*
		{0x8a, 0x40}, //R x0
		{0x8b, 0xf0}, //R y0
		{0x8c, 0x01}, //R y0[8], x0[9:8]
		{0x8d, 0x41}, //R a1[6:0]
		{0x8e, 0xfe}, //R b1[7:0]
		{0x8f, 0x53}, //R b2[7:4], a2[3:0]

		{0x90, 0x40}, //G x0
		{0x91, 0xf0}, //G y0
		{0x92, 0x01}, //G y0[8], x0[9:8]
		{0x93, 0x30}, //G a1[6:0]
		{0x94, 0xC2}, //G b1[7:0]
		{0x95, 0x43}, //G b2[7:4], a2[3:0]

		{0x96, 0x40}, //B x0
		{0x97, 0xf0}, //B y0
		{0x98, 0x01}, //B y0[8], x0[9:8]
		{0x99, 0x30}, //B a1[6:0]
		{0x9a, 0xC2}, //B b1[7:0]
		{0x9b, 0x43}, //B b2[7:4], a2[3:0] --- <|above settings only for OV module, fluorescent light


		{0x8a, 0x57},//for 95% LSC
		{0x8b, 0xe7},
		{0x8c, 0x1 },
		{0x8d, 0x60}, //60:D65; 48:cwf
		{0x8e, 0xff},
		{0x8f, 0x53},

		{0x90, 0x57},
		{0x91, 0xe7},
		{0x92, 0x1 },
		{0x93, 0x33},
		{0x94, 0x95},
		{0x95, 0x43},

		{0x96, 0x55},
		{0x97, 0xe7},
		{0x98, 0x1 },
		{0x99, 0x34},
		{0x9a, 0xb3},
		{0x9b, 0x43},

		//---OV7740 AE---
		{0x24, OV7740_AEW}, //Aec agc high range
		{0x25, OV7740_AEB}, //Aec agc low range
		{0x26, OV7740_VPT}, //vpt

		//---OV7740 AWB---
		{0xac, 0x2e},//Advance AWB - Lens_9347
		{0xad, 0x25},
		{0xae, 0x11},
		{0xaf, 0x12},
		{0xb0, 0x01},
		{0xb1, 0x48},
		{0xb2, 0x10},
		{0xb3, 0x0e},
		{0xb4, 0x0a},
		{0xb5, 0xff},
		{0xb6, 0xa6},
		{0xb7, 0x4d},
		{0xb8, 0x88},
		{0xb9, 0x85},
		{0xba, 0x5b},
		{0xbb, 0x29},
		{0xbc, 0x6b},
		{0xbd, 0x3d},
		{0xbe, 0xff},
		{0xbf, 0x00},

		//---OV7740 GMA---
		{0x9c, 0x08},//DEFAULT GMA
		{0x9d, 0x12},
		{0x9e, 0x23},
		{0x9f, 0x45},
		{0xa0, 0x55},
		{0xa1, 0x64},
		{0xa2, 0x72},
		{0xa3, 0x7f},
		{0xa4, 0x8b},
		{0xa5, 0x95},
		{0xa6, 0xa7},
		{0xa7, 0xb5},
		{0xa8, 0xcb},
		{0xa9, 0xdd},
		{0xaa, 0xec},
		{0xab, 0x1a},

		//---OV7740 CMX---
		{0xce, 0x78},//CMX
		{0xcf, 0x6e},
		{0xd0, 0x0a},
		{0xd1, 0x0c},
		{0xd2, 0x84},
		{0xd3, 0x90},
		{0xd4, 0x1e},
	*/

	//---OV7740 LSC---Redck
	{0x8a,0x42},
	{0x8b,0xc6},
	{0x90,0x42},
	{0x91,0xc6},
	{0x96,0x42},
	{0x97,0xc6},
	{0x8c,0x01},
	{0x8d,0xd0},
	{0x8e,0xc2},
	{0x8f,0xf3},
	{0x92,0x01},
	{0x93,0xf6},
	{0x94,0xc2},
	{0x95,0xe4},
	{0x98,0x01},
	{0x99,0xf6},
	{0x9a,0xc2},
	{0x9b,0xf4},

	//---OV7740 ---Gamma---Redck
	{0x9c,0x08},
	{0x9d,0x16},
	{0x9e,0x2f},
	{0x9f,0x56},
	{0xa0,0x66},
	{0xa1,0x75},
	{0xa2,0x80},
	{0xa3,0x88},
	{0xa4,0x8f},
	{0xa5,0x96},
	{0xa6,0xa3},
	{0xa7,0xaf},
	{0xa8,0xc4},
	{0xa9,0xd7},
	{0xaa,0xe8},
	{0xab,0x20},

	//---OV7740 ---CCM---Redck
	/*CCM--1
	{0xce,0x2d},
	{0xcf,0x27},
	{0xd0,0x07},
	{0xd1,0x09},
	{0xd2,0x32},
	{0xd3,0x3c},
	{0xd4,0x5e},
	*/
	//CCM--2
	{0xce,0x2b},
	{0xcf,0x22},
	{0xd0,0x0a},
	{0xd1,0x03},
	{0xd2,0x31},
	{0xd3,0x35},
	{0xd4,0x5e},

	/*CCM--3
	{0xce,0x2c},
	{0xcf,0x1c},
	{0xd0,0x11},
	{0xd1,0x06},
	{0xd2,0x33},
	{0xd3,0x3b},
	{0xd4,0x5e},
	*/

	//---OV7740 ---AWB---Redck
	{0x80,0x7f},
	{0xac,0x0e},
	{0xad,0x25},
	{0xae,0x11},
	{0xaf,0x12},
	{0xb0,0x01},
	{0xb1,0x48},
	{0xb2,0x25},
	{0xb3,0x2e},
	{0xb4,0x32},
	{0xb5,0xff},
	{0xb6,0xb7},
	{0xb7,0x88},
	{0xb8,0x30},
	{0xb9,0x85},
	{0xba,0x20},
	{0xbb,0x18},
	{0xbc,0x40},
	{0xbd,0x48},
	{0xbe,0xff},
	{0xbf,0x00},

	//---OV7740---Denoise & Sharpness----Redck
	{0xcc,0x63},
	{0xcb,0x06},

};

static OV_FpsSetting_t*  GetOvFpsSetting(U8 Fps, OV_FpsSetting_t staOvFpsSetting[], U8 byArrayLen)
{
	U8 i;
	U8 Idx;

	//ISP_MSG((" array size = %bd\n", byArrayLen));

	if (g_bIsHighSpeed)
	{
		Idx=2; //8fps for uxga, 10 fps for vga
	}
	else
	{
		Idx = 0;
	}

	for(i=0; i< byArrayLen; i++)
	{
		//ISP_MSG((" fps i = %bd\n",i));
		if(staOvFpsSetting[i].byFps ==Fps)
		{
			Idx=i;
			break;
		}
	}
	//ISP_MSG((" get fps idx = %bd\n",Idx));
	return &staOvFpsSetting[Idx];
}

void GetSensorPclkHsync(U16 wSensorSPFormat, U8 byFps)
{
	OV_FpsSetting_t *pOV7740_FpsSetting;
	pOV7740_FpsSetting = GetOvFpsSetting(byFps, g_staOV7740FpsSetting, sizeof(g_staOV7740FpsSetting)/sizeof(OV_FpsSetting_t));
	g_wSensorHsyncWidth = pOV7740_FpsSetting->wExtraDummyPixel;
	g_dwPclk= pOV7740_FpsSetting->dwPixelClk;	
}

void OV7740SetFormatFps(U8 SetFormat, U8 byFps)
{
	OV_FpsSetting_t *pOV7740_FpsSetting;

	SetFormat = SetFormat;	// for delete warning

	pOV7740_FpsSetting = GetOvFpsSetting(byFps, g_staOV7740FpsSetting, sizeof(g_staOV7740FpsSetting)/sizeof(OV_FpsSetting_t));

	// hemonel 2009-08-05: move to Resetsensor func() for still image time
//	Write_SenReg(0x12, 0x80);//sensor soft reset
//	WaitTimeOut_Delay(1);
	WriteSensorSettingBB(sizeof(gc_OV7740_Setting)>>1, gc_OV7740_Setting);//sensor common set
	// hemonel 2010-01-13: move to here
	WriteSensorSettingBB(sizeof(gc_OV7740_ISP_Setting)>>1, gc_OV7740_ISP_Setting);

	Write_SenReg_Mask(0x11, pOV7740_FpsSetting->byClkrc, 0x3F); // write CLKRC
	Write_SenReg(0x29, INT2CHAR(pOV7740_FpsSetting->wExtraDummyPixel,0)); //Write dummy pixel LSB
	Write_SenReg(0x2A, INT2CHAR(pOV7740_FpsSetting->wExtraDummyPixel,1)); // Writedummy pixel MSB

	g_wAECExposureRowMax = 502;
	//g_wSensorHsyncWidth = pOV7740_FpsSetting->wExtraDummyPixel;
	//g_dwPclk= pOV7740_FpsSetting->dwPixelClk;
	GetSensorPclkHsync(VGA_FRM, byFps)
}


void CfgOV7740ControlAttr(void)
{
	g_bySensorSize = SENSOR_SIZE_VGA;
	g_bySensorSPFormat = VGA_FRM;

	{
		memcpy(g_asOvCTT,gc_OV7740_CTT,sizeof(gc_OV7740_CTT));
	}

	{
		// AE backlight compensation parameter
		g_byOVAEW_Normal = OV7740_AEW;
		g_byOVAEB_Normal = OV7740_AEB;

		g_byOVAEW_BLC = g_byOVAEW_Normal + 0x20;
		g_byOVAEB_BLC = g_byOVAEB_Normal + 0x20;
	}

	{
		//g_bySV28VoltSel =  SV28_VOL_3V3;	// 2009-05-13: conference deside ov7740 use 3.3v, not use 3.3v vubs power
		//g_bySV18VoltSel=  SV18_VOL_1V5;
		//	XBYTE[REG_TUNESVIO_SVA] =  SV28_VOL_3V3|SV18_VOL_1V5;
		// hemonel 2010-09-08: support 1.8V sensor IO
#if (_CHIP_ID_ & _RTS5840_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_3V30;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V79;
#elif (_CHIP_ID_ & _RTS5832_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_3V33;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V85;
#elif (_CHIP_ID_ & _RTS5829B_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_3V31;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V80;
#endif
	}

//	if(g_byFormatCfgExist==0) // format config not exist, use default setting.
	{
		InitFormatFrameFps();
	}


}



void	SetOV7740Sharpness(U8 bySetValue)
{
	if(bySetValue == 4)
	{
		// auto sharp mode
		Write_SenReg_Mask(0xCC, 0x00, 0x40);// auto mode, edge enhancement
	}
	else
	{
		// manual sharp mode
		Write_SenReg_Mask(0xCC, bySetValue|0x40, 0x5F);// manual mode, edge enhancement
	}
}

void SetOV7740Gain(float fGain)
{
}

void SetOV7740ImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.
{
	/***
		0x40 mirror
		0x80 flip
		0xc0 mirror and flip
	***/
	Write_SenReg_Mask(0x0C, (bySnrImgDir&0x03)<<6, 0xC0);
	Write_SenReg_Mask(0x16, bySnrImgDir&0x01, 0x03);	// hemonel 2010-01-27: fix mirror bug
}


void SetOV7740WindowCrop(U16 wXStart,U16 wXEnd,U16 wYStart,U16 wYEnd )
{
	// recalculate window
	wXEnd = wXEnd - wXStart; //H size
	wYEnd = wYEnd - wYStart; //V size
	wXStart += 0x94;	//0xa0
	wYStart += 0x06;	//0x0a

	// update window
	Write_SenReg(0x17, wXStart>>2); //H start_bit[9:2]
	Write_SenReg(0x18, wXEnd>>2); //H size_bit[9:2]
	Write_SenReg(0x19, wYStart>>1); //V start_bit[8:1]
	Write_SenReg(0x1A, wYEnd>>1); //V size_bit[8:1]

	//---V size[0], H size[1:0], V start[0], H start[1:0]---//
	Write_SenReg_Mask(0x16,((wYEnd&0x01)<<5)|((wXEnd&0x03)<<3)|((wYStart&0x01)<<2)|(wXStart&0x03), 0x3F);
}

void SetOV7740OutputDim(U16 wWidth,U16 wHeight)
{

	if((wWidth != 640)&&(wHeight != 480))
	{
		// need scale
		Write_SenReg_Mask(0x82, 0x0C, 0x0C);// enable scale H, scale V;
	}
	else
	{
		Write_SenReg_Mask(0x82, 0x00, 0x0C);// disable scale
	}

	Write_SenReg(0x31, wWidth>>2); // HoutSize[9:2]
	Write_SenReg(0x32, wHeight>>1); // VoutSize[8:1]
	Write_SenReg_Mask(0x34, ((wWidth&0x03)<<1)|(wHeight&0x01), 0x07); // HoutSize[1:0], VoutSize[0]

}


void SetOV7740PwrLineFreq(U8 byFPS, U8 byLightFrq)
{
	U8 byBdSel, byFltrEn;
	OV_BandingFilter_t BdFilter;

	BdFilter = OV_SetBandingFilter(byFPS);

	if (byLightFrq == PWR_LINE_FRQ_50)
	{
		byFltrEn = 0x20;
		byBdSel = 0x40;
	}
	else if (byLightFrq == PWR_LINE_FRQ_60)
	{
		byFltrEn = 0x20;
		byBdSel = 0x00;
	}
	else
	{
		//for auto de-banding mode
		byFltrEn = 0x20;
		byBdSel = 0x80;

		//for disable de-banding mode
		//byFltrEn = 0x00;
	}

	Write_SenReg_Mask(0xEC, byBdSel, 0xC0);
	Write_SenReg_Mask(0x13, byFltrEn, 0x20);

	//---set 50Hz banding filter---//
	Write_SenReg(0x50, INT2CHAR(BdFilter.wD50Base,0));//BD50 8LSB
	Write_SenReg_Mask(0x52, INT2CHAR(BdFilter.wD50Base,1)<<4, 0x30);//BD50 2MSB_bit[5:4]
	Write_SenReg_Mask(0x21, (BdFilter.byD50Step<<4), 0xF0);//BD50 step, bit[7:4]

	//---set 60Hz banding filter---//
	Write_SenReg(0x51, INT2CHAR(BdFilter.wD60Base,0));//BD60 8LSB
	Write_SenReg_Mask(0x52, INT2CHAR(BdFilter.wD60Base,1)<<6, 0xC0);//BD60 2MSB_bit[7:6]
	Write_SenReg_Mask(0x21, BdFilter.byD60Step, 0x0F);//BD60 step, bit[3:0]

}


void SetOV7740WBTemp(U16 wSetValue)
{
	OV_CTT_t ctt;

	ctt = OV_SetColorTemperature(wSetValue);

	Write_SenReg(0x02, ctt.wRgain);
	Write_SenReg(0x03, ctt.wGgain);
	Write_SenReg(0x01, ctt.wBgain);
}
/*
OV_CTT_t GetOV7740AwbGain(void)
{
	OV_CTT_t ctt;

	Read_SenReg(0x02, &ctt.wRgain);
	Read_SenReg(0x03, &ctt.wGgain);
	Read_SenReg(0x01, &ctt.wBgain);

	return ctt;
}
*/
void SetOV7740WBTempAuto(U8 bySetValue)
{
	if(bySetValue)
	{
		// atuo white balance
		Write_SenReg_Mask(0x13, 0x02,0x02);
	}
	else
	{
		// manual white balance
		Write_SenReg_Mask(0x13, 0x00,0x02);
	}
}

void SetOV7740BackLightComp(U8 bySetValue)
{
	U8 byAEW,byAEB;

	if(bySetValue)
	{
		byAEW = g_byOVAEW_BLC;
		byAEB = g_byOVAEB_BLC;
	}
	else
	{
		byAEW = g_byOVAEW_Normal;
		byAEB = g_byOVAEB_Normal;
	}
	Write_SenReg(0x24, byAEW);
	Write_SenReg(0x25, byAEB);
}

void SetOV7740IntegrationTimeAuto(U8 bySetValue)
{
	if(bySetValue == EXPOSURE_TIME_AUTO_MOD_APERTPRO)
	{
		// auto exposure
		Write_SenReg_Mask(0x15, 0x80, 0x80);	// enable night mode
		Write_SenReg_Mask(0x13, 0x05,  0x05);
	}
	else
	{
		// manual expsoure
		Write_SenReg_Mask(0x15, 0x00, 0x80);	// close night mode
		Write_SenReg_Mask(0x13, 0x00,  0x05);
	}
}

void SetOV7740IntegrationTime(U16 wEspline)
{
	U16 wDummyline = 0;
	U16 wExpMax = g_wAECExposureRowMax-2;

	if(wEspline > wExpMax)
	{
		wDummyline = wEspline - wExpMax;
		wEspline = wExpMax;
	}

	DBG_SENSOR(("\n wEspline value = %u\n", wEspline));
	DBG_SENSOR(("wDummyline value = %u\n", wDummyline));
	// write exposure time and dummy line to register
	Write_SenReg_Mask(0x04, (wEspline&0x03),0x03);
	Write_SenReg(0x10, INT2CHAR(wEspline,0));
	Write_SenReg(0x0F, INT2CHAR(wEspline,1));
	Write_SenReg(0x2D, INT2CHAR(wDummyline, 0));
	Write_SenReg(0x2E, INT2CHAR(wDummyline, 1));
}
#endif
