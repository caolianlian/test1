#ifndef _OV2720_H_
#define _OV2720_H_

#define OV2720_AEW	0x70// 0x78 ---jqg 20090422
#define OV2720_AEB	0x68// 0x68
#define OV2720_VPT	0xd4

#define _USE_2LANE_

void OV2720SetFormatFps(U16 SetFormat, U8 Fps);
void CfgOV2720ControlAttr(void);
void SetOV2720ImgDir(U8 bySnrImgDir);
void SetOV2720IntegrationTime(U16 wEspline);
void SetOV2720Exposuretime_Gain(float fExpTime, float fTotalGain);
void InitOV2720IspParams();
void OV2720_POR(void );
void InitOV2720IspParams(void );
void SetOV2720DynamicISP(U8 byAEC_Gain);
void SetOV2720DynamicISP_AWB(U16 wColorTempature);
void SetOV2720Gain(float fGain);
#endif // _

