#include "Inc.h"

#ifdef RTS58XX_SP_YACBAC1S

OV_CTT_t code gc_YACBAC1S_CTT[3] =
{
	{3000,0x3E,0x20,0x4D},
	{4050,0x43,0x20,0x40},
	{6500,0x55,0x20,0x2C},
};

//-----------20090731__JQG-------------
// YACBAA0S FPS calu: (664+Hblank)X(504+VBlank) /pclk = 1/FPS
//			for YUV: PCLK = MCLK / (2 X 2^CLKRC)
//			Vblank: 4
OV_FpsSetting_t code g_staYACBAC1SFpsSetting[]=
{
	//  FPS	extradummypixel clkrc	pclk
//	{1,		(1176+1),	(23),	  1000000},	// 1M
//	{3,		(523+1),	(11),	  2000000},	// 2M
//	{5, 		(0),		(11),	  2000000},	// 2M
//	{8,  		(196+1),	(5),		  4000000},	// 4M
	{9,  		(0x0288),	(1),		  6000000},
	{10,		(0x06a2),	(0),		12000000},
	{15,		(0x0390),	(0),		12000000},
	{20,		(0x0200),	(0),		12000000},
	{25,		(0x0118),	(0),		12000000},
	{30,		(0x007b),	(0),		12000000},// 12M
};



t_RegSettingBB code gc_YACBAC1S_Setting[] =
{
	{0x03, 0x00},
	{0x01, 0xf1},
	{0x01, 0xf3},
	{0x01, 0xf1},

	{0x03, 0x20},//page 3
	{0x10, 0x1c},//ae off
	{0x03, 0x22},//page 4
	{0x10, 0x6b},//awb off

//page 0
	{0x03, 0x00},
	{0x10, 0x00},
	{0x11, 0x90},
	{0x12, 0x00},
	{0x20, 0x00},
	{0x21, 0x06},
	{0x22, 0x00},
	{0x23, 0x06},
	{0x24, 0x01},
	{0x25, 0xe4},
	{0x26, 0x02},
	{0x27, 0x84},
	{0x40, 0x00},//	Hblank 336
	{0x41, 0x88},//
	{0x42, 0x00},//	Vsync 4
	{0x43, 0x04},//

////  BLC setting 090216 by steve
	{0x80, 0x3e}, //0x3e-->0x08 By Sam
	{0x81, 0x96},
	{0x82, 0x90},
	{0x83, 0x00},  //090414 ver 滲唳
	{0x84, 0x2c},  //00  090414 ver 滲唳


	{0x90, 0x0e},
	{0x91, 0x0f},
	{0x92, 0x3a},
	{0x93, 0x30},
	{0x94, 0x88},
	{0x95, 0x80},
	{0x98, 0x20},
//{0x90, 0x0e},
//{0x91, 0x08},
//{0x92, 0x60},
//{0x93, 0x10},
//{0x94, 0x88},	//add calvin 090410
//{0x95, 0x80},	//add calvin 090410
//{0x98, 0x20},

//Sam Change black level 2009.09.11
//{0xa0, 0x48},
//{0xa2, 0x48},
//{0xa4, 0x48},
//{0xa6, 0x48},
	{0xa0, 0x00},
	{0xa2, 0x00},
	{0xa4, 0x00},
	{0xa6, 0x00},
	{0xa8, 0x42},
	{0xaa, 0x42},
	{0xac, 0x42},
	{0xae, 0x42},

//page 02
	{0x03, 0x02},
	{0x10, 0x00},
	{0x13, 0x00},
	{0x18, 0x1C},
	{0x19, 0x00},
	{0x1a, 0x00}, //0x08-->0x00  by Sam
	{0x1b, 0x08},
	{0x1C, 0x00},
	{0x1D, 0x00},
	{0x20, 0x33},
	{0x21, 0xaa},
	{0x22,0xa6},
	{0x23,0xb0},
	{0x31,0x99},
	{0x32,0x00},
	{0x33,0x00},
	{0x34,0x3C},


//{0x23, 0x00},
//{0x31, 0x99},
//{0x32, 0x00},
//{0x33, 0x00},
//{0x34, 0x3C},
	{0x50, 0x21},
	{0x54, 0x30},
	{0x56, 0xFe},
	{0x60,0x64},
	{0x61,0x82},
	{0x62, 0x66},
	{0x63,0x7d},
	{0x64,0x66},
	{0x65,0x7d},
	{0x72,0x68},
	{0x73,0x7b},
	{0x74,0x68},
	{0x75,0x7b},
	{0x76,0x66},
	{0x77,0x70},
	{0x80,0x02},
	{0x81,0x5f},
	{0x82,0x09},
	{0x83,0x1b},
	{0x84,0x09},
	{0x85,0x1b},
	{0x86,0x09},
	{0x87,0x1b},
	{0x92,0x35},
	{0x93,0x47},
	{0x94,0x35},
	{0x95,0x47},
	{0x96,0x05},
	{0x97,0x0c},
	{0xa0,0x03},
	{0xa1,0x5d},
	{0xa4,0x5d},
	{0xa5,0x03},
	{0xa8,0x1d},
	{0xa9,0x2f},
	{0xaa,0x49},
	{0xab,0x5a},
	{0xac,0x10},
	{0xad,0x0e},
	{0xb0,0x01},
	{0xb1,0x0e},
	{0xb2,0x03},
	{0xb3,0x0c},
	{0xb4,0x05},
	{0xb5,0x0a},
	{0xb8,0x65},
	{0xb9,0x72},
	{0xba,0x65},
	{0xbb,0x72},
	{0xbc,0x04},
	{0xbd,0x10},
	{0xbe,0x04},
	{0xbf,0x10},
	{0xc0,0x63},
	{0xc1,0x84},

//page 10
	{0x03, 0x10},
	{0x10, 0x03},//ISPCTL1
	{0x12, 0x30},//Y offet, dy offseet OFF by Leemaster
	{0x40, 0x01},//01->08->01
	{0x41, 0x10},//08->10
	{0x50, 0x78},// 90 --> 50

	{0x60,0x00},
	{0x61,0x90},
	{0x62,0x90},
	{0x63,0x60},
	{0x64,0x80},

//page 11

	{0x03,0x11},
	{0x10,0x99},
	{0x11,0x0e},

	{0x20, 0x44}, //44(Low lux more blur)

	{0x21,0x29},
	{0x50,0x05},
	{0x60,0x0f},
	{0x62,0x43},
	{0x63,0x63},
	{0x74,0x08},
	{0x75,0x08},


//page 12
	{0x03,0x12},
	{0x40,0x23},
	{0x41,0x3b},
	{0x50,0x05},
	{0x70,0x1d},
	{0x74,0x05},
	{0x75,0x04},
	{0x77,0x20},
	{0x78,0x10},
	{0x91,0x34},
	{0xb0,0xc9},
	{0xd0,0xb1},

//page 13
	{0x03,0x13},
	{0x10,0x3b},
	{0x11,0x03},
	{0x12,0x00},
	{0x13,0x02},
	{0x14,0x00},
	{0x20,0x03},
	{0x21,0x01},
	{0x23,0x3d},
	{0x24,0x01},
	{0x25,0x4a},
	{0x28,0x00},
	{0x29,0x78},
	{0x30,0xff},
	{0x80,0x0d},
	{0x81,0x13},
	{0x83,0x5d},
	{0x90,0x04},
	{0x91,0x03},
	{0x93,0x3d},
	{0x94,0x0f},
	{0x95,0x8f},

//page 14


// 1113
//{0x03,  0x14},   //page 14

	{0x03, 0x14},  //page 14 Lens shading
	{0x10, 0x01},

//1120
	{0x20,  0x95},   //xcen
	{0x21,  0xa2},   //ycen
	{0x22,  0x76},
	{0x23,  0x66},
	{0x24,  0x66},

/// 1113-66%
//page 15
	{0x03,0x15},
	{0x10,0x03},
	{0x14,0x3c},
	{0x16,0x2c},
	{0x17,0x2f},
	{0x30,0xcb},
	{0x31,0x61},
	{0x32,0x16},
	{0x33,0x30},
	{0x34,0xD0},
	{0x35,0x2b},
	{0x36,0x01},
	{0x37,0x3F},
	{0x38,0x85},
	{0x40,0x87},
	{0x41,0x18},
	{0x42,0x91},
	{0x43,0x94},
	{0x44,0x9f},
	{0x45,0x33},
	{0x46,0x00},
	{0x47,0x94},
	{0x48,0x14},

	{0x80, 0x00},
	{0x84, 0x80},
	{0x85, 0x80},
	{0x86, 0x80},




//page 16
//{0x03, 0x16},
//{0x30, 0x00},
//{0x31, 0x13},
//{0x32, 0x1e},
//{0x33, 0x31},
//{0x34, 0x51},
//{0x35, 0x6c},
//{0x36, 0x84},
//{0x37, 0x99},
//{0x38, 0xab},
//{0x39, 0xbb},
//{0x3a, 0xc9},
//{0x3b, 0xde},
//{0x3c, 0xed},
//{0x3d, 0xf8},
//{0x3e, 0xff},


	{0x03,0x16},
	{0x30,0x00},
	{0x31,0x0b},
	{0x32,0x18},
	{0x33,0x35},
	{0x34,0x5d},
	{0x35,0x7a},
	{0x36,0x93},
	{0x37,0xa7},
	{0x38,0xb8},
	{0x39,0xc6},
	{0x3a,0xd2},
	{0x3b,0xe4},
	{0x3c,0xf1},
	{0x3d,0xf9},
	{0x3e,0xff},

	{0x03,0x17},
	{0xC4,0x4B},
	{0xC5,0x3E},
	{0x03,0x20},
	{0x10,0x0c},
	{0x11,0x00},
	{0x20,0x01},
	{0x28,0x3f},
	{0x29,0xa3},
	{0x2a,0xf0},
	{0x2b,0xf4},
	{0x30,0xf8},
	{0x60,0x99},
	{0x70,0x38},
	{0x78,0x23},
	{0x79,0x13},
	{0x7A,0x24},
	{0x83,0x00},
	{0x84,0xc3},
	{0x85,0x50},
	{0x86,0x01},
	{0x87,0xf4},
	{0x88,0x02},
	{0x89,0x49},
	{0x8a,0xf0},
	{0x8b,0x3A},
	{0x8c,0x98},
	{0x8d,0x30},
	{0x8e,0xD4},
	{0x8f,0xc4},
	{0x90,0x68},
	{0x91,0x02},
	{0x92,0xda},
	{0x93,0x77},
	{0x98,0x8C},
	{0x99,0x23},
	{0x9c,0x05},
	{0x9d,0xdc},
	{0x9e,0x00},
	{0x9f,0xfa},
	{0xb0,0x14},
	{0xb1,0x14},
	{0xb2,0x80},
	{0xb3,0x14},
	{0xb4,0x14},
	{0xb5,0x38},
	{0xb6,0x26},
	{0xb7,0x20},
	{0xb8,0x1d},
	{0xb9,0x1b},
	{0xba,0x1a},
	{0xbb,0x19},
	{0xbc,0x19},
	{0xbd,0x18},
	{0xc0,0x14},
	{0xc8,0x90},
	{0xc9,0x80},

//pag 20
	{0x03, 0x20},
	{0x10, 0x0c},
	{0x11, 0x00},
	{0x20, 0x01},
	{0x28, 0x3f},   // modify by Ban 1006
	{0x29, 0xa3},
	{0x2a, 0xf0},
	{0x2b, 0xf4},//	1/120 Anti banding
	{0x30, 0x78}, //0xf8->0x78 1/120 Anti banding
	{0x60, 0xCF},//	//weight  80->6a-->00 By Sam
	{0x70, 0x44},//  38-> 44 By Sam
	{0x78, 0x23},//	//yth1
	{0x79, 0x22},//	20-->22 By Sam
	{0x7A, 0x24},//
	{0x83, 0x00},	//30fps
	{0x84, 0xc3},
	{0x85, 0x50},
	{0x86, 0x00},	// min
	{0x87, 0xfa},
	{0x88, 0x03}, //ExpMax 5fps
	{0x89, 0x0D},
	{0x8a, 0x40},
	{0x8b, 0x3A},	//Exp100Hz
	{0x8c, 0x98},
	{0x8d, 0x30},	//Exp120Hz
	{0x8e, 0xD4},
	{0x8f, 0xc4},
	{0x90, 0x68},
	{0x91, 0x02},	//fixed
	{0x92, 0xda},
	{0x93, 0x77},
	{0x98, 0x8C}, //outdoor th1
	{0x99, 0x23},	//outdoor th2
	{0x9c, 0x05},// 0c-->05 by sam
	{0x9d, 0x78},// 35-->78 By Sam
	{0x9e, 0x00},//
	{0x9f, 0xc8},//fa-->c8 By Sam
	{0xb0, 0x10},//18-->10 By Sam
	{0xb1, 0x10},//14-->10 By Sam
	{0xb2, 0x80},//
	{0xb3, 0x10},//18-->10 By Sam
	{0xb4, 0x10},//18-->10 By Sam
	{0xb5, 0x30},//40-->30 By Sam
	{0xb6, 0x20},
	{0xb7, 0x1b},//25-->1b By Sam
	{0xb8, 0x18},//22-->18 By Sam
	{0xb9, 0x16},//20-->16 By Sam
	{0xba, 0x15},
	{0xbb, 0x15},
	{0xbc, 0x14},
	{0xbd, 0x14},
	{0xc0, 0x0c},	//skygain
//{0xc3, 0x5F},//DPC ON
//{0xc4, 0x58},//DPC OFF
	{0xc8, 0x90},
	{0xc9, 0x80},

	{0x03, 0x22},
	{0x10, 0xF2},
	{0x11, 0x26},
	{0x21, 0x40},
	{0x30, 0x80},
	{0x31, 0x80},
	{0x38, 0x11},
	{0x39, 0x33},
	{0x40, 0xf0},
	{0x41, 0x33},
	{0x42, 0x33},
	{0x43, 0xf3},
	{0x44, 0x55},
	{0x45, 0x44},
	{0x46, 0x02},
//{0x80, 0x18},
//{0x81, 0x20},
//{0x82, 0x6B},
	{0x80, 0x43}, // change for init AWB blueish_
	{0x81, 0x20},
	{0x82, 0x36},

	{0x83, 0x66},
	{0x84, 0x18},
	{0x85, 0x6B},
	{0x86, 0x20},
	{0x87, 0x4a},
	{0x88, 0x3f},
	{0x89, 0x33},
	{0x8a, 0x28},
	{0x8b, 0x05},
	{0x8d, 0x25},
	{0x8e, 0x31},
	{0x8f, 0x63},// 10
	{0x90, 0x63},// 18
	{0x91, 0x5d},// 20
	{0x92, 0x5a},// 28
	{0x93, 0x49},// 30
	{0x94, 0x47},// 38
	{0x95, 0x43},// 40
	{0x96, 0x3c},// 48
	{0x97, 0x28},// 50
	{0x98, 0x27},//d},// 58
	{0x99, 0x25},//28},// 60
	{0x9a, 0x23},//26},// 68
	{0x9b, 0x08},
//{0xb4, 0xea},
	{0x03, 0x22},
	{0x10, 0xe2},

	{0x03, 0x20},
	{0x10, 0x8c},

	{0x01, 0xd0},
};



t_RegSettingBB code gc_YACBAC1S_Scale_Setting[] =
{

	{0x03, 0x13}, //Page mode 13
	{0x83, 0x5c}, //disable edge 2nd
	{0x80, 0x0a}, //disable edge 2nd

	{0x03, 0x18}, //Page mode 18
	{0x10, 0x05}, //enable scaling
	{0x20, 0x01},
	{0x21, 0x80},
	{0x22, 0x01},
	{0x23, 0x20},
	{0x2c, 0x0d},
	{0x2d, 0x55},
	{0x2e, 0x0d},
	{0x2f, 0xff},
	{0x30, 0x4e},

};

t_RegSettingBB code gc_YACBAC1S_NonScale_Setting[] =
{
	{0x03, 0x13},//Page mode 13
	{0x83, 0x5d},
	{0x03, 0x18},//Page mode 18
	{0x10, 0x00},
};

static OV_FpsSetting_t*  GetHynixFpsSetting(U8 Fps, OV_FpsSetting_t staHynixFpsSetting[], U8 byArrayLen)
{
	U8 i;
	U8 Idx;

	if (g_bIsHighSpeed)
	{
		Idx=2; //8fps for uxga, 10 fps for vga
	}
	else
	{
		Idx = 0;
	}

	for(i=0; i< byArrayLen; i++)
	{
		if(staHynixFpsSetting[i].byFps ==Fps)
		{
			Idx=i;
			break;
		}
	}
	return &staHynixFpsSetting[Idx];
}

//this function is also called during Probe stage to calculate the transfer_size/LWM/HWM
void GetSensorPclkHsync(U16 wSensorSPFormat,U8 byFps)
{
	OV_FpsSetting_t *pSensor_FpsSetting;

	wSensorSPFormat = wSensorSPFormat; // for delete warning

	pSensor_FpsSetting=GetHynixFpsSetting(byFps, g_staYACBAC1SFpsSetting, sizeof(g_staYACBAC1SFpsSetting)/sizeof(OV_FpsSetting_t));
	g_wSensorHsyncWidth = pSensor_FpsSetting->wExtraDummyPixel + 640; //1361; // this for manual exposure
	g_dwPclk= pSensor_FpsSetting->dwPixelClk;
}

void YACBAC1SSetFormatFps(U8 SetFormat, U8 byFps)
{
	OV_FpsSetting_t *pSensor_FpsSetting;

	SetFormat = SetFormat;

	pSensor_FpsSetting=GetHynixFpsSetting(byFps, g_staYACBAC1SFpsSetting, sizeof(g_staYACBAC1SFpsSetting)/sizeof(OV_FpsSetting_t));

	WriteSensorSettingBB(sizeof(gc_YACBAC1S_Setting)>>1, gc_YACBAC1S_Setting);//sensor common set

	Write_SenReg(0x03, 0x00);//page 0
	Write_SenReg(0x11, 0x90);//90->variation,    94->fix
	Write_SenReg(0x12, pSensor_FpsSetting->byClkrc); // write CLKRC

	Write_SenReg(0x40, INT2CHAR(pSensor_FpsSetting->wExtraDummyPixel,1)); //hsync_high byte.
	Write_SenReg(0x41, INT2CHAR(pSensor_FpsSetting->wExtraDummyPixel,0)); //hsync_low byte
	Write_SenReg(0x42, 0x00);//vsync_high byte
	Write_SenReg(0x43, 0x04);//Vsync_low byte

	//-------add for anti-flicker under low freq PCLK
	if(byFps < 10)//Fps < 10, PCLK = 6MHz, new_step= step/4
	{
		Write_SenReg(0x03, 0x20);//page 20
		Write_SenReg(0x8b, 0x1d);//0x0e);//50Hz
		Write_SenReg(0x8c, 0x4c);//0xa6);
		Write_SenReg(0x8d, 0x18);//0x0c);//60Hz
		Write_SenReg(0x8e, 0x6a);//0x35);
	}
	else //Fps > = 10, PCLK = 24MHz
	{
		Write_SenReg(0x03, 0x20);//page 20
		Write_SenReg(0x8b, 0x3a);//50Hz
		Write_SenReg(0x8c, 0x98);
		Write_SenReg(0x8d, 0x30);//60Hz
		Write_SenReg(0x8e, 0xd4);

	}
	//-------

	//g_wAECExposureRowMax = 504; //not use this param
	//g_wSensorHsyncWidth = 664+pSensor_FpsSetting->wExtraDummyPixel;
	//g_dwPclk= pSensor_FpsSetting->dwPixelClk;
	GetSensorPclkHsync(VGA_FRM,byFps);	
}


void CfgYACBAC1SControlAttr(void)
{
	g_bySensorSize = SENSOR_SIZE_VGA;
	g_wSensorSPFormat = VGA_FRM;

	{
		memcpy(g_asOvCTT,gc_YACBAC1S_CTT,sizeof(gc_YACBAC1S_CTT));
	}

	{
		// AE backlight compensation parameter
		g_byOVAEW_Normal = YACBAC1S_AE_TARGET;
		g_byOVAEB_Normal = YACBAC1S_AE_TARGET;

		g_byOVAEW_BLC = g_byOVAEW_Normal + 0x20;
		g_byOVAEB_BLC = g_byOVAEB_Normal + 0x20;
	}

	{
		//g_bySV28VoltSel =  SV28_VOL_2V9;
		//	XBYTE[REG_TUNESVIO_SVA] =  SV28_VOL_2V9|SV18_VOL_1V8;
		// hemonel 2010-09-08: support 1.8V sensor IO
#if (_CHIP_ID_ & _RTS5840_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V79;
#elif (_CHIP_ID_ & _RTS5832_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V85;
#elif (_CHIP_ID_ & _RTS5829B_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V82;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V80;
#endif
	}

	{
		InitFormatFrameFps();
	}


}


#ifndef _USE_BK_HSBC_ADJ_
void SetYACBAC1SBrightness(S16 swSetValue)
{

	return;
}


void	SetYACBAC1SContrast(U16 wSetValue)
{

	return;
}

void	SetYACBAC1SSaturation(U16 wSetValue)
{

	return;
}

void	SetYACBAC1SHue(S16 swSetValue)
{

	return;
}
#endif

void	SetYACBAC1SSharpness(U8 bySetValue)
{
	U8 bySharpLowerLimit;//, bySharpUpperLimit;

	switch (bySetValue)
	{
	case 0:
		bySharpLowerLimit = 0x00;
		//bySharpUpperLimit = 0x00;
		break;
	case 1:
		bySharpLowerLimit = 0x03;
		//bySharpUpperLimit = 0x03;
		break;
	case 2:
		bySharpLowerLimit = 0x05;
		//bySharpUpperLimit = 0x05;
		break;
	case 3:
		bySharpLowerLimit = 0x07;
		//bySharpUpperLimit = 0x07;
		break;
	case 4:
		bySharpLowerLimit = 0x10;
		//bySharpUpperLimit = 0x10;
		break;
	case 5:
		bySharpLowerLimit = 0x18;
		//bySharpUpperLimit = 0x18;
		break;
	case 6:
		bySharpLowerLimit = 0x20;
		//bySharpUpperLimit = 0x20;
		break;
	default:
		bySharpLowerLimit = 0x28;
		//bySharpUpperLimit = 0x28;
		break;
	}

	//only set 0x90 2nd edgement, don't set 0x20 1th edgement(noise too much)
	Write_SenReg(0x03, 0x13);
	Write_SenReg_Mask(0x90, bySharpLowerLimit, 0x3F);
	Write_SenReg_Mask(0x91, bySharpLowerLimit, 0x3F);

}

void SetYACBAC1SGain(float fGain)
{
}

void SetYACBAC1SImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.
{
	/***0x00 normal
		0x01 mirror
		0x02 flip
		0x03 mirror and flip
	***/
	Write_SenReg(0x03, 0x00);
	Write_SenReg_Mask(0x11, bySnrImgDir, 0x03);
	Write_SenReg(0x03, 0x02);
	Write_SenReg_Mask(0x1a, ~((bySnrImgDir &0x01)<<4), 0x10);
}


void SetYACBAC1SOutputDim(U16 wWidth,U16 wHeight)
{
	U8 byVmode;
	U16 byBkWinWidth = 0;
	wWidth = wWidth;

	switch (wHeight)
	{
	case 120:
		byVmode = 0x20;
		WriteSensorSettingBB(sizeof(gc_YACBAC1S_NonScale_Setting)>>1, gc_YACBAC1S_NonScale_Setting);
		break;
	case 144:
		byVmode = 0x10;
		//Write_SenReg(0x03, 0x00); //Page mode 0 //____use window___
		//Write_SenReg(0x21, ((480-288)/2 + 5)); // start x
		//Write_SenReg(0x23, ((640-352)/2 + 7)); // start y
		//Write_SenReg(0x24, 0x01); //height
		//Write_SenReg(0x25, 0x20);
		//Write_SenReg(0x26, 0x01); //width
		//Write_SenReg(0x27, 0x60);

		byBkWinWidth = (376-352)/2; //_______use scaling,then crop_____
		WriteSensorSettingBB(sizeof(gc_YACBAC1S_Scale_Setting)>>1, gc_YACBAC1S_Scale_Setting);
		break;
	case 240:
		byVmode = 0x10;
		WriteSensorSettingBB(sizeof(gc_YACBAC1S_NonScale_Setting)>>1, gc_YACBAC1S_NonScale_Setting);
		break;
	case 288:
		byVmode = 0x00;
		//Write_SenReg(0x03, 0x00); //Page mode 0//____use window___
		//Write_SenReg(0x21, ((480-288)/2 + 5)); // start x
		//Write_SenReg(0x23, ((640-352)/2 + 7)); // start y
		//Write_SenReg(0x24, 0x01); //height
		//Write_SenReg(0x25, 0x20);
		//Write_SenReg(0x26, 0x01); //width
		//Write_SenReg(0x27, 0x60);

		byBkWinWidth = (376-352);//_______use scaling,then crop_____
		WriteSensorSettingBB(sizeof(gc_YACBAC1S_Scale_Setting)>>1, gc_YACBAC1S_Scale_Setting);
		break;
	case 480:
	default:
		byVmode = 0x00;
		WriteSensorSettingBB(sizeof(gc_YACBAC1S_NonScale_Setting)>>1, gc_YACBAC1S_NonScale_Setting);
		break;
	}

	Write_SenReg(0x03, 0x00); //Page mode 0
	Write_SenReg_Mask(0x10, byVmode, 0x30);
//	XBYTE[CCS_HSYNC_TCTL0] = byBkWinWidth;
	SetBkWindowStart(byBkWinWidth, 0);
}


void SetYACBAC1SPwrLineFreq(U8 byFPS, U8 byLightFrq)
{
	U8 byPwrSel;
	byFPS = byFPS;

	if (PWR_LINE_FRQ_50==byLightFrq)
	{
		byPwrSel = 0x9c;
		Write_SenReg(0x03, 0x20);
		Write_SenReg(0x84, 0xAF);
		Write_SenReg(0x85, 0xC8);
		Write_SenReg(0x86, 0x01);
		Write_SenReg(0x87, 0xF4);
	}
	else if (PWR_LINE_FRQ_60==byLightFrq)
	{
		byPwrSel = 0x8c;
		Write_SenReg(0x03, 0x20);
		Write_SenReg(0x84, 0xC3);
		Write_SenReg(0x85, 0x50);
		Write_SenReg(0x86, 0x00);
		Write_SenReg(0x87, 0xC8);//0xFA);
	}
	else //PWR_LINE_FRQ_DIS --- auto de-banding
	{
		byPwrSel = 0xcc;//0xcc;
		Write_SenReg(0x03, 0x20);
		Write_SenReg(0x84, 0xC3);
		Write_SenReg(0x85, 0x50);
		Write_SenReg(0x86, 0x00);
		Write_SenReg(0x87, 0xC8);
	}
	Write_SenReg(0x03, 0x20); //Page mode 20
	Write_SenReg_Mask(0x10, 0x00, 0x0f); // low nibble must set 0 first
	Write_SenReg(0x10, byPwrSel);
}


void SetYACBAC1SWBTemp(U16 wRgain, U16 wGgain, U16 wBgain)
{
//	OV_CTT_t ctt;

//	ctt = OV_SetColorTemperature(wSetValue);

	Write_SenReg(0x03, 0x22);
	Write_SenReg(0x80, wRgain); // 趨 勢 對，但 單位 色溫 不對.  R'(6bit) = 1/2R(RT)
	Write_SenReg(0x81, wGgain);
	Write_SenReg(0x82, wBgain);// color temperature range map
}

/*
OV_CTT_t GetYACBAC1SAwbGain(void)
{
	OV_CTT_t ctt;

	Write_SenReg(0x03, 0x22);//page 22
	Read_SenReg(0x80, &ctt.wRgain);
	Read_SenReg(0x81, &ctt.wGgain);
	Read_SenReg(0x82, &ctt.wBgain);

	return ctt;
}
*/
void SetYACBAC1SWBTempAuto(U8 bySetValue)
{
	if(bySetValue)
	{
		// atuo white balance
		Write_SenReg(0x03, 0x22);
		Write_SenReg_Mask(0x10, 0x80,0x80);
		Write_SenReg_Mask(0x11, 0x06,0x07);
	}
	else
	{
		// manual white balance
		Write_SenReg(0x03, 0x22);
		Write_SenReg_Mask(0x10, 0x00,0x80);
		Write_SenReg_Mask(0x11, 0x00,0x07);
	}
}


void SetYACBAC1SBackLightComp(U8 bySetValue)
{
	U8 byAEtarg;

//	if(bySetValue==2)
	{
//		byAEtarg = 0x84;
	}
//	else if(bySetValue==1)
	if(bySetValue)
	{
		byAEtarg = 0x5D;
	}
	else
	{
		byAEtarg = 0x42;
	}
	Write_SenReg(0x03, 0x20);
	Write_SenReg(0x70, byAEtarg);

}

U16 GetYACBAC1SAEGain()
{
	U16 wAG;
	Write_SenReg(0x03, 0x20);
	Read_SenReg(0xb0, &wAG);
	return wAG;
}

void SetYACBAC1SIntegrationTimeAuto(U8 bySetValue)
{
	U16 wTmp;

	Write_SenReg(0x03, 0x20);	// write page address

	if(bySetValue == EXPOSURE_TIME_AUTO_MOD_APERTPRO)
	{
		// auto exposure
		Read_SenReg(0x10, &wTmp);
		Write_SenReg_Mask(0x10, 0x00,  0x8F);
		Write_SenReg_Mask(0x10, (0x80|(U8)wTmp), 0x8f); // for Hynix AE bug_20091015

		//Write_SenReg_Mask(0x10, 0x80,  0x80);
	}
	else
	{
		// manual expsoure
		Write_SenReg_Mask(0x10, 0x00,  0x80);
	}
}

void SetYACBAC1SIntegrationTime(U32 dwSetValue)
{
	U32 dwExpTime;

	if (dwSetValue < 200)  // for Hynix AE bug_20091015
	{
		dwSetValue = 200;
	}
	else if (dwSetValue > 8000)
	{
		dwSetValue = 8000;
	}

	//dwExpTime  =  (dwSetValue*100000/8) * (g_dwPclk/1000000000); //unit: ns; dwsetvalue unit: 100us
	dwExpTime  =  (dwSetValue * (g_dwPclk/10000)) /8;

	Write_SenReg(0x03, 0x20);	// write page address

	// write exposure time to register
	Write_SenReg(0x83, LONG2CHAR(dwExpTime, 2));
	Write_SenReg(0x84, LONG2CHAR(dwExpTime, 1));
	Write_SenReg(0x85, LONG2CHAR(dwExpTime, 0));
}
#endif

