/*
********************************************************************************
*                                      				Realtek's 5828 Project
*
*											Layer:
*										Module:
*								(c) Copyright 2011-2012, Realtek Corp.
*                                           			All Rights Reserved
*
*                                                  			V0.01
*
* File : RS0509.C
********************************************************************************
*/

/*
********************************************************************************
 \file		RS0509.c
 \brief

GENERAL DESCRIPTION:

EXTERNALIZED FUNCTIONS:

Copyright (c) 2011-2012 by Realtek Incorporated.  All Rights Reserved.

 \version 0.01
 \date    2011-2012

********************************************************************************
*/

/*
================================================================================

                        EDIT HISTORY FOR MODULE

when        	who		Ver		what, where, why
---------- 	-----   	-----	--------------------------------------------------------------
2011-11-30	Jimmy	0.01		Init version

================================================================================
*/
#include "Inc.h"

#ifdef RTS58XX_SP_RS0509
//Now only pixclk <45mhz can output 2 lane mipi setting...because RS0509 not autohalve mipiclk at 2 lane
//Jimmy.these macro only open one at a time
//#define RS0509_SIZE_QXGA
//#define RS0509_SIZE_FHD
//#define RS0509_SIZE_UXGA
#define RS0509_SIZE_HD
//#define RS0509_SIZE_SXGA

OV_CTT_t code gc_RS0509_CTT[3] =
{
	{3000,0x0100,0x0100,0x0100},
	{4150,0x0100,0x0100,0x0100},
	{6500,0x0100,0x0100,0x0100},
};


#ifdef RS0509_SIZE_QXGA
code OV_FpsSetting_t  g_staRS0509QXGAFpsSetting[]=
{
	// FPS 	ExtraDummyPixel 		clkrc 	pclk
	{3,		(2048+8824),			(DSI_CMU_POSTDIV_2),		 50250000},
	{5,		(2048+4476),			(DSI_CMU_POSTDIV_2),		 50250000},
	{8,		(2048+2030),			(DSI_CMU_POSTDIV_2), 		 50250000},
	{9,		(2048+1576),			(DSI_CMU_POSTDIV_2), 		 50250000},
	{10,		(2048+1214),			(DSI_CMU_POSTDIV_2), 		 50250000},
	{15,		(2048+126),			(DSI_CMU_POSTDIV_2), 		 50250000},
	{20,		(2048+1214),			(DSI_CMU_POSTDIV_0), 		100500000},
	{25,		(2048+562),			(DSI_CMU_POSTDIV_0),		100500000},
	{30,		(2048+126),			(DSI_CMU_POSTDIV_0),		100500000},
};

code OV_FpsSetting_t  g_staRS0509HDFpsSetting[]=
{
	// FPS 	ExtraDummyPixel 		clkrc 	pclk
	{5,		(1280+4768),			(DSI_CMU_POSTDIV_2),		22500000},
	{8,		(1280+2500),			(DSI_CMU_POSTDIV_2), 		22500000},
	{9,		(1280+2080),			(DSI_CMU_POSTDIV_2), 		22500000},
	{10,		(1280+1744),			(DSI_CMU_POSTDIV_2), 		22500000},
	{15,		(1280+736),			(DSI_CMU_POSTDIV_2), 		22500000},
	{20,		(1280+1744),			(DSI_CMU_POSTDIV_0), 		45000000},
	{25,		(1280+1138),			(DSI_CMU_POSTDIV_0),		45000000},
	{30,		(1280+616),			(DSI_CMU_POSTDIV_0),		45000000},
};
#endif

#ifdef RS0509_SIZE_FHD
code OV_FpsSetting_t  g_staRS0509FHDFpsSetting[]=
{
	// FPS 	ExtraDummyPixel 		clkrc 	pclk
	{5,		(1920+4600),			(DSI_CMU_POSTDIV_2),		40500000},
	{8,		(1920+2664),			(DSI_CMU_POSTDIV_2), 		40500000},
	{9,		(1920+2156),			(DSI_CMU_POSTDIV_2), 		40500000},
	{10,		(1920+1748),			(DSI_CMU_POSTDIV_2), 		40500000},
	{15,		(1920+524),			(DSI_CMU_POSTDIV_2), 		40500000},
	{20,		(1920+1748),			(DSI_CMU_POSTDIV_0), 		81000000},
	{25,		(1920+1014),			(DSI_CMU_POSTDIV_0),		81000000},
	{30,		(1920+524),			(DSI_CMU_POSTDIV_0),		81000000},
};
#endif

#ifdef RS0509_SIZE_HD
code OV_FpsSetting_t  g_staRS0509HDFpsSetting[]=
{
	// FPS 	ExtraDummyPixel 		clkrc 	pclk
	{5,		(1280+4768),			(DSI_CMU_POSTDIV_2),		22500000},
	{8,		(1280+2500),			(DSI_CMU_POSTDIV_2), 		22500000},
	{9,		(1280+2080),			(DSI_CMU_POSTDIV_2), 		22500000},
	{10,		(1280+1744),			(DSI_CMU_POSTDIV_2), 		22500000},
	{15,		(1280+736),			(DSI_CMU_POSTDIV_2), 		22500000},
	{20,		(1280+1744),			(DSI_CMU_POSTDIV_0), 		45000000},
	{25,		(1280+1138),			(DSI_CMU_POSTDIV_0),		45000000},
	{30,		(1280+616),			(DSI_CMU_POSTDIV_0),		45000000},
};
#endif

#ifdef RS0509_SIZE_UXGA
code OV_FpsSetting_t  g_staRS0509UXGAFpsSetting[]=
{
	// FPS 	ExtraDummyPixel 		clkrc 	pclk
	{5,		(1600+4752),			(DSI_CMU_POSTDIV_2),		39000000},
	{8,		(1600+2382),			(DSI_CMU_POSTDIV_2), 		39000000},
	{9,		(1600+1940),			(DSI_CMU_POSTDIV_2), 		39000000},
	{10,		(1600+1586),			(DSI_CMU_POSTDIV_2), 		39000000},
	{15,		(1600+524),			(DSI_CMU_POSTDIV_2), 		39000000},
	{20,		(1600+1586),			(DSI_CMU_POSTDIV_0), 		78000000},
	{25,		(1600+948),			(DSI_CMU_POSTDIV_0),		78000000},
	{30,		(1600+524),			(DSI_CMU_POSTDIV_0),		78000000},
};
#endif

#ifdef RS0509_SIZE_SXGA
code OV_FpsSetting_t  g_staRS0509SXGAFpsSetting[]=
{
	// FPS 	ExtraDummyPixel 		clkrc 	pclk
	{5,		(1280+4158),			(DSI_CMU_POSTDIV_2),		28500000},
	{8,		(1280+2118),			(DSI_CMU_POSTDIV_2), 		28500000},
	{9,		(1280+1740),			(DSI_CMU_POSTDIV_2), 		28500000},
	{10,		(1280+1438),			(DSI_CMU_POSTDIV_2), 		28500000},
	{15,		(1280+532),			(DSI_CMU_POSTDIV_2), 		28500000},
	{20,		(1280+1438),			(DSI_CMU_POSTDIV_0), 		57000000},
	{25,		(1280+894),			(DSI_CMU_POSTDIV_0),		57000000},
	{30,		(1280+532),			(DSI_CMU_POSTDIV_0),		57000000},
};
#endif

static OV_FpsSetting_t*  GetOvFpsSetting(U8 Fps, OV_FpsSetting_t staOvFpsSetting[], U8 byArrayLen)
{
	U8 i;
	U8 Idx;

	ISP_MSG((" array size = %bd\n", byArrayLen));

	if(g_bIsHighSpeed)
	{
		Idx=2; //8fps for uxga, 10 fps for vga
	}
	else
	{
		Idx = 0;
	}

	for(i=0; i< byArrayLen; i++)
	{
		ISP_MSG((" fps i = %bd\n",i));
		if(staOvFpsSetting[i].byFps ==Fps)
		{
			Idx=i;
			break;
		}
	}
	ISP_MSG((" get fps idx = %bd\n",Idx));
	return &staOvFpsSetting[Idx];
}

void InitRS0509IspParams()
{
	// AWB initial gain
	//use D50 R/G/B gain as AWB initial value
	g_wAWBRGain_Last= 0x17C;
	g_wAWBGGain_Last= 0x100;
	g_wAWBBGain_Last= 0x226;
	g_wProjectGR = g_wAWBRGain_Last;
	g_wProjectGB = g_wAWBBGain_Last;

	// Special ISP
	g_bySaturation_Def =64;
	g_byContrast_Def = 32; //Neil Tuning at chicony

	g_wDynamicISPEn = DYNAMIC_LSC_EN | DYNAMIC_SHARPPARAM_EN | DYNAMIC_SHARPNESS_EN 
		| DYNAMIC_GAMMA_EN | DYNAMIC_CCM_CT_EN;
}

/*
********************************************************************************
*										Power On RS0509 TX
* FUNCTION PowerOnRS0509TX
********************************************************************************
*/
/**
  Init RLE0509 Power, and power on.

  \param none

  \retval none.
********************************************************************************
*/
static U8 PowerOnRS0509TX(void )
{
	//BandGap On volt=1.218mV
	Write_SenReg(BANDGAP_CTRL, BANDGAP_VOLT_1218|RT_POW1_EN);
	uDelay(10);
	//Tx PLL On, post-div disable
	Write_SenReg(DSI_POWER_CTRL0, DSI_CMU_SELLDO|DSI_CMU_PWDB_EN|DSI_CMU_LDO_EN);
	uDelay(10);
	//Turn 50ohm adjust current
	Write_SenReg_Mask(BANDGAP_CTRL, PWDB_K_EN, PWDB_K_EN);
	//Tx other analog power on,except PLL
	Write_SenReg(DSI_POWER_CTRL1, DSI_POW_EN);
	uDelay(10);

	return TRUE;
}

void GetSensorPclkHsync(U16 wSensorSPFormat, U8 byFps)
{
	OV_FpsSetting_t *pRS0509_FpsSetting;
	wSensorSPFormat = wSensorSPFormat;
#ifdef RS0509_SIZE_QXGA
	pRS0509_FpsSetting = GetOvFpsSetting(byFps, g_staRS0509QXGAFpsSetting, \
	                                     sizeof(g_staRS0509QXGAFpsSetting)/sizeof(OV_FpsSetting_t));
#endif
#ifdef RS0509_SIZE_UXGA
	pRS0509_FpsSetting = GetOvFpsSetting(byFps, g_staRS0509UXGAFpsSetting, \
	                                     sizeof(g_staRS0509UXGAFpsSetting)/sizeof(OV_FpsSetting_t));
#endif
#ifdef RS0509_SIZE_FHD
	pRS0509_FpsSetting = GetOvFpsSetting(byFps, g_staRS0509FHDFpsSetting, \
	                                     sizeof(g_staRS0509FHDFpsSetting)/sizeof(OV_FpsSetting_t));
#endif
#ifdef RS0509_SIZE_HD
	pRS0509_FpsSetting = GetOvFpsSetting(byFps, g_staRS0509HDFpsSetting, \
	                                     sizeof(g_staRS0509HDFpsSetting)/sizeof(OV_FpsSetting_t));
#endif
#ifdef RS0509_SIZE_SXGA
	pRS0509_FpsSetting = GetOvFpsSetting(byFps, g_staRS0509SXGAFpsSetting, \
	                                     sizeof(g_staRS0509SXGAFpsSetting)/sizeof(OV_FpsSetting_t));
#endif
	g_wSensorHsyncWidth =  pRS0509_FpsSetting->wExtraDummyPixel; // this for manual exposure
	g_dwPclk = pRS0509_FpsSetting->dwPixelClk;		// this for scale speed
}

#ifdef RS0509_SIZE_QXGA
#ifdef _MIPI_EXIST_
/*
********************************************************************************
*										ConfigureResolutionQXGA
* FUNCTION ConfigureResolutionQXGA
********************************************************************************
*/
/**
  Configure RLE0509 Resolution QXGA.

  	# Example:
	# mipi_clk = Fref * (DSI_SSC_NC+2+DSI_SSC_FC/4096)/(DSI_CMU_PREDIV)/(DSI_CMU_PSTDV+2)
	# 		 = 12 *(65+2+0/4096) = 804MHz
	# mipi_clk_lane = mipi_clock/2 = 402MHz

  	# Example:
	# fixed pattern model configuration
	# frame_sum =
	# (Frame_width��(speed_div+1)/(speed_mult+1)+dummy_pix) ��(frame_height + dummy_line)
	# (2048 *(0+1)/(0+1) + x )*( 1536 + 4 )= 3350000
	# fps = tx_clk/frame_sum = mipi_clk/8/frame_sum = mipi_clk_lane*2/8/frame_sum
	#      =  804*10^6/8/ frame_sum= 30 ==> x = 126
  \param none

  \retval none.
********************************************************************************
*/
static U8 ConfigureResolutionQXGA(U8 byFps)
{
	U16 wTmp;
	OV_FpsSetting_t *pRS0509_FpsSetting;

	pRS0509_FpsSetting = GetOvFpsSetting(byFps, g_staRS0509QXGAFpsSetting, \
	                                     sizeof(g_staRS0509QXGAFpsSetting)/sizeof(OV_FpsSetting_t));

	//---------------------------------PLL MIPICLK PIXCLK----------------------------------
	//DSI_CMU_PREDIV = 1
	Write_SenReg(DSI_CMU_CTRL0, DSI_CMU_CP_7uA5|DSI_CMU_LDOREF_1V26|DSI_CMU_PREDIV_1);
	//DSI_CMU_POSTDIV
	if (DSI_CMU_POSTDIV_0 != pRS0509_FpsSetting->byClkrc)
	{
		Write_SenReg(DSI_CMU_CTRL1, (pRS0509_FpsSetting->byClkrc-2));
		Write_SenReg_Mask(DSI_POWER_CTRL0, DSI_CMU_SEL_POSTDIV ,DSI_CMU_SEL_POSTDIV);
	}
	//DSI_SSC_NC = 65
	Write_SenReg(DSI_SSC_NC_L, 65);
	Write_SenReg(DSI_SSC_NC_H, 0);
	//DIS_SSC_FC = 0
	Write_SenReg(DSI_SSC_FC_L, 0);
	Write_SenReg(DSI_SSC_FC_H, 0);

	//---------------------------------FIXP SETTING--------------------------------------
	//24 COLOR Setting
	Write_SenReg(COLOR_BLOCK_WIDTH_L, 0x1C);	//(2048-100x2-20x7)/6 = 284 = 0x11C
	Write_SenReg(COLOR_BLOCK_HEIGHT_L, 0x35);	//(1536-100x2-20x5)/4 = 309 = 0x135
	Write_SenReg(COLOR_BLOCK_WH_H, 0x11);
	//Frame Width = 0x0800= 2048
	Write_SenReg(FIXP_FRAME_WIDTH_H, 0x08);
	Write_SenReg(FIXP_FRAME_WIDTH_L, 0x00);
	//Frame Height = 0x0600 = 1536
	Write_SenReg(FIXP_FRAME_HEIGTH_H, 0x06);
	Write_SenReg(FIXP_FRAME_HEIGTH_L, 0x00);

	wTmp = pRS0509_FpsSetting->wExtraDummyPixel - 2048;
	//Dummy Pixel = 0x20C = 524 must have enough dummy pixel to insert FS/LP etc non-protocal state. and raw format translation.
	Write_SenReg(FIXP_DUMMY_PIXEL_H, INT2CHAR(wTmp, 1));
	Write_SenReg(FIXP_DUMMY_PIXEL_L, INT2CHAR(wTmp, 0));
	//Dummy Line = 0x0004 = 04 frame num 0, always output fix pattern. Bug: If this is 0, the ssor will stop transfer images.
	Write_SenReg(FIXP_DUMMY_LINE_H, 0x00);
	Write_SenReg(FIXP_DUMMY_LINE_L, 0x04);	//0X18
	//Speed = 0
	Write_SenReg(FIXP_SPEED_CTRL, 0x00);
	//Frame Num
	Write_SenReg(FIXP_FRAME_NUM, 0xFF);

	//---------------------------------MIPI IF SETTING--------------------------------------
	Write_SenReg(0x0303, 0x00);
	Write_SenReg(0x0304, 0x31);
	Write_SenReg(0x0305, 0x0C);
	Write_SenReg(0x0306, 0x06);
	Write_SenReg(0x0307, 0x0D);
	Write_SenReg(0x0308, 0x09);
	Write_SenReg(0x0309, 0x1D);
	Write_SenReg(0x030A, 0x01);
	Write_SenReg(0x030B, 0x09);
	Write_SenReg(0x030C, 0x09);
	Write_SenReg(0x030D, 0x09);
	Write_SenReg(0x030E, 0x05);

	Write_SenReg(MIPI_TX_DATA_TYPE, DATA_TYPE_RAW8);
	Write_SenReg(MIPI_TX_CONFIG, LANE0_EN|LANE1_EN);		// 2 Lane can only run 15FPS, 1 lane can only run 20 fps
	Write_SenReg(MIPI_TX_DATA_SRC, DATA_SRC_FIXP);
	Write_SenReg(MIPI_TX_ENABLE_CTRL, MIPI_TX_ENABLE);

	//step3.FIXP RUN
	Write_SenReg(FIXP_CTRL, FIXP_RUN);

	//g_wSensorHsyncWidth =  pRS0509_FpsSetting->wExtraDummyPixel; // this for manual exposure
	//g_dwPclk = pRS0509_FpsSetting->dwPixelClk;		// this for scale speed
	GetSensorPclkHsync(QXGA_FRM, byFps);

	g_wAECExposureRowMax = 1560-4;	// this for max exposure time
	g_wAEC_LineNumber = 1560;	// this for AE insert dummy line algothrim
	g_wSensorWidthBefBLC = 2048;
	g_wSensorHeightBefBLC = 1536;

	//-----------------------------Dphy Setting--------------------------------
	SetMipiDphy(MIPI_DATA_LANE0_EN |MIPI_DATA_LANE1_EN, \
	            MIPI_DATA_FORMAT_RAW10 , MIPI_DATA_TYPE_RAW10, \
	            HSTERM_EN_TIME_44);
	if (0x03 == (g_byMipiDphyCtrl&0x03))		//RS0509 2 lane,mipi clk can't  halve, so merge ispclk need double
	{
		g_dwPclk <<=1;
	}
	return TRUE;
}
#else
/*
********************************************************************************
*										ConfigureResolutionQXGA
* FUNCTION ConfigureResolutionQXGA
********************************************************************************
*/
/**
  Configure RLE0509 Resolution QXGA.

  	# Example:
	# mipi_clk = Fref * (DSI_SSC_NC+2+DSI_SSC_FC/4096)/(DSI_CMU_PREDIV)/(DSI_CMU_PSTDV+2)
	# 		 = 12 *(65+2+0/4096) = 804MHz
	# mipi_clk_lane = mipi_clock/2 = 402MHz

  	# Example:
	# fixed pattern model configuration
	# frame_sum = 
	# (Frame_width��(speed_div+1)/(speed_mult+1)+dummy_pix) ��(frame_height + dummy_line)
	# (2048 *(0+1)/(0+1) + x )*( 1536 + 4 )= 3350000
	# fps = tx_clk/frame_sum = mipi_clk/8/frame_sum = mipi_clk_lane*2/8/frame_sum
	#      =  804*10^6/8/ frame_sum= 30 ==> x = 126
  \param none

  \retval none.
********************************************************************************
*/
static U8 ConfigureResolutionQXGA(U8 byFps)
{
	U16 wTmp;
	OV_FpsSetting_t *pRS0509_FpsSetting;

	pRS0509_FpsSetting = GetOvFpsSetting(byFps, g_staRS0509QXGAFpsSetting, \
	                                     sizeof(g_staRS0509QXGAFpsSetting)/sizeof(OV_FpsSetting_t));

	//---------------------------------PLL MIPICLK PIXCLK----------------------------------
	//DSI_CMU_PREDIV = 1
	Write_SenReg(DSI_CMU_CTRL0, DSI_CMU_CP_7uA5|DSI_CMU_LDOREF_1V26|DSI_CMU_PREDIV_1);
	//DSI_CMU_POSTDIV
	if (DSI_CMU_POSTDIV_0 != pRS0509_FpsSetting->byClkrc)
	{
		Write_SenReg(DSI_CMU_CTRL1, (pRS0509_FpsSetting->byClkrc-2));
		Write_SenReg_Mask(DSI_POWER_CTRL0, DSI_CMU_SEL_POSTDIV ,DSI_CMU_SEL_POSTDIV);
	}
	//DSI_SSC_NC = 65
	Write_SenReg(DSI_SSC_NC_L, 65);
	Write_SenReg(DSI_SSC_NC_H, 0);
	//DIS_SSC_FC = 0
	Write_SenReg(DSI_SSC_FC_L, 0);
	Write_SenReg(DSI_SSC_FC_H, 0);

	//---------------------------------FIXP SETTING--------------------------------------
	//24 COLOR Setting
	Write_SenReg(COLOR_BLOCK_WIDTH_L, 0x1C);	//(2048-100x2-20x7)/6 = 284 = 0x11C
	Write_SenReg(COLOR_BLOCK_HEIGHT_L, 0x35);	//(1536-100x2-20x5)/4 = 309 = 0x135
	Write_SenReg(COLOR_BLOCK_WH_H, 0x11);
	//Frame Width = 0x0800= 2048
	Write_SenReg(FIXP_FRAME_WIDTH_H, 0x08);
	Write_SenReg(FIXP_FRAME_WIDTH_L, 0x00);
	//Frame Height = 0x0600 = 1536
	Write_SenReg(FIXP_FRAME_HEIGTH_H, 0x06);
	Write_SenReg(FIXP_FRAME_HEIGTH_L, 0x00);

	wTmp = pRS0509_FpsSetting->wExtraDummyPixel - 2048;
	//Dummy Pixel = 0x20C = 524 must have enough dummy pixel to insert FS/LP etc non-protocal state. and raw format translation.
	Write_SenReg(FIXP_DUMMY_PIXEL_H, INT2CHAR(wTmp, 1));
	Write_SenReg(FIXP_DUMMY_PIXEL_L, INT2CHAR(wTmp, 0));
	//Dummy Line = 0x0004 = 04 frame num 0, always output fix pattern. Bug: If this is 0, the ssor will stop transfer images.
	Write_SenReg(FIXP_DUMMY_LINE_H, 0x00);
	Write_SenReg(FIXP_DUMMY_LINE_L, 0x04);	//0X18
	//Speed = 0
	Write_SenReg(FIXP_SPEED_CTRL, 0x00);
	//Frame Num
	Write_SenReg(FIXP_FRAME_NUM, 0xFF);

	//---------------------------------DVP IF SETTING-------------------------------------
	Write_SenReg(DVP_FRAME_WIDTH_H, 0x08);	//2048
	Write_SenReg(DVP_FRAME_WIDTH_L, 0X00);
	Write_SenReg(DVP_FRAME_HEIGHT_H, 0x06);	//1536
	Write_SenReg(DVP_FRAME_HEIGHT_L, 0x00);
	Write_SenReg(DVP_DUMMY_LEFT_H, 0x00);
	Write_SenReg(DVP_DUMMY_LEFT_L, 0x00);
	Write_SenReg(DVP_DUMMY_RIGHT_H, 0x00);
	Write_SenReg(DVP_DUMMY_RIGHT_L, 0x00);
	Write_SenReg(DVP_DUMMY_BOTTOM_H, 0x00);
	Write_SenReg(DVP_DUMMY_BOTTOM_L, 0x00);
	Write_SenReg(DVP_DATA_LEFT_H, 0x00);
	Write_SenReg(DVP_DATA_LEFT_L, 0x00);
	Write_SenReg(DVP_DATA_RIGHT_H, 0x00);
	Write_SenReg(DVP_DATA_RIGHT_L, 0x00);
	Write_SenReg(DVP_DATA_BOTTOM_H, 0x00);
	Write_SenReg(DVP_DATA_BOTTOM_L, 0x00);
	Write_SenReg(DVP_VSYNC_CTRL, DVP_VSYNC_HIGH_ACTIVE);	//High Active
	Write_SenReg(DVP_HSYNC_CTRL, DVP_HSYNC_HIGH_ACTIVE);	//High Active
	Write_SenReg(DVP_PCLK_CTRL, DVP_FALINGEDGE_OUTPUT);		//PCLK Faling edge output data
	Write_SenReg(DVP_HSYNC_BREAK_H, 0x00);	//little value, Just toggle hsync and wait next valiad data
	Write_SenReg(DVP_HSYNC_BREAK_L, 0x0F);
	Write_SenReg(DVP_VSYNC_BREAK_H, 0x00);	//little value, Just toggle hsync and wait next valiad data
	Write_SenReg(DVP_VSYNC_BREAK_L, 0x0f) ;
	Write_SenReg(DVP_VHSYNC_BREAK_H, 0x00);
	Write_SenReg(DVP_VHSYNC_BREAK_L, 0x0F);	//little value, Just toggle hsync and wait next valiad data
	Write_SenReg(DVP_HVSYNC_BREAK_H, 0x00);
	Write_SenReg(DVP_HVSYNC_BREAK_L, 0x0F);	//little value, Just toggle hsync and wait next valiad data

	Write_SenReg(0x0111, 0x00);
	Write_SenReg(0x010f, 0x01);
	Write_SenReg(0x0110, 0x04);	//12mA smller than this value will have some problem

	Write_SenReg(DVP_CLK_CTRL, 0x81);	//only 0x81 is valid
	Write_SenReg(DVP_ENABLE_CTRL, DVP_EN);

	//step3. FIXP RUN
	Write_SenReg(FIXP_CTRL, FIXP_RUN);

	//g_wSensorHsyncWidth =  pRS0509_FpsSetting->wExtraDummyPixel; // this for manual exposure
	//g_dwPclk = pRS0509_FpsSetting->dwPixelClk;		// this for scale speed
	GetSensorPclkHsync(QXGA_FRM, byFps);
	g_wAECExposureRowMax = 1540-4;	// this for max exposure time
	g_wAEC_LineNumber = 1540;	// this for AE insert dummy line algothrim
	g_wSensorWidthBefBLC = 2048;
	g_wSensorHeightBefBLC = 1536;

	return TRUE;
}
#endif	//#ifdef _MIPI_EXIST_

#ifdef _MIPI_EXIST_
/*
********************************************************************************
*										ConfigureResolutionHD
* FUNCTION ConfigureResolutionHD
********************************************************************************
*/
/**
  Configure RLE0509 Resolution HD.

  	# Example:
	# mipi_clk = Fref * (DSI_SSC_NC+2+DSI_SSC_FC/4096)/(DSI_CMU_PREDIV)/(DSI_CMU_PSTDV+2)
	# 		 = 12 *(28+2+0/4096) / = 360MHz
	# mipi_clk_lane = mipi_clock/2 = 180MHz

  	# Example:
	# fixed pattern model configuration
	# frame_sum =
	# (Frame_width��(speed_div+1)/(speed_mult+1)+dummy_pix) ��(frame_height + dummy_line)
	# (1280 *(0+1)/(0+1) + x )*( 720 + 24 )= 1500000
	# fps = tx_clk/frame_sum = mipi_clk/8/frame_sum = mipi_clk_lane*2/8/frame_sum
	#      =  360*10^6/8/ frame_sum= 30 ==> x = 656
  \param none

  \retval none.
********************************************************************************
*/
static U8 ConfigureResolutionHD(U8 byFps)
{
	U16 wTmp;
	OV_FpsSetting_t *pRS0509_FpsSetting;

	pRS0509_FpsSetting = GetOvFpsSetting(byFps, g_staRS0509HDFpsSetting, \
	                                     sizeof(g_staRS0509HDFpsSetting)/sizeof(OV_FpsSetting_t));

	//---------------------------------PLL MIPICLK PIXCLK----------------------------------
	//DSI_CMU_PREDIV = 1
	Write_SenReg(DSI_CMU_CTRL0, DSI_CMU_CP_7uA5|DSI_CMU_LDOREF_1V26|DSI_CMU_PREDIV_1);
	//DSI_CMU_POSTDIV
	if (DSI_CMU_POSTDIV_0 != pRS0509_FpsSetting->byClkrc)
	{
		Write_SenReg(DSI_CMU_CTRL1, (pRS0509_FpsSetting->byClkrc-2));
		Write_SenReg_Mask(DSI_POWER_CTRL0, DSI_CMU_SEL_POSTDIV ,DSI_CMU_SEL_POSTDIV);
	}
	//DSI_SSC_NC = 52
	Write_SenReg(DSI_SSC_NC_L, 28);	 //28=360MHZ
	Write_SenReg(DSI_SSC_NC_H, 0);
	//DIS_SSC_FC = 0
	Write_SenReg(DSI_SSC_FC_L, 0);
	Write_SenReg(DSI_SSC_FC_H, 0);

	//---------------------------------FIXP SETTING--------------------------------------
	//24 COLOR Setting
	Write_SenReg(COLOR_BLOCK_WIDTH_L, 0x9C);	//(1280-100x2-20x7)/6
	Write_SenReg(COLOR_BLOCK_HEIGHT_L, 0x69);	//(720-100x2-20x5)/4
	Write_SenReg(COLOR_BLOCK_WH_H, 0x00);
	//Frame Width = 0x0500= 1280
	Write_SenReg(FIXP_FRAME_WIDTH_H, 0x05);
	Write_SenReg(FIXP_FRAME_WIDTH_L, 0x00);
	//Frame Height = 0x2D0 = 720
	Write_SenReg(FIXP_FRAME_HEIGTH_H, 0x02);
	Write_SenReg(FIXP_FRAME_HEIGTH_L, 0xD0);

	wTmp = pRS0509_FpsSetting->wExtraDummyPixel - 1280;
	//Dummy Pixel = 0x92C = 2348 must have enough dummy pixel to insert FS/LP etc non-protocal state. and raw format translation.
	Write_SenReg(FIXP_DUMMY_PIXEL_H, INT2CHAR(wTmp, 1));
	Write_SenReg(FIXP_DUMMY_PIXEL_L, INT2CHAR(wTmp, 0));
	//Dummy Line = 0x018 = 24 frame num 0, always output fix pattern. Bug: If this is 0, the ssor will stop transfer images.
	Write_SenReg(FIXP_DUMMY_LINE_H, 0x00);
	Write_SenReg(FIXP_DUMMY_LINE_L, 0x18);
	//Speed = 0
	Write_SenReg(FIXP_SPEED_CTRL, 0x00);
	//Frame Num
	Write_SenReg(FIXP_FRAME_NUM, 0xFF);

	//---------------------------------MIPI IF SETTING--------------------------------------
	Write_SenReg(0x0303, 0x00);
	Write_SenReg(0x0304, 0x31);
	Write_SenReg(0x0305, 0x0C);
	Write_SenReg(0x0306, 0x06);
	Write_SenReg(0x0307, 0x0D);
	Write_SenReg(0x0308, 0x09);
	Write_SenReg(0x0309, 0x1D);
	Write_SenReg(0x030A, 0x01);
	Write_SenReg(0x030B, 0x09);
	Write_SenReg(0x030C, 0x09);
	Write_SenReg(0x030D, 0x09);
	Write_SenReg(0x030E, 0x05);

	Write_SenReg(MIPI_TX_DATA_TYPE, DATA_TYPE_RAW10);	//RAW10
	Write_SenReg(MIPI_TX_CONFIG, LANE0_EN);		// 1 Lane
	Write_SenReg(MIPI_TX_DATA_SRC, DATA_SRC_FIXP);
	Write_SenReg(MIPI_TX_ENABLE_CTRL, MIPI_TX_ENABLE);

	//step3.FIXP RUN
	Write_SenReg(FIXP_CTRL, FIXP_RUN);

	//g_wSensorHsyncWidth =  pRS0509_FpsSetting->wExtraDummyPixel; // this for manual exposure
	//g_dwPclk = pRS0509_FpsSetting->dwPixelClk;		// this for scale speed
	GetSensorPclkHsync(HD720P_FRM, byFps);
	//if (0x03 == (XBYTE[MIPI_TX_CONFIG]&0x03))		//RS0509 2 lane,mipi clk can't  halve, so merge ispclk need double
	//{
	//	g_dwPclk <<=1;
	//}
	g_wAECExposureRowMax = 744-4;	// this for max exposure time
	g_wAEC_LineNumber = 744;	// this for AE insert dummy line algothrim
	g_wSensorWidthBefBLC = 1280;
	g_wSensorHeightBefBLC = 720;

	//-----------------------------Dphy Setting--------------------------------
	SetMipiDphy(MIPI_DATA_LANE0_EN , \
	            MIPI_DATA_FORMAT_RAW10 , MIPI_DATA_TYPE_RAW10, \
	            HSTERM_EN_TIME_44);
	if (0x03 == (g_byMipiDphyCtrl&0x03))		//RS0509 2 lane,mipi clk can't  halve, so merge ispclk need double
	{
		g_dwPclk <<=1;
	}
	return TRUE;
}
#else
/*
********************************************************************************
*										ConfigureResolutionHD
* FUNCTION ConfigureResolutionHD
********************************************************************************
*/
/**
  Configure RLE0509 Resolution HD.

  	# Example:
	# mipi_clk = Fref * (DSI_SSC_NC+2+DSI_SSC_FC/4096)/(DSI_CMU_PREDIV)/(DSI_CMU_PSTDV+2)
	# 		 = 12 *(28+2+0/4096) / = 360MHz
	# mipi_clk_lane = mipi_clock/2 = 180MHz

  	# Example:
	# fixed pattern model configuration
	# frame_sum = 
	# (Frame_width��(speed_div+1)/(speed_mult+1)+dummy_pix) ��(frame_height + dummy_line)
	# (1280 *(0+1)/(0+1) + x )*( 720 + 24 )= 1500000
	# fps = tx_clk/frame_sum = mipi_clk/8/frame_sum = mipi_clk_lane*2/8/frame_sum
	#      =  360*10^6/8/ frame_sum= 30 ==> x = 656
  \param none

  \retval none.
********************************************************************************
*/
static U8 ConfigureResolutionHD(U8 byFps)
{
	U16 wTmp;
	OV_FpsSetting_t *pRS0509_FpsSetting;

	pRS0509_FpsSetting = GetOvFpsSetting(byFps, g_staRS0509HDFpsSetting, \
	                                     sizeof(g_staRS0509HDFpsSetting)/sizeof(OV_FpsSetting_t));

	//---------------------------------PLL MIPICLK PIXCLK----------------------------------
	//DSI_CMU_PREDIV = 1
	Write_SenReg(DSI_CMU_CTRL0, DSI_CMU_CP_7uA5|DSI_CMU_LDOREF_1V26|DSI_CMU_PREDIV_1);
	//DSI_CMU_POSTDIV
	if (DSI_CMU_POSTDIV_0 != pRS0509_FpsSetting->byClkrc)
	{
		Write_SenReg(DSI_CMU_CTRL1, (pRS0509_FpsSetting->byClkrc-2));
		Write_SenReg_Mask(DSI_POWER_CTRL0, DSI_CMU_SEL_POSTDIV ,DSI_CMU_SEL_POSTDIV);
	}
	//DSI_SSC_NC = 52
	Write_SenReg(DSI_SSC_NC_L, 28);	 //28=360MHZ
	Write_SenReg(DSI_SSC_NC_H, 0);
	//DIS_SSC_FC = 0
	Write_SenReg(DSI_SSC_FC_L, 0);
	Write_SenReg(DSI_SSC_FC_H, 0);

	//---------------------------------FIXP SETTING--------------------------------------
	//24 COLOR Setting
	Write_SenReg(COLOR_BLOCK_WIDTH_L, 0x9C);	//(1280-100x2-20x7)/6
	Write_SenReg(COLOR_BLOCK_HEIGHT_L, 0x69);	//(720-100x2-20x5)/4
	Write_SenReg(COLOR_BLOCK_WH_H, 0x00);
	//Frame Width = 0x0500= 1280
	Write_SenReg(FIXP_FRAME_WIDTH_H, 0x05);
	Write_SenReg(FIXP_FRAME_WIDTH_L, 0x00);
	//Frame Height = 0x2D0 = 720
	Write_SenReg(FIXP_FRAME_HEIGTH_H, 0x02);
	Write_SenReg(FIXP_FRAME_HEIGTH_L, 0xD0);

	wTmp = pRS0509_FpsSetting->wExtraDummyPixel - 1280;
	//Dummy Pixel = 0x92C = 2348 must have enough dummy pixel to insert FS/LP etc non-protocal state. and raw format translation.
	Write_SenReg(FIXP_DUMMY_PIXEL_H, INT2CHAR(wTmp, 1));
	Write_SenReg(FIXP_DUMMY_PIXEL_L, INT2CHAR(wTmp, 0));
	//Dummy Line = 0x018 = 24 frame num 0, always output fix pattern. Bug: If this is 0, the ssor will stop transfer images.
	Write_SenReg(FIXP_DUMMY_LINE_H, 0x00);
	Write_SenReg(FIXP_DUMMY_LINE_L, 0x18);
	//Speed = 0
	Write_SenReg(FIXP_SPEED_CTRL, 0x00);
	//Frame Num
	Write_SenReg(FIXP_FRAME_NUM, 0xFF);

	//---------------------------------DVP IF SETTING-------------------------------------
	Write_SenReg(DVP_FRAME_WIDTH_H, 0x05);	//1280
	Write_SenReg(DVP_FRAME_WIDTH_L, 0X00);
	Write_SenReg(DVP_FRAME_HEIGHT_H, 0x02);	//720
	Write_SenReg(DVP_FRAME_HEIGHT_L, 0xD0);
	Write_SenReg(DVP_DUMMY_LEFT_H, 0x00);
	Write_SenReg(DVP_DUMMY_LEFT_L, 0x00);
	Write_SenReg(DVP_DUMMY_RIGHT_H, 0x00);
	Write_SenReg(DVP_DUMMY_RIGHT_L, 0x00);
	Write_SenReg(DVP_DUMMY_BOTTOM_H, 0x00);
	Write_SenReg(DVP_DUMMY_BOTTOM_L, 0x00);
	Write_SenReg(DVP_DATA_LEFT_H, 0x00);
	Write_SenReg(DVP_DATA_LEFT_L, 0x00);
	Write_SenReg(DVP_DATA_RIGHT_H, 0x00);
	Write_SenReg(DVP_DATA_RIGHT_L, 0x00);
	Write_SenReg(DVP_DATA_BOTTOM_H, 0x00);
	Write_SenReg(DVP_DATA_BOTTOM_L, 0x00);
	Write_SenReg(DVP_VSYNC_CTRL, DVP_VSYNC_HIGH_ACTIVE);	//High Active
	Write_SenReg(DVP_HSYNC_CTRL, DVP_HSYNC_HIGH_ACTIVE);	//High Active
	Write_SenReg(DVP_PCLK_CTRL, DVP_FALINGEDGE_OUTPUT);		//PCLK Faling edge output data
	Write_SenReg(DVP_HSYNC_BREAK_H, 0x00);	//little value, Just toggle hsync and wait next valiad data
	Write_SenReg(DVP_HSYNC_BREAK_L, 0x0F);
	Write_SenReg(DVP_VSYNC_BREAK_H, 0x00);	//little value, Just toggle hsync and wait next valiad data
	Write_SenReg(DVP_VSYNC_BREAK_L, 0x0f) ;
	Write_SenReg(DVP_VHSYNC_BREAK_H, 0x00);
	Write_SenReg(DVP_VHSYNC_BREAK_L, 0x0F);	//little value, Just toggle hsync and wait next valiad data
	Write_SenReg(DVP_HVSYNC_BREAK_H, 0x00);
	Write_SenReg(DVP_HVSYNC_BREAK_L, 0x0F);	//little value, Just toggle hsync and wait next valiad data

	Write_SenReg(0x0111, 0x00);
	Write_SenReg(0x010f, 0x01);
	Write_SenReg(0x0110, 0x04);	//12mA smller than this value will have some problem

	Write_SenReg(DVP_CLK_CTRL, 0x81);	//only 0x81 is valid
	Write_SenReg(DVP_ENABLE_CTRL, DVP_EN);

	//step3. FIXP RUN
	Write_SenReg(FIXP_CTRL, FIXP_RUN);
	//g_wSensorHsyncWidth =  pRS0509_FpsSetting->wExtraDummyPixel; // this for manual exposure
	//g_dwPclk = pRS0509_FpsSetting->dwPixelClk;		// this for scale speed
	GetSensorPclkHsync(HD720P_FRM, byFps);
	g_wAECExposureRowMax = 744-4;	// this for max exposure time
	g_wAEC_LineNumber = 744;	// this for AE insert dummy line algothrim
	g_wSensorWidthBefBLC = 1280;
	g_wSensorHeightBefBLC = 720;
	return TRUE;
}
#endif	//ifdef _MIPI_EXIST_
#endif	//#ifdef RS0509_SIZE_QXGA

#ifdef RS0509_SIZE_FHD
#ifdef _MIPI_EXIST_
/*
********************************************************************************
*										ConfigureResolutionFHD
* FUNCTION ConfigureResolutionFHD
********************************************************************************
*/
/**
  Configure RLE0509 Resolution FHD.

  	# Example:
	# mipi_clk = Fref * (DSI_SSC_NC+2+DSI_SSC_FC/4096)/(DSI_CMU_PREDIV)/(DSI_CMU_PSTDV+2)
	# 		 = 12 *(52+2+0/4096) = 648MHz
	# mipi_clk_lane = mipi_clock/2 = 324MHz

  	# Example:
	# fixed pattern model configuration
	# frame_sum =
	# (Frame_width��(speed_div+1)/(speed_mult+1)+dummy_pix) ��(frame_height + dummy_line)
	# (1920 *(0+1)/(0+1) + x )*( 1080 + 24 )= 2700000
	# fps = tx_clk/frame_sum = mipi_clk/8/frame_sum = mipi_clk_lane*2/8/frame_sum
	#      =  648*10^6/8/ frame_sum= 30 ==> x = 524
  \param none

  \retval none.
********************************************************************************
*/
static U8 ConfigureResolutionFHD(U8 byFps)
{
	U16 wTmp;
	OV_FpsSetting_t *pRS0509_FpsSetting;

	pRS0509_FpsSetting = GetOvFpsSetting(byFps, g_staRS0509FHDFpsSetting, \
	                                     sizeof(g_staRS0509FHDFpsSetting)/sizeof(OV_FpsSetting_t));

	//---------------------------------PLL MIPICLK PIXCLK----------------------------------
	//DSI_CMU_PREDIV = 1
	Write_SenReg(DSI_CMU_CTRL0, DSI_CMU_CP_7uA5|DSI_CMU_LDOREF_1V26|DSI_CMU_PREDIV_1);
	//DSI_CMU_POSTDIV
	if (DSI_CMU_POSTDIV_0 != pRS0509_FpsSetting->byClkrc)
	{
		Write_SenReg(DSI_CMU_CTRL1, (pRS0509_FpsSetting->byClkrc-2));
		Write_SenReg_Mask(DSI_POWER_CTRL0, DSI_CMU_SEL_POSTDIV ,DSI_CMU_SEL_POSTDIV);
	}
	//DSI_SSC_NC = 52
	Write_SenReg(DSI_SSC_NC_L, 52);	 //52=648MHZ
	Write_SenReg(DSI_SSC_NC_H, 0);
	//DIS_SSC_FC = 0
	Write_SenReg(DSI_SSC_FC_L, 0);
	Write_SenReg(DSI_SSC_FC_H, 0);

	//---------------------------------FIXP SETTING--------------------------------------
	//24 COLOR Setting
	Write_SenReg(COLOR_BLOCK_WIDTH_L, 0x07);	//(1920-100x2-20x7)/6
	Write_SenReg(COLOR_BLOCK_HEIGHT_L, 0xC3);	//(1080-100x2-20x5)/4
	Write_SenReg(COLOR_BLOCK_WH_H, 0x10);
	//Frame Width = 0x0780= 1920
	Write_SenReg(FIXP_FRAME_WIDTH_H, 0x07);
	Write_SenReg(FIXP_FRAME_WIDTH_L, 0x80);
	//Frame Height = 0x438 = 1080
	Write_SenReg(FIXP_FRAME_HEIGTH_H, 0x04);
	Write_SenReg(FIXP_FRAME_HEIGTH_L, 0x38);

	wTmp = pRS0509_FpsSetting->wExtraDummyPixel - 1920;
	//Dummy Pixel = 0x20C = 524 must have enough dummy pixel to insert FS/LP etc non-protocal state. and raw format translation.
	Write_SenReg(FIXP_DUMMY_PIXEL_H, INT2CHAR(wTmp, 1));
	Write_SenReg(FIXP_DUMMY_PIXEL_L, INT2CHAR(wTmp, 0));
	//Dummy Line = 0x018 = 24 frame num 0, always output fix pattern. Bug: If this is 0, the ssor will stop transfer images.
	Write_SenReg(FIXP_DUMMY_LINE_H, 0x00);
	Write_SenReg(FIXP_DUMMY_LINE_L, 0x18);
	//Speed = 0
	Write_SenReg(FIXP_SPEED_CTRL, 0x00);
	//Frame Num
	Write_SenReg(FIXP_FRAME_NUM, 0xFF);

	//---------------------------------MIPI IF SETTING--------------------------------------
	Write_SenReg(0x0303, 0x00);
	Write_SenReg(0x0304, 0x31);
	Write_SenReg(0x0305, 0x0C);
	Write_SenReg(0x0306, 0x06);
	Write_SenReg(0x0307, 0x0D);
	Write_SenReg(0x0308, 0x09);
	Write_SenReg(0x0309, 0x1D);
	Write_SenReg(0x030A, 0x01);
	Write_SenReg(0x030B, 0x09);
	Write_SenReg(0x030C, 0x09);
	Write_SenReg(0x030D, 0x09);
	Write_SenReg(0x030E, 0x05);

	Write_SenReg(MIPI_TX_DATA_TYPE, DATA_TYPE_RAW10);	//RAW10
	Write_SenReg(MIPI_TX_CONFIG, LANE0_EN|LANE1_EN);		// 2 Lane
	Write_SenReg(MIPI_TX_DATA_SRC, DATA_SRC_FIXP);
	Write_SenReg(MIPI_TX_ENABLE_CTRL, MIPI_TX_ENABLE);

	//step3.FIXP RUN
	Write_SenReg(FIXP_CTRL, FIXP_RUN);

	//g_wSensorHsyncWidth =  pRS0509_FpsSetting->wExtraDummyPixel; // this for manual exposure
	//g_dwPclk = pRS0509_FpsSetting->dwPixelClk;		// this for scale speed
	GetSensorPclkHsync(HD1080P_FRM, byFps);
	//if (0x03 == (XBYTE[MIPI_TX_CONFIG]&0x03))		//RS0509 2 lane,mipi clk can't  halve, so merge ispclk need double
	//{
	//	g_dwPclk <<=1;
	//}
	g_wAECExposureRowMax = 1104-4;	// this for max exposure time
	g_wAEC_LineNumber = 1104;	// this for AE insert dummy line algothrim
	g_wSensorWidthBefBLC = 1920;
	g_wSensorHeightBefBLC = 1080;

	//-----------------------------Dphy Setting--------------------------------
	SetMipiDphy(MIPI_DATA_LANE0_EN | MIPI_DATA_LANE1_EN , \
	            MIPI_DATA_FORMAT_RAW10 , MIPI_DATA_TYPE_RAW10, \
	            HSTERM_EN_TIME_44);
	if (0x03 == (g_byMipiDphyCtrl&0x03))		//RS0509 2 lane,mipi clk can't  halve, so merge ispclk need double
	{
		g_dwPclk <<=1;
	}
	return TRUE;
}
#else
/*
********************************************************************************
*										ConfigureResolutionFHD
* FUNCTION ConfigureResolutionFHD
********************************************************************************
*/
/**
  Configure RLE0509 Resolution FHD.

  	# Example:
	# mipi_clk = Fref * (DSI_SSC_NC+2+DSI_SSC_FC/4096)/(DSI_CMU_PREDIV)/(DSI_CMU_PSTDV+2)
	# 		 = 12 *(52+2+0/4096) = 648MHz
	# mipi_clk_lane = mipi_clock/2 = 324MHz

  	# Example:
	# fixed pattern model configuration
	# frame_sum = 
	# (Frame_width��(speed_div+1)/(speed_mult+1)+dummy_pix) ��(frame_height + dummy_line)
	# (1920 *(0+1)/(0+1) + x )*( 1080 + 24 )= 2700000
	# fps = tx_clk/frame_sum = mipi_clk/8/frame_sum = mipi_clk_lane*2/8/frame_sum
	#      =  648*10^6/8/ frame_sum= 30 ==> x = 524
  \param none

  \retval none.
********************************************************************************
*/
static U8 ConfigureResolutionFHD(U8 byFps)
{
	U16 wTmp;
	OV_FpsSetting_t *pRS0509_FpsSetting;

	pRS0509_FpsSetting = GetOvFpsSetting(byFps, g_staRS0509FHDFpsSetting, \
	                                     sizeof(g_staRS0509FHDFpsSetting)/sizeof(OV_FpsSetting_t));

	//---------------------------------PLL MIPICLK PIXCLK----------------------------------
	//DSI_CMU_PREDIV = 1
	Write_SenReg(DSI_CMU_CTRL0, DSI_CMU_CP_7uA5|DSI_CMU_LDOREF_1V26|DSI_CMU_PREDIV_1);
	//DSI_CMU_POSTDIV
	if (DSI_CMU_POSTDIV_0 != pRS0509_FpsSetting->byClkrc)
	{
		Write_SenReg(DSI_CMU_CTRL1, (pRS0509_FpsSetting->byClkrc-2));
		Write_SenReg_Mask(DSI_POWER_CTRL0, DSI_CMU_SEL_POSTDIV ,DSI_CMU_SEL_POSTDIV);
	}
	//DSI_SSC_NC = 52
	Write_SenReg(DSI_SSC_NC_L, 52);	 //52=648MHZ
	Write_SenReg(DSI_SSC_NC_H, 0);
	//DIS_SSC_FC = 0
	Write_SenReg(DSI_SSC_FC_L, 0);
	Write_SenReg(DSI_SSC_FC_H, 0);

	//---------------------------------FIXP SETTING--------------------------------------
	//24 COLOR Setting
	Write_SenReg(COLOR_BLOCK_WIDTH_L, 0x07);	//(1920-100x2-20x7)/6
	Write_SenReg(COLOR_BLOCK_HEIGHT_L, 0xC3);	//(1080-100x2-20x5)/4
	Write_SenReg(COLOR_BLOCK_WH_H, 0x10);
	//Frame Width = 0x0780= 1920
	Write_SenReg(FIXP_FRAME_WIDTH_H, 0x07);
	Write_SenReg(FIXP_FRAME_WIDTH_L, 0x80);
	//Frame Height = 0x438 = 1080
	Write_SenReg(FIXP_FRAME_HEIGTH_H, 0x04);
	Write_SenReg(FIXP_FRAME_HEIGTH_L, 0x38);

	wTmp = pRS0509_FpsSetting->wExtraDummyPixel - 1920;
	//Dummy Pixel = 0x20C = 524 must have enough dummy pixel to insert FS/LP etc non-protocal state. and raw format translation.
	Write_SenReg(FIXP_DUMMY_PIXEL_H, INT2CHAR(wTmp, 1));
	Write_SenReg(FIXP_DUMMY_PIXEL_L, INT2CHAR(wTmp, 0));
	//Dummy Line = 0x018 = 24 frame num 0, always output fix pattern. Bug: If this is 0, the ssor will stop transfer images.
	Write_SenReg(FIXP_DUMMY_LINE_H, 0x00);
	Write_SenReg(FIXP_DUMMY_LINE_L, 0x18);
	//Speed = 0
	Write_SenReg(FIXP_SPEED_CTRL, 0x00);
	//Frame Num
	Write_SenReg(FIXP_FRAME_NUM, 0xFF);

	//---------------------------------DVP IF SETTING-------------------------------------
	Write_SenReg(DVP_FRAME_WIDTH_H, 0x07);	//1920
	Write_SenReg(DVP_FRAME_WIDTH_L, 0x80);
	Write_SenReg(DVP_FRAME_HEIGHT_H, 0x04);	//1080
	Write_SenReg(DVP_FRAME_HEIGHT_L, 0x38);
	Write_SenReg(DVP_DUMMY_LEFT_H, 0x00);
	Write_SenReg(DVP_DUMMY_LEFT_L, 0x00);
	Write_SenReg(DVP_DUMMY_RIGHT_H, 0x00);
	Write_SenReg(DVP_DUMMY_RIGHT_L, 0x00);
	Write_SenReg(DVP_DUMMY_BOTTOM_H, 0x00);
	Write_SenReg(DVP_DUMMY_BOTTOM_L, 0x00);
	Write_SenReg(DVP_DATA_LEFT_H, 0x00);
	Write_SenReg(DVP_DATA_LEFT_L, 0x00);
	Write_SenReg(DVP_DATA_RIGHT_H, 0x00);
	Write_SenReg(DVP_DATA_RIGHT_L, 0x00);
	Write_SenReg(DVP_DATA_BOTTOM_H, 0x00);
	Write_SenReg(DVP_DATA_BOTTOM_L, 0x00);
	Write_SenReg(DVP_VSYNC_CTRL, DVP_VSYNC_HIGH_ACTIVE);	//High Active
	Write_SenReg(DVP_HSYNC_CTRL, DVP_HSYNC_HIGH_ACTIVE);	//High Active
	Write_SenReg(DVP_PCLK_CTRL, DVP_FALINGEDGE_OUTPUT);		//PCLK Faling edge output data
	Write_SenReg(DVP_HSYNC_BREAK_H, 0x00);	//little value, Just toggle hsync and wait next valiad data
	Write_SenReg(DVP_HSYNC_BREAK_L, 0x0F);
	Write_SenReg(DVP_VSYNC_BREAK_H, 0x00);	//little value, Just toggle hsync and wait next valiad data
	Write_SenReg(DVP_VSYNC_BREAK_L, 0x0f) ;
	Write_SenReg(DVP_VHSYNC_BREAK_H, 0x00);
	Write_SenReg(DVP_VHSYNC_BREAK_L, 0x0F);	//little value, Just toggle hsync and wait next valiad data
	Write_SenReg(DVP_HVSYNC_BREAK_H, 0x00);
	Write_SenReg(DVP_HVSYNC_BREAK_L, 0x0F);	//little value, Just toggle hsync and wait next valiad data

	Write_SenReg(0x0111, 0x00);
	Write_SenReg(0x010f, 0x01);
	Write_SenReg(0x0110, 0x04);	//12mA smller than this value will have some problem

	Write_SenReg(DVP_CLK_CTRL, 0x81);	//only 0x81 is valid
	Write_SenReg(DVP_ENABLE_CTRL, DVP_EN);

	//step3. FIXP RUN
	Write_SenReg(FIXP_CTRL, FIXP_RUN);

	//g_wSensorHsyncWidth =  pRS0509_FpsSetting->wExtraDummyPixel; // this for manual exposure
	//g_dwPclk = pRS0509_FpsSetting->dwPixelClk;		// this for scale speed
	GetSensorPclkHsync(HD1080P_FRM, byFps);
	g_wAECExposureRowMax = 1104-4;	// this for max exposure time
	g_wAEC_LineNumber = 1104;	// this for AE insert dummy line algothrim
	g_wSensorWidthBefBLC = 1920;
	g_wSensorHeightBefBLC = 1080;

	return TRUE;
}
#endif	//#ifdef _MIPI_EXIST_
#endif	//#ifdef _RS0509_SIZE_FHD

#ifdef RS0509_SIZE_HD
#ifdef _MIPI_EXIST_
/*
********************************************************************************
*										ConfigureResolutionHD
* FUNCTION ConfigureResolutionHD
********************************************************************************
*/
/**
  Configure RLE0509 Resolution HD.

  	# Example:
	# mipi_clk = Fref * (DSI_SSC_NC+2+DSI_SSC_FC/4096)/(DSI_CMU_PREDIV)/(DSI_CMU_PSTDV+2)
	# 		 = 12 *(28+2+0/4096) / = 360MHz
	# mipi_clk_lane = mipi_clock/2 = 180MHz

  	# Example:
	# fixed pattern model configuration
	# frame_sum =
	# (Frame_width��(speed_div+1)/(speed_mult+1)+dummy_pix) ��(frame_height + dummy_line)
	# (1280 *(0+1)/(0+1) + x )*( 720 + 24 )= 1500000
	# fps = tx_clk/frame_sum = mipi_clk/8/frame_sum = mipi_clk_lane*2/8/frame_sum
	#      =  360*10^6/8/ frame_sum= 30 ==> x = 656
  \param none

  \retval none.
********************************************************************************
*/
static U8 ConfigureResolutionHD(U8 byFps)
{
	U16 wTmp;
	OV_FpsSetting_t *pRS0509_FpsSetting;

	pRS0509_FpsSetting = GetOvFpsSetting(byFps, g_staRS0509HDFpsSetting, \
	                                     sizeof(g_staRS0509HDFpsSetting)/sizeof(OV_FpsSetting_t));

	//---------------------------------PLL MIPICLK PIXCLK----------------------------------
	//DSI_CMU_PREDIV = 1
	Write_SenReg(DSI_CMU_CTRL0, DSI_CMU_CP_7uA5|DSI_CMU_LDOREF_1V26|DSI_CMU_PREDIV_1);
	//DSI_CMU_POSTDIV
	if (DSI_CMU_POSTDIV_0 != pRS0509_FpsSetting->byClkrc)
	{
		Write_SenReg(DSI_CMU_CTRL1, (pRS0509_FpsSetting->byClkrc-2));
		Write_SenReg_Mask(DSI_POWER_CTRL0, DSI_CMU_SEL_POSTDIV ,DSI_CMU_SEL_POSTDIV);
	}
	//DSI_SSC_NC = 52
	Write_SenReg(DSI_SSC_NC_L, 28);	 //28=360MHZ
	Write_SenReg(DSI_SSC_NC_H, 0);
	//DIS_SSC_FC = 0
	Write_SenReg(DSI_SSC_FC_L, 0);
	Write_SenReg(DSI_SSC_FC_H, 0);

	//---------------------------------FIXP SETTING--------------------------------------
	//24 COLOR Setting
	Write_SenReg(COLOR_BLOCK_WIDTH_L, 0x9C);	//(1280-100x2-20x7)/6
	Write_SenReg(COLOR_BLOCK_HEIGHT_L, 0x69);	//(720-100x2-20x5)/4
	Write_SenReg(COLOR_BLOCK_WH_H, 0x00);
	//Frame Width = 0x0500= 1280
	Write_SenReg(FIXP_FRAME_WIDTH_H, 0x05);
	Write_SenReg(FIXP_FRAME_WIDTH_L, 0x00);
	//Frame Height = 0x2D0 = 720
	Write_SenReg(FIXP_FRAME_HEIGTH_H, 0x02);
	Write_SenReg(FIXP_FRAME_HEIGTH_L, 0xD0);

	wTmp = pRS0509_FpsSetting->wExtraDummyPixel - 1280;
	//Dummy Pixel = 0x92C = 2348 must have enough dummy pixel to insert FS/LP etc non-protocal state. and raw format translation.
	Write_SenReg(FIXP_DUMMY_PIXEL_H, INT2CHAR(wTmp, 1));
	Write_SenReg(FIXP_DUMMY_PIXEL_L, INT2CHAR(wTmp, 0));
	//Dummy Line = 0x018 = 24 frame num 0, always output fix pattern. Bug: If this is 0, the ssor will stop transfer images.
	Write_SenReg(FIXP_DUMMY_LINE_H, 0x00);
	Write_SenReg(FIXP_DUMMY_LINE_L, 0x18);
	//Speed = 0
	Write_SenReg(FIXP_SPEED_CTRL, 0x00);
	//Frame Num
	Write_SenReg(FIXP_FRAME_NUM, 0xFF);

	//---------------------------------MIPI IF SETTING--------------------------------------
	Write_SenReg(0x0303, 0x00);
	Write_SenReg(0x0304, 0x31);
	Write_SenReg(0x0305, 0x0C);
	Write_SenReg(0x0306, 0x06);
	Write_SenReg(0x0307, 0x0D);
	Write_SenReg(0x0308, 0x09);
	Write_SenReg(0x0309, 0x1D);
	Write_SenReg(0x030A, 0x01);
	Write_SenReg(0x030B, 0x09);
	Write_SenReg(0x030C, 0x09);
	Write_SenReg(0x030D, 0x09);
	Write_SenReg(0x030E, 0x05);

	Write_SenReg(MIPI_TX_DATA_TYPE, DATA_TYPE_RAW10);	//RAW10
	Write_SenReg(MIPI_TX_CONFIG, LANE0_EN);		// 1 Lane
	Write_SenReg(MIPI_TX_DATA_SRC, DATA_SRC_FIXP);
	Write_SenReg(MIPI_TX_ENABLE_CTRL, MIPI_TX_ENABLE);

	//step3.FIXP RUN
	Write_SenReg(FIXP_CTRL, FIXP_RUN);

	//g_wSensorHsyncWidth =  pRS0509_FpsSetting->wExtraDummyPixel; // this for manual exposure
	//g_dwPclk = pRS0509_FpsSetting->dwPixelClk;		// this for scale speed
	GetSensorPclkHsync(HD720P_FRM, byFps);
	//if (0x03 == (XBYTE[MIPI_TX_CONFIG]&0x03))		//RS0509 2 lane,mipi clk can't  halve, so merge ispclk need double
	//{
	//	g_dwPclk <<=1;
	//}
	g_wAECExposureRowMax = 744-4;	// this for max exposure time
	g_wAEC_LineNumber = 744;	// this for AE insert dummy line algothrim
	g_wSensorWidthBefBLC = 1280;
	g_wSensorHeightBefBLC = 720;

	//-----------------------------Dphy Setting--------------------------------
	SetMipiDphy(MIPI_DATA_LANE0_EN , \
	            MIPI_DATA_FORMAT_RAW10 , MIPI_DATA_TYPE_RAW10, \
	            HSTERM_EN_TIME_44);
	if (0x03 == (g_byMipiDphyCtrl&0x03))		//RS0509 2 lane,mipi clk can't  halve, so merge ispclk need double
	{
		g_dwPclk <<=1;
	}
	return TRUE;
}
#else
/*
********************************************************************************
*										ConfigureResolutionHD
* FUNCTION ConfigureResolutionHD
********************************************************************************
*/
/**
  Configure RLE0509 Resolution HD.

  	# Example:
	# mipi_clk = Fref * (DSI_SSC_NC+2+DSI_SSC_FC/4096)/(DSI_CMU_PREDIV)/(DSI_CMU_PSTDV+2)
	# 		 = 12 *(28+2+0/4096) / = 360MHz
	# mipi_clk_lane = mipi_clock/2 = 180MHz

  	# Example:
	# fixed pattern model configuration
	# frame_sum = 
	# (Frame_width��(speed_div+1)/(speed_mult+1)+dummy_pix) ��(frame_height + dummy_line)
	# (1280 *(0+1)/(0+1) + x )*( 720 + 24 )= 1500000
	# fps = tx_clk/frame_sum = mipi_clk/8/frame_sum = mipi_clk_lane*2/8/frame_sum
	#      =  360*10^6/8/ frame_sum= 30 ==> x = 656
  \param none

  \retval none.
********************************************************************************
*/
static U8 ConfigureResolutionHD(U8 byFps)
{
	U16 wTmp;
	OV_FpsSetting_t *pRS0509_FpsSetting;

	pRS0509_FpsSetting = GetOvFpsSetting(byFps, g_staRS0509HDFpsSetting, \
	                                     sizeof(g_staRS0509HDFpsSetting)/sizeof(OV_FpsSetting_t));

	//---------------------------------PLL MIPICLK PIXCLK----------------------------------
	//DSI_CMU_PREDIV = 1
	Write_SenReg(DSI_CMU_CTRL0, DSI_CMU_CP_7uA5|DSI_CMU_LDOREF_1V26|DSI_CMU_PREDIV_1);
	//DSI_CMU_POSTDIV
	if (DSI_CMU_POSTDIV_0 != pRS0509_FpsSetting->byClkrc)
	{
		Write_SenReg(DSI_CMU_CTRL1, (pRS0509_FpsSetting->byClkrc-2));
		Write_SenReg_Mask(DSI_POWER_CTRL0, DSI_CMU_SEL_POSTDIV ,DSI_CMU_SEL_POSTDIV);
	}
	//DSI_SSC_NC = 52
	Write_SenReg(DSI_SSC_NC_L, 28);	 //28=360MHZ
	Write_SenReg(DSI_SSC_NC_H, 0);
	//DIS_SSC_FC = 0
	Write_SenReg(DSI_SSC_FC_L, 0);
	Write_SenReg(DSI_SSC_FC_H, 0);

	//---------------------------------FIXP SETTING--------------------------------------
	//24 COLOR Setting
	Write_SenReg(COLOR_BLOCK_WIDTH_L, 0x9C);	//(1280-100x2-20x7)/6
	Write_SenReg(COLOR_BLOCK_HEIGHT_L, 0x69);	//(720-100x2-20x5)/4
	Write_SenReg(COLOR_BLOCK_WH_H, 0x00);
	//Frame Width = 0x0500= 1280
	Write_SenReg(FIXP_FRAME_WIDTH_H, 0x05);
	Write_SenReg(FIXP_FRAME_WIDTH_L, 0x00);
	//Frame Height = 0x2D0 = 720
	Write_SenReg(FIXP_FRAME_HEIGTH_H, 0x02);
	Write_SenReg(FIXP_FRAME_HEIGTH_L, 0xD0);

	wTmp = pRS0509_FpsSetting->wExtraDummyPixel - 1280;
	//Dummy Pixel = 0x92C = 2348 must have enough dummy pixel to insert FS/LP etc non-protocal state. and raw format translation.
	Write_SenReg(FIXP_DUMMY_PIXEL_H, INT2CHAR(wTmp, 1));
	Write_SenReg(FIXP_DUMMY_PIXEL_L, INT2CHAR(wTmp, 0));
	//Dummy Line = 0x018 = 24 frame num 0, always output fix pattern. Bug: If this is 0, the ssor will stop transfer images.
	Write_SenReg(FIXP_DUMMY_LINE_H, 0x00);
	Write_SenReg(FIXP_DUMMY_LINE_L, 0x18);
	//Speed = 0
	Write_SenReg(FIXP_SPEED_CTRL, 0x00);
	//Frame Num
	Write_SenReg(FIXP_FRAME_NUM, 0xFF);

	//---------------------------------DVP IF SETTING-------------------------------------
	Write_SenReg(DVP_FRAME_WIDTH_H, 0x05);	//1280
	Write_SenReg(DVP_FRAME_WIDTH_L, 0X00);
	Write_SenReg(DVP_FRAME_HEIGHT_H, 0x02);	//720
	Write_SenReg(DVP_FRAME_HEIGHT_L, 0xD0);
	Write_SenReg(DVP_DUMMY_LEFT_H, 0x00);
	Write_SenReg(DVP_DUMMY_LEFT_L, 0x00);
	Write_SenReg(DVP_DUMMY_RIGHT_H, 0x00);
	Write_SenReg(DVP_DUMMY_RIGHT_L, 0x00);
	Write_SenReg(DVP_DUMMY_BOTTOM_H, 0x00);
	Write_SenReg(DVP_DUMMY_BOTTOM_L, 0x00);
	Write_SenReg(DVP_DATA_LEFT_H, 0x00);
	Write_SenReg(DVP_DATA_LEFT_L, 0x00);
	Write_SenReg(DVP_DATA_RIGHT_H, 0x00);
	Write_SenReg(DVP_DATA_RIGHT_L, 0x00);
	Write_SenReg(DVP_DATA_BOTTOM_H, 0x00);
	Write_SenReg(DVP_DATA_BOTTOM_L, 0x00);
	Write_SenReg(DVP_VSYNC_CTRL, DVP_VSYNC_HIGH_ACTIVE);	//High Active
	Write_SenReg(DVP_HSYNC_CTRL, DVP_HSYNC_HIGH_ACTIVE);	//High Active
	Write_SenReg(DVP_PCLK_CTRL, DVP_FALINGEDGE_OUTPUT);		//PCLK Faling edge output data
	Write_SenReg(DVP_HSYNC_BREAK_H, 0x00);	//little value, Just toggle hsync and wait next valiad data
	Write_SenReg(DVP_HSYNC_BREAK_L, 0x0F);
	Write_SenReg(DVP_VSYNC_BREAK_H, 0x00);	//little value, Just toggle hsync and wait next valiad data
	Write_SenReg(DVP_VSYNC_BREAK_L, 0x0f) ;
	Write_SenReg(DVP_VHSYNC_BREAK_H, 0x00);
	Write_SenReg(DVP_VHSYNC_BREAK_L, 0x0F);	//little value, Just toggle hsync and wait next valiad data
	Write_SenReg(DVP_HVSYNC_BREAK_H, 0x00);
	Write_SenReg(DVP_HVSYNC_BREAK_L, 0x0F);	//little value, Just toggle hsync and wait next valiad data

	Write_SenReg(0x0111, 0x00);
	Write_SenReg(0x010f, 0x01);
	Write_SenReg(0x0110, 0x04);	//12mA smller than this value will have some problem

	Write_SenReg(DVP_CLK_CTRL, 0x81);	//only 0x81 is valid
	Write_SenReg(DVP_ENABLE_CTRL, DVP_EN);

	//step3. FIXP RUN
	Write_SenReg(FIXP_CTRL, FIXP_RUN);
	//g_wSensorHsyncWidth =  pRS0509_FpsSetting->wExtraDummyPixel; // this for manual exposure
	//g_dwPclk = pRS0509_FpsSetting->dwPixelClk;		// this for scale speed
	GetSensorPclkHsync(HD720P_FRM, byFps);
	g_wAECExposureRowMax = 744-4;	// this for max exposure time
	g_wAEC_LineNumber = 744;	// this for AE insert dummy line algothrim
	g_wSensorWidthBefBLC = 1280;
	g_wSensorHeightBefBLC = 720;
	return TRUE;
}
#endif	//ifdef _MIPI_EXIST_
#endif	//ifdef _RS0509_SIZE_HD

#ifdef RS0509_SIZE_UXGA
#ifdef _MIPI_EXIST_
/*
********************************************************************************
*										ConfigureResolutionUXGA
* FUNCTION ConfigureResolutionUXGA
********************************************************************************
*/
/**
  Configure RLE0509 Resolution UXGA.

  	# Example:
	# mipi_clk = Fref * (DSI_SSC_NC+2+DSI_SSC_FC/4096)/(DSI_CMU_PREDIV)/(DSI_CMU_PSTDV+2)
	# 		 = 12 *(50+2+0/4096) / = 624MHz
	# mipi_clk_lane = mipi_clock/2 = 264MHz

  	# Example:
	# fixed pattern model configuration
	# frame_sum =
	# (Frame_width��(speed_div+1)/(speed_mult+1)+dummy_pix) ��(frame_height + dummy_line)
	# (1600 *(0+1)/(0+1) + x )*( 1200 + 24 )= 2600000
	# fps = tx_clk/frame_sum = mipi_clk/8/frame_sum = mipi_clk_lane*2/8/frame_sum
	#      =  624*10^6/8/ frame_sum= 30 ==> x = 524
  \param none

  \retval none.
********************************************************************************
*/
static U8 ConfigureResolutionUXGA(U8 byFps)
{
	U16 wTmp;
	OV_FpsSetting_t *pRS0509_FpsSetting;

	pRS0509_FpsSetting = GetOvFpsSetting(byFps, g_staRS0509UXGAFpsSetting, \
	                                     sizeof(g_staRS0509UXGAFpsSetting)/sizeof(OV_FpsSetting_t));

	//---------------------------------PLL MIPICLK PIXCLK----------------------------------
	//DSI_CMU_PREDIV = 1
	Write_SenReg(DSI_CMU_CTRL0, DSI_CMU_CP_7uA5|DSI_CMU_LDOREF_1V26|DSI_CMU_PREDIV_1);
	//DSI_CMU_POSTDIV
	if (DSI_CMU_POSTDIV_0 != pRS0509_FpsSetting->byClkrc)
	{
		Write_SenReg(DSI_CMU_CTRL1, (pRS0509_FpsSetting->byClkrc-2));
		Write_SenReg_Mask(DSI_POWER_CTRL0, DSI_CMU_SEL_POSTDIV ,DSI_CMU_SEL_POSTDIV);
	}
	//DSI_SSC_NC = 50
	Write_SenReg(DSI_SSC_NC_L, 50);
	Write_SenReg(DSI_SSC_NC_H, 0);
	//DIS_SSC_FC = 0
	Write_SenReg(DSI_SSC_FC_L, 0);
	Write_SenReg(DSI_SSC_FC_H, 0);

	//---------------------------------FIXP SETTING--------------------------------------
	//24 COLOR Setting
	Write_SenReg(COLOR_BLOCK_WIDTH_L, 0xD2);	//(1600-100x2-20x7)/6 = 210
	Write_SenReg(COLOR_BLOCK_HEIGHT_L, 0xE1);	//(1200-100x2-20x5)/4 = 225
	Write_SenReg(COLOR_BLOCK_WH_H, 0x00);
	//Frame Width = 0x064 = 1600
	Write_SenReg(FIXP_FRAME_WIDTH_H, 0x06);
	Write_SenReg(FIXP_FRAME_WIDTH_L, 0x40);
	//Frame Height = 0x4B0 = 1200
	Write_SenReg(FIXP_FRAME_HEIGTH_H, 0x04);
	Write_SenReg(FIXP_FRAME_HEIGTH_L, 0xB0);

	wTmp = pRS0509_FpsSetting->wExtraDummyPixel - 1600;
	//Dummy Pixel = 0x92C = 2348 must have enough dummy pixel to insert FS/LP etc non-protocal state. and raw format translation.
	Write_SenReg(FIXP_DUMMY_PIXEL_H, INT2CHAR(wTmp, 1));
	Write_SenReg(FIXP_DUMMY_PIXEL_L, INT2CHAR(wTmp, 0));
	//Dummy Line = 0x018 = 24 frame num 0, always output fix pattern. Bug: If this is 0, the ssor will stop transfer images.
	Write_SenReg(FIXP_DUMMY_LINE_H, 0x00);
	Write_SenReg(FIXP_DUMMY_LINE_L, 0x18);
	//Speed = 0
	Write_SenReg(FIXP_SPEED_CTRL, 0x00);
	//Frame Num
	Write_SenReg(FIXP_FRAME_NUM, 0xFF);

	//---------------------------------MIPI IF SETTING--------------------------------------
	Write_SenReg(0x0303, 0x00);
	Write_SenReg(0x0304, 0x31);
	Write_SenReg(0x0305, 0x0C);
	Write_SenReg(0x0306, 0x06);
	Write_SenReg(0x0307, 0x0D);
	Write_SenReg(0x0308, 0x09);
	Write_SenReg(0x0309, 0x1D);
	Write_SenReg(0x030A, 0x01);
	Write_SenReg(0x030B, 0x09);
	Write_SenReg(0x030C, 0x09);
	Write_SenReg(0x030D, 0x09);
	Write_SenReg(0x030E, 0x05);

	Write_SenReg(MIPI_TX_DATA_TYPE, DATA_TYPE_RAW10);	//RAW10
	Write_SenReg(MIPI_TX_CONFIG, LANE0_EN|LANE1_EN);		// 2 Lane
	Write_SenReg(MIPI_TX_DATA_SRC, DATA_SRC_FIXP);
	Write_SenReg(MIPI_TX_ENABLE_CTRL, MIPI_TX_ENABLE);

	//step3.FIXP RUN
	Write_SenReg(FIXP_CTRL, FIXP_RUN);

	//g_wSensorHsyncWidth =  pRS0509_FpsSetting->wExtraDummyPixel; // this for manual exposure
	//g_dwPclk = pRS0509_FpsSetting->dwPixelClk;		// this for scale speed
	GetSensorPclkHsync(UXGA_FRM, byFps);
	//if (0x03 == (XBYTE[MIPI_TX_CONFIG]&0x03))		//RS0509 2 lane,mipi clk can't  halve, so merge ispclk need double
	//{
	//	g_dwPclk <<=1;
	//}
	g_wAECExposureRowMax = 1224-4;	// this for max exposure time
	g_wAEC_LineNumber = 1224;	// this for AE insert dummy line algothrim
	g_wSensorWidthBefBLC = 1600;
	g_wSensorHeightBefBLC = 1200;

	//-----------------------------Dphy Setting--------------------------------
	SetMipiDphy(MIPI_DATA_LANE0_EN | MIPI_DATA_LANE1_EN , \
	            MIPI_DATA_FORMAT_RAW10 , MIPI_DATA_TYPE_RAW10, \
	            HSTERM_EN_TIME_44);
	if (0x03 == (g_byMipiDphyCtrl&0x03))		//RS0509 2 lane,mipi clk can't  halve, so merge ispclk need double
	{
		g_dwPclk <<=1;
	}
	return TRUE;
}
#else
/*
********************************************************************************
*										ConfigureResolutionUXGA
* FUNCTION ConfigureResolutionUXGA
********************************************************************************
*/
/**
  Configure RLE0509 Resolution UXGA.

  	# Example:
	# mipi_clk = Fref * (DSI_SSC_NC+2+DSI_SSC_FC/4096)/(DSI_CMU_PREDIV)/(DSI_CMU_PSTDV+2)
	# 		 = 12 *(50+2+0/4096) / = 624MHz
	# mipi_clk_lane = mipi_clock/2 = 264MHz

  	# Example:
	# fixed pattern model configuration
	# frame_sum = 
	# (Frame_width��(speed_div+1)/(speed_mult+1)+dummy_pix) ��(frame_height + dummy_line)
	# (1600 *(0+1)/(0+1) + x )*( 1200 + 24 )= 2600000
	# fps = tx_clk/frame_sum = mipi_clk/8/frame_sum = mipi_clk_lane*2/8/frame_sum
	#      =  624*10^6/8/ frame_sum= 30 ==> x = 524
  \param none

  \retval none.
********************************************************************************
*/
static U8 ConfigureResolutionUXGA(U8 byFps)
{
	U16 wTmp;
	OV_FpsSetting_t *pRS0509_FpsSetting;

	pRS0509_FpsSetting = GetOvFpsSetting(byFps, g_staRS0509UXGAFpsSetting, \
	                                     sizeof(g_staRS0509UXGAFpsSetting)/sizeof(OV_FpsSetting_t));

	//---------------------------------PLL MIPICLK PIXCLK----------------------------------
	//DSI_CMU_PREDIV = 1
	Write_SenReg(DSI_CMU_CTRL0, DSI_CMU_CP_7uA5|DSI_CMU_LDOREF_1V26|DSI_CMU_PREDIV_1);
	//DSI_CMU_POSTDIV
	if (DSI_CMU_POSTDIV_0 != pRS0509_FpsSetting->byClkrc)
	{
		Write_SenReg(DSI_CMU_CTRL1, (pRS0509_FpsSetting->byClkrc-2));
		Write_SenReg_Mask(DSI_POWER_CTRL0, DSI_CMU_SEL_POSTDIV ,DSI_CMU_SEL_POSTDIV);
	}
	//DSI_SSC_NC = 50
	Write_SenReg(DSI_SSC_NC_L, 50);
	Write_SenReg(DSI_SSC_NC_H, 0);
	//DIS_SSC_FC = 0
	Write_SenReg(DSI_SSC_FC_L, 0);
	Write_SenReg(DSI_SSC_FC_H, 0);

	//---------------------------------FIXP SETTING--------------------------------------
	//24 COLOR Setting
	Write_SenReg(COLOR_BLOCK_WIDTH_L, 0xD2);	//(1600-100x2-20x7)/6 = 210
	Write_SenReg(COLOR_BLOCK_HEIGHT_L, 0xE1);	//(1200-100x2-20x5)/4 = 225
	Write_SenReg(COLOR_BLOCK_WH_H, 0x00);
	//Frame Width = 0x064 = 1600
	Write_SenReg(FIXP_FRAME_WIDTH_H, 0x06);
	Write_SenReg(FIXP_FRAME_WIDTH_L, 0x40);
	//Frame Height = 0x4B0 = 1200
	Write_SenReg(FIXP_FRAME_HEIGTH_H, 0x04);
	Write_SenReg(FIXP_FRAME_HEIGTH_L, 0xB0);

	wTmp = pRS0509_FpsSetting->wExtraDummyPixel - 1600;
	//Dummy Pixel = 0x92C = 2348 must have enough dummy pixel to insert FS/LP etc non-protocal state. and raw format translation.
	Write_SenReg(FIXP_DUMMY_PIXEL_H, INT2CHAR(wTmp, 1));
	Write_SenReg(FIXP_DUMMY_PIXEL_L, INT2CHAR(wTmp, 0));
	//Dummy Line = 0x018 = 24 frame num 0, always output fix pattern. Bug: If this is 0, the ssor will stop transfer images.
	Write_SenReg(FIXP_DUMMY_LINE_H, 0x00);
	Write_SenReg(FIXP_DUMMY_LINE_L, 0x18);
	//Speed = 0
	Write_SenReg(FIXP_SPEED_CTRL, 0x00);
	//Frame Num
	Write_SenReg(FIXP_FRAME_NUM, 0xFF);

	//---------------------------------DVP IF SETTING-------------------------------------
	Write_SenReg(DVP_FRAME_WIDTH_H, 0x06);	//1600
	Write_SenReg(DVP_FRAME_WIDTH_L, 0X40);
	Write_SenReg(DVP_FRAME_HEIGHT_H, 0x04);	//1200
	Write_SenReg(DVP_FRAME_HEIGHT_L, 0xB0);
	Write_SenReg(DVP_DUMMY_LEFT_H, 0x00);
	Write_SenReg(DVP_DUMMY_LEFT_L, 0x00);
	Write_SenReg(DVP_DUMMY_RIGHT_H, 0x00);
	Write_SenReg(DVP_DUMMY_RIGHT_L, 0x00);
	Write_SenReg(DVP_DUMMY_BOTTOM_H, 0x00);
	Write_SenReg(DVP_DUMMY_BOTTOM_L, 0x00);
	Write_SenReg(DVP_DATA_LEFT_H, 0x00);
	Write_SenReg(DVP_DATA_LEFT_L, 0x00);
	Write_SenReg(DVP_DATA_RIGHT_H, 0x00);
	Write_SenReg(DVP_DATA_RIGHT_L, 0x00);
	Write_SenReg(DVP_DATA_BOTTOM_H, 0x00);
	Write_SenReg(DVP_DATA_BOTTOM_L, 0x00);
	Write_SenReg(DVP_VSYNC_CTRL, DVP_VSYNC_HIGH_ACTIVE);	//High Active
	Write_SenReg(DVP_HSYNC_CTRL, DVP_HSYNC_HIGH_ACTIVE);	//High Active
	Write_SenReg(DVP_PCLK_CTRL, DVP_FALINGEDGE_OUTPUT);		//PCLK Faling edge output data
	Write_SenReg(DVP_HSYNC_BREAK_H, 0x00);	//little value, Just toggle hsync and wait next valiad data
	Write_SenReg(DVP_HSYNC_BREAK_L, 0x0F);
	Write_SenReg(DVP_VSYNC_BREAK_H, 0x00);	//little value, Just toggle hsync and wait next valiad data
	Write_SenReg(DVP_VSYNC_BREAK_L, 0x0f) ;
	Write_SenReg(DVP_VHSYNC_BREAK_H, 0x00);
	Write_SenReg(DVP_VHSYNC_BREAK_L, 0x0F);	//little value, Just toggle hsync and wait next valiad data
	Write_SenReg(DVP_HVSYNC_BREAK_H, 0x00);
	Write_SenReg(DVP_HVSYNC_BREAK_L, 0x0F);	//little value, Just toggle hsync and wait next valiad data

	Write_SenReg(0x0111, 0x00);
	Write_SenReg(0x010f, 0x01);
	Write_SenReg(0x0110, 0x04);	//12mA smller than this value will have some problem

	Write_SenReg(DVP_CLK_CTRL, 0x81);	//only 0x81 is valid
	Write_SenReg(DVP_ENABLE_CTRL, DVP_EN);

	//step3. FIXP RUN
	Write_SenReg(FIXP_CTRL, FIXP_RUN);
	//g_wSensorHsyncWidth =  pRS0509_FpsSetting->wExtraDummyPixel; // this for manual exposure
	//g_dwPclk = pRS0509_FpsSetting->dwPixelClk;		// this for scale speed
	GetSensorPclkHsync(UXGA_FRM, byFps);
	g_wAECExposureRowMax = 1224-4;	// this for max exposure time
	g_wAEC_LineNumber = 1224;	// this for AE insert dummy line algothrim
	g_wSensorWidthBefBLC = 1600;
	g_wSensorHeightBefBLC = 1200;

	return TRUE;
}
#endif
#endif

#ifdef RS0509_SIZE_SXGA
#ifdef _MIPI_EXIST_
/*
********************************************************************************
*										ConfigureResolutionSXGA
* FUNCTION ConfigureResolutionSXGA
********************************************************************************
*/
/**
  Configure RLE0509 Resolution SXGA.

  	# Example:
	# mipi_clk = Fref * (DSI_SSC_NC+2+DSI_SSC_FC/4096)/(DSI_CMU_PREDIV)/(DSI_CMU_PSTDV+2)
	# 		 = 12 *(36+2+0/4096) / = 456MHz
	# mipi_clk_lane = mipi_clock/2 = 228MHz

  	# Example:
	# fixed pattern model configuration
	# frame_sum =
	# (Frame_width��(speed_div+1)/(speed_mult+1)+dummy_pix) ��(frame_height + dummy_line)
	# (1280 *(0+1)/(0+1) + x )*( 1024 + 24 )= 1900000
	# fps = tx_clk/frame_sum = mipi_clk/8/frame_sum = mipi_clk_lane*2/8/frame_sum
	#      =  *456^6/8/ frame_sum= 30 ==> x = 246
  \param none

  \retval none.
********************************************************************************
*/
static U8 ConfigureResolutionSXGA(U8 byFps)
{
	U16 wTmp;
	OV_FpsSetting_t *pRS0509_FpsSetting;

	pRS0509_FpsSetting = GetOvFpsSetting(byFps, g_staRS0509SXGAFpsSetting, \
	                                     sizeof(g_staRS0509SXGAFpsSetting)/sizeof(OV_FpsSetting_t));

	//---------------------------------PLL MIPICLK PIXCLK----------------------------------
	//DSI_CMU_PREDIV = 1
	Write_SenReg(DSI_CMU_CTRL0, DSI_CMU_CP_7uA5|DSI_CMU_LDOREF_1V26|DSI_CMU_PREDIV_1);
	//DSI_CMU_POSTDIV
	if (DSI_CMU_POSTDIV_0 != pRS0509_FpsSetting->byClkrc)
	{
		Write_SenReg(DSI_CMU_CTRL1, (pRS0509_FpsSetting->byClkrc-2));
		Write_SenReg_Mask(DSI_POWER_CTRL0, DSI_CMU_SEL_POSTDIV ,DSI_CMU_SEL_POSTDIV);
	}
	//DSI_SSC_NC = 36
	Write_SenReg(DSI_SSC_NC_L, 36);
	Write_SenReg(DSI_SSC_NC_H, 0);
	//DIS_SSC_FC = 0
	Write_SenReg(DSI_SSC_FC_L, 0);
	Write_SenReg(DSI_SSC_FC_H, 0);

	//---------------------------------FIXP SETTING--------------------------------------
	//24 COLOR Setting
	Write_SenReg(COLOR_BLOCK_WIDTH_L, 0x9C);	//(1280-100x2-20x7)/6 = 156
	Write_SenReg(COLOR_BLOCK_HEIGHT_L, 0xB5);	//(1024-100x2-20x5)/4 = 181
	Write_SenReg(COLOR_BLOCK_WH_H, 0x00);
	//Frame Width = 0x0500 = 1280
	Write_SenReg(FIXP_FRAME_WIDTH_H, 0x05);
	Write_SenReg(FIXP_FRAME_WIDTH_L, 0x00);
	//Frame Height = 0x0400 = 1024
	Write_SenReg(FIXP_FRAME_HEIGTH_H, 0x04);
	Write_SenReg(FIXP_FRAME_HEIGTH_L, 0x00);

	wTmp = pRS0509_FpsSetting->wExtraDummyPixel - 1280;
	//Dummy Pixel = 0x92C = 2348 must have enough dummy pixel to insert FS/LP etc non-protocal state. and raw format translation.
	Write_SenReg(FIXP_DUMMY_PIXEL_H, INT2CHAR(wTmp, 1));
	Write_SenReg(FIXP_DUMMY_PIXEL_L, INT2CHAR(wTmp, 0));
	//Dummy Line = 0x018 = 24 frame num 0, always output fix pattern. Bug: If this is 0, the ssor will stop transfer images.
	Write_SenReg(FIXP_DUMMY_LINE_H, 0x00);
	Write_SenReg(FIXP_DUMMY_LINE_L, 0x18);
	//Speed = 0
	Write_SenReg(FIXP_SPEED_CTRL, 0x00);
	//Frame Num
	Write_SenReg(FIXP_FRAME_NUM, 0xFF);

	//---------------------------------MIPI IF SETTING--------------------------------------
	Write_SenReg(0x0303, 0x00);
	Write_SenReg(0x0304, 0x31);
	Write_SenReg(0x0305, 0x0C);
	Write_SenReg(0x0306, 0x06);
	Write_SenReg(0x0307, 0x0D);
	Write_SenReg(0x0308, 0x09);
	Write_SenReg(0x0309, 0x1D);
	Write_SenReg(0x030A, 0x01);
	Write_SenReg(0x030B, 0x09);
	Write_SenReg(0x030C, 0x09);
	Write_SenReg(0x030D, 0x09);
	Write_SenReg(0x030E, 0x05);

	Write_SenReg(MIPI_TX_DATA_TYPE, DATA_TYPE_RAW10);	//RAW10
	Write_SenReg(MIPI_TX_CONFIG, LANE0_EN|LANE1_EN);		// 2 Lane
	Write_SenReg(MIPI_TX_DATA_SRC, DATA_SRC_FIXP);
	Write_SenReg(MIPI_TX_ENABLE_CTRL, MIPI_TX_ENABLE);

	//step3.FIXP RUN
	Write_SenReg(FIXP_CTRL, FIXP_RUN);

	//g_wSensorHsyncWidth =  pRS0509_FpsSetting->wExtraDummyPixel; // this for manual exposure
	//g_dwPclk = pRS0509_FpsSetting->dwPixelClk;		// this for scale speed
	GetSensorPclkHsync(SXGA_FRM, byFps);
	//if (0x03 == (XBYTE[MIPI_TX_CONFIG]&0x03))		//RS0509 2 lane,mipi clk can't  halve, so merge ispclk need double
	//{
	//	g_dwPclk <<=1;
	//}
	g_wAECExposureRowMax = 1048-4;	// this for max exposure time
	g_wAEC_LineNumber = 1048;	// this for AE insert dummy line algothrim
	g_wSensorWidthBefBLC = 1280;
	g_wSensorHeightBefBLC = 1024;

	//-----------------------------Dphy Setting--------------------------------
	SetMipiDphy(MIPI_DATA_LANE0_EN | MIPI_DATA_LANE1_EN , \
	            MIPI_DATA_FORMAT_RAW10 , MIPI_DATA_TYPE_RAW10, \
	            HSTERM_EN_TIME_44);
	if (0x03 == (g_byMipiDphyCtrl&0x03))		//RS0509 2 lane,mipi clk can't  halve, so merge ispclk need double
	{
		g_dwPclk <<=1;
	}
	return TRUE;
}
#else
/*
********************************************************************************
*										ConfigureResolutionSXGA
* FUNCTION ConfigureResolutionSXGA
********************************************************************************
*/
/**
  Configure RLE0509 Resolution SXGA.

  	# Example:
	# mipi_clk = Fref * (DSI_SSC_NC+2+DSI_SSC_FC/4096)/(DSI_CMU_PREDIV)/(DSI_CMU_PSTDV+2)
	# 		 = 12 *(36+2+0/4096) / = 456MHz
	# mipi_clk_lane = mipi_clock/2 = 192MHz

  	# Example:
	# fixed pattern model configuration
	# frame_sum = 
	# (Frame_width��(speed_div+1)/(speed_mult+1)+dummy_pix) ��(frame_height + dummy_line)
	# (1280 *(0+1)/(0+1) + x )*( 1024 + 24 )= 1900000
	# fps = tx_clk/frame_sum = mipi_clk/8/frame_sum = mipi_clk_lane*2/8/frame_sum
	#      =  *456^6/8/ frame_sum= 30 ==> x = 246
  \param none

  \retval none.
********************************************************************************
*/
static U8 ConfigureResolutionSXGA(U8 byFps)
{
	U16 wTmp;
	OV_FpsSetting_t *pRS0509_FpsSetting;

	pRS0509_FpsSetting = GetOvFpsSetting(byFps, g_staRS0509SXGAFpsSetting, \
	                                     sizeof(g_staRS0509SXGAFpsSetting)/sizeof(OV_FpsSetting_t));

	//---------------------------------PLL MIPICLK PIXCLK----------------------------------
	//DSI_CMU_PREDIV = 1
	Write_SenReg(DSI_CMU_CTRL0, DSI_CMU_CP_7uA5|DSI_CMU_LDOREF_1V26|DSI_CMU_PREDIV_1);
	//DSI_CMU_POSTDIV
	if (DSI_CMU_POSTDIV_0 != pRS0509_FpsSetting->byClkrc)
	{
		Write_SenReg(DSI_CMU_CTRL1, (pRS0509_FpsSetting->byClkrc-2));
		Write_SenReg_Mask(DSI_POWER_CTRL0, DSI_CMU_SEL_POSTDIV ,DSI_CMU_SEL_POSTDIV);
	}
	//DSI_SSC_NC = 36
	Write_SenReg(DSI_SSC_NC_L, 36);
	Write_SenReg(DSI_SSC_NC_H, 0);
	//DIS_SSC_FC = 0
	Write_SenReg(DSI_SSC_FC_L, 0);
	Write_SenReg(DSI_SSC_FC_H, 0);

	//---------------------------------FIXP SETTING--------------------------------------
	//24 COLOR Setting
	Write_SenReg(COLOR_BLOCK_WIDTH_L, 0x9C);	//(1280-100x2-20x7)/6 = 156
	Write_SenReg(COLOR_BLOCK_HEIGHT_L, 0xB5);	//(1024-100x2-20x5)/4 = 181
	Write_SenReg(COLOR_BLOCK_WH_H, 0x00);
	//Frame Width = 0x0500 = 1280
	Write_SenReg(FIXP_FRAME_WIDTH_H, 0x05);
	Write_SenReg(FIXP_FRAME_WIDTH_L, 0x00);
	//Frame Height = 0x0400 = 1024
	Write_SenReg(FIXP_FRAME_HEIGTH_H, 0x04);
	Write_SenReg(FIXP_FRAME_HEIGTH_L, 0x00);

	wTmp = pRS0509_FpsSetting->wExtraDummyPixel - 1280;
	//Dummy Pixel = 0x92C = 2348 must have enough dummy pixel to insert FS/LP etc non-protocal state. and raw format translation.
	Write_SenReg(FIXP_DUMMY_PIXEL_H, INT2CHAR(wTmp, 1));
	Write_SenReg(FIXP_DUMMY_PIXEL_L, INT2CHAR(wTmp, 0));
	//Dummy Line = 0x018 = 24 frame num 0, always output fix pattern. Bug: If this is 0, the ssor will stop transfer images.
	Write_SenReg(FIXP_DUMMY_LINE_H, 0x00);
	Write_SenReg(FIXP_DUMMY_LINE_L, 0x18);
	//Speed = 0
	Write_SenReg(FIXP_SPEED_CTRL, 0x00);
	//Frame Num
	Write_SenReg(FIXP_FRAME_NUM, 0xFF);

	//---------------------------------DVP IF SETTING-------------------------------------
	Write_SenReg(DVP_FRAME_WIDTH_H, 0x05);	//1280
	Write_SenReg(DVP_FRAME_WIDTH_L, 0X00);
	Write_SenReg(DVP_FRAME_HEIGHT_H, 0x04);	//1024
	Write_SenReg(DVP_FRAME_HEIGHT_L, 0x00);
	Write_SenReg(DVP_DUMMY_LEFT_H, 0x00);
	Write_SenReg(DVP_DUMMY_LEFT_L, 0x00);
	Write_SenReg(DVP_DUMMY_RIGHT_H, 0x00);
	Write_SenReg(DVP_DUMMY_RIGHT_L, 0x00);
	Write_SenReg(DVP_DUMMY_BOTTOM_H, 0x00);
	Write_SenReg(DVP_DUMMY_BOTTOM_L, 0x00);
	Write_SenReg(DVP_DATA_LEFT_H, 0x00);
	Write_SenReg(DVP_DATA_LEFT_L, 0x00);
	Write_SenReg(DVP_DATA_RIGHT_H, 0x00);
	Write_SenReg(DVP_DATA_RIGHT_L, 0x00);
	Write_SenReg(DVP_DATA_BOTTOM_H, 0x00);
	Write_SenReg(DVP_DATA_BOTTOM_L, 0x00);
	Write_SenReg(DVP_VSYNC_CTRL, DVP_VSYNC_HIGH_ACTIVE);	//High Active
	Write_SenReg(DVP_HSYNC_CTRL, DVP_HSYNC_HIGH_ACTIVE);	//High Active
	Write_SenReg(DVP_PCLK_CTRL, DVP_FALINGEDGE_OUTPUT);		//PCLK Faling edge output data
	Write_SenReg(DVP_HSYNC_BREAK_H, 0x00);	//little value, Just toggle hsync and wait next valiad data
	Write_SenReg(DVP_HSYNC_BREAK_L, 0x0F);
	Write_SenReg(DVP_VSYNC_BREAK_H, 0x00);	//little value, Just toggle hsync and wait next valiad data
	Write_SenReg(DVP_VSYNC_BREAK_L, 0x0f) ;
	Write_SenReg(DVP_VHSYNC_BREAK_H, 0x00);
	Write_SenReg(DVP_VHSYNC_BREAK_L, 0x0F);	//little value, Just toggle hsync and wait next valiad data
	Write_SenReg(DVP_HVSYNC_BREAK_H, 0x00);
	Write_SenReg(DVP_HVSYNC_BREAK_L, 0x0F);	//little value, Just toggle hsync and wait next valiad data

	Write_SenReg(0x0111, 0x00);
	Write_SenReg(0x010f, 0x01);
	Write_SenReg(0x0110, 0x04);	//12mA smller than this value will have some problem

	Write_SenReg(DVP_CLK_CTRL, 0x81);	//only 0x81 is valid
	Write_SenReg(DVP_ENABLE_CTRL, DVP_EN);

	//step3. FIXP RUN
	Write_SenReg(FIXP_CTRL, FIXP_RUN);
	//g_wSensorHsyncWidth =  pRS0509_FpsSetting->wExtraDummyPixel; // this for manual exposure
	//g_dwPclk = pRS0509_FpsSetting->dwPixelClk;		// this for scale speed
	GetSensorPclkHsync(SXGA_FRM, byFps);
	g_wAECExposureRowMax = 1048-4;	// this for max exposure time
	g_wAEC_LineNumber = 1048;	// this for AE insert dummy line algothrim
	g_wSensorWidthBefBLC = 1280;
	g_wSensorHeightBefBLC = 1024;

	return TRUE;
}
#endif
#endif


void RS0509SetFormatFps(U16 wSetFormat, U8 byFps)
{
	byFps = byFps;
	wSetFormat = wSetFormat;

	TURN_RS0509_LED_ON(GPIO2_EN);

	//step 1. Power On
	PowerOnRS0509TX();
#ifdef RS0509_SIZE_QXGA
	if ((g_wSensorSPFormat&wSetFormat) ==  QXGA_FRM)
	{
		//step2. Resolution Setting
		ConfigureResolutionQXGA(byFps);
	}
	else
	{
		//step2. Resolution Setting
		ConfigureResolutionHD(byFps);
	}	
#endif
#ifdef RS0509_SIZE_UXGA
	if ((g_wSensorSPFormat&wSetFormat) ==  UXGA_FRM)
	{
		//step2. Resolution Setting
		ConfigureResolutionUXGA(byFps);
	}
#endif
#ifdef RS0509_SIZE_FHD
	if ((g_wSensorSPFormat&wSetFormat) ==  HD1080P_FRM)
	{
		//step2. Resolution Setting
		ConfigureResolutionFHD(byFps);
	}
#endif
#ifdef RS0509_SIZE_HD
	if ((g_wSensorSPFormat&wSetFormat) ==  HD720P_FRM)
	{
		//step2. Resolution Setting
		ConfigureResolutionHD(byFps);
	}
#endif
#ifdef RS0509_SIZE_SXGA
	if ((g_wSensorSPFormat&wSetFormat) ==  SXGA_FRM)
	{
		//step2. Resolution Setting
		ConfigureResolutionSXGA(byFps);
	}
#endif

}

void CfgRS0509ControlAttr(void)
{
	U8 i = 0;

	{
		memcpy(g_asOvCTT, gc_RS0509_CTT,sizeof(gc_RS0509_CTT));
	}

	{
		// hemonel 2010-09-08: support 1.8V sensor IO
#if (_CHIP_ID_ & _RTS5840_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_2V79;
#elif (_CHIP_ID_ & _RTS5832_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_2V81;
#elif (_CHIP_ID_ & _RTS5829B_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V82;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_2V81;
#endif
	}

#ifdef RS0509_SIZE_QXGA
	g_bySensorSize = SENSOR_SIZE_QXGA;
	g_wSensorSPFormat = QXGA_FRM|HD720P_FRM;
	g_aVideoFormat[0].byaVideoFrameTbl[0] = 2;
	g_aVideoFormat[0].byaVideoFrameTbl[1]= F_SEL_640_480;
	g_aVideoFormat[0].byaVideoFrameTbl[2]= F_SEL_2048_1536;

	g_aVideoFormat[0].byaStillFrameTbl[0] = 2;
	g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_640_480;
	g_aVideoFormat[0].byaStillFrameTbl[2]=F_SEL_2048_1536;
#endif

#ifdef RS0509_SIZE_FHD
	g_bySensorSize = SENSOR_SIZE_FHD;
	g_wSensorSPFormat = HD1080P_FRM;
	g_aVideoFormat[0].byaVideoFrameTbl[0] = 2;
	g_aVideoFormat[0].byaVideoFrameTbl[1]= F_SEL_640_480;
	g_aVideoFormat[0].byaVideoFrameTbl[2]= F_SEL_1920_1080;

	g_aVideoFormat[0].byaStillFrameTbl[0] = 2;
	g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_640_480;
	g_aVideoFormat[0].byaStillFrameTbl[2]=F_SEL_1920_1080;
#endif

#ifdef RS0509_SIZE_UXGA
	g_bySensorSize = SENSOR_SIZE_UXGA;
	g_wSensorSPFormat = UXGA_FRM;
	g_aVideoFormat[0].byaVideoFrameTbl[0] = 2;
	g_aVideoFormat[0].byaVideoFrameTbl[1]= F_SEL_640_480;
	g_aVideoFormat[0].byaVideoFrameTbl[2]= F_SEL_1600_1200;

	g_aVideoFormat[0].byaStillFrameTbl[0] = 2;
	g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_640_480;
	g_aVideoFormat[0].byaStillFrameTbl[2]=F_SEL_1600_1200;
#endif

#ifdef RS0509_SIZE_HD
	g_bySensorSize = SENSOR_SIZE_HD720P;
	g_wSensorSPFormat = HD720P_FRM;
	g_aVideoFormat[0].byaVideoFrameTbl[0] = 2;
	g_aVideoFormat[0].byaVideoFrameTbl[1]= F_SEL_640_480;
	g_aVideoFormat[0].byaVideoFrameTbl[2]= F_SEL_1280_720;

	g_aVideoFormat[0].byaStillFrameTbl[0] = 2;
	g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_640_480;
	g_aVideoFormat[0].byaStillFrameTbl[2]=F_SEL_1280_720;
#endif

#ifdef RS0509_SIZE_SXGA
	g_bySensorSize = SENSOR_SIZE_SXGA;
	g_wSensorSPFormat = SXGA_FRM;
	g_aVideoFormat[0].byaVideoFrameTbl[0] = 2;
	g_aVideoFormat[0].byaVideoFrameTbl[1]= F_SEL_640_480;
	g_aVideoFormat[0].byaVideoFrameTbl[2]= F_SEL_1280_1024;

	g_aVideoFormat[0].byaStillFrameTbl[0] = 2;
	g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_640_480;
	g_aVideoFormat[0].byaStillFrameTbl[2]=F_SEL_1280_1024;
#endif

	g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_640_480]= FPS_30 | FPS_25 | FPS_20 |FPS_15|FPS_10 |FPS_9 |FPS_8 |FPS_5;
	g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_1280_720]= FPS_9 |FPS_8 |FPS_5;
	g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_1280_1024]= FPS_9 |FPS_8 |FPS_5;
	g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_1600_1200]= FPS_5;
	g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_1920_1080]= FPS_5;
	g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_2048_1536]= FPS_3;
	g_aVideoFormat[0].byFormatType = FORMAT_TYPE_YUY2;

#ifdef _ENABLE_MJPEG_
	// copy YUY2 setting to MJPEG setting
	memcpy(&(g_aVideoFormat[1]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

	g_aVideoFormat[1].waFrameFpsBitmap[F_SEL_640_480]= FPS_30 | FPS_25 | FPS_20 |FPS_15|FPS_10 |FPS_9 |FPS_8 |FPS_5;

	g_aVideoFormat[1].waFrameFpsBitmap[F_SEL_1280_720]= FPS_30 | FPS_25 | FPS_20 |FPS_15|FPS_10 |FPS_9 |FPS_8 |FPS_5;
	g_aVideoFormat[1].waFrameFpsBitmap[F_SEL_1280_1024]= FPS_30 | FPS_25 | FPS_20 |FPS_15|FPS_10 |FPS_9 |FPS_8 |FPS_5;
	g_aVideoFormat[1].waFrameFpsBitmap[F_SEL_1600_1200]= FPS_30 | FPS_25 | FPS_20 |FPS_15|FPS_10 |FPS_9 |FPS_8 |FPS_5;
#ifdef _MIPI_EXIST_	
	g_aVideoFormat[1].waFrameFpsBitmap[F_SEL_1920_1080]= FPS_10 |FPS_9 |FPS_8 |FPS_5;
#else
	g_aVideoFormat[1].waFrameFpsBitmap[F_SEL_1920_1080]= FPS_30 | FPS_25 | FPS_20|FPS_15|FPS_10 |FPS_9 |FPS_8 |FPS_5;
#endif
	g_aVideoFormat[1].waFrameFpsBitmap[F_SEL_2048_1536]= FPS_15|FPS_10 |FPS_9 |FPS_8 |FPS_5|FPS_3;

	// modify MJPEG format type
	g_aVideoFormat[1].byFormatType = FORMAT_TYPE_MJPG;

#endif

	WhiteBalanceTempAutoItem.Last = 0;//last


	{
		//Brightness - BrightnessItem

		//Contrast - ContrastItem

		//Saturation - SaturationItem

		//Hue - 	HueItem

		//Sharpness - SharpnessItem
		//	SharpnessItem.Min = 0;
		//	SharpnessItem.Max = 8;
		//	SharpnessItem.Res = 1;
		//	SharpnessItem.Def = 3;
		//	SharpnessItem.Last  =SharpnessItem.Def;

		// gamma - 	GammaItem

		// gain - GainItem

		// whitebalance - WhiteBalanceTempItem

		// backlight - BackLightCompItem

		// powerline -PwrLineFreqItem
		//	PwrLineFreqItem.Def = PWR_LINE_FRQ_60;
		//	PwrLineFreqItem.Last = PwrLineFreqItem.Def;

		// exposure time -	ExposureTimeAbsolutItem

		// low light - LowLightCompItem
	}

	InitRS0509IspParams();
}
#endif
