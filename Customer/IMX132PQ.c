#include "Inc.h"

#ifdef RTS58XX_SP_IMX132PQ

U8 gc_digital_gain_h;
U8 gc_digital_gain_l;

void InitIMX132PQIspParams();

#define IMX132PQ_SIZE_720P_1LANE
//#define IMX132PQ_SIZE_1080P_1LANE
//#define IMX132PQ_SIZE_1080P_2LANE

#ifdef IMX132PQ_SIZE_720P_1LANE
// mipi 1lane 720p 30fps setting
code OV_FpsSetting_t  g_staIMX132PQFpsSetting[]=
{
//  FPS     ExtraDummyPixel clkrc 	pclk
	{5,		(5062),			(8),		20250000},
	{10,		(5062),			(4), 	40500000},
	{15,		(3375),			(4), 	40500000},
	{20,		(5062),			(2), 	81000000},
	{25,		(4050),			(2),		81000000},
	{30,		(3375),			(2),		81000000},
};

#else

code OV_FpsSetting_t  g_staIMX132PQFpsSetting[]=
{
//  FPS     ExtraDummyPixel clkrc 	pclk
	{5,		(7140),			(8),		40500000},
	{10,		(3570),			(8), 	40500000},
	{15,		(2380),			(8), 	40500000},
	{20,		(3570),			(4), 	81000000},
	{25,		(2856),			(4),		81000000},
	{30,		(2380),			(4),		81000000},
};

#endif

#ifdef IMX132PQ_SIZE_720P_1LANE
// mipi 1lane 720p 30fps setting
t_RegSettingWB code gc_IMX132PQ_Reg_Setting[] =
{
//	{0x0103, 0x01},

//	{0x0305, 0x02}, // PLL Input Clock Divided by 2
	{0x0307, 0x2d},	  // PLL Mult x67	 24MHz/2*45=540MHz

	{0x0101, 0x03},  // image_orientation
	{0x303E, 0x58},

	{0x0342, 0x0D},  //line_length_pck, 3375
	{0x0343, 0x2F},

	{0x0346, 0x00},  //y_addr_start, 28
	{0x0347, 0x1C},

	{0x034A, 0x04},   //x_addr_end, 1171
	{0x034B, 0x93},

	{0x034E, 0x02},   // y_output_size, 762
	{0x034F, 0xFA},

	{0x0385, 0x02},   //y_even_inc,  2
	{0x0387, 0x03},  // y_odd_inc, 3

	{0x3040, 0x08},
	{0x3041, 0x97},

	{0x3048, 0x22},
	{0x306A, 0x30},
	{0x309E, 0x45},
	{0x30A0, 0x11},

	{0x034C, 0x05},   //x_output_size, 1316
	{0x034D, 0x24},

	{0x0381, 0x02},   //x_even_inc, 2
	{0x0383, 0x03},   //x_odd_inc,3
	{0x30D5, 0x21},
	{0x304D, 0x03},
	{0x304C, 0x47},
	{0x3102, 0x07},
	{0x3103, 0x1B},
	{0x3105, 0x00},
	{0x3104, 0x0E},
	{0x3107, 0x00},
	{0x3106, 0x35},
	{0x315C, 0x37},
	{0x315D, 0x36},
	{0x316E, 0x38},
	{0x316F, 0x37},
	{0x3318, 0x64},
	{0x3301, 0x01},

	{0x0112, 0x0a}, //ccp_data_format, 0x0a0a for raw10
	{0x0113, 0x0a},

	{0x0340, 0x3},
	{0x0341, 0x20},

	{0x0202, 0x0},
	{0x0203, 0xD0},

// {0x0111, 0x1},  //data/clock signaling

//	{0x0100, 0x01},
};

#elif (defined IMX132PQ_SIZE_1080P_1LANE)

// mipi 1lane 1080p 30fps setting
t_RegSettingWB code gc_IMX132PQ_Reg_Setting[] =
{
//	{0x0103, 0x01},

// 	{0x0305, 0x04},	// PLL Input Clock Divided by 4      
	{0x0307,	0x87},	// PLL Mult x135	 24MHz/4*135=810MHz  

  	{0x0101, 0x03},

//	{0x0342, 0x08},  //line_length_pck, 2250
//	{0x0343, 0xCA},  

//	{0x0340, 0x4},   //frame_length_lines, 1200
//	{0x0341, 0xB0},  

	{0x303E,	0x5A},
	{0x3105,	0x00},
	{0x3104,	0x18},
 	{0x3107,	0x00},
	{0x3106,	0x65},
 	{0x3318,	0x61},
	{0x3301,	0x01},

	{0x0112, 0x0a}, //ccp_data_format, 0x0a0a for raw10
	{0x0113, 0x0a},

	{0x0340, 0x04},	//frame_length_lines, 1124
	{0x0341, 0x64},
	{0x0342, 0x09},	//line_length_pck, 2380
	{0x0343, 0x4c},
	{0x0344, 0x00},  //x_addr_start 0
	{0x0345, 0x00},
	{0x0346, 0x00},  //y_addr_start 16
	{0x0347, 0x1C},
	{0x0348, 0x07},  //x_addr_end  1975
	{0x0349, 0xB7},
	{0x034A, 0x04},  // y_addr_end
	{0x034B, 0x93},
	{0x034C, 0x07},  //x_output_size
	{0x034D, 0xB8},
	{0x034E, 0x04}, 	//y_output_size 1100
	{0x034F, 0x4c},

	{0x0202, 0x0},
	{0x0203, 0xD0},

// 	{0x0111, 0x1},  //data/clock signaling
//	{0x0100, 0x01},
};

#else 

// mipi 2lane 1080p 30fps setting
t_RegSettingWB code gc_IMX132PQ_Reg_Setting[] =
{
//	{0x0305, 0x04},	// PLL Input Clock Divided by 2
	{0x0307, 0x43},	// PLL Mult x67	 24MHz/2*67=804MHz  
	{0x30A4, 0x02},
	{0x303C, 0x4B},

	{0x3087, 0x53},     
	{0x308B, 0x5A},     
	{0x3094, 0x11},     
	{0x309D, 0xA4},     
	{0x30C6, 0x00},     
	{0x30C7, 0x00},     
	{0x3118, 0x2F},     
	{0x312A, 0x00},     
	{0x312B, 0x0B},     
	{0x312C, 0x0B},     
	{0x312D, 0x13}, 
	
	{0x3032, 0x40},     

	{0x0340, 0x04},	//frame_length_lines, 1124
	{0x0341, 0x64},
	{0x0342, 0x09},	//line_length_pck, 2380
	{0x0343, 0x4c},

	{0x0344, 0x00},  //x_addr_start 0
	{0x0345, 0x00},
	{0x0346, 0x00},  //y_addr_start 16
	{0x0347, 0x1C},
	{0x0348, 0x07},  //x_addr_end  1975
	{0x0349, 0xB7},
	{0x034A, 0x04},  // y_addr_end
	{0x034B, 0x93},
	{0x034C, 0x07},  //x_output_size
	{0x034D, 0xB8},
	{0x034E, 0x04}, 	//y_output_size 1100
	{0x034F, 0x4c},
/*

	{0x0340, 0x04},	//frame_length_lines, 1200
	{0x0341, 0xB0},
	{0x0342, 0x08},	//line_length_pck, 2248
	{0x0343, 0xC8},
	{0x0344, 0x00},  //x_addr_start
	{0x0345, 0x00},
	{0x0346, 0x00},  //y_addr_start
	{0x0347, 0x1C},
	{0x0348, 0x07},  //x_addr_end
	{0x0349, 0xB7},
	{0x034A, 0x04},  // y_addr_end
	{0x034B, 0x93},
	{0x034C, 0x07},  //x_output_size
	{0x034D, 0xB8},
	{0x034E, 0x04}, 	//y_output_size
	{0x034F, 0x78},
*/
	{0x0381, 0x01},
	{0x0383, 0x01},
	{0x0385, 0x01},
	{0x0387, 0x01},
	{0x303D, 0x10},
	{0x303E, 0x4A},
	{0x3048, 0x00},
	{0x304C, 0x2F},
	{0x304D, 0x02},
	{0x309B, 0x00},
	{0x309E, 0x41},
	{0x30A0, 0x10},
	{0x30A1, 0x0B},
	{0x30B2, 0x00},
	{0x30D5, 0x00},
	{0x30D6, 0x00},
	{0x30D7, 0x00},
	{0x30DE, 0x00},
	{0x3102, 0x0C},
	{0x3103, 0x33},
	{0x3104, 0x30},
	{0x3105, 0x00},
	{0x3106, 0xCA},
	{0x315C, 0x3D},
	{0x315D, 0x3C},
	{0x316E, 0x3E},
	{0x316F, 0x3D},
	{0x3301, 0x00},
	{0x3318, 0x61},

	{0x0202, 0x0},
	{0x0203, 0xD0},

	{0x0112, 0x0a}, //ccp_data_format, 0x0a0a for raw10
	{0x0113, 0x0a},

//	{0x0100, 0x01},
};	
#endif

OV_CTT_t code gc_IMX132PQ_CTT[3] =
{
	{3000,0x120,0x100,0x280},
	{4150,0x1a0,0x100,0x210},
	{6500,0x1e6,0x100,0x160},
};

U8 code gc_byYgamma_IMX132[16]= {0, 8, 16, 24, 32, 40, 48, 56, 64, 80, 96, 112, 128, 149, 179, 217}; //111014_PaoChi


static OV_FpsSetting_t*  GetOvFpsSetting(U8 Fps, OV_FpsSetting_t staOvFpsSetting[], U8 byArrayLen)
{
	U8 i;
	U8 Idx;

	ISP_MSG((" array size = %bd\n", byArrayLen));

	if(g_bIsHighSpeed)
	{
		Idx=2; //8fps for uxga, 10 fps for vga
	}
	else
	{
		Idx = 0;
	}

	for(i=0; i< byArrayLen; i++)
	{
		ISP_MSG((" fps i = %bd\n",i));
		if(staOvFpsSetting[i].byFps ==Fps)
		{
			Idx=i;
			break;
		}
	}
	ISP_MSG((" get fps idx = %bd\n",Idx));
	return &staOvFpsSetting[Idx];
}

static U16 MapSnrGlbGain2SnrRegSetting(U16 wGain)
{
	U16  sensorGain=0;
	float temp;

	if(wGain>128)    //Use Analog Gain here 128=8*16
	{
		sensorGain = 256 - (U16)(4096/(U16)128);

		//set digital gain of sensor
		temp = (float)wGain /(float)128;
		gc_digital_gain_h = (U8)temp;
		gc_digital_gain_l = (temp-(float)gc_digital_gain_h)*(float)256;
		//XBYTE[ISP_AE_GAIN_CTRL] |= 0X80;
	}
	else//max analog gain is 8 times
	{
		sensorGain = 256 - (U16)(4096/(U16)wGain);
		gc_digital_gain_h=1;
		gc_digital_gain_l = 0;
		//XBYTE[ISP_AE_GAIN_CTRL] &= 0X7F;
	}

	// lower byte output analog gain
	// high byte output digital gain
	return sensorGain;
}

float SnrRegSetting2SnrGlbGain(U16 wSnrRegGain)
{
	return 256.0/(256.0-(float)wSnrRegGain)*(float)((gc_digital_gain_h<<8)|gc_digital_gain_l)/256.0;
}

//this function is also called during Probe stage to calculate the transfer_size/LWM/HWM
void GetSensorPclkHsync(U16 wSensorSPFormat,U8 byFps)
{
	OV_FpsSetting_t *pIMX132PQ_FpsSetting= NULL;

	wSensorSPFormat = wSensorSPFormat;
	
	pIMX132PQ_FpsSetting=GetOvFpsSetting(byFps, g_staIMX132PQFpsSetting, sizeof(g_staIMX132PQFpsSetting)/sizeof(OV_FpsSetting_t));			

	g_wSensorHsyncWidth = pIMX132PQ_FpsSetting->wExtraDummyPixel; // this for manual exposure
	g_dwPclk = pIMX132PQ_FpsSetting->dwPixelClk;		// this for scale speed
}

void IMX132PQSetFormatFps(U16 SetFormat, U8 Fps)
{
	U16 wTemp;
	OV_FpsSetting_t *pIMX132PQ_FpsSetting;

	// hemonel 2010-04-13: for delete complier warning
	SetFormat = SetFormat;

	pIMX132PQ_FpsSetting=GetOvFpsSetting(Fps, g_staIMX132PQFpsSetting, sizeof(g_staIMX132PQFpsSetting)/sizeof(OV_FpsSetting_t));

	Write_SenReg(0x0103, 0x01);

	// 1) change hclk if necessary
	Write_SenReg(0x0305, pIMX132PQ_FpsSetting->byClkrc);

	// initial all register setting
	WriteSensorSettingWB(sizeof(gc_IMX132PQ_Reg_Setting)/3, gc_IMX132PQ_Reg_Setting);

	wTemp = pIMX132PQ_FpsSetting->wExtraDummyPixel;
	Write_SenReg(0x0343, INT2CHAR(wTemp, 0)); //Write dummy pixel LSB
	Write_SenReg(0x0342, INT2CHAR(wTemp, 1)); //Write dummy pixel MSB

	Write_SenReg(0x0100, 0x01);

	// 3) update variable for AE
	// line_length_pck
	//g_wSensorHsyncWidth = pIMX132PQ_FpsSetting->wExtraDummyPixel;// this for manual exposure

#ifdef IMX132PQ_SIZE_720P_1LANE
	// frame_length_lines
	g_wAEC_LineNumber = 720;// this for AE insert dummy line algothrim

	// backend isp receive widow width and height
	g_wSensorWidthBefBLC = 1280;
	g_wSensorHeightBefBLC = 720;

	// sensor register limit, frmae_length_lines-n(n from sensor spec definition)
	g_wAECExposureRowMax = 716; 	// this for max exposure time<g_wAEC_LineNumber

	// clock that transfer one pixel data
	//g_dwPclk = pIMX132PQ_FpsSetting->dwPixelClk;


#elif (defined IMX132PQ_SIZE_1080P_1LANE)

	// frame_length_lines
	g_wAEC_LineNumber = 1200;// this for AE insert dummy line algothrim

	// backend isp receive widow width and height
	g_wSensorWidthBefBLC = 1920;
	g_wSensorHeightBefBLC = 1080;

	// sensor register limit, frmae_length_lines-n(n from sensor spec definition)
	g_wAECExposureRowMax = 1196; 	// this for max exposure time<g_wAEC_LineNumber

	// clock that transfer one pixel data
	//g_dwPclk = pIMX132PQ_FpsSetting->dwPixelClk;

#elif (defined IMX132PQ_SIZE_1080P_2LANE)
	// frame_length_lines
	g_wAEC_LineNumber = 1200;// this for AE insert dummy line algothrim

	// backend isp receive widow width and height
	g_wSensorWidthBefBLC = 1920;
	g_wSensorHeightBefBLC = 1080;

	// sensor register limit, frmae_length_lines-n(n from sensor spec definition)
	g_wAECExposureRowMax = 1196; 	// this for max exposure time<g_wAEC_LineNumber

	// clock that transfer one pixel data
	//g_dwPclk = pIMX132PQ_FpsSetting->dwPixelClk;

#endif

	GetSensorPclkHsync(SetFormat,Fps);

#ifdef IMX132PQ_SIZE_720P_1LANE
	SetMipiDphy( MIPI_DATA_LANE0_EN, MIPI_DATA_FORMAT_RAW10, MIPI_DATA_TYPE_RAW10, HSTERM_EN_TIME_33);
#elif (defined IMX132PQ_SIZE_1080P_1LANE)
	SetMipiDphy( MIPI_DATA_LANE0_EN, MIPI_DATA_FORMAT_RAW10, MIPI_DATA_TYPE_RAW10, HSTERM_EN_TIME_33);
#elif (defined IMX132PQ_SIZE_1080P_2LANE)
	SetMipiDphy( MIPI_DATA_LANE0_EN | MIPI_DATA_LANE1_EN, MIPI_DATA_FORMAT_RAW10, MIPI_DATA_TYPE_RAW10, HSTERM_EN_TIME_44);
#endif

	return;
}

void CfgIMX132PQControlAttr(void)
{
	U8 i;

#ifdef IMX132PQ_SIZE_720P_1LANE
	g_bySensorSize = SENSOR_SIZE_HD720P;
	g_wSensorSPFormat = HD720P_FRM;
#else
	g_bySensorSize = SENSOR_SIZE_FHD;
	g_wSensorSPFormat = HD1080P_FRM;
#endif

	memcpy(g_asOvCTT, gc_IMX132PQ_CTT,sizeof(gc_IMX132PQ_CTT));

	//if(g_byBKSensorPwrSettingExist==0)  //sensor power setting
	{
#if (_CHIP_ID_ & _RTS5840_)
	XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
	XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V79;
#elif (_CHIP_ID_ & _RTS5832_)
	XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
	XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V85;
#elif (_CHIP_ID_ & _RTS5829B_)
	XBYTE[REG_TUNESVA] =  SVA_VOL_2V82;
	XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V80;
#endif

		// Power On Sequenc VDDC-->SVA-->SVIO to meet spec
	}

	//if(g_byFormatCfgExist==0) // format config not exist, use default setting.
	{
		//====== resolution  setting ===========
		// -- preview ---
#ifndef IMX132PQ_SIZE_720P_1LANE
		g_aVideoFormat[0].byaVideoFrameTbl[0] = 9;		// resolution number
#else
		g_aVideoFormat[0].byaVideoFrameTbl[0] = 7;		// resolution number
#endif
		g_aVideoFormat[0].byaVideoFrameTbl[1]=F_SEL_640_480;
		g_aVideoFormat[0].byaVideoFrameTbl[2]=F_SEL_160_120;
		g_aVideoFormat[0].byaVideoFrameTbl[3]=F_SEL_176_144;
		g_aVideoFormat[0].byaVideoFrameTbl[4]=F_SEL_320_240;
		g_aVideoFormat[0].byaVideoFrameTbl[5]=F_SEL_352_288;
		g_aVideoFormat[0].byaVideoFrameTbl[6]=F_SEL_640_400;
		g_aVideoFormat[0].byaVideoFrameTbl[7]=F_SEL_1280_720;
#ifndef IMX132PQ_SIZE_720P_1LANE
		g_aVideoFormat[0].byaVideoFrameTbl[8]=F_SEL_1280_800;
		g_aVideoFormat[0].byaVideoFrameTbl[9]=F_SEL_1920_1080;
#endif
		//--still image--
#ifndef IMX132PQ_SIZE_720P_1LANE
		g_aVideoFormat[0].byaStillFrameTbl[0] = 5;	// resolution number
		g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_1920_1080;
#else
		g_aVideoFormat[0].byaStillFrameTbl[0] = 4;	// resolution number
#endif
		g_aVideoFormat[0].byaStillFrameTbl[2]=F_SEL_1280_720;
		g_aVideoFormat[0].byaStillFrameTbl[3]=F_SEL_160_120;
		g_aVideoFormat[0].byaStillFrameTbl[4]=F_SEL_320_240;
		g_aVideoFormat[0].byaStillFrameTbl[5]=F_SEL_640_480;

		//====== fps  setting ===========
		{
			for(i=0; i<10; i++)
			{
				/*
					F_SEL_640_480    (0)
					F_SEL_160_120    (1)
					F_SEL_176_144    (2)
					F_SEL_320_180	(3)
					F_SEL_320_200    (4)
					F_SEL_320_240    (5)
					F_SEL_352_288    (6)
					F_SEL_424_240	(7)
					F_SEL_640_360	(8)
					F_SEL_640_400    (9)
				*/
				g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_15|FPS_20|FPS_25|FPS_30;
			}
			for(i=10; i<18; i++)
			{
				/*
					F_SEL_800_600    (10)
					F_SEL_848_480	(11)
					F_SEL_960_540	 (12)
					F_SEL_1024_768   (13)
					F_SEL_1280_720   (14)
					F_SEL_1280_800   (15)
					F_SEL_1280_960   (16)
					F_SEL_1280_1024  (17)
				*/
				g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_10|FPS_5;
			}
		}

		//====== format type  setting ===========
		g_aVideoFormat[0].byFormatType = FORMAT_TYPE_YUY2;

#ifdef _ENABLE_MJPEG_
		// copy YUY2 setting to MJPEG setting
		memcpy(&(g_aVideoFormat[1]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

		// modify MJPEG FPS setting
		for(i=7; i<16; i++)
		{
			g_aVideoFormat[1].waFrameFpsBitmap[i] = (FPS_5|FPS_20|FPS_25|FPS_30);
		}

		// modify MJPEG format type
		g_aVideoFormat[1].byFormatType = FORMAT_TYPE_MJPG;

#endif

#ifdef _ENABLE_M420_FMT_
		// copy YUY2 setting to M420 setting
		memcpy(&(g_aVideoFormat[2]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

		// modify M420 FPS setting
		g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_800_600] =FPS_15|FPS_10|FPS_5;//|FPS_1;             //bit07 F_800_600
		g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_960_540] =FPS_15|FPS_10|FPS_5;//|FPS_5|FPS_3|FPS_1;                    //bit08 F_960_720
		g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1024_768] =FPS_15|FPS_10|FPS_5;//|FPS_1;                    //bit09 F_1024_768
		g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_720] =FPS_11|FPS_5;//FPS_9|FPS_8|FPS_1;    //bit10 F_1280_720

		// modify M420 format type
		g_aVideoFormat[2].byFormatType = FORMAT_TYPE_M420;
#endif
	}

	//if(g_byControlAttrExistFlag==0)
	{
		//Brightness - BrightnessItem

		//Contrast - ContrastItem

		//Saturation - SaturationItem

		//Hue - 	HueItem

		//Sharpness - SharpnessItem
		//	SharpnessItem.Min = 0;
		//	SharpnessItem.Max = 8;
		//	SharpnessItem.Res = 1;
		//	SharpnessItem.Def = 3;
		//	SharpnessItem.Last  =SharpnessItem.Def;

		// gamma - 	GammaItem

		// gain - GainItem

		// whitebalance - WhiteBalanceTempItem

		// backlight - BackLightCompItem

		// powerline -PwrLineFreqItem

		// exposure time -	ExposureTimeAbsolutItem

		// low light - LowLightCompItem
	}

	InitIMX132PQIspParams();
}

void SetIMX132PQIntegrationTime(U16 wEspline)
{
	//-----------Write Exposuretime setting-----------
	Write_SenReg(0x0203, INT2CHAR(wEspline, 0));	// change exposure value low
	Write_SenReg(0x0202, INT2CHAR(wEspline, 1));	// change exposure value high

	//----------- write gain setting-------------------
	Write_SenReg(0x0205, 0x0f);	// fixed gain at manual exposure control
	Write_SenReg(0x0204, 0);
}

void SetIMX132PQGain(float fGain)
{
}

void SetIMX132PQImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.
{
	bySnrImgDir ^= 0x03;
	Write_SenReg_Mask(0x0101, bySnrImgDir, 0x03);

	switch(bySnrImgDir)
	{
	case 0:
		SetBLCWindowStart(1, 0); //00, 01, 11
		break;
	case 1:
		SetBLCWindowStart(0, 0); //10, 11, 01, 00
		break;
	case 2:
		SetBLCWindowStart(1, 1);
		break;
	case 3:
		SetBLCWindowStart(0, 1);
		break;
	default:
		break;
	}
}

// wExpTime: unit with 1/10000 seconds
// byGain: unit with 1/16
void SetIMX132PQExposuretime_Gain(float fExpTime, float fTotalGain)
{
	U16 wExposureRows_floor;
	U16  wGainRegSetting;
	float fSnrGlbGain;
	float fExposureRows;
	U16 wSetDummy;

	wGainRegSetting=MapSnrGlbGain2SnrRegSetting((U16)(fTotalGain*16.0));
	fSnrGlbGain = SnrRegSetting2SnrGlbGain(wGainRegSetting);

	fExposureRows = fExpTime/g_fSensorRowTimes;	//unit : 1row
	wExposureRows_floor = (U16)(fExposureRows);

	// set dummy
	wSetDummy = g_wAFRInsertDummylines + g_wAEC_LineNumber;
	if (wSetDummy%2 == 1)
	{
		wSetDummy++;
	}

	// write exposure, gain, dummy line
	if(fabs(g_fCurExpTime-fExpTime) > 0.0001)
	{
		// group register hold
		Write_SenReg(0x0104, 0x01);

		//-----------Write  frame length or dummy lines------------
		Write_SenReg(0x0341, INT2CHAR(wSetDummy, 0));
		Write_SenReg(0x0340, INT2CHAR(wSetDummy, 1));
		
		//-----------Write Exposuretime setting-----------
		Write_SenReg(0x0203, INT2CHAR(wExposureRows_floor, 0));	// change exposure value low
		Write_SenReg(0x0202, INT2CHAR(wExposureRows_floor, 1));	// change exposure value high

		//----------- write gain setting-------------------
		Write_SenReg(0x0205, INT2CHAR(wGainRegSetting, 0));
		Write_SenReg(0x0204, INT2CHAR(wGainRegSetting, 1));

		//set digital gain
		Write_SenReg(0x20E,gc_digital_gain_h);
		Write_SenReg(0x20F,gc_digital_gain_l);

		Write_SenReg(0x210,gc_digital_gain_h);
		Write_SenReg(0x211,gc_digital_gain_l);

		Write_SenReg(0x212,gc_digital_gain_h);
		Write_SenReg(0x213,gc_digital_gain_l);

		Write_SenReg(0x214,gc_digital_gain_h);
		Write_SenReg(0x215,gc_digital_gain_l);

		SetISPAEGain(fTotalGain, fSnrGlbGain, 1);

		// group register release
		Write_SenReg(0x0104, 0);
		g_fCurExpTime = fExpTime;	// back up exposure rows
	}
	else
	{
		// group register hold
		Write_SenReg(0x0104, 1);

		//-----------Write  frame length or dummy lines------------
		Write_SenReg(0x0341, INT2CHAR(wSetDummy, 0));
		Write_SenReg(0x0340, INT2CHAR(wSetDummy, 1));
		
		// write gain setting
		Write_SenReg(0x0205, INT2CHAR(wGainRegSetting, 0));
		Write_SenReg(0x0204, INT2CHAR(wGainRegSetting, 1));

		//set digital gain
		Write_SenReg(0x20E,gc_digital_gain_h);
		Write_SenReg(0x20F,gc_digital_gain_l);

		Write_SenReg(0x210,gc_digital_gain_h);
		Write_SenReg(0x211,gc_digital_gain_l);

		Write_SenReg(0x212,gc_digital_gain_h);
		Write_SenReg(0x213,gc_digital_gain_l);

		Write_SenReg(0x214,gc_digital_gain_h);
		Write_SenReg(0x215,gc_digital_gain_l);

		SetISPAEGain(fTotalGain, fSnrGlbGain, 1);

		// group register release
		Write_SenReg(0x0104, 0);
	}
	return;
}

void InitIMX132PQIspParams()
{
	int i;

	g_wAWBRGain_Last= 0x17C;
	g_wAWBGGain_Last= 0x100;
	g_wAWBBGain_Last= 0x226;
	g_wProjectGR = g_wAWBRGain_Last;
	g_wProjectGB = g_wAWBBGain_Last;

	for(i=0; i<16; i++)
	{
		g_Ygamma[i]=gc_byYgamma_IMX132[i];
	}

	// Special ISP
	g_bySaturation_Def =68;
	g_byContrast_Def = 33; //Neil Tuning at chicony

	g_wDynamicISPEn = DYNAMIC_LSC_EN | DYNAMIC_SHARPPARAM_EN | 
		DYNAMIC_SHARPNESS_EN | DYNAMIC_CCM_BRIGHT_EN;
}

void IMX132PQ_POR()
{
#ifdef _MIPI_EXIST_
	Init_MIPI();
#else
	Init_CCS();
#endif

	ENTER_SENSOR_RESET();

	// power on
	SensorPowerControl(SWITCH_ON,EN_DELAYTIME);
	uDelay(1);

	// Initialize I2C Controller Timing
	LEAVE_SENSOR_RESET();

	// clock select 24M
	if( g_bySSCEnable== TRUE)
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_SSC_24M);
	}
	else
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_96M|CCS_CLK_DIVIDER_4);
	}

	EnableSensorHCLK();

	g_bySensorIsOpen = SENSOR_OPEN;

	WaitTimeOut_Delay(1);
}

void SetIMX132PQDynamicISP(U8 byAEC_Gain)
{
	byAEC_Gain = byAEC_Gain;
}

#endif
