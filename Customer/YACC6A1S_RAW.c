#include "Inc.h"

#ifdef RTS58XX_SP_YACC6A1S

OV_CTT_t code gc_YACC6A1S_CTT[3] =
{
	{2800,192,256,392},
	{4117,256,256,320},
	{6790,280,256,232},
};

// HI161 FPS timing:
//		1280x1024 Frame time = (1072+VSYNC)*(1320+HBLANK)*To, To is Pixel clock cycle time
//		1280x960   Frame time = (992+VSYNC)*(1320+HBLANK)*To //(1008+VSYNC)*(1320+HBLANK)*To
//		1280x720   Frame time = (768+VSYNC)*(1320+HBLANK)*To
//		VSYNC register page0 0x42,0x43, HBLANK register page0 0x40,0x41
//		VSYNC min is 4, do not change this register
/*
code OV_FpsSetting_t  g_staYACC6A1SFpsSetting_720P[]=
{
//==========redck=============================
// FPS     ExtraDummyPixel   clkrc 	   pclk

      {10,		(1320+280),	0x71,	15000000  },
      {30,		(1320+280),	0x74,	37500000 },

//========================================
 };

code OV_FpsSetting_t  g_staYACC6A1SFpsSetting_800P[]=
{
//==========redck=============================
// FPS     ExtraDummyPixel   clkrc 	   pclk
	{7,		(1320+280),		0x71,	   18000000},
	{30,		(1320+280),		0x77,	   48000000},

//========================================
 };
*/
// line_length_pck = PCLK/(fps* (992+4)) = PCLK/(fps*996)
code OV_FpsSetting_t  g_staYACC6A1SFpsSetting_960P[]=
{
// 	FPS     ExtraDummyPixel   clkrc 	   pclk
	{5,		3012,		0x71,	    15000000},
	{9,		2508,		0x72,	    22500000},
	{10,		2256,		0x72,	    22500000},
	{15,		2008,		0x73,	    30000000},
	{20,		1880,		0x74,	    37500000},
	{25,		1804,		0x75,	    45000000},
	{30,		1756,		0x76,	    52500000},
};

// line_length_pck = PCLK/(fps* (1072+4)) = PCLK/(fps*1076)
code OV_FpsSetting_t  g_staYACC6A1SFpsSetting_SXGA[]=
{
//==========redck=============================
// FPS     ExtraDummyPixel   clkrc 	   pclk
	{5,		2788,		0x71,	    15000000},
	{9,		2322,		0x72,	    22500000},
	{10,		2090,		0x72,	    22500000},
	{15,		1858,		0x73,	    30000000},
	{20,		1742,		0x74,	    37500000},
	{25,		1672,		0x75,	    45000000},
	{30,		1626,		0x76,	  	52500000},
//========================================
};

// sensor setting
t_RegSettingBB code gc_YACC6A1S_Setting[]=
{
	// Bayer Default Setting , DPC ON setting
	//PAGE0
	{0x01,0x01},
	{0x01,0x03},
	{0x01,0x01},

	{0x03,0x20},  //page 20
	{0x10,0x0c},  //ae off
	{0x03,0x22},  //page 22
	{0x10,0x69},  //awb off

	////////////////////////////// 0 Page MODE CTL

	{0x03,0x00},
	{0x0e,0x75},  //PLL 0x73 x2 0x74 x2.5 0x75 x3
	{0x10,0x04},  //pre3 auto window
	{0x11,0x90},  //NO Flip
	{0x12,0x00},
	{0x20,0x00},
	{0x21,0x0A},
	{0x22,0x00},
	{0x23,0x0A},

	{0x24,0x04},
	{0x25,0x00},
	{0x26,0x05},
	{0x27,0x00},

	{0x38,0x02},
	{0x39,0x0A},
	{0x3A,0x01},
	{0x3B,0x55},
	{0x3C,0x05},
	{0x3D,0x00},

	{0x40,0x01}, //Hblank_280
	{0x41,0x18},
	{0x42,0x00}, //VSYNK_20
	{0x43,0x04}, //35->4 for 30fps 53 for 20 fps

	////BLC
	{0x80,0x08},  //100408
	{0x81,0x0f},  //100408
	{0x82,0x90},  //100408
	{0x83,0x00},
	{0x84,0xcc},  //100408
	{0x85,0x00},
	{0x86,0xd4},  //100408

	{0x90,0x0e},  //8.57 fps
	{0x91,0x0f},  //8 fps
	{0x92,0x39},  // 20100429 by richard
	{0x93,0x38},  //091202  50-->93
	{0x94,0x95}, //091202
	{0x95,0x90}, //091202
	{0x98,0x38},  //20

	//Dark BLC
	{0xa0,0x01},  // 20100309
	{0xa2,0x01},  // 20100309
	{0xa4,0x01},  // 20100309
	{0xa6,0x01},  // 20100309

	//Normal BLC
	{0xa8,0x00},
	{0xaa,0x00},
	{0xac,0x00},
	{0xae,0x00},

	//Out  BLC
	{0x99,0x00},
	{0x9a,0x00},
	{0x9b,0x00},
	{0x9c,0x00},

	////////////////////////////// 2 Page Analog
	{0x03,0x02},
	{0x18,0x2c},
	{0x19,0x00},
	{0x1a,0x39}, //100224 09 -> 39
	{0x1c,0x3C}, //100408 dcdc 3c ->34 -> 3c -> 34
	{0x1d,0x01}, //100408 add
	{0x1e,0x30}, // 100224 00 -> 30
	{0x1f,0x10},  //091207 dcdc auto ->10 -> 00

	{0x20,0x99}, // 100224 aa -> 77
	{0x21,0xaa}, // 100408 33 -> 77 -> aa
	{0x22,0xa7}, // 100224 73 -> a7
	{0x23,0x33}, // 100408 30 -> 32

	{0x27,0x34}, // 100224 3c -> 34
	{0x2B,0x80},  //091207 add
	{0x2E,0x11},  //091207 add
	{0x2F,0xa1},  // 20100429 by richard 71->a1

	{0x30,0x00},
	{0x31,0x99},
	{0x32,0x00},
	{0x33,0x00},
	{0x34,0x22},  //091207 add

	{0x3D,0x03},
	{0x3e,0x0b}, // 100224 add
	{0x49,0xF1}, //20100416 A1-->71->D1->F1(6.0)
	{0x4A,0x02}, //100415 Added v6.0
	{0x50,0x20},  // 100408  20->29 -> 28
	{0x52,0x03},  //091207 add
	{0x54,0x3C}, // 100408 3c -> 2c ->
	{0x55,0x1c},
	{0x56,0x11},
	{0x5d,0xa2}, // 100224 aa -> a2
	{0x5E,0x5a},

	{0x60,0x6c},
	{0x61,0x77},
	{0x62,0x6c},
	{0x63,0x75},
	{0x64,0x6c},
	{0x65,0x75},
	{0x67,0x0c},
	{0x68,0x0c},
	{0x69,0x0c},
	{0x72,0x6d},
	{0x73,0x74},
	{0x74,0x6d},
	{0x75,0x74},
	{0x76,0x6d},
	{0x77,0x74},

	{0x7c, 0x6b},
	{0x7d, 0x84},

	{0x80, 0x01},
	{0x81, 0x58},
	{0x82, 0x03},
	{0x83, 0x10},
	{0x84, 0x57},
	{0x85, 0x59},
	{0x86, 0x57},
	{0x87, 0x59},

	{0x92, 0x2b},
	{0x93, 0x38},
	{0x94, 0x57},
	{0x95, 0x59},
	{0x96, 0x57},
	{0x97, 0x59},

	{0xa0, 0x02},
	{0xa1, 0x53},
	{0xa2, 0x02},
	{0xa3, 0x53},
	{0xa4, 0x53},
	{0xa5, 0x02},
	{0xa6, 0x53},
	{0xa7, 0x02},
	{0xa8, 0x6c},
	{0xa9, 0x70},
	{0xaa, 0x6c},
	{0xab, 0x70},
	{0xac, 0x00},
	{0xad, 0x00},
	{0xae, 0x00},
	{0xaf, 0x00},

	{0xb0, 0x77},
	{0xb1, 0x80},
	{0xb4, 0x78},
	{0xb5, 0x7f},
	{0xb8, 0x78},
	{0xb9, 0x7e},
	{0xbc, 0x79},
	{0xbd, 0x7d},
	{0xb2, 0x77},
	{0xb3, 0x80},
	{0xb6, 0x78},
	{0xb7, 0x7f},
	{0xba, 0x78},
	{0xbb, 0x7e},
	{0xbe, 0x79},
	{0xbf, 0x7d},

	{0xc4, 0x13},
	{0xc5, 0x29},
	{0xc6, 0x3b},
	{0xc7, 0x51},
	{0xc8, 0x14},
	{0xc9, 0x28},
	{0xca, 0x14},
	{0xcb, 0x28},
	{0xcc, 0x3c},
	{0xcd, 0x50},
	{0xce, 0x3c},
	{0xcf, 0x50},

	{0xd0,0x0a},  //091202 dcdc
	{0xd1,0x09},  //091202 dcdc
	{0xd2,0x20},//091202 dcdc
	{0xd3,0x00},//091202 dcdc
	{0xd4,0x0a}, //100409
	{0xd5,0x0a}, //100409
	{0xd6,0x29}, //100409  34->29
	{0xd7,0x28}, //100409  2c->28

	{0xe0,0x61}, // 100224 e0 -> e1->61 (V6.0)
	{0xe1,0x61}, // 100224 e0 -> e1->61 (V6.0)
	{0xe2,0x61}, // 100224 e0 -> e1->61 (V6.0)
	{0xe3,0x61}, // 100224 e0 -> e1->61 (V6.0)
	{0xe4,0x61}, // 100224 e0 -> e1->61 (V6.0)
	{0xe5,0x01},  //091202 dcdc

	{0xea, 0x58},

	////////////////////////////// 10 Page ISP-CTL1
	{0x03,0x10},
	{0x10,0xF0}, // For bayer 03},  //YUV422_YUYV
	{0x11,0x00}, //For bayer
	{0x31,0x00}, //For bayer
	{0x34,0x40}, //For bayer
	{0x37,0x20}, //For bayer
	{0x3f,0xF4}, //For bayer

	//Saturation Control OFF for Bayer
	{0x60,0x00}, //For bayer


	////////////////////////////// 11 Page D-LPF
	{0x03,0x11},
	{0x10,0x00}, //For bayer D-LPF
	{0x60,0x00}, //For bayer
	{0x70,0x00}, //Aberration //For bayer

	////////////////////////////// 12 Page YC2D-LPF
	{0x03,0x12},
	{0x20,0x00},//For bayer
	{0x21,0x00},//For bayer
	{0x90,0x5d},//For bayer

	////////////////////////////// 13 Page Edge
	{0x03,0x13},
	//1DY
	{0x10,0x00},//For bayer
	//2DY
	{0x80,0x00},//For bayer

	////////////////////////////// 14 Page LensShading
	{0x03,0x14},
	{0x10,0x01}, // 20100429 by richard 07->01
	{0x20,0x80}, // 20100225 Richard 0x9c-->0xE0
	{0x21,0x80}, // 20100225 Richard 0x50-->0xF0
	{0x22,0x90}, // 20100225 Richard 0xb0-->0xC8
	{0x23,0x80}, // 20100225 Richard 0x40-->0xE0
	{0x24,0x90}, // 20100225 Richard 0xa0-->0xE8
	{0x25,0x80}, // 20100225 Richard 0x60-->0xff
	{0x28,0x36}, // 20100225 Richard {0x60-->{0xff
	{0x29,0x37}, // 20100225 Richard {0x60-->{0xff
	{0x40,0x72}, // 20100225 Richard 0x85-->0x68
	{0x41,0x6b}, // 20100225 Richard 0x82-->0x4A
	{0x42,0x5b}, // 20100225 Richard 0x78-->0x48
	{0x43,0x6b}, // 20100225 Richard 0x82-->0x4A

	////////////////////////////// 15page CMC
	//CMC Fuction Start
	{0x03,0x15},
	{0x10,0x00},  //For bayer
	{0x80,0x00},  //For bayer

	////////////////////////////// 16 Page Gamma
	{0x03,0x16},
	{0x10,0x00},//For bayer

	////////////////////////////// 17 Page
	{0x03,0x17 },
	{0x10,0x00 }, //For bayer

	////////////////////////////// 18 Page
	{0x03, 0x18},
	{0x10, 0x00},//For bayer

	////////////////////////////// 20 Page
	{0x03,0x20 }, //PAGE 0x20
	{0x10,0x0C },
	{0x11,0x1C },
	{0x1a,0x04 },// 100224 00 -> 04
	//{0x21,0x18 },
	//{0x20,0x01 },//Disable ? : By LEEMASTER 2010.04.28
	//{0x27,0x2A },
	//{0x28,0xBf },

	// AntiBand setting
	//{0x2A,0xfc},  // 20100429 by richard  0xf0->0xfc
	//{0x2B,0x34},//(AntiBand 1/100)
	//{0x30,0x78},//(AntiBand 1/100)
	//{0x2B,0xF4},	//(AntiBand 2/100)
	//{0x30,0xF8},	//(AntiBand 2/100)

	//{0x39,0x22},
	//{0x3a,0xde},    //df->de ae escape 20091210
	//{0x3b,0x22},
	//{0x3c,0xde},    //df->de ae escape 20091210

	//{0x2e,0x06},  // 10409 0x00 -> 0x06
	//{0x2F,0xcc},  // 10409 0x00 -> 0xcc
	//{0x43,0xc0},
	//{0x56,0x28},
	//{0x57,0x78},
	//{0x58,0x16}, //0x20},
	//{0x59,0x43}, //0x60},
	//{0x5b,0x04},
	//{0x5e,0x9f},  // hsync inc size 1.3mega 0x9f
	//{0x5f,0x59}, //0x7f},  // vsync inc size 1.3mega 0x7f

	//// Center + Bottom : By LEEMASTER 2010.04.28
	//{0x60,0xaa},//{0x60,0xaf}, //{0x60,0xa0},   //Weight
	//{0x61,0xaa},//{0x61,0xfa}, //{0x61,0xfa},
	//{0x62,0xaa},//{0x62,0xaf}, //{0x62,0xa0},
	//{0x63,0xaa},//{0x63,0xfa}, //{0x63,0xfa},
	//{0x64,0xaf},//{0x64,0xff}, //{0x64,0xff},
	//{0x65,0xfa},//{0x65,0xff}, //{0x65,0xff},
	//{0x66,0xaf},//{0x66,0xff}, //{0x66,0xff},
	//{0x67,0xfa},//{0x67,0xff}, //{0x67,0xff},
	//{0x68,0xaf},//{0x68,0xff}, //{0x68,0xff},
	//{0x69,0xfa},//{0x69,0xff}, //{0x69,0xff},
	//{0x6a,0xff},//{0x6a,0xff}, //{0x6a,0xff},
	//{0x6b,0xff},//{0x6b,0xff}, //{0x6b,0xff},
	//{0x6c,0xff},//{0x6c,0xaf}, //{0x6c,0xa0},
	//{0x6d,0xff},//{0x6d,0xfa}, //{0x6d,0xfa},
	//{0x6e,0xff},//{0x6e,0xaf}, //{0x6e,0xa0},
	//{0x6f,0xff},//{0x6f,0xfa}, //{0x6f,0xfa},

	//{0x70,0x32},// Y target 3c -> 35->32
	//{0x71,0x00},// 100304 richard 3c->00
	//{0x78,0x23},//091204
	//{0x79,0x14},//091204
	//{0x76,0x21},//unlock bound1 middle 88->21
	//{0x77,0x81},//unlock bound2 middle fe->81
	//{0x78,0x22},//Y-TH1
	//{0x79,0x19},//14->1E @50 hz Y-th2 50->24 -> 12->19
	//{0x7A,0x23},
	//{0x7B,0x22},
	//{0x7d,0x23},
	//{0x7e,0x22},

	{0x86,0x01},   //Exp Min
	{0x87,0x90},

	//Pll 3x
	{0x83,0x03}, //100226 richard Exp Normal 20fps @60hz
	{0x84,0x6E},
	{0x85,0xE8},

	//{0x88,0x09}, //Exp Max //5fps @60hz
	//{0x89,0xBA},
	//{0x8a,0x3c},
	{0x88,0x08}, //Exp Max //8fps @60hz
	{0x89,0x95},
	{0x8a,0x44},

	{0x8b,0xaf},   //Exp 100
	{0x8c,0xc8},

	{0x8d,0x92},   //Exp 120
	{0x8e,0x7c},

	{0x91,0x05},   //20091210 06->05
	{0x92,0xe9},   //20091210 0a->e9
	{0x93,0xac},   //20091210 a8->ac
	{0x94,0x02},   //20091210 00->02
	{0x95,0x7a},   //20091210 61->7a
	{0x96,0xc4},   //20091210 a8->c4

	//{0x98,0x9e},   //outdoor th1
	//{0x99,0x45},   //outdoor th2

	//{0x9b,0x9a},   //outdoor th1
	//{0x9a,0x28},   //outdoor th2

	//Exptime 4S limit
	//{0x9c,0x08},//09->08
	//{0x9d,0x34},//c4->34

	//Exptime 4S unit
	//{0x9e,0x01},//HBLANK(280) + 1320 = 1600(1 line),1line/4 = 400= 0x1af
	//{0x9f,0x90},

	//{0xa0,0x03},
	//{0xa1,0x0d},
	//{0xa2,0x40},

	//Exp12000
	//{0xa3,0x00},  //Antibandtime/256=120*256=30720fps
	//{0xa4,0x30},

	//Capture Exptime
	//{0xA5,0x01},  //Capture ExpmaxH
	//{0xA6,0x6E},  //Capture ExpmaxM
	//{0xA7,0x36},  //Capture ExpmaxL
	//{0xA8,0x1D},  //Capture Exp100H
	//{0xA9,0x4C},  //Capture Exp100L
	//{0xAA,0x18},  //Capture Exp120H //PLL 2x ����ٰ� ����.
	//{0xAB,0x6A},  //Capture Exp120L
	//{0xAC,0x00},  //Capture Exp12000H
	//{0xAD,0x18},  //Capture Exp12000L
	//{0xAE,0x23},  //Capture Outtime th2
	//
	//{0xaf,0x04},  // 20100309 for AE reset

	//AG set ag min 15
	{0xb0,0x10},  //AG
	{0xb1,0x10},  // 20100429 by richard //AGMIN
	{0xb2,0x3a},  // 20100429 by richard //AGMAX
	//{0xb3,0x14},  //AGLVL
	//{0xb4,0x14},  //AGTH1
	//{0xb5,0x38},  //AGTH2
	//{0xb6,0x26},  //AGBTH1
	//{0xb7,0x20},  //AGBTH2
	//{0xb8,0x1D},  //AGBTH3 //Adaptive_AG Min_0x2a
	//{0xb9,0x1B},  //AGBTH4
	//{0xba,0x1A},  //AGBTH5
	//{0xbb,0x19}, //AGBTH6
	//{0xbc,0x19},  //AGBTH7
	//{0xbd,0x18},  //AGBTH8

	//{0xbe,0x30},  //AGBTH9
	//{0xbf,0x2f},  //AGBTH10
	//{0xc0,0x2f},  //AGBTH11
	//{0xc1,0x2e},  //AGBTH12

	//{0xc2,0x2a},  //Adaptive AGMIN
	//{0xc3,0x2a},  //Adaptive AGLVL
	//{0xc4,0x2a},   //20091210 2a->25
	//{0xc5,0x0a},  //20091210 0z->1f

	//{0xc6,0x48},  //Middle AG
	//{0xcc,0x48},  //Middle AG min
	//{0xca,0xC2},  //Middle DG
	//{0xc7,0x18},  //Sky_gain
	//{0xc8,0xff},  //Middle indoor max c8->d6
	//{0xc9,0x80},  //Middle inddor min

	//22page
	{0x03,0x22},  //page 22
	{0x10,0x69},  //awb off  //For bayer

	//{0x03,0x20},
	//{0x10,0x8C},  //AE ON
	//{0x03,0x22},
	//{0x10,0xEB},  //AWB ON
	{0x01,0xf0},
};
/*
t_RegSettingBB code gc_YACC6A1S_720P[] =
{
	// BAYER  1288x728 30fps
// pll setting
{0x03, 0x00},
//{0x0e, 0x75},

//////////////////// 0 page
{0x03, 0x00},
// mode video 1
// hblank
{0x40, 0x01},
//{0x41, 0x18},
{0x41, 0x18},

// vblank
{0x42, 0x00},
//{0x43, 0x14},  //2011-05-23 PaoChi, Fix 30fps issue
{0x43, 0x1e},  //2011-05-23 PaoChi, Fix 30fps issue

{0x03,0x00},
{0x13,0x80},

//Output Window

//{0x24,0x02},//Height H
//{0x25,0xD8},//Height L 720 -> 728
{0x26,0x05},//Width H
{0x27,0x08},//Width L 1280 -> 1288

 //Raw Window

{0x30, 0x00},//Start H No Flip
{0x31, 0x9F},//Start L : 159
{0x32, 0x03},//Stop H :
{0x33, 0x77},//Stop L : 887  = 879 + 8

{0x34, 0x02},//Pxl Height H : ( 887 - 159 ) + 22 = 750
//{0x35, 0xEE},//Pxl Height L
{0x35, 0xE0},//2011-05-23 PaoChi, Fix 30fps issue



};

t_RegSettingBB code gc_YACC6A1S_800P[] =
{
// BAYER  1288x808

// pll setting
{0x03, 0x00},
//{0x0e, 0x75},

//////////////////// 0 page
{0x03, 0x00},
// mode video 1
//{0x10, 0x04},

// hblank
{0x40, 0x01},//01->00
{0x41, 0x18},//18->B4
// vblank
{0x42, 0x00},
{0x43, 0x04},

{0x20,0x00},
{0x21,0x00},
{0x22,0x00},
{0x23,0x00},

//Output Window

{0x24,0x04},
{0x25,0x08},  // 808
{0x26,0x05},
{0x27,0x08},  //1288

 //Raw Window

{0x30,0x00},//
{0x31,0x74},// 116
{0x32,0x03},
{0x33,0x9c},// 924

{0x34,0x03},
{0x35,0x28},  //808 = 924 - 116
};
*/
t_RegSettingBB code gc_YACC6A1S_960P[] =
{
// BAYER  1288x968
//////////////////// 0 page
	{0x03, 0x00},
// mode video 1
//{0x0e, 0x75}, //PLL 3x
	{0x10, 0x04},
	{0x13, 0x80},


// hblank
//{0x40, 0x00},//01->00
//{0x41, 0xb4},//18->B4
// hblank
	{0x40, 0x01},//01->00
	{0x41, 0x18},//18->B4
// vblank
	{0x42, 0x00},
	{0x43, 0x04},

	{0x20,0x00},
	{0x21,0x00},
	{0x22,0x00},
	{0x23,0x00},

	{0x24,0x04},
	{0x25,0x08},
	{0x26,0x05},
	{0x27,0x08},

	{0x30,0x00},//00
	{0x31,0x25},//9F->A0
	{0x32,0x03},
	{0x33,0xEc},//7E-70
	{0x34,0x03},
	{0x35,0xd0},
};

t_RegSettingBB code gc_YACC6A1S_SXGA[] =
{
// BAYER  1288x1032
//////////////////// 0 page
	{0x03, 0x00},
// mode video 1
	{0x10, 0x04},


//Output Window
	{0x20, 0x00},
	{0x21, 0x00},
	{0x22, 0x00},
	{0x23, 0x00},
	{0x24, 0x04},
	{0x25, 0x10},
	{0x26, 0x05},
	{0x27, 0x10},

// hblank
	{0x40, 0x01},//01->00
	{0x41, 0x18},//18->B4
// vblank
	{0x42, 0x00},
	{0x43, 0x04},
//{0x43, 0x16}, //2011-05-26 PaoChi, Fix HCLK 30MHz, 30fps issue

//Raw Window
	{0x30,0x00},//00
	{0x31,0x00},//00
	{0x32,0x04},
	{0x33,0x08},//1032
	{0x34,0x04},
	{0x35,0x08},//1032 - 0 = 1032

};

static OV_FpsSetting_t*  GetOvFpsSetting(U8 Fps, OV_FpsSetting_t staOvFpsSetting[], U8 byArrayLen)
{
	U8 i;
	U8 Idx;

	ISP_MSG((" array size = %bd\n", byArrayLen));

	if(g_bIsHighSpeed)
	{
		Idx=2; //8fps for uxga, 10 fps for vga
	}
	else
	{
		Idx = 0;
	}

	for(i=0; i< byArrayLen; i++)
	{
		ISP_MSG((" fps i = %bd\n",i));
		if(staOvFpsSetting[i].byFps ==Fps)
		{
			Idx=i;
			break;
		}
	}
	ISP_MSG((" get fps idx = %bd\n",Idx));
	return &staOvFpsSetting[Idx];
}

static U16 MapSnrGlbGain2SnrRegSetting(U16 wGain)
{
	U16 sensorGain;

	if (wGain <= 8)
	{
		sensorGain = 0 ;
	}
	else
	{
		sensorGain = ((wGain - 8) << 1);
	}

	return sensorGain;
}

float SnrRegSetting2SnrGlbGain(U16 wSnrRegGain)
{
	return (float)wSnrRegGain/32.0 + 0.5;
}

//this function is also called during Probe stage to calculate the transfer_size/LWM/HWM
void GetSensorPclkHsync(U16 wSensorSPFormat,U8 byFps)
{
	OV_FpsSetting_t *pYACC6A1S_FpsSetting;

	if(wSensorSPFormat  == SXGA_FRM)
	{
		pYACC6A1S_FpsSetting=GetOvFpsSetting(byFps, g_staYACC6A1SFpsSetting_SXGA, sizeof(g_staYACC6A1SFpsSetting_SXGA)/sizeof(OV_FpsSetting_t));
	}
	else
	{
		pYACC6A1S_FpsSetting=GetOvFpsSetting(byFps, g_staYACC6A1SFpsSetting_960P, sizeof(g_staYACC6A1SFpsSetting_960P)/sizeof(OV_FpsSetting_t));
	}

	g_wSensorHsyncWidth = pYACC6A1S_FpsSetting->wExtraDummyPixel; // this for manual exposure
	g_dwPclk = pYACC6A1S_FpsSetting->dwPixelClk;		// this for scale speed
}

void YACC6A1SSetFormatFps(U16 SetFormat, U8 Fps)
{
	U16 wTemp;
	OV_FpsSetting_t *pYACC6A1S_FpsSetting;

	// hemonel 2010-04-13: for delete complier warning
	SetFormat = SetFormat;

	// initial all register setting

	WriteSensorSettingBB(sizeof(gc_YACC6A1S_Setting)/2, gc_YACC6A1S_Setting);
	/*
	if((g_wSensorSPFormat & SetFormat) == HD720P_FRM)
	{
		pYACC6A1S_FpsSetting=GetOvFpsSetting(Fps, g_staYACC6A1SFpsSetting_720P, sizeof(g_staYACC6A1SFpsSetting_720P)/sizeof(OV_FpsSetting_t));
		WriteSensorSettingBB(sizeof(gc_YACC6A1S_720P)/2, gc_YACC6A1S_720P);
	}
	else
	*/
	if(((g_wSensorSPFormat & SetFormat) == SXGA_FRM))
	{
		pYACC6A1S_FpsSetting=GetOvFpsSetting(Fps, g_staYACC6A1SFpsSetting_SXGA, sizeof(g_staYACC6A1SFpsSetting_SXGA)/sizeof(OV_FpsSetting_t));
		WriteSensorSettingBB(sizeof(gc_YACC6A1S_SXGA)/2, gc_YACC6A1S_SXGA);
	}
	else //if(((g_wSensorSPFormat & SetFormat) == XVGA_FRM))
	{
		pYACC6A1S_FpsSetting=GetOvFpsSetting(Fps, g_staYACC6A1SFpsSetting_960P, sizeof(g_staYACC6A1SFpsSetting_960P)/sizeof(OV_FpsSetting_t));
		WriteSensorSettingBB(sizeof(gc_YACC6A1S_960P)/2, gc_YACC6A1S_960P);
	}


	// 2) write sensor register
	Write_SenReg(0x03,0x00);
	Write_SenReg(0x0e, pYACC6A1S_FpsSetting->byClkrc);

	wTemp = pYACC6A1S_FpsSetting->wExtraDummyPixel - 1320; // From HI161 spec timing
	Write_SenReg(0x40,INT2CHAR(wTemp, 1));
	Write_SenReg(0x41,INT2CHAR(wTemp, 0));

//	Write_SenReg(0x42,0x00);
//	Write_SenReg(0x43, pYACC6A1S_FpsSetting->byDummyLine);

	// 3) update variable for AE
	//g_wSensorHsyncWidth = pYACC6A1S_FpsSetting->wExtraDummyPixel; // this for manual exposure
	//g_dwPclk = pYACC6A1S_FpsSetting->dwPixelClk;		// this for scale speed
	GetSensorPclkHsync((g_wSensorSPFormat & SetFormat),Fps);
	/*
	if((g_wSensorSPFormat & SetFormat) == HD720P_FRM)
	{
	      	g_wAEC_LineNumber = 772;	// this for AE insert dummy line algothrim
		g_wAECExposureRowMax = 768;	// this for max exposure time

		g_wSensorWidthBefBLC = 1280;
		g_wSensorHeightBefBLC = 720;
	}
	else
	*/
	if((g_wSensorSPFormat & SetFormat) == SXGA_FRM)
	{
		g_wAEC_LineNumber = 1076;	// this for AE insert dummy line algothrim
		g_wAECExposureRowMax =1072;	// this for max exposure time

		g_wSensorWidthBefBLC = 1280;
		g_wSensorHeightBefBLC = 1024;
	}
	else //if((g_wSensorSPFormat & SetFormat) == XVGA_FRM)
	{
		g_wAEC_LineNumber = 996;	// this for AE insert dummy line algothrim
		g_wAECExposureRowMax = 992;	// this for max exposure time

		g_wSensorWidthBefBLC = 1280;
		g_wSensorHeightBefBLC = 960;
	}


	//g_wBLCStartX = 0;
	//g_wBLCStartY = 0;
	// hemonel 2010-02-26: sensor output BGBG at first row, so skip 1 row for backend ISP bayer pattern GRGR alignment
//	SetBkWindowStart(0, 1);

	DBG_SENSOR(("Fps=%bu\n",Fps));
	return;

}

void CfgYACC6A1SControlAttr(void)
{
	U8 i;

	g_bySensorSize = SENSOR_SIZE_SXGA;
	g_wSensorSPFormat =SXGA_FRM|XVGA_FRM;

	memcpy(g_asOvCTT,gc_YACC6A1S_CTT,sizeof(gc_YACC6A1S_CTT));

	{
#if (_CHIP_ID_ & _RTS5840_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_1V84;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_2V90;
#elif (_CHIP_ID_ & _RTS5832_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_1V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_2V88;
#elif (_CHIP_ID_ & _RTS5829B_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_1V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_2V92;
#endif
	}

	{
		//-------- configure high speed frame size-----------
		//video
		g_aVideoFormat[0].byaVideoFrameTbl[0] = 8;
		g_aVideoFormat[0].byaVideoFrameTbl[1]=F_SEL_640_480;
		g_aVideoFormat[0].byaVideoFrameTbl[2]=F_SEL_160_120;
		g_aVideoFormat[0].byaVideoFrameTbl[3]=F_SEL_176_144;
		g_aVideoFormat[0].byaVideoFrameTbl[4]=F_SEL_320_240;
		g_aVideoFormat[0].byaVideoFrameTbl[5]=F_SEL_352_288;
		g_aVideoFormat[0].byaVideoFrameTbl[6]=F_SEL_1280_720;
		g_aVideoFormat[0].byaVideoFrameTbl[7]=F_SEL_1280_960;
		g_aVideoFormat[0].byaVideoFrameTbl[8]=F_SEL_1280_1024;
		//still image
		g_aVideoFormat[0].byaStillFrameTbl[0] = 3;
		g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_640_480;
		g_aVideoFormat[0].byaStillFrameTbl[2]=F_SEL_1280_720;
		g_aVideoFormat[0].byaStillFrameTbl[3]=F_SEL_1280_1024;

		//-------- configure high speed FPS F_SEL_1280_1024;setting-----------
		for(i=0; i<7; i++)
		{
			/*640_480,
			160_120,
			176_144,
			320_200,
			320_240,
			352_288,
			640_400*/
			g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_30|FPS_25|FPS_20|FPS_15;
		}

		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_1280_720] = FPS_9 | FPS_5;
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_1280_800] = FPS_9 | FPS_5;
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_1280_960] = FPS_9 | FPS_5;
		g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_1280_1024] = FPS_5;

		//====== format type  setting ===========
		g_aVideoFormat[0].byFormatType = FORMAT_TYPE_YUY2;
#ifdef _ENABLE_MJPEG_
		// copy YUY2 setting to MJPEG setting
		memcpy(&(g_aVideoFormat[1]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

		// modify MJPEG FPS setting
		for(i=7; i<13; i++) //bit0-6 VGA and smaller size, 30fps max
		{
			g_aVideoFormat[1].waFrameFpsBitmap[i] =FPS_30|FPS_25|FPS_20|FPS_15;;//|FPS_1|FPS_3|FPS_5|FPS_10;
		}
		g_aVideoFormat[1].waFrameFpsBitmap[F_SEL_1280_1024] = FPS_30|FPS_25|FPS_20|FPS_15;;

		// modify MJPEG format type
		g_aVideoFormat[1].byFormatType = FORMAT_TYPE_MJPG;
#endif

#ifdef _ENABLE_M420_FMT_
		memcpy(&(g_aVideoFormat[2]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));
		g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_720] =FPS_11|FPS_5;
		g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_800] =FPS_11|FPS_5;
		g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_960] =FPS_11|FPS_5;
		g_aVideoFormat[2].waFrameFpsBitmap[F_SEL_1280_1024] =FPS_11|FPS_5;
		g_aVideoFormat[2].byFormatType = FORMAT_TYPE_M420;
#endif
	}

	{
		//Brightness - BrightnessItem

		//Contrast - ContrastItem
		//ContrastItem.Last = 3;
		//Saturation - SaturationItem
		//SaturationItem.Last = 60;
		//Hue - 	HueItem

		//Sharpness - SharpnessItem
		//	SharpnessItem.Min = 0;
		//	SharpnessItem.Max = 8;
		//	SharpnessItem.Res = 1;
		//	SharpnessItem.Def = 3;
		//	SharpnessItem.Last  =SharpnessItem.Def;

		// gamma - 	GammaItem

		// gain - GainItem

		// whitebalance - WhiteBalanceTempItem

		// backlight - BackLightCompItem

		// powerline -PwrLineFreqItem

		// exposure time -	ExposureTimeAbsolutItem

		// low light - LowLightCompItem
	}
	InitYACC6A1SIspParams();
}

void SetYACC6A1SIntegrationTime(U32 dwSetValue)
{
	U16 wEspBase;
	U32 dwExpTime;

	//g_dwSetValue = dwSetValue;

	if (dwSetValue == 0x004e)
	{
		wEspBase = 1;
	}
	else if (dwSetValue == 0x009c)
	{
		wEspBase = 2;
	}
	else if (dwSetValue == 0x0138)
	{
		wEspBase = 3;
	}
	else if (dwSetValue == 0x0271)
	{
		wEspBase = 4;
	}
	else if (dwSetValue == 0x04e2)
	{
		wEspBase = 5;
	}
	else if (dwSetValue == 0x09c4)
	{
		wEspBase = 6;
	}
	else
	{
		wEspBase = 7;
	}

	if((PwrLineFreqItem.Last == PWR_LINE_FRQ_60))
	{
		dwExpTime = 0x124AD*wEspBase;
		//dwExpTime = 0x9256*wEspBase;
	}
	else // (PwrLineFreqItem.Last == PWR_LINE_FRQ_60)
	{
		dwExpTime = 0x107AC*wEspBase;
	}

	Write_SenReg(0x03,0x20);
	Write_SenReg(0x83, LONG2CHAR(dwExpTime, 2));
	Write_SenReg(0x84, LONG2CHAR(dwExpTime, 1));
	Write_SenReg(0x85, LONG2CHAR(dwExpTime, 0));

	Write_SenReg(0x03,0x20);
	Write_SenReg(0xb0,0x4c);

}

void SetYACC6A1SImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.
{
	WaitFrameSync(ISP_INT1, ISP_DATA_START_INT);
	Write_SenReg(0x03, 0x00);
	Write_SenReg_Mask(0x11,bySnrImgDir, 0x03);
	WaitFrameSync(ISP_INT1, ISP_DATA_START_INT);

	SetBkWindowStart(bySnrImgDir & 0x01, (bySnrImgDir ^ 0x02)>>1);
}

void SetYACC6A1SExposuretime_Gain(float fExpTime, float fTotalGain)
{
	U16  wGainRegSetting;
	float fSnrGlbGain;
	U32 dwExpTime;

	wGainRegSetting=MapSnrGlbGain2SnrRegSetting((U16)(fTotalGain*16.0));
	fSnrGlbGain = SnrRegSetting2SnrGlbGain(wGainRegSetting);

	if(fabs(g_fCurExpTime-fExpTime) > 0.0001)
	{
		dwExpTime =  (U16)(fExpTime*(g_dwPclk/80000));

		// HI161 must set exposure time at frame start, do not set at data start
		Write_SenReg(0x03,0x20);
		Write_SenReg(0x83, LONG2CHAR(dwExpTime, 2));
		Write_SenReg(0x84, LONG2CHAR(dwExpTime, 1));
		Write_SenReg(0x85, LONG2CHAR(dwExpTime, 0));

		// delay one frame for sync
		WaitFrameSync(ISP_INT1, ISP_DATA_START_INT);
		Write_SenReg(0x03,0x20);
		Write_SenReg(0xb0, INT2CHAR(wGainRegSetting, 0)); //Change Analog Gain

		SetISPAEGain(fTotalGain, fSnrGlbGain, 1);

		g_fCurExpTime = fExpTime;
	}
	else
	{
		Write_SenReg(0x03,0x20);
		Write_SenReg(0xb0, INT2CHAR(wGainRegSetting, 0)); //Change Analog Gain

		SetISPAEGain(fTotalGain, fSnrGlbGain, 1);
	}
}

void YACC6A1S_POR()
{
#ifdef _MIPI_EXIST_
	Init_MIPI();
#else
	Init_CCS();
#endif
	ENTER_SENSOR_RESET();
	// power on
	SensorPowerControl(SWITCH_ON,EN_DELAYTIME);
	uDelay(1);
	// Initialize I2C Controller Timing
	LEAVE_SENSOR_RESET();
	// clock select 24M
	if( g_bySSCEnable== TRUE)
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_SSC_24M);
	}
	else
	{
		//CHANGE_CCS_CLK(CCS_CLK_SEL_96M|CCS_CLK_DIVIDER_4);
		CHANGE_CCS_CLK(CCS_CLK_SEL_60M|CCS_CLK_DIVIDER_2);
	}

	EnableSensorHCLK();
	g_bySensorIsOpen = SENSOR_OPEN;
	WaitTimeOut_Delay(1);  //OV9726 spec request 8192clk
}

void InitYACC6A1SIspParams()
{
	// AWB initial gain
	//use D50 R/G/B gain as AWB initial value
	g_wAWBRGain_Last= 0x100;
	g_wAWBGGain_Last= 0x102;
	g_wAWBBGain_Last= 0x113;
	g_wProjectGR = g_wAWBRGain_Last;
	g_wProjectGB = g_wAWBBGain_Last;

	// Special ISP
	g_bySaturation_Def =64;
	g_byContrast_Def = 32; //Neil Tuning at chicony

	g_wDynamicISPEn = DYNAMIC_SHARPPARAM_EN | DYNAMIC_SHARPNESS_EN;
}


void SetYACC6A1SDynamicISP(U8 byAEC_Gain)
{
	byAEC_Gain = byAEC_Gain;
}

void SetYACC6A1SDynamicISP_AWB(U16 wColorTempature)
{
	wColorTempature = wColorTempature;
}
#endif
