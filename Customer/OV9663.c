#include "Inc.h"

#ifdef RTS58XX_SP_OV9663
// hemonel 2009-03-17: tune fps ok
// row = 526
// column = pclk/(row*fps)
// extra dummy = column-numer per line(760)
code OV_FpsSetting_t  g_staOV9663VGAFpsSetting[]=
{
	// FPS ExtraDummyPixel clkrc 	   pclk
	//	{1,		(0+1),		(29),	  400000},
	{3,		(0+1),		(9),		 1200000},	// 2.9999
	{5,		(0+1),		(5),		 2000000},	//5.0000
	{10,		(0+1),		(2),		 4000000},	//10.0000
	{15,		(0+1),		(1), 		 6000000},	//14.9999
	{20,		(760+2),		(0),	     	12000000},		// 19.9956
	{25,		(304+1),		(0),		12000000},		// 25.0025
	{30,		(0+1),		(0),		12000000},	// 30.0000
};
// hemonel 2009-03-17: tune fps ok
// row = 1052
// column = pclk/(row*fps)
// extra dummy = column-numer per line(1520)
code OV_FpsSetting_t  g_staOV9663SXGAFpsSetting[]=
{
	//	{1,		(0+1),		(14), 	  1600000},
	{3,		(0+1),		(4), 	  	  4800000},	// 2.9999
	{5,		(0+1),		(2),  	  8000000}, 	// 5.0000
	{8,		(1330+2),	(0), 		24000000},	// 7.9996
	{9,		(1013+2),	(0), 		24000000},	// 9.0000
	{10,		(760+2),		(0), 		24000000},	// 9.9978
	{11,		(553+1),		(0),		24000000},
	{12,		(381),		(0), 		24000000},	// 9.9978
	{15,		(0+1),		(0),		24000000},	// 14.999
};

//----------------------------------
// intial sensor register setting
//----------------------------------
t_RegSettingBB code gc_OV9663_VGA_Setting[] =
{
	// IO output
	{0xD5, 0xFF},	 //Pixel Data Output
	{0xD6, 0x3F}, //Pixel Data Output

	//PLL
	{0x3d, 0x3C},	 // fclk
	{0x11, 0x81}, //0x80_30fps
	{0x2A, 0x00},
	{0x2B, 0x00},

	// power control
	{0x3A, 0xD9},
	{0x3B, 0x00},
	{0x3C, 0x58},
	{0x3E, 0x50}, //PLL Bypass or Enable , 0x50=Bypass
	{0x71, 0x00},

	//Sync signal
	{0x15, 0x00}, //PCLK always output , VSYNC positive polarity
	{0x09, 0x01},

	//Data Format YUV
	{0xD7, 0x10},	 //YUYV sequence , YUV output
	{0x6A, 0x24},
	{0x85, 0xE7},

	// sample option
	{0x63, 0x00},

	// windowing
	{0x12, 0x40}, //VGA Mode
	{0x4D, 0x09},
	{0x17, 0x0C}, //H_Start
	{0x18, 0x5C}, //H_End
	{0x19, 0x02}, //V_Start
	{0x1A, 0x3F}, //V_End
	{0x03, 0x43}, //V_Start / V_End LSB //0x03},changed: jqg20090415, for Night mode
	{0x32, 0xB4},	 //H_Start / H_End LSB
	{0x2B, 0x00},
	{0x5C, 0x80},// AEC Full Zone Average

	// BLC
	{0x36, 0xB4}, // De-noise divider value
	{0x65, 0x10},
	{0x70, 0x02},
	{0x71, 0x9F},
	{0x64, 0xA4},	// BLC Line select , VGA = A4

	//Scaling
	{0xab, 0xe7},
	{0xb9, 0x50},
	{0xba, 0x3c},
	{0xbb, 0x50},
	{0xbc, 0x3c},

	//BLC
	{0x0D, 0x92},
	{0x0D, 0x90}
};

t_RegSettingBB code gc_OV9663_SXGA_Setting[] =
{
	// IO output
	{0xD5, 0xFF},
	{0xD6, 0x3F},

	//PLL
	{0x3d, 0x3C},
	{0x11, 0x81}, //0x80_30fps
	{0x2A, 0x00},
	{0x2B, 0x00},

	// power control
	{0x3A, 0xD9},
	{0x3B, 0x00},
	{0x3C, 0x58},
	{0x3E, 0x50},
	{0x71, 0x00},

	//Sync signal
	{0x15, 0x00},
	{0x09, 0x01},
	//Data Format YUV
	{0xD7, 0x10},
	{0x6A, 0x24},
	{0x85, 0xE7},

	// sample option
	{0x63, 0x01},

	// windowing
	{0x12, 0x00}, //Don't del this, for SXGA resolution ___JQG_20090806___
	{0x4D, 0x11},  // hemonel 2009-08-21: preview sxga and still image vga, after still image, sxga image has sawtooth
	{0x17, 0x0C},
	{0x18, 0x5C},
	{0x19, 0x01},
	{0x1A, 0x82},
	{0x03, 0x4f},//0x0F},changed: jqg20090415, for Night mode
	{0x32, 0x34},	 //H_Start / H_End LSB
	{0x2B, 0x00},
	{0x5C, 0x80},// AEC Full Zone Average

	// BLC
	{0x36, 0xB4},
	{0x65, 0x10},
	{0x70, 0x02},
	{0x71, 0x9C},
	{0x64, 0x24},

	//scaling
	{0xab, 0xe7},
	{0xb9, 0xa0},
	{0xba, 0x80},
	{0xbb, 0xa0},
	{0xbc, 0x80},

	//BLC
	{0x0D, 0x82},
	{0x0D, 0x80}

};

OV_CTT_t code gc_OV9663_CTT[3] =
{
	{3000,0x40,0x43,0x5C},
	{4050,0x4C,0x40,0x4E},
	{6500,0x60,0x44,0x40},
};


t_RegSettingBB code gc_OV9663_ISP_Setting[]=
{
	//AEC;	Average;9 zone;
	{0x5C, 0x80},
	{0x43, 0x00},
	{0x5D, 0x55},
	{0x5E, 0x57},
	{0x5F, 0x21},

	{0x03, 0x83}, //0x43 //night mode, 1/2 frame rate
	{0x0F, 0x4C},	// 0x4E
	{0x06, 0x50}, //AGC 4x trigger night mode
	{0x24, OV9663_AEW},
	{0x25, OV9663_AEB},
	{0x26, 0x72}, // 0x82

	//BF 60Hz
	{0x14, 0x28},	// 0x28 //AGC gain ceiling 16x, enable AEC<banding step when too bright
	{0x0C, 0x3C}, //0x38
	{0x4F, 0x9E},
	{0x50, 0x84},
	{0x5A, 0x23},  // 0x67

	//de-noise enable auto
	//{0xAB, 0xe7}, //___for scale on__JQG_20090806
	{0xb0, 0x43},
	{0xac, 0x04},
	{0x84, 0x80},	//0x40

	//sharpness
	{0xad, 0x82},
	{0xd9, 0x11},
	{0xda, 0x00},
	{0xae, 0x10},

	//UVave
	{0x74, 0xc0},

	//UVadj
	{0x7c, 0x28},
	{0x65, 0x11},
	{0x66, 0x00},
	{0x41, 0xc0},
	{0x5b, 0x24},
	{0x60, 0x82},
	{0x05, 0x07},
	{0x03, 0x83},//0x4F},changed: jqg20090415, for Night mode
	{0xd2, 0x94},

	//SAT & Brightness
	{0xc8, 0x06},
	{0xcb, 0x40},
	{0xcc, 0x40},
	{0xcf, 0x00},
	{0xd0, 0x20}, // (0x24)
	{0xd1, 0x00}, // (0x10)
	{0xc7, 0x18},

	//OV9663_LSC////2009_04_06 Setting by Pao-Chi
	{0x7d, 0xaf}, //X coordinate
	{0x7e, 0x97}, //Y coordinate, this value for ov9663_2_18
	{0x7f, 0x00}, //radius
	{0x80, 0x07}, //B channel
	{0x81, 0x09}, //R channel
	{0x82, 0x07}, //G channel or RGB channel
	{0x83, 0x07}, //LSC_ensable, RGB different coefficients

	//OV9663_AE//


	//OV9663_AWB////AWB advance, 2009_04_06 Setting by Pao-Chi
	{0x96, 0xf0},
	{0x97, 0x00},
	{0x92, 0x28},
	{0x94, 0x03},
	{0x93, 0x2c},
	{0x95, 0x48},
	{0x91, 0xaf},
	{0x90, 0xd4},
	{0x8e, 0x48},
	{0x8f, 0x63},
	{0x8d, 0x1b},
	{0x8c, 0x0c},
	{0x8b, 0x0d},
	{0x86, 0x9e},
	{0x87, 0x11},
	{0x88, 0x22}, // 0x22: advanced AWB; 0xa2:simple AWB
	{0x89, 0x05},
	{0x8a, 0x03},

	//OV9663_GAMMA//
	{0x9b, 0x0e},
	{0x9c, 0x1c},
	{0x9d, 0x34},
	{0x9e, 0x5a},
	{0x9f, 0x68},
	{0xa0, 0x76},
	{0xa1, 0x82},
	{0xa2, 0x8e},
	{0xa3, 0x98},
	{0xa4, 0xa0},
	{0xa5, 0xb0},
	{0xa6, 0xbe},
	{0xa7, 0xd2},
	{0xa8, 0xe2},
	{0xa9, 0xee},
	{0xaa, 0x18},

	//OV9663_CCM////CMX  ,2009_04_06 Setting by Pao-Chi
	{0xbd, 0x0a},
	{0xbe, 0x12},
	{0xbf, 0x05},
	{0xc0, 0x01},
	{0xc1, 0x25},
	{0xc2, 0x27},
	{0xc3, 0x25},
	{0xc4, 0x20},
	{0xc5, 0x05},
	{0xc6, 0x98},
	{0xc7, 0x80},
	{0x69, 0x48},
};

//find propriety setting
static OV_FpsSetting_t*  GetOvFpsSetting(U8 Fps, OV_FpsSetting_t staOvFpsSetting[], U8 byArrayLen)
{
	U8 i;
	U8 Idx;

	ISP_MSG((" array size = %bd\n", byArrayLen));

	if(g_bIsHighSpeed)
	{
		Idx=2; //8fps for uxga, 10 fps for vga
	}
	else
	{
		Idx = 0;
	}

	for(i=0; i< byArrayLen; i++)
	{
		ISP_MSG((" fps i = %bd\n",i));
		if(staOvFpsSetting[i].byFps ==Fps)
		{
			Idx=i;
			break;
		}
	}
	ISP_MSG((" get fps idx = %bd\n",Idx));
	return &staOvFpsSetting[Idx];
}

void GetSensorPclkHsync(U16 wSensorSPFormat, U8 byFps)
{
	OV_FpsSetting_t *pOV9663_FpsSetting;
	if (wSensorSPFormat == VGA_FRM)	
	{
		pOV9663_FpsSetting=GetOvFpsSetting(byFps, g_staOV9663VGAFpsSetting, sizeof(g_staOV9663VGAFpsSetting)/sizeof(OV_FpsSetting_t));		
		g_wSensorHsyncWidth =  pOV9663_FpsSetting->wExtraDummyPixel + 640+120;
	}
	else
	{
		pOV9663_FpsSetting=GetOvFpsSetting(byFps, g_staOV9663SXGAFpsSetting, sizeof(g_staOV9663SXGAFpsSetting)/sizeof(OV_FpsSetting_t));
		g_wSensorHsyncWidth =  pOV9663_FpsSetting->wExtraDummyPixel + 1280+240;
	}
	g_wSensorHsyncWidth =  pOV9663_FpsSetting->wExtraDummyPixel;
	g_dwPclk= pOV9663_FpsSetting->dwPixelClk;	
}

void OV9663SetFormatFps(U8 SetFormat, U8 Fps)
{
	U16 wTemp;
	OV_FpsSetting_t *pOV9663_FpsSetting;

	if((g_wSensorSPFormat & SetFormat) == VGA_FRM)
	{
		pOV9663_FpsSetting=GetOvFpsSetting(Fps, g_staOV9663VGAFpsSetting, sizeof(g_staOV9663VGAFpsSetting)/sizeof(OV_FpsSetting_t));
		// hemonel 2009-08-05: move to Resetsensor func() for still image time
		//	Write_SenReg_Mask(0x12, 0x80, 0x80); //reset all regs to default value
		//	WaitTimeOut_Delay(1);
		WriteSensorSettingBB(sizeof(gc_OV9663_VGA_Setting)>>1, gc_OV9663_VGA_Setting);

		Write_SenReg_Mask(0x11, (pOV9663_FpsSetting->byClkrc), 0x3F); // write CLKRC
		wTemp = pOV9663_FpsSetting->wExtraDummyPixel;
		Write_SenReg(0x2A, (INT2CHAR(wTemp,1))<<4);	//Write dummy pixel MSB
		Write_SenReg(0x2B, INT2CHAR(wTemp,0));		//Write dummy pixel LSB

		g_wAECExposureRowMax = 526;
		//wTemp += 640+120;	// total pixels number at one line
		GetSensorPclkHsync(VGA_FRM, Fps);
	}
	else
	{
		pOV9663_FpsSetting=GetOvFpsSetting(Fps, g_staOV9663SXGAFpsSetting, sizeof(g_staOV9663SXGAFpsSetting)/sizeof(OV_FpsSetting_t));
		// hemonel 2009-08-05: move to Resetsensor func() for still image time
		//	Write_SenReg_Mask(0x12, 0x80, 0x80); //reset all regs to default value
		//	WaitTimeOut_Delay(1);
		// SXGA
		WriteSensorSettingBB(sizeof(gc_OV9663_SXGA_Setting)>>1, gc_OV9663_SXGA_Setting);

		Write_SenReg_Mask(0x11, (pOV9663_FpsSetting->byClkrc), 0x3F); // write CLKRC
		wTemp = pOV9663_FpsSetting->wExtraDummyPixel;
		Write_SenReg(0x2A, (INT2CHAR(wTemp,1))<<4);	//Write dummy pixel MSB
		Write_SenReg(0x2B, INT2CHAR(wTemp,0));		//Write dummy pixel LSB

		g_wAECExposureRowMax = 1052;
		//wTemp += 1280+240;	// total pixels number at one line
		GetSensorPclkHsync(SXGA_FRM, Fps);
	}
	// hemonel 2010-01-13: move to here
	WriteSensorSettingBB(sizeof(gc_OV9663_ISP_Setting) >>1,  gc_OV9663_ISP_Setting);

	//g_wSensorHsyncWidth = wTemp;
	//g_dwPclk= pOV9663_FpsSetting->dwPixelClk;
}

void CfgOV9663ControlAttr(void)
{
	g_bySensorSize = SENSOR_SIZE_SXGA;
	g_wSensorSPFormat = SXGA_FRM | VGA_FRM;

	{
		memcpy(g_asOvCTT,gc_OV9663_CTT,sizeof(gc_OV9663_CTT));
	}

	{
		// AE backlight compensation parameter
		g_byOVAEW_Normal = OV9663_AEW;
		g_byOVAEB_Normal = OV9663_AEB;

		g_byOVAEW_BLC = g_byOVAEW_Normal + 0x20;
		g_byOVAEB_BLC = g_byOVAEB_Normal + 0x20;
	}

	{
		//g_bySV28VoltSel =  SV28_VOL_2V9;
		//g_bySV18VoltSel=  SV18_VOL_1V5;
		//	XBYTE[REG_TUNESVIO_SVA] =  SV28_VOL_2V9|SV18_VOL_1V5;
		// hemonel 2010-09-08: support 1.8V sensor IO

#if (_CHIP_ID_ & _RTS5840_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V79;
#elif (_CHIP_ID_ & _RTS5832_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V85;
#elif (_CHIP_ID_ & _RTS5829B_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V82;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V80;
#endif
	}

//	if(g_byFormatCfgExist==0) // format config not exist, use default setting.
	{
		InitFormatFrameFps();
	}


}

#ifndef _USE_BK_HSBC_ADJ_

void SetOV9663Brightness(S16 swSetValue)
{
	U8 byBrightSign;
	U16 wBrightVal;

	Write_SenReg_Mask(0xC8, 0x04, 0x04); //contrast en, from ovtool

	if(swSetValue>=0)
	{
		byBrightSign = 0x00;
		wBrightVal = swSetValue;
	}
	else
	{
		byBrightSign = 0x08;
		wBrightVal = -swSetValue;
	}
	Write_SenReg_Mask(0xC7, byBrightSign, 0x08); //from ovtool
	Write_SenReg(0xD1, wBrightVal);

}

void	SetOV9663Contrast(U16 wSetValue)
{
	Write_SenReg_Mask(0xC8, 0x04, 0x04); //contrast en
	Write_SenReg_Mask(0xC7, 0x34, 0x34); //from ovtool
	Write_SenReg_Mask(0x64, 0x02, 0x02); //from ovtool
	Write_SenReg(0xD0, wSetValue);

}
void	SetOV9663Saturation(U16 wSetValue)
{
	Write_SenReg_Mask(0xC8, 0x02, 0x02); //saturation en
	Write_SenReg(0xCB, wSetValue);
	Write_SenReg(0xCC, wSetValue);
}
void	SetOV9663Hue(S16 swSetValue)
{
	U8 bySign, byHueCos ,byHueSin;
	U16 wHue;
	float fHue;

	if(swSetValue>=0)
	{
		wHue = swSetValue;
		bySign = 0x12;
	}
	else
	{
		wHue = -swSetValue;
		bySign = 0x11;
	}

	fHue = (float)wHue/(float)180*3.1415926;

	byHueCos = cos(fHue)*128;
	byHueSin = sin(fHue)*128;

	Write_SenReg(0xC7, bySign);
	Write_SenReg_Mask(0xC8, 0x01, 0x01); //hue en
	Write_SenReg(0xC9, byHueCos);
	Write_SenReg(0xCA, byHueSin);
}
#endif

void	SetOV9663Sharpness(U8 bySetValue)
{
	U8 byRegAE = 0x20;//, byShrpen = 0x04;
	U8 byShrpGn0_2 , byShrpGn4_8 , byShrpGn8_16 ;

	switch (bySetValue)
	{
		//case 0:
		//	byShrpen = 0x00; //sharpness disable
		//	break;
	case 0:
		byShrpGn0_2 = 0x20;
		byShrpGn4_8 = 0x00;
		byShrpGn8_16 = 0x00;
		break;
	case 1:
		byShrpGn0_2 = 0x22;
		byShrpGn4_8 = 0x22;
		byShrpGn8_16 = 0x22;
		break;
	case 2:
		byShrpGn0_2 = 0x24;
		byShrpGn4_8 = 0x44;
		byShrpGn8_16 = 0x44;
		break;
	case 3: //default---recommend
		byShrpGn0_2 = 0x89;//0x98
		byShrpGn4_8 = 0x46; //0x49
		byShrpGn8_16 = 0x02;
		byRegAE = 0x10;
		break;
	case 4:
		byShrpGn0_2 = 0x26;
		byShrpGn4_8 = 0x66;
		byShrpGn8_16 = 0x66;
		break;
	case 5:
		byShrpGn0_2 = 0x28;
		byShrpGn4_8 = 0x88;
		byShrpGn8_16 = 0x88;
		break;
	case 6:
		byShrpGn0_2 = 0x2a;
		byShrpGn4_8 = 0xaa;
		byShrpGn8_16 = 0xaa;
		break;
	default:
		byShrpGn0_2 = 0x2c;
		byShrpGn4_8 = 0xcc;
		byShrpGn8_16 = 0xcc;
		break;
		/*case 7:
			byShrpGn0_2 = 0x2e;
			byShrpGn4_8 = 0xee;
			byShrpGn8_16 = 0xee;
			break;*/

	}
	//Write_SenReg_Mask(0xAB, byShrpen, 0x04);
	Write_SenReg(0xad, byShrpGn0_2);
	Write_SenReg(0xd9, byShrpGn4_8);
	Write_SenReg(0xda, byShrpGn8_16);
	Write_SenReg(0xae, byRegAE);
}

#ifndef _USE_BK_SPECIAL_EFFECT_
void SetOV9663Effect(U8 byEffect)
{
	U8 byEfft = 0x18, bySDE = 0x90; //SDE: special digital effect enable
	U8 byUvalue = 0x80, byVvalue = 0x80;

	switch (byEffect)
	{
	case SNR_EFFECT_NEGATIVE:
		byEfft = 0x40;
		//bySDE = 0x90;
		break;
	case SNR_EFFECT_MONOCHROME:
		//byEfft |= 0x20; //from spec
		//byEfft = 0x18; //from ovtool
		//bySDE = 0x90;
		break;
	case SNR_EFFECT_SEPIA:
		//byEfft = 0x18;
		//bySDE = 0x90;
		byUvalue = 0x40;
		byVvalue = 0xa0;
		break;
	case SNR_EFFECT_GREENISH:
		//byEfft = 0x18;
		//bySDE = 0x90;
		byUvalue = 0x60;
		byVvalue = 0x60;
		break;
	case SNR_EFFECT_REDDISH:
		//byEfft = 0x18;
		//bySDE = 0x90;
		byUvalue = 0x80;
		byVvalue = 0xc0;
		break;
	case SNR_EFFECT_BLUISH:
		//byEfft = 0x18;
		//bySDE = 0x90;
		byUvalue = 0xa0;
		byVvalue = 0x40;
		break;
	default:
		byEfft = 0x06; //no effect
		bySDE = 0x80;
		break;
	}
	Write_SenReg(0xC8, byEfft);
	Write_SenReg(0xC7, bySDE);
	Write_SenReg(0xCD, byUvalue);
	Write_SenReg(0xCE, byVvalue);
}
#endif

void SetOV9663Gain(float fGain)
{
}

void SetOV9663ImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.
{
	U8 byFlip; //, byMirror;
	switch (bySnrImgDir & 0x03)
	{
	case SNR_IMG_DIR_MIRROR:
		byFlip = 0x80;
		//byMirror = 0x88;
		break;
	case SNR_IMG_DIR_FLIP:
		byFlip = 0x40;
		//byMirror = 0x80;
		break;
	case SNR_IMG_DIR_FLIP_MIRROR:
		byFlip = 0xc0;
		//byMirror = 0x88;
		break;
	default: //normal
		byFlip = 0x00;
		//byMirror = 0x80;
		break;
	}

	Write_SenReg_Mask(0x04, byFlip, 0xC0);
	//Write_SenReg_Mask(0x33, byMirror, 0x08);
	Write_SenReg_Mask(0x33, ((bySnrImgDir&0x01)<<3), 0x08); //Mirror
}


/*
void SetOV9663WindowCrop(U16 wXStart,U16 wXEnd,U16 wYStart,U16 wYEnd )
{
}
*/

void SetOV9663OutputDim(U16 wWidth,U16 wHeight)
{

	U8 byInHor = 0x50 , byInVer = 0x3c;
	U8 byOutHor = 0x50, byOutVer = 0x3c;
	U8 byFifoDelay = 0x0;
	U8 bySc = 0xe7, byFifoEn = 0xe7, byFifoMode = 0x24;
	U16 wBkWinHeight = 0, wBkWinWidth = 0;

	if (wHeight < 480)
	{
		//byInHor = 0x50;
		//byInVer = 0x3c;
		bySc = 0xef;
		byFifoEn = 0xef;
		byFifoMode = 0x14;
		byFifoDelay = (U8)(((U32)(0x280 - wWidth) * wWidth)/0x280);
		byOutHor = (U8)(wWidth >>3);
		byOutVer = (U8)(wHeight >>3);

	}
	/*else if (wHeight == 480)
	{
		byInHor = 0x50;
		byInVer = 0x3c;
		byOutHor = 0x50;
		byOutVer = 0x3c;
	}
	*/
	else if (wHeight > 480)
	{
		byInHor = 0xa0;	//1280
		byOutHor = 0xa0;
		/*
		switch (wHeight)
		 {
		 	case 720:
				byInVer = 0x5a;
				byOutVer = 0x5a;
				Write_SenReg(0x19, 0x13);//720 start
				Write_SenReg(0x1a, 0x70);//720 end
				break;
		 	case 800:
				byInVer = 0x64;
				byOutVer = 0x64;
				Write_SenReg(0x19, 0x0e);//800 start
				Write_SenReg(0x1a, 0x74);//800 end
				break;
		 	case 1024:
			default:
				byInVer = 0x80;
				byOutVer = 0x80;
				//Write_SenReg(0x19, 0x01);//1024 start
				//Write_SenReg(0x1a, 0x82);//1024 end
				break;
		}
		*/
		// hemonel 2009-12-08: use backend image crop
		byInVer = 0x80;	//1024
		byOutVer = 0x80;
		wBkWinWidth = (1280-wWidth);
		wBkWinHeight = (1024-wHeight)/2;
	}

	Write_SenReg(0xab, bySc); //scale on
	Write_SenReg(0x85, byFifoEn);
	Write_SenReg(0x6a, byFifoMode);

	Write_SenReg(0xb9, byInHor); //horizontal input
	Write_SenReg(0xba, byInVer); //veritcal input

	Write_SenReg(0xBB, byOutHor); // h bit 10:3
	Write_SenReg(0xBC, byOutVer); // v bit 10:3
	Write_SenReg(0xd3,byFifoDelay);

	Write_SenReg_Mask(0xB7, 0x00, 0xF0);
	Write_SenReg_Mask(0xB8, 0x00, 0xC0);
#ifndef _MIPI_EXIST_
	SetBkWindowStart(wBkWinWidth, wBkWinHeight);
#endif
}

OV_BandingFilter_t  OV_SetOV9665BandingFilter(U8 byFps)
{
	OV_BandingFilter_t BdFilter;

	BdFilter.wD50Base= CalAntiflickerStep(byFps, 50, g_wAECExposureRowMax);
	BdFilter.byD50Step= 100/byFps -1;

	BdFilter.wD60Base= CalAntiflickerStep(byFps, 60, g_wAECExposureRowMax);
	BdFilter.byD60Step= 120/byFps -1;

	if(BdFilter.wD50Base<=0x35) //this value must be less than 0x35, for AE stable___jqg_20090805
	{
		BdFilter.wD50Base= CalAntiflickerStep(byFps, 25, g_wAECExposureRowMax*2);// ((U16)byFps*g_wAECExposureRowMax*2/(U16)(100/4)+1)/2;
		BdFilter.byD50Step= (100/4)/byFps -1;
	}

	if(BdFilter.wD60Base<=0x35) //this value must be less than 0x35, for AE stable___jqg_20090805
	{
		BdFilter.wD60Base= CalAntiflickerStep(byFps, 30, g_wAECExposureRowMax*2);//((U16)byFps*g_wAECExposureRowMax*2/(U16)(120/4)+1)/2;
		BdFilter.byD60Step= (120/4)/byFps -1;
	}

	return BdFilter;

}

void SetOV9663PwrLineFreq(U8 byFPS, U8 byLightFrq)
{
	U8 byBdSel, byFltrEn;
	OV_BandingFilter_t BdFilter;

	BdFilter = OV_SetOV9665BandingFilter(byFPS);

	if (byLightFrq == PWR_LINE_FRQ_50)
	{
		byFltrEn = 0x20;
		byBdSel = 0x04;
	}
	else if (byLightFrq == PWR_LINE_FRQ_60)
	{
		byFltrEn = 0x20;
		byBdSel = 0x00;
	}
	else
	{
		//for auto de-banding mode
		byFltrEn = 0x20;
		byBdSel = 0x02;

		//for disable de-banding mode
		//byFltrEn = 0x00;
	}

	Write_SenReg_Mask(0x0C, byBdSel, 0x06);
	Write_SenReg_Mask(0x13, byFltrEn, 0x20);

	//---set 50Hz banding filter---//
	Write_SenReg(0x4F, INT2CHAR(BdFilter.wD50Base,0));//BD50 8LSB
	Write_SenReg_Mask(0x4E, INT2CHAR(BdFilter.wD50Base,1)<<6, 0xC0);//BD50 2MSB_bit[7:6]
	Write_SenReg_Mask(0x5A, (BdFilter.byD50Step<<4), 0xF0);//BD50 step, bit[7:4]

	//---set 60Hz banding filter---//
	Write_SenReg(0x50, INT2CHAR(BdFilter.wD60Base,0));//BD60 8LSB
	Write_SenReg_Mask(0x4E, INT2CHAR(BdFilter.wD60Base,1)<<4, 0x30);//BD60 2MSB_bit[5:4]
	Write_SenReg_Mask(0x5A, BdFilter.byD60Step, 0x0F);//BD60 step, bit[3:0]

}

void SetOV9663WBTemp(U16 wRgain, U16 wGgain, U16 wBgain)
{
//	OV_CTT_t ctt;

//	ctt = OV_SetColorTemperature(wSetValue);

	Write_SenReg(0x02, wRgain);
	Write_SenReg(0x16, wGgain);
	Write_SenReg(0x01, wBgain);
}
/*
OV_CTT_t GetOV9663AwbGain(void)
{
	OV_CTT_t ctt;

	Read_SenReg(0x02, &ctt.wRgain);
	Read_SenReg(0x16, &ctt.wGgain);
	Read_SenReg(0x01, &ctt.wBgain);

	return ctt;
}
*/
void SetOV9663WBTempAuto(U8 bySetValue)
{
	if(bySetValue)
	{
		// atuo white balance
		Write_SenReg_Mask(0x13, 0x02,0x02);
	}
	else
	{
		// manual white balance
		Write_SenReg_Mask(0x13, 0x00,0x02);
	}
}


void SetOV9663BackLightComp(U16 bySetValue)
{
	U8 byAEW,byAEB;

	if(bySetValue)
	{
		byAEW = g_byOVAEW_BLC;
		byAEB = g_byOVAEB_BLC;
	}
	else
	{
		byAEW = g_byOVAEW_Normal;
		byAEB = g_byOVAEB_Normal;
	}
	Write_SenReg(0x24, byAEW);
	Write_SenReg(0x25, byAEB);
}

void SetOV9663IntegrationTimeAuto(U8 bySetValue)
{
	if(bySetValue == EXPOSURE_TIME_AUTO_MOD_APERTPRO)
	{
		// auto exposure
		Write_SenReg_Mask(0x0F, 0x08,  0x08);
		Write_SenReg_Mask(0x13, 0x05,  0x05);
	}
	else
	{
		// manual expsoure
		Write_SenReg_Mask(0x0F, 0x00,  0x08);
		Write_SenReg_Mask(0x13, 0x00,  0x05);
	}
}

void SetOV9663IntegrationTime(U16 wEspline)
{
	U16 wDummyline = 0;
	U16 wExpMax = g_wAECExposureRowMax-2;

	if(wEspline > wExpMax)
	{
		wDummyline = wEspline - wExpMax;
		wEspline = wExpMax;
	}

	DBG_SENSOR(("set wEspline value = %u\n", wEspline));
	DBG_SENSOR(("set wDummyline value = %u\n", wDummyline));
	// write exposure time and dummy line to register
	Write_SenReg_Mask(0x04, (wEspline&0x03),0x03);
	Write_SenReg(0x10, (wEspline>>2));
	Write_SenReg_Mask(0x45, (wEspline>>10),0x3F);
	Write_SenReg(0x2D, INT2CHAR(wDummyline, 0));
	Write_SenReg(0x2E, INT2CHAR(wDummyline, 1));
}
#endif
