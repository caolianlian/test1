#ifndef _MI1040_H_
#define _MI1040_H_

#define MI1040_AEW  0x44
#define MI1040_AEB  0x38
#define MI1040_VV   0x72
#define MI1040REG_HOSTCMD   0x0080
#define MI1040_HOSTCMD_OK   0x8000
#define MI1040_HOSTCMD_0    0x0001
#define MI1040_HOSTCMD_1    0x0002
#define MI1040_HOSTCMD_2    0x0004

typedef struct MI1040_FpsSetting
{
	U8 byFps;
	U16 wPLLCtlM_N; //  M,N parameter of PLL
	U16 wPLLPara_P; //  p parameter of PLL
	U16 wHorizWidth;   // cam_sensor_cfg_line_length_pck
	U16 wVertHeight;   //  cam_sensor_cfg_frame_length_lines
	U16 wfine_integ_time_min; //sensor_cfg_fine_integ_time_min
	U16 wfine_integ_time_max; //sensor_cfg_fine_integ_time_max
	U32 dwPixelClk;
	U16 waet_max_frame_rate;
	U16 waet_min_frame_rate;
} MI1040_FpsSetting_t;


void MI1040SetFormatFps(U16 SetFormat, U8 Fps);
void CfgMI1040ControlAttr(void);
void SetMI1040ImgDir(U8 bySnrImgDir);
void SetMI1040IntegrationTime(U16 wEspline);
void SetMI1040Exposuretime_Gain(float fExpTime, float fTotalGain);
void InitMI1040IspParams();
void MI1040_POR();
void SetMI1040DynamicISP(U8 byAEC_Gain);
void SetMI1040DynamicISP_AWB(U16 wColorTempature);
void SetMI1040Gain(float fGain);
#endif // _MI1040_H_

