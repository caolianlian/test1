#ifndef _T4K71_H_
#define _T4K71_H_

typedef struct T4K71_FpsSetting
{
	U8 byFps;
	U16 wExtraDummyPixel;   // register 0x2a,0x2b: dummy pixel adjustment
	U8  PLL_Multi; // register 0x11: clock pre-scalar
	U8  OP_CLK_Div;
	U8  SYS_CLK_Div;
	U8  Pix_Clk_Div;
	U32 dwPixelClk;
} T4K71_FpsSetting_t;


void T4K71SetFormatFps(U16 SetFormat, U8 Fps);
void CfgT4K71ControlAttr(void);
void SetT4K71ImgDir(U8 bySnrImgDir);
void SetT4K71IntegrationTime(U16 wEspline);
void SetT4K71Exposuretime_Gain(float fExpTime, float fTotalGain);
void InitT4K71IspParams();
void T4K71_POR();
void InitT4K71IspParams();
void SetT4K71DynamicISP(U8 bymode);
void SetT4K71DynamicISP_AWB(U16 wColorTempature);
#endif // _OV9710_H_

