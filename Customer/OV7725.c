#include "Inc.h"

#ifdef RTS58XX_SP_OV7725

code OV_CTT_t gc_OV7725_CTT[3] =
{
	{3000,0x40,0x5E,0xCF},
	{4050,0x44,0x40,0x7A},
	{6500,0x4b,0x40,0x48},
};

// hemonel 2009-03-17: tune fps ok
// row = 510
// column = pclk/(row*fps)
// extra dummy = column-numer per line(784)
code OV_FpsSetting_t  g_staOV7725FpsSetting[]=
{
	//  FPS	extradummypixel clkrc	pclk = 24M/(clkrc+1)
	//	{1,		(1176+1),	(23),	  1000000},	// 1M
	{3,		(523+1),	(11),	  2000000},	// 2M	// 2.998
	{5, 		(0),		(11),	  2000000},	// 2M
	//	{8,  		(196+1),	(5),		  4000000},	// 4M	// 7.995
	//	{9,  		(87+1),	(5),		  4000000},	// 4M	// 8.995
	{10,		(0),		(5),		  4000000},	// 4M
	{15,		(0),		(3),		  6000000},	// 6M
	{20,		(0),		(2),		  8000000},	// 8M
	{25,		(157),	(1),		12000000},	// 12M
	{30,		(0),		(1),		12000000},	// 12M
};

t_RegSettingBB code gc_OV7725_Setting[] =
{
	//{0x12, 0x80},// reset
	{0x3D, 0x03},	// DC offset for analog process

	// sensor output window: 656*480, start(136,14)
	{0x17, 0x22},
	{0x18, 0xA4},
	{0x19, 0x07},
	{0x1A, 0xF0},
	{0x32, 0x00},

	// output size 640*480+1
	{0x29, 0xA0},
	{0x2C, 0xF0},
	{0x2A, 0x00},

	// CLKRC
	{0x11, 0x01}, //0x03 for 15fps. integration time x2
	{0x42, 0x7F}, // BLC blue channel target value
	{0x4D, 0x09}, // G channel gain 1x, B channel 1.5x, R channel 1.25x
	{0x63, 0xE0}, // AWB control
	{0x64, 0xFF}, // enable all // 0xff: enable FIFO, UV adjust, SDE, CCM, interpolation, gamma,black dead pixel, white dead pixel
	{0x65, 0x20}, // DSP control2
	{0x66, 0x00}, // DSP control3
	{0x67, 0x48}, //output YUV or RGB, AEC before gamma

	{0x0D, 0x41}, // PLL 4X
	{0x0F, 0xC5},
	{0x14, 0x21}, // gain ceiling 4x, average based AEC
	{0x22, 0x7F}, // banding AEC value
	{0x23, 0x03},

	{0x26, 0xA1}, // VPT
	{0x2B, 0x00}, // dummy pixel insert 0
	{0x6B, 0xAA}, // simple AWB
	{0x13, 0xFF},//0xE7}, // enable AWB,AEC,AGC,------0xe7---jqg 20090409

	{0x0E, 0x99}, //Night mode, AGC 8X, 1/2 frame rate

	// sharp and denoise parameter
	{0x90, 0x05},
	{0x91, 0x01},
	{0x92, 0x03}, //0x05},  0x03 from ovtool -- jqg 20090408
	{0x93, 0x00},

	//SDE
	{0x9B, 0x08}, // brightness
	{0x9C, 0x20}, // contrast
	{0x9E, 0x81},//0x00}, // uv adjust
	{0x9F, 0xfa},//0x00},
	// hemonel 2008-11-26: enable saturation , enable hue, enable brightness and contrast
	{0xA6, 0x07}, // enable brightness and contrast
	{0xAC, 0xFF},	//  auto sharp

	{0x30, 0x5e},
	{0x35, 0x79}, //from ovtool--jqg--20090409
	{0x36, 0x7e},
	{0x37, 0x79},
	{0x38, 0x79},
	{0x60, 0x00},
	{0x61, 0x05},
	{0xa6, 0x04},

};


t_RegSettingBB code gc_OV7725_ISP_Setting[]=
{

	//OV7725_LSC CWF 100%//
	{0x47, 0x10}, //0x0f}, //X coordinate this XY for ov7725_2_014
	{0x48, 0x00}, //0x0a}, //Y coordinate, this XY for ov7725_2_021
	{0x49, 0x1C}, //G channel or RGB channel
	{0x4a, 0x01}, //radius
	{0x4b, 0x1C}, //R channel
	{0x4c, 0x28}, //B channel
	{0x46, 0x05}, //LSC_ensable, RGB different coefficients

	//OV7725_AE//
	{0x24, OV7725_AEW}, // AEW {0x24, 0x40},
	{0x25, OV7725_AEB}, //AEB {0x25, 0x30},

	//OV7725_AWB//
	{0x76, 0x14}, //data bot
	{0x75, 0xf0}, //data top
	{0x77, 0x28}, //day lmt
	{0x79, 0x64}, //day spt
	{0x78, 0x0b}, //a lmt
	{0x7a, 0x3c}, //a spt
	{0x72, 0x84}, //day ky
	{0x71, 0xaa}, //a kx
	{0x73, 0x07}, //a ec
	{0x74, 0x06}, //day fc
	{0x6e, 0x72}, //cwf xct
	{0x6f, 0x4c}, //cwf yct
	{0x70, 0x1f}, //cwf ss
	{0x6c, 0x11},
	{0x6d, 0x50},
	{0x6b, 0x15}, //awb simple advanced
	{0x6a, 0x00},
	{0x69, 0x9d},//0x1e}, //awb gain max gain    from ovtool

	//OV7725_GAMMA; default//
	{0x7E, 0x0C},
	{0x7F, 0x16},
	{0x80, 0x2A},
	{0x81, 0x4E},
	{0x82, 0x61},
	{0x83, 0x6F},
	{0x84, 0x7B},
	{0x85, 0x86},
	{0x86, 0x8E},
	{0x87, 0x97},
	{0x88, 0xA4},
	{0x89, 0xAF},
	{0x8A, 0xC5},
	{0x8B, 0xD7},
	{0x8C, 0xE8},
	{0x8D, 0x20},

	//OV7725_CCM_D65//
	{0x97, 0x11},
	{0x98, 0x6e},
	{0x99, 0x7f},
	{0x94, 0x5e},
	{0x95, 0x4e},
	{0x96, 0x10},
	{0x9a, 0x1e},

};

//find propriety setting
static OV_FpsSetting_t*  GetOvFpsSetting(U8 Fps, OV_FpsSetting_t staOvFpsSetting[], U8 byArrayLen)
{
	U8 i;
	U8 Idx;

	ISP_MSG((" array size = %bd\n", byArrayLen));

	if(g_bIsHighSpeed)
	{
		Idx=2; //8fps for uxga, 10 fps for vga
	}
	else
	{
		Idx = 0;
	}

	for(i=0; i< byArrayLen; i++)
	{
		ISP_MSG((" fps i = %bd\n",i));
		if(staOvFpsSetting[i].byFps ==Fps)
		{
			Idx=i;
			break;
		}
	}
	ISP_MSG((" get fps idx = %bd\n",Idx));
	return &staOvFpsSetting[Idx];
}

void GetSensorPclkHsync(U16 wSensorSPFormat, U8 byFps)
{
	OV_FpsSetting_t *pOv7725_FpsSetting;
	pOv7725_FpsSetting=GetOvFpsSetting(byFps, g_staOV7725FpsSetting, sizeof(g_staOV7725FpsSetting)/sizeof(OV_FpsSetting_t));
	g_wSensorHsyncWidth = 784+pOv7725_FpsSetting->wExtraDummyPixel;
	g_dwPclk= pOv7725_FpsSetting->dwPixelClk;	
}

void OV7725SetFormatFps(U8 SetFormat, U8 Fps)
{
	OV_FpsSetting_t *pOv7725_FpsSetting;

	SetFormat = SetFormat;	// for delete warning
	pOv7725_FpsSetting=GetOvFpsSetting(Fps, g_staOV7725FpsSetting, sizeof(g_staOV7725FpsSetting)/sizeof(OV_FpsSetting_t));

	// initial all register setting
	WriteSensorSettingBB(sizeof(gc_OV7725_Setting)>>1, gc_OV7725_Setting);
	// hemonel 2010-01-13: move to here
	WriteSensorSettingBB(sizeof(gc_OV7725_ISP_Setting) >>1, gc_OV7725_ISP_Setting);

	// 2) write sensor register for fps
	Write_SenReg_Mask(0x11, pOv7725_FpsSetting->byClkrc, 0x3F);	// write CLKRC
	Write_SenReg(0x2B, INT2CHAR(pOv7725_FpsSetting->wExtraDummyPixel,0));	//Write dummy pixel LSB
	Write_SenReg_Mask(0x2A, ((pOv7725_FpsSetting->wExtraDummyPixel)>>4), 0xF0);	// Writedummy pixel MSB

	// 3) update variable for AE banding filter
	g_wAECExposureRowMax = 510-2;
	//g_wSensorHsyncWidth = 784+pOv7725_FpsSetting->wExtraDummyPixel;
	//g_dwPclk= pOv7725_FpsSetting->dwPixelClk;
	GetSensorPclkHsync(VGA_FRM, Fps);
}

void CfgOV7725ControlAttr(void)
{
	U8 i;

	g_bySensorSize = SENSOR_SIZE_VGA;
	g_wSensorSPFormat = VGA_FRM;

	{
		memcpy(g_asOvCTT,gc_OV7725_CTT,sizeof(gc_OV7725_CTT));
	}

	{
		// AE backlight compensation parameter
		g_byOVAEW_Normal = OV7725_AEW;
		g_byOVAEB_Normal = OV7725_AEB;

		g_byOVAEW_BLC = g_byOVAEW_Normal + 0x20;
		g_byOVAEB_BLC = g_byOVAEB_Normal + 0x20;
	}

	{
		//g_bySV28VoltSel =  SV28_VOL_3V0;	// 2009-05-13: conference deside ov7740 use 3.3v, not use 3.3v vubs power
		//	XBYTE[REG_TUNESVIO_SVA] =  SV28_VOL_3V0|SV18_VOL_1V8;
		// hemonel 2010-09-08: support 1.8V sensor IO
#if (_CHIP_ID_ & _RTS5840_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_3V04;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V79;
#elif (_CHIP_ID_ & _RTS5832_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_3V00;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V85;
#elif (_CHIP_ID_ & _RTS5829B_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_3V00;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V80;
#endif
	}

//	if(g_byFormatCfgExist==0) // format config not exist, use default setting.
	{
		//====== resolution  setting ===========
		// -- preview ---
		g_aVideoFormat[0].byaVideoFrameTbl[0] = 6;		// resolution number
		g_aVideoFormat[0].byaVideoFrameTbl[1]=F_SEL_640_480;
		g_aVideoFormat[0].byaVideoFrameTbl[2]=F_SEL_160_120;
		g_aVideoFormat[0].byaVideoFrameTbl[3]=F_SEL_176_144;
		g_aVideoFormat[0].byaVideoFrameTbl[4]=F_SEL_320_240;
		g_aVideoFormat[0].byaVideoFrameTbl[5]=F_SEL_352_288;
		g_aVideoFormat[0].byaVideoFrameTbl[6]=F_SEL_640_480;
		//--still image--
		g_aVideoFormat[0].byaStillFrameTbl[0] = 6;	// resolution number
		g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_640_480;
		g_aVideoFormat[0].byaStillFrameTbl[2]=F_SEL_160_120;
		g_aVideoFormat[0].byaStillFrameTbl[3]=F_SEL_176_144;
		g_aVideoFormat[0].byaStillFrameTbl[4]=F_SEL_320_240;
		g_aVideoFormat[0].byaStillFrameTbl[5]=F_SEL_352_288;
		g_aVideoFormat[0].byaStillFrameTbl[6]=F_SEL_640_480;

		//====== fps  setting ===========
		for(i=0; i<7; i++)
		{
			/*640_480,
			160_120,
			176_144,
			320_200,
			320_240,
			352_288,
			640_400*/
			g_aVideoFormat[0].waFrameFpsBitmap[i]= FPS_15|FPS_20|FPS_25|FPS_30;
		}

		//====== format type  setting ===========
		g_aVideoFormat[0].byFormatType = FORMAT_TYPE_YUY2;

#ifdef _ENABLE_MJPEG_
		// copy YUY2 setting to MJPEG setting
		memcpy(&(g_aVideoFormat[1]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

		// modify MJPEG format type
		g_aVideoFormat[1].byFormatType = FORMAT_TYPE_MJPG;
#endif

#ifdef _ENABLE_M420_FMT_
		// copy YUY2 setting to M420 setting
		memcpy(&(g_aVideoFormat[2]),&(g_aVideoFormat[0]),sizeof(g_aVideoFormat[0]));

		// modify M420 format type
		g_aVideoFormat[2].byFormatType = FORMAT_TYPE_M420;
#endif
	}


}

#ifndef _USE_BK_HSBC_ADJ_
void SetOV7725Brightness(S16 swSetValue)
{
	U16 wBrightSign;
	U16 wBrightVal;

	Read_SenReg(0xAB, &wBrightSign);
	if(swSetValue>=0)
	{
		wBrightSign &= 0xF7;	// Brightness sign positive
		wBrightVal = swSetValue;
	}
	else
	{
		wBrightSign |= 0x08;		// Brightness sign negative
		wBrightVal = -swSetValue;
	}
	Write_SenReg(0xAB, wBrightSign);	// write Brightness sign
	Write_SenReg(0x9B, wBrightVal);

	return;
}


void	SetOV7725Contrast(U16 wSetValue)
{
	Write_SenReg(0x9C, wSetValue);

	return;
}

void	SetOV7725Saturation(U16 wSetValue)
{
	Write_SenReg(0xA7, wSetValue);
	Write_SenReg(0xA8, wSetValue);

	return;
}

void	SetOV7725Hue(S16 swSetValue)
{
	U8 byHueCos,byHueSin;
	U16 wHue,wHueSinSign;
	float fHue;

	Read_SenReg(0xAB, &wHueSinSign);
	if(swSetValue>=0)
	{
		wHueSinSign &= 0xFC;
		wHue = swSetValue;
	}
	else
	{
		wHueSinSign |= 0x03;
		wHue = -swSetValue;
	}
	fHue = (float)wHue/(float)180*3.1415926;
	byHueCos = cos(fHue)*128;
	byHueSin = sin(fHue)*128;

	Write_SenReg(0xAB, wHueSinSign);
	Write_SenReg(0xA9, byHueCos);
	Write_SenReg(0xAA, byHueSin);

	return;
}
#endif
// manual sharpness mode
void	SetOV7725Sharpness(U8 bySetValue)
{
	if(bySetValue == 4)
	{
		// auto sharp mode
		Write_SenReg_Mask(0xac,0x20,0x20);
	}
	else
	{
		// manual sharp mode
		Write_SenReg_Mask(0xac,0x00,0x20);
		Write_SenReg_Mask(0x8F, bySetValue, 0x1F);
	}
}

#ifndef _USE_BK_SPECIAL_EFFECT_
void SetOV7725Effect(U8 byEffect)
{

	U8 /*byEfft = 0x20,*/ bySDE = 0x20; //SDE: special digital effect enable
	U8 byUvalue = 0x80, byVvalue = 0x80;

	switch (byEffect)
	{
	case SNR_EFFECT_NEGATIVE:
		//byEfft = 0x20;
		bySDE = 0x40;
		break;
	case SNR_EFFECT_MONOCHROME:
		//byEfft = 0x20;
		//bySDE = 0x20;
		break;
	case SNR_EFFECT_SEPIA:
		//byEfft = 0x20;
		bySDE = 0x18;
		byUvalue = 0x40;
		byVvalue = 0xa0;
		break;
	case SNR_EFFECT_GREENISH:
		//byEfft = 0x20;
		bySDE = 0x18;
		byUvalue = 0x60;
		byVvalue = 0x60;
		break;
	case SNR_EFFECT_REDDISH:
		//byEfft = 0x20;
		bySDE = 0x18;
		byUvalue = 0x80;
		byVvalue = 0xc0;
		break;
	case SNR_EFFECT_BLUISH:
		//byEfft = 0x20;
		bySDE = 0x18;
		byUvalue = 0xa0;
		byVvalue = 0x40;
		break;
	default:
		//byEfft = 0x0; //no effect
		bySDE = 0x04;
		break;
	}
	//Write_SenReg_Mask(0x64, byEfft, 0x20);
	Write_SenReg_Mask(0xa6, bySDE, 0xf8);
	Write_SenReg(0x60, byUvalue);
	Write_SenReg(0x61, byVvalue);

}
#endif

void SetOV7725Gain(float fGain)
{
}

void SetOV7725ImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.
{
	/***
		0x40 mirror
		0x80 flip
		0xc0 mirror and flip
	***/
	Write_SenReg_Mask(0x0C, (bySnrImgDir&0x03)<<6,0xC0);
}

/*void SetOV7725WindowCrop(U16 wXStart,U16 wXEnd,U16 wYStart,U16 wYEnd )
{
	//DBG(("@12-->%d,%d,%d,%d\n",wXStart, wXEnd, wYStart, wYEnd));

	// recalculate window
	wXEnd = wXEnd - wXStart +16;	// width
	wYEnd = wYEnd - wYStart;	// height
	wXStart += 136;
	wYStart += 14;

	// update window
	Write_SenReg(0x17, wXStart>>2);
	Write_SenReg(0x18, wXEnd>>2);
	Write_SenReg(0x19, wYStart>>1);
	Write_SenReg(0x1A, wYEnd>>1);

	Write_SenReg_Mask(0x32, ((wYStart&0x01)<<6)|((wXStart&0x03)<<4)|((wYEnd&0x01)<<2)|(wXEnd&0x03), 0x77);
	return;
}

void SetOV7725OutputDim(U16 wWidth,U16 wHeight)
{
	U16 wTmp;

	Read_SenReg(0x65, &wTmp);
	if((wWidth != 640)&&(wHeight != 480))
	{	// need scale
		Write_SenReg(0x65, wTmp|0x0F);	// enable DCW and Zoom
	}
	else
	{	// only crop
		Write_SenReg(0x65, wTmp&0xF0);	// disable DCW and Zoom
	}

	Write_SenReg(0x29, wWidth>>2);	// HoutSize[9:2]
	Write_SenReg(0x2C, wHeight>>1);	// VoutSize[8:1]
	Write_SenReg_Mask(0x2A, ((wHeight&0x01)<<2)|(wWidth&0x03), 0x07);	// HoutSize[1:0], VoutSize[0]
}*/

U16 CalAntiflickerStep(U8 byFps, U8 byFrq, U16 wRowNumber)
{
	return ((U16)byFps * wRowNumber/(U16)byFrq+1)/2; // round to integer
}

OV_BandingFilter_t  OV_SetBandingFilter(U8 byFps)
{
	OV_BandingFilter_t BdFilter;

	BdFilter.wD50Base= CalAntiflickerStep(byFps, 50, g_wAECExposureRowMax);//((U16)byFps*g_wAECExposureRowMax/(U16)50+1)/2;
	BdFilter.byD50Step= 100/byFps -1;

	BdFilter.wD60Base= CalAntiflickerStep(byFps, 60, g_wAECExposureRowMax);//((U16)byFps*g_wAECExposureRowMax/(U16)60+1)/2;
	BdFilter.byD60Step= 120/byFps -1;

	if(BdFilter.wD50Base<0x10)
	{
		BdFilter.wD50Base= CalAntiflickerStep(byFps, 25, g_wAECExposureRowMax);//(U16)byFps*g_wAECExposureRowMax/(U16)(100/2);
		BdFilter.byD50Step= (100/2)/byFps -1;
	}

	if(BdFilter.wD60Base<0x10)
	{
		BdFilter.wD60Base= CalAntiflickerStep(byFps, 30, g_wAECExposureRowMax);//(U16)byFps*g_wAECExposureRowMax/(U16)(120/2);
		BdFilter.byD60Step= (120/2)/byFps -1;
	}

	return BdFilter;

}

void SetOV7725PwrLineFreq(U8 byFPS, U8 byLightFrq)
{
	OV_BandingFilter_t BdFilter;

	// hemonel 2009-07-06: optimize code
	BdFilter = OV_SetBandingFilter(byFPS);

	if(PWR_LINE_FRQ_DIS == byLightFrq)
	{
		// disable banding filter
		Write_SenReg_Mask(0x13, 0x00, 0x20);
	}
	else
	{
		// enable banding filter
		Write_SenReg_Mask(0x13, 0x20, 0x20);
		/*
		// calculate banding filter value and step
		if(PWR_LINE_FRQ_50 == byLightFrq)
		{
			byBandingTime = 100;
		}
		else
		{
			byBandingTime = 120;
		}
		byBandingFilterVal = (U16)byFPS*g_wAECExposureRowMax/(U16)byBandingTime;
		byBandingFilterStep = byBandingTime/byFPS -1;

		// hemonel 2009-02-18 bug: AE not work when fps too slow
		// banding filter value must be more than 0x10
		if(byBandingFilterVal<0x10)
		{
			byBandingTime>>=1;
			byBandingFilterVal = (U16)byFPS*g_wAECExposureRowMax/(U16)byBandingTime;
			byBandingFilterStep = byBandingTime/byFPS -1;
		}
		*/
		if(PWR_LINE_FRQ_50 == byLightFrq)
		{
			Write_SenReg(0x22, BdFilter.wD50Base);
			Write_SenReg(0x23, BdFilter.byD50Step);
		}
		else
		{
			Write_SenReg(0x22, BdFilter.wD60Base);
			Write_SenReg(0x23, BdFilter.byD60Step);
		}
	}
}

void SetOV7725WBTemp(U16 wRgain, U16 wGgain, U16 wBgain)
{
//	OV_CTT_t ctt;

//	ctt = OV_SetColorTemperature(wSetValue);

	Write_SenReg(0x02, wRgain);
	Write_SenReg(0x03, wGgain);
	Write_SenReg(0x01, wBgain);
}
/*
OV_CTT_t GetOV7725AwbGain(void)
{
	OV_CTT_t ctt;

	Read_SenReg(0x02, &ctt.wRgain);
	Read_SenReg(0x03, &ctt.wGgain);
	Read_SenReg(0x01, &ctt.wBgain);

	return ctt;
}
*/
void SetOV7725WBTempAuto(U8 bySetValue)
{
	if(bySetValue)
	{
		// atuo white balance
		Write_SenReg_Mask(0x13, 0x02,0x02);
	}
	else
	{
		// manual white balance
		Write_SenReg_Mask(0x13, 0x00,0x02);
	}
}

void SetOV7725BackLightComp(U8 bySetValue)
{
	U8 byAEW,byAEB;

	if(bySetValue)
	{
		byAEW = g_byOVAEW_BLC;
		byAEB = g_byOVAEB_BLC;
	}
	else
	{
		byAEW = g_byOVAEW_Normal;
		byAEB = g_byOVAEB_Normal;
	}
	Write_SenReg(0x24, byAEW);
	Write_SenReg(0x25, byAEB);
}

void SetOV7725IntegrationTimeAuto(U8 bySetValue)
{
	if(bySetValue == EXPOSURE_TIME_AUTO_MOD_APERTPRO)
	{
		// auto exposure
		Write_SenReg_Mask(0x0E, 0x80,  0x80);		// enable night mode
		Write_SenReg_Mask(0x13, 0x05,  0x05);
	}
	else
	{
		// manual expsoure
		Write_SenReg_Mask(0x0E, 0x00,  0x80);		// close night mode
		Write_SenReg_Mask(0x13, 0x00,  0x05);
	}
}

void SetOV7725IntegrationTime(U16 wEspline)
{
	U16 wDummyline = 0;
	U16 wExpMax = g_wAECExposureRowMax-2;

	if(wEspline > wExpMax)
	{
		wDummyline = wEspline - wExpMax;
		wEspline = wExpMax;
	}

	DBG_SENSOR(("set wEspline value = %u\n", wEspline));
	DBG_SENSOR(("set wDummyline value = %u\n", wDummyline));
	// write exposure time and dummy line to register
	Write_SenReg(0x10, INT2CHAR(wEspline, 0));
	Write_SenReg(0x08, INT2CHAR(wEspline, 1));
	Write_SenReg(0x2D, INT2CHAR(wDummyline, 0));
	Write_SenReg(0x2E, INT2CHAR(wDummyline, 1));
}
#endif
