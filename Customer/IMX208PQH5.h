#ifndef _IMX208PQH5_H_
#define _IMX208PQH5_H_

void IMX208PQH5SetFormatFps(U16 SetFormat, U8 Fps);
void CfgIMX208PQH5ControlAttr(void);
void SetIMX208PQH5ImgDir(U8 bySnrImgDir);
void SetIMX208PQH5IntegrationTime(U16 wEspline);
void SetIMX208PQH5Exposuretime_Gain(float fExpTime, float fTotalGain);
void InitIMX208PQH5IspParams(void );
void InitIMX208PQH5IspParamsByResolution(U16 wCurWidth, U16 wCurHeight);
void IMX208PQH5_POR(void );
void SetIMX208PQH5DynamicISP(U8 byAEC_Gain);
void SetIMX208PQH5DynamicISP_AWB(U16 wColorTempature);
#endif // _IMX208PQH5_H_

