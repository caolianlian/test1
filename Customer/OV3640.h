#ifndef _OV3640_H_
#define _OV3640_H_

#define OV3640_AEW	0x78// 0x78 ---jqg 20090422
#define OV3640_AEB	0x68// 0x68
#define OV3640_VPT	0xd4

typedef struct OV3640_FpsSetting
{
	U8 byFps;
	U16 wFrameratelmax;   // samsung5CA framerate
	U16 wFrameratelmin;
	U8  byClk; // samsung5CA ISPclk
	U32 dwPixelClk;
} OV3640_FpsSetting_t;

void OV3640_POR();
void SetOV3640Fps(U16 SetFormat, U8 Fps);
void CfgOV3640ControlAttr(void);
void SetOV3640Brightness(S16 swSetValue);
void	SetOV3640Contrast(U16 wSetValue);
void	SetOV3640Saturation(U16 wSetValue);
void	SetOV3640Hue(S16 swSetValue);
void	SetOV3640Sharpness(U8 bySetValue);
void SetOV3640Effect(U8 byEffect);
void SetOV3640ImgDir(U8 bySnrImgDir);
void SetOV3640WindowCrop(U16 wXStart,U16 wXEnd,U16 wYStart,U16 wYEnd );
void SetOV3640OutputDim(U16 wWidth,U16 wHeight);
void SetOV3640PwrLineFreq(U8 byFPS, U8 byLightFrq);
void SetOV3640WBTemp(U16 wRgain, U16 wGgain, U16 wBgain);
void SetOV3640WBTempAuto(U8 bySetValue);
void SetOV3640BackLightComp(U8 bySetValue);

extern code OV_CTT_t gc_OV3640_CTT[3];
void SetOV3640IntegrationTimeAuto(U8 bySetValue);
void SetOV3640IntegrationTime(U16 wEspline);
void SetOV3640LowLightComp(U8 bySetValue);
void OV3640PreviewToCapture(U16 wPreviewWidth, U16 wStillWidth);
void OV3640CaptureToPreview();
#endif // _OV775_H_
