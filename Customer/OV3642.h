#ifndef _OV3642_H_
#define _OV3642_H_

#define OV3642_AEW	0x78// 0x78 ---jqg 20090422
#define OV3642_AEB	0x68// 0x68
#define OV3642_VPT	0xd4

typedef struct OV3642_FpsSetting
{
	U8 byFps;
	U8  byClkrc; // register 0x11: clock pre-scalar
	U8 byPLL;
	U8 byExtraDummyPixel;   // register 0x2a,0x2b: dummy pixel adjustment
	U32 dwPixelClk;
} OV3642_FpsSetting_t;
void OV3642_POR();
void OV3642SetFormatFps(U16 SetFormat, U8 Fps);
void CfgOV3642ControlAttr(void);
void SetOV3642Brightness(S16 swSetValue);
void	SetOV3642Contrast(U16 wSetValue);
void	SetOV3642Saturation(U16 wSetValue);
void	SetOV3642Hue(S16 swSetValue);
void	SetOV3642Sharpness(U8 bySetValue);
void SetOV3642Effect(U8 byEffect);
void SetOV3642ImgDir(U8 bySnrImgDir);
void SetOV3642WindowCrop(U16 wXStart,U16 wXEnd,U16 wYStart,U16 wYEnd );
void SetOV3642OutputDim(U16 wWidth,U16 wHeight);
void SetOV3642PwrLineFreq(U8 byFPS, U8 byLightFrq);
void SetOV3642WBTemp(U16 wRgain, U16 wGgain, U16 wBgain);
void SetOV3642WBTempAuto(U8 bySetValue);
void SetOV3642BackLightComp(U8 bySetValue);

extern code OV_CTT_t gc_OV3642_CTT[3];
void SetOV3642IntegrationTimeAuto(U8 bySetValue);
void SetOV3642IntegrationTime(U16 wEspline);
void SetOV3642LowLightComp(U8 bySetValue);
void OV3642PreviewToCapture(U16 wPreviewWidth, U16 wStillWidth);
void OV3642CaptureToPreview();
#endif // _OV775_H_

