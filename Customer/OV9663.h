#ifndef _OV9663_H_
#define _OV9663_H_

#define OV9663_AEW	0x44	// 0x3E, change to 0x44 for AE stable
#define OV9663_AEB	0x38
#define OV9663_VV	0x72	//0x82 //Add: jqg---20090422

void OV9663SetFormatFps(U8 SetFormat, U8 Fps);
void CfgOV9663ControlAttr(void);
extern code OV_CTT_t gc_OV9663_CTT[3];
void SetOV9663Brightness(S16 swSetValue);
void	SetOV9663Contrast(U16 wSetValue);
void	SetOV9663Saturation(U16 wSetValue);
void	SetOV9663Hue(S16 swSetValue);
void	SetOV9663Sharpness(U8 bySetValue);
void SetOV9663Effect(U8 byEffect);
void SetOV9663ImgDir(U8 bySnrImgDir);
/*void SetOV9663WindowCrop(U16 wXStart,U16 wXEnd,U16 wYStart,U16 wYEnd );*/
void SetOV9663OutputDim(U16 wWidth,U16 wHeight);
void SetOV9663PwrLineFreq(U8 byFPS, U8 byLightFrq);
void SetOV9663WBTemp(U16 wRgain, U16 wGgain, U16 wBgain);
OV_CTT_t GetOV9663AwbGain(void);
void SetOV9663WBTempAuto(U8 bySetValue);
void SetOV9663BackLightComp(U16 bySetValue);
void SetOV9663IntegrationTimeAuto(U8 bySetValue);
void SetOV9663IntegrationTime(U16 wEspline);
void SetOV9663Gain(float fGain);
#endif // _OV9663_H_
