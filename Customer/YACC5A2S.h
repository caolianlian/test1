#ifndef _YACC5A2S_H_
#define _YACC5A2S_H_

#define YACC5A2S_AE_TARGET 0x3f

extern OV_CTT_t code gc_YACC5A2S_CTT[3];
void YACC5A2SSetFormatFps(U8 SetFormat, U8 byFps);
void SetYACC5A2SImgDir(U8 bySnrImgDir) ;
void CfgYACC5A2SControlAttr(void);
#ifndef _USE_BK_HSBC_ADJ_
void SetYACC5A2SBrightness(S16 swSetValue);
void	SetYACC5A2SContrast(U16 wSetValue);
void	SetYACC5A2SSaturation(U16 wSetValue);
void	SetYACC5A2SHue(S16 swSetValue);
#endif
void	SetYACC5A2SSharpness(U8 bySetValue);
void SetYACC5A2SOutputDim(U16 wWidth,U16 wHeight);
void SetYACC5A2SPwrLineFreq(U8 byFPS, U8 byLightFrq);
void SetYACC5A2SWBTemp(U16 wSetValue);
OV_CTT_t GetYACC5A2SAwbGain(void);
void SetYACC5A2SWBTempAuto(U8 bySetValue);
void SetYACC5A2SBackLightComp(U8 bySetValue);
U16  GetYACC5A2SAEGain();
void SetYACC5A2SIntegrationTimeAuto(U8 bySetValue);
void SetYACC5A2SIntegrationTime(U32 dwSetValue);
void SetYACC5A2SGain(float fGain);
#endif

