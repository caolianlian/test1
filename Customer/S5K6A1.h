#ifndef _S5K6A1_H_
#define _S5K6A1_H_

void S5K6A1SetFormatFps(U16 SetFormat, U8 Fps);
void CfgS5K6A1ControlAttr(void);
void SetS5K6A1ImgDir(U8 bySnrImgDir);
void SetS5K6A1IntegrationTime(U16 wEspline);
void SetS5K6A1Exposuretime_Gain(float fExpTime, float fTotalGain);
void InitS5K6A1IspParams();
void S5K6A1_POR();
void SetS5K6A1DynamicISP(U8 byAEC_Gain);
void SetS5K6A1DynamicISP_AWB(U16 wColorTempature);
void SetS5K6A1Gain(float fGain);
#endif