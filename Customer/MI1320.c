#include "Inc.h"

#ifdef RTS58XX_SP_MI1320

code MI1320_FpsSetting_t  g_staMi1320VGAFpsSetting[]=
{
	{1,/*CCS_HCLK_24M,*/(352),(980),0,0x1001,0x050f, 1500000},

	{3,/*CCS_HCLK_24M,*/(283),(17),53,0x1001,0x050f,  1500000},

	{5,/*CCS_HCLK_24M,*/(283),(17),53,0x1401,0x050b,  2500000},

	{10,/*CCS_HCLK_24M,*/(283),(17),53,0x1401,0x0505, 5000000},

	{15,/*CCS_HCLK_24M,*/(283),(17),53,0x1401,0x0503, 7500000},

	{20,/*CCS_HCLK_24M,*/(283),(17),53, 0x1401, 0x0502, 10000000},
	{25,/*CCS_HCLK_24M,*/(245),(17),459,0x1001,0x0501,12000000},
	{30,/*CCS_HCLK_24M,*/(189),(17),531,0x1201,0x0501,13500000},
};

code MI1320_FpsSetting_t  g_staMi1320SXGAFpsSetting[]=
{
	{1,/*CCS_HCLK_24M,*/(378),(1968),2000,0x1401,0x050b,  5000000},
	{3,/*CCS_HCLK_24M,*/(530),(1168), 400, 0x1001,  0x0503,12000000},
	{5,/*CCS_HCLK_24M,*/(587),(568),   0,    0x1401,  0x0503,15000000},//get 6fps setting from 2020 wizard, then change PLL control


	{8,/*CCS_HCLK_24M,*/(1095),(17),233,0x1401,0x0502, 20000000},

	{9,/*CCS_HCLK_24M,*/(1095),(17),233,0x1E03, 0x0501, 22500000},

	{10,/*CCS_HCLK_24M,*/(426),(368),400,0x1001,0x0501, 24000000},
	{15,/*CCS_HCLK_24M,*/(426),(18), 300,0x1201,0x0501, 27000000},
};


t_RegSettingWW code gc_MI1320_SOC_LSC_Setting[]=
{
	{0x180, 0x0003}, 	// LENS_CORRECT_CONTROL
	{0x181, 0x0000}, 	// LENS_ADJ_VERT_RED_0
	{0x182, 0x0000}, 	// LENS_ADJ_VERT_RED_1_2
	{0x183, 0x0000}, 	// LENS_ADJ_VERT_RED_3_4
	{0x184, 0x0000}, 	// LENS_ADJ_VERT_GREEN_0
	{0x185, 0x0000}, 	// LENS_ADJ_VERT_GREEN_1_2
	{0x186, 0x0000}, 	// LENS_ADJ_VERT_GREEN_3_4
	{0x187, 0x0000}, 	// LENS_ADJ_VERT_BLUE_0
	{0x188, 0x0000}, 	// LENS_ADJ_VERT_BLUE_1_2
	{0x189, 0x0000}, 	// LENS_ADJ_VERT_BLUE_3_4
	{0x18A, 0xCF1F}, 	// LENS_ADJ_HORIZ_RED_0
	{0x18B, 0xE0DC}, 	// LENS_ADJ_HORIZ_RED_1_2
	{0x18C, 0xF3E7}, 	// LENS_ADJ_HORIZ_RED_3_4
	{0x18D, 0x0000}, 	// LENS_ADJ_HORIZ_RED_5
	{0x18E, 0xD51A}, 	// LENS_ADJ_HORIZ_GREEN_0
	{0x18F, 0xE5E3}, 	// LENS_ADJ_HORIZ_GREEN_1_2
	{0x190, 0xF5EB}, 	// LENS_ADJ_HORIZ_GREEN_3_4
	{0x191, 0x0000}, 	// LENS_ADJ_HORIZ_GREEN_5
	{0x192, 0xDC15}, 	// LENS_ADJ_HORIZ_BLUE_0
	{0x193, 0xE9E7}, 	// LENS_ADJ_HORIZ_BLUE_1_2
	{0x194, 0xF8EF}, 	// LENS_ADJ_HORIZ_BLUE_3_4
	{0x195, 0x0000}, 	// LENS_ADJ_HORIZ_BLUE_5
	{0x1B6, 0x0000}, 	// LENS_ADJ_VERT_RED_5_6
	{0x1B7, 0x0000}, 	// LENS_ADJ_VERT_RED_7_8
	{0x1B8, 0x0000}, 	// LENS_ADJ_VERT_GREEN_5_6
	{0x1B9, 0x0000}, 	// LENS_ADJ_VERT_GREEN_7_8
	{0x1BA, 0x0000}, 	// LENS_ADJ_VERT_BLUE_5_6
	{0x1BB, 0x0000}, 	// LENS_ADJ_VERT_BLUE_7_8
	{0x1BC, 0x190C}, 	// LENS_ADJ_HORIZ_RED_6_7
	{0x1BD, 0x2724}, 	// LENS_ADJ_HORIZ_RED_8_9
	{0x1BE, 0x0032}, 	// LENS_ADJ_HORIZ_RED_10
	{0x1BF, 0x140A}, 	// LENS_ADJ_HORIZ_GREEN_6_7
	{0x1C0, 0x1C1D}, 	// LENS_ADJ_HORIZ_GREEN_8_9
	{0x1C1, 0x002B}, 	// LENS_ADJ_HORIZ_GREEN_10
	{0x1C2, 0x140A}, 	// LENS_ADJ_HORIZ_BLUE_6_7
	{0x1C3, 0x1A1D}, 	// LENS_ADJ_HORIZ_BLUE_8_9
	{0x1C4, 0x003B} 	// LENS_ADJ_HORIZ_BLUE_10

	/*	{ 0x180, 0x0006}, 	// LENS_CORRECT_CONTROL
		{ 0x181, 0xF40B}, 	// LENS_ADJ_VERT_RED_0
		{ 0x182, 0xFAF4}, 	// LENS_ADJ_VERT_RED_1_2
		{ 0x183, 0x02FE}, 	// LENS_ADJ_VERT_RED_3_4
		{ 0x184, 0xF70C}, 	// LENS_ADJ_VERT_GREEN_0
		{ 0x185, 0xF9F3}, 	// LENS_ADJ_VERT_GREEN_1_2
		{ 0x186, 0x02FD}, 	// LENS_ADJ_VERT_GREEN_3_4
		{ 0x187, 0xF708}, 	// LENS_ADJ_VERT_BLUE_0
		{ 0x188, 0xFAF6}, 	// LENS_ADJ_VERT_BLUE_1_2
		{ 0x189, 0x01FD}, 	// LENS_ADJ_VERT_BLUE_3_4
		{ 0x18A, 0xE716}, 	// LENS_ADJ_HORIZ_RED_0
		{ 0x18B, 0xF4F2}, 	// LENS_ADJ_HORIZ_RED_1_2
		{ 0x18C, 0xFAF8}, 	// LENS_ADJ_HORIZ_RED_3_4
		{ 0x18D, 0x00FF}, 	// LENS_ADJ_HORIZ_RED_5
		{ 0x18E, 0xEA16}, 	// LENS_ADJ_HORIZ_GREEN_0
		{ 0x18F, 0xF6F2}, 	// LENS_ADJ_HORIZ_GREEN_1_2
		{ 0x190, 0xFAF9}, 	// LENS_ADJ_HORIZ_GREEN_3_4
		{ 0x191, 0x00FF}, 	// LENS_ADJ_HORIZ_GREEN_5
		{ 0x192, 0xEC11}, 	// LENS_ADJ_HORIZ_BLUE_0
		{ 0x193, 0xF6F4}, 	// LENS_ADJ_HORIZ_BLUE_1_2
		{ 0x194, 0xFDFB}, 	// LENS_ADJ_HORIZ_BLUE_3_4
		{ 0x195, 0x0002}, 	// LENS_ADJ_HORIZ_BLUE_5
		{ 0x1B6, 0x0A06}, 	// LENS_ADJ_VERT_RED_5_6
		{ 0x1B7, 0x140D}, 	// LENS_ADJ_VERT_RED_7_8
		{ 0x1B8, 0x0E08}, 	// LENS_ADJ_VERT_GREEN_5_6
		{ 0x1B9, 0x1A0E}, 	// LENS_ADJ_VERT_GREEN_7_8
		{ 0x1BA, 0x0A05}, 	// LENS_ADJ_VERT_BLUE_5_6
		{ 0x1BB, 0x170B}, 	// LENS_ADJ_VERT_BLUE_7_8
		{ 0x1BC, 0x0905}, 	// LENS_ADJ_HORIZ_RED_6_7
		{ 0x1BD, 0x0B0D}, 	// LENS_ADJ_HORIZ_RED_8_9
		{ 0x1BE, 0x001D}, 	// LENS_ADJ_HORIZ_RED_10
		{ 0x1BF, 0x0903}, 	// LENS_ADJ_HORIZ_GREEN_6_7
		{ 0x1C0, 0x0B0E}, 	// LENS_ADJ_HORIZ_GREEN_8_9
		{ 0x1C1, 0x0020}, 	// LENS_ADJ_HORIZ_GREEN_10
		{ 0x1C2, 0x0604}, 	// LENS_ADJ_HORIZ_BLUE_6_7
		{ 0x1C3, 0x090A}, 	// LENS_ADJ_HORIZ_BLUE_8_9
		{ 0x1C4, 0x001B} 	// LENS_ADJ_HORIZ_BLUE_10
	*/
};



//find propriety setting
MI1320_FpsSetting_t*  GetMi1320FpsSetting(U8 Format,U8 Fps)
{
	U8 i;
	U8 Idx=0;
	if(g_bIsHighSpeed)
	{
		Idx=3; //8fps for uxga, 10 fps for vga
	}
	else
	{
		Idx = 1;
	}
	if(Format ==VGA_FRM)
	{
		for(i=0; i< (sizeof(g_staMi1320VGAFpsSetting)/sizeof(MI1320_FpsSetting_t)); i++)
		{
			if(g_staMi1320VGAFpsSetting[i].byFps ==Fps)
			{
				Idx=i;
				break;
			}
		}
		return &g_staMi1320VGAFpsSetting[Idx];

	}
	else //if(Format ==UXGA_FRM)
	{
		for(i=0; i< (sizeof(g_staMi1320SXGAFpsSetting)/sizeof(MI1320_FpsSetting_t)); i++)
		{
			if(g_staMi1320SXGAFpsSetting[i].byFps ==Fps)
			{
				Idx=i;
				break;
			}
		}
		return &g_staMi1320SXGAFpsSetting[Idx];
	}
}

void Mi1320SetFormatFps(U8 SetFormat,U8 Fps)
{
	U16 wTmp1;

	U32 dwTmp1,dwTmp2;

	MI1320_FpsSetting_t *pMi1320_FpsSetting;

	Write_SenReg(0x013A, 0x0202); //output 10bit raw data_context A
	//{0x013A, 0x0600}, //output (8+2) raw data_context A
	Write_SenReg(0x0021, 0x8400);//read-mode context_A enable binning, half speed,column skip=2,row skip=2
	Write_SenReg(0x019B, 0x0202); //output 10bit raw data_context B
	//{0x019B, 0x0600}, //output (8+2) raw data_context B
	Write_SenReg(0x0020,  0x0100); //read-mode context B, maximum pixel clk = 1/2 master clock

	Write_SenReg(0x0106, 0x001C); //disable Manual WB, AE, defect correction, Clip aperture, lens shading, flicker detection, CCM, AWB.
	pMi1320_FpsSetting=GetMi1320FpsSetting( SetFormat, Fps);
	Write_SenReg(0x0065, 0xA000);//bypass the locked PLL

	Write_SenReg(0x0066, pMi1320_FpsSetting->wPLLCtlM_N); //set parameter M = 40
	Write_SenReg(0x0067, pMi1320_FpsSetting->wPLLPara_P); //set parameter P = 3, N = 1.
	WaitTimeOut_Delay(1);
	Write_SenReg(0x0065, 0x2000); //use PLL now
	WaitTimeOut_Delay(5);
	Write_SenReg(0x00a, 0x8011); // divider = 1;



//	g_dwPixelClk = pMi1320_FpsSetting->dwPixelClk;

	/////////////////////////////////////
	//antiflick setting
	dwTmp1   =  (pMi1320_FpsSetting->wHorizBlanks+g_wSensorCurFormatWidth+8)*100000 ;
	dwTmp1 /= (pMi1320_FpsSetting->dwPixelClk/10); //line time, unit: 1us.
	//60Hz
	wTmp1     =  (1000000/(120*8)) / dwTmp1;                           //60Hz lines
	dwTmp2   =  (1000000/(120*8)) -  (dwTmp1*wTmp1);       //remainder
	if(dwTmp2 > (dwTmp1/2))
	{
		wTmp1+=1;
	}
	Write_SenReg(0x25c,((wTmp1+3)<<8)|(wTmp1-3) ); //60Hz search flicker


	//50Hz
	wTmp1     =  (1000000/(100*8)) / dwTmp1;                           //50Hz lines
	dwTmp2   =  (1000000/(100*8)) -  (dwTmp1*wTmp1);       //remainder
	if(dwTmp2 > (dwTmp1/2))
	{
		wTmp1+=1;
	}
	Write_SenReg(0x25D,((wTmp1+3)<<8)|(wTmp1-3) ); //50Hz search flicker

	//shutter width basic 60Hz
	dwTmp2 = 1000000/(30*dwTmp1);
	Write_SenReg(0x257, (U16)dwTmp2); //60Hz context A
	Write_SenReg(0x259, (U16)dwTmp2); //60Hz context B
	dwTmp2 = 1000000/(25*dwTmp1);
	Write_SenReg(0x258, (U16)dwTmp2); //50Hz context A
	Write_SenReg(0x25a, (U16)dwTmp2); //50Hz context B
	//auto exposure line size A, B
	wTmp1 = pMi1320_FpsSetting->wHorizBlanks+g_wSensorCurFormatWidth+8;
	Write_SenReg(0x239, wTmp1);
	Write_SenReg(0x23a, wTmp1);
	//auto Exposure shutter delay limit a,b
	//Write_SenReg(0x23b, (U16));
	//Write_SenReg(0x23c, (U16));
	//mode control
	Write_SenReg(0x106, 0x6002);//enable AE,defect correction,clip aperture correction,flicker detection



	if((g_wSensorSPFormat & SetFormat) == VGA_FRM)
	{
		/* VGA, select_MI1320 register_context A*/

		//context A
		Write_SenReg(0x0007, pMi1320_FpsSetting->wHorizBlanks); //dummy pixel = 0x00BE
		Write_SenReg(0x0008, pMi1320_FpsSetting->wVertBlanks); //dummy line = 0x0042
		Write_SenReg(0x00C8, 0x0000); //select context A
//		g_wDumpxlPerline=pMi1320_FpsSetting->wHorizBlanks+8;
		g_wAECExposureRowMax = pMi1320_FpsSetting->wVertBlanks+520;
	}
	else  //SXGA
	{
		/* SXGA, select_MI1320 register_context B */
		//context B
		Write_SenReg(0x0005, pMi1320_FpsSetting->wHorizBlanks); //dummy pixel = 0x0184
		Write_SenReg(0x0006, pMi1320_FpsSetting->wVertBlanks); //dummy line = 0x0011
		Write_SenReg(0x00C8, 0x000B); //select context B

//		g_wDumpxlPerline = pMi1320_FpsSetting->wHorizBlanks+8;//396
		g_wAECExposureRowMax = pMi1320_FpsSetting->wVertBlanks+1032;//1053;
	}
	Write_SenReg(0x000b,pMi1320_FpsSetting->wExtraDelay);
	//lens shading correction setting
	//WriteSensorSettingWW((sizeof(gc_MI1320_SOC_LSC_Setting)/sizeof(t_RegSettingWW)), gc_MI1320_SOC_LSC_Setting);


	//cheney.cai 2009-06-24,do not select setting ,just replace setting when EEPROM configuration exist
	//if((g_byVdSOCIspSettingExistBitmap&VD_SNRISP_REGSETTING_LSC) == 0)
	{
//		LoadVdSensorSetting((U8*)gc_MI1320_SOC_LSC_Setting,sizeof(gc_MI1320_SOC_LSC_Setting)/5);
		WriteSensorSettingWW(sizeof(gc_MI1320_SOC_LSC_Setting)/4,gc_MI1320_SOC_LSC_Setting);
		Write_SenReg_Mask(0x106, 0x0400,0x0400);
	}

	SetMi1320ISPMisc();
}

void SetMi1320WindowCrop(U16 wXStart,U16 wXEnd,U16 wYStart,U16 wYEnd )
{

	U16 wTmp_width,wTmp_height;
	if(g_wSensorCurFormat  == VGA_FRM)
	{
		wTmp_width = (wXEnd-wXStart)<<1;
		wTmp_height = (wYEnd-wYStart)<<1;
		wXStart <<= 1;
		wYStart <<= 1;
	}
	else
	{
		wTmp_width = (wXEnd-wXStart);
		wTmp_height = (wYEnd-wYStart);
	}
	Write_SenReg(0x1A5, 0x4000|wXStart); //horizontal pan
	Write_SenReg(0x1A8, 0x4000|wYStart); //Vertical pan
	Write_SenReg(0x1A6, wTmp_width); //zoom width
	Write_SenReg(0x1A9, wTmp_height); //zoon heigth
}

void SetMi1320OutputDim(U16 wWidth,U16 wHeight)
{
	Write_SenReg(0x1A7,wWidth); //horizontal output size A
	Write_SenReg(0x1AA,wHeight); //vertical output size A

	Write_SenReg(0x1A1,wWidth); //horizontal output size B
	Write_SenReg(0x1A4,wHeight); //vertical output size B
}

#ifndef _USE_BK_SPECIAL_EFFECT_
void SetMi1320SensorEffect(U8 byEffect)
{
	U8 bytmp;
	switch(byEffect)
	{
	case SNR_EFFECT_NEGATIVE:
		bytmp =0x03;
		break;
	case SNR_EFFECT_MONOCHROME:
		bytmp = 0x01;//
		break;
	case SNR_EFFECT_SEPIA:
		bytmp =0x02;//
		break;
	default:
		bytmp =0x00;//no effect;
		break;
	}
	Write_SenReg(0x1e2,bytmp);

}
#endif

void SetMI1320Gain(float fGain)
{
}

void SetMi1320ImgDir(U8 bySnrImgDir) //flip: bit1,mirror bit0.
{
	U8 byTmp;
	byTmp = (bySnrImgDir&0x01)<<1;
	byTmp+=(bySnrImgDir&0x02)>>1;
	Write_SenReg(0x020, 0x0100|(U16)byTmp);//mode_sensor_mode_B
}

void SetMi1320PwrLineFreq(U8 byFps, U8 bySetValue)
{
	U8 byTmp;

	byFps = byFps; // for delete warning
	if(bySetValue == PWR_LINE_FRQ_DIS)
	{
		byTmp=0x0000;//auto flicker detection
	}
	else //	manual mode.
	{
		if(bySetValue == PWR_LINE_FRQ_50)//50Hz
		{
			byTmp = 0x0001;
		}
		else//60Hz
		{
			byTmp = 0x0003;
		}
	}
	Write_SenReg(0x25b,byTmp);
}
void SetMi1320LSC()
{

}
void SetMi1320CCM()
{

}
void SetMi1320AE()
{
	U8 byTmp1,byTmp2;
	byTmp1 = (g_byOVAEW_Normal+g_byOVAEB_Normal)/2;
	byTmp2 = (g_byOVAEW_Normal-g_byOVAEB_Normal)/2;
	//set sensor AE target and tracking stability
	Write_SenReg(0x22E, ((((U16)byTmp2)<<8)|byTmp1));
	//set sensor AE speed and  sensitivity
	Write_SenReg(0x22F, 0x9120);
	//use default AE window setting



}
void SetMi1320AWB()
{

}
void SetMi1320DPC()
{

}

void SetMi1320NRC()
{
}

void SetMi1320MISC()
{
}
void SetMi1320BackLightComp(U8 bySetValue)
{
	U16 wTmp;
	if(bySetValue==0)
	{
		Read_SenReg(0x106, &wTmp);
		Write_SenReg(0x106, wTmp&0xFFF3);//bit[3:2] = 0;     auto exposure sampling window is specified by full window
	}
	else
	{
		Read_SenReg(0x106, &wTmp);
		Write_SenReg(0x106, wTmp|0x000c);//bit[3:2] =11;	 auto exposure sampling window is specified by the weighted sum of the full window.
		//center window four times more heavily
	}
}

void	CfgMi1320ControlAttr()
{
	g_bySensorSize = SENSOR_SIZE_SXGA;
	g_wSensorSPFormat = SXGA_FRM | VGA_FRM;

	{
//		memcpy(g_asOvCTT,gc_MI1320_CTT,sizeof(gc_MI1320_CTT));
	}

	{
		// AE backlight compensation parameter
		g_byOVAEW_Normal = MI1320_AEW;
		g_byOVAEB_Normal = MI1320_AEB;

		g_byOVAEW_BLC = g_byOVAEW_Normal + 0x20;
		g_byOVAEB_BLC = g_byOVAEB_Normal + 0x20;
	}

	{
		//	XBYTE[REG_TUNESVIO_SVA] =  SV28_VOL_2V9|SV18_VOL_1V8;
		// hemonel 2010-09-08: support 1.8V sensor IO
#if (_CHIP_ID_ & _RTS5840_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V79;
#elif (_CHIP_ID_ & _RTS5832_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V85;
#elif (_CHIP_ID_ & _RTS5829B_)
		XBYTE[REG_TUNESVA] =  SVA_VOL_2V82;
		XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V80;
#endif
	}

//	if(g_byFormatCfgExist==0) // format config not exist, use default setting.
	{
		InitFormatFrameFps();
	}



}

#ifndef _USE_BK_HSBC_ADJ_
void  SetMI1320Brightness(S16 swSetValue)
{
	S16 swTmp;
	//find  setting  first.

	swTmp=swSetValue;

	if(swTmp<0)
	{
		swTmp=0;
	}
	Write_SenReg(0x134,swTmp&0x00ff);

}

U8 code g_cMi1320SaturationTable[]=
{
	0x6,//0%, black and white
	0x4,//25%   of full saturatoin
	0x3,//37,5% ~~
	0x2,//50%
	0x1,//75%
	0X0,//100%
	0X9,//112.5%
	0XA,//125%
	0XB,//137.5%
	0X5//150%
};

void  SetMI1320Saturation(U16 wSetValue)
{
	U16 wTmp1;
	U16 wTmp2;
	//find saturation setting index first.
	wTmp1=(wSetValue );

	wTmp2=  g_cMi1320SaturationTable[wTmp1];
	Write_SenReg(0x125,(wTmp2<<3)|0x0005);
}
#endif

void  SetMi1320Sharpness(U8 bySetValue)
{
	U16 wTmp;
	//find  setting  first.
	wTmp=(bySetValue );
	Write_SenReg(0x105,wTmp&0x0007);
}

void SetMi1320ISPMisc()
{
	SetMi1320LSC();
	SetMi1320CCM();
	SetMi1320AE();
	SetMi1320AWB();
	SetMi1320DPC();
	SetMi1320NRC();
	SetMi1320MISC();
}

void SetMi1320IntegrationTimeAuto(U8 bySetValue)
{
}

void SetMi1320IntegrationTime(U16 wEspline)
{
}
#endif
