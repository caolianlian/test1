#ifndef _CAMFUN_H_
#define _CAMFUN_H_

void	InitMCU(void);
void	InitCAM(U8 byPG_EN);
void	USBProSuspend(void);
void	USBProResume(void);
void	USBProReset(void);
void	USBProSetupPKT(void);
void global_vars_init(); 
void SwitchToCacheMode(void);
U32 GetFrameInterval(U8 IsVideo,U8 FMT,U8 FRM,U8 IsHighSpeed,U8 FpsIdx);
U32 GetMaxFrameSize(U8 IsVideo,U8 FmtIdx, U8 FrmIdx,U8 IsHighSpeed) ;
U8 GetDefaultFPS(U8 IsVideo,U8 FmtIdx, U8 FrmIdx,U8 IsHighSpeed);

void UbistTest_FlashChecking(void);
void UbistTest_SensorCommunication(void);
void UbistTest_Register(void);
void UbistTest_Controller(void);
void UbistTest_Luminance(void);
void UbistTest_BlinkLED(void);

U8 L3LEEPRead(U8 DLen, U16 DAddr, U16 SAddr);
U8 Cache_I2CEEPRead(U8 DLen, U16 DAddr, U32 SAddr,U8 addr_len);
void Init_VdfunctionPointers();
#endif