#include "pc_cam.h"
#include "camutil.h"
#include "camreg.h"
#include "camvdcfg.h"
//#include "camspi.h"
#include "camuvc.h"
#include "CamCtl.h"
#include "Global_vars.h"

U8 GetVdCfgByte(U16 wSAddr)
{
	return CBYTE[CACHE_CODE_CFG_BASE+wSAddr];
}

U8 CopyVdCfgByte2SRam(U16 wSAddr,U8* pBuf,U8 byLen)
{
	U8 i;

	for(i=0; i<byLen; i++)
	{
		pBuf[i] = GetVdCfgByte(wSAddr+i);
	}
	return TRUE;
}

void LoadNVMCfg()
{
	if((VD_CFG_INFO_EXIST_FLAG == CBYTE[CACHE_CODE_CFG_BASE+CFG_INFO_TAG_OFFSET])
	        && (0x55 == CBYTE[CACHE_CODE_CFG_BASE]))
	{
		LoadVendorCfg();
	}
}

void LoadVendorCfg(void )
{
	U16 data offset;
	U8  i;
	U8   DescsNum;
	U16   DescSize;
	U8  DescType;

	DescsNum =  GetVdCfgByte(CFG_INFO_DESCNUM_OFFSET);
	offset = CFG_INFO_DESCNUM_OFFSET+1;		// offset pointer the first configure descriptor
	
	for(i=0 ; i<DescsNum; i++)
	{
		ASSIGN_INT(DescSize, ((GetVdCfgByte(offset+1)&0xC0)>>6), GetVdCfgByte(offset));
		DescType= (GetVdCfgByte(offset+1)&0x3F);

		offset += 2;	// offset pointer the configure data
	
		switch(DescType)
		{
			case VDCFG_DESC_TYPE_STR_MANUFACTOR:
				g_wVdStrManuFactorAddr = (offset);
				break;
			case VDCFG_DESC_TYPE_STR_PRODUCT:
				g_wVdStrProductAddr = (offset);
				break;
			case VDCFG_DESC_TYPE_STR_SERIALNUM:
				g_wVdStrSerialNumAddr = (offset);
				break;
			case VDCFG_DESC_TYPE_STR_IADSTR:
				g_wVdIADStrAddr = (offset);
				break;
#ifdef _UBIST_TEST_
			case VDCFG_DESC_TYPE_STR_UBISTNAME:
				g_wVdUBISTStrAddr = (offset);
				break;
#endif
			case VDCFG_DESC_TYPE_APSPEC:
				if(VDCFG_DESC_SUBTYPE_AP_BARINFO==GetVdCfgByte(offset))
				{
					g_wBarInfoOffset = (offset+1);
				}
				break;
			case VDCFG_DESC_TYPE_VID_PID:
				g_byVendorIDL   =  GetVdCfgByte(offset);
				g_byVendorIDH  =  GetVdCfgByte(offset+1);
				g_byProductIDL  =  GetVdCfgByte(offset+2);
				g_byProductIDH =  GetVdCfgByte(offset+3);
				g_byDevVerID_L  =  GetVdCfgByte(offset+4);
				g_byDevVerID_H =  GetVdCfgByte(offset+5);
				break;
			default:
				break;
		}
		offset += DescSize-2 ;
	}

	return;
}

