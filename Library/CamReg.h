/*
*******************************************************************************
*                           Cardreader RTS5151 project
*
*                           Layer:
*                           Module:
*
*
*
*
* File : Define.h
*******************************************************************************
*/
/**
*******************************************************************************
  \file Define.h
  \brief Constant and macro definintion.



* \version 0.1
* \date 2006

*******************************************************************************/

/*=============================================================================

                            EDIT HISTORY FOR MODULE

 when           who         what, where, why
 -----------    ---------   ---------------------------------------------------
 2006/1/13      JinZhong    Create this file

==============================================================================*/


#ifndef	_CAMREG_H_
#define	_CAMREG_H_
#include "CamReg2.h"


#define FLASH_DOWNLOAD_TAG_ADDR0	0xFFFE	// hemonel 2008-06-16: change 56K end	
#define FLASH_DOWNLOAD_TAG_ADDR1	0xFFFF

#define FLASH_DOWNLOAD_FLAG0  0x55
#define FLASH_DOWNLOAD_FLAG1  0xaa

#define SF_DOWNLOAD_FLAG0         0xaa
#define SF_DOWNLOAD_FLAG1         0x55

#define SF_DWN_SPICI_DELINK_FLAG0	0x5A
#define SF_DWN_SPICI_DELINK_FLAG1	0xA5

#define SF_FAST_DWN_SPICS_DELINK_FLAG0		0xA5
#define SF_FAST_DWN_SPICS_DELINK_FLAG1		0x5A

#define SF_FAST_DOWNLOAD_FLAG0	0xAA
#define SF_FAST_DOWNLOAD_FLAG1	0xA5

#define CACHE_TAG_ADDR0	0xEFFE	// hemonel 2010-12-07: change 60K end	
#define CACHE_TAG_ADDR1	0xEFFF

#define FLASH_DOWNLOAD_TAG0    0x55
#define FLASH_DOWNLOAD_TAG1    0xaa

#define SF_DOWNLOAD_TAG0           0x55
#define SF_DOWNLOAD_TAG1           0xAA

#define NORMAL_ROMMODE		0x00
#define FORCE_ROMMODE			0x01	//used to offline download KGD serial flash


//////////////////////////////////////
//code ram buffer space definition
//////////////////////////////////////

//////////////////////////////////////
//ISP registers
//////////////////////////////////////

//**page0 ISP System Control Registers
#define	ISP_CONTROL0					0x8000
#define	ISP_CONTROL1					0x8001
#define	ISP_CONTROL2					0x8002
#define	ISP_CONTROL3					0x8003
#define	ISP_BUS_WIDTH					0x8004
#define	ISP_IMAGE_SEL					0x8005
#define	ISP_INT_EN0					0x8006
#define	ISP_INT_FLAG0					0x8007
#define	ISP_INT_EN1					0x8008
#define	ISP_INT_FLAG1					0x8009
#define	ISP_ABORT_CNT_L				0x800A
#define	ISP_ABORT_CNT_H				0x800B
#define	ISP_FT_WIDTH_L				0x800C  //frame width after blc window 12-bit
#define	ISP_FT_WIDTH_H				0x800D
#define	ISP_FT_HEIGHT_L				0x800E
#define	ISP_FT_HEIGHT_H				0x800F
#define	ISP_BK_WIDTH_L				0x8010
#define	ISP_BK_WIDTH_H				0x8011
#define	ISP_BK_HEIGHT_L 				0x8012
#define	ISP_BK_HEIGHT_H				0x8013
#define	ISP_DUMMY0					0x8014
#define	ISP_DUMMY1					0x8015
#define	ISP_DUMMY2					0x8016
#define	ISP_DUMMY3					0x8017
#define	ISP_SHIFTREG_FSYNC_EN		0x8018
#define	ISP_SHIFTREG_FSYNC_A			0x8019
#define	ISP_SHIFTREG_FSYNC_B			0x801A
#define	ISP_SHIFTREG_FSYNC_C			0x801B 
#define	ISP_SHIFTREG_FSYNC_D			0x801C
#define	ISP_FRAME_IDLE					0x801D
#define	ISP_DPC_SPEED_CTRL			0x801E
#define	ISP_INTP_SPEED_CTRL			0x801F
#define	ISP_EEH_SPEED_CTRL			0x8020
#define	ISP_ZOOM_SPEED_CTRL			0x8021
#define	ISP_DPC_LAST_BREAK			0x8022  
#define	ISP_INTP_LAST_BREAK			0x8023
#define	ISP_EEH_LAST_BREAK			0x8024
#define	ISP_ZOOM_LAST_BREAK			0x8025
#define   ISP_CLK_HALF_ENABLE			0x8026
#define	AE_STATIS_LOC 					0x8027 //0:before gamma  ;1:after gamma
#define 	ISP_LOW_DISABLE				0x8028
#define 	ISP_ABORT_ENABLE				0x8029
#define	ISP_DATA_ENABLE				0x802A
#define 	ISP_DATA_DELAY				0x802B
#define	ISP_SIE_CTRL					0x802c
#define   ISP_MIPI_TRANSFER_EN     		0x802D
#define 	ISP_TEST_CTRL					0x802F
#define 	ISP_TEST_DATA_L				0x8030
#define 	ISP_TEST_DATA_H				0x8031
#define 	ISP_JPEG_CUT_ENABLE			0x8032
#define 	ISP_JPEG_ALIGN_DIR_MODE		0x8033
#define	ISP_AWB_STATIS_LOC			0x8034

//////////////////////////////////
//page1 Lens Shading Correction Registers
#define  ISP_NLSC_CENTER_X_R_L			0x8100
#define  ISP_NLSC_CENTER_X_R_H			0x8101
#define  ISP_NLSC_CENTER_Y_R_L			0x8102
#define  ISP_NLSC_CENTER_Y_R_H			0x8103
#define  ISP_NLSC_DISTANCE_R_L			0x8104//distance between first pixel and center ,11bit
#define  ISP_NLSC_DISTANCE_R_H			0x8105
#define  ISP_NLSC_ERROR_R_L				0x8106
#define  ISP_NLSC_ERROR_R_H			0x8107
#define  ISP_NLSC_MODE					0x8108
//#define  ISP_NLSC_R_ADJ_RATE			0x8109	// move to reg2.h
//#define  ISP_NLSC_G_ADJ_RATE			0x810A
//#define  ISP_NLSC_B_ADJ_RATE			0x810B
//normal lsc  R curve from 0x810c~0x813b
#define  ISP_NLSC_CURVE_R_BASE			0x810C
//normal lsc  R curve from 0x813c~0x816b
#define  ISP_NLSC_CURVE_G_BASE			0x813C
//normal lsc  R curve from 0x816c~0x819b
#define  ISP_NLSC_CURVE_B_BASE			0x816C
#define	ISP_MICRO_LSC_PARA_0			0x81A0
#define	ISP_MICRO_LSC_PARA_1			0x81A1
#define	ISP_MICRO_LSC_PARA_2			0x81A2
#define	ISP_MICRO_LSC_PARA_3			0x81A3
#define	ISP_MICRO_LSC_CTRL			0x81A5
#define ISP_MICRO_LSC_ADDR_H			0x81A6
#define	ISP_MICRO_LSC_ADDR_L			0x81A7

#define  ISP_NLSC_CENTER_X_G_L			0x81B0
#define  ISP_NLSC_CENTER_X_G_H			0x81B1
#define  ISP_NLSC_CENTER_Y_G_L			0x81B2
#define  ISP_NLSC_CENTER_Y_G_H			0x81B3
#define  ISP_NLSC_DISTANCE_G_L			0x81B4//distance between first pixel and center ,11bit
#define  ISP_NLSC_DISTANCE_G_H			0x81B5
#define  ISP_NLSC_ERROR_G_L				0x81B6
#define  ISP_NLSC_ERROR_G_H			0x81B7
#define  ISP_NLSC_CENTER_X_B_L			0x81B8
#define  ISP_NLSC_CENTER_X_B_H			0x81B9
#define  ISP_NLSC_CENTER_Y_B_L			0x81BA
#define  ISP_NLSC_CENTER_Y_B_H			0x81BB
#define  ISP_NLSC_DISTANCE_B_L			0x81BC//distance between first pixel and center ,11bit
#define  ISP_NLSC_DISTANCE_B_H			0x81BD
#define  ISP_NLSC_ERROR_B_L				0x81BE
#define  ISP_NLSC_ERROR_B_H			0x81BF

#define ISP_NLSC_GAIN_STEP_R 			0x81c0
#define ISP_NLSC_GAIN_STEP_G			0x81c1
#define ISP_NLSC_GAIN_STEP_B 			0x81c2
#define ISP_NLSC_GAIN_CTRL  			0x81c3

////////////////////////////////
///page2 : AWB and AE statis and gain
//0X8200~0X8211
#define 	ISP_AWB_ROUGH_GAIN_R_1		0x8200
#define 	ISP_AWB_ROUGH_GAIN_R_2		0x8201
#define 	ISP_AWB_ROUGH_GAIN_R_3		0x8202
#define 	ISP_AWB_ROUGH_GAIN_R_4		0x8203
#define 	ISP_AWB_ROUGH_GAIN_R_5		0x8204
#define 	ISP_AWB_ROUGH_GAIN_R_6		0x8205

#define ISP_AWB_ROUGH_GAIN_B_1          0x8206	
#define ISP_AWB_ROUGH_GAIN_B_2          0x8207	
#define ISP_AWB_ROUGH_GAIN_B_3          0x8208	
#define ISP_AWB_ROUGH_GAIN_B_4          0x8209	
#define ISP_AWB_ROUGH_GAIN_B_5          0x820A	
#define ISP_AWB_ROUGH_GAIN_B_6          0x820B	

#define ISP_AWB_ROUGH_RG_MAX            0x820C	
#define ISP_AWB_ROUGH_RG_MIN            0x820D	
#define ISP_AWB_ROUGH_BG_MAX            0x820E	
#define ISP_AWB_ROUGH_BG_MIN            0x820F	
#define ISP_AWB_ROUGH_BRIGHT_MAX        0x8210	
#define ISP_AWB_ROUGH_BRIGHT_MIN        0x8211	

#define ISP_AWB_RGBC_C11_H              0x8212	
#define ISP_AWB_RGBC_C11_L              0x8213	
#define ISP_AWB_RGBC_C21_H              0x8214	
#define ISP_AWB_RGBC_C21_L              0x8215	
#define ISP_AWB_RGBC_C12_H              0x8216	
#define ISP_AWB_RGBC_C12_L              0x8217	
#define ISP_AWB_RGBC_C22_H              0x8218	
#define ISP_AWB_RGBC_C22_L              0x8219	
#define ISP_AWB_RGBC_C31_H              0x821A	
#define ISP_AWB_RGBC_C31_L              0x821B	
#define ISP_AWB_RGBC_C41_H              0x821C	
#define ISP_AWB_RGBC_C41_L              0x821D	
#define ISP_AWB_RGBC_C32_H              0x821E	
#define ISP_AWB_RGBC_C32_L              0x821F	
#define ISP_AWB_RGBC_C42_H              0x8220	
#define ISP_AWB_RGBC_C42_L              0x8221	
#define ISP_AWB_RGBC_C51_H              0x8222	
#define ISP_AWB_RGBC_C51_L              0x8223	
#define ISP_AWB_RGBC_C61_H              0x8224	
#define ISP_AWB_RGBC_C61_L              0x8225	
#define ISP_AWB_RGBC_C52_H              0x8226	
#define ISP_AWB_RGBC_C52_L              0x8227	
#define ISP_AWB_RGBC_C62_H              0x8228	
#define ISP_AWB_RGBC_C62_L              0x8229	
#define ISP_AWB_RGBC_K1                 0x822A	
#define ISP_AWB_RGBC_K3                 0x822B	
#define ISP_AWB_RGBC_K5                 0x822C	


#define 	ISP_AWB_WP_NUM1_L			0x822F
#define 	ISP_AWB_WP_NUM1_H			0x8230
#define 	ISP_AWB_WP_NUM2_L			0x8231
#define 	ISP_AWB_WP_NUM2_H			0x8232
#define 	ISP_AWB_WP_NUM3_L			0x8233
#define 	ISP_AWB_WP_NUM3_H			0x8234
#define 	ISP_AWB_WP_NUM4_L			0x8235
#define 	ISP_AWB_WP_NUM4_H			0x8236
#define 	ISP_AWB_WP_NUM5_L			0x8237
#define 	ISP_AWB_WP_NUM5_H			0x8238
#define 	ISP_AWB_WP_NUM6_L			0x8239
#define 	ISP_AWB_WP_NUM6_H			0x823A

#define 	ISP_AWB_FINE_GAIN_R_L		0x823B
#define 	ISP_AWB_FINE_GAIN_R_H		0x823C
#define 	ISP_AWB_FINE_GAIN_B_L		0x823F
#define 	ISP_AWB_FINE_GAIN_B_H		0x8240

#define 	ISP_AWB_FINE_RG_MAX			0x8241
#define 	ISP_AWB_FINE_RG_MIN			0x8242
#define 	ISP_AWB_FINE_BG_MAX			0x8243
#define 	ISP_AWB_FINE_BG_MIN			0x8244
#define 	ISP_AWB_FINE_BRIGHT_MAX		0x8245
#define 	ISP_AWB_FINE_BRIGHT_MIN		0x8246

#define 	ISP_AWB_FINE_SUM_R_L			0x8247
#define 	ISP_AWB_FINE_SUM_R_M			0x8248
#define 	ISP_AWB_FINE_SUM_R_H			0x8249
#define 	ISP_AWB_FINE_SUM_G_L			0x824A
#define 	ISP_AWB_FINE_SUM_G_M		0x824B
#define 	ISP_AWB_FINE_SUM_G_H			0x824C
#define 	ISP_AWB_FINE_SUM_B_L			0x824D
#define 	ISP_AWB_FINE_SUM_B_M			0x824E
#define 	ISP_AWB_FINE_SUM_B_H			0x824F
#define 	ISP_AWB_FINE_WP_NUM_L		0x8250
#define 	ISP_AWB_FINE_WP_NUM_H		0x8251


#define 	ISP_AWB_START_X_L				0x8252
#define 	ISP_AWB_START_X_H			0x8253
#define 	ISP_AWB_START_Y_L				0x8254
#define 	ISP_AWB_START_Y_H			0x8255
#define 	ISP_AWB_WIN_WIDTH			0x8256
#define 	ISP_AWB_WIN_HEIGHT			0x8257
#define 	ISP_AWB_WIN_BRIGHT_MAX		0x8258
#define 	ISP_AWB_WIN_BRIGHT_MIN		0x8259
#define 	ISP_AWB_BRIGHT_COEF_R           0x825A	
#define 	ISP_AWB_BRIGHT_COEF_G           0x825B	
#define 	ISP_AWB_BRIGHT_COEF_B           0x825C	
#define 	ISP_AWB_WIN_ENABLE			0x825E
#define 	ISP_AWB_STATIS_CTRL			0x825F
#define 	ISP_AWB_WIN_SUM_R_H			0x8261
#define 	ISP_AWB_WIN_SUM_R_M			0x8262
#define 	ISP_AWB_WIN_SUM_R_L			0x8263
#define 	ISP_AWB_WIN_SUM_G_H			0x8265
#define 	ISP_AWB_WIN_SUM_G_M			0x8266
#define 	ISP_AWB_WIN_SUM_G_L			0x8267
#define 	ISP_AWB_WIN_SUM_B_H			0x8269
#define 	ISP_AWB_WIN_SUM_B_M			0x826a
#define 	ISP_AWB_WIN_SUM_B_L			0x826b
#define 	ISP_AWB_WIN_ADDR				0x826C

//8267~826F 		Reserved
#define	ISP_AWB_GAIN_R_L				0x8270
#define	ISP_AWB_GAIN_R_H				0x8271
#define	ISP_AWB_GAIN_G_L				0x8272
#define	ISP_AWB_GAIN_G_H				0x8273
#define	ISP_AWB_GAIN_B_L				0x8274
#define	ISP_AWB_GAIN_B_H				0x8275
#define	ISP_AWB_GAIN_STEP_R			0x8276
#define	ISP_AWB_GAIN_STEP_G			0x8277
#define	ISP_AWB_GAIN_STEP_B			0x8278
#define	ISP_AWB_GAIN_CTRL				0x8279

#define ISP_AWB_GAIN_R_NOW_L 			0x827A		//ISP AWB R Gain
#define ISP_AWB_GAIN_R_NOW_H 			0x827B		//ISP AWB R Gain
#define ISP_AWB_GAIN_G_NOW_L 			0x827C		//ISP AWB G Gain
#define ISP_AWB_GAIN_G_NOW_H 			0x827D		//ISP AWB G Gain
#define ISP_AWB_GAIN_B_NOW_L 			0x827E		//ISP AWB B Gain
#define ISP_AWB_GAIN_B_NOW_H 			0x827F		//ISP AWB B Gain

//AE
#define	ISP_AE_CTRL					0x8280
#define	ISP_AE_BRIGHT_COEF_R			0x8281
#define	ISP_AE_BRIGHT_COEF_G			0x8282
#define	ISP_AE_BRIGHT_COEF_B			0x8283
#define	ISP_AE_START_X_L				0x8284
#define	ISP_AE_START_X_H				0x8285
#define	ISP_AE_START_Y_L				0x8286
#define	ISP_AE_START_Y_H				0x8287
#define	ISP_AE_WIN_WIDTH				0x8288
#define	ISP_AE_WIN_HEIGHT				0x8289
#define	ISP_AE_BRIGHT_MAX				0x828A
#define	ISP_AE_DISCNT_ENABLE			0x828B
#define	ISP_AE_ADDR					0x828C
#define	ISP_AE_SUM_L					0x828D
#define	ISP_AE_SUM_H					0x828E

#define ISP_AE_MAX_Y 0x8290		//ISP ae statis max of Y
#define ISP_AE_MIN_Y 0x8291		//ISP ae statis min of Y
#define ISP_AE_MAX_R 0x8292		//ISP ae statis max of R
#define ISP_AE_MIN_R 0x8293		//ISP ae statis min of R
#define ISP_AE_MAX_G 0x8294		//ISP ae statis max of G
#define ISP_AE_MIN_G 0x8295		//ISP ae statis min of G
#define ISP_AE_MAX_B 0x8296		//ISP ae statis max of B
#define ISP_AE_MIN_B 0x8297		//ISP ae statis min of B
		
#define ISP_AE_SUM_MAX_Y_L 0x8298		//ISP ae statis max sum of Y
#define ISP_AE_SUM_MAX_Y_H 0x8299		//ISP ae statis max sum of Y
#define ISP_AE_SUM_MIN_Y_L 0x829A		//ISP ae statis min sum of Y
#define ISP_AE_SUM_MIN_Y_H 0x829B		//ISP ae statis min sum of Y
#define ISP_AE_SUM_MAX_R_L 0x829C		//ISP ae statis max sum of R
#define ISP_AE_SUM_MAX_R_H 0x829D		//ISP ae statis max sum of R
#define ISP_AE_SUM_MIN_R_L 0x829E		//ISP ae statis min sum of R
#define ISP_AE_SUM_MIN_R_H 0x829F		//ISP ae statis min sum of R
#define ISP_AE_SUM_MAX_G_L 0x82A0		//ISP ae statis max sum of G
#define ISP_AE_SUM_MAX_G_H 0x82A1		//ISP ae statis max sum of G
#define ISP_AE_SUM_MIN_G_L 0x82A2		//ISP ae statis min sum of G
#define ISP_AE_SUM_MIN_G_H 0x82A3		//ISP ae statis min sum of G
#define ISP_AE_SUM_MAX_B_L 0x82A4		//ISP ae statis max sum of B
#define ISP_AE_SUM_MAX_B_H 0x82A5		//ISP ae statis max sum of B
#define ISP_AE_SUM_MIN_B_L 0x82A6		//ISP ae statis min sum of B
#define ISP_AE_SUM_MIN_B_H 0x82A7		//ISP ae statis min sum of B

#define ISP_AE_HWIN_START_X_H		0x82A8
#define ISP_AE_HWIN_START_X_L		0x82A9
#define ISP_AE_HWIN_START_Y_H		0x82AA
#define ISP_AE_HWIN_START_Y_L		0x82AB
#define ISP_AE_HWIN_END_X_H 	0x82AC
#define ISP_AE_HWIN_END_X_L 	0x82AD
#define ISP_AE_HWIN_END_Y_H 	0x82AE
#define ISP_AE_HWIN_END_Y_L 	0x82AF

//82B0~82FF Reserved


/////////////////////////////
///Page3 : color & brightness on raw/RGB domain
#define	ISP_BLC_START_X_L				0x8300
#define	ISP_BLC_START_X_H				0x8301
#define	ISP_BLC_START_Y_L				0x8302
#define	ISP_BLC_START_Y_H				0x8303
#define	ISP_BLC_OFFSET_GR				0x8304
#define	ISP_BLC_GAIN_GR				0x8305
#define	ISP_BLC_OFFSET_R				0x8306
#define	ISP_BLC_GAIN_R					0x8307
#define	ISP_BLC_OFFSET_B				0x8308
#define	ISP_BLC_GAIN_B					0x8309
#define	ISP_BLC_OFFSET_GB				0x830A
#define	ISP_BLC_GAIN_GB 				0x830B

//0x831B~0x831F Reserved
#define	ISP_CCM_PARA_ADDR_BASE		0x8320
#define	ISP_CCM_R_AFFECT_R_H			0x8320
#define	ISP_CCM_R_AFFECT_R_L			0x8321
#define	ISP_CCM_G_AFFECT_R_H			0x8322
#define	ISP_CCM_G_AFFECT_R_L			0x8323
#define	ISP_CCM_B_AFFECT_R_H			0x8324
#define	ISP_CCM_B_AFFECT_R_L			0x8325
#define	ISP_CCM_R_AFFECT_G_H			0x8326
#define	ISP_CCM_R_AFFECT_G_L			0x8327
#define	ISP_CCM_G_AFFECT_G_H			0x8328
#define	ISP_CCM_G_AFFECT_G_L			0x8329
#define	ISP_CCM_B_AFFECT_G_H			0x832A
#define	ISP_CCM_B_AFFECT_G_L			0x832B
#define	ISP_CCM_R_AFFECT_B_H			0x832C
#define	ISP_CCM_R_AFFECT_B_L			0x832D
#define	ISP_CCM_G_AFFECT_B_H			0x832E
#define	ISP_CCM_G_AFFECT_B_L			0x832F
#define	ISP_CCM_B_AFFECT_B_H			0x8330
#define	ISP_CCM_B_AFFECT_B_L			0x8331
#define	ISP_CCM_SYNC					0x8335

//0x8336~0x8337 Reserved
#define	ISP_BRIGHTNESS					0x8338

//0x833D~0x833F	Reserved
#define	ISP_GAMMA_ADDR_BASE			0x8340 //0x8340~0x8357 
#define	ISP_GAMMA_ADDR_P0			0x8340
#define	ISP_GAMMA_ADDR_P1			0x8341
#define	ISP_GAMMA_ADDR_P2			0x8342
#define	ISP_GAMMA_ADDR_P3			0x8343
#define	ISP_GAMMA_ADDR_P4			0x8344
#define	ISP_GAMMA_ADDR_P5			0x8345
#define	ISP_GAMMA_ADDR_P6			0x8346
#define	ISP_GAMMA_ADDR_P7			0x8347
#define	ISP_GAMMA_ADDR_P8			0x8348
#define	ISP_GAMMA_ADDR_P9			0x8349
#define	ISP_GAMMA_ADDR_PA			0x834A
#define	ISP_GAMMA_ADDR_PB			0x834B
#define	ISP_GAMMA_ADDR_PC			0x834C
#define	ISP_GAMMA_ADDR_PD			0x834D
#define	ISP_GAMMA_ADDR_PE			0x834E
#define	ISP_GAMMA_ADDR_PF			0x834F
#define	ISP_GAMMA_ADDR_PG			0x8350
#define	ISP_GAMMA_ADDR_PH			0x8351
#define	ISP_GAMMA_ADDR_PI			0x8352
#define	ISP_GAMMA_ADDR_PJ			0x8353
#define	ISP_GAMMA_ADDR_PK			0x8354
#define	ISP_GAMMA_ADDR_PL			0x8355
#define	ISP_GAMMA_ADDR_PM			0x8356
#define	ISP_GAMMA_ADDR_PN			0x8357
#define	ISP_GAMMA_ADDR_PO			0x8358
#define	ISP_GAMMA_ADDR_PP			0x8359
#define	ISP_GAMMA_ADDR_PQ			0x835A
#define	ISP_GAMMA_ADDR_PR			0x835B
#define ISP_TGAMMA_CTRL 			0x835c 
#define	ISP_GAMMA_SYNC				0x835D
#define ISP_GAMMA_SEL 				0x835E
#define ISP_TGAMMA_RATE 			0x835F

//0x835D~0x837F reserved
#define ISP_GAMMA_R_PARAMETER		0x8380 //8380~839b
//839C~839F reserved
#define ISP_GAMMA_B_PARAMETER		0x83a0 //83A0~83Bb
//83Bc~83BF	reserved

#define	ISP_NLC_G_PARAMETER	0x83C0
#define	ISP_NLC_R_PARAMETER	0x83d0
#define	ISP_NLC_B_PARAMETER	0x83E0
#define	ISP_NLC_SYNC	    0x83f0
#define	ISP_NLC_SEL	        0x83f1

//0x83f2~0x83FF Reserved


//////////////////////////////
///Page4 : color &brightness on YUV domain
//0x8400~0x8418 Reserved

#define ISP_UVT_UCENTER 0x8418
#define ISP_UVT_VCENTER 0x8419
#define ISP_UVT_UINC 0x841A
#define ISP_UVT_VINC 0x841B

#define	ISP_HUE_SIN_X					0x841C
#define	ISP_HUE_COS_X					0x841D
#define	ISP_HUE_SIN_Y					0x841E
#define	ISP_HUE_COS_Y					0x841F
#define	ISP_MONO_MEAN 				0x8420
#define	ISP_SPECIAL_TARGET_U			0x8421
#define	ISP_SPECIAL_TARGET_V			0x8422
#define	ISP_SPECIAL_TUNE				0x8423
#define	ISP_CONTRAST_MEAN			0x8424
#define	ISP_CONTRAST					0x8425
#define	ISP_YMODE						0x8426
#define	ISP_Y_OFFSET					0x8427
#define	ISP_Y_GAIN						0x8428
#define	ISP_U_OFFSET					0x8429
#define	ISP_U_GAIN						0x842A
#define	ISP_V_OFFSET					0x842B
#define	ISP_V_GAIN						0x842C

//0x842d~0x842f Reserved
#define	ISP_YGAMMA_P0					0x8430
#define	ISP_YGAMMA_P1					0x8431
#define	ISP_YGAMMA_P2					0x8432
#define	ISP_YGAMMA_P3					0x8433
#define	ISP_YGAMMA_P4					0x8434
#define	ISP_YGAMMA_P5					0x8435
#define	ISP_YGAMMA_P6					0x8436
#define	ISP_YGAMMA_P7					0x8437
#define	ISP_YGAMMA_P8					0x8438
#define	ISP_YGAMMA_P9					0x8439
#define	ISP_YGAMMA_PA					0x843A
#define	ISP_YGAMMA_PB					0x843B
#define	ISP_YGAMMA_PC					0x843C
#define	ISP_YGAMMA_PD					0x843D
#define	ISP_YGAMMA_PE					0x843E
#define	ISP_YGAMMA_PF					0x843F
#define	ISP_YGAMMA_SYNC				0x8440
//0x8441~0x84FF reserved

///////////////////////////////////
//page5 NR & INTERPOLATION & IIR & EDGE_ENHANCE
#define ISP_RDLOC_DIST_START_L			0x8500
#define ISP_RDLOC_DIST_START_H		0x8501
#define ISP_RDLOC_DIST_END_L			0x8502
#define ISP_RDLOC_DIST_END_H			0x8503
#define ISP_RDLOC_MAX					0x8504
#define ISP_RDLOC_RATE					0x8505
#define ISP_RDLOC_ENABLE				0x8506
#define ISP_GLOC_MAX					0x8507
#define ISP_GLOC_RATE					0x8508
#define ISP_GLOC_ENABLE				0x8509
#define ISP_RD_MM2_RATE				0x850A
#define ISP_DDP_SEL					0x850B
#define ISP_NR_MODE0_LPF				0x850C	
#define ISP_NR_MODE1_LPF				0x850D
#define ISP_NR_COLOR_ENABLE_CTRL		0x850E	
#define ISP_NR_COLOR_MAX				0x850F

#define ISP_DDP_CTRL					0x8510
#define ISP_DP_THD_D1					0x8511
#define	ISP_DP_BRIGHT_THD_MIN			0x8512
#define	ISP_DP_BRIGHT_THD_MAX			0x8513
#define	ISP_DP_DARK_THD_MIN			0x8514
#define	ISP_DP_DARK_THD_MAX			0x8515
//0x8516 Reserved
#define ISP_DDP_BRIGHT_RATE			0x8517

#define ISP_DPC_SEL 0x8518

#define ISP_DDP_DARK_RATE				0x8519
#define ISP_NR_EDGE_THD				0x851A
#define ISP_NR_MM_THD1					0x851D
#define ISP_NR_MODE					0x851F
#define ISP_NR_GRGB_CTRL				0x8520
//0x8521~0x8524
#define ISP_NR_CHAOS_MAX				0x8525
#define ISP_NR_CHAOS_THD				0x8526
#define ISP_NR_CHAOS_CFG				0x8527
#define ISP_NR_DTHD_CFG				0x8528
// 8529
#define ISP_NR_MMM_ENABLE				0x852A
#define ISP_NR_MMM_D0					0x852B
#define ISP_NR_MMM_D1					0x852C
#define ISP_NR_MMM_RATE				0x852D
#define ISP_NR_MMM_MIN					0x852E
#define ISP_NR_MMM_MAX				0x852F
#define ISP_INTP_EDGE_THD0				0x8530
#define ISP_INTP_EDGE_THD1				0x8531
#define ISP_INTP_DCT_B1					0x8532
#define ISP_INTP_DCT_B2					0x8533
#define ISP_INTP_MODE					0x8534
#define ISP_INTP_CHAOS_MAX				0x8535
#define ISP_INTP_CHAOS_THD				0x8536
#define ISP_INTP_CHAOS_CFG				0x8537
#define ISP_INTP_DTHD_CFG				0x8538

#define ISP_MOIRE_EDGE_CTRL			0x8539
#define ISP_MOIRE_CTRL					0x853A
#define ISP_MOIRE_MAX					0x853B
#define ISP_MOIRE_RATE_R				0x853C
#define ISP_MOIRE_RATE_B				0x853D
#define ISP_MOIRE_NUM_R_L				0x853E
#define ISP_MOIRE_NUM_R_H				0x853F
#define ISP_MOIRE_NUM_G_L				0x8540
#define ISP_MOIRE_NUM_G_H				0x8541
#define ISP_MOIRE_NUM_B_L				0x8542
#define ISP_MOIRE_NUM_B_H				0x8543

#define ISP_MOIRE_SUM_R_L				0x8544
#define ISP_MOIRE_SUM_R_M				0x8545
#define ISP_MOIRE_SUM_R_H				0x8546
#define ISP_MOIRE_SUM_G_L				0x8547
#define ISP_MOIRE_SUM_G_M				0x8548
#define ISP_MOIRE_SUM_G_H				0x8549
#define ISP_MOIRE_SUM_B_L				0x854A
#define ISP_MOIRE_SUM_B_M				0x854B
#define ISP_MOIRE_SUM_B_H				0x854C

#define ISP_INTP_EDGE_SMOOTH			0x854D
#define ISP_ILOC_MAX					0x854E
#define ISP_ILOC_RATE					0x854F

#define ISP_RGB_IIR_CTRL 				0x8550
#define ISP_RGB_VIIR_COEF				0x8551
#define ISP_RGB_HLPF_COEF				0x8552
#define ISP_RGB_DIIR_MAX				0x8553
#define ISP_RGB_DIIR_COEF				0x8554
#define ISP_RGB_BRIGHT_COEF			0x8555
#define ISP_RGB_DIIR_COFF_CUT			0x8556

#define ISP_MOIRE_RATE 				0x8558 

#define ISP_EDG_DIFF_C0  				0x8560  
#define ISP_EDG_DIFF_C1  				0x8561  
#define ISP_EDG_DIFF_C2  				0x8562  
#define ISP_EDG_DIFF_C3  				0x8563  
#define ISP_EDG_DIFF_C4  				0x8564  
#define ISP_EDG_DCT_THD1				0x8565

#define ISP_EEH_DTHD_CFG				0x8569
#define ISP_EEH_UVIIR_Y_CUTS			0x856A
#define ISP_EEH_UVIIR_Y_CMIN			0x856B
#define ISP_EEH_IIR_COEF				0x856C
#define ISP_EEH_IIR_CUTS				0x856D
#define ISP_EEh_IIR_STEP				0x856E
#define ISP_EEH_IIR_ENABLE			0x856F
#define ISP_FCRD_MIN					0x8570
#define ISP_FCRD_YCUTS		  			0x8571
#define ISP_FCRD_CFG					0x8572
#define ISP_FCRD_MAX					0x8573
#define ISP_RELIEVO_BRIGHT			0x8574
#define ISP_SHARPNESS					0x8575


//AF
#define ISP_AF_CTRL 					0x8576
#define ISP_AF_THD						0x8577
#define ISP_AF_START0_X_H				0x8578
#define ISP_AF_START0_X_L				0x8579
#define ISP_AF_START0_Y_H				0x857A
#define ISP_AF_START0_Y_L				0x857B
#define ISP_AF_END0_X_H 				0x857C
#define ISP_AF_END0_X_L				0x857D
#define ISP_AF_END0_Y_H 				0x857E
#define ISP_AF_END0_Y_L				0x857F
#define ISP_AF_START1_X_H				0x8580
#define ISP_AF_START1_X_L				0x8581
#define ISP_AF_START1_Y_H				0x8582
#define ISP_AF_START1_Y_L				0x8583
#define ISP_AF_END1_X_H				0x8584
#define ISP_AF_END1_X_L				0x8585
#define ISP_AF_END1_Y_H				0x8586
#define ISP_AF_END1_Y_L				0x8587
#define ISP_AF_SUM0 					0x8588
#define ISP_AF_SUM1 					0x858C
#define ISP_AF_NUM0  					0x8590
#define ISP_AF_NUM1  					0x8594

//..................
#define ISP_EEH_SHARP_ARRAY0			0x85E4
#define ISP_EEH_SHARP_ARRAY1			0x85E5
#define ISP_EEH_SHARP_ARRAY2			0x85E6
#define ISP_EEH_SHARP_ARRAY3			0x85E7
#define ISP_EEH_SHARP_ARRAY4			0x85E8
#define ISP_EEH_SHARP_ARRAY5			0x85E9
#define ISP_EEH_SHARP_ARRAY6			0x85EA
#define ISP_EEH_SHARP_ARRAY7			0x85EB
#define ISP_EEH_SHARP_ARRAY8			0x85EC
#define ISP_EEH_SHARP_ARRAY9			0x85ED
#define ISP_EEH_SHARP_ARRAY10			0x85EE
#define ISP_EEH_SHARP_ARRAY11			0x85EF
#define ISP_BRIGHT_TRM_B1				0x85F0
#define ISP_BRIGHT_TRM_B2				0x85F1
#define ISP_BRIGHT_TRM_THD0			0x85F2
#define ISP_BRIGHT_TRM_THD1			0x85F3
#define ISP_BRIGHT_TRM_K				0x85F4
#define ISP_DARK_TRM_B1				0x85F5
#define ISP_DARK_TRM_B2				0x85F6
#define ISP_DARK_TRM_THD0				0x85F7
#define ISP_DARK_TRM_THD1				0x85F8
#define ISP_DARK_TRM_K					0x85F9
#define ISP_BRIGHT_RATE					0x85FA
#define ISP_DARK_RATE					0x85FB
#define ISP_EEH_CRC_RATE				0x85FC


//////////////////////////////////
//Page6 zoom&JPEG
#define	ISP_ZOOM_START_X_L			0x8600
#define	ISP_ZOOM_START_X_H			0x8601
#define	ISP_ZOOM_START_Y_L			0x8602
#define	ISP_ZOOM_START_Y_H			0x8603
#define	ISP_ZOOM_STEP_X_L				0x8604
#define	ISP_ZOOM_STEP_X_H				0x8605
#define	ISP_ZOOM_STEP_Y_L				0x8606
#define	ISP_ZOOM_STEP_Y_H				0x8607

#define	ISP_ZOOM_COEF0				0x8608
#define	ISP_ZOOM_COEF1				0x8609
#define	ISP_ZOOM_COEF2				0x860A
#define	ISP_ZOOM_COEF3				0x860B
#define	ISP_ZOOM_COEF4				0x860C
#define	ISP_ZOOM_COEF5				0x860D
#define	ISP_ZOOM_COEF6				0x860E
#define	ISP_ZOOM_COEF7				0x860F
#define	ISP_ZOOM_COEF8				0x8610
#define	ISP_ZOOM_COEF9				0x8611
#define	ISP_ZOOM_COEFA				0x8612
#define	ISP_ZOOM_COEFB				0x8613

#define	ISP_ZOOM_SYNC					0x8615

#define	ZOOM_START_X_TUNE_L			0x8616
#define	ZOOM_START_X_TUNE_H			0x8617
#define	ZOOM_HORIZ_STEP_TUNE_L		0x8618
#define	ZOOM_HORIZ_STEP_TUNE_M		0x8619
#define	ZOOM_HORIZ_STEP_TUNE_H		0x861A


#if  ((defined _ENABLE_ANTI_FLICK_ )||(defined _ENABLE_OUTDOOR_DETECT_))
#define	FLICK_CTRL								0x8620
#define	FLICK_HORIZONTAL_SAMPLE_STEP		0x8621
#define	FLICK_VERTICAL_SAMPLE_STEP			0x8622
#define	FLICK_HORIZONTAL_SAMPLE_START_L		0x8623
#define	FLICK_HORIZONTAL_SAMPLE_START_H 	0x8624
#define	FLICK_VERTICAL_SAMPLE_START_L		0x8625
#define	FLICK_VERTICAL_SAMPLE_START_H		0x8626
#define	FLICK_PRE_SHIFT_DATA_LEFT_BITS 		0x8627
#define FLICK_MCU_RW_CTRL						0x8628
#define	FLICK_FFT_RESULT_ADDRESS				0x8629
#define	FLICK_FLICK_MEM_DATA_2				0x862A
#define	FLICK_FLICK_MEM_DATA_1				0x862B
#define	FLICK_FLICK_MEM_DATA_0 				0x862C
#define	FLICK_FFT_RESULT_CONTRL 				0x862D
#define FLICK_FFT_RESUT_SUM2_127_4			0x862E
#define FLICK_FFT_RESUT_SUM2_127_3			0x862F
#define FLICK_FFT_RESUT_SUM2_127_2			0x8630
#define FLICK_FFT_RESUT_SUM2_127_1			0x8631
#endif

//8632~867F	Reserved.	

//#ifdef _ENABLE_MJPEG_
#define JENC_START	              			0x8680
#define JENC_CONFIGURE             			0x8681
#define JENC_IMAGESIZE_XL          			0x8682
#define JENC_IMAGESIZE_XH         			0x8683
#define JENC_IMAGESIZE_YL          			0x8684
#define JENC_IMAGESIZE_YH         			0x8685
#define JENC_MCUNUM_L              			0x8686
#define JENC_MCUNUM_H              			0x8687
#define JENC_RST_INTERVAL_L        		0x8688
#define JENC_RST_INTERVAL_H        		0x8689

#define JENC_COMPONENT0_L          		0x868A
#define JENC_COMPONENT0_H          		0x868B
#define JENC_COMPONENT1_L          		0x868C
#define JENC_COMPONENT1_H          		0x868D
#define JENC_COMPONENT2_L          		0x868E
#define JENC_COMPONENT2_H          		0x868F
#define JENC_COMPONENT3_L          		0x8690
#define JENC_COMPONENT3_H          		0x8691
#define JENC_FRAME_INTERVAL_L      		0x8692
#define JENC_FRAME_INTERVAL_H      		0x8693
#define JENC_QTABLE_ACCESS            		0x8694
#define JENC_QTABLE_ADDR                 		0x8695
#define JENC_QTABLE_DATA0                 	0x8696
#define JENC_QTABLE_DATA1				0x8697
#define JENC_QTABLE_DATA2                 	0x8698

#define JENC_MEM_OVERFLOW                 	0x8699// not used , zb
#define JENC_ACRA_HIGH                 		0x869A
#define JENC_ACRA_LOW                 		0x869B
#define JENC_ACRA_LOW_NUM                 	0x869C
#define JENC_ACRA_RATE                 		0x869D
#define JENC_ACRA_INV_HIGH_L                 0x869E
#define JENC_ACRA_INV_HIGH_H                 0x869F
#define JENC_ACRA_INV_NORM_L               	0x86A0
#define JENC_ACRA_INV_NORM_H               	0x86A1
#define ISP_JPEG_ACRA_ENBALE			0x86A2
//#endif //_ENABLE_MJPEG_

#define ISP_M420_CTRL						0x86A3
#define ISP_M420_INTERNAL_SPEED_CTL		0x86A4
#define ISP_M420_LASTROW_SPEED_CTL		0x86A5

#define ISP_JPEG_ALIGN_MODE			0x86A6
#define ISP_ACRA_RATE_LOAD				0x86A7

#define JENC_FRAMEWIDTH_H 0x86a8
#define JENC_FRAMEWIDTH_L 0x86a9
#define JENC_FRAMEHEIGHT_H 0x86aa
#define JENC_FRAMEHEIGHT_L 0x86ab

#define MIPI_APHY_REG0      0x86C0
#define MIPI_APHY_REG1      0x86C1
#define MIPI_APHY_REG2      0x86C2
#define MIPI_APHY_REG3      0x86C3
#define MIPI_APHY_REG4      0x86C4
#define MIPI_APHY_REG5      0x86C5
#define MIPI_APHY_REG6      0x86C6
#define MIPI_APHY_REG7      0x86C7
#define MIPI_APHY_REG8      0x86C8
#define MIPI_APHY_REG9      0x86C9
#define MIPI_APHY_REG10     0x86CA
#define MIPI_APHY_REG11     0x86CB
#define MIPI_APHY_REG12     0x86CC
#define MIPI_APHY_REG13     0x86CD
#define MIPI_APHY_REG14     0x86CE
#define MIPI_APHY_REG15     0x86CF
#define MIPI_APHY_REG16     0x86D0
#define MIPI_APHY_REG17     0x86D1
#define MIPI_APHY_REG18     0x86D2

#define MIPI_DPHY_CTRL      0x86D3
#define MIPI_DPHY_DATA_FORMAT      0x86D4
#define MIPI_DPHY_DATA_TYPE      0x86D5
#define MIPI_DPHY_DEGLITCH      0x86D6
#define MIPI_DPHY_DIVSEL	0x86D7
#define MIPI_DPHY_PWDB		0x86D8

#define MIPI_DPHY_DATALANE0_ESCAPE      0x86E3
#define MIPI_DPHY_DATALANE1_ESCAPE      0x86E4
#define MIPI_DPHY_INT_EN      0x86E5
#define MIPI_DPHY_INT_STATUS      0x86E6
#define MIPI_DPHY_HSTERM      0x86E7
#define MIPI_DPHY_ECC_ERR_L      0x86E8
#define MIPI_DPHY_ECC_ERR_H     0x86E9
#define MIPI_DPHY_CRC_ERR_L     0x86EA
#define MIPI_DPHY_CRC_ERR_H     0x86EB
#define MIPI_DPHY_ACTIVE_LEVEL     0x86EC
#define MIPI_DPHY_CLKLANE_WAKEUP_L     0x86ED
#define MIPI_DPHY_CLKLANE_WAKEUP_H     0x86EE
#define MIPI_DPHY_RESET		0x86EF

#define ISP_MIPI_DBG_MODE   0x86FE
#define ISP_MIPI_DBG_SEL    0x86FF

// surrend de-noise
#define ISP_LOC_DIST_START_L		0x8728
#define ISP_LOC_DIST_START_H		0x8729
#define ISP_LOC_DIST_END_L			0x872A
#define ISP_LOC_DIST_END_H			0x872B
#define ISP_LOC_MAX					0x872C
#define ISP_LOC_RATE				0x872D
#define ISP_LOC_ENABLE				0x872E

#define ISP_COLOR_RATE      0x8780	
#define ISP_COLOR_UMIN0     0x8781	
#define ISP_COLOR_UMAX0     0x8782	
#define ISP_COLOR_VMIN0     0x8783	
#define ISP_COLOR_VMAX0     0x8784	
#define ISP_COLOR_UMIN1     0x8785	
#define ISP_COLOR_UMAX1     0x8786	
#define ISP_COLOR_VMIN1     0x8787	
#define ISP_COLOR_VMAX1     0x8788	
#define ISP_COLOR_UMIN2     0x8789	
#define ISP_COLOR_UMAX2     0x878a	
#define ISP_COLOR_VMIN2     0x878b	
#define ISP_COLOR_VMAX2     0x878c	
#define ISP_COLOR_UMIN3     0x878d	
#define ISP_COLOR_UMAX3     0x878e	
#define ISP_COLOR_VMIN3     0x878f	
#define ISP_COLOR_VMAX3     0x8790	
#define ISP_COLOR_UMIN4     0x8791	
#define ISP_COLOR_UMAX4     0x8792	
#define ISP_COLOR_VMIN4     0x8793	
#define ISP_COLOR_VMAX4     0x8794	
#define ISP_COLOR_UMIN5     0x8795	
#define ISP_COLOR_UMAX5     0x8796	
#define ISP_COLOR_VMIN5     0x8797	
#define ISP_COLOR_VMAX5     0x8798	
#define ISP_COLOR_CTRL      0x8799	

#define	ISP_COLOR_NUM0 	0x879c
#define	ISP_COLOR_NUM1 	0x87a0
#define	ISP_COLOR_NUM2 	0x87a4
#define	ISP_COLOR_NUM3 	0x87a8
#define	ISP_COLOR_NUM4 	0x87ac
#define	ISP_COLOR_NUM5 	0x87b0
#define	ISP_COLOR_YSUM 	0x87b4

#define	ISP_HDR_ENABLE               	0x8800
#define	ISP_LOC_CONT_LEVEL          	0x8801
#define	ISP_HDR_LEVEL                	0x8802
#define	ISP_LOC_CONT_RATE_MIN       	0x8803
#define	ISP_LOC_CONT_RATE_MAX       	0x8804
#define	ISP_HDR_HALO_THD            	0x8805
#define ISP_HDR_ADJ_CTRL              0x8806	
#define ISP_HDR_ADJ_GOING            	0x8807	
#define ISP_HDR_STEP                  	0x8808	
#define ISP_LOC_CONT_STEP             0x8809	
#define ISP_HDR_LEVEL_NOW             0x880A	
#define ISP_LOC_CONT_LEVEL_NOW       	0x880B	
#define	ISP_LOC_CONT_PARAMETER      	0x8810
#define	ISP_HDR_PARAMETER            	0x8820
#define	ISP_HDR_LPF_COEF             	0x8840
#define	ISP_HDR_MAX_COEF             	0x8850

#define HW_TM_INIT_LL		0xDE50
#define HW_TM_INIT_LH		0xDE51
#define HW_TM_INIT_HL		0xDE52
#define HW_TM_INIT_HH		0xDE53


//////////////////////////////////////
//SPI WB registers
//////////////////////////////////////
#define SPI_COM_TRANSFER		0xFCE0
#define SPI_SUB1_MODE			0xFCE1
#define SPI_SUB1_COMMAND		0xFCE2
#define SPI_SUB1_ADDR0			0xFCE3
#define SPI_SUB1_ADDR1			0xFCE4
#define SPI_SUB1_ADDR2			0xFCE5
#define SPI_SUB1_LENGTH		0xFCE6
#define SPI_SUB2_MODE			0xFCE7
#define SPI_SUB2_COMMAND		0xFCE8
#define SPI_SUB2_ADDR0			0xFCE9
#define SPI_SUB2_ADDR1			0xFCEA
#define SPI_SUB2_ADDR2			0xFCEB
#define SPI_SUB2_LENGTH		0xFCEC
#define SPI_SUB3_MODE			0xFCED
#define SPI_SUB3_COMMAND		0xFCEE
#define SPI_SUB3_ADDR0			0xFCEF
#define SPI_SUB3_ADDR1			0xFCF0
#define SPI_SUB3_ADDR2			0xFCF1
#define SPI_SUB3_LENGTH		0xFCF2
#define SPI_SUB4_MODE			0xFCF3
#define SPI_SUB4_COMMAND		0xFCF4
#define SPI_SUB4_ADDR0			0xFCF5
#define SPI_SUB4_ADDR1			0xFCF6
#define SPI_SUB4_ADDR2			0xFCF7
#define SPI_SUB4_LENGTH		0xFCF8
#define SPI_COM_TCTL1			0xFCF9
#define SPI_COM_TCTL2			0xFCFA
#define SPI_TOP_CTL				0xFCFB
#define SPI_COM_CRAM_ADDR		0xFCFC
// 0xFCFD~0xFCFF reserved

#if (_CHIP_ID_ & _RTS5829B_)
#define CFG_SAFE_BYTE 			0xFD80
#define CFG_SAFE_TIME1			0xFD81
#define CFG_SAFE_TIME0			0xFD82
#endif

//////////////////////////////////////
//usb system control registers
//////////////////////////////////////
#define USBCTL                                   0xFE00
#define USBSTAT                                 0XFE01
#define EPTOP_IRQSTAT                    0XFE02
#define USBSYS_IRQSTAT                 0XFE03
#define EPTOP_IRQEN                        0XFE04
#define USBSYS_IRQEN                     0XFE05
#define DEVADDR                                0XFE06
#define UTMI_STA                               0XFE07
#define PHY_TUNE                              0XFE08
#define UTMI_TUNE                             0XFE09
#define SLB_SEED	                            0XFE0A
#define SLB_BIST                                  0XFE0B
#define VCONTROL                              0XFE0C
#define VSTATUSIN                             0XFE0D
#define VLOADM                                  0XFE0E
#define VSTATUSOUT                          0XFE0F
#define FORCE_TOGGLE                     0XFE10
#define UTMI_TST                                0XFE11
#define FRCERST                                  0XFE12
#define USBTEST                                  0XFE13
#define PKTERR_CNT_L                       0XFE14
#define PKTERR_CNT_H                      0XFE15
#define RXERR_CNT_L                         0XFE16
#define RXERR_CNT_H                        0XFE17
#define FNUM0                                     0XFE18
#define FNUM1                                     0XFE19
#define ERRCNT_CTL                           0XFE1A
#define TST_CTL				 0XFE1B
#define POR_NO_SUSPEND			0XFE1C
#define SIE_STATUS				0xFE1D
// 0xFE1E		reserved
#define RST_LOAD_MODE			0xFE1F
#define PHY_DEBUG_MODE		0xFE20
#define HW_VER_ID1                	0xFE21
#define HW_VER_ID2				0xFE22
#define SIE_FCNT_EN				0xFE23
#define SIE_FCNT0				0xFE24
#define SIE_FCNT1				0xFE25
#define SIE_FCNT2				0xFE26
#define SIE_TCNT0				0xFE27
#define SIE_TCNT1				0xFE28
#define SIE_FRAME_DROP			0xFE29
#define FCNT_SEC_POWER			0xFE2A
#define FCNT_INT				0xFE2B
#define SIE_CUR_FNUM_L			0xFE2C
// FE2D reserved
#define DUMMY0					0xFE2E
#define DUMMY1					0xFE2F
//////////////////////////////////////
//usb ENDPOINT control registers
//////////////////////////////////////
//EP0
#define EP0_CFG                                 0XFE30
#define EP0_CTL                                  0XFE31
#define EP0_STAT                                0XFE32
#define EP0_IRQEN                              0XFE33
#define EP0_IRQSTAT                          0XFE34
#define EP0_MAXPKT                           0XFE35
#define EP0_DAT                                   0XFE36
#define EP0_BC                                     0XFE37
//EPA
#define EPA_CFG                                  0XFE38
#define EPA_CTL                                   0XFE39
#define EPA_STAT                                 0XFE3A
#define EPA_IRQEN                              0XFE3B
#define EPA_IRQSTAT                          0XFE3C
#define EPA_MAXPKT0                         0XFE3D
#define EPA_MAXPKT1                         0XFE3E
#define EPA_DAT                                   0XFE3F
#define EPA_BC0                                   0XFE40
#define EPA_BC1                                   0XFE41
// FE42 reserved
#define EPA_FLOW_CTL                        0xFE43
// FE44 reserved
//EPC
#define EPC_CFG                                 0XFE45
#define EPC_CTL                                  0XFE46
#define EPC_STAT                                0XFE47
#define EPC_IRQEN                              0XFE48
#define EPC_IRQSTAT                          0XFE49
#define EPC_MAXPKT                           0XFE4A
#define EPC_DAT                                   0XFE4B
#define EPC_BC                                     0XFE4C
#define LPM_CFG						0xFE4D
// FE4E~FE4F reserved
//////////////////////////////////////
//usb video streaming registers
//////////////////////////////////////
/*
#define PTS_CTL		0xFE50
#define PTS_FPS		0xFE51
#define PTS_INTERVAL_N2	0xFE52
#define PTS_INTERVAL_N1	0xFE53
#define PTS_INTERVAL_N0	0xFE54
#define PTS_INTERVAL_F		0xFE55
*/

#ifdef _USB2_LPM_ // JQG_add_2010_0415_
#define USB_LPM			0xFE56
#define USB_LPM2		0xFE57
#endif
//EPD for AUDIO Interrupt
#define EPD_CFG 			0xFE58
#define EPD_CTL 			0xFE59
#define EPD_STAT 		0xFE5A
#define EPD_IRQEN		0xFE5B
#define EPD_IRQSTAT		0xFE5C
#define EPD_MAXPKT		0xFE5D
#define EPD_DAT			0xFE5E
#define EPD_BC			0xFE5F
//#define EPE_CFG                              	0XFE7B
//#define EPE_CTL                              	0XFE7C
//#define EPE_IRQEN                            	0XFE4E
//#define EPE_IRQSTAT                         	0XFE4F
#define EPAE_FLOW_CTL                        	0xFE43

// FE60~FE5F
#define PH_INFO                                   0XFE60
#define PH_PTS3                                   0XFE61
#define PH_PTS2                                   0XFE62
#define PH_PTS1                                   0XFE63
#define PH_PTS0                                   0XFE64
#define PH_SCR3                                   0XFE65
#define PH_SCR2                                   0XFE66
#define PH_SCR1                                   0XFE67
#define PH_SCR0                                   0XFE68
#define PH_SOFCNT_H                         0XFE69
#define PH_SOFCNT_L                          0XFE6A
#define PH_CTL				    0XFE6B

#define RF_EPA_TRANSFER_SIZE_0               0XFE6E
#define RF_EPA_TRANSFER_SIZE_1               0XFE6F
/////////////////////////////////////
//////////////////////////////////////
//EPB registers
//EPB for AUDIO Streaming
//#define EPB_CFG			0xFE50
//#define EPB_CTL			0xFE51
//#define EPB_STAT			0xFE52
//#define EPB_IRQEN 		0xFE53
//#define EPB_IRQSTAT		0xFE54
//#define EPB_MAXPKT0		0xFE55
//#define EPB_MAXPKT1		0xFE56
//#define EPB_DAT			0xFE57
//#define EPB_BC0			0xFE58
//#define EPB_BC1			0xFE59
//#define EPB_FLOW_CTL	0xFE5A
#define EPB_CFG			0xFE70
#define EPB_CTL			0xFE71
//#define EPB_STAT		0xFE72
#define EPB_IRQEN 		0xFE73
#define EPB_IRQSTAT		0xFE74
#define EPB_MAXPKT0		0xFE75
#define EPB_MAXPKT1		0xFE76
#define EPB_DAT			0xFE77
//#define EPB_BC0			0xFE78
//#define EPB_BC1			0xFE79
#define EPB_FLOW_CTL	0xFE7A
#define RF_HWM_0				0xFE7B
#define RF_HWM_1				0xFE7C
#define RF_LWM_0				0xFE7D
#define RF_LWM_1				0xFE7E
#define RF_WM_ST				0xFE7F
//system control registers
//////////////////////////////////////
// boot regs
//#define BOOTCTL   				 0xFE80	// hemonel 2010-05-25 move this register to 0xFFA8
// FE81 reserved
//CLK regs
#define MCUCLK					0xFE81
#define CLKCTL       				 0xFE82
#define CLKDIV     				 0xFE83
#define CLKSEL    				 0xFE84
//WATCH dog control regs
#define WTDG_CTL 				  0XFE85
#define WTDG_INTCTL			  0XFE86

//soft interrupt registers
#define SYS_SOFTIRQ_EN            	0XFE8D
//#define SYS_SOFTIRQ_TRIGGER	0XFE8E //_JQG_20091123_5803_delt this register
#define SYS_SOFTIRQ_STS             	0XFE8F
// FE90~FE92 reserved
//MCU interface control regs
#define	SR_MCU_CTL			0xFE93
#define    DRV_MCU_CTL                   0xFE94
#define	MCU_OE_CTL			0xFE95
#define 	BLK_CLK_EN			0xFE96
// FE97~FE99 membist
#define RS232_UTXD  			0xFE97
#define RS232_UCTL  				0xFE98

//SCRATCH 0, 1
#define	SCRATCH0				0xFE9A
#define    SCRATCH1                          0xFE9B
//MEM BIST
#define	MEMBIST	                           0xFE9C
//////////////////////////////////////
//GPIO control registers
//////////////////////////////////////
#define    SPI_OE_CTL                      0xFE9D
#define    GPIO_SPI_SLWR_CTL      0XFE9E
#define    GPIO_SPI_DRV_CTL         0XFE9F
#define 	GPIO_DIR_L                               0xFEA0
#define 	GPIO_DIR_H                              0xFEA1
#define 	GPIO_PULL_CTL_L0                 0xFEA2
#define 	GPIO_PULL_CTL_L1                 0xFEA3
#define 	GPIO_PULL_CTL_H0                0xFEA4
#define 	GPIO_INT_CTL                         0xFEA5
#define 	GPIO_L                      		       0xFEA6
#define 	GPIO_H                                      0xFEA7
#define 	GPIO_IRQEN_FALL_L              0xFEA8
#define 	GPIO_IRQEN_FALL_H             0xFEA9
#define 	GPIO_IRQEN_RISE_L              0xFEAA
#define 	GPIO_IRQEN_RISE_H             0xFEAB
#define 	GPIO_IRQSTAT_FALL_L         0xFEAC
#define 	GPIO_IRQSTAT_FALL_H        0xFEAD
#define 	GPIO_IRQSTAT_RISE_L         0xFEAE
#define 	GPIO_IRQSTAT_RISE_H        0xFEAF


//////////////////////////////////////
//SPI control registers
//////////////////////////////////////
#ifdef _CACHE_MODE_
#define  SPI_CLK_DIVIDER0                  0xFEBA
#else//#ifndef _CACHE_MODE_
#define   SPI_COMMAND                          0xFEB0
#define   SPI_ADDR0                      0xFEB1
#define   SPI_ADDR1                      0xFEB2
#define   SPI_ADDR2                      0xFEB3
#define   SPI_CA_NUMBER                    0xFEB4
#define   SPI_LENGTH0                   0xFEB5
#define   SPI_LENGTH1                   0xFEB6
#define  SPI_TRANSFER                  0xFEB7
#define  SPI_CONTROL                   0xFEB8
#define  SPI_TCTL                            0xFEB9
#define  SPI_CLK_DIVIDER0                  0xFEBA
#define  SPI_CLK_DIVIDER1                  0xFEBB
#define  SPI_XCHGM_ADDR0        0xFEBC
// FEBD reserved
#define  SPI_SIG                             0xFEBE
#define  SPI_PULL_CTL                  0xFEBF
#endif

//////////////////////////////////////
//Camera Sensor Controller registers
//////////////////////////////////////
//#define CCS_CLK_SWITCH		0xFEC0	// move to reg2.h
#define CCS_DRIVE_SEL		0xFEC1
#define CCS_SR_CTL			0xFEC2
#define CCS_MISC_CTL		0xFEC3
// FEC4~FEC8 reserved
#define CCS_PIN_PULL_CTL1	0xFEC9
#define CCS_PIN_PULL_CTL2 	0xFECA
#define PIN_SHARE_SEL		0xFECB
#define CCS_CLK_SWITCH_CTL	0xFECC
//#define CCS_CONTROL			0xFECD	// move to reg2.h
#define CCS_HSYNC_TCTL0	0xFECE
#define CCS_HSYNC_TCTL1	0xFECF
#define CCS_VSYNC_TCTL0	0xFED0
#define CCS_VSYNC_TCTL1	0xFED1
#define CCS_HORIZON_NUM0	0xFED2
#define CCS_HORIZON_NUM1	0xFED3
#define CCS_VERTICAL_NUM0	0xFED4
#define CCS_VERTICAL_NUM1	0xFED5
#define CCS_TRANSFER		0xFED6
#define CCS_INT_EN			0xFED7
//#define CCS_INT_STS			0xFED8	// move to reg2.h
#define CCS_FRAME_CNT		0xFED9
#define CCS_SUBSAMPLE_CTL	0xFEDA
#define CCS_SUBSAMPLE_HOR	0xFEDB
#define CCS_SUBSAMPLE_VER	0xFEDC
#define CCS_FRAME_VLD_CTL   0xFEDD
//0xFEDE~0xFEDF 	reserved

//////////////////////////////////////
//Sensor I2C Controller registers
//////////////////////////////////////
#define I2C_ADDR0			0xFEE0
#define I2C_ADDR1			0xFEE1
#define I2C_ADDR2			0xFEE2
#define I2C_ADDR3			0xFEE3
#define I2C_ADDR4			0xFEE4
#define I2C_ADDR5			0xFEE5
#define I2C_ADDR_LEN		0xFEE6
// FEE7~FEED reserved
#define I2C_DATA_LEN		0xFEEE
#define I2C_TRANSFER		0xFEEF
#define I2C_STATUS			0xFEF0
#define I2C_PULL_CTL			0xFEF1
#define I2C_TCTL_SCL_LOW	0xFEF2
#define I2C_TCTL_SCL_HIGH	0xFEF3
#define I2C_TCTL_DAT_HD		0xFEF4
#define I2C_TCTL_SX_SU		0xFEF5
#define I2C_TCTL_SX_HD		0xFEF6
#define I2C_DATA_SCRATCH0	0xFEF7
#define I2C_DATA_SCRATCH1	0xFEF8
#define I2C_DATA_SCRATCH2	0xFEF9
#define I2C_DATA_SCRATCH3	0xFEFA
#define I2C_DATA_SCRATCH4	0xFEFB
#define I2C_DATA_SCRATCH5  0xFEFC
#define I2C_DATA_SCRATCH6  0xFEFD
#define I2C_DATA_SCRATCH7  0xFEFE
//0xFEFB~0xFEFF 		reserved

//////////////////////////////////////
//PHY RF registers
//////////////////////////////////////
//NON-CRYSTAL PHY Registers
#define FREQ_TOP_CONFIG_1    0xFF00
#define FREQ_TOP_CONFIG_2    0xFF01
#define FREQ_TOP_CONFIG_3    0xFF02
#define FREQ_TOP_CONFIG_4    0xFF03
#define FREQ_TOP_CONFIG_5    0xFF04
#define FREQ_TOP_CONFIG_6    0xFF05
#define FREQ_TOP_CONFIG_7    0xFF06

#if (_CHIP_ID_ & _RTS5840_)
#define FREQ_TOP_CONFIG_8    0xFF07
#define FREQ_TOP_CONFIG_9    0xFF08
#define FREQ_TOP_CONFIG_10  0xFF09
#else
#define FREQ_TOP_CONFIG_8    0xFFB9		//RTS5832 move 0xFF07~0xFF09 to 0xFFB9~0xFFBB
#define FREQ_TOP_CONFIG_9    0xFFBA
#define FREQ_TOP_CONFIG_10  0xFFBB
#endif

#define FREQ_TOP_CONFIG_11  0xFF0A
#define FREQ_TOP_CONFIG_12  0xFF0B

#define THERMAL_SENSOR_CTL	0xFF0C // this is for 5822
#define THERMAL_SENSOR_CFG	0xFF0D // this is for 5822
#define THERMAL_SENSOR_TEMPE	0xFF0E // this is for 5822
#define THERMAL_SENSOR_DEBUG	0xFF0F // this is for 5822

// FREQ_TOP_CONFIG_8
#define RESTORE_NF_EN		0x01
#define FRE_EXT_NF_SEL        (1<<4)
#define FRE_EXT_CTRL_EN      (1<<3)
//#define  LDO28_PWRCTL        0xFF0D 	// hemonel 2009-12-15: RTS5803 delete this register


//----------
//---JQG add below content, for 5803 change---
//----------
#define  OCP_PARAMETER0	0xFF10
#define  OCP_PARAMETER1	0xFF11
#define SV28_OCP_CTL		0xFF12
#define SV18_OCP_CTL		0xFF13
#define OCP_INT_EN			0xFF14
// FF15~FF16 reserved
#define SV28_PWR_CTL		0xFF17
#define SV18_PWR_CTL		0xFF18
#define RC_OSC_CFG               0xFF1C
// FF19~FF1F reserved
//////////////////////////////////////
//SSC registers
//////////////////////////////////////
#define SSCPLL_CSICP		0xFF20
#define SSCPLL_RS			0xFF21
#define SSC_DIV_N_L			0xFF22
#define SSC_DIV_N_H			0xFF23
#define SSC_DIV_F_SEL		0xFF24
#define SSC_DIV_EXT_F		0xFF25
#define SSC_TBASE			0xFF26
#define SSC_STEP_IN			0xFF27
// FF28~FF2F reserved
//////////////////////////////////////

//AD filter registers
#define ADF_CFG				0xFF30
#define ADF_GAIN_L			0xFF31
#define ADF_GAIN_R			0xFF32
#define ADF_CTRL0			0xFF33
#define ADF_CTRL1			0xFF34
#define ADF_CTRL2			0xFF35

#define 	DMIC_CTL			0xFF38
#define   DMIC_STATUS      	0xFF39
#define   DMIC_CLK_DETECT_CTL      0xFF3A
#define	DMIC_PAD_CTL		0xFF3B
#define	DMIC_PAD_INT_STS	0xFF3E
#define	DMIC_PAD_INT_EN	0xFF3F

//Cache control registers
//////////////////////////////////////
#define CACHE_CFG		0xFF40
#define CACHE_CTL		0xFF41
#define ROM_SIZE0		0xFF42
#define ROM_SIZE1		0xFF43
#define EXTCODE_START0		0xFF44
#define EXTCODE_START1		0xFF45
// FF46~FF4F reserved
//////////////////////////////////////
//Cache I2C registers
//////////////////////////////////////
#define CACHE_I2C_ADDR0		0XFF50
#define CACHE_I2C_ADDR1		0XFF51
#define CACHE_I2C_ADDR2		0XFF52
#define CACHE_I2C_ADDR3		0XFF53
#define CACHE_I2C_ADDR4		0XFF54
#define CACHE_I2C_ADDR5		0XFF55
#define CACHE_I2C_ADDR_LEN		0XFF56
#define CACHE_I2C_DATA_LEN0		0XFF57
#define CACHE_I2C_DATA_LEN1		0XFF58
#define CACHE_I2C_TRANSFER		0xFF59
#define CACHE_I2C_STATUS		0xFF5A
#define CACHE_I2C_PULL_CTL		0xFF5B
#define CACHE_I2C_TCTL_SCL_LOW0	0xFF5C
#define CACHE_I2C_TCTL_SCL_HIGH0	0xFF5D
#define CACHE_I2C_TCTL_SCL1		0xFF5E
#define CACHE_I2C_TCTL_DAT_HD	0xFF5F
#define CACHE_I2C_TCTL_SX_SU0	0xFF60
#define CACHE_I2C_TCTL_SX_HD0	0xFF61
#define CACHE_I2C_TCTL_SX1		0xFF62
#define CACHE_I2C_XCHGM_ADDR	0xFF63
#define CACHE_I2C_DEV_ADDR	0xFF64
#define CACHE_I2C_CTRL			0xFF65
// FF65~FF6F reserved
/////////////////////////////////////

//ALC registers
//#define ALC_CFG			0xFF70
//#define ALC_BK_GAIN_L	0xFF71
//#define ALC_BK_GAIN_R	0xFF72
//#define ALC_FT_BOOST 	0xFF73
//#define ALC_MISC_CFG	0xFF74
//#define ALC_ATK_CONFIG 	0xFF75
//#define ALC_NOISE_GATE_CFG0	0xFF76
//#define ALC_NOISE_GATE_CFG1	0xFF77
//#define ALC_NOISE_GATE_CFG2	0xFF78
//#define ALC_THRESHOLD	0xFF79
//#define ALC_ZERO_CFG	0xFF7A
//#define ALC_RC_CFG0		0xFF7B
//#define ALC_RC_CFG1		0xFF7C
//#define ALC_RC_WD_MAX	0xFF7D
//#define ALC_RC_WD_MIN	0xFF7E
//#define ALC_STATUS		0xFF7F

//DMIC control register
////////////////////////////////////
//#define    DMIC_STATUS      0xFE79
//#define    DMIC_CLK_DETECT_CTL      0xFE7A
//#define	MIC_PAD_CTL        0xFE7B
//#define	MIC_ADC_CTL_H   0xFE7C
//#define	MIC_ADC_CTL_L    0xFE7D
//#define	MIC_HID_CTL_H    0xFE7E
//#define	MIC_HID_CTL_L     0xFE7F

//////////////////////////////////////
//System control registers
//////////////////////////////////////
#define SYSCTL       				 0xFF90
#define PG_EN					0xFF91
#define PG_FSM_RST				0xFF92
#define PG_TCFG1				0xFF93
#define PG_TCFG2				0xFF94
#define PG_TCFG3				0xFF95
#define PG_TCFG4				0xFF96
#define PG_TCFG5				0xFF97
#define PG_TCFG6				0xFF98
#define PG_DMY0					0xFF99
#define PG_GPIO_AL_INT_STS		0xFF9A
#define PG_GPIO_AL_INT_EN		0xFF9B
#define PG_GPIO_AL_CTRL0		0xFF9C
#define PG_GPIO_AL_CTRL1		0xFF9D
#define PG_DELINK_CTRL			0xFF9E

//////////////////////////////////////
//PHY RF registers
//////////////////////////////////////
#define REG_BANDGAP		0xFFA0
#define REG_TUNE			0xFFA1
#define REG_HSTX_CFG		0xFFA2
#define REG_SENSEL			0xFFA3
//#define REG_TUNESVA		0xFFA4	// move to reg2.h
#define REG_FSSR			0xFFA5

#define AL_BOOTCTL			0xFFA8
#define NON_CRYSTAL_CTL	0xFFA9
//#define REG_TUNESVIO		0xFFAA	// move to reg2.h
#define REG_PHY_CFG_0		0xFFAC
#define REG_PHY_CFG_1		0xFFAD


#define PG_DEVADDR			0xFFB0
#define PG_SIE_STATUS		0xFFB1
#define PG_EPA_MAXPKT0 	0xFFB2
#define PG_EPA_MAXPKT1		0xFFB3
#define PG_EPB_MAXPKT0		0xFFB4
#define PG_EPB_MAXPKT1		0xFFB5
//#define PG_EPE_MAXPKT0		0XFFB8
#define PG_EPE_MAXPKT1		0XFFB9
#define PG_EPE_STALL_ENABLE 0xFFBA
#define AL_EP_EN_STALL		0xFFB6
#define AL_CACHE_CFG		0xFFB7
#define AL_LPM_REG			0xFFB8

//////////////////////////////////////
//Registers definitions end
//////////////////////////////////////

//%%%%%%%%%%%%%%%%%%%%
//registers bit definitions
//%%%%%%%%%%%%%%%%%%%%

//0x8034 ISP_AWB_STATIS_LOC
#define AWB_STATIS_LOC_MASK		0x01
#define AWB_STATIS_BEFORE_LSC		0x01
#define AWB_STATIS_AFTER_LSC		0x00

#define FLICK_STATIS_START 0x1
#define FLICK_STATIS_STOP 0x2
#define FLICK_IDLE 0x4
#define FLICK_FFT_ERROR  0x20
#define FLICK_MCU_READ  0x01
#define FLICK_MCU_WRITE 0x02
#define FLICK_MCU_READ_WRITE_DONE 0x04


//0x833c ISP_RGB_AE_GAIN_CONTROL
#define ISP_RGB_AE_GAIN_LOAD 0x80
#define ISP_RGB_AE_GAIN_0_FRAME 0x0
#define ISP_RGB_AE_GAIN_1_FRAME  0x1
#define ISP_RGB_AE_GAIN_2_FRAME  0x2
#define ISP_RGB_AE_GAIN_3_FRAME  0x3

//0x835c ISP_TGAMMA_CTRL
#define ISP_TGAMMA_EN 0x80

#ifdef _MIPI_EXIST_
//0X86C6 MIPI_APHY_REG6
#define MIPI_AUTO_SKEW_EN	0x02
#define MIPI_SKEW_EN		0x01

//0X86D2 MIPI_APHY_REG18
#define LANE0_AUTOSKEW_DONE	0x02
#define LANE1_AUTOSKEW_DONE	0x04
#define TWO_LANE_AUTOSKEW_DONE	0x06

//0x86D3 MIPI_DPHY_CTRL
#define MIPI_PIX_DIN_SEL     0x80
#define MIPI_CRC16_EN       0x40
#define MIPI_ECC_EN         0x20
#define MIPI_DATA_LANE_SWITCH   0x40
#define MIPI_BIT_SWITCH     0x10
#define MIPI_DATA_LANE1_EN  0x02
#define MIPI_DATA_LANE0_EN  0x01
#define MIPI_DATA_LANE_ALL_EN	(MIPI_DATA_LANE0_EN | MIPI_DATA_LANE1_EN)

//0x86D4 MIPI_DPHY_DATA_FORMAT
#define MIPI_DATA_FORMAT_RAW8   0x00
#define MIPI_DATA_FORMAT_RAW10  0x01
#define MIPI_DATA_FORMAT_YUV    0x02
#define MIPI_DATA_FORMAT_JPEG   0x03
#define MIPI_YUV_TYPE_NULL      0x00
#define MIPI_YUV_TYPE_UYVY      0x00
#define MIPI_YUV_TYPE_VYUY      0x10
#define MIPI_YUV_TYPE_YVYU      0x20
#define MIPI_YUV_TYPE_YUYV      0x30
//0x86D5 MIPI_DPHY_DATA_TYPE
#define MIPI_DATA_TYPE_YUYV8    0x1E
#define MIPI_DATA_TYPE_YUYV10   0x1F
#define MIPI_DATA_TYPE_RAW8     0x2A
#define MIPI_DATA_TYPE_RAW10    0x2B

#define SNR_MIPI_OUTPUT 0x01
#define SNR_DVP_OUTPUT  0x00

#define SEL_MIPI_DATA    0x01
#define SEL_CCS_DATA   0x00

//0x86D8 MIPI_DPHY_PWDB
#define MIPI_DATALANE0_PWDB_DIS	0x01
#define MIPI_DATALANE1_PWDB_DIS	0x02
#define MIPI_CLK_DATA_LANE_PWDB	0x00

//0x86EF MIPI_DPHY_RESET
#define RESET_MIPI_DPHY		0x01

#endif

// ISP_AWB_WIN_ENABLE	825Eh
#define AWB_WIN_ST_BRIGHT_CONSTRAIN_EN	0x01
#define AWB_WIN_ST_RBGB_CONSTRAIN_EN 		0x02
#define AWB_WIN_ST_BRGR_CONSTRAIN_EN		0x04
#define AWB_WIN_ST_BGRG_CONSTRAIN_EN		0x08

// ISP_CCM_SYNC			8335h
#define CCM_LOAD		0x01

// ISP_CCMB_SYNC 		8375h
#define CCMB_LOAD		0x01

// SPI_COM_TRANSFER 	FCE0
#define SPI_COM_TRANSFER_START		0x80
#define SPI_COM_TRANSFER_END			0x40
#define SPI_COM_TRANSFER_TIMEOUT		0x08
#define SPI_COM_TRANSFER_RESET		0x04
#define SPI_COM_TRANSFER_NUM_1		0x00
#define SPI_COM_TRANSFER_NUM_2		0x01
#define SPI_COM_TRANSFER_NUM_3		0x02
#define SPI_COM_TRANSFER_NUM_4		0x03

// ISP_CONTROL0		8000
#define ISP_FRAME_STOP					0x04
#define ISP_STOP						0x08
#define ISP_TRANSFER_IDLE				0x10
#define ISP_TRANSFER_GET_STILLIMG		0x40
#define ISP_TRANSFER_START				0x80

// ISP_CONTROL1		8001
#define ISP_YGAMMA_EN					0x01
#define ISP_ZOOM_EN					0x02
#define ISP_EDGEENHANCE_EN			0x04
#define ISP_GAMMA_EN					0x08
#define ISP_IIRFILTER_EN 				0X10
#define ISP_DEADPXL_EN					0x20
#define ISP_MICROLSC_EN				0x40
#define ISP_NORMALSC_EN				0x80

//ISP_CONTROL2	8002
#define ISP_EDGEDRAW_EN				0x01
#define ISP_SPECIAL_EN					0x02
#define ISP_MONO_EN					0x08
#define ISP_GRAY_EN						0x10
#define ISP_NEGATIVE_EN					0x20
#define ISP_QUARTERSAMPLE_EN			0x40
#define ISP_HALFSAMPLE_EN				0x80

#define ISP_SPECIAL_EFFECT_MASK	(ISP_MONO_EN|ISP_EDGEDRAW_EN|ISP_SPECIAL_EN|ISP_GRAY_EN|ISP_NEGATIVE_EN)

//ISP_CONTROL3	8003
#define ISP_TRAPEZIUMCORRECT_EN		0x01
#define ISP_NLC_EN		0x10
#define ISP_UV_TUNE_EN		0x08


//ISP_IMAGE_SEL	8005
#define ISP_IM_SOURCE_YUV	0x80
#define ISP_PATHC_SELECT	0x40
#define ISP_IM_SOURCE_RAW  0x00

#define ISP_BUS_WiDTH16_5821	0x10
#define ISP_BUS_WiDTH16_5827	0x01
#define ISP_BUS_WiDTH32_5827	0x03
#define ISP_BUS_WiDTH8_JPEG_BYPASS	0x00

#define YUV422_FINAL_FORMAT 	0x00
#define YUV420_VENDER_FORMAT	0x02
#define MJPEG_FORMAT	0x03
#define RSDH_FORMAT 0x04
#define MJPG_BY_PASS	0x0E

//ISP_INT_EN0 && ISP_INT_FLAG0	8006 & 8007
#define ISP_FRM_START_INT		0x40
#define ISP_AE_STAT_INT			0x20
#define ISP_JEPG_OF_INT			0x10
#define ISP_DEADPIXL_INT		0x08
#define ISP_FRAMEABORT_INT	0x04
#define ISP_AF_STAT_INT		0x02
#define ISP_AWB_STAT_INT		0x01

//ISP_INT_EN1 && ISP_INT_FLAG1	8008 & 8009
#define ISP_BRIGHT_COMP_INT	0x01
#if  ((defined _ENABLE_ANTI_FLICK_ )||(defined _ENABLE_OUTDOOR_DETECT_))
#define ISP_FLICK_DETECT_INT 0x10
#endif

//ISP_EEH_SPEED_CTRL	8020
#define ISP_EDG_SPEED_MULT		0xF0
#define ISP_EDG_SPEED_DIV		0x0F

//ISP_SIE_CTRL 0x802c
#define SIE_STOP 0x02
#define SIE_START 0x01

//ISP_NLSC_MODE  8108
#define NLSC_R_CURVE_ADJ_EN	0x08
#define NLSC_G_CURVE_ADJ_EN	0x04
#define NLSC_B_CURVE_ADJ_EN	0x02
#define NLSC_STEP_MODE_32		0x00
#define NLSC_STEP_MODE_64		0x01

//ISP_MICRO_LSC_CTRL	81A4
#define ISP_MLSC_WRITE	0x08
#define ISP_MLSC_READ	0x04
#define ISP_MLSC_GRID_MODE 	0x03

//ISP_NLSC_GAIN_CTRL 81C3
#define NLSC_GAIN_CTRL_GOING	0x40
#define NLSC_GAIN_CTRL_CHANGE_STEP 0X01
#define NLSC_GAIN_CTRL_CHANGE_DIRECTLY 0X00
#define NLSC_GAIN_CTRL_START_LOAD 0x80
#define NLSC_GAIN_CTRL_STOP_LOAD	0x70

//ISP_AWB_WIN_ENABLE 825E
#define AWB_STATIS_BRIGHT_CONSTRAIN_EN		       0x01
#define AWB_STATIS_BG_CONSTRAIN_EN				0x02
#define AWB_STATIS_RG_CONSTRAIN_EN				0x04


//ISP_AWB_STATIS_CTRL	825F
#define ISP_AWB_STAT_START	0x80
#define ISP_AWB_STAT_STOP		0x40
#define ISP_AWB_STAT_GOING	0x20
#define ISP_AWB_ROUGH_EN		0x08
#define ISP_AWB_FINE_EN		0x04

//ISP_AWB_GAIN_CTRL	  8279
#define ISP_AWB_LOADGAIN				0x80
#define ISP_AWB_LOADGAIN_STOP		0x40
#define ISP_AWB_GOING					0x20
#define ISP_AWB_CHANGEGAIN_STEP		0x01

//ISP_AE_CTRL	8280
#define ISP_AE_STAT_START		0x80
#define ISP_AE_STAT_STOP		0x40
#define ISP_AE_ISGOING			0x20

//ISP_GAMMA_SEL	835E
#define ISP_GAMMA_ALL_CURVE_G 0x0
#define ISP_GAMMA_STANDALONE_CURVE 0x1

// ISP_NLC_SYNC 83f0
#define ISP_NLC_LOAD_PARAMETER 0x1

//ISP_NLC_SEL 83f1
#define ISP_NLC_ALL_CURVE_G 0x0
#define ISP_NLC_STANDALONE_CURVE 0x1

//ISP_YOFFSET	8427
#define ISP_BRIGHTNESS_INCREASE	0x00
#define ISP_BRIGHTNESS_DECREASE	0x80

//ISP_RDLOC_ENABLE		8506
#define ISP_CRNR_NR_EN		0x01
#define ISP_CRNR_NR_DIS		0x00


//#define ISP_NR_MMM_ENABLE				0x852A
#define ISP_NR_MMM_EN	0x01


//ISP_GLOC_ENABLE		8509
#define ISP_CRNR_GRGB_EN	0x01
#define ISP_CRNR_GRGB_DIS	0x00

//ISP_DDP_SEL 0x850B
#define ISP_CONTINUE_DDP_EN	0x01

//ISP_INTP_EDGE_SMOOTH		854D
#define ISP_CRNR_INTP_EN	0x02
#define ISP_CRNR_INTP_DIS	0x00
#define ISP_INTP_SMOOTH_EN		0x01
#define ISP_INTP_SMOOTH_DIS	0x00

//ISP_LOC_ENABLE		872E
#define ISP_CRNR_EEH_EN	0x01
#define ISP_CRNR_EEH_DIS	0x00

//ISP_HDR_ENABLE 0x8800
#define ISP_HDR_MAX_EN 0x10
#define ISP_HALO_REDUCTION_EN 0x08
#define ISP_LOCAL_CONTRAST_EN 0x04
#define ISP_HDR_DOWNSAMPLE_LPF_EN 0x02
#define ISP_HDR_EN 0x01

//ISP_HDR_ADJ_CTRL 0x8806	
#define ISP_START_LOAD_HDRCONT_LEVLE 0x80
#define ISP_STOP_LOAD_HDRCONT_LEVLE 0x40
#define ISP_LOAD_CONT_LEVLE_STEP 0x02
#define ISP_LOAD_HDR_LEVLE_STEP 0x01

//ISP_YMODE	0x8426
#define YCBCR_MODE  0x01
#define YUV_MODE  0x00


//ISP_NOISE_RD_CTRL FD3C
#define ISP_NOISERD_MF_RED	0x40
#define ISP_NOISERD_MF_GREEN	0x20
#define ISP_NOISERD_MF_BLUE	0x10
#define ISP_NOISERD_MODE_MEDIAN	0x03
#define ISP_NOISERD_MODE2	0x02
#define ISP_NOISERD_MODE1	0x01

//ISP_FRAME_IDLE	 FD74
#define ISP_FIDLE_AF_EE	0x10
#define ISP_FIDLE_BET_ZEE	0x08
#define ISP_FIDLE_BET_INTPT		0x04
#define ISP_FIDLE_BET_DPCINTP	0x02
#define ISP_FIDLE_BEF_DPC	0x01

//ISP_HUE_SIN_X	841C
#define ISP_HUE_SIN_X_SIGN		0x80
#define ISP_HUE_SIN_X_PARAM	0x7F

//ISP_HUE_COS_X	841D
#define ISP_HUE_COS_X_SIGN		0x80
#define ISP_HUE_COS_X_PARAM	0x7F

//ISP_HUE_SIN_Y	841E
#define ISP_HUE_SIN_Y_SIGN		0x80
#define ISP_HUE_SIN_Y_PARAM	0x7F

//ISP_HUE_COS_Y	841F
#define ISP_HUE_COS_Y_SIGN		0x80
#define ISP_HUE_COS_Y_PARAM	0x7F

//ISP_AF_CTRL
#define ISP_AF_STAT_START	0x80
#define ISP_AF_STAT_STOP	0x40
#define ISP_AF_STAT_GOING	0x01

//ISP_INT_EN1 && ISP_INT_FLAG1	8008 & 8009
#define ISP_DATA_END_INT    0x08
#define ISP_DATA_START_INT  0x04
#define ISP_FRM_END_INT     0x02
#define ISP_BRIGHT_COMP_INT	0x01
#define ISP_INT1            0x01
#define ISP_INT0            0x00

//CFG_SAFE_TIME_1 	0xFD81
#define CFG_SAFE_LINE_WAKE_EN		0x80

// PG_DELINK_CTRL		0xFF9E
#define PAD_DELINK_PULL_UP		0x80
#define PAD_DELINK_PULL_DOWN	0x40
#define PAD_DELINK_OUT_DRIVE_HIGH	0x20
#define PAD_DELINK_OUT 	0x10
#define PAD_DELINK_SPI_CS		0x08
#define DELINK_LOW_ACTIVE		0x04
#define DELINK_HIGH_ACTIVE		0x00
#define FORCE_DELINK			0x02
#define DELINK_ENABLE			0x01

//JENC_CTL
#define JPEG_STILL_SEL               0X02
#define JPEG_VIDEO_SEL              0X00
#define JPEG_SET				0X01
//#define M420_ENABLE				0X04
#define JPEG_CLEAR				0X00
#define JPEG_MODULE_RST          0X80

//#define M420_FORMAT_EN		0X04
//#define M420_FORMAT_DIS		0X00


//JENC_CONFIGURE
#define JPEG_CFG_CMPONT_NUM3     0X80
#define JPEG_CFG_QT_NUM2               0X10
#define JPEG_CFG_HFT_EXIST             0X08
#define JPEG_CFG_MRK_RESTRT_EN   0X04
#define JPEG_CFG_CLR_COMP_NUM3  0X02
#define JPEG_QT_READ                   0X02
#define JPEG_QT_WRITE                 0X01

//MIC mode control 0xFF88h
#define 	DMIC2UAC_EN		0x01
#define 	ADC2UAC_EN		0x02
#define 	ADC2CODEC_EN		0x04

// PTS_CTL		FE50h
#define PTS_FIX_INTERVAL_MODE_EN	0x01

// BOOTCTL        FE80
#define	BOOT_FROM_ROM			0x80
#define	BOOT_FROM_DL			0x40

//SYSCTL            FE81
#define   ROM_MODE                  0X00
#define   ICE_MODE                    0X01
#define   EA_MODE                      0X01
#define   ROM_MODE2                0X11

//CLKCTL            FE82
//-#define   MCU_CLK_CHNG     	0x01		//-Jimmy.20120321.unused in new MCU CLK Method
#define	ISP_CLK_CHNG		0x02
#define 	JPEG_CLK_CHNG		0x04

//CLKDIV            FE83
//ISP
#define  ISP_CLKDIV_MASK     0x30
#define  ISP_CLK_DIV1               0X00
#define  ISP_CLK_DIV2               0X10
#define  ISP_CLK_DIV4               0X20
#define  ISP_CLK_DIV8               0X30
//-Jimmy.20120321.unused in new MCU CLK Method
//-MCU
//-#define  MCU_CLKDIV_MASK    0x03
//-#define  MCU_CLK_DIV1               0X00
//-#define  MCU_CLK_DIV2               0X01
//-#define  MCU_CLK_DIV4               0X02
//-#define  MCU_CLK_DIV32               0X03

//DHE
#define DHE_SOURCE_YUV420 	0x0
#define DHE_SOURCE_YUV422		0x1

/*
//JPEG
#define  JPEG_CLKDIV_MASK   	    0xC0
#define  JPEG_CLK_DIV1               0X00
#define  JPEG_CLK_DIV2               0X01
#define  JPEG_CLK_DIV4               0X02
#define  JPEG_CLK_DIV8               0X03
*/

#define JPEG_CLKDIV_MASK			0xC0
#define 	JPEG_CLOCK_60M			0x00
#define 	JPEG_CLOCK_30M			0x01
#define 	JPEG_CLOCK_15M			0x02
#define 	JPEG_CLOCK_7M5			0x03

#define JPEG_CLOCK_96M				0x04
#define JPEG_CLOCK_48M				0x05
#define JPEG_CLOCK_24M				0x06
#define JPEG_CLOCK_12M				0x07

#define	JPEG_CLOCK_120M			0x08
//#define	JPEG_CLOCK_60M			0x09
//#define	JPEG_CLOCK_30M			0x0A
//#define	JPEG_CLOCK_15M			0x0B

#define	JPEG_CLOCK_160M			0x0C
#define	JPEG_CLOCK_80M			0x0D
#define	JPEG_CLOCK_40M			0x0E
#define	JPEG_CLOCK_20M			0x0F

//CLKSEL          FE84
#define MCU_CLKSEL_MASK		0x03
#define MCU_CLKSEL_60M                     0x00
#define MCU_CLKSEL_80M                     0x01
#define MCU_CLKSEL_96M                     0x02

#define ISP_CLKSEL_MASK		0x30
#define ISP_CLKSEL_60M                     0x00
#define ISP_CLKSEL_80M                     0x10
#define ISP_CLKSEL_96M                     0x20
#ifdef _TEST_CHIP_
#define ISP_CLKSEL_120M                   0x30
#endif

#define JPEG_CLKSEL_MASK		0xC0
/*
//#define JPEG_CLKSEL_60M                     0x00
//#define JPEG_CLKSEL_80M                     0x40
//#define JPEG_CLKSEL_96M                     0x80
// bit 7:6---- CLKSEL, bit 5:0---- always zero
#ifdef _TEST_CHIP_
#define JPEG_CLOCK_60M                     0x00
#define JPEG_CLOCK_96M                     0x40
#define JPEG_CLOCK_120M                   0x80
#ifdef _IC_CODE_
#define JPEG_CLOCK_160M                   0x0C
#else
#define JPEG_CLOCK_80M                      0x04
#endif
#else
 #define 	JPEG_CLOCK_60M			0x00
 #define 	JPEG_CLOCK_30M			0x01
 #define 	JPEG_CLOCK_15M			0x02
 #define 	JPEG_CLOCK_7M5			0x03
#define	JPEG_CLOCK_96M			0x04
#define	JPEG_CLOCK_48M			0x05
#define	JPEG_CLOCK_24M			0x06
#define	JPEG_CLOCK_12M			0x07
#define	JPEG_CLOCK_120M			0x08
//#define	JPEG_CLOCK_60M			0x09
//#define	JPEG_CLOCK_30M			0x0A
//#define	JPEG_CLOCK_15M			0x0B
#define	JPEG_CLOCK_160M			0x0C
#define	JPEG_CLOCK_80M			0x0D
#define	JPEG_CLOCK_40M			0x0E
#define	JPEG_CLOCK_20M			0x0F

#endif
*/
//-Jimmy.20120321.unused in new MCU CLK Method
//-//bit7:4---always zero; bit3:2--- CLKSEL; bit1:0---CLKDIV
//-#define	MCU_CLOCK_60M			0x00
//-#define	MCU_CLOCK_30M			0x01
//-#define	MCU_CLOCK_15M			0x02	// default value
//-#define	MCU_CLOCK_1M875		0x03
//-#define	MCU_CLOCK_80M			0x04
//-#define	MCU_CLOCK_40M			0x05
//-#define	MCU_CLOCK_20M			0x06
//-#define	MCU_CLOCK_2M5			0x07

//-#define	MCU_CLOCK_96M			0x08
//-#define	MCU_CLOCK_48M			0x09
//-#define	MCU_CLOCK_24M			0x0A
//-#define	MCU_CLOCK_3M				0x0B

#define	MCU_CLK_MASK				0x07
#define	MCU_CLOCK_120M			0x00
#define	MCU_CLOCK_60M			0x01
#define	MCU_CLOCK_30M			0x02
#define	MCU_CLOCK_15M			0x03
#define	MCU_CLOCK_7M5			0x04
#define	MCU_CLOCK_3M75			0x05
#define	MCU_CLOCK_1M875			0x06

#define	ISP_CLOCK_60M			0x00
#define	ISP_CLOCK_30M			0x01
#define	ISP_CLOCK_15M			0x02
#define	ISP_CLOCK_7M5			0x03
#define	ISP_CLOCK_80M			0x04
#define	ISP_CLOCK_40M			0x05
#define	ISP_CLOCK_20M			0x06
#define	ISP_CLOCK_10M			0x07
#define	ISP_CLOCK_96M			0x08
#define	ISP_CLOCK_48M			0x09
#define	ISP_CLOCK_24M			0x0A
#define	ISP_CLOCK_12M			0x0B
#define    ISP_CLOCK_72M			0x0C
#define    ISP_CLOCK_36M			0x0D
#define    ISP_CLOCK_18M			0x0E
#define    ISP_CLOCK_9M			0x0F

//7M5, 9M, 10M,12M,
//15M,18M,20M,24M,
//30M,36M,40M,48M,
//60M, 72M,80M,96M










//WTDGCTL  FE85
#define  WDG2RST                          0X80
#define  WDG_RST_REG                 0X40

#define  WDG_TIME_1S                (0<<2)
#define  WDG_TIME_2S                (1<<2)
#define  WDG_TIME_4S                (2<<2)
#define  WDG_TIME_8S                (3<<2)
#define  WDG_CNT_CLR                 0X02
#define  WDG_EN                            0X01

//WTDGINTCTL  FE86
#define WDG_INT_PENDING          0X10
#define WDG_INT_ENABLE            0X01

//MODEDCT             FE87
//#define RSTB_MODE_DETECT     0X80
//#define MODE_OUT_VLD             0X40
//#define T_RSTB_VTH1H_MSK     0X30
//#define MODE_MSK                      0X0F

// HW TMR0 INT EN //FE87
#define I2C_DEV_CONFLICT_INT_EN	0X02
#define HW_TMR0_INT_EN			0X01

// HW TMR0 INT FLAG //FE88
#define I2C_DEV_CONFLICT_INT_FLAG	0X02
#define HW_TMR0_INT_FLAG			0X01

//T_RSTB_VTH1L   FF88
//T_TH1                   FF89
//T_TH2                   FF8A
//T_TH3                   FF8B
//T_VDETECT	    0xFE8C

//SSC_PLL_CTL                   0xFE8D

//SSC_DIV_EXT_F		0xFE8E
//SSC_DIV_N_0			0xFE8F
//SSC_DIV_N_1			0xFE90
//SSC_CTL1			0xFE91
//SSC_CTL2			0xFE92

//MCU interface control regs
//SR_MCU_CTL			0xFE93
//DRV_MCU_CTL                       0xFE94
//MCU_OE_CTL			0xFE95

// BLK_CLK_EN			FE96
#define ISP_CCS_CLK_EN		0x01
#define JPEG_CLK_EN			0x02
#define SIE_PWRDWN_EN         0x04
#define I2C_CLK_EN                  0x08
#define SPI_CRC_CLK_EN         0x10
#define CACHE_I2C_CLK_EN      0x20
#ifdef _MIPI_EXIST_
#define MIPI_CLK_EN         0x40
#endif
//MEMBIST	                           0xFE9C

//FE98 RS232_UCTL
#define RS232_WRITE 				0x01
#define RS232_BUSY  				0x04

//USBCTL 				FE00
#define	USB_GO_SUSPEND			0x10
#define	USB_RMTWKUP_EN			0x08
#define	USB_RMTWKUP_FALL_EDGE	0x04
#define	USB_CONNECT				0x01
#define	USB_DISCONNECT			0x00


//USBSTAT                                 0XFE01
#define	VBUS_STAT				0x02
#define	HIGH_SPEED				0x01

//EPTOP_IRQSTAT                    0XFE02
//EPTOP_IRQEN                        0XFE04
#define	SETUP_PKT_INT		0x80
#define	EPC_INT				0x08
#define   EPB_INT				0x04
#define	EPA_INT				0x02
#define	EP0_INT				0x01
#define	EPE_INT				0x20
#ifdef _HID_BTN_
#define	EPD_INT				0x10
#endif
//USBSYS_IQRSTAT                 0XFE03
//USBSYS_IRQEN                     0XFE05
#define	PORT_RESET_INT			0x10
#define	RESUME_INT				0x08
#define	SUSPEND_INT				0x04
#define	CTL_STS_INT				0x02
#define	SOF_INT					0x01

//FORCE_TOGGLE                     0XFE10
#define FORCE_EPA_DATA0	0x94
// DEVADDR                                0XFE06

//UTMI_STA                               0XFE07
//PHY_TUNE                              0XFE08
//UTMI_TUNE                             0XFE09
//SLBSEED	                            0XFE0A
//SLBBIST                                  0XFE0B
//VCONTROL                              0XFE0C
//VSTATUSIN                             0XFE0D
//VLOADM                                  0XFE0E
//VSTATUSOUT                          0XFE0F
//FORCE_TOGGLE                     0XFE10
//UTMI_TST                                0XFE11

///////////////////////////////////////////
//soft interrupt registers
///////////////////////////////////////////
//SYS_SOFTIRQ_EN                        0XFE1C
//SYS_SOFTIRQ_TRIGGER	        0XFE1D
//SYS_SOFTIRQ_STS                      0XFE1E
//#define SF_IRQ_INIT_SENSOR    	    0X01
//#define SF_IRQ_INIT_VDPOINTERS    0X02
#define SF_IRQ_LOAD_EEPROM          0X04
#define PATCH_UVC_PP				0x01
#define PATCH_IQ_TABLE				0x02
#define SF_IRQ_INIT_ISP				0X08
//#define SF_IRQ_LOAD_VDINITSYSCFG     0x08
#define SF_IRQ_SWITCH_2_CACHEMODE 0X10
#define SF_IRQ_INIT_VIDEO_PARAM	0X20
#ifdef _UBIST_TEST_
#define SF_IRQ_READ_SENSORID		0X40
#endif
#define SF_IRQ_INIT_ISP_HW		0x80
#ifdef _LENOVO_IDEA_EYE_
#define SF_IRQ_MTD 0x40
#endif




//#define  SF_IRQ_

// SIE_FCNT_EN			0xFE23
#define	FW_SIE_FCNT_EN		0x01
#define 	SIE_FCNT_EN_FLAG		0x80

// FCNT_SEC_POWER		0xFE2A
#define	FCNT_INT_INTERVAL_1		0
#define	FCNT_INT_INTERVAL_2		1
#define	FCNT_INT_INTERVAL_4		2
#define	FCNT_INT_INTERVAL_8		3
#define	FCNT_INT_INTERVAL_16		4
#define	FCNT_INT_INTERVAL_32		5
#define	FCNT_INT_INTERVAL_64		6
#define	FCNT_INT_INTERVAL_128		7

// SIE_FRAME_DROP		0xFE29
#define	SIE_FRAME_DROP_EN		0x01

// FCNT_INT				0xFE2B
#define	FCNT_INT_EN			0x80
#define	FCNT_INT_FLAG			0x01

//EP0
// EP0_CFG                                 0XFE30
#define EP0_SFR_EN			0x20
#define EP0_DIR_IN                   0X10
#define EP0_DIR_OUT                0X00

// EP0_CTL                                  0XFE31
#define	EP0_FIFO_FLUSH				0x20
#define	EP0_STALL_EP				         0x10
#define	EP0_NAK_OUT					0x08
#define	EP0_AUTO_VALID				0x04
#define	EP0_CTL_HSK					0x02
#define	EP0_FIFO_VALID				0x01

// EP0_STAT                                0XFE32
#define	EP0_STS_END				0x80
//
//
//
#define	EP0_FIFO_EMPTY			0x02
#define	EP0_FIFO_FULL			0x01

// EP0_IRQEN                              0XFE33
// EP0_IRQSTAT                          0XFE34
#define EP0_IRQ_DATA_RECV   0X08
#define EP0_IRQ_DATA_TRAN   0X04
#define EP0_IRQ_OUT_TOKEN   0X02
#define EP0_IRQ_IN_TOKEN   	   0X01

// EP0_MAXPKT                           0XFE35
// EP0_DAT                                   0XFE36
// EP0_BC                                     0XFE37

//EPA

// EPA_CFG                                  0XFE38
#define	EPA_EN					0x80
#define   EPA_TYPE_BULK			0x40
#define   EPA_TYPE_ISO			0x00
#define 	EPA_BULK_MODE_BURST	0x20
#define	EPA_JPEG_MODE_BURST	0x10	//used for burst handle mjpeg data
#define	EPA_NUMBER1                 0x01

// EPA_CTL                                   0XFE39
#define	EPA_FIFO_FLUSH				0x20
#define    EPA_STALL_EP				         0x10
#define    EPA_FIFO_VALID                                0X01
#define    EPA_PACKET_EN                                 0X02

// EPA_STAT                                 0XFE3A
#define    EPA_FIFO_EMPTY				0x02
#define	EPA_FIFO_FULL				0x01

// EPA_IRQEN                              0XFE3B
// EPA_IRQSTAT                          0XFE3C
#define EPA_IRQ_DATA_TRAN                        0X04
#define EPA_IRQ_IN_TOKEN                           0X01

// EPA_MAXPKT0                         0XFE3D
// EPA_MAXPKT1                         0XFE3E
// EPA_DAT                                   0XFE3F
// EPA_BC0                                   0XFE40
// EPA_BC1                                   0XFE41
// EPB_CFG
#define	EPB_EN					0x80
#define    EPB_TYPE_BULK                                    0x40
#define    EPB_TYPE_ISO                                       0x00
#define    EPB_NUMBER1                            0x02

// EPB_CTL
#define	EPB_FIFO_FLUSH				0x20
#define    EPB_STALL_EP				         0x10
#define    EPB_FIFO_VALID                                0X01

// EPB_STAT
#define    EPB_FIFO_EMPTY				0x02
#define	EPB_FIFO_FULL				0x01

// EPB_IRQEN
// EPB_IRQSTAT
#define EPB_IRQ_DATA_TRAN                        0X04
#define EPB_IRQ_IN_TOKEN                           0X01

// EPB_FLOW_CTL
#define EPB_TRANSFER_EN				0x01

//EPC
// EPC_CFG                                 0XFE45

#define	EPC_EN					0x80
#define    EPC_NUMBER3                            0x03

// EPC_CTL                                  0XFE46
#define	EPC_FIFO_FLUSH				0x20
#define	EPC_STALL_EP				         0x10
#define	EPC_FIFO_VALID				0x01

// EPC_STAT                                0XFE47
//
//
//
#define	FIFO_EMPTY				0x02
#define	FIFO_FULL				0x01

// EPC_IRQEN                              0XFE48
// EPC_IRQSTAT                          0XFE49
#define EPC_IRQ_DATA_TRAN                        0X04
#define EPC_IRQ_IN_TOKEN                           0X01

// EPC_MAXPKT                           0XFE4A
// EPC_DAT                                   0XFE4B
// EPC_BC                                     0XFE4C

// LPM_CFG 0XFE4D
#define	FORCE_FW_REMOTE_WAKEUP					0x08
#define	CFG_EP0_L1_EN								0x04
#define	EP0_VALID_REMOTE_WAKEUP					0x02
#define	EPC_VALID_REMOTE_WAKEUP					0x01

#define	EPE_EN					0x80
#define EPE_IRQ_IN_TOKEN      0X01
#define	EPE_FIFO_FLUSH			0x20
#define EPE_PACKET_EN         	0X02
#define	EPE_STALL_EP			0x10
#define EPAE_TRANSFER_MODE 	0x3

#define EPAE_DWORD_NUM0		0xFF50
#define EPAE_DWORD_NUM1		0xFF51

#define EPE_NUMBER1			0x5


#define	SETUP_PKT_bmREQUST_TYPE	        0xE060
#define	SETUP_PKT_bREQUEST			0xE061
#define	SETUP_PKT_wVALUE_L			0xE062
#define	SETUP_PKT_wVALUE_H			0xE063
#define	SETUP_PKT_wINDEX_L			0xE064
#define	SETUP_PKT_wINDEX_H			0xE065
#define	SETUP_PKT_wLEN_L			0xE066
#define	SETUP_PKT_wLEN_H			0xE067

#ifdef _USB2_LPM_ // JQG_add_2010_0415_
//USB_LPM 0XFE56
#define	LPM_NAK			0x20
#define	ISP_LPM_EN			0x10
#define	LPM_SUPPORT		0x04
#define	LPM_ACK			0x02
#define	LPM_OK				0x01
#endif

// EPD_CFG                                 0XFE58
#define	EPD_EN					0x80
#define    EPD_NUMBER4                            0x04

// EPD_CTL					0xFE59
#define	EPD_FIFO_FLUSH				0x20
#define	EPD_STALL_EP				         0x10
#define	EPD_FIFO_VALID				0x01

//PH_INFO                                   0XFE60
#define PH_INFO_STILL_IMG      0X20
#define PH_INFO_SCR_EN           0X08
#define PH_INFO_PTS_EN           0x04
#define PH_INFO_EOFHEADER    0x80

//PH_CTL
#define PH_CTL_0Byte_WHEN_EMPTY       0x00
#define PH_CTL_PH_WHEN_EMPTY            0x80



//CRC_CTL                       0xFE70
#define CRC_CTL_RUN         0X80
#define CRC_CTL_RST          0X40

//RF_WM_ST				0xFE7F
#define BULK_SIE_FIFO_FULL		0x08
#define BULK_SIE_FIFO_EMPTY	0x04

// SPI_OE_CTL                      0xFE9D
#define SPI_CS_SEL_GPIO4	0x10
#define SPI_CS_SEL                 0X08
#define SPI_SCK_SEL               0X04
#define SPI_MOSI_SEL             0X02
#define SPI_MISO_SEL             0X01
// GPIO_SPI_SLWR_CTL      0XFE9E
// GPIO_SPI_DRV_CTL         0XFE9F

//GPIO_PULL_CTL_L0                 0xFEA2
#define GPIO_PULL_UP          0x2
#define GPIO_PULL_DOWN   0x1
#define GPIO_NO_PULL         0x0


//GPIO_INT_CTL                0XFEa5
#define    GPIO_GLB_INT_EN             0x80
#define    GPIO_GLB_INT_STS           0x40

#define    FALLING_EDGE                  0
#define    RISING_EDGE                     1


//  SPI_CMD                          0xFEB0
//  SPI_ADDR0                      0xFEB1
//  SPI_ADDR1                      0xFEB2
//  SPI_ADDR2                      0xFEB3

// SPI_CA_NUM                    0xFEB4

// SPI_LENGTH0                   0xFEB5

// SPI_LENGTH1                   0xFEB6





// SPI_CA_NUMBER -- 0xFEB4
#define 	SPI_COMMAND_BIT_8	0xE0

#define    SPI_ADDRESS_BIT_16     0x0F
#define	SPI_ADDRESS_BIT_24	0x17
#define	SPI_ADDRESS_BIT_32	0x1F

// SPI_TRANSFER                  0xFEB7
#define	SPI_TRANSFER_START	0x80
#define	SPI_TRANSFER_END	0x40

#define    SPI_RD_OV_FLG                  0x20
#define    SPI_WR_OV_FLG                 0x10

#define    SPI_TRANS_MODE_MSK     0x07

#define	SPI_C_MODE0			0x00
#define	SPI_CA_MODE0			0x01
#define	SPI_CDO_MODE0			0x02
#define	SPI_CDI_MODE0			0x03
#define	SPI_CADO_MODE0		0x04
#define	SPI_CADI_MODE0		0x05
#define	SPI_POLLING_MODE0		0x06
#define	SPI_FAST_READ_MODE	0x07
#define	SPI_FAST_READ_DUAL_OUT_MODE		0x08
#define	SPI_FAST_READ_DUAL_INOUT_MODE	0x09


//  SPI_CONTROL                   0xFEB8
#define    SPI_PAD_SPI_SEL                      0x80
#define 	CS_POLARITY_HIGH		0x40
#define 	CS_POLARITY_LOW		         0x00
#define	DTO_MSB_FIRST			0x00
#define	DTO_LSB_FIRST			0x20
#define   SPI_AUTO_MODE               0x02
#define   SPI_MANUAL_MODE           0x00

#define	SPI_MODE0				0x00
#define	SPI_MODE1				0x04
#define	SPI_MODE2				0x08
#define	SPI_MODE3				0x0C


#define   SPI_RESET                                    0x01

//  SPI_TCTL                            0xFEB9
//  SPI_CLK_DIV0                  0xFEBa
//  SPI_CLK_DIV1                  0xFEBb
//  SPI_XCHGM_ADDR0        0xFEBc
//  SPI_XCHGM_ADDR1        0xFEBd
//  SPI_SIG                             0xFEBe

//  SPI_PULL_CTL                  0xFEBf










#define CCS_HCLK_30M   (CCS_CLK_SEL_60M|CCS_CLK_DIVIDER_2)
//#define CCS_HCLK_36M   (CCS_CLK_SEL_72M|CCS_CLK_DIVIDER_2)
#define CCS_HCLK_40M   (CCS_CLK_SEL_80M|CCS_CLK_DIVIDER_2)
#define CCS_HCLK_48M   (CCS_CLK_SEL_96M|CCS_CLK_DIVIDER_2)
#define CCS_HCLK_15M   (CCS_CLK_SEL_60M|CCS_CLK_DIVIDER_4)
//#define CCS_HCLK_18M   (CCS_CLK_SEL_72M|CCS_CLK_DIVIDER_4)
#define CCS_HCLK_20M   (CCS_CLK_SEL_80M|CCS_CLK_DIVIDER_4)
#define CCS_HCLK_24M   (CCS_CLK_SEL_96M|CCS_CLK_DIVIDER_4)
#define CCS_HCLK_7M5   (CCS_CLK_SEL_60M|CCS_CLK_DIVIDER_8)
//#define CCS_HCLK_9M     (CCS_CLK_SEL_72M|CCS_CLK_DIVIDER_8)
#define CCS_HCLK_10M   (CCS_CLK_SEL_80M|CCS_CLK_DIVIDER_8)
#define CCS_HCLK_12M   (CCS_CLK_SEL_96M|CCS_CLK_DIVIDER_8)
#define CCS_HCLK_3M75 (CCS_CLK_SEL_60M|CCS_CLK_DIVIDER_16)
//#define CCS_HCLK_4M5   (CCS_CLK_SEL_72M|CCS_CLK_DIVIDER_16)
#define CCS_HCLK_5M      (CCS_CLK_SEL_80M|CCS_CLK_DIVIDER_16)
#define CCS_HCLK_6M      (CCS_CLK_SEL_96M|CCS_CLK_DIVIDER_16)


// CCS_PIN_PULL_CTL1	0xFEC9
#define VSYNC_PULL_HIGH		0x80
#define VSYNC_PULL_DOWN		0x40
#define VSYNC_NO_PULL			0x00
#define HSYNC_PULL_HIGH		0x20
#define HSYNC_PULL_DOWN		0x10
#define HSYNC_NO_PULL			0x00
//#define OE_PULL_HIGH			0x08
//#define OE_PULL_DOWN			0x04
//#define OE_NO_PULL				0x00
#define DOUT_PULL_HIGH			0x02
#define DOUT_PULL_DOWN		0x01
#define DOUT_NO_PULL			0x00

// CCS_PIN_PULL_CTL2	0xFECA
//#define RESET_PULL_HIGH		0x80
//#define RESET_PULL_DOWN		0x40
//#define RESET_NO_PULL			0x00
#define PWDN_PULL_HIGH			0x20
#define PWDN_PULL_DOWN		0x10
#define PWDN_NO_PULL			0x00
#define SYSCLK_PULL_HIGH		0x08
#define SYSCLK_PULL_DOWN		0x04
#define SYSCLK_NO_PULL			0x00
#define PIXCLK_PULL_HIGH		0x02
#define PIXCLK_PULL_DOWN		0x01
#define PIXCLK_NO_PULL			0x00

// PIN_SHARE_SEL 0xFECB
#define PACKAGE_SEL_QFN40		0x00
#define PACKAGE_SEL_QFN32		0x03

//CCS_CONTROL 0xFECD
#define CCS_SSOR_MODE_8P2			0x30
#define CCS_SSOR_MODE_2P8			0x20
#define CCS_SSOR_MODE_8BIT		0x10
#define CCS_SSOR_MODE_NORM		0x00
#define CCS_PCLK_STOP_EN			0x08
#define CCS_PCLK_RISING_EDGE		0x04
#define CCS_PCLK_FALLING_EDGE		0x00
#define CCS_VSYNC_RISING_EDGE		0x02
#define CCS_VSYNC_FALLING_EDGE	0x00
#define CCS_HSYNC_RISING_EDGE		0x01
#define CCS_HSYNC_FALLING_EDGE	0x00

// CCS_TRANSFER		0xFED6
#define CCS_TRANSFER_EN			0x80
#define CCS_TRANSFER_DIS			0x00
#define CCS_TRANSFER_END			0x40
#define CCS_TRANSFER_RESET			0x20
//#define CCS_TRANSFER_FRAME_STOP	0x10
//#define CCS_GET_STILL_IMAGE_MODE		0x00
//#define CCS_REAL_TIME_IMAGE_MODE		0x01
#define CCS_JPG_VSYNC_GPIO7		0x08 // this is for 5820X
#define CCS_JPG_VSYNC_ORG			0x00 // this is for 5820X
#define CCS_JPG_BYPASS_EN			0x04 // this is for 5820X
#define CCS_JPG_BYPASS_MODE2		0x01 // this is for 5820X
#define CCS_JPG_BYPASS_MODE1		0x00 // this is for 5820X

//CCS_INT_EN	0xFED7
#define CCS_INT_DATA_START_EN		0x08
#define CCS_INT_FRM_END_EN		0X04
#define CCS_INT_FRM_START_EN		0x02
#define CCS_INT_BFR_ABORT_EN		0X01

// CCS_SUBSAMPLE_CTL	0xFEDA
#define CCS_SUBSAMPLE_HOR_EN	0x01
#define CCS_SUBSAMPLE_VER_EN	0x02

// I2C_TRANSFER		0xFEEC
#define I2C_TRANSFER_START			0x80
#define I2C_TRANSFER_END			0x40
#define I2C_TRANSFER_STOP			0x10
#define I2C_WRITE_MODE				0x00
#define I2C_READ_MODE				0x01
#define I2C_RANDOM_READ_MODE		0x02
#define I2C_SET_ADDRESS_MODE		0x03

// I2C_STATUS		0xFEED
#define I2C_TRANSFER_ERROR		0x80

// I2C_PULL_CTL		0xFEF0
#define SCL_PULL_HIGH		0x08
#define SCL_PULL_DOWN		0x04
#define SCL_NO_PULL			0x00
#define SDA_PULL_HIGH		0x02
#define SDA_PULL_DOWN		0x01
#define SDA_NO_PULL		0x00

// SV28_OCP_CTL		0xFF12
//#define SV28_1QUA_SEL   0x01
//#define SV28_D_SEL   0x02
#define SV28_OCP_DETECT_EN	  0x04
#define SV28_OCP_AUTO_PWROFF   0x08
#define SV28_18_OCPCTL_POWER_EN		0x01

// SV18_OCP_CTL		0xFF13
//#define SV18_1QUA_SEL   0x01
//#define SV18_D_SEL   0x02
#define SV18_OCP_DETECT_EN	  0x04
#define SV18_OCP_AUTO_PWROFF   0x08

// SV28_PWR_CTL		0xFF17
#define SV28_A_SEL		0x01
#define SV28_B_SEL		0x02
#define SV28_C_SEL		0x04
#define SV28_D_SEL		0x08

// SV18_PWR_CTL		0xFF18
#define SV18_A_SEL		0x01
#define SV18_B_SEL		0x02
#define SV18_C_SEL		0x04
#define SV18_D_SEL		0x08

//AD filter registers	0xFF30
#define AD_DATA_WIDTH_8	0x40
#define AD_DATA_WIDTH_16	0x00
#define AD_DATA_WIDTH_24	0x80
#define ADF_FULL_SPEED		0x20
#define ADF_CLK_EN			0x04
#define ADF_CHN_SWITCH		0x10
#define ADF_48K_CLK			0x00
#define ADF_24K_CLK			0x01
#define ADF_16K_CLK			0x02
#define ADF_96K_CLK			0x03
#define ADF_44K1_CLK		0x08
#define ADF_22K05_CLK		0x09
#define ADF_11K025_CLK		0x0A
#define ADF_32K_CLK			0x0B

#define ADF_SAMPLERATE_MASK	0x0B

//ADF_CTRL0     0XFF33
#define MONO_CHANNEL_SEL		0X00
#define RIGHT_CHANNEL_SEL		0X01
#define LEFT_CHANNEL_SEL		0X02
#define BOTH_CHANNEL_SEL		0X03

// ADF_CTRL1			0xFF34
#define LEFT_CHANNEL_MUTE		0x80
#define RIGHT_CHANNEL_MUTE	0x40
#define HIGH_PASS_FILTER_EN	0x20
#define AD_DMIC_SEL				0x10
#define DMIC_LEFT_EDGE_SEL		0x08
#define DMIC_RIGHT_EDGE_SEL	0x04
#define DMIC_LEFT_FALLING_EDGE_SEL		0x08
#define DMIC_LEFT_RISING_EDGE_SEL			0x00
#define DMIC_RIGHT_FALLING_EDGE_SEL		0x04
#define DMIC_RIGHT_RISING_EDGE_SEL		0x00
#define AD_UPDATA_EN			0x02

//ADF_CTRL2  		0xFF35
#define COPY_LEFT_TO_RIGHT	0x08
#define COPY_RIGHT_TO_LEFT	0x0C
#define NO_CHANNEL_COPY		0x00

// DMIC_CTL			0xFF3B
#define DMIC_DRIVE_4MA		0x00
#define DMIC_DRIVE_8MA		0x01
#define DMIC_DRIVE_12MA		0x02
#define DMIC_SR_QUICK		0x00
#define DMIC_SR_SLOW		0x0c
#define DMIC_CLK_NO_PULL	0x00
#define DMIC_CLK_PULL_DOWN	0x10
#define DMIC_CLK_PULL_UP		0x20
#define DMIC_DATA_NO_PULL		0x00
#define DMIC_DATA_PULL_DOWN	0x40
#define DMIC_DATA_PULL_UP		0x80


// DMIC_PAD_INT_STS DMIC_PAD_INT_EN	0xFF3E
#define GPIO_MICDATA_FEDGE_INT	0x01
#define GPIO_MICCLK_FEDGE_INT	0x02
#define GPIO_MICDATA_REDGE_INT	0x04
#define GPIO_MICCLK_REDGE_INT	0x08
#define GPIO_MIC_REDGE_INT	0x0C
#define GPIO_MIC_FEDGE_INT	0X03

#define GPIO_MIC_DATA		0x00
#define GPIO_MIC_CLK		0x01


// AL_CACHE_CFG		0xFFB7
#define	CACHE_EN				0x80
#define 	MCU_RESET_ONLY		0x40
#define 	AUTO_CACHE_MODE		0x20
#define 	SPI_CACHE_MODE		0x10
#define 	I2C_CACHE_MODE		0x08
#define 	I2C_CACHE_BYTE_ADDR	0x04
#define 	I2C_CACHE_WORD_ADDR	0x00

// CACHE_CFG	0xFF40
#define 	CACHE_RESET		0X01

// CACHE_CTL	0xFF41
#define	CACHE_SPI_CS		0x80

// SYSCTL		0xFF90
#define RC400K_EN			0x80

// PG_EN		0xFF91
#define ISO_EN				0x01
#define DV12S_POWOFF_EN	0x02
#define RESUME_RST_EN		0x04
#define PLL_TIMECNT_EN		0x08
#define PLL_SW_ON_EN		0x10
#define APHY_HANDLER_EN	0x20

// PG_FSM_RST		0xFF92
#define PG_STATE_RST		0x01
#define SUSPEND_FLAG		0x02
#define PG_FPGA_DATARAM_REVERT	0x04

// PG_GPIO_AL_INT_STS PG_GPIO_AL_INT_EN
#define GPIO_AL0_FEDGE_INT	0x01
#define GPIO_AL1_FEDGE_INT	0x02
#define GPIO_AL0_REDGE_INT	0x04
#define GPIO_AL1_REDGE_INT	0x08
#define GPIO_AL_REDGE_INT	0x0C
#define GPIO_AL_FEDGE_INT	0X03

#define GPIO_AL0		0x00
#define GPIO_AL1		0x01

#define GPIO_SEL_NORMAL	0x00
#define GPIO_SEL_AL			0x01
#define GPIO_SEL_DMIC		0x02

// PG_GPIO_AL_CTRL0		0xFF9C
#define GPIO_AL0_OUTPUT			0x01
#define GPIO_AL1_OUTPUT			0x02
#define GPIO_AL0_DRIVING_HIGH		0x04
#define GPIO_AL1_DRIVING_HIGH		0x08

// PG_GPIO_AL_CTRL1		0xFF9D
#define PG_GPIO_AL_DRIVING_4MA	0x00
#define PG_GPIO_AL_DRIVING_8MA	0x10
#define PG_GPIO_AL_DRIVING_12MA	0x20
#define PG_GPIO_AL_SLEWRATE_FAST	0x00
#define PG_GPIO_AL_SLEWRATE_SLOW	0x40
#define GPIO_AL0_PULL_DOWN		0x01

// REG_BANDGAP				0xFFA0
#define REG_BG5V_1V14			0x00
#define REG_BG5V_1V16			0x08
#define REG_BG5V_1V20			0x10
#define REG_BG5V_1V24			0x18
#define REG_VBG_SEL_1V15		0x00
#define REG_VBG_SEL_1V17		0x01
#define REG_VBG_SEL_1V19		0x02
#define REG_VBG_SEL_1V21		0x03
#define REG_VBG_SEL_1V23		0x04
#define REG_VBG_SEL_1V25		0x05
#define REG_VBG_SEL_1V27		0x06
#define REG_VBG_SEL_1V29		0x07

// REG_TUNE				0xFFA1
#define REG_TUNED33_3V1		0x00
#define REG_TUNED33_3V2		0x20
#define REG_TUNED33_3V3		0x40
#define REG_TUNED33_3V4		0x60
#define REG_TUNED12_1V00		0x00
#define REG_TUNED12_1V05		0x04
#define REG_TUNED12_1V10		0x08
#define REG_TUNED12_1V15		0x0C
#define REG_TUNED12_1V20		0x10
#define REG_TUNED12_1V25		0x14
#define REG_TUNED12_1V30		0x18
#define REG_TUNED12_1V35		0x1C

// NON_CRYSTAL_CTL		0xFFA9
#define XTAL_AUTO_DETECT_RST	0x80
#define XTAL_ENABLE				0x04
#define XTAL_MODE_MASK			0x02
#define XTAL_MODE				0x02	// hemonel 2010-11-08: non-xtal bug because spec definition reverse 
#define NON_XTAL_MODE			0x00
#define XTAL_FREE_EN			0x01

// SPI_READ_MODE		0xFF41
#define SPI_NORMAL_READ	0x00
#define SPI_FAST_READ		0x01
#define SPI_FAST_READ_DUAL_OUT	0x02
#define SPI_FAST_READ_DUAL_INOUT	0x03

//SSC
#define SSC_8X_EN			0x08
#define SSC_RSTB			0x04
#define SSCPLL_RS_1			0x01
#define SSCPLL_RSTB			0x04
#define SSCPLL_POW			0x02

//EP0
sfr EP0_DATA_IN_SFR = 0x90;
//#define EP0_DATA_IN(x)     (EP0_DATA_IN_SFR=(x))
#define EP0_DATA_IN(x)     (XBYTE[EP0_DAT]=(x))
#define EP0_DATA_OUT()    (XBYTE[EP0_DAT])
//#define EP0_FFV_HSK()      (XBYTE[EP0_CTL]=EP0_CTL_HSK|EP0_FIFO_VALID)

#ifdef _EP0_LPM_L1_
//Jimmy.20130130.uDelay(2)-->LPM L1 hw need at least 60uS,in the 60uS,fw can not to force remote wakeup
//If fw to force remote wakeup, hw may not wakeup
#define EP0_FFV()		     { XBYTE[EP0_CTL]=EP0_FIFO_VALID;uDelay(2);XBYTE[LPM_CFG] |=FORCE_FW_REMOTE_WAKEUP;}
#define EP0_HSK()  	     {XBYTE[EP0_CTL]=EP0_CTL_HSK;uDelay(2);XBYTE[LPM_CFG] |=FORCE_FW_REMOTE_WAKEUP; }
#define EP0_FFV_HSK()		{EP0_FFV();EP0_HSK();}
#else
#define EP0_FFV_HSK()		{XBYTE[EP0_CTL] = EP0_FIFO_VALID; XBYTE[EP0_CTL]=EP0_CTL_HSK;}
#define EP0_FFV()		     (XBYTE[EP0_CTL]=EP0_FIFO_VALID)
#define EP0_HSK()  	     (XBYTE[EP0_CTL]=EP0_CTL_HSK)
#endif
#define EP0_STALL()            (XBYTE[EP0_CTL]=EP0_STALL_EP)
#define EP0_FFFLUSH()      (XBYTE[EP0_CTL]=EP0_FIFO_FLUSH)

#define EP0_TST_STALL()    (XBYTE[EP0_CTL]==EP0_STALL_EP)
#define EP0_TST_EMPTY() (XBYTE[EP0_BC]==0)
#ifdef _HID_BTN_
#define EPD_DATA_IN(x)     (XBYTE[EPD_DAT]=(x))
#define EPD_DATA_OUT()    (XBYTE[EPD_DAT])
#define EPD_FFV()		      (XBYTE[EPD_CTL]=EPD_FIFO_VALID)
#define EPD_STALL()             (XBYTE[EPD_CTL]=EPD_STALL_EP)
#define EPD_FFFLUSH()        (XBYTE[EPD_CTL]=EPD_FIFO_FLUSH)
#define EPD_TST_STALL()     (XBYTE[EPD_CTL]==EPD_STALL_EP)
#endif



//EPC
#define EPC_DATA_IN(x)     (XBYTE[EPC_DAT]=(x))
#define EPC_DATA_OUT()    (XBYTE[EPC_DAT])
#define EPC_FFV()		      (XBYTE[EPC_CTL]=EPC_FIFO_VALID)
#define EPC_STALL()             (XBYTE[EPC_CTL]=EPC_STALL_EP)
#define EPC_FFFLUSH()        (XBYTE[EPC_CTL]=EPC_FIFO_FLUSH)

#define EPC_TST_STALL()     (XBYTE[EPC_CTL]==EPC_STALL_EP)
#define EPC_TST_FFV()	       ((XBYTE[EPC_CTL]&EPC_FIFO_VALID)==EPC_FIFO_VALID)
#define EPC_TST_EMPTY()    (XBYTE[EPC_BC]==0X40)
//EPA
#define EPA_STALL()             (XBYTE[EPA_CTL]=EPA_STALL_EP)
#define EPA_FFFLUSH()       (XBYTE[EPA_CTL]=EPA_FIFO_FLUSH)
#define EPA_TST_STALL()     (XBYTE[EPA_CTL]==EPA_STALL_EP)


sfr EA_FLASH = 0x93;		// by hemonel: 2006-09-25

sfr HW_TM_CTRL	= 0x94;
#define  HW_TM_EN()	(HW_TM_CTRL |= 0x01)
#define HW_TM_DIS()	(HW_TM_CTRL &= ~0x01)
#define  HW_TM_INT_EN()	(HW_TM_CTRL |= 0x02)
#define HW_TM_INT_DIS()	(HW_TM_CTRL &= ~0x02)
#define CLR_TM_INT_STS()		(HW_TM_CTRL |= 0x04)

sbit EPFI = 0xD8^5;
sbit PFI = 0xD8^4;

#define CHANGE_TO_FLASH	      (EA_FLASH |= 0x01)
#define CHANGE_TO_RAM	      (EA_FLASH &= ~0x01)
#define CHECK_ACCESS_FLASH  ((EA_FLASH&0x01) == 0x01)

#define   SET_GPIO_L_OUTPUT(idx)   {XBYTE[GPIO_DIR_L]|=(0x01<<(idx)); }
#define   SET_GPIO_L_INPUT(idx)      {XBYTE[GPIO_DIR_L] &=(~(0x01<<(idx))); }
#define SetGPIODirW(wDir) {	XBYTE[GPIO_DIR_H] = ((wDir)/256);	XBYTE[GPIO_DIR_L]  = ((wDir)%256);}

#define   GPIO_L_TOGGLE(idx)                  {XBYTE[GPIO_L] ^= (0x01<<(idx));}
#define	GPIO_L_ALL_DRIVE_HIGH()    	 {XBYTE[GPIO_L] |= 0xFF; }
#define   GPIO_L_DRIVE_LOW(idx)                 {XBYTE[GPIO_L] &= (~(0x01<<(idx)));}
#define   GPIO_L_DRIVE_HIGH(idx)               {XBYTE[GPIO_L] |= (0x01<<(idx));}
#define   TST_GPIO_HIGH(idx)              ((XBYTE[GPIO_L]&(0x01<<(idx)))==(0x01<<(idx)))
#define   TST_GPIO_LOW(idx)             ((XBYTE[GPIO_L]&(0x01<<(idx)))== 0)
//////////////////////////////////////////////////////////////////
#define   GPIO_H_TOGGLE(idx)                 {XBYTE[GPIO_H] ^= (0x01<<(idx));}

#define   SET_GPIO_H_OUTPUT(idx)   {XBYTE[GPIO_DIR_H]|=(0x01<<(idx)); }
#define   SET_GPIO_H_INPUT(idx)      {XBYTE[GPIO_DIR_H] &=(~(0x01<<(idx))); }
#define   GPIO_H_DRIVE_LOW(idx)                {XBYTE[GPIO_H] &= (~(0x01<<(idx)));}
#define   GPIO_H_DRIVE_HIGH(idx)              {XBYTE[GPIO_H] |= (0x01<<(idx));}
#define   TST_GPIO_HIGH_H(idx)             ((XBYTE[GPIO_H]&(0x01<<(idx)))==(0x01<<(idx)))
#define   TST_GPIO_LOW_H(idx)              ((XBYTE[GPIO_H]&(0x01<<(idx)))==0)

//#define   TEST_BTN_PRESS()          TST_GPIO_LOW(HW_TRIGGER_GPIO) //  for LED on with GPIO low ,

// update SLB pass flag, for RTS5801 LED light High Active
//#define   SHOW_SLB_FAIL()			   {GPIO_H_DRIVE_LOW(BLINK_GPIO_IDX-8);}
//#define   SHOW_SLB_PASS()                       {GPIO_H_DRIVE_HIGH(BLINK_GPIO_IDX-8);}
//#define   SHOW_OLT_WORK()                      {GPIO_H_TOGGLE(BLINK_GPIO_IDX-8);}

#define   SHOW_SLB_FAIL()			   TURN_LED_OFF()
#define   SHOW_SLB_PASS()                      TURN_LED_ON()
#define   SHOW_OLT_WORK()                      (LED_TOGGLE())

#define  GPIO7_POWER_ON()                   GPIO_L_DRIVE_HIGH(7) 
#define  GPIO7_POWER_OFF()                 GPIO_L_DRIVE_LOW(7)


//Jimmy.20120607.LED use GPIO7
#ifdef _RS232_DEBUG_
#define TURN_LED_ON()
#define TURN_LED_OFF()
#define LED_TOGGLE()
#else
#ifdef _IC_CODE_
#define TURN_LED_OFF()			GPIO_L_DRIVE_LOW(7)
#define TURN_LED_ON()			GPIO_L_DRIVE_HIGH(7)
#else
#define TURN_LED_OFF()			GPIO_L_DRIVE_HIGH(7)
#define TURN_LED_ON()			GPIO_L_DRIVE_LOW(7)
#endif
#define	LED_TOGGLE()			(XBYTE[GPIO_L] ^= 0x80)
#endif
//#define TURN_LED_ON()			GPIO_H_DRIVE_HIGH(0)
//#define TURN_LED_OFF()			GPIO_H_DRIVE_LOW(0)
//#define LED_TOGGLE()				(XBYTE[GPIO_H] ^= 0x01)



#define DROP_FRAME_SET(n)                   (XBYTE[CCS_FRAME_VLD_CTL]=((n)-1)) //DROP n-1 frame per n frame

#define ADF_PARAM_UPDATE()		{XBYTE [ADF_CTRL1] &= (~AD_UPDATA_EN); XBYTE [ADF_CTRL1] |= AD_UPDATA_EN; XBYTE [ADF_CTRL1] &= (~AD_UPDATA_EN); }

//#define POWER_OFF_SV18()                (XBYTE[SV18_PWR_CTL] |= 0X01)
//#define POWER_ON_SV18()                  (XBYTE[SV18_PWR_CTL] &= (~0x01))
// hemonel 2009-12-28: RTS5803 SV18/SV28 LDO open time is 10ms from 0v to full at 4.7uf loading, so firmware delay 20ms
// hemonel 2010-01-06: LDO soft start
#define POWER_ON_SV18()               {XBYTE[SV18_PWR_CTL]   |= SV18_A_SEL; uDelay(9);XBYTE[SV18_PWR_CTL]   |= SV18_B_SEL; uDelay(9);XBYTE[SV18_PWR_CTL]   |= SV18_C_SEL; uDelay(5);XBYTE[SV18_PWR_CTL]   |= SV18_D_SEL;}// delay 0.4+0.4+0.2ms
#define POWER_OFF_SV18()                   ( XBYTE[SV18_PWR_CTL]  &= ~(SV18_A_SEL|SV18_B_SEL|SV18_C_SEL|SV18_D_SEL ))

//first power on 1/4 LDO ,then other 3/4 LDO  , LOW active
// hemonel 2009-11-14: add delay time from 100us to 1ms when power on SV28 for hamilton platform
//#define POWER_ON_SV28()                 {XBYTE[LDO28_PWRCTL]   &= ~LDO28_1QUA_SEL; uDelay(10);XBYTE[LDO28_PWRCTL]   &= ~LDO28_3QUA_SEL;}
//#define POWER_OFF_SV28()               ( XBYTE[LDO28_PWRCTL]  |= LDO28_3QUA_SEL|LDO28_1QUA_SEL )
// hemonel 2009-12-28: RTS5803 SV18/SV28 LDO open time is 10ms from 0v to full at 4.7uf loading, so firmware delay 20ms
// hemonel 2010-01-06: LDO soft start
// hemonel 2011-02-23: Restore to RTS5822A
#define POWER_ON_SV28()                  {XBYTE[SV28_PWR_CTL]   |= SV28_A_SEL; uDelay(9);XBYTE[SV28_PWR_CTL]   |= SV28_B_SEL; uDelay(9);XBYTE[SV28_PWR_CTL]   |= SV28_C_SEL; uDelay(5);XBYTE[SV28_PWR_CTL]   |=  SV28_D_SEL;}// delay 0.4+0.4+0.2ms
#define POWER_OFF_SV28()               ( XBYTE[SV28_PWR_CTL]  &= ~(SV28_A_SEL|SV28_B_SEL|SV28_C_SEL|SV28_D_SEL))
// hemonel 2010-11-12: SV28 PMOS D power on polarity is reverse for fix hardware bug
//#define POWER_ON_SV28()                  {XBYTE[SV28_PWR_CTL]   |= SV28_A_SEL; uDelay(6);XBYTE[SV28_PWR_CTL]   |= SV28_B_SEL; uDelay(6);XBYTE[SV28_PWR_CTL]   |= SV28_C_SEL; uDelay(3);XBYTE[SV28_PWR_CTL]   &=  ~SV28_D_SEL;}// delay 0.4+0.4+0.2ms
//#define POWER_OFF_SV28()               ( XBYTE[SV28_PWR_CTL]  = SV28_D_SEL)
#define POWER_ON_GPIO8()         	{XBYTE[GPIO_DIR_H]  |= 0x01;XBYTE[GPIO_H] |= 0x01;}
#define POWER_OFF_GPIO8()		{XBYTE[GPIO_DIR_H]  |= 0x01;XBYTE[GPIO_H] &= ~0x01;}

#define SPI_I2C_BUFFER_BASE	0xDE00

#define SPI_BUFFER_BASE_ADDR	SPI_I2C_BUFFER_BASE //0xde00
#define SPI_I2C_BUFFER_LEN 	64

#define SPI_SCRATCH_BUFFER_OFFSET  (SPI_I2C_BUFFER_LEN+0) //64+0
#define SPI_SCRATCH_BUFFER_BASE      (SPI_I2C_BUFFER_BASE+SPI_SCRATCH_BUFFER_OFFSET)//0xde00+64+0
#define SPI_SCRATCH_BUFFER_LEN         8

#define I2C_SCRATCH_BUFFER_OFFSET	       (SPI_I2C_BUFFER_LEN+SPI_SCRATCH_BUFFER_LEN+0)//64+8+0
#define I2C_SCRATCH_BUFFER_BASE_ADDR	       (SPI_I2C_BUFFER_BASE+I2C_SCRATCH_BUFFER_OFFSET)//0xde00+64+8+0

#define ROM_CODE_SIZE	0xEE00
#ifdef _PPWB_ADDR_0X10000_
#define PP_WB_ADDR_BASE		0x10000
#else
#define PP_WB_ADDR_BASE		0xF000
#endif

#define PP_WB_EXIST_ADDR_OFFSET	62

#define CACHE_CODE_CFG_BASE  0xEE00 // hemonel 2010-12-07:_SPI Cache configure must be placed at 0xEE00~0xEFFD, since 0xF000~0xFFFF is reserved for SPI SF PPWB



#endif
