/*
*******************************************************************************
*                                       Realtek's Camera Project
*
*                                       Layer: UVC
*                                       Module: UVC Descriptor
*                                   (c) Copyright 2011-2012, Realtek Corp.
*                                           All Rights Reserved
*
*                                                  V1.0
*
*
* File : CamUvcRtkExtDesc.c
*******************************************************************************
*/
/**
*******************************************************************************
  \file CamUvcRtkExtDesc.c
  \Realtek Extended Control Unit descriptor initialization.
  
  
  
Copyright (c) 2011-2012 by Realtek Incorporated.  All Rights Reserved.


*  \version 1.0
*  \date    8/5/2013

*******************************************************************************/

/*=============================================================================

                            EDIT HISTORY FOR MODULE

 when           who         what, where, why
 -----------    ---------   ---------------------------------------------------

==============================================================================*/

#include "CamUvcRtkExtDesc.h"

#if defined _RTK_EXTENDED_CTL_ || defined _LENOVO_IDEA_EYE_EXTENDEDUNIT_

extern  Desc_t  UVC_Descs[UVC_DESC_TOTAL_NUM];

//Add any other control support at here 

U8  code iRtkExtUnitStr[] = 
{
    0x20,                       // bLength
    USB_STRING_DESCRIPTOR_TYPE,         // bDescriptorType
    'R',
    'e',
    'a',
    'l',
    't',
    'e',
    'k',
    ' ',    
    'E',
    'x', 
    't', 
    'e', 
    'n',
    'd',  
    'e',
    'd',
    ' ',
    'C',
    'o', 
    'n', 
    't', 
    'r',
    'o',  
    'l',
    's',
    ' ',    
    'U',
    'n', 
    'i', 
    't', 
    ' '
};  

//          ===>Realtek Extended Controls Unit Descriptor<===
U8 code VideoRtkExtUnitDesc[]=
{
	DESC_SIZE_RTK_EXT_UNIT, //0x1c,   //bLength:                           
	CS_INTERFACE,   //bDescriptorType:                   
	VC_EXTENSION_UNIT,   //bDescriptorSubtype:                
	ENT_ID_RTK_EXTENDED_CTL_UNIT,   //bUnitID:  
	//{     
       //GUID {26B8105A-0713-4870-979D-DA79444BB68E}
	0x5A,
	0x10,
	0xB8,
	0x26,
    
	0x13,
	0x07,
    
	0x70,
	0x48,
    
	0x97,
	0x9D,
    
	0xDA,
	0x79,
	0x44,
	0x4B,
	0xB6,
	0x8E,
	 //}  
	0x00,     // control numbers ,will be determined by g_wRtkExtUnitCtlSel
	0x01,    //bNrInPins:                         
	SRC_ID_RTK_EXTENDED_CTL_UNIT, //baSourceID[1]:                    
	0x04,//bControlSize:
	0x07,
	0x00,
	0x38,
	0x00,
//     D00 = 1   ISP special effect -  Vendor-Specific (Optional)
//     D01 = 1   EV compensation -  Vendor-Specific (Optional)
//     D02 = 1   color temperature estimation -  Vendor-Specific (Optional)
//     D03 = 0   no -  Vendor-Specific (Optional)
//     D04 = 0   no -  Vendor-Specific (Optional)
//     D05 = 0   no -  Vendor-Specific (Optional)
//     D06 = 0   no -  Vendor-Specific (Optional)
//     D07 = 0   no -  Vendor-Specific (Optional)
//     D08 = 0   no -  Vendor-Specific (Optional)
//     D09 = 0   no -  Vendor-Specific (Optional)
//     D10 = 0   no -  Vendor-Specific (Optional)
//     D11 = 0   no -  Vendor-Specific (Optional)
//     D12 = 0   no -  Vendor-Specific (Optional)
//     D13 = 0   no -  Vendor-Specific (Optional)
//     D14 = 0   no -  Vendor-Specific (Optional)
//     D15 = 0   no -  Vendor-Specific (Optional)
//     D16 = 0   no -  Vendor-Specific (Optional)
//     D17 = 0   no -  Vendor-Specific (Optional)
//     D18 = 0   no -  Vendor-Specific (Optional)
//     D19 = 1   ROI -  Vendor-Specific (Optional)
//     D20 = 1   ROI status -  Vendor-Specific (Optional)
//     D21 = 1   Preview LED off -  Vendor-Specific (Optional)
//     D22 = 0   no -  Vendor-Specific (Optional)
//     D23 = 0   no -  Vendor-Specific (Optional)
    I_RTK_EXTENDED_CTL_UNIT  //iExtension:
};


U8 GetRealtekExtendedCtlNum(void)
{
	U8 byCtlNum,i;

	byCtlNum = 0;
	
	for(i=0;i<32;i++)
	{
		byCtlNum += (g_dwRtkExtUnitCtlSel>>i)&1;
	}

	return byCtlNum;
}

void UvcRealtekExtendedUnitDes(U8 byDescIdx)
{
	UVC_Descs[byDescIdx].Desc_Str = VideoRtkExtUnitDesc; 
	UVC_Descs[byDescIdx].type_idx = D_TYPE_EXTU_RTK;   

	g_wVCCSdescSize += DESC_SIZE_RTK_EXT_UNIT ;
}

U8	UvcRealtekExtendedUnitDesPro(U8 byDescIdx,U8 byDescOffset)
{
	switch(byDescOffset)
	{
		case 20:
			return GetRealtekExtendedCtlNum();
		case 24:
			return LONG2CHAR(g_dwRtkExtUnitCtlSel,0);
		case 25:
			return LONG2CHAR(g_dwRtkExtUnitCtlSel,1);	
		case 26:
			return LONG2CHAR(g_dwRtkExtUnitCtlSel,2);
		case 27:
			return LONG2CHAR(g_dwRtkExtUnitCtlSel,3);
		default:
			return UVC_Descs[byDescIdx].Desc_Str[byDescOffset];
	}
}
#endif
