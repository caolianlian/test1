#include "pc_cam.h"
#include "camutil.h"
#include "camreg.h"
#include "camuvc.h"
#include "camctl.h"
#include "CamIsp.h"
#include "CamProcess.h"
#include "camsensor.h"
#include "CAMI2C.h"
#include "camspi.h"
#include "camvdcfg.h"
#include "global_vars.h"
#include "camvdcmd.h"
#include "ISP_Lib.h"
#include "ISP_vars.h"
#include "CamUvcRtkExtCtl.h"

extern void InitCustomizedVars(void);

void Init_Sensor_GlobalVars()
{
	U8 i;

	// sensor type
	g_wSensor = 0x1001; //SENSOR_FT2_MODEL;  for usb debug

	// sensor power state
	g_bySensorIsOpen = SENSOR_CLOSED;
 	g_byFpsLast      = 0;

	g_bySnrPowerOnSeq =SNR_PWRCTL_SEQ_GPIO8| (SNR_PWRCTL_SEQ_SV18<<2)|(SNR_PWRCTL_SEQ_SV28<<4);
	g_bySnrPowerOffSeq = SNR_PWRCTL_SEQ_SV28| (SNR_PWRCTL_SEQ_SV18<<2)| (SNR_PWRCTL_SEQ_GPIO8 <<4);

	// sensor I2C
	g_SnrRegAccessProp.byI2CID = 0x00;
	g_SnrRegAccessProp.byMode_DataWidth= I2C_ACCESS_DATAWIDTH_1|I2C_ADDR_MODE_BA;
	g_SnrRegAccessProp.byI2CPageSelAddr = 0x00;

	// sensor format
	g_wSensorCurFormat = 0;
	g_bySensorSize = SENSOR_SIZE_UNKNOWN;
	g_wSensorSPFormat = 0;
	g_wSensorCurFormatWidth = 640;
	g_wSensorCurFormatHeight = 480;

	// sensor setting

	g_dwPclk = (U32)12*1000*1000;	// default set 12MHz
	g_wSensorHsyncWidth = 640;
#ifdef _MIPI_EXIST_
	g_byMipiDphyCtrl = 0;
#endif
#ifdef _RS0509_PREVIEW_ISSUE_
	g_wSOFCount = 0;
#endif
	// hemonel 2010-01-11: add fps 120, 60 ,24, delete fps 1
	g_byaFPSTable[0] =SENSOR_FPS_120;
	g_byaFPSTable[1] =SENSOR_FPS_60;
	g_byaFPSTable[2] =SENSOR_FPS_30;
	g_byaFPSTable[3] =SENSOR_FPS_25;

	g_byaFPSTable[4] =SENSOR_FPS_24;
	g_byaFPSTable[5] =SENSOR_FPS_23;
	g_byaFPSTable[6] =SENSOR_FPS_20;
	g_byaFPSTable[7] =SENSOR_FPS_15;

	g_byaFPSTable[8] =SENSOR_FPS_12;
	g_byaFPSTable[9] =SENSOR_FPS_11;
	g_byaFPSTable[10]  =SENSOR_FPS_10;
	g_byaFPSTable[11]   =SENSOR_FPS_9;

	g_byaFPSTable[12] =SENSOR_FPS_8;
	g_byaFPSTable[13] =SENSOR_FPS_7;
	g_byaFPSTable[14] =SENSOR_FPS_5;
	g_byaFPSTable[15] =SENSOR_FPS_3;

	g_VideoFormatFSYUY2.byFormatType = FORMAT_TYPE_YUY2;
	g_VideoFormatFSYUY2.byaVideoFrameTbl[0] = 1;  //	2 ,// first byte is frame number
	g_VideoFormatFSYUY2.byaVideoFrameTbl[1] = F_SEL_160_120; //	F_SEL_176_144,
	g_VideoFormatFSYUY2.byaVideoFrameTbl[2] = F_SEL_NONE; //	F_SEL_160_120,
	g_VideoFormatFSYUY2.byaVideoFrameTbl[3] = F_SEL_NONE;
	g_VideoFormatFSYUY2.byaVideoFrameTbl[4] = F_SEL_NONE;
	g_VideoFormatFSYUY2.byaVideoFrameTbl[5] = F_SEL_NONE;
	g_VideoFormatFSYUY2.byaVideoFrameTbl[6] = F_SEL_NONE;
	g_VideoFormatFSYUY2.byaVideoFrameTbl[7] = F_SEL_NONE;
	g_VideoFormatFSYUY2.byaVideoFrameTbl[8] = F_SEL_NONE;
	g_VideoFormatFSYUY2.byaVideoFrameTbl[9] = F_SEL_NONE;
	g_VideoFormatFSYUY2.byaVideoFrameTbl[10] = F_SEL_NONE;
	g_VideoFormatFSYUY2.byaVideoFrameTbl[11] = F_SEL_NONE;
	g_VideoFormatFSYUY2.byaVideoFrameTbl[12] = F_SEL_NONE;

	g_VideoFormatFSYUY2.byaStillFrameTbl[0] = 1;  //	2 ,// first byte is frame number
	g_VideoFormatFSYUY2.byaStillFrameTbl[1] = F_SEL_160_120; //	F_SEL_176_144,
	g_VideoFormatFSYUY2.byaStillFrameTbl[2] = F_SEL_NONE; //	F_SEL_160_120,
	g_VideoFormatFSYUY2.byaStillFrameTbl[3] = F_SEL_NONE;
	g_VideoFormatFSYUY2.byaStillFrameTbl[4] = F_SEL_NONE;
	g_VideoFormatFSYUY2.byaStillFrameTbl[5] = F_SEL_NONE;
	g_VideoFormatFSYUY2.byaStillFrameTbl[6] = F_SEL_NONE;
	g_VideoFormatFSYUY2.byaStillFrameTbl[7] = F_SEL_NONE;
	g_VideoFormatFSYUY2.byaStillFrameTbl[8] = F_SEL_NONE;
	g_VideoFormatFSYUY2.byaStillFrameTbl[9] = F_SEL_NONE;
	g_VideoFormatFSYUY2.byaStillFrameTbl[10] = F_SEL_NONE;
	g_VideoFormatFSYUY2.byaStillFrameTbl[11] = F_SEL_NONE;
	g_VideoFormatFSYUY2.byaStillFrameTbl[12] = F_SEL_NONE;

	g_VideoFormatFSYUY2.waFrameFpsBitmap[0] = FPS_3;  //640_480
	g_VideoFormatFSYUY2.waFrameFpsBitmap[1] = FPS_10; //160_120
	g_VideoFormatFSYUY2.waFrameFpsBitmap[2] = FPS_5; // 176_144
	g_VideoFormatFSYUY2.waFrameFpsBitmap[3] = FPS_3;
	g_VideoFormatFSYUY2.waFrameFpsBitmap[4] = FPS_3;
	g_VideoFormatFSYUY2.waFrameFpsBitmap[5] = FPS_3;
	g_VideoFormatFSYUY2.waFrameFpsBitmap[6] = FPS_3;
	g_VideoFormatFSYUY2.waFrameFpsBitmap[7] = FPS_3;
	g_VideoFormatFSYUY2.waFrameFpsBitmap[8] = FPS_3;
	g_VideoFormatFSYUY2.waFrameFpsBitmap[9] = FPS_3;
	g_VideoFormatFSYUY2.waFrameFpsBitmap[10] = FPS_3;
	g_VideoFormatFSYUY2.waFrameFpsBitmap[11] = FPS_3;
	g_VideoFormatFSYUY2.waFrameFpsBitmap[12] = FPS_3;
	g_VideoFormatFSYUY2.waFrameFpsBitmap[13] = FPS_3;
	g_VideoFormatFSYUY2.waFrameFpsBitmap[14] = FPS_3;
	g_VideoFormatFSYUY2.waFrameFpsBitmap[15] = FPS_3;
	g_VideoFormatFSYUY2.waFrameFpsBitmap[16] = FPS_3;
	g_VideoFormatFSYUY2.waFrameFpsBitmap[17] = FPS_3;
	g_VideoFormatFSYUY2.waFrameFpsBitmap[18] = FPS_3;	

	for (i = 0; i <MAX_FORMAT_TYPE; i++)
	{
		g_aVideoFormat[i].byFormatType = FORMAT_TYPE_NC;
	}

	//g_bySensor_YuvMode = YUV422_MODE; // 0; for more YUYV sensor __jqg_20100330__
	g_bySensor_YuvMode = RAW_MODE;
	g_byStartVideo =0;

	for(i=0; i<CTT_INDEX_MAX; i++)
	{
		g_asOvCTT[i].wCT = 0;
		g_asOvCTT[i].wRgain = 0;
		g_asOvCTT[i].wGgain = 0;
		g_asOvCTT[i].wBgain = 0;
	}

	// Back light compensation  ,zhangbo  move to here ,these is only used in soc sensor
	g_byOVAEW_BLC = 0x58;
	g_byOVAEB_BLC = 0x4B;
	g_byOVAEW_Normal = 0x50;
	g_byOVAEB_Normal = 0x43;

#ifdef _STILL_IMG_BACKUP_SETTING_
	g_wPreviewBackupExposure = 0;
	g_wPreviewBackupGain = 0;
#endif

#ifdef _BLACK_SCREEN_
	g_byBackendIspSwitchPoint =0;
	g_byIspCtl = 0;//BLACK_PATTERN_ENABLE;
	g_byIsBlack = 0;
	g_wBlackPatternGain_TH = 32;
	g_wBlackPatternExposuretime_TH = 800;
	g_byBlackPatternYavg_TH = 32;
#endif

#ifdef _IQ_TABLE_CALIBRATION_
	g_byIQPatchGrpOnePatchOne = 0;	
#endif
}

void Init_ExternalMemory_GlobalVars()
{
#ifndef _CACHE_MODE_
	g_bySPIClockSinkSel = SPI_CLK_SINK_SF;
	g_wSFCapacity=0;
	g_bySFProgModeSP=SF_SP_BYTE_PROG | SF_SP_PAGE_PROG;
	g_bySFChipEROpCode=SF_CHIP_ERASE;
#endif
	g_bySFManuID=0;
	g_wSFDeviceID=0;	
	g_byReadMode = SPI_CADI_MODE0;
}

void Init_USB_GlobalVars()
{
	// USB
	g_bIsHighSpeed=0;

	g_wVdStrManuFactorAddr = 0 ;
	g_wVdStrProductAddr = 0;
	g_wVdStrSerialNumAddr = 0;
	g_wVdIADStrAddr =0;

	// USF IF test
	g_wTimerCounterForUSBIF = 0;
	g_fThermalCofe = -2.038593198;
	g_byThermalCalibrationOffset = 0;


	g_byIsHighSpeedDesc=1;
	g_bybTrigger=CTL_TRIGGER_STS_NORMAL;

	// VID/PID/DevID
	g_byVendorIDL=_RT_VID_L_;
	g_byVendorIDH=_RT_VID_H_;
	g_byProductIDL=_5840_PID_L_;
	g_byProductIDH=_5840_PID_H_;

	// hemonel 2009-08-28: use customer ID+ customer project number to express device rev.
	g_byDevVerID_L  =g_cVdFwVerSection0.wPID;		// customer Project number
    g_byDevVerID_H =g_cVdFwVerSection0.wVID;		// customer ID

#ifdef _NF_RESTORE_
	g_bySuspendFW_N = 0;
	g_bySuspendFW_F = 0;
	g_byBypassFirstSOF = 0;
#endif	

#ifdef _SAVE_POWER_
	g_byPHYHSRxTxPwrDownMode=SP_USB_HSRX_PWRDWN_EN|SP_USB_HSTX_PWRDWN_EN;
#else
	g_byPHYHSRxTxPwrDownMode=0;
#endif

#ifdef _EN_PHY_DBG_MODE_
	g_byPhyAddrDbg =0x81;
#endif


	// UBIST and Extension Unit For Dell Test
#ifdef _UBIST_TEST_
	g_wVdUBISTStrAddr =0;
	g_byUBISTEnble = 1;	// Jimmy 2012-03-29: cofigure string and vid/pid only, others deleted
	g_byUbistTestProgress = 0x00;
	g_byUbistTestReturnCode = 0x04;
	g_byUbistTestAbort = 0;
	g_byUbistTestEnable = 0;
#endif // _UBIST_TEST_
#ifdef _DELL_EXU_
	g_byMaxAutoGain = 0;
	g_wMaxAutoExpTime = 0;
	g_byLEDStatus = 1;
	g_byLEDBlinkCnt = 0;
	g_byStreamActive = 0;
#endif




	g_byVendorReqStatus=VDREQ_STS_NO_ERROR;
	g_byConfigurationValue=0;
	g_byVideoStrmIFSetting=0;

	// UVC
	g_byCommitFPS = 15;
	g_bySnrEffect = SNR_EFFECT_NORMAL;
	g_byVCLastError=VC_ERR_NOERROR;
	g_byVSLastError=VS_ERR_NOERROR;
	g_byDataInOutCtlSize =MAX_RWFLASH_SIZE;

	g_wCurFrameWidth=0;
	g_wCurFrameHeight=0;
#if(defined _ENABLE_MJPEG_ || defined _ENABLE_M420_FMT_)
	g_wCurMJPEGOutputWidth = 0;
	g_wCurMJPEGOutputHeight= 0;
	g_wSensorMJPGByPass = 0;
#endif
	g_byCurFps = 0;
	g_byCurFormat = 0;

	//g_byPreviewDropFrameNumber =10;	// hemonel 2011-02-18: must set no less than 1 because AE setting valid after still image
	g_byPreviewDelayTimeMax =10;		// unit: 100ms
	g_byStillimgDropFrameNumber= 15;
	g_byStillimgDelayTimeMax= 10;		// unit: 100ms

#ifdef _SENSOR_ESD_ISSUE_
	g_byESDExist = 0;
	g_byStreamOn = 0;
	g_wESDTimerCounter = 0;
#endif

#ifdef _RTK_EXTENDED_CTL_
	InitRtkExtISPSpecialEffectCtl();
	InitRtkExtEVCompensationCtl();
	InitRtkExtROICtl();
	InitRtkExtPreviewLEDOffCtl();
	InitRtkExtISOCtl();
#endif

#ifdef _AF_ENABLE_
	Ctl_FocusAbsolut.Info = CONTROL_INFO_SP_SET
	                        |CONTROL_INFO_SP_GET
	                        |CONTROL_INFO_DIS_BY_AUTO
	                        |CONTROL_INFO_SP_AUTOUPDATA;
	Ctl_FocusAbsolut.ChangeFlag = CTL_CHNGFLG_RDY;
	Ctl_FocusAbsolut.OpMask = CONTROL_OP_SET_CUR
	                          |CONTROL_OP_GET_CUR
	                          |CONTROL_OP_GET_MAX
	                          |CONTROL_OP_GET_MIN
	                          |CONTROL_OP_GET_RES
	                          |CONTROL_OP_GET_DEF;
	Ctl_FocusAbsolut.Len = CONTROL_LEN_2;
	Ctl_FocusAbsolut.pAttr = (CtlAttr_t*)&FocusAbsolutItem;

	Ctl_FocusAuto.Info = CONTROL_INFO_SP_SET
	                     |CONTROL_INFO_SP_GET;
	Ctl_FocusAuto.ChangeFlag = CTL_CHNGFLG_RDY;
	Ctl_FocusAuto.OpMask = CONTROL_OP_SET_CUR
	                       |CONTROL_OP_GET_CUR
	                       |CONTROL_OP_GET_MIN
	                       |CONTROL_OP_GET_MAX
	                       |CONTROL_OP_GET_LEN
	                       |CONTROL_OP_GET_RES
	                       |CONTROL_OP_GET_DEF;
	Ctl_FocusAuto.Len = CONTROL_LEN_1;
	Ctl_FocusAuto.pAttr = (CtlAttr_t*)&FocusAutoItem;
#endif

	Ctl_ExposureTimeAbsolut.Info = CONTROL_INFO_SP_SET
	                               |CONTROL_INFO_SP_GET
	                               |CONTROL_INFO_DIS_BY_AUTO
	                               |CONTROL_INFO_SP_AUTOUPDATA;
	Ctl_ExposureTimeAbsolut.ChangeFlag = CTL_CHNGFLG_RDY;
	Ctl_ExposureTimeAbsolut.OpMask = CONTROL_OP_SET_CUR
	                                 |CONTROL_OP_GET_CUR
	                                 |CONTROL_OP_GET_MAX
	                                 |CONTROL_OP_GET_MIN
	                                 |CONTROL_OP_GET_RES
	                                 |CONTROL_OP_GET_DEF;
	Ctl_ExposureTimeAbsolut.Len = CONTROL_LEN_4;
	Ctl_ExposureTimeAbsolut.pAttr = (CtlAttr_t*)&ExposureTimeAbsolutItem;

	Ctl_ExposureTimeAuto.Info = CONTROL_INFO_SP_SET
	                            |CONTROL_INFO_SP_GET;
	Ctl_ExposureTimeAuto.ChangeFlag = CTL_CHNGFLG_RDY;
	Ctl_ExposureTimeAuto.OpMask = CONTROL_OP_SET_CUR
	                              |CONTROL_OP_GET_CUR
	                              |CONTROL_OP_GET_MIN
	                              |CONTROL_OP_GET_MAX
	                              |CONTROL_OP_GET_LEN
	                              |CONTROL_OP_GET_RES
	                              |CONTROL_OP_GET_DEF;
	Ctl_ExposureTimeAuto.Len = CONTROL_LEN_1;
	Ctl_ExposureTimeAuto.pAttr = (CtlAttr_t*)&ExposureTimeAutoItem;

	Ctl_LowLightComp.Info = CONTROL_INFO_SP_SET
	                        |CONTROL_INFO_SP_GET;
	Ctl_LowLightComp.ChangeFlag = CTL_CHNGFLG_RDY;
	Ctl_LowLightComp.OpMask = CONTROL_OP_SET_CUR
	                          |CONTROL_OP_GET_CUR
	                          |CONTROL_OP_GET_RES
	                          |CONTROL_OP_GET_DEF
	                          |CONTROL_OP_GET_MIN
	                          |CONTROL_OP_GET_MAX
	                          |CONTROL_ATTR_UNSIGNED;
	Ctl_LowLightComp.Len = CONTROL_LEN_1;
	Ctl_LowLightComp.pAttr = (CtlAttr_t*)&LowLightCompItem;

	Ctl_Brightness.Info = CONTROL_INFO_SP_SET
	                      |CONTROL_INFO_SP_GET;
	Ctl_Brightness.ChangeFlag = CTL_CHNGFLG_RDY;
	Ctl_Brightness.OpMask = CONTROL_OP_SET_CUR
	                        |CONTROL_OP_GET_CUR
	                        |CONTROL_OP_GET_MAX
	                        |CONTROL_OP_GET_MIN
	                        |CONTROL_OP_GET_RES
	                        |CONTROL_OP_GET_DEF
	                        |CONTROL_ATTR_SIGNED;
	Ctl_Brightness.Len = CONTROL_LEN_2;
	Ctl_Brightness.pAttr = (CtlAttr_t*)&BrightnessItem;

	Ctl_Contrast.Info = CONTROL_INFO_SP_SET
	                    |CONTROL_INFO_SP_GET;
	Ctl_Contrast.ChangeFlag = CTL_CHNGFLG_RDY;
	Ctl_Contrast.OpMask = CONTROL_OP_SET_CUR
	                      |CONTROL_OP_GET_CUR
	                      |CONTROL_OP_GET_MAX
	                      |CONTROL_OP_GET_MIN
	                      |CONTROL_OP_GET_RES
	                      |CONTROL_OP_GET_DEF
	                      |CONTROL_ATTR_UNSIGNED;
	Ctl_Contrast.Len = CONTROL_LEN_2;
	Ctl_Contrast.pAttr = (CtlAttr_t*)&ContrastItem;

	Ctl_Hue.Info = CONTROL_INFO_SP_SET
	               |CONTROL_INFO_SP_GET;
	Ctl_Hue.ChangeFlag = CTL_CHNGFLG_RDY;
	Ctl_Hue.OpMask = CONTROL_OP_SET_CUR
	                 |CONTROL_OP_GET_CUR
	                 |CONTROL_OP_GET_MAX
	                 |CONTROL_OP_GET_MIN
	                 |CONTROL_OP_GET_RES
	                 |CONTROL_OP_GET_DEF
	                 |CONTROL_ATTR_SIGNED;
	Ctl_Hue.Len = CONTROL_LEN_2;
	Ctl_Hue.pAttr  = (CtlAttr_t*)&HueItem;

	Ctl_Saturation.Info = CONTROL_INFO_SP_SET
	                      |CONTROL_INFO_SP_GET;
	Ctl_Saturation.ChangeFlag = CTL_CHNGFLG_RDY;
	Ctl_Saturation.OpMask = CONTROL_OP_SET_CUR
	                        |CONTROL_OP_GET_CUR
	                        |CONTROL_OP_GET_MAX
	                        |CONTROL_OP_GET_MIN
	                        |CONTROL_OP_GET_RES
	                        |CONTROL_OP_GET_DEF
	                        |CONTROL_ATTR_UNSIGNED;
	Ctl_Saturation.Len = CONTROL_LEN_2;
	Ctl_Saturation.pAttr = (CtlAttr_t*)&SaturationItem;


	Ctl_Sharpness.Info = CONTROL_INFO_SP_SET
	                     |CONTROL_INFO_SP_GET;
	Ctl_Sharpness.ChangeFlag = CTL_CHNGFLG_RDY;
	Ctl_Sharpness.OpMask = CONTROL_OP_SET_CUR
	                       |CONTROL_OP_GET_CUR
	                       |CONTROL_OP_GET_MAX
	                       |CONTROL_OP_GET_MIN
	                       |CONTROL_OP_GET_RES
	                       |CONTROL_OP_GET_DEF
	                       |CONTROL_ATTR_UNSIGNED;
	Ctl_Sharpness.Len = CONTROL_LEN_2;
	Ctl_Sharpness.pAttr = (CtlAttr_t*)&SharpnessItem;


	Ctl_Gamma.Info = CONTROL_INFO_SP_SET
	                 |CONTROL_INFO_SP_GET;
	Ctl_Gamma.ChangeFlag = CTL_CHNGFLG_RDY;
	Ctl_Gamma.OpMask = CONTROL_OP_SET_CUR
	                   |CONTROL_OP_GET_CUR
	                   |CONTROL_OP_GET_MAX
	                   |CONTROL_OP_GET_MIN
	                   |CONTROL_OP_GET_RES
	                   |CONTROL_OP_GET_DEF
	                   |CONTROL_ATTR_UNSIGNED;
	Ctl_Gamma.Len = CONTROL_LEN_2;
	Ctl_Gamma.pAttr = (CtlAttr_t*)&GammaItem;


	Ctl_WhiteBalanceTemp.Info = CONTROL_INFO_SP_SET
	                            |CONTROL_INFO_SP_GET
	                            |CONTROL_INFO_DIS_BY_AUTO
	                            |CONTROL_INFO_SP_AUTOUPDATA;
	Ctl_WhiteBalanceTemp.ChangeFlag = CTL_CHNGFLG_RDY;
	Ctl_WhiteBalanceTemp.OpMask = CONTROL_OP_SET_CUR
	                              |CONTROL_OP_GET_CUR
	                              |CONTROL_OP_GET_MAX
	                              |CONTROL_OP_GET_MIN
	                              |CONTROL_OP_GET_LEN
	                              |CONTROL_OP_GET_RES
	                              |CONTROL_OP_GET_DEF
	                              |CONTROL_ATTR_UNSIGNED;
	Ctl_WhiteBalanceTemp.Len = CONTROL_LEN_2;
	Ctl_WhiteBalanceTemp.pAttr = (CtlAttr_t*)&WhiteBalanceTempItem;


	Ctl_BackLightComp.Info = CONTROL_INFO_SP_SET
	                         |CONTROL_INFO_SP_GET;
	Ctl_BackLightComp.ChangeFlag = CTL_CHNGFLG_RDY;
	Ctl_BackLightComp.OpMask = CONTROL_OP_SET_CUR
	                           |CONTROL_OP_GET_CUR
	                           |CONTROL_OP_GET_MAX
	                           |CONTROL_OP_GET_MIN
	                           |CONTROL_OP_GET_RES
	                           |CONTROL_OP_GET_DEF
	                           |CONTROL_ATTR_UNSIGNED;
	Ctl_BackLightComp.Len = CONTROL_LEN_2;
	Ctl_BackLightComp.pAttr = (CtlAttr_t*)&BackLightCompItem;


	Ctl_Gain.Info = CONTROL_INFO_SP_SET
	                |CONTROL_INFO_SP_GET;
	Ctl_Gain.ChangeFlag = CTL_CHNGFLG_RDY;
	Ctl_Gain.OpMask = CONTROL_OP_SET_CUR
	                  |CONTROL_OP_GET_CUR
	                  |CONTROL_OP_GET_MAX
	                  |CONTROL_OP_GET_MIN
	                  |CONTROL_OP_GET_RES
	                  |CONTROL_OP_GET_DEF
	                  |CONTROL_ATTR_UNSIGNED;
	Ctl_Gain.Len = CONTROL_LEN_2;
	Ctl_Gain.pAttr = (CtlAttr_t*)&GainItem;


	Ctl_PwrLineFreq.Info = CONTROL_INFO_SP_SET
	                       |CONTROL_INFO_SP_GET;
	Ctl_PwrLineFreq.ChangeFlag = CTL_CHNGFLG_RDY;
	Ctl_PwrLineFreq.OpMask = CONTROL_OP_SET_CUR
	                         |CONTROL_OP_GET_CUR
	                         |CONTROL_OP_GET_RES
	                         |CONTROL_OP_GET_DEF
	                         |CONTROL_OP_GET_MIN
	                         |CONTROL_OP_GET_MAX
	                         |CONTROL_ATTR_UNSIGNED;
	Ctl_PwrLineFreq.Len = CONTROL_LEN_1;
	Ctl_PwrLineFreq.pAttr = (CtlAttr_t*)&PwrLineFreqItem;


	Ctl_WhiteBalanceTempAuto.Info = CONTROL_INFO_SP_SET
	                                |CONTROL_INFO_SP_GET;
	Ctl_WhiteBalanceTempAuto.ChangeFlag = CTL_CHNGFLG_RDY;
	Ctl_WhiteBalanceTempAuto.OpMask = CONTROL_OP_SET_CUR
	                                  |CONTROL_OP_GET_CUR
	                                  |CONTROL_OP_GET_MAX
	                                  |CONTROL_OP_GET_MIN
	                                  |CONTROL_OP_GET_LEN
	                                  |CONTROL_OP_GET_DEF
	                                  |CONTROL_OP_GET_RES;
	Ctl_WhiteBalanceTempAuto.Len = CONTROL_LEN_1;
	Ctl_WhiteBalanceTempAuto.pAttr = (CtlAttr_t*)&WhiteBalanceTempAutoItem;


	Ctl_PanTilt.Info = CONTROL_INFO_SP_SET
	                   |CONTROL_INFO_SP_GET;
	Ctl_PanTilt.ChangeFlag = CTL_CHNGFLG_RDY;
	Ctl_PanTilt.OpMask = CONTROL_OP_SET_CUR
	                     |CONTROL_OP_GET_CUR
	                     |CONTROL_OP_GET_MAX
	                     |CONTROL_OP_GET_MIN
	                     |CONTROL_OP_GET_RES
	                     |CONTROL_OP_GET_DEF
	                     |CONTROL_ATTR_SIGNED;
	Ctl_PanTilt.Len = CONTROL_LEN_8;
	Ctl_PanTilt.pAttr = NULL;


	Ctl_Zoom.Info = CONTROL_INFO_SP_SET
	                |CONTROL_INFO_SP_GET;
	Ctl_Zoom.ChangeFlag = CTL_CHNGFLG_RDY;
	Ctl_Zoom.OpMask = CONTROL_OP_SET_CUR
	                  |CONTROL_OP_GET_CUR
	                  |CONTROL_OP_GET_MAX
	                  |CONTROL_OP_GET_MIN
	                  |CONTROL_OP_GET_RES
	                  |CONTROL_OP_GET_DEF
	                  |CONTROL_ATTR_UNSIGNED;
	Ctl_Zoom.Len = CONTROL_LEN_2;
	Ctl_Zoom.pAttr = (CtlAttr_t*)&ZoomItem;


	Ctl_Roll.Info = CONTROL_INFO_SP_SET
	                |CONTROL_INFO_SP_GET;
	Ctl_Roll.ChangeFlag = CTL_CHNGFLG_RDY;
	Ctl_Roll.OpMask = CONTROL_OP_SET_CUR
	                  |CONTROL_OP_GET_CUR
	                  |CONTROL_OP_GET_MAX
	                  |CONTROL_OP_GET_MIN
	                  |CONTROL_OP_GET_RES
	                  |CONTROL_OP_GET_DEF
	                  |CONTROL_ATTR_SIGNED;
	Ctl_Roll.Len = CONTROL_LEN_2;
	Ctl_Roll.pAttr = (CtlAttr_t*)&RollItem;

	Ctl_TrapeziumCorrection.Info = CONTROL_INFO_SP_SET
	                               |CONTROL_INFO_SP_GET;
	Ctl_TrapeziumCorrection.ChangeFlag = CTL_CHNGFLG_RDY;
	Ctl_TrapeziumCorrection.OpMask = CONTROL_OP_SET_CUR
	                                 |CONTROL_OP_GET_CUR
	                                 |CONTROL_OP_GET_MAX
	                                 |CONTROL_OP_GET_MIN
	                                 |CONTROL_OP_GET_RES
	                                 |CONTROL_OP_GET_DEF
	                                 |CONTROL_ATTR_SIGNED
	                                 |CONTROL_OP_GET_LEN;
	Ctl_TrapeziumCorrection.Len = CONTROL_LEN_1;
	Ctl_TrapeziumCorrection.pAttr = (CtlAttr_t*)&TCorrectionItem;

	g_byStillFPS = 15;
	g_dwPanFalseValue_ForXPbug = 0;

#ifdef _UVC_PPWB_
	memset(&g_VsPropCommit, 0, sizeof(g_VsPropCommit));
#endif
#ifdef _UVC_COMMITWB_
	g_VsBulkCommit.byCommitFormatIndex = 1;
	g_VsBulkCommit.byCommitFrameIndex = 1;
	g_VsBulkCommit.byCommitFPS = 30;
	g_VsBulkCommit.wLWM = LPM_LWM;
	g_VsBulkCommit.wHWM = LPM_HWM;
	g_VsBulkCommit.wTransferSize= TRANSFER_SIZE;
#endif

#ifdef _LENOVO_JAPAN_PROPERTY_PAGE_
	g_byAWBRGain_Lenovo = 128;
	g_byAWBGGain_Lenovo = 128;
	g_byAWBBGain_Lenovo = 128;
	g_byVendorDriver_Lenovo	= 0; 
#endif

	memset(&g_VsStlProbe, 0, sizeof (g_VsStlProbe));
	memset(&g_VsProbe, 0, sizeof(g_VsProbe));
	memset(&g_VsCommit, 0, sizeof(g_VsCommit));
	memset(&g_VsStlCommit, 0, sizeof(g_VsStlCommit));
	g_wProcessUnitChange_Flag = 0;
	g_wCameraTerminalChange_Flag = 0;
	g_byCommitChange_Flag=0;
#ifdef _RTK_EXTENDED_CTL_
	g_dwRtkExtCtlUnitChange_Flag = 0;
#endif
	g_wVCCSdescSize = 0;
	g_wVSCSdescSize = 0;
	g_wTotalCfgDescSize = 0;

	

	g_cwaResolutionTable[F_SEL_640_480].wWidth = VIDEO_WIDTH_640;
	g_cwaResolutionTable[F_SEL_640_480].wHeight= VIDEO_HEIGHT_480;

	g_cwaResolutionTable[F_SEL_160_120].wWidth = VIDEO_WIDTH_160;
	g_cwaResolutionTable[F_SEL_160_120].wHeight= VIDEO_HEIGHT_120;

#ifdef _IMX076_320x240_60FPS_
	g_cwaResolutionTable[F_SEL_160_90].wWidth = VIDEO_WIDTH_160;
	g_cwaResolutionTable[F_SEL_160_90].wHeight= VIDEO_HEIGHT_90;
#endif
	g_cwaResolutionTable[F_SEL_176_144].wWidth = VIDEO_WIDTH_176;
	g_cwaResolutionTable[F_SEL_176_144].wHeight= VIDEO_HEIGHT_144;

	g_cwaResolutionTable[F_SEL_320_200].wWidth = VIDEO_WIDTH_320;
	g_cwaResolutionTable[F_SEL_320_200].wHeight= VIDEO_HEIGHT_200;

	g_cwaResolutionTable[F_SEL_320_240].wWidth = VIDEO_WIDTH_320;
	g_cwaResolutionTable[F_SEL_320_240].wHeight= VIDEO_HEIGHT_240;

	g_cwaResolutionTable[F_SEL_352_288].wWidth = VIDEO_WIDTH_352;
	g_cwaResolutionTable[F_SEL_352_288].wHeight= VIDEO_HEIGHT_288;

	g_cwaResolutionTable[F_SEL_640_400].wWidth = VIDEO_WIDTH_640;
	g_cwaResolutionTable[F_SEL_640_400].wHeight= VIDEO_HEIGHT_400;

	g_cwaResolutionTable[F_SEL_800_600].wWidth = VIDEO_WIDTH_800;
	g_cwaResolutionTable[F_SEL_800_600].wHeight= VIDEO_HEIGHT_600;

	//g_cwaResolutionTable[F_SEL_960_720].wWidth = VIDEO_WIDTH_960;
	//g_cwaResolutionTable[F_SEL_960_720].wHeight= VIDEO_HEIGHT_720;

	g_cwaResolutionTable[F_SEL_960_540].wWidth = VIDEO_WIDTH_960;
	g_cwaResolutionTable[F_SEL_960_540].wHeight= VIDEO_HEIGHT_540;	

	g_cwaResolutionTable[F_SEL_1024_768].wWidth = VIDEO_WIDTH_1024;
	g_cwaResolutionTable[F_SEL_1024_768].wHeight= VIDEO_HEIGHT_768;

	g_cwaResolutionTable[F_SEL_1280_720].wWidth = VIDEO_WIDTH_1280;
	g_cwaResolutionTable[F_SEL_1280_720].wHeight= VIDEO_HEIGHT_720;

	g_cwaResolutionTable[F_SEL_1280_800].wWidth = VIDEO_WIDTH_1280;
	g_cwaResolutionTable[F_SEL_1280_800].wHeight= VIDEO_HEIGHT_800;

	g_cwaResolutionTable[F_SEL_1280_960].wWidth = VIDEO_WIDTH_1280;
	g_cwaResolutionTable[F_SEL_1280_960].wHeight= VIDEO_HEIGHT_960;

#ifdef _NEW_FT2_MODEL_
	g_cwaResolutionTable[F_SEL_1280_192].wWidth = VIDEO_WIDTH_1280;
	g_cwaResolutionTable[F_SEL_1280_192].wHeight= VIDEO_HEIGHT_192;

	g_cwaResolutionTable[F_SEL_1600_240].wWidth = VIDEO_WIDTH_1600;
	g_cwaResolutionTable[F_SEL_1600_240].wHeight= VIDEO_HEIGHT_240;
#else
	g_cwaResolutionTable[F_SEL_1280_1024].wWidth = VIDEO_WIDTH_1280;
	g_cwaResolutionTable[F_SEL_1280_1024].wHeight= VIDEO_HEIGHT_1024;

	g_cwaResolutionTable[F_SEL_1600_1200].wWidth = VIDEO_WIDTH_1600;
	g_cwaResolutionTable[F_SEL_1600_1200].wHeight= VIDEO_HEIGHT_1200;
#endif

	g_cwaResolutionTable[F_SEL_1920_1080].wWidth = VIDEO_WIDTH_1920;
	g_cwaResolutionTable[F_SEL_1920_1080].wHeight= VIDEO_HEIGHT_1080;

	g_cwaResolutionTable[F_SEL_1600_900].wWidth = VIDEO_WIDTH_1600;
	g_cwaResolutionTable[F_SEL_1600_900].wHeight= VIDEO_HEIGHT_900;

	g_cwaResolutionTable[F_SEL_2048_1536].wWidth = VIDEO_WIDTH_2048;
	g_cwaResolutionTable[F_SEL_2048_1536].wHeight= VIDEO_HEIGHT_1536;
	
	g_cwaResolutionTable[F_SEL_2592_1944].wWidth = VIDEO_WIDTH_2592;
	g_cwaResolutionTable[F_SEL_2592_1944].wHeight= VIDEO_HEIGHT_1944;

	g_cwaResolutionTable[F_SEL_848_480].wWidth = VIDEO_WIDTH_848;
	g_cwaResolutionTable[F_SEL_848_480].wHeight= VIDEO_HEIGHT_480;

	g_cwaResolutionTable[F_SEL_640_360].wWidth = VIDEO_WIDTH_640;
	g_cwaResolutionTable[F_SEL_640_360].wHeight= VIDEO_HEIGHT_360;

	g_cwaResolutionTable[F_SEL_424_240].wWidth = VIDEO_WIDTH_424;
	g_cwaResolutionTable[F_SEL_424_240].wHeight= VIDEO_HEIGHT_240;

	g_cwaResolutionTable[F_SEL_320_180].wWidth = VIDEO_WIDTH_320;
	g_cwaResolutionTable[F_SEL_320_180].wHeight= VIDEO_HEIGHT_180;







	// UAC & MIC
#ifdef _UAC_EXIST_
	g_wEPB_ADF_delay = 50;
	g_byAudioStrmIFSetting=0;
	g_wAudioVolumeCurr = 0x0; //0db
	g_byAudioAttrCurr= 0; //bit0: mute, bit1: boost, bit2: AGC

	g_byADF_Chn_Switch = 0x00;		// left & right channel no switch
	g_byALC_Cfg = 0x80;				// ALC enable & bypass
	g_byALC_FT_Boost = 0x14;		// default 0x14
	g_byALC_NRGate_Cfg0 = 0x00;		//default 0x00
	g_byConfigMicArray = 0;
	g_wConfigMicArrayCfgAddr = 0;
#endif	

#ifdef _TCORRECTION_EN_
	g_wExtUIspCtl = EXU1_CTL_SEL_TCORRECTION;
#else
	g_wExtUIspCtl = 	0;
#endif
	memset(g_aVideoFormat, 0, MAX_FORMAT_TYPE * (sizeof(VideoFormat_T)));
#ifdef _BULK_ENDPOINT_
	g_byLPMSetbyAP = 0;
	g_wHWMbyAp = 0;
	g_wHWM = LPM_HWM;
	g_wLWM = LPM_LWM;
	g_wTransferSize = TRANSFER_SIZE;
#endif

#ifdef _REMOTE_WAKEUP_SUPPORT_
	g_byRemoteWakeupSupport = 1;
#else
	g_byRemoteWakeupSupport = 0;
#endif
	g_byHostEnableRemoteWakeup = 0;
	g_byRemoteWakeupNotify = 0;
	
}

void Init_System_GlobalVars()
{
#ifdef _S3_RESUMKEK_
	g_wPollTimeOutCnt=0;
#else
	g_byPollTimeOutCnt=0;
#endif	
#if (defined (_SLB_TEST_))
	g_bySLBTestMode=SLBTESTMODE_HSONLY;
	g_bySLB_FSTstCnt = 3;
	g_dwSLB_HSTstCnt = 0x200;
	g_wSLB_HSFailTh = 0x05;
	g_bySLB_FixPattern =0xff;
	g_bySLB_Seed = 0x01;
	g_bySLB_FSFailTh = 1;
	g_wSLB_HSDlyTime = 1;
#endif

	// hemonel 2009-11-23: add led toggle for download code to distinguish download status.
	g_wLEDToggleInterval = 0;

	g_wBarInfoOffset=0;

#ifdef _IC_CODE_
#ifdef _SAVE_POWER_
	g_bySSCEnable = 0;
#else
	g_bySSCEnable = 0;
#endif
#else
	g_bySSCEnable = 0;
#endif

#ifdef _HID_BTN_
	g_byHIDBtnLast = 0;
#endif

	g_byPG_USBSYS_IRQEN = 0;
	g_byWhiteBalanceAutoLast_BackForUVCtest=0;

	g_byVDCMDPVLEDOff = 0;

}


void global_vars_init()
{
	InitCustomizedVars();
	Init_ISP_GlobalVars();

	Init_Sensor_GlobalVars();
	Init_ExternalMemory_GlobalVars();
	Init_USB_GlobalVars();
	Init_System_GlobalVars();
}
