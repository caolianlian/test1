#ifndef _UVC_RTK_EXT_CTL_H_
#define _UVC_RTK_EXT_CTL_H_

#include "CamUvcRtkExtDesc.h"
#include "CamReg.h"
#include "CamCtl.h"

void InitRtkExtISPSpecialEffectCtl(void);
void InitRtkExtEVCompensationCtl(void);
void SetRtkExtROIMax(void);
void InitRtkExtROICtl(void);
void InitRtkExtPreviewLEDOffCtl(void);
void InitRtkExtISOCtl(void);
void VideoCtlRtkExtReq(void);
void InitIdeaEyeCtl(void);

#endif
