#ifndef _CAMISPPO_H_
#define _CAMISPPO_H_

#define COLOR_R		0x00
#define COLOR_G		0x01
#define COLOR_B		0x02

#ifdef _IQ_TABLE_CALIBRATION_
//Version must same to the spec and fw;otherwise will not patch
#define IQ_TABLE_PATCH_MVersion	0x00
#define IQ_TABLE_PATCH_SVersion	0x03

#define IQ_TABLE_PATCH_BASE	0x1E000
#define IQ_TABLE_PATCH_HEADER_LEN		(16)
#define CLSC_FHD_PATCH_OFFSET		(16)
#ifdef IQ_CAL_CLSC_FHD
#define CLSC_FHD_PATCH_LEN		(sizeof(IQ_LSC_Circle_t))
#else
#define CLSC_FHD_PATCH_LEN		(0)
#endif
#define MLSC_FHD_PATCH_OFFSET	(CLSC_FHD_PATCH_OFFSET+CLSC_FHD_PATCH_LEN)
#ifdef IQ_CAL_MLSC_FHD
#define MSLC_FHD_PATCH_LEN		(sizeof(IQ_LSC_Micro_t))
#else
#define MSLC_FHD_PATCH_LEN		(0)
#endif
#define CLSC_5M_PATCH_OFFSET		(MLSC_FHD_PATCH_OFFSET+MSLC_FHD_PATCH_LEN)
#ifdef IQ_CAL_CLSC_5M
#define CLSC_5M_PATCH_LEN		(sizeof(IQ_LSC_Circle_t))
#else
#define CLSC_5M_PATCH_LEN		(0)
#endif
#define MLSC_5M_PATCH_OFFSET	(CLSC_5M_PATCH_OFFSET+CLSC_5M_PATCH_LEN)
#ifdef IQ_CAL_MLSC_5M
#define MSLC_5M_PATCH_LEN		(sizeof(IQ_LSC_Micro_t))
#else
#define MSLC_5M_PATCH_LEN		(0)
#endif
#define DYN_NLSC_PATCH_OFFSET		(MLSC_5M_PATCH_OFFSET+MSLC_5M_PATCH_LEN)
#ifdef IQ_CAL_DYN_LSC
#define DYN_NLSC_PATCH_LEN		(18)
#else
#define DYN_NLSC_PATCH_LEN		(0)
#endif

#endif


void SetBackendScale(U16 wSnrWidth, U16 wSnrHeight, U16 wUsbWidth, U16 wUsbHeight);
void SetBackendISP(U16 width, U16 wheight);
void ISPmoireProcess(void);
U16 GetColorTemperature(U8 byBgain, U8 byRgain);
void InitIspParams(void);
void InitIspParamsByResolution(U16 wCurWidth, U16 wCurHeight);
void SetDynamicISP(U8 byAEC_Gain);
void SetDynamicISP_AWB(U16 wColorTempature);
void SetSkinBeauty(void);
void InitAWBRoughTuneParameters(void);
void SetBKScale(U8 byZoomSet, U16 wSnrWidth, U16 wSnrHeight, U16 wUsbWidth, U16 wUsbHeight,S8 sbyTrapeziumSet);
void SetZoomStart(S8 sbyPan, S8 sbyTilt, S8 sbyTrapeziumSet);
void SetDynamicLSC(void);
void SetDynamicISP_FS(void);
void SetRGBGamma(void);
#ifdef _RTK_EXTENDED_CTL_
void PostZoom2PreZoom(U16 *wX1, U16 *wY1, U16 *wX2, U16 *wY2);
#endif
void InitISP(U8 byPG_EN);
void SF_IRQ_InitISP(void);
void SF_IRQ_InitISPHW(void);
#endif
