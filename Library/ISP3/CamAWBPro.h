#ifndef _CAMAWBPRO_H_
#define _CAMAWBPRO_H_

#define AWB_SLOW_MODE	0
#define AWB_FAST_MODE	1

//g_byAWB_State
#define AWB_STABLE 0x01
#define AWB_UNSTABLE 0x02

void SetAWBWindow(U16 wWidth, U16 wHeight);
void InitAWBParams(void);
void SetBKAWBGain(U16 wRGain,U16 wGGain,U16 wBGain,U8 byflag);
void GetAWBNearestTemper(void);
void AWB_Start_FineTune(void );
void GetAWBFineTuneGain(void);
void GetAWBFinalGain(void);
void GetAWBGrayworldGain(void);
void GetAWBGrayworldGain2(void);
S16 TransferAWB_GB(S8 sbyK, S16 swB,U8 byGB, U8 byGR, S16 wTrans_deno_k);
void ISP_AWBStaticsStart(void);
void GetAWBWindowSt(void);
void SetAWBGain(void);
void GetAWBStatic_BeforeLSC(void);

#endif
