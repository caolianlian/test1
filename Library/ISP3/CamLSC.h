#ifndef _CAMLSC_H_
#define _CAMLSC_H_

#define LIGHT_A_TEMPERATURE 			(0)
#define LIGHT_U30_TEMPERATURE 		(1)
#define LIGHT_CWF_TEMPERATURE 		(2)
#define LIGHT_D50_TEMPERATURE 		(3)
#define LIGHT_D65_TEMPERATURE 		(4)
#define LIGHT_D75_TEMPERATURE 		(5)
#define LIGHT_UNKNOWN_TEMPERATURE 	(6)

void SetDynamicLSCbyCT(U16 wColorTempature);
void SetDynamicLSC(void);
void SetNLSCRateOfLightSource(U8 ls);
void SetDynamicLSCbyCT_New(void);
#endif 
