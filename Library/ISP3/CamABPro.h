#ifndef _CAMABPRO_H_
#define _CAMABPRO_H_

#define DYNAMIC_FLICK_DETECT_STATE 1
#define STATIC_FLICK_DETECT_STATE 2
#define STATIC_FLICK_DOUBLE_DETECT_STATE 3
#define DYNAMIC_FLICK_DOUBLE_DETECT_STATE 4
#define FLICK_DETECT_END_STATE 5
#define MIN_FLICK_DETECT_FPS 10
#define Anti_Flick_BANDING_POINT_COUNT 8

#define AB_START_AE_INITIAL  0x0
#define AB_START_WAIT_AE 0x1
#define AB_START_RUN 0x2
#define AB_START_SKIP_AE_UNSTABLE 0x3
#define AB_START_SKIP_AE_STABLE 0x4

#define FLICK_POSSIBLE_EXIST 0x1
#define FLICK_MUST_NOT_EXIST 0x0

#define AB_FPS_THRESHOLD_MAX 25
#define AB_FPS_THRESHOLD_MID 20
#define AB_FPS_THRESHOLD_MIN 15

void AutoBanding(void);
void InitAutoBandingSetting(U16 width, U16 height);
void ABStaticsStart(void);
#if ((defined _ENABLE_ANTI_FLICK_) ||(defined _ENABLE_OUTDOOR_DETECT_))
U8 GetBandRatio(void);
U8 JudgeObjectMovement(void);
#endif
#endif

