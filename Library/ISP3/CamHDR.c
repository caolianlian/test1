#include "pc_cam.h"
#include "camutil.h"
#include "CamReg.h"
#include "CamUvc.h"
#include "CamCtl.h"
//#include "camisp.h"
//#include "CamSensor.h"
//#include "CamI2C.h"
//#include "CamProcess.h"
#include "Global_vars.h"
#include "ISP_vars.h"
#include "CamVdCfg.h"
#include "CamIspPro.h"
//#include "CamAECPro.h"
//#include "CamAWBPro.h"
//#include "ISP_TuningTable.h"
//#ifdef _ENABLE_ANTI_FLICK_
//#include "CamABPro.h"
//#endif

#ifndef _HW_HDR_ENABLE_
U8 g_byGammaCount=0;
S8 g_asbyGamma_diff[28];
#endif

void HdrAdjust(void)
{
#ifdef _HW_HDR_ENABLE_
	U8 hdr_level;

	if(g_bBacklightFlag==1)
	{
		 hdr_level = LinearIntp_Byte_Bound(g_tHdrTh.byHistBlkPxlTh, 0, g_tHdrTh.byHistBlkPxlMax, g_tHdrTh.byHDRMaxTuneVaule,(U8)g_byHistEqLB);
		XBYTE[ISP_HDR_LEVEL] = hdr_level;
		XBYTE[ISP_TGAMMA_RATE] = LinearIntp_Byte_Bound(10, g_byTgamma_rate_min,  g_tHdrTh.byHDRMaxTuneVaule-50,g_byTgamma_rate_max,(U8)hdr_level);
	}
	else
	{
		XBYTE[ISP_HDR_LEVEL] =  0;
		XBYTE[ISP_TGAMMA_RATE] =0;
	}	
	XBYTE[ISP_GAMMA_SYNC] = 1;
	XBYTE[ISP_HDR_ADJ_CTRL] = ISP_START_LOAD_HDRCONT_LEVLE | ISP_LOAD_HDR_LEVLE_STEP;

#else

	U8 i;
	U8 byPercentCurL;
	U16 temp_Hdr_level=0;

	if((g_byGammaCount==0))
	{
		if(g_bBacklightFlag==1)
		{
			if(g_byFX>=174)
			{
				temp_Hdr_level =255;
			}
			else if(g_byFX>=140)
			{
				temp_Hdr_level = 120+(g_byFX-140)*4;
			}
			else if(g_byFX>=120)
			{
				temp_Hdr_level = 60+(g_byFX-120)*3;
			}
			else if(g_byFX>=100)
			{
				temp_Hdr_level = 20+(g_byFX-100)*2;
			}
			else if(g_byFX>=80)
			{
				temp_Hdr_level = g_byFX-80;
			}

			byPercentCurL = LinearIntp_Byte_Bound(g_tHdrTh.byHistBlkPxlTh, 0, g_tHdrTh.byHistBlkPxlMax, g_tHdrTh.byHDRMaxTuneVaule,(U8)g_byHistEqLB);
			
	    	byPercentCurL = byPercentCurL>temp_Hdr_level?temp_Hdr_level:byPercentCurL;
			
		}
		else
		{
			byPercentCurL = 0;
		}
		
		for (i=0;i<28;i++)
		{
			g_asbyGamma_diff[i] =  LinearIntp_Byte_Bound(0, g_byIQGamma.abyNor[i], 100, g_byHDRGamma[i],  byPercentCurL)-g_aGamma_Def[i];
		}		
	}
	
	for (i=0;i<28;i++)
	{
		g_aGamma_Def[i] = (U8)(g_aGamma_Def[i]+g_asbyGamma_diff[i]/5);
	}
	
	g_byGammaCount++;		
	if(g_byGammaCount>5)
	{
		g_byGammaCount=0;
	}

	for(i=1;i<28;i++)
	{
		if(g_aGamma_Def[i]<g_aGamma_Def[i-1])
		{
			g_aGamma_Def[i]=g_aGamma_Def[i-1];
		}
	}
		
	SetRGBGamma();	
	
#endif	
}

U8 HdrJudge(void)
{
	S8 i;
	U8 byFX = 0;
	U32 dwFX = 0;
	U32 dwhist_total = 0;
	U32 dwHistEqLB = 0;
	U32 dwHistEqHB = 0;
	U16 wSTnum;
	U8 byH_cmp;
	U8 byL_cmp;	
	U8 byYMean_cmp=0;

	wSTnum = g_wSTnum*25;

	for(i=0;i<64;i++)
	{
		dwFX += ((U32)g_wAE_Hist_StNum[i])*abs(i-((g_byAEMeanValue+2)>>2));
		dwhist_total += g_wAE_Hist_StNum[i];

		if (i<8)
		{
			dwHistEqLB = dwHistEqLB + (g_wAE_Hist_StNum[i]);
		}
		if (i>56)
		{
			dwHistEqHB = dwHistEqHB + (g_wAE_Hist_StNum[i]);
		}
	}

	g_byFX = dwFX*10/dwhist_total;

	if(g_byFX>=80)
	{
		g_bBacklightFlag=1;
	}
	else
	{
		g_bBacklightFlag=0;
	}

	if(g_bBacklightFlag==1)
	{
		g_byHistEqLB = dwHistEqLB*100/wSTnum;
		g_byHistEqHB = dwHistEqHB*100/wSTnum;

		byH_cmp=LinearIntp_Byte_Bound(10, 0, 20, 20, (U8)g_byHistEqHB);
		byL_cmp=LinearIntp_Byte_Bound(10, 0, 60, 0, (U8)g_byHistEqLB);

		if(byH_cmp>byL_cmp)
		{
			byYMean_cmp=byH_cmp-byL_cmp;
		}		
	}

	return byYMean_cmp;
}
