#include "pc_cam.h"
#include "CamReg.h"
#include "Global_vars.h"
#include "ISP_vars.h"
#include "CamIspPro.h"
//#include "camutil.h"
//#include "CamAECPro.h"
#include "CamAFPro.h"

#ifdef _AF_ENABLE_

//zhangbo sensor board 6
//U16 g_wAFPos[AF_MAX_STEP]= {225,232,240,247,255,262,270,278,285,292,     300,308,315,322,330,338,345,355,375,385,     405,415,425,435,445,455,465,475,485,495,    505,515,525,535,545,555,565,575,585,595};
// sensor board 7
//U16  g_wAFPos[AF_MAX_STEP]= {265,280,295,310,325,340,355,370,385,415,445,475,505,535,565,595,};
//sensor board 1
//U16 g_wAFPos[AF_MAX_STEP]= {260,270,282,295,307,319,332,344,357,370,385,415,445,475,505,530,};
//Chicony PA20F
//U16   g_wAFPos[AF_MAX_STEP]= {100,105,108,111,116,121,127,132,138,148,158,168,178,188,198,208,};
//U16   g_wAFPos[AF_MAX_STEP]= {0,109,115,121,128,134,140,147,153,159,165,172,178,184,191,197,203,210,216,222,228,235,241,247,254,260,266,272,279,285,291,298,304,310,316,323,329,335,342,348};
//ov2650
U16 g_wAFPos[AF_MAX_STEP]= {250,260,270,280,295,310,325,350,375,400,425,450,500,550,600,650,};



/*
U8 VCM_Read(U16 * pwData)
{
	
	U8 ret = FALSE;
	U8 byLowByte4,byHighByte6;

	START_I2C_CLK();

	//Set read signal
	XBYTE[I2C_ADDR0] = 0x19;
	XBYTE[I2C_ADDR_LEN] = 0x01;

	XBYTE[I2C_DATA_LEN] = I2C_DATA_LEN_2;

	XBYTE[I2C_TRANSFER] = I2C_TRANSFER_START | I2C_READ_MODE;
	if(!WaitTimeOut(I2C_TRANSFER, I2C_TRANSFER_END, 1, 10))
	{
		// timeout
		XBYTE[I2C_TRANSFER] = I2C_TRANSFER_STOP;
		goto ret_VCM_Read;
	}

	// hemonel 2008-03-20: error process
	if(XBYTE[I2C_STATUS]&I2C_TRANSFER_ERROR)
	{
		goto ret_VCM_Read;
	}

	byHighByte6 = XBYTE[I2C_DATA_SCRATCH0]&0x3F;
	byLowByte4 = (XBYTE[I2C_DATA_SCRATCH1]&0xF0)>>4;
	*pwData = ((U16)byHighByte6<<4)+(U16)byLowByte4;
	ret = TRUE;

ret_VCM_Read:
	STOP_I2C_CLK();
	return ret;
}
*/

U8 VCM_Write(U16 wData)
{
	U8 ret = FALSE;
	U8 byHighByte6,byLowByte4;

	START_I2C_CLK();

	//Set read signal
	XBYTE[I2C_ADDR0] = 0x18;
	XBYTE[I2C_ADDR_LEN] = 0x01;

	byHighByte6 =(wData>>4)&0x3F;
	byLowByte4 = (wData<<4)&0xF0;

	XBYTE[I2C_DATA_SCRATCH0] = byHighByte6;
	XBYTE[I2C_DATA_SCRATCH1] = byLowByte4;
	XBYTE[I2C_DATA_LEN] = I2C_DATA_LEN_2;

	XBYTE[I2C_TRANSFER] = I2C_TRANSFER_START | I2C_WRITE_MODE;
	if(!WaitTimeOut(I2C_TRANSFER, I2C_TRANSFER_END, 1, 10))
	{
		// timeout
		XBYTE[I2C_TRANSFER] = I2C_TRANSFER_STOP;
		goto ret_VCM_Write;
	}

	// hemonel 2008-03-20: error process
	if(XBYTE[I2C_STATUS]&I2C_TRANSFER_ERROR)
	{
		goto ret_VCM_Write;
	}

	ret = TRUE;

ret_VCM_Write:
	STOP_I2C_CLK();
	return ret;
}

void JudgeAFSearchMode()
{
	if(g_byAFEnterRoughSearch==1)
	{
		g_byAFStatus = AF_ROUGH_SEARCH;
		g_byAFSearchStep =g_byAF_RoughStep;
		g_byAFSharpPeakTh1=g_byAFSharpPeakTh1_Rough;
		g_byAFSharpPeakTh2=g_byAFSharpPeakTh2_Rough;
	}
	else
	{
		g_byAFStatus = AF_FINE_SEARCH;
		g_byAFSearchStep =g_byAF_FineStep;	
		g_byAFSharpPeakTh1=g_byAFSharpPeakTh1_Fine;
		g_byAFSharpPeakTh2=g_byAFSharpPeakTh2_Fine;
	}
	//update color info
	GetLastColor();

}

static void DirectionTry(U8 byDirect, U8 byJudgeTime)
{
	if(byJudgeTime==0)
	{
		g_byAFPosIndexLast1 =g_byAF_Stable_Position;
		g_dwAFSharpLast1=g_dwAFCurrentSharp;

		g_byAFPosIndex =g_byAF_Stable_Position+byDirect*2-1;
		VCM_Write(g_wAFPos[g_byAFPosIndex]);

		return ;
	}

	if(g_dwAFCurrentSharp<g_dwAFSharpLast1)
	{
		// try search direction false, need turn direction
		g_byAFPosIndexLast2 =g_byAFPosIndexLast1 =g_byAFPosIndex;
		g_dwAFSharpLast3 = g_dwAFSharpLast2=g_dwAFSharpLast1=g_dwAFCurrentSharp;

		g_bySearchingDir = (byDirect+1)%2; //turn direction;
		g_byAFPosIndex +=g_bySearchingDir*2-1;

		g_byAFSearchTimes =1;
		VCM_Write(g_wAFPos[g_byAFPosIndex]);

		JudgeAFSearchMode();
	}
	else
	{
		// try search direction true, continue search
		g_byAFPosIndexLast2=g_byAFPosIndexLast1;
		g_byAFPosIndexLast1 =g_byAFPosIndex;
		g_dwAFSharpLast3=g_dwAFSharpLast2=g_dwAFSharpLast1;
		g_dwAFSharpLast1=g_dwAFCurrentSharp;

		g_bySearchingDir=byDirect;
		g_byAFPosIndex +=g_bySearchingDir*2-1;

		g_byAFSearchTimes =2;
		VCM_Write(g_wAFPos[g_byAFPosIndex]);

		JudgeAFSearchMode();
	}
	return;
}

static void JudgeAFDirection(U8 byJudgeTime)
{
	if((g_byAF_Stable_Position<=(AF_MAX_STEP/2))
	        &&(g_byAF_Stable_Position>=g_byAF_RoughStep))
	{
		DirectionTry(1, byJudgeTime);
	}
	else if((g_byAF_Stable_Position>(AF_MAX_STEP/2))
	        &&(g_byAF_Stable_Position<(AF_MAX_STEP-g_byAF_RoughStep)))
	{
		DirectionTry(0, byJudgeTime);
	}
	else
	{
		if(g_byAF_Stable_Position<g_byAF_RoughStep)//very far~5m
		{
			g_bySearchingDir=1;
		}
		else //if(g_byAF_Stable_Position>=(AF_MAX_STEP-g_byAF_RoughStep))
		{
			g_bySearchingDir=0;
		}

		JudgeAFSearchMode();

		g_byAFPosIndexLast2 =g_byAFPosIndexLast1 =g_byAFPosIndex;
		g_dwAFSharpLast3 = g_dwAFSharpLast2=g_dwAFSharpLast1=g_dwAFCurrentSharp;

		g_byAFPosIndex +=g_byAFSearchStep*g_bySearchingDir*2-g_byAFSearchStep;

		g_byAFSearchTimes =1;
		VCM_Write(g_wAFPos[g_byAFPosIndex]);
	}
}

static U8 AFSharpHillSearch(U8 byNextStatus)
{
	// judge sharpness hill
	// use g_byAFSharpDiffTh for filter the fake hill
	if(g_byAFSearchTimes >= 3)
	{
		if((g_dwAFSharpLast1>g_dwAFCurrentSharp+g_byAFSharpPeakTh1)&&(g_dwAFSharpLast1>g_dwAFSharpLast3+g_byAFSharpPeakTh2))
		{
			g_byAFStatus = byNextStatus;
			return 1;
		}
	}

	// judge whether searching direction is true or not
	// if not, change searching direction
	if((g_dwAFSharpLast1>g_dwAFCurrentSharp+g_byAFSharpPeakTh1)&&(g_dwAFSharpLast2>g_dwAFSharpLast1+g_byAFSharpPeakTh1))
	{
		if((g_byAFPosIndex<(AF_MAX_STEP-2))&&(g_byAFPosIndex>1))
		{
			g_bySearchingDir = (g_bySearchingDir+1)%2; //turn direction
		}

		g_byAFSerachDirectionChangeTimes++;
	}

	// if can't find the peak after change direction two times , half the g_byAFSharpPeakTh1 and g_byAFSharpPeakTh2
	if(g_byAFSerachDirectionChangeTimes>=g_byAFDirChangeTimes_Th)
		{
		g_byAFSharpPeakTh1 /=2;
		g_byAFSharpPeakTh2 /=2;
		g_byAFDirChangeTimes_Th=2;
		g_byAFSerachDirectionChangeTimes=0;
	}

	//save max sharpness position 
	if((g_byAFControl&AF_MAX_SHARP_EN) ==AF_MAX_SHARP_EN )
	{		
   		if(g_dwAFCurrentSharp>g_dwAFMaxSharpness)
   		{
   			g_dwAFMaxSharpness = g_dwAFCurrentSharp;
   			g_byAFMaxSharpPos = g_byAFPosIndex;			
   		}

		//when g_byAFSharpPeakTh2 is very small ,set the max sharpness position
		if(g_byAFSharpPeakTh2<8)
		{
			g_byAFPosIndex=g_byAFMaxSharpPos;

			VCM_Write(g_wAFPos[g_byAFPosIndex]);
			g_byAFStatus = byNextStatus;	

			g_byAFSerachDirectionChangeTimes = 0;
			return 2;
		}
	}

	
	// save current sharpness and position index to last1
	g_dwAFSharpLast3=g_dwAFSharpLast2;
	g_dwAFSharpLast2=g_dwAFSharpLast1;
	g_dwAFSharpLast1 = g_dwAFCurrentSharp;
	g_byAFPosIndexLast2=g_byAFPosIndexLast1;
	g_byAFPosIndexLast1 = g_byAFPosIndex;
	
	// get next position index
	if(g_byAFPosIndex>=(AF_MAX_STEP-g_byAFSearchStep))
	{
		g_bySearchingDir = 0;
		g_byAFSerachDirectionChangeTimes++;
	}
	else if(g_byAFPosIndex<g_byAFSearchStep)
	{
		g_bySearchingDir = 1; //turn direction
		g_byAFSerachDirectionChangeTimes++;
	}
	g_byAFPosIndex += 2*g_bySearchingDir*g_byAFSearchStep-g_byAFSearchStep;       

	g_byAFSearchTimes++;
	
	// update VCM to next position
	VCM_Write(g_wAFPos[g_byAFPosIndex]);
	return 0;
}

void GetLastColor(void)
{
	U8 i,j;
	for(i=0; i<5; i++)
	{
		for(j=0; j<5; j++)
		{
			g_byAFStable_RMean[i][j]= g_byAWB_WinMeanR[i][j];
			g_byAFStable_GMean[i][j]= g_byAWB_WinMeanG[i][j];
			g_byAFStable_BMean[i][j]= g_byAWB_WinMeanB[i][j];			
		}
	}

}

U8 GetColorDiff()
{
	U8 i,j;
	U16 wRDiff;
	U16 wGDiff;
	U16 wBDiff;

	U16 wTotalDiff;
	wRDiff=wGDiff=wBDiff=0;
	for(i=0; i<5; i++)
	{
		for(j=0; j<5; j++)
		{
			//Rmean
			wRDiff += (U16)abs((S16)g_byAFStable_RMean[i][j]-(S16)g_byAWB_WinMeanR[i][j]);
			wGDiff += (U16)abs((S16)g_byAFStable_GMean[i][j]-(S16)g_byAWB_WinMeanG[i][j]);
			wBDiff += (U16)abs((S16)g_byAFStable_BMean[i][j]-(S16)g_byAWB_WinMeanB[i][j]);	
		}
	}

	// total diff
	wRDiff = wRDiff/25;
	wGDiff =wGDiff/25;
	wBDiff = wBDiff/25;

	wTotalDiff =wRDiff+wGDiff+wBDiff;

	g_wAFColorChange= wTotalDiff;

	if(wTotalDiff>g_byAF_ColorDiff_Th)
	{
		g_byAFStatTime2 ++;		
	}
	else 
	{
		g_byAFStatTime2=0;
	}	

	if(g_byAFStatTime2 >g_byAFDiffTimes_Th)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

U8 GetSharpDiff()
{
	float fSharpChange;

	if(g_dwAF_Stable_Sharpness == 0)
	{
		fSharpChange = g_dwAFCurrentSharp+g_fAF_SharpDiff_High_Th;
	}
	else
	{
		fSharpChange = (float)g_dwAFCurrentSharp/(float)g_dwAF_Stable_Sharpness;	
	}

	g_fAFSharpChange = fSharpChange;
	
	if((fSharpChange > g_fAF_SharpDiff_High_Th) || (fSharpChange < g_fAF_SharpDiff_Low_Th))
	{
       	g_byAFStatTime++;		
	}
	else 
	{
		g_byAFStatTime=0;
	}

	if(g_byAFStatTime >g_byAFDiffTimes_Th)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

U8 IsCameraMove()
{
	U8 i,j;
	U16 wDiff;
	U8 byDiffBlock=0;
	U8 byCenterBlockDiffs=0;
	U8 byIsMoving;

	if((g_byAFStatus!=AF_ROUGH_SEARCH)&&(g_byAFStatus!=AF_FINE_SEARCH))
	{
		return 0;
	}

	
	for(i=0; i<5; i++)
	{
		for(j=0; j<5; j++)
		{
			wDiff = (U16)abs((S16)g_byAFStable_RMean[i][j]-(S16)g_byAWB_WinMeanR[i][j])
				  + (U16)abs((S16)g_byAFStable_GMean[i][j]-(S16)g_byAWB_WinMeanG[i][j])
			 	  + (U16)abs((S16)g_byAFStable_BMean[i][j]-(S16)g_byAWB_WinMeanB[i][j]);	
			if(wDiff>g_byAFMoveColor_Thd)
			{
				byDiffBlock ++;
				if((i>=1)&&(i<=4)&&(j>=1)&&(j<=4))
				{
					byCenterBlockDiffs ++;
				}
			}
		}
	}
	//update color info
	GetLastColor();

	if((byDiffBlock>g_byAFMoveBlockNum_Thd)||(byCenterBlockDiffs>=g_byAFMoveCenterBlockNum_Thd))
	{
		//camera is moving
		byIsMoving= 1;
		g_byIsMoving =1;
	}
	else
	{
		//not moving
		byIsMoving= 0;
		g_byIsMoving =0;
	}	

	if(1==byIsMoving)
	{
		g_byAFPosIndexLast2= g_byAFPosIndexLast1=g_byAFPosIndex;
	 	g_dwAFSharpLast3 = g_dwAFSharpLast2=g_dwAFSharpLast1=g_dwAFCurrentSharp;	
		g_byAFSerachDirectionChangeTimes = 0;
		g_byAFSearchTimes = 0;
		g_dwAFMaxSharpness=100; //if no sharpness >100,set vcm to 12;
		g_byAFMaxSharpPos =12; 	// set to near scene
		//g_byAFPosIndex=1;
		//g_bySearchingDir=1;
		g_byAFDirChangeTimes_Th=2;
		
		return 1;				
	}
	
	return 0;
}

void DelayBeforeStable()
{
	float fSharpdiff;
	if(g_byAFStatTime<3)// delay 3  times  before stable 
	{
		g_byAFStatTime++;
		fSharpdiff = (float)g_dwAFCurrentSharp/(float)g_dwAF_Stable_Sharpness;
		if((fSharpdiff>g_fAF_SharpDiff_High_Th2)||(fSharpdiff<g_fAF_SharpDiff_Low_Th2))
		{
			g_byAFStatus =AF_FINE_SEARCH;
		}
	}
	else	if(g_byAFStatTime==3)
	{
		g_byAFPosIndex=g_byAFPosIndexLast1;

		VCM_Write(g_wAFPos[g_byAFPosIndex]);
		g_byAFStatus = AF_AT_RIGHT_POSITION;	

	}	

}

void RestartAF(void)
{
	if((g_dwAFCurrentSharp<g_wAFRoughEntrance_Th)&&(g_byAFControl&AF_ROUGHSEARCH_EN)==AF_ROUGHSEARCH_EN)
	{
		g_byAFEnterRoughSearch = 1;
	}
	else
	{
		g_byAFEnterRoughSearch = 0;
	}

	g_byAFStatus = AF_JUDGE_DIRECTION;	
	g_dwAFMaxSharpness=100;
	g_byAFMaxSharpPos =12;
	g_byAFSerachDirectionChangeTimes = 0;
	g_byAFSearchTimes = 0;
	g_byAFDirChangeTimes_Th=2;

	JudgeAFDirection(0);
}

void AutoFocus()
{
	U8 byAFSharpOverThread,byColorDiffOverThread;
	U8 byRet;
	
	g_wAF_CurPosition= g_wAFPos[g_byAFPosIndex]; 

	if(1==IsCameraMove())
	{
		return;
	}
		
	switch(g_byAFStatus)
	{
		//rough search the focus position
		case AF_ROUGH_SEARCH :
			if(AFSharpHillSearch(AF_FINE_SEARCH)!=0 )
			{
				g_bySearchingDir = (g_bySearchingDir+1)%2; //turn direction
				
				g_byAFPosIndexLast2= g_byAFPosIndexLast1=g_byAFPosIndex;
			 	g_dwAFSharpLast3 = g_dwAFSharpLast2=g_dwAFSharpLast1=g_dwAFCurrentSharp;	
				g_byAFSearchStep =g_byAF_FineStep;
				g_byAFSerachDirectionChangeTimes = 0;
				g_byAFSearchTimes = 0;
				g_byAFDirChangeTimes_Th=1;
				g_byAFSharpPeakTh1=g_byAFSharpPeakTh1_Fine;
				g_byAFSharpPeakTh2=g_byAFSharpPeakTh2_Fine;

			}
			break;
		//fine search 	for more accurate position
		case AF_FINE_SEARCH :
			byRet = AFSharpHillSearch(AF_ADJUST_BEFORE_STABLE);
			if(byRet==1)
			{
				g_dwAF_Stable_Sharpness = g_dwAFCurrentSharp;
				g_byAFPosIndex=g_byAFPosIndexLast1;

				g_byAFStatTime=0;
			}
			else if(byRet==2)
			{
				g_byAFPosIndex=g_byAFMaxSharpPos;
				g_byAFStatTime=3;
			}
			break;
		case AF_ADJUST_BEFORE_STABLE:	
			//if AF_ADJUST_BEF_STABLE is open ,delay 3 frame before enter stable to make sure the sharpness changes very small
			if((g_byAFControl&AF_ADJUST_BEF_STABLE) ==AF_ADJUST_BEF_STABLE )
			{	
				DelayBeforeStable();
			}
			// else, directly write the stable position ,and enterAF_AT_RIGHT_POSITION
			else
			{
				VCM_Write(g_wAFPos[g_byAFPosIndex]);
				g_byAFStatus = AF_AT_RIGHT_POSITION;	
			}
			break;
		case AF_AT_RIGHT_POSITION :	
			{
				//update color info
				GetLastColor();
				//set stable postion and sharpness 
				g_byAF_Stable_Position = g_byAFPosIndex;
				g_dwAF_Stable_Sharpness = g_dwAFCurrentSharp;
				
				g_byAFStatTime2=0;
				g_byAFStatTime=0;
				g_byAFStatus = AF_STABLE;	

				break;
			}
		case AF_STABLE :
		{

			byAFSharpOverThread = 0;
			byColorDiffOverThread = 0;
			if((g_byAFControl&AF_SHARP_DIFF_EN) ==AF_SHARP_DIFF_EN )
			{
			 	byAFSharpOverThread = GetSharpDiff();
			}
			if((g_byAFControl&AF_COLOR_DIFF_EN) ==AF_COLOR_DIFF_EN )
			{
				byColorDiffOverThread = GetColorDiff();
			}

			//if((g_byAECStatus == AE_STABLE)||(ExposureTimeAutoItem.Last == EXPOSURE_TIME_AUTO_MOD_MANUAL))
			{
				// restart search focus postion
		       	if((byAFSharpOverThread==1)||(byColorDiffOverThread==1))
		       	{       			
		       		RestartAF();
		       	}
			}
			break;
		}
		// judge direction before search
		case AF_JUDGE_DIRECTION :
			JudgeAFDirection(1);
			break;
		default :
			break;
	}

}

void SetROIAFWindow(U16 wTop, U16 wLeft, U16 wBottom, U16 wRight)
{
#ifdef _RTK_EXTENDED_CTL_
	PostZoom2PreZoom(&wLeft, &wTop, &wRight, &wBottom);
#endif
	r_wISP_AF_START1_X = r_wISP_AF_START0_X = wLeft;
	r_wISP_AF_START1_Y = r_wISP_AF_START0_Y = wTop;
	r_wISP_AF_END1_X = r_wISP_AF_END0_X = wRight;
	r_wISP_AF_END1_Y = r_wISP_AF_END0_Y = wBottom;
}

#endif

#if (defined _AF_ENABLE_  || defined _SCENE_DETECTION_)

void GetFrameEdgeSharp()
{
#ifdef _SCENE_DETECTION_
/*
	U8 i,j;

	for (i=0;i<5;i++)
	{
		for (j=0;j<5;j++)
		{
			ASSIGN_INT(g_wASWinSharpLast[i][j], XBYTE[ISP_AF_SUM1_H+2*(5*i+j)], XBYTE[ISP_AF_SUM1_L+2*(5*i+j)]);
		}		
	}
*/	
#endif

#ifdef _AF_ENABLE_
     // mutiply by 4 is for center 9 window weight
	g_dwAFCurrentSharp = (r_dwISP_AF_SUM0-r_dwISP_AF_SUM1) + r_dwISP_AF_SUM1*4;
#endif

}

void SetAFWindow(U16 wWidth, U16 wHeight)
{
	U16 wWindow_width, wWindow_height;	

	// 5*5 AF statics window
	// full window exposure
	wWindow_width = wWidth/(U16)5;
	wWindow_height = wHeight/(U16)5;
	
	switch (4) 
	{
		case 0:	// 3/4
			wWindow_width = (wWindow_width*3)>>2;
			wWindow_height = (wWindow_height*3)>>2;
			break;
		case 1:	// 1/2
			wWindow_width = wWindow_width>>1;
			wWindow_height = wWindow_height>>1;
			break;
		case 2:	// 2/3
			wWindow_width = wWindow_width*2/3;
			wWindow_height = wWindow_height*2/3;
			break;
		case 3:	// 1/4
			wWindow_width = wWindow_width>>2;
			wWindow_height = wWindow_height>>2;
			break;
		case 4:	// 1/5
			wWindow_width = wWindow_width*4/5;
			wWindow_height = wWindow_height*4/5;
			break;
		default:	// undefined window			
			break;	
	}

	r_wISP_AF_START0_X = (wWidth-wWindow_width*5)>>1;
	r_wISP_AF_START0_Y = (wHeight-wWindow_height*5)>>1;
	r_wISP_AF_END0_X = r_wISP_AF_START0_X + wWindow_width*5;
	r_wISP_AF_END0_Y = r_wISP_AF_START0_Y + wWindow_height*5;

	r_wISP_AF_START1_X = r_wISP_AF_START0_X+wWindow_width;
	r_wISP_AF_START1_Y = r_wISP_AF_START0_Y+wWindow_height;
	r_wISP_AF_END1_X =r_wISP_AF_END0_X - wWindow_width;
	r_wISP_AF_END1_Y =r_wISP_AF_END0_Y - wWindow_height;

	return;
}

void InitAFSetting(U16 wWidth, U16 wHeight)
{
	SetAFWindow(wWidth,wHeight);
#ifdef _AF_ENABLE_
	VCM_Write(g_wAFPos[g_byAFPosIndex]);
#endif	
	//adjust edge detect threadhold ,if needed
	XBYTE[ISP_AF_THD] = 8;
	
}

void ISP_AFStaticsStart(void)
{
	// start statistic
#ifndef _SCENE_DETECTION_
	if (FocusAutoItem.Last == CTL_DEF_CT_FOCUS_AUTO)
	{
		// hemonel 2009-02-25: delete statis delay because hardware will do this.
		XBYTE[ISP_AF_CTRL] = ISP_AF_STAT_START;
	}
#else
	XBYTE[ISP_AF_CTRL] = ISP_AF_STAT_START;
#endif	
}

#endif

#if (defined _AF_TEST_ && defined _AF_ENABLE_)

void AF_TRAVERSAL()
{
	U32 dwAFSum0;
	U32 dwAFEdgeNum0;

	U32 dwAFSum1;
	U32 dwAFEdgeNum1;
	
	static U16 wPosition = 0;

	dwAFSum0 	= r_dwISP_AF_SUM0;
	dwAFEdgeNum0 = r_dwISP_AF_NUM0;
	dwAFSum1 	= r_dwISP_AF_SUM1;
	dwAFEdgeNum1 = r_dwISP_AF_NUM1;
	
	if (wPosition)
	{
		AF_MSG(("p:%u s0:%lx s1:%lx n0:%lx n1:%lx\n",
			wPosition, dwAFSum0, dwAFEdgeNum0, 
			dwAFSum1, dwAFEdgeNum1));
	}	

	if(wPosition<768)
	{
		wPosition += 2;
		VCM_Write(wPosition);
	}
	else
	{
		wPosition = 2;
	}	
}

#endif

