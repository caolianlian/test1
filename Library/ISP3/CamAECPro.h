#ifndef _CAMAECPRO_H_
#define _CAMAECPRO_H_

#ifdef _AE_NEW_SPEED_ENABLE_
#define AE_NORMAL_SPEED	0
#define AE_FAST_SPEED		1
#define AE_SUPER_SPEED		2

#define AE_NON_ACTIVE	0
#define AE_RISE_UP		1
#define AE_DROP_DOWN	2
#else
#define AEC_SLOW_MODE	0
#define AEC_FAST_MODE	1
#endif

//AEC Status
#define AE_STABLE 0x00
#define AE_LTLOWER 0x01
#define AE_GTUPPER 0x02
#define AE_FORCE	0x03
#define AE_MINEPTG	0x04
#define AE_MAXEPTG	0x05

#define AE_MANUAL_ADJUST 0x00
#define AE_AUTO_ADJUST	0x01

#define AE_SLOW_MODE 0x80
#define AE_HIGHLIGHT_MODE 0x40
#define AE_DARK_MODE 0x20

#define AE_STABLE_STATUS 0x00
#define AE_DELAY_STATUS 0x01
#define AE_NOSTABLE_STATUS 0x02
#define AE_MAXEPTG_STATUS 0x03
#define AE_MINEPTG_STATUS 0x04
#define AE_MANUAL_STATUS	0x05
#define AE_INITIAL_STATUS	0x06

//#define AE_HIST_ALGO		0x01
#define AE_AVERAGE_ALGO	0x00

// AEC Control
//#define AEC_ANTI_OVEREXP_EN		0x01
#define AEC_STABLE_DELAY_EN		0x02
#define AEC_SAMEBLOCK_CMP_EN		0x04
#define AEC_STABLE_RANGE_EXT_EN	0x08
#define AEC_DYNAMIC_AEMEANLOWBOUND_EN	0x10

void InitAECSetting(void);
void SetExpTime_Gain(float fAE_Value);
void AEC_SmoothAdjust(void);
void AE_AntiExp_Start_Static(void);
void AE_AntiExp_Static_Pro(void);
void SetISPAEGain(float fTotalGain, float fSnrGain, U8 byDelayFrm);
void AutoExposure(void);
void SetICAEGain(void);
void AE_BrightnessCompensationPro(void);
void ISP_AEStaticsStart(void);
void SetAECFaceExposure();
void SetROIAEWindow(U16 wTop, U16 wLeft, U16 wBottom, U16 wRight);
#endif