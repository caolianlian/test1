#ifndef _CAMAFPRO_H
#define _CAMAFPRO_H

//#include "pc_cam.h"

#define AF_MAX_STEP	16

//g_byAFControl
#define AF_SHARP_DIFF_EN 0x01
#define AF_COLOR_DIFF_EN 0x02
#define AF_MAX_SHARP_EN 0x04
#define AF_ROUGHSEARCH_EN 0x08
#define AF_ADJUST_BEF_STABLE 0x10

//byAFStatus
#define AF_JUDGE_DIRECTION 0x00
#define AF_AT_RIGHT_POSITION 0x01
#define AF_STABLE 0x02
#define AF_FINE_SEARCH 0x03
#define AF_ROUGH_SEARCH 0x04
#define AF_ADJUST_BEFORE_STABLE 0x05

//g_byAFStaticsAnaylseCtl
#define AF_TRAVERSAL_START	 0x01
#define AF_TRAVERSAL_END		 0x02
#define AF_SHARPNESS_READ		 0x04

void RestartAF(void);
void AutoFocus();
void ISP_AFStaticsStart(void);
void InitAFSetting(U16 wWidth, U16 wHeight);
void GetFrameEdgeSharp();
void AF_TRAVERSAL();
void GetLastColor(void);
void SetROIAFWindow(U16 wTop, U16 wLeft, U16 wBottom, U16 wRight);
#endif
