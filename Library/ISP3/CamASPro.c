#include "pc_cam.h"
#include "camutil.h"
#include "CamReg.h"
#include "CamUvc.h"
#include "CamCtl.h"
#include "camisp.h"
#include "CamSensor.h"
#include "CamI2C.h"
#include "CamProcess.h"
#include "Global_vars.h"
#include "ISP_vars.h"

#include "CamVdCfg.h"
#include "CamIspPro.h"
#include "CamASPro.h"
#include "ISP_TuningTable.h"

#ifdef _SCENE_DETECTION_

extern void SetNLSCRateOfLightSource(U8 ls);

void ASDetectWhiteScene()
{
	U8 i,j;
	U8 byWhite_Sum=0;
	U8 byWhite_Sum_Dark=0;

	for (i=0;i<5;i++)
	{
		for (j=0;j<5;j++)
		{
			//-----HD720 Mjpeg is noisy, be careful-----//
			if(g_wCurFrameWidth == 1280)
			{
				if (g_wASWinSharpLast[i][j] < g_byWhite_Edge_HD_TH)
				{
					byWhite_Sum++;
				}
			}
			else
			{
				if (g_wASWinSharpLast[i][j] < g_byWhite_Edge_TH)
				{
					byWhite_Sum++;
				}
			}

			if(g_wASWinSharpLast[i][j] < g_byWhite_Edge_Dark_TH)
			{
				byWhite_Sum_Dark++;
			}
		}
	}

	if(g_fAEC_EtGain<800)
	{
		if(byWhite_Sum> g_byWhite_Edge_Block_TH)
		{
			g_byWhiteScene = 1;
		}
		else
		{
			g_byWhiteScene = 0;
		}
	}
	else
	{
		if(byWhite_Sum_Dark> g_byWhite_Edge_Block_Dark_TH)
		{
			g_byWhiteScene = 1;
		}
		else
		{
			g_byWhiteScene = 0;
		}
	}
}

void ASDetectYellowScene()
{
#ifdef _NEW_AUTO_LSC_FUNCTION_	
	if(	((S16)g_byWinMeanR_BeforeLSC[2][2]-(S16)g_byWinMeanB_BeforeLSC[2][2]>25)&&
		((S16)g_byWinMeanR_BeforeLSC[0][0]-(S16)g_byWinMeanB_BeforeLSC[0][0]>16)&&
		((S16)g_byWinMeanR_BeforeLSC[4][4]-(S16)g_byWinMeanB_BeforeLSC[4][4]>16)&&
		((S16)g_byWinMeanR_BeforeLSC[0][4]-(S16)g_byWinMeanB_BeforeLSC[0][4]>15)&&
		((S16)g_byWinMeanR_BeforeLSC[4][0]-(S16)g_byWinMeanB_BeforeLSC[4][0]>15))
	{
		g_byYellowScene = 1;		
	}
	//Avoid concussion by some scene which RB delta is nearby the threshold 
	else if((((S16)g_byWinMeanR_BeforeLSC[2][2]-(S16)g_byWinMeanB_BeforeLSC[2][2])<10)||
		(((S16)g_byWinMeanR_BeforeLSC[0][0]-(S16)g_byWinMeanB_BeforeLSC[0][0])<10)||
		(((S16)g_byWinMeanR_BeforeLSC[4][4]-(S16)g_byWinMeanB_BeforeLSC[4][4])<10)||
		(((S16)g_byWinMeanR_BeforeLSC[0][4]-(S16)g_byWinMeanB_BeforeLSC[0][4])<10)||
		(((S16)g_byWinMeanR_BeforeLSC[4][0]-(S16)g_byWinMeanB_BeforeLSC[4][0])<10))
	{
		g_byYellowScene = 0;
	}
#else
	if(	((S16)g_byAWB_WinMeanR[2][2]-(S16)g_byAWB_WinMeanB[2][2]>25)&&
		((S16)g_byAWB_WinMeanR[0][0]-(S16)g_byAWB_WinMeanB[0][0]>16)&&
		((S16)g_byAWB_WinMeanR[4][4]-(S16)g_byAWB_WinMeanB[4][4]>16)&&
		((S16)g_byAWB_WinMeanR[0][4]-(S16)g_byAWB_WinMeanB[0][4]>15)&&
		((S16)g_byAWB_WinMeanR[4][0]-(S16)g_byAWB_WinMeanB[4][0]>15))
	{
		g_byYellowScene = 1;		
	}
	//Avoid concussion by some scene which RB delta is nearby the threshold 
	else if((((S16)g_byAWB_WinMeanR[2][2]-(S16)g_byAWB_WinMeanB[2][2])<10)||
		(((S16)g_byAWB_WinMeanR[0][0]-(S16)g_byAWB_WinMeanB[0][0])<10)||
		(((S16)g_byAWB_WinMeanR[4][4]-(S16)g_byAWB_WinMeanB[4][4])<10)||
		(((S16)g_byAWB_WinMeanR[0][4]-(S16)g_byAWB_WinMeanB[0][4])<10)||
		(((S16)g_byAWB_WinMeanR[4][0]-(S16)g_byAWB_WinMeanB[4][0])<10))
	{
		g_byYellowScene = 0;
	}
#endif	
}

void ASGetLightSource()
{
	if (g_byWhiteScene && g_byYellowScene)
	{
		g_byLastIllumCT = LIGHT_A_TEMPERATURE;
	}		
	else
	{
		g_byLastIllumCT = LIGHT_D65_TEMPERATURE;
	}			
}

#endif

/*#ifdef _SCENE_CHANGE_AE_BOUNDARY_
void ASChangeAEBoundary()
{
	U8 zeros,i;
	
	for (i=0,zeros=0;i<64;i++)
	{
		if(g_wAE_Hist_StNum[i]==0)
		{
			zeros++;
		}
	}

	if((g_byWhiteScene==1) && (zeros>45))
	{
		g_byAEMean_L_Hist_U	= ct_IQ_Table.ae.target.byMeanUR_L;
		g_byAEMean_H_Hist_U = ct_IQ_Table.ae.target.byMeanUR_H;
	}
	else
	{
		g_byAEMean_L_Hist_U	= ct_IQ_Table.ae.target.byMeanUR_L +g_byAddAE_U;
		g_byAEMean_H_Hist_U = ct_IQ_Table.ae.target.byMeanUR_H +g_byAddAE_U;	
	}
}
#endif*/

#ifdef _ENABLE_OUTDOOR_DETECT_
void OutdoorModeDetect()
{


	if((g_fAEC_EtGain<(float)g_byOutdoorModeEnter_Thd))
	{
		if(g_byExistFlicker==1)
		{
			g_byOutDoorScene=0;
		}
		else
		{
			if(g_bySceneChangeDelay==1)
			{
				return;
			}

			g_byOutdoorET++;
			g_byIndoorET=0;
			if((g_byOutdoorET>20))
			{
				g_byOutDoorScene=1;
				g_byOutdoorET=20;
			}
		}


		
	}
	else if(g_fAEC_EtGain>(float)g_byOutdoorModeExit_Thd)
	{
		g_byIndoorET++;
		g_byOutdoorET=0;
		if(g_byIndoorET>10)
		{
			g_byOutDoorScene=0;
			g_byIndoorET=10;

		}

		g_byExistFlicker=0;
	}
		
}

void AWBDynamicBoundarybyScene()
{
	if(g_byOutDoorScene==1)
	{
		g_byAWB_K3 = gc_AWBDynmaicBoundary.byAWB_K3;
		g_wAWB_B3 = gc_AWBDynmaicBoundary.wAWB_B3;
		g_byAWB_K4 = gc_AWBDynmaicBoundary.byAWB_K4;
		g_wAWB_B4 = gc_AWBDynmaicBoundary.wAWB_B4;
		g_byAWB_K5 = gc_AWBDynmaicBoundary.byAWB_K5;
		g_wAWB_B5 = gc_AWBDynmaicBoundary.wAWB_B5;
		g_byAWB_K6 = gc_AWBDynmaicBoundary.byAWB_K6;
		g_wAWB_B6= gc_AWBDynmaicBoundary.wAWB_B6;
	}
	else if(g_byOutDoorScene==0)
	{
		memcpy((U8*)g_aAWBRoughGain_R, (U8*)&ct_IQ_Table.awb.simple, sizeof(IQ_AWB_Simple_t));
	}

}

#endif


