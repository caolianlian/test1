#ifndef _CAMASPRO_H_
#define _CAMASPRO_H_ 

#define LIGHT_A_TEMPERATURE 			(0)
#define LIGHT_U30_TEMPERATURE 		(1)
#define LIGHT_CWF_TEMPERATURE 		(2)
#define LIGHT_D50_TEMPERATURE 		(3)
#define LIGHT_D65_TEMPERATURE 		(4)
#define LIGHT_D75_TEMPERATURE 		(5)

#define AS_ET_FLICK_LEVEL 50	//10000/120*0.6 = 50


void ASDetectWhiteScene();
void ASDetectYellowScene();
//void ASChangeAEBoundary();
void ASSetDynamicLSC(U8 byCT);
void ASGetLightSource();
#ifdef _ENABLE_OUTDOOR_DETECT_
void OutdoorModeDetect();
void  FlickerDetect();
#endif
#endif
