#include "pc_cam.h"
#include "camreg.h"
#include "global_vars.h"
#include "ISP_vars.h"
#include "GlobalAbsIspVar.h"
#include "GlobalISPRegVar.h"
#include "CamAECPro.h"
#include "CamAWBPro.h"
#include "CamAFPro.h"
//#include "CamIspPro.h"
#include "CamIsp.h"
#include "CamABPro.h"
#ifdef _RTK_EXTENDED_CTL_
#include "CamRtkExtIsp.h"
#endif

/*global vars initial function*/

//U8 code gc_byAE_Weight[5][5] = {{6,6,7,6,6,},{6,7,10,7,6,},{7,10,12,10,7,},{6,7,10,7,6,},{6,6,7,6,6,}};
//U8 code gc_byYgamma_LowLux[16]={32,55,75,90,102,112,122,130,138,153,165,177,188,207,225,240};
U8 code gc_byYgamma[16]= {0,8,16,24,32,40,48,56,64,80,96,112,128,160,192,224};
U8 code gc_byRGBGammaDef_RAW[28] = {0,16,32,43, 54, 62,70, 77,83, 95, 106, 115, 124, 132,140, 147,154, 161,167,174, 180,195,  201,211, 221,230, 239,247};
#ifdef _DECOLOR_TONE_ENABLE_
// color tone correction gamma 
U8 code gc_aCTC_Gamma[4][28]={
	{0, 4, 8, 12, 16, 20, 24, 28, 32, 40, 48, 56, 64, 72, 80, 88,   96,   104, 112, 120, 128, 144, 160, 176, 192, 208, 224, 240}, //AE no get down 
	{0,4,9,13,18,22,26,31,35,44,53,62,70,79,88,97,106,114,123,132,141,158,176,194,211,224,235,246},//AE get down to 90%
	{0, 5, 9, 14, 19, 23, 28, 33, 37, 47, 56, 67, 75, 84, 94, 103, 112, 122, 130, 139, 148, 165, 182, 198, 215, 226, 235, 246},//AE get down to 85%
	{0,5,10,15,20,25,30,35,40,50,60,70,80,90,100,110,120,130,140,150,160,180,200,215,225,233,241,249},//AE get down to 80%
};
#endif

U8 code gc_byAE_WinWeight[25]=
{
	4,4,4,4,4,
	4,5,5,5,4,
	4,5,5,5,4,
	4,5,5,5,4,
	4,4,4,4,4,
};

void Init_AB_GlobalVars()
{
#if ((defined _ENABLE_ANTI_FLICK_) ||(defined _ENABLE_OUTDOOR_DETECT_))
	g_byISPABStaticsEn = 0;
#endif
#if ((defined SUPPORT_DEFENT_OBJECT_MOVEMENT) ||(defined _ENABLE_OUTDOOR_DETECT_))
	g_byABStableBlockDiffTh = 6;
#endif

#ifdef _ENABLE_ANTI_FLICK_
	g_byABCurrentStep = 0;
	g_byABRatioMaxThreshold = 70;
	g_byABRatioThirdThreshold = 50;
	g_byABRatioSecThreshold = 50;
	g_byABRatioMinThreshold = 38;
	g_byABRatioThreshold = g_byABRatioMaxThreshold;
	g_byABDynamicDctCntSetting= 10;
	g_byABStaticDctCntSetting = 10;
	g_byABDctCntThreshold = 5;

	g_byABDummylines = 30;

	g_byABBypassThreshold = 25;
	g_byABStableBlockDiffTh = 6;

	g_byABDetectMinFps = 8;
#ifdef _FLICK_DETECT_GAIN_THRESHOLD_
	g_byABDetectMaxGain = 120;
#endif
	g_byABIFExceedFpsThreshold = 0;

	g_byABDetectCount = 0;
	g_byABDetectStatis = 0;

	g_byABIfFlickExist = 0;
	g_byABDetectType = DYNAMIC_FLICK_DETECT_STATE;
	g_byABCurPowerLine = PWR_LINE_FRQ_50;

	g_byABRatioSum = 0,
	g_byABLastRatioSum = 0;
#endif

}

void Init_AEC_GlobalVars()
{
	U8 i;

	//AEC
	//-------- AEC variable definition start----------
	g_byISPAEStaticsEn = 0;

	g_byAEC_Mode = 0;
	g_byAEForce_Adjust = 0;
	g_fSensorRowTimes = 1;
	g_fAEC_EtGain = 200;
	g_fAEC_Gain = 1.0;
	g_wAFRInsertDummylines = 0;
	g_fCurExpTime =0;

	// AEC stable delay
	g_byAE_StableOut_DelayCnt = 0;
	g_byAE_Delay_Cfg = 8;

	// AEC window
	g_wAEC_Fps_Coef = 64;	// select 4x gain for HP2011 FW 232

	// AEC threshold
	g_fAEC_Min_ExpTime = 100;

	// AEC threhold configure
	g_wAECGlobalgainmax = 192;//128;//96; 	// select 4x gain for HP2011 FW 232

	g_byAEC_HighLight_Thread = 90;
	g_byAEC_Backlight_Ratio = 20;//use AE_Hist Params  //Use AE_mean 8;/* param to adjust AE  Stable Region uppper*/	// hemonel 2010-06-22: modify from 16 to 8, because dell backlight compensation 0~3,default use 3

	g_byAEC_AdjustStatus = AE_INITIAL_STATUS;
	g_fAEC_SmoothExpectYmeanCmp_all = 1.0;

	// 5fps setting
	g_byAEMaxStep50Hz = 13;	// 100/FPS
	g_byAEMaxStep60Hz = 16;	// 120/FPS
	g_fAEC_Adjust_Th = 0.06;
	//g_byAEUnstableDiff = 6;

	//g_byAE_Algo_Select =  AE_HIST_ALGO;

	g_wYpercentage_Th_L = 40;			//30/1024
	g_wYpercentage_Th_H = 40;
	g_byAEC_Mean_Target_L = 60;
	g_byAEC_Mean_Target = 66;
	g_byAEC_Mean_Target_H = 80;
	g_byAEC_HistPos_Th_L = 10;
	g_byAEC_HistPos_Th_H = 190;

	g_fAEC_HighContrast_Exp_L = 0.4;
	
	//g_byAEMean_L_Hist = 50;//40;
	//g_byAEMean_H_Hist = 60;//45;
	//g_byAEMean_L_Hist_U = 90;
	//g_byAEMean_H_Hist_U = 100;
	//g_byAECStableYUpper = 60;	// Reck 2010-08-19: Use Hist AE params
	//g_byAECStableYLower = 70;	//  Reck 2010-08-19: Use Hist AE params

	//g_byAE_HistAlgo_Case = 0;
	g_byAEMeanValue = 0;



	g_byAEC_Stable_BlockDiffTh = 6;
	g_byAEC_Stable_BlockNumTh = 20;

	for (i=0; i<25; i++)
	{
		g_byAE_Stable_WinYMean[i]=0;
		g_byAE_WinYMean[i]= 0;
		g_byAE_WinWeight[i]=gc_byAE_WinWeight[i];
	}



	g_byAE_Hist_MeanTargetDec_Extent = 5;


	g_byAEC_Control =  AEC_STABLE_DELAY_EN | AEC_STABLE_RANGE_EXT_EN|AEC_DYNAMIC_AEMEANLOWBOUND_EN;

	// g_wAEC_Gain_Threshold_30fps, g_wAEC_Gain_Threshold_15fps adjust the threshold value of 30fps and 15fps,
	// if the exposure time exceed g_wAEC_Gain_Threshold_30fps's threshold value, we tune fps to 15,
	// and if the exposure time exceed g_wAEC_Gain_Threshold_15fps's threshold value, we tune fps to minimum
	g_wAEC_CurGain_Threshold_15fps = g_wAEC_Gain_Threshold_15fps = 96;
	g_wAEC_CurGain_Threshold_30fps = g_wAEC_Gain_Threshold_30fps = 96;

#ifdef _AE_NEW_SPEED_ENABLE_
	g_byAE_SpeedMode = AE_NORMAL_SPEED;
	g_byAE_LastStatus = AE_NON_ACTIVE;
	g_byAE_StaticsDelay = 0;
#else
	g_byAEC_AdjustMode = AEC_SLOW_MODE;
#endif

#ifdef _FACIAL_EXPOSURE_
	g_byAEFaceWindowSet=0;
	g_bySimilar_last=100;
	g_byFaceExposure_En=0;
	g_byFaceLostTime=0;
#endif
#ifdef _FACIALAEWINSET_XU_
	g_byXU_FacialAEWin_Set =0;
	for(i=0;i<7;i++)
	{
		g_byaFacial_AE_Parameters[i]=0;
	}
#endif
#ifdef _DECOLOR_TONE_ENABLE_
	 g_byAEDecreaseRate=15;
#endif

}

void Init_AWB_GlobalVars()
{
	U8 i,j;

	//AWB
	//-------- AWB variable definition start----------
	g_byAWB_K1 = 8;
	g_wAWB_B1 = 85;
	g_wAWB_B2 = 66;
	g_byAWB_K3 = 4;
	g_wAWB_B3 = 47;
	g_byAWB_K4 = -60;
	g_wAWB_B4 = 332;

	g_byAWB_K5 =50;
	g_wAWB_B5 = -50;

	g_byAWB_K6 =2;
	g_wAWB_B6 = 19;
	g_wAWB_B_Up = 105;
	g_wAWB_B_Down =47;
	g_swAWB_B_Left = 62;
	g_swAWB_B_Right=372;


	g_wAWB_RGB_SumTh = 650;	// hemonel 2011-08-25: becasue change the Ymean calucation formula

	g_aAWBRoughGain_R[0] = 28;
	g_aAWBRoughGain_R[1] = 32;
	g_aAWBRoughGain_R[2] = 36;
	g_aAWBRoughGain_R[3] = 40;
	g_aAWBRoughGain_R[4] = 44;
	g_aAWBRoughGain_R[5] = 48;
	g_aAWBRoughGain_B[0] = 48;
	g_aAWBRoughGain_B[1] = 44;
	g_aAWBRoughGain_B[2] = 40;
	g_aAWBRoughGain_B[3] = 36;
	g_aAWBRoughGain_B[4] = 32;
	g_aAWBRoughGain_B[5] = 28;

	g_byAWBFineMax_RG = 32+6;
	g_byAWBFineMin_RG=32-6;
	g_byAWBFineMax_BG= 32+6;
	g_byAWBFineMin_BG= 32-6;
	g_byAWBFineMax_Bright = 225;
	g_byAWBFineMin_Bright = 20;
	g_byISPAWBStaticsEn = 0;
	g_wAWBGainDiffTh	= 8;

	g_wTemPixelNumThread_L=5;


	g_byAWB_Win_Bright_Max = 230;
	g_byAWB_Win_Bright_Min = 0;
	g_wAWBRGain_Last = 0x100;
	g_wAWBGGain_Last = 0x100;
	g_wAWBBGain_Last = 0x100;
	g_wProjectGR = g_wAWBRGain_Last;
	g_wProjectGB = g_wAWBBGain_Last;
	g_wFtGB = 0x100;
	g_wFtGR = 0x100;
	g_byFtGainTh = 30;

	for (i=0; i<5; i++)
	{
		for(j=0; j<5; j++)
		{
			g_byAWBWin_Rgain_FirstProject[i][j]=0;
			g_byAWBWin_Bgain_FirstProject[i][j]=0;
			g_byAWBWin_Brightness[i][j]=0;
		}
	}

	g_byAWBFixed_YmeanTh =10;
	g_byAWB_Hold = 0;
	g_byAWB_SameBlock_Hold = 0;
	g_byAWBEnterStableDelay = 0;
	g_byAWBExitStableDelay=0;
	g_byAWBStableDelay_Cfg = 20; //100;


	g_byAWBDiffWindowsTh=12;
	g_byAWBColorDiff_Th=7;
	g_byAWB_State = AWB_UNSTABLE;

		g_byAWBGainStep=1;
	g_byAWBGainStep_Max=4;
	g_byAWBStepNumbers=30;

	g_byAWB_AdjustMode = AWB_SLOW_MODE;
}

void Init_AF_GlobalVars()
{
#ifdef _AF_ENABLE_

	// search parameter
	g_byAFStatus = AF_FINE_SEARCH;

	g_dwAFMaxSharpness=100; //if no sharpness >100,set vcm to 12;
	g_byAFMaxSharpPos =12; 	// set to near scene

	g_byAFSerachDirectionChangeTimes = 0;
	g_byAFSearchTimes = 0;

	g_bySearchingDir = 1;
	g_dwAFCurrentSharp=0;
	g_dwAFSharpLast1=0;
	g_dwAFSharpLast2=0;
	g_dwAFSharpLast3=0;
	g_wAF_CurPosition=260;
	g_byAFPosIndexLast2=g_byAFPosIndexLast1=g_byAFPosIndex=1;	

	// threshold parameter		
	g_byAFSharpPeakTh2=30;
	g_byAFSharpPeakTh1 =5;	
	g_fAF_SharpDiff_High_Th =1.3;
	g_fAF_SharpDiff_Low_Th = 0.7;
	g_byAF_ColorDiff_Th	=40;
	g_fAF_SharpDiff_High_Th2 =1.1;
	g_fAF_SharpDiff_Low_Th2 = 0.8;
	g_byAFSharpPeakTh1_Fine=10;	
	g_byAFSharpPeakTh2_Fine=32;
	g_byAFSharpPeakTh1_Rough=20;
	g_byAFSharpPeakTh2_Rough=50;	
	g_byAFStatTime2=0;	
	g_byAFStatTime=0;	

	g_byAF_FineStep =1;
	g_byAF_RoughStep=4;
	g_byAFDiffTimes_Th=5;
	g_byAFSearchStep = g_byAF_FineStep;
	g_byAFEnterRoughSearch =0;
	g_wAFRoughEntrance_Th=1200;
	g_byAFTotalSteps =AF_MAX_STEP;
	g_byAFDirChangeTimes_Th=2;
	g_byAFMoveBlockNum_Thd=12; 
	g_byAFMoveCenterBlockNum_Thd=4; 
	g_byAFMoveColor_Thd=16; 			


	g_byAFControl = AF_SHARP_DIFF_EN|AF_COLOR_DIFF_EN|AF_MAX_SHARP_EN/*|AF_ADJUST_BEFORE_STABLE|AF_ROUGHSEARCH_EN*/;	
#endif
}

void Init_OtherISP_GlobalVars()
{
	U8 i;

	//hdr
	g_byTgamma_rate_max=63;
	g_byTgamma_rate_min =20;

	//BLC
	g_wSensorWidthBefBLC = 1280;
	g_wSensorHeightBefBLC = 800;

	g_byoffsetR = 0;
	g_byoffsetG1 = 0;
	g_byoffsetG2 = 0;
	g_byoffsetB = 0;


	//LSC MLSC
	g_bySubResolution_For_LSC = 0;

	// denoise parameter.
	g_bySharpParamIndex_Last =0;
	g_bySharp_VIIR_Coef = 0x80;
	g_bySharp_HIIR_Coef = 0x88;
	g_bySharp_EdgeDct_Thd1 = 6;

	//CCM
	for (i=0; i<9; i++)
	{
		g_aCCM[i] =0;
		g_aCCM_Normal[i] = 0;
	}
	g_aCCM[0] = 1;
	g_aCCM[4] = 1;
	g_aCCM[8] = 1;
	g_aCCM_Normal[0] = 1;
	g_aCCM_Normal[4] = 1;
	g_aCCM_Normal[8] = 1;
	g_byCCMState = NO_CCM;

	//Gamma
	for (i=0; i<28; i++)
	{
		g_aGamma_Def[i] = gc_byRGBGammaDef_RAW[i];
#ifdef _DECOLOR_TONE_ENABLE_
		g_aCTC_Gamma[i] = gc_aCTC_Gamma[2][i]; //AE default get down to 85%
#endif

	}

	// zoom
	g_byCIFscaleParam_En = 1;

	// half sample
	g_bySetHalfSubSample_En = 1;
	g_byHalfSubSample_En = 0;

	// SPE
	g_bySharpness_Normal = 30;
	g_bySharpness_Def = 30;

	g_bySaturation_Def = 64;		// hemonel 2011-03-03: UV gain range become to 2^-6
	g_byContrast_Def = 32;

	// Y gamma
	for (i=0; i<16; i++)
	{
		g_Ygamma[i] = gc_byYgamma[i];
	}

	// U,V offset for preffered color
	g_byU_Offset_Normal = 0x22;
	g_byV_Offset_Normal = 0x26;

	// dynamic ISP control
	g_wDynamicISPEn =0;//DYNAMIC_LSC_EN | DYNAMIC_SHARPPARAM_EN |DYNAMIC_GAMMA_EN |DYNAMIC_CCM_CT_EN;
	g_byDynISP_FrameInterval = 0;


	// Scene detect
#ifdef _SCENE_DETECTION_
	g_byWhite_Edge_HD_TH = 150;
	g_byWhite_Edge_TH = 70;
	g_byWhite_Edge_Dark_TH = 40; 
	g_byWhite_Edge_Block_TH = 16;
	g_byWhite_Edge_Block_Dark_TH = 12;
	g_byAddAE_U = 7;
	g_byWhiteScene = 0;
	g_byYellowScene = 0;
	memset(g_wASWinSharpLast, 0 , 50);
	
#endif

#ifdef _ENABLE_OUTDOOR_DETECT_
	g_byOutdoorET=0;
	g_byIndoorET=0;
	g_byOutDoorScene=0;
	g_byOutdoorModeEnter_Thd= 40;//25;
	g_byOutdoorModeExit_Thd= 60;
	g_byExistFlicker=0;
#endif

	// others parameter
	g_byFirstStatic = 0;
	g_wSTnum = 0;

	// MJPEG
#ifdef _ENABLE_MJPEG_
	g_byQtableScale=6;
	//g_byQtableCurScale = 0;	
	//g_byJpeg_ACRA_En = 0;
#ifdef _JPEG_RATE_ADJ_	
	g_byQtableScaleGTHD = 20;
	g_byQtableScaleGTVGA = 12;
	g_byQtableScaleLEQVGA = 6;
#endif	
#endif
#ifdef _STILL_IMG_BACKUP_SETTING_
	g_byIsAEAWBFixed = 0;
#endif
	g_byFirstPreview = 0;

	g_byDynamicLSCSet=0;

	g_byLastIllumCT = 4;
	
	g_byHistEqLB = 0;
	g_byHistEqHB = 0;
	g_bBacklightFlag = 0;

#ifdef _NEW_AUTO_LSC_FUNCTION_
	g_byPre_ind=6;
	g_byind = 6;
	g_byind_count=0;
	g_wRGain_CT = g_wAWBRGain_Last;
	g_wBGain_CT = g_wAWBBGain_Last;
#endif
	
#ifdef _RTK_EXTENDED_CTL_
	g_byROIColorDiffTh = 14;
	g_byROIColorChangeBlockNumTh = 8;
	g_byROIDiffTimesTh = 5;
	
	g_byROIAEStatus = ROI_AE_QUIT;
#ifdef _AF_ENABLE_
	g_byROIAFStatus = ROI_AF_QUIT;
#endif	
	g_byROIStatus = ROI_QUIT;
#endif

#ifdef _RTK_EXTENDED_CTL_
	//ISO
	g_fAEC_ISO_gain_min = 0;
	g_fAEC_ISO_gain_max = 0;
	//ISO_MSG(("gain_max = %f, max_15fps = %f, max_30fps = %f\n",g_fAEC_ISO_gain_max,g_fAEC_ISO_gain_max_15fps,g_fAEC_ISO_gain_max_30fps));
#endif

#ifdef _LENOVO_IDEA_EYE_
	g_byMTDWinNCent= 30;
	g_byMTDSensCent= 30;
#endif

}

void Init_ISP_GlobalVars()
{
#ifdef _ENABLE_ANTI_FLICK_
	Init_AB_GlobalVars();
#endif

	Init_AEC_GlobalVars();
	Init_AWB_GlobalVars();
	Init_AF_GlobalVars();
	Init_OtherISP_GlobalVars();
}
