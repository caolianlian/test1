#include "pc_cam.h"
#include "camutil.h"
#include "CamReg.h"
#include "CamUvc.h"
#include "CamCtl.h"
#include "CamSensor.h"
//#include "CamI2C.h"
//#include "CamProcess.h"
#include "Global_vars.h"
#include "ISP_vars.h"
#include "CamVdCfg.h"
#include "CamIspPro.h"
#include "CamAECPro.h"
//#include "CamAWBPro.h"
#if  ((defined _ENABLE_ANTI_FLICK_ )||(defined _ENABLE_OUTDOOR_DETECT_))
#include "CamABPro.h"
#endif
#include "CamHDR.h"
#include "CamASPro.h"
#ifdef _RTK_EXTENDED_CTL_
#include "CamRtkExtIsp.h"
#endif

void NotifyHostMotionDetected(void);
void NotifyHostNoMotionDetected(void);
	

//Set AEC windows size (5*5 window)
void SetAECWindow(U16 wStart_X,U16 wStart_Y,U16 wWidth, U16 wHeight)
{
	U16 wEndX, wEndY;
	U8 bywidth,byheight;

	// 5*5 AE statics window
	// full window exposure
	bywidth =((wWidth/(U16)5)>>3);
	byheight=((wHeight/(U16)5)>>3);

	//start X
	XBYTE[ISP_AE_START_X_L] =INT2CHAR(wStart_X,0);
	XBYTE[ISP_AE_START_X_H] =INT2CHAR(wStart_X,1);

	//Start Y
	XBYTE[ISP_AE_START_Y_L] =INT2CHAR(wStart_Y,0);
	XBYTE[ISP_AE_START_Y_H] =INT2CHAR(wStart_Y,1);

	//Window Size
	XBYTE[ISP_AE_WIN_WIDTH] =bywidth;
	XBYTE[ISP_AE_WIN_HEIGHT] =byheight;

	//Histogram window must be same as AE window statics window for fix the ISP4.0/ISp4.1 bug
	XBYTE[ISP_AE_HWIN_START_X_L] = INT2CHAR(wStart_X,0);
	XBYTE[ISP_AE_HWIN_START_X_H] = INT2CHAR(wStart_X,1);
	XBYTE[ISP_AE_HWIN_START_Y_L] = INT2CHAR(wStart_Y,0);
	XBYTE[ISP_AE_HWIN_START_Y_H] = INT2CHAR(wStart_Y,1);

	wEndX = wStart_X + (U16)bywidth*40;
	wEndY = wStart_Y + (U16)byheight*40;
	XBYTE[ISP_AE_HWIN_END_X_L] = INT2CHAR(wEndX,0);
	XBYTE[ISP_AE_HWIN_END_X_H] = INT2CHAR(wEndX,1);
	XBYTE[ISP_AE_HWIN_END_Y_L] = INT2CHAR(wEndY,0);
	XBYTE[ISP_AE_HWIN_END_Y_H] = INT2CHAR(wEndY,1);

	g_wSTnum = (U16)bywidth*(U16)byheight;
}

#ifdef _FACIALAEWINSET_XU_
void FacialAEWinSet_XU()
{
	U8 h,w;
	U16 blockwidth;
	U16 blockheight;
	U16 wstartX;
	U16 wstartY;
	U8 byBlockNum;
	
	byBlockNum= g_byaFacial_AE_Parameters[2];	//g_byaFacial_AE_Parameters[2] is window index set by ap.
	if(byBlockNum==0)
	{
		SetAECWindow(0, 0,g_wSensorCurFormatWidth, g_wSensorCurFormatHeight);
	}
	else
	{
		h= (byBlockNum-1)/5;
		w= (byBlockNum-1)%5;

		blockwidth =  g_wSensorCurFormatWidth/5;
		blockheight = g_wSensorCurFormatHeight/5;
		wstartX = blockwidth*w;
		wstartY = blockheight*h;
		SetAECWindow(wstartX, wstartY,blockwidth, blockheight);
	}

}
#endif

#if ((defined _FACIAL_EXPOSURE_) || (defined _RTK_EXTENDED_CTL_))
static void SetAEWeightByWindow(U16 wWinStartX, U16 wWinStartY, U16 wWinEndX, U16 wWinEndY, U8 byAEWinWeight[])
{
	U16 ol_stx, ol_sty, ol_endx, ol_endy;	
	U16 blk_stx, blk_sty, blk_endx, blk_endy;
 	U16 blkw, blkh;
	U32 dwTotalArea, dwOverlapArea;
	U8 x, y, i;

	PostZoom2PreZoom(&wWinStartX, &wWinStartY, &wWinEndX, &wWinEndY);

	if(g_byHalfSubSample_En)
	{
		wWinStartX <<= 1;
		wWinStartY <<= 1;
		wWinEndX <<= 1;
		wWinEndY <<= 1;
	}

	dwTotalArea = ((U32)(wWinEndX-wWinStartX))*((U32)(wWinEndY-wWinStartY));
	blkw = g_wSensorCurFormatWidth/5;
	blkh = g_wSensorCurFormatHeight/5;

	for(y = 0, i = 0; y < 5; y++)
	{
		blk_sty = y*blkh;
		blk_endy = blk_sty + blkh;
		ol_sty = (blk_sty > wWinStartY) ? blk_sty : wWinStartY;
		ol_endy = (blk_endy < wWinEndY) ? blk_endy : wWinEndY;
		
		for(x = 0; x < 5; x++, ++i)
		{
			//block start and end 
			blk_stx = x*blkw;
			blk_endx = blk_stx + blkw;

			//overlap region start and end 
			ol_stx = (blk_stx > wWinStartX) ? blk_stx : wWinStartX;
			ol_endx = (blk_endx < wWinEndX) ? blk_endx : wWinEndX;

			//judge whether the block and face region have overlap region?
			if((ol_endx > ol_stx) && (ol_endy > ol_sty))
			{
				dwOverlapArea = ((U32)(ol_endx-ol_stx))*((U32)(ol_endy-ol_sty));
				byAEWinWeight[i] = dwOverlapArea*100/dwTotalArea;
			}
			else
			{
				byAEWinWeight[i] = 0;
			}
		}
	}
}
#endif

 #ifdef _FACIAL_EXPOSURE_
//if face detection function open ,AE window size is set according to detected face region  :zhangbo 20110908
void SetAECFaceExposure()
{
	U8 x;
	U32 dwFaceArea;
	U32 dwOverlapArea;
 	U16 blkw,blkh;

	U32	dwGlobalSum ;
	U32	dwFaceSum;
	U16	wWeightSum;
	U8 byAE_WinWeight[25];
	
	if((g_bySimilar_last>80)&&(g_bySimilar>80))
	{
		if((g_wAE_WinWidth==g_wCurFrameWidth)&&(g_wAE_WinHeight==g_wCurFrameHeight))
		{
			g_byFaceAEmode=0;
			// jack_hu 12-08-16: AE4.0 has changed the parameters
			g_byAEC_Mean_Target_L = ct_IQ_Table.ae.target.byMeanTarget_L;
			g_byAEC_Mean_Target = ct_IQ_Table.ae.target.byMeanTarget;
			g_byAEC_Mean_Target_H = ct_IQ_Table.ae.target.byMeanTarget_H;

			for (x=0; x<25; x++)
			{
				g_byAE_WinWeight[x]=gc_byAE_WinWeight[x];
			}

			g_byAEC_Control &= (~AEC_SAMEBLOCK_CMP_EN);
			g_byFaceLostTime++;
			if(g_byFaceLostTime>2)
			{
				g_byFaceLostTime=2;
			}
			if(g_byFaceLostTime==1)
			{
				g_byAE_Delay_Cfg=255;
			}
			else
			{
				g_byAE_Delay_Cfg=30;
			}
		}
		else
		{
			g_byFaceAEmode=1;
			g_byFaceLostTime=0;
			// jack_hu 12-08-16: AE4.0 has changed the parameters
			g_byAEC_Control &= (~AEC_SAMEBLOCK_CMP_EN);
			g_byAE_Delay_Cfg=255;

			SetAEWeightByWindow(g_wAE_WinStartX, g_wAE_WinStartY, g_wAE_WinStartX+g_wAE_WinWidth, g_wAE_WinStartY+g_wAE_WinHeight, byAE_WinWeight)	

			dwGlobalSum =0;
			dwFaceSum =0;
			wWeightSum=0;
				
			for(x=0;x<25;x++)
			{
				dwGlobalSum += g_byAE_WinYMean[x];
				dwFaceSum += (U16)(g_byAE_WinYMean[x])*(U16)(byAE_WinWeight[x]);
				wWeightSum +=(U16)(byAE_WinWeight[x]);
			}

			dwFaceSum = dwFaceSum/wWeightSum;
			dwGlobalSum = dwGlobalSum/25;
			
			if((dwGlobalSum > dwFaceSum+20) || (dwGlobalSum < dwFaceSum-20))
			{
				g_byAEC_Mean_Target_L = ct_IQ_Table.ae.target.byMeanTarget_L-16;
				g_byAEC_Mean_Target = ct_IQ_Table.ae.target.byMeanTarget-8;
				g_byAEC_Mean_Target_H = ct_IQ_Table.ae.target.byMeanTarget_H-8;

				for (x = 0; x < 25; x++)
				{
					g_byAE_WinWeight[x] = byAE_WinWeight[x];
				}					
			}
		}
	}
}
#endif

#ifdef _RTK_EXTENDED_CTL_
void SetROIAEWindow(U16 wTop, U16 wLeft, U16 wBottom, U16 wRight)
{
	SetAEWeightByWindow(wLeft, wTop, wRight, wBottom, g_byAE_WinWeight);
}
#endif

// ret  rowtime unit:0.1ms
static float Cal_sensor_rowtime(void)
{
	return ((float)g_wSensorHsyncWidth*(float)10000)/(float)g_dwPclk;     // unit microsecond
}

// unit: 0.1 ms
static float GetAECExposureTimeMax(U16 wExtraDummyLines)
{
	return ((float)(g_wAECExposureRowMax+ wExtraDummyLines) *g_fSensorRowTimes) ;
}

void InitAECSetting(void)
{
	XBYTE[AE_STATIS_LOC]= 0x00;

	// init hardware setting
//	SetAECWindow(0,0,wWidth, wHeight);

	// init AEC variable
	g_fSensorRowTimes = Cal_sensor_rowtime();	

	// use shift register for AE working delay compensation
	XBYTE[ISP_SHIFTREG_FSYNC_A] = 0x10;
	XBYTE[ISP_SHIFTREG_FSYNC_B] = 0x10;
	XBYTE[ISP_SHIFTREG_FSYNC_C] = 0x10;
	XBYTE[ISP_SHIFTREG_FSYNC_D] = 0x10;
	XBYTE[ISP_SHIFTREG_FSYNC_EN] = 0x01;

	XBYTE[ISP_AE_BRIGHT_COEF_R] = 5;
	XBYTE[ISP_AE_BRIGHT_COEF_G] = 6;
	XBYTE[ISP_AE_BRIGHT_COEF_B] = 5;
}

U8 DynamicAEmeanLowBoundary()
{
	U8 byAEmeanLowBoundary_Dec;
	U32 dwAEC_EtGain;

	dwAEC_EtGain = g_fAEC_EtGain;

	// construct function: y=(y2/log2(x2/x1)) * log2(x/x1)
	// y2 = 20, x1 = 800, x2 = 12800
	if(dwAEC_EtGain <=800)
	{
		byAEmeanLowBoundary_Dec =0;
	}
	else if(dwAEC_EtGain<= 1600)
	{
		byAEmeanLowBoundary_Dec =LinearIntp_Word(800,0, 1600,g_byAE_Hist_MeanTargetDec_Extent,dwAEC_EtGain);
	}
	else if(dwAEC_EtGain<= 3200)
	{
		byAEmeanLowBoundary_Dec =LinearIntp_Word(1600,g_byAE_Hist_MeanTargetDec_Extent, 3200,g_byAE_Hist_MeanTargetDec_Extent*2,dwAEC_EtGain);
	}
	else if(dwAEC_EtGain<= 6400)
	{
		byAEmeanLowBoundary_Dec =LinearIntp_Word(3200,g_byAE_Hist_MeanTargetDec_Extent*2, 6400,g_byAE_Hist_MeanTargetDec_Extent*3,dwAEC_EtGain);
	}
	else if(dwAEC_EtGain< 12800)
	{
		byAEmeanLowBoundary_Dec =LinearIntp_Word(6400,g_byAE_Hist_MeanTargetDec_Extent*3, 12800,g_byAE_Hist_MeanTargetDec_Extent*4,dwAEC_EtGain);
	}
	else
	{
		byAEmeanLowBoundary_Dec = g_byAE_Hist_MeanTargetDec_Extent*4;
	}

	return byAEmeanLowBoundary_Dec;
}

/*
*********************************************************************************************************
*											Get AE Statistics
* FUNCTION GetAEYmean
**********************************************************************************************************
*/
/*
  Get the 3 AE statistic values, which are the average Y value g_byAEMeanValue, the position of the
  histogram's light area g_byAEC_HistPos_H above which the ratio of pixels is A%, and the position of the
  histogram's dark area g_byAEC_HistPos_L below which the ratio of pixels is B%.

  \param None

  \retval None
*********************************************************************************************************
*/

static void GetAEYmean()
{
	S8 i;
	U32 dwWinSum;
	U16 wTemp;
	U16 wratio_L = 0;
	U16 wratio_H = 0;
	U16 wpixnum;
	U16 wSTnum;
	U16 wYsum;
	U16 wSumWeight;

	wSTnum = g_wSTnum*25;

	// AE compensation working delay
	// 		AE working delay usually is 3 frames. For speed up AE stable, decrease exposure time with two frame compensation
	//		and increase exposure time with three frame compensation.
	if(XBYTE[ISP_SHIFTREG_FSYNC_D]> 0x10)
	{
		g_fAEC_SmoothExpectYmeanCmp_all = (float)((U32)((U16)XBYTE[ISP_SHIFTREG_FSYNC_B] * (U16)XBYTE[ISP_SHIFTREG_FSYNC_C]) * (U32)XBYTE[ISP_SHIFTREG_FSYNC_D])/(float)4096;
	}
	else
	{
		g_fAEC_SmoothExpectYmeanCmp_all = (float)(((U16)XBYTE[ISP_SHIFTREG_FSYNC_B] * (U16)XBYTE[ISP_SHIFTREG_FSYNC_C]) )/(float)256;
	}

	//Leo ++ for dynamic Y percentage TH  start
	//get Historgram
	for (i = 0; i < 64; i++)
	{
		XBYTE[ISP_AE_ADDR]= (U8)i;
		ASSIGN_INT(g_wAE_Hist_StNum[i], XBYTE[ISP_AE_SUM_H], XBYTE[ISP_AE_SUM_L]);

		/*if (i<16)
		{
			dwHistEqLB2 = dwHistEqLB2 + (g_wAE_Hist_StNum[i]);
		}*/
	}

	/*dwHistEqLB2=dwHistEqLB2*100/wSTnum;

	if(g_byDynamicYpercentage==1)
	{
		if(g_fAEC_EtGain <= 500)//Disable the Dynamic Y% for AE control
		{
			byYprecen_flag = 0;
		}
		else
		{
			byYprecen_flag = 1;
		}

		if(byYprecen_flag==1)
		{
			if(dwHistEqLB2>25)
			{
				utemp = (U8)dwHistEqLB2;

				//g_wYpercentage_Th = LinearIntp_Byte_Bound(25, 40, 50, 160, utemp);
				byYpercentage_Th_Cur= LinearIntp_Byte_Bound(25, 40, 60, 140, utemp);

				if(byYpercentage_Th_Cur>g_byYpercentage_Th_Pre)
				{
					byYpercentage_Th_Diff = byYpercentage_Th_Cur-g_byYpercentage_Th_Pre;
				}
				else
				{
					byYpercentage_Th_Diff = g_byYpercentage_Th_Pre-byYpercentage_Th_Cur;
				}

				if(byYpercentage_Th_Diff >=40)
				{
					g_wYpercentage_Th = byYpercentage_Th_Cur;
					g_byYpercentage_Th_Pre = byYpercentage_Th_Cur;
				}
			}
			else
			{
				g_wYpercentage_Th = 40;
			}
		}
		else
		{
			g_wYpercentage_Th = 40;
		}
	}*/
	//Leo ++ for dynamic Y percentage TH  end

	//use AE window statics calculate Y average
	dwWinSum = 0;
	wSumWeight=0;
	for(i=0; i<25; i++)
	{
		XBYTE[ISP_AE_ADDR] = 64+(U8)i;
		ASSIGN_INT(wYsum, XBYTE[ISP_AE_SUM_H], XBYTE[ISP_AE_SUM_L]);
		g_byAE_WinYMean[i] = (((U32)wYsum)<<8)/(U32)g_wSTnum;
		dwWinSum += (U16)(g_byAE_WinYMean[i])*(U16)(g_byAE_WinWeight[i]);
		wSumWeight+= g_byAE_WinWeight[i];
	}
	g_byAEMeanValue = ClipWord(dwWinSum/wSumWeight,1,255);

	// hemonel 2012-02-24: add HDR function from LeoChou
	if ( (g_wDynamicISPEn&DYNAMIC_HDR_EN) == DYNAMIC_HDR_EN)
	{
		g_byAEMeanValue = ClipWord(((U16)g_byAEMeanValue+(U16)HdrJudge()),1,255);
	}

	// find the position of the histogram's light area
	wpixnum = ((U32)wSTnum*(U32)g_wYpercentage_Th_H) >> 10;
	wTemp =0;
	for (i = 63; i >= 0; i--)
	{
		wTemp += g_wAE_Hist_StNum[i];
		if (wTemp >= wpixnum)
		{
			wratio_H = wTemp;
			wratio_L = wTemp - g_wAE_Hist_StNum[i];
			break;
		}
	}
	g_byAEC_HistPos_H = LinearIntp_Word(wratio_H,((U8)i)<<2, wratio_L,(U16)(((U8)i)<<2)+4,wpixnum);

	// jack hu 2012-08-16: find the position of the histogram's dark area
	wpixnum = ((U32)wSTnum*(U32)g_wYpercentage_Th_L) >> 10;
	wTemp = 0;
	for(i = 0; i < 64; i++)
	{
		wTemp += g_wAE_Hist_StNum[i];
		if(wTemp >= wpixnum)
		{
			wratio_H = wTemp;
			wratio_L = wTemp - g_wAE_Hist_StNum[i];
			break;
		}
	}
	
	g_byAEC_HistPos_L = LinearIntp_Word(wratio_L, ((U8)i)<<2, wratio_H, (U16)(((U8)i)<<2)+4, wpixnum);
	if(g_byAEC_HistPos_L == 0)
	{
		g_byAEC_HistPos_L = 1;
	}
	
	// dynamic AE mean target
	if(g_byAEC_Control&AEC_DYNAMIC_AEMEANLOWBOUND_EN)
	{
		g_byAE_OverExpTargetDecVal = DynamicAEmeanLowBoundary();
		g_byAEMeanValue = ClipWord((U16)g_byAEMeanValue+(U16)g_byAE_OverExpTargetDecVal, 0, 255);
	}
	ISO_MSG(("g_byAEMeanValue,%bx\n",g_byAEMeanValue));
}

void UpdateAECMeanBlock(void)
{
	U8 i;

	// update 5x5 block Ymean at AEC stable for same block judge
	for (i=0; i<25; i++)
	{
		g_byAE_Stable_WinYMean[i] = g_byAE_WinYMean[i];
	}
}

/*
*********************************************************************************************************
*										Judge the current AE status
* FUNCTION JudgeAE
**********************************************************************************************************
*/
/*
  Judge the current AE status which is used to determine whether to adjust AE.

  \param	fAdjStep	the current adjusting step of AE

  \retval	AE_STABLE_STATUS(0)		the AE has been stable and should not be adjusted
  		 	AE_DELAY_STATUS(1)		the AE adjusting should be delayed and do not adjust it now
  		 	AE_NONSTABLE_STATUS(2)	the AE is unstable and should be adjusted
  		 	AE_MINSTEP_STATUS(4)	the AE is at Highlight Mode
*********************************************************************************************************
*/
//judge if need to adjust AE
//return value: 0-stable situation;2-need to adjust AE in this frame;1-not stable, but do not adjust AE in this frame
//jack hu 2012-08-16: add an parameter of JudgeAE(), AE status is judged by fAdjStep
static U8 JudgeAE(float fAdjStep)
{
	float fAdjust_Th;

	if((g_byAEC_Control & AEC_STABLE_RANGE_EXT_EN) && (g_byAECStatus == AE_STABLE) && (g_fAEC_SmoothExpectYmeanCmp_all == 1.0))
	{
		fAdjust_Th = g_fAEC_Adjust_Th*1.5;
	}
	else
	{
		fAdjust_Th = g_fAEC_Adjust_Th;
	}
	
	if(fAdjStep > 1.0+fAdjust_Th)
	{
		g_byAECStatus = AE_LTLOWER;
		if(g_fAEC_SmoothExpectYmeanCmp_all < 1.0)
		{
			return AE_DELAY_STATUS;
		}
	}
	else if(fAdjStep < 1.0-fAdjust_Th)
	{
		g_byAECStatus = AE_GTUPPER;
		if(g_fAEC_SmoothExpectYmeanCmp_all > 1.0)
		{
			return AE_DELAY_STATUS;
		}
	}
	else
	{
		if(g_fAEC_SmoothExpectYmeanCmp_all == 1.0)
		{
			g_byAECStatus = AE_STABLE;
			g_byAE_StableOut_DelayCnt = g_byAE_Delay_Cfg;
			UpdateAECMeanBlock();
			return AE_STABLE_STATUS;
		}
		else
		{
			return AE_DELAY_STATUS;
		}
	}

	//HighLight Mode
	if ( (g_byAEC_Mode&AE_HIGHLIGHT_MODE) == AE_HIGHLIGHT_MODE )
	{
		if (g_byAECStatus == AE_GTUPPER)
		{
			// hemonel 2010-03-05 check: min_exptime use different thread between hight mode and normal mode
		//	if ((U32)g_fAEC_EtGain == g_wAEC_Min_ExpTime)
			if(fabs(g_fAEC_EtGain-g_fAEC_Min_ExpTime) < 0.0001) 
			{
				g_byAECStatus = AE_MINEPTG;
				return AE_MINEPTG_STATUS;
			}
		}
	}
	else
	{
		if (g_byAECStatus == AE_GTUPPER)
		{
		//	if ((U32)g_fAEC_EtGain == g_wAEC_Min_ExpTime)
			if(fabs(g_fAEC_EtGain-g_fAEC_Min_ExpTime) < 0.0001) 
			{
				if (g_byAEMeanValue > g_byAEC_HighLight_Thread)
				{
					g_byAEC_Mode |= AE_HIGHLIGHT_MODE;
				}
				else
				{
					g_byAECStatus = AE_MINEPTG;
					return AE_MINEPTG_STATUS;
				}
			}
		}
	}

	if(g_byAECStatus == AE_LTLOWER)
	{
#ifdef _RTK_EXTENDED_CTL_
		if(RtkExtISOItem.Last != ISO_AUTO)
		{
			if(g_fAEC_ISO_gain_max - g_fAEC_Gain < 0.0001)
			{
				g_byAECStatus = AE_MAXEPTG;
				return AE_MAXEPTG_STATUS;
			}
		}
		else
#endif
		{
			if(((float)g_wAECGlobalgainmax)/16.0 - g_fAEC_Gain < 0.0001)
			{
				g_byAECStatus = AE_MAXEPTG;
				return AE_MAXEPTG_STATUS;
			}
		}
	}

	return AE_NOSTABLE_STATUS;
}

/*
*********************************************************************************************************
*										Get the AE Adjust Step
* FUNCTION GetAEAdjStep
**********************************************************************************************************
*/
/*
  Calculate the AE adjust step according to the AE statistics

  \param none

  \retval 	the AE adjust step
*********************************************************************************************************
*/

static float GetAEAdjStep()
{
	U8 by_BackLight;
	float fMean_Adj, fMean_Adj_L, fMean_Adj_H, fHist_Adj_L, fHist_Adj_H;
	float fHistPos_LH, fFinal_Adj;

	by_BackLight = BackLightCompItem.Last*g_byAEC_Backlight_Ratio;
	fMean_Adj_L = ((float)(g_byAEC_Mean_Target_L + by_BackLight))/((float)g_byAEMeanValue);
	fMean_Adj_H = ((float)(g_byAEC_Mean_Target_H + by_BackLight))/((float)g_byAEMeanValue);

	if(((U16)g_byAEC_HistPos_Th_H)*((U16)g_byAEC_HistPos_L) < ((U16)g_byAEC_HistPos_Th_L)*((U16)g_byAEC_HistPos_H))
	{
		fHistPos_LH = pow((float)g_byAEC_HistPos_L, g_fAEC_HighContrast_Exp_L)*pow((float)g_byAEC_HistPos_H, (1.0f-g_fAEC_HighContrast_Exp_L));

		if(g_byAEForce_Adjust == 0 && (fHistPos_LH >= g_fAEC_HistPos_LH_Target - g_fAEC_HistPos_LH_Target_Th) && (fHistPos_LH <= g_fAEC_HistPos_LH_Target + g_fAEC_HistPos_LH_Target_Th))
		{
			fMean_Adj = 1.0;
		}
		else
		{
			fMean_Adj = g_fAEC_HistPos_LH_Target/fHistPos_LH;
		}
			
		fFinal_Adj = ClipFloat(fMean_Adj, fMean_Adj_L, fMean_Adj_H);
	}
	else
	{
		fMean_Adj = ((float)(g_byAEC_Mean_Target + by_BackLight))/((float)g_byAEMeanValue);
		fHist_Adj_H = ((float)g_byAEC_HistPos_Th_H)/((float)g_byAEC_HistPos_H);
	
		if(g_byAEForce_Adjust == 0 && (g_byAEC_HistPos_L >= g_byAEC_HistPos_Th_L-2) && (g_byAEC_HistPos_L <= g_byAEC_HistPos_Th_L+2))
		{
			fHist_Adj_L = 1.0;
		}
		else
		{
			fHist_Adj_L = ((float)g_byAEC_HistPos_Th_L)/((float)g_byAEC_HistPos_L);
		}

		fFinal_Adj = ClipFloat(ClipFloat(fMean_Adj, fHist_Adj_L, fHist_Adj_H), fMean_Adj_L, fMean_Adj_H);
	}

	return fFinal_Adj;
}

/*
*********************************************************************************************************
*										Get the Number of Dummy Lines
* FUNCTION GetInsertDummy
**********************************************************************************************************
*/
/*
  This function is to choose an appropriate number of dummy lines. AE will dynamically change fps according
  to the gain. Usually, the gain is larger, the noise is larger. When the gain is too large, AE will reduce
  fps to acquire larger exposure time and lower gain.

  \param	dw_EtGain	the exposure value which could be divided into the exposure time and the gain. When
  						ISO = manual, it's used as exposure time.

  \retval	the number of dummy lines
*********************************************************************************************************
*/
static U16 GetInsertDummy(U32 dw_EtGain)
{
	float fBandingTime;
	U8 minstep;
	U8 maxstep;
	U16 wTemp;
#ifndef _Discrate_Frame_Rate_
	U8 i;
	U8 step;
#else
	float step;
	float midstep;
#endif

	if(PwrLineFreqItem.Last == PWR_LINE_FRQ_50
#ifdef _ENABLE_ANTI_FLICK_
	        || (g_byABCurPowerLine==PWR_LINE_FRQ_50 && PwrLineFreqItem.Last == PWR_LINE_FRQ_DIS)
#endif
	  )
	{
		fBandingTime = 100.0;
		minstep = 100/g_byCurFps;	// minstep = 100/max fps
		maxstep = g_byAEMaxStep50Hz;
#ifdef _Discrate_Frame_Rate_
		midstep = 100.0/SENSOR_FPS_15;
#endif
	}
	else
	{
		fBandingTime = 10000.0/120;
		minstep = 120/g_byCurFps;	// minstep = 120/max fps
		maxstep = g_byAEMaxStep60Hz;
#ifdef _Discrate_Frame_Rate_
		midstep = 120.0/SENSOR_FPS_15;
#endif
	}

	// find the AE step
#ifndef _Discrate_Frame_Rate_
	step = minstep;
	for(i= maxstep-1; i>= minstep; i--)
	{
		// hemonel 2010-08-24: optimize because g_wAEC_Fps_Coef is usually set more than 32, so condition 2 is alaways true
		if((dw_EtGain >= i*fBandingTime*g_wAEC_Fps_Coef/16))
		{
			step = i+1;
			break;
		}
	}
#else
#ifdef _RTK_EXTENDED_CTL_
	if(RtkExtISOItem.Last == ISO_AUTO)
	{
#endif//#ifdef _RTK_EXTENDED_CTL_
		if (g_byCurFps > SENSOR_FPS_15)
		{

			//if dw_EtGain > g_wAEC_Gain_Threshold_15fps's threshold value, tune fps to min fps
			if(dw_EtGain > (midstep*fBandingTime*g_wAEC_CurGain_Threshold_15fps/16))
			{
				step = maxstep;
				//jack hu 2012-08-16: amend the two thresholds to avoid AE hunting caused by fps fluctuation
				g_wAEC_CurGain_Threshold_15fps = g_wAEC_Gain_Threshold_15fps*0.9;
				g_wAEC_CurGain_Threshold_30fps = g_wAEC_Gain_Threshold_30fps*0.83;
			}
			//if dw_EtGain > g_wAEC_Gain_Threshold_30fps's threshold value, tune fps to 15
			else if(dw_EtGain > (minstep*fBandingTime*g_wAEC_CurGain_Threshold_30fps/16))
			{
				step = midstep;
				//jack hu 2012-08-16: amend the two thresholds to avoid AE hunting caused by fps fluctuation
				g_wAEC_CurGain_Threshold_15fps = g_wAEC_Gain_Threshold_15fps;
				g_wAEC_CurGain_Threshold_30fps = g_wAEC_Gain_Threshold_30fps*0.83;
			}
			// else we keep 30 fps
			else
			{
				step = minstep;
				//jack hu 2012-08-16: amend the two thresholds to avoid AE hunting caused by fps fluctuation
				g_wAEC_CurGain_Threshold_15fps = g_wAEC_Gain_Threshold_15fps;
				g_wAEC_CurGain_Threshold_30fps = g_wAEC_Gain_Threshold_30fps;
			}
		}
		//if dw_EtGain > g_wAEC_Gain_Threshold_15fps's threshold value, tune fps to min fps
		else if (g_byCurFps >= SENSOR_FPS_8)
		{
			if(dw_EtGain > (minstep*fBandingTime*g_wAEC_CurGain_Threshold_15fps/16))
			{
				step = maxstep;
				//jack hu 2012-08-16: amend this thresholds to avoid AE hunting caused by fps fluctuation
				g_wAEC_CurGain_Threshold_15fps = g_wAEC_Gain_Threshold_15fps*0.83;
			}
			else
			{
				// else we keep current fps
				step = minstep;
				//jack hu 2012-08-16: amend this thresholds to avoid AE hunting caused by fps fluctuation
				g_wAEC_CurGain_Threshold_15fps = g_wAEC_Gain_Threshold_15fps;
			}
		}
		else
		{
			//  we keep current fps
			step = minstep;
		}
#ifdef _RTK_EXTENDED_CTL_
	}
	else//ISO manual
	{
		if (g_byCurFps > 15)
		{
			if(dw_EtGain > (midstep*fBandingTime))//15
			{
				step = maxstep;
			}
			else if(dw_EtGain > (minstep*fBandingTime))//30
			{
				step = midstep;
			}
			// else we keep 30 fps
			else
			{
				step = minstep;
			}
		}
		else if (g_byCurFps >= 8)
		{
			if(dw_EtGain > (minstep*fBandingTime))//15
			{
				step = maxstep;
			}
			else//30
			{
				// else we keep current fps
				step = minstep;
			}
		}
		else
		{
			//  we keep current fps
			step = minstep;
		}
	}
#endif //#ifdef _RTK_EXTENDED_CTL_
#endif

#ifdef _ENABLE_ANTI_FLICK_
	g_byABCurrentStep = step;
#endif

	fBandingTime *= step;	// exposure time
	wTemp = fBandingTime/g_fSensorRowTimes+1;
	if(wTemp > g_wAECExposureRowMax)
	{
		return (wTemp-g_wAECExposureRowMax);
	}
	else
	{
		return 0;
	}
}

/*
*********************************************************************************************************
*										Get Expoure Time
* FUNCTION GetSensorET
**********************************************************************************************************
*/
/*
  Calculate the total gain of AE.

  \param	fAE_Value	the exposure value which could be divided into the exposure time and the gain
  			Exp_time	the exposre time

  \retval	the total gain
*********************************************************************************************************
*/
static float GetSensorET(float f_EtGain)
{
	float fBandingTime;
	float fEtMax;
	float fEtMin;
	float fEt;
	U16 wEt;
	//U32 maxEt;
#ifdef _STATIC_FLICK_DETECT_
	U8 byMultiple;
#endif

	if(PwrLineFreqItem.Last == PWR_LINE_FRQ_DIS)
	{
#ifdef _ENABLE_ANTI_FLICK_
		//for auto banding, fBandingTime will be auto set, Peter Sun, 2011/05/24
		if (g_byABCurPowerLine == PWR_LINE_FRQ_60)
		{
			fBandingTime = 10000.0/120;
		}
		else
		{
			fBandingTime = 100.0;
		}
#else
		fBandingTime = g_fSensorRowTimes;	// limit AE min value = 2
#endif
	}
	else if(PwrLineFreqItem.Last == PWR_LINE_FRQ_50)
	{
		fBandingTime = 100.0;
	}
	else
	{
		fBandingTime = 10000.0/120;
	}
	
#ifdef _RTK_EXTENDED_CTL_
	if(RtkExtISOItem.Last == ISO_AUTO)
	{
#endif//#ifdef _RTK_EXTENDED_CTL_
		if ( (g_byAEC_Mode&AE_HIGHLIGHT_MODE)==AE_HIGHLIGHT_MODE )
		{
			if(f_EtGain >= fBandingTime)
			{
				// exit strong light mode
				g_byAEC_Mode &= ~AE_HIGHLIGHT_MODE;
			}
			else
			{
				// in strong light mode
				fBandingTime = g_fSensorRowTimes;
			}
		}

		g_fAEC_Min_ExpTime = fBandingTime;
		fEt = f_EtGain;
		
#ifdef _RTK_EXTENDED_CTL_
	}
	else//ISO manual
	{
		//ISO_MSG(("f_EtGain = %f, fBandingTime*g_fAEC_ISO_gain_min = %f\n",f_EtGain,fBandingTime*g_fAEC_ISO_gain_min));
		if ( (g_byAEC_Mode&AE_HIGHLIGHT_MODE)==AE_HIGHLIGHT_MODE )
		{
			if(f_EtGain > fBandingTime*g_fAEC_ISO_gain_min)
			{
				g_byAEC_Mode &= ~AE_HIGHLIGHT_MODE;
			}
			else
			{
				fBandingTime = g_fSensorRowTimes;
			}
		}

		g_fAEC_Min_ExpTime = fBandingTime*g_fAEC_ISO_gain_min;
		
		fEt = f_EtGain/g_fAEC_ISO_gain_min;
		//ISO_MSG(("fEt = %f\n",fEt));
	}
#endif
	fEtMin = fBandingTime;
	
	if(LowLightCompItem.Last != 0)
	{
		g_wAFRInsertDummylines = GetInsertDummy((U32)fEt);
	}
	else
	{
		g_wAFRInsertDummylines = 0;
	}
	fEtMax = GetAECExposureTimeMax(g_wAFRInsertDummylines);
	
	// clip dw_EtGain in wEtMin~ wEtMax
	//ISO_MSG(("fEt = %f\n",fEt));

	fEt = ClipFloat(fEt, fEtMin, fEtMax);

	//ISO_MSG(("fEt = %f, fEtMin = %f, fEtMax = %f\n",fEt,fEtMin, fEtMax));
	
	// calculate the number of bandingtime
	wEt =fEt/fBandingTime;		// hemonel 2010-08-24: fEt must use float because round integer will loss decimal,  such as 3.999 will round to 3, but really is 4

	// hemonel 2010-06-28: fix 60Hz strong mode bug
	if(wEt<1)
	{
		wEt =1;
	}
	//ISO_MSG(("wEt = %bx\n",(U8)wEt));

#ifdef _STATIC_FLICK_DETECT_
	if (PwrLineFreqItem.Last == PWR_LINE_FRQ_DIS)
	{
		if (g_byABCurPowerLine == PWR_LINE_FRQ_60) 
		{
			// (10000x/120)/100 = 5x/6
			byMultiple = wEt%6;
		}
		else
		{
			// 100x/(10000/120) = 6x/5
			byMultiple = wEt%5;
		}

		if (byMultiple)
		{
			if (!g_byABIfFlickExist)
			{
				AB_MSG(("not integral multiple\n"));

				g_byABIfFlickExist = FLICK_POSSIBLE_EXIST;

				g_byABStart = 0;
				g_byABDetectType = DYNAMIC_FLICK_DETECT_STATE;
				g_byABDetectCount = 0;
				g_byABDetectStatis = 0;
			}
			else
			{
				if  (g_byABDetectType == STATIC_FLICK_DETECT_STATE
				        ||g_byABDetectType == STATIC_FLICK_DOUBLE_DETECT_STATE)
				{
					g_wAFRInsertDummylines += g_byABDummylines;
					AB_MSG(("dls+ %bu\n", g_byABDummylines));
				}
			}
		}
		else
		{
			if (g_byABIfFlickExist)
			{
				AB_MSG(("integral multiple\n"));

				g_byABIfFlickExist = FLICK_MUST_NOT_EXIST;

				g_byABStart = 0;
				g_byABDetectType = DYNAMIC_FLICK_DETECT_STATE;
				g_byABDetectCount = 0;
				g_byABDetectStatis = 0;
			}
		}
	}
#endif

	// calculate the real exposure time
	//wEt = ((float)wEt * fBandingTime+0.5);	// round to near integer

	// hemonel 2010-06-28: fix 60Hz strong mode bug ( wEt != 0)
	//Reck Add 2010-08-06: fix AE precision bug (may cause wEt>wMax)
	//wEt = ClipWord(wEt, 1,(U16)fEtMax) ;

	fEt = ((float)wEt)*fBandingTime;
	//ISO_MSG(("fEt = %f\n",fEt));

	return fEt;
}

/*
*********************************************************************************************************
*										Get the Total Gain
* FUNCTION GetSensorGain
**********************************************************************************************************
*/
/*
  Calculate the total gain of AE.

  \param	fAE_Value	the exposure value which could be divided into the exposure time and the gain
  			Exp_time	the exposre time

  \retval	the total gain
*********************************************************************************************************
*/
static float GetSensorGain(float fAE_Value, float fExp_Time)
{	
	// hemonel 2010-04-01: fix set value and current value not eval
#ifdef _RTK_EXTENDED_CTL_
	if(RtkExtISOItem.Last == ISO_AUTO)
	{
		return ClipFloat(fAE_Value/((float)fExp_Time), (float)1.0, ((float)g_wAECGlobalgainmax)/(float)16.0);
	}
	else
	{
		//ISO_MSG(("Gain before clip: %f\n",fAE_Value/((float)fExp_Time)));
		return ClipFloat(fAE_Value/((float)fExp_Time), g_fAEC_ISO_gain_min, g_fAEC_ISO_gain_max);
	}
#else
	return ClipFloat(fAE_Value/((float)fExp_Time), (float)1.0, ((float)g_wAECGlobalgainmax)/(float)16.0);
#endif
}

/*
*********************************************************************************************************
*									Get and Set the ISP Digital Gain
* FUNCTION SetISPAEGain
**********************************************************************************************************
*/
/*
  Calculate and Set the ISP Digital Gain. The ISP Digital Gain is introduced by ISP4.0, on the purpose of
  improving the accuracy of the AE gain. Its accuracy is 1/256, which is more precise than the accuracy of
  most sensors' analog gain. The ISP gain and the sensor gain should be validated on the same frame.

  \param	fTotalGain	the total gain of AE
  			fSnrGain	the sensor gain
  			byDelayFrm	the number of the frames after which the ISP gain is validated

  \retval	none
*********************************************************************************************************
*/
void SetISPAEGain(float fTotalGain, float fSnrGain, U8 byDelayFrm)
{
	U16 wISPGain;

	wISPGain = (U16)(fTotalGain*256.0/fSnrGain);
	if(wISPGain>=0x1000)
	{
		wISPGain = 0x0fff;
	}
	ISO_MSG(("fTotalGain = %f . fSnrGain = %f\n", fTotalGain, fSnrGain));
	ISO_MSG(("ISP GAIN: %f\n\n", INT2CHAR(wISPGain, 1)+(float)INT2CHAR(wISPGain, 0)/256.0));

	XBYTE[ISP_RGB_AE_GAIN] = INT2CHAR(wISPGain, 1);
	XBYTE[ISP_RGB_AE_GAIN+1] = INT2CHAR(wISPGain, 0);
	XBYTE[ISP_RGB_AE_GAIN_CONTROL] = 0x80|byDelayFrm;
}

/*
*********************************************************************************************************
*										Set Exposure Time and Gain
* FUNCTION SetExpTime_Gain
**********************************************************************************************************
*/
/*
  Calculate and set the exposure time and gain. 

  \param	fAE_Value	the exposure value which could be divided into the exposure time and the gain

  \retval	none
*********************************************************************************************************
*/
void SetExpTime_Gain(float fAE_Value)
{
	float fExpTime;

	fExpTime = GetSensorET(fAE_Value);
	ISO_MSG(("fExpTime = %f\n",fExpTime));
	g_fAEC_Gain = GetSensorGain(fAE_Value, fExpTime);
	ISO_MSG(("Gain after clip: %f\n",g_fAEC_Gain));
	g_fAEC_EtGain = fExpTime*g_fAEC_Gain;
	ISO_MSG(("g_fAEC_EtGain: %f\n",g_fAEC_EtGain));
	SetSensorExposuretime_Gain(fExpTime, g_fAEC_Gain);
}

/*
*********************************************************************************************************
*									Judge the similarity between scenes
* FUNCTION JudgeAESen
**********************************************************************************************************
*/
/*
  Judge whether the current scene is similary with the scene of the last stable status. If true, the AE
  should not be adjusted until the YMean is too far away from the target. Else, the AE will be adjusted.

  \param	none

  \retval	1	the current scene is similary with the scene of the last stable status
  			0	the current scene is not similary with the scene of the last stable status
*********************************************************************************************************
*/
U8 JudgeAESen()
{
	S16 sdiff;
	U8 byblockcount;
	U8 i;

	byblockcount =0;
	for(i=0; i<25; i++)
	{
		sdiff = (S16)g_byAE_WinYMean[i]-(S16)g_byAE_Stable_WinYMean[i];
		if(abs(sdiff)<=g_byAEC_Stable_BlockDiffTh)
		{
			byblockcount++;
		}
	}

	if (byblockcount>=g_byAEC_Stable_BlockNumTh)
	{
		return 1;
	}
	return 0;
}

/*
*********************************************************************************************************
*											Auto Exposure Control
* FUNCTION AutoExposure
**********************************************************************************************************
*/
/*
  This function is process the AE statistical interrupt. Extract the AE statistic values, calculate the AE
  adjusting step, judge whether the AE should be adjusted, and adjusted the AE if needed.

  \param None

  \retval None
*********************************************************************************************************
*/

void AutoExposure(void)
{
	U8 by_adjust;
	float fAE_Value;
	U8 bySameblk;
	U16 wComp;
	float fAdjStep;
	
#ifdef _AE_NEW_SPEED_ENABLE_
	float fTemp;
#endif
#ifdef _LENOVO_IDEA_EYE_
	U16 wDiff, wDiffTh;
	U8 byMTDWinDiffNum = 0, byMTDWinTh;
	U8 i;
#endif

	bySameblk =0;
	GetAEYmean();
	
#if ((defined _DEFENT_OBJECT_MOVEMENT_) ||(defined _ENABLE_OUTDOOR_DETECT_) \
	|| (defined _LENOVO_IDEA_EYE_))
	memcpy(g_byABLastWinYMean, g_byABPresentWinYMean, sizeof(g_byABLastWinYMean));
	memcpy(g_byABPresentWinYMean, g_byAE_WinYMean, sizeof(g_byABPresentWinYMean));
#endif

#ifdef _LENOVO_IDEA_EYE_
	if (g_byMTDDetectBackend)
	{
		for (i=0;i<25;i++)
		{
#ifdef _LENOVO_IDEA_EYE_20_DEGREE		
			if ((i%5)==0 || ((i+1)%5)==0)
			{
				continue;
			}
#endif			
			wDiffTh = ((U16)g_byABLastWinYMean[i])*g_byMTDSensCent;
			wDiff = abs((S16)g_byABPresentWinYMean[i]-(S16)g_byABLastWinYMean[i]);
			wDiff *= 100;
	
			if (wDiff > wDiffTh)
			{
				byMTDWinDiffNum++;
				IDEAEYE_MSG(("%bu Diff_Y:%bu, %bu ", i, g_byABPresentWinYMean[i], g_byABLastWinYMean[i]));
				IDEAEYE_MSG(("%u, %u\n", wDiff, wDiffTh));						
			}
		}	

#ifdef _LENOVO_IDEA_EYE_20_DEGREE
		byMTDWinTh = (U8)((U16)g_byMTDWinNCent*4/25);
		byMTDWinTh = (U8)ClipWord(byMTDWinTh, 1, 16);
#else
		byMTDWinTh = (U8)((U16)g_byMTDWinNCent/4);
		byMTDWinTh = (U8)ClipWord(byMTDWinTh, 1, 25);
#endif

		IDEAEYE_MSG(("Th:%bu, DiffNum:%bu\n", byMTDWinTh, byMTDWinDiffNum));
		if(byMTDWinDiffNum >= byMTDWinTh)
		{
			NotifyHostMotionDetected();
			g_byMTDWinDetected = 1;
			IDEAEYE_MSG(("Detected\n"));
		}
		else
		{
			NotifyHostNoMotionDetected();
			IDEAEYE_MSG(("no motion\n"));
		}

	}	
#endif	

	if(ExposureTimeAutoItem.Last != EXPOSURE_TIME_AUTO_MOD_APERTPRO)
	{
#ifdef _AE_NEW_SPEED_ENABLE_
		g_byAE_SpeedMode = AE_NORMAL_SPEED;
		g_byAE_LastStatus = AE_NON_ACTIVE;
#endif
	}
	else
	{
		if(g_byAEC_Control&AEC_SAMEBLOCK_CMP_EN)
		{
			if (g_byAEC_AdjustStatus == AE_STABLE_STATUS)
			{
				bySameblk = JudgeAESen();
			}
		}
		
		// jack hu 2012-08-16: cancel dynamic AE boundary
//#ifdef _SCENE_CHANGE_AE_BOUNDARY_
		//Change AE boundary for lab test
		//ASChangeAEBoundary();
//#endif

		// jack_hu 2012-08-16: get AE Adjust Step
		fAdjStep = GetAEAdjStep();

		// jack_hu 2012-08-16: judge current AE state
		by_adjust = JudgeAE(fAdjStep);
		
		//After stable, AEC will decrease sensitivity
		//The following condition will not adjust AEC:
		// case 1: If delay time is not expired from stable to unstable,
		// case 2: If there are many same block with these block at stable.
		if (( (g_byAEC_Control&AEC_STABLE_DELAY_EN)&&(g_byAE_StableOut_DelayCnt>0))||(bySameblk == 1))
		{
#ifdef _FACIAL_EXPOSURE_
			if(g_byFaceAEmode==1)
			{
				by_adjust = AE_STABLE_STATUS;
			}
			else if((g_byAEMeanValue >25)&& (g_byAEMeanValue < 150))
			{
				by_adjust = AE_STABLE_STATUS;
			}
#else
			if((g_byAEMeanValue >25)&& (g_byAEMeanValue < 200))
			{
				by_adjust = AE_STABLE_STATUS;
			}
#endif
		}

		// If force adjust, please AEC must adjust at this time
		if(g_byAEForce_Adjust == 1)
		{
			g_byAE_StableOut_DelayCnt = 0;
			by_adjust = AE_NOSTABLE_STATUS;
			g_byAECStatus = AE_FORCE;
		}

#ifndef _AE_NEW_SPEED_ENABLE_
		if(g_byAEC_AdjustMode == AEC_SLOW_MODE)
		{
			if(fAdjStep > 2 || fAdjStep < 0.5)
			{
				g_byAEC_AdjustMode = AEC_FAST_MODE;
			}
		}
		else
		{
			if(by_adjust == AE_STABLE_STATUS)
			{
				g_byAEC_AdjustMode = AEC_SLOW_MODE;
			}
		}
#endif

		g_byAEC_AdjustStatus = by_adjust;
		if(by_adjust != AE_NOSTABLE_STATUS)
		{
#ifdef _AE_NEW_SPEED_ENABLE_
			g_byAE_SpeedMode = AE_NORMAL_SPEED;
			g_byAE_LastStatus = AE_NON_ACTIVE;
#endif
		}
		else
		{
			// smooth AE adjust step:
			//		Construct a AE adjust step function. This function is monotonic based on current brightness.
			//		But also this adjust step will not overshoot in most cases becasue exposure time and gain working delay.
#ifdef _AE_NEW_SPEED_ENABLE_
			if(fAdjStep < 1.0)
			{
				if(g_byAEMeanValue >= 252)
				{
					g_byAE_SpeedMode = AE_SUPER_SPEED;
					
					fAdjStep = pow(fAdjStep, 1.25);					
					fAE_Value = fAdjStep*g_fAEC_EtGain;
					g_fAEC_SetEtgain = fAE_Value;
				}
				else if((fAdjStep < 0.4)||((fAdjStep < 0.65)&&(g_byAE_LastStatus == AE_NON_ACTIVE)))
				{
					g_byAE_SpeedMode = AE_FAST_SPEED;
					
					fAdjStep= pow(fAdjStep, 1);
					fAE_Value = fAdjStep*g_fAEC_EtGain;
					g_fAEC_SetEtgain = fAE_Value;
				}
				else
				{
					g_byAE_SpeedMode = AE_NORMAL_SPEED;
					
					if((fAdjStep <= 0.7)&&(g_wAE_Hist_StNum[63] > g_wSTnum))
					{
						fAdjStep = pow(fAdjStep, 0.25);
					}					
					else
					{
						fTemp = fAdjStep - 1.0;
						fAdjStep = -0.5*fTemp*fTemp + 1.0;
					}
					
					if(fAdjStep > 0.985) fAdjStep = 0.985;
					fAE_Value = fAdjStep*g_fAEC_EtGain;
					g_fAEC_SetEtgain = fAdjStep*fAE_Value;
				}

				g_byAE_LastStatus = AE_DROP_DOWN;
			}
			else
			{
				if(fAdjStep > 3)
				{
					g_byAE_SpeedMode = AE_SUPER_SPEED;
					
					fAdjStep = pow(fAdjStep, 0.65);
					fAE_Value = fAdjStep*g_fAEC_EtGain;
					g_fAEC_SetEtgain = fAE_Value;
				}
				else if((fAdjStep > 2)&&(g_byAE_LastStatus == AE_NON_ACTIVE))
				{
					g_byAE_SpeedMode = AE_FAST_SPEED;
					
					fAdjStep= pow(fAdjStep, 0.8);
					fAE_Value = fAdjStep*g_fAEC_EtGain;
					g_fAEC_SetEtgain = fAE_Value;
				}
				else
				{
					g_byAE_SpeedMode = AE_NORMAL_SPEED;
					
					fTemp = 1.0/fAdjStep - 1.0;
					fAdjStep = 1.0/(-0.36*fTemp*fTemp + 1.0);
					if(fAdjStep < 1.015) fAdjStep = 1.015;
					fAE_Value = fAdjStep*g_fAEC_EtGain;
					g_fAEC_SetEtgain = fAdjStep*fAE_Value;
				}
				
				g_byAE_LastStatus = AE_RISE_UP;
			}
#else
			// smooth AE adjust step:
			//		Construct a AE adjust step function. This function is monotonic based on current brightness.
			//		But also this adjust step will not overshoot in most cases becasue exposure time and gain working delay.
			/*if(fAdjStep <1.0)
			{
				// decrease exposure time or gain

				// smooth AE step	at normal condition			
				if(fAdjStep > 0.7162)
				{
					fAdjStep= pow(fAdjStep, 0.2);
				}
				else if(fAdjStep > 0.4871)	//	if((fAdjStep <= 14.0/16)&&(fAdjStep > 12.0/16))
				{
					fAdjStep = 14.0/16; // 0.8;
				}					
				else if(fAdjStep > 0.3488)	//	else if(fAdjStep > 11.0/16)
				{
					fAdjStep = 12.0/16; // 0.6;
				}
				// enhance AE speed at too bright condition				
				else 	//	else if(fAdjStep < 10.5/16)
				{
					fAdjStep = 8.0/16; // 0.4;
				}					
			}
			else
			{
				// increase exposure time or gain

				// enhance AE speed at too dark condition	
				if(fAdjStep > 25)		
				{
					fAdjStep = 2.4; // 2; // 3.2;
				}
				// smooth AE step	at normal condition	
				else if(fAdjStep > 7.59)			// if(fAdjStep > 1.5)
				{
					fAdjStep = 1.8; // 1.5; // 2;
				}
				else if(fAdjStep > 2.48)		// if(fAdjStep > 1.2)
				{
					fAdjStep = 1.2;
				}
				else if(fAdjStep > 1.6)		// if(fAdjStep > 1.1)
				{
					fAdjStep = 1.1;
				}
				else
				{
					fAdjStep = pow(fAdjStep, 0.2);
				}
			}*/
			
			if(g_byAEC_AdjustMode == AEC_FAST_MODE)
			{
				if(fAdjStep <1.0)
				{
					// decrease exposure time or gain
					// smooth AE step	at normal condition
					if(fAdjStep > 0.7162)
					{
						fAdjStep= pow(fAdjStep, 0.125);
					}
					else if(fAdjStep > 0.3806) //if(fAdjStep > 0.4871)	//	if((fAdjStep <= 14.0/16)&&(fAdjStep > 12.0/16))
					{
						fAdjStep = 0.9; //14.0/16;
					}
					//else if(fAdjStep > 0.3488)	//	else if(fAdjStep > 11.0/16)
					//{
					//	fAdjStep =  //12.0/16;
					//}
					// enhance AE speed at too bright condition
					else 	//	else if(fAdjStep < 10.5/16)
					{
						fAdjStep = 0.78; //8.0/16;
					}
				}
				else
				{
					// increase exposure time or gain
	
					// enhance AE speed at too dark condition
					if(fAdjStep > 20) //if(fAdjStep > 25)
					{
						fAdjStep = 1.5;
					}
					// smooth AE step	at normal condition
					else if(fAdjStep > 3.2) //if(fAdjStep > 7.59)			// if(fAdjStep > 1.5)
					{
						fAdjStep = 1.22; // 1.5;
					}
					//else if(fAdjStep > 2.48)		// if(fAdjStep > 1.2)
					//{
					//	fAdjStep = 1.2;
					//}
					else if(fAdjStep > 1.48) //if(fAdjStep > 1.6)		// if(fAdjStep > 1.1)
					{
						fAdjStep = 1.08;
					}
					else
					{
						fAdjStep = pow(fAdjStep, 0.1);
					}
				}
			}
			else
			{
				if(fAdjStep < 1.0)
				{
					if(fAdjStep > 0.81)
					{
						fAdjStep = 0.99; //0.975;
					}
					else if(fAdjStep > 0.52)
					{
						fAdjStep = 0.95;
					}
					else 	//if(fAdjStep > 0.17)
					{
						fAdjStep = 0.88;
					}
					//else
					//{
					//	fAdjStep = 0.75;
					//}
				}
				else
				{	
					if(fAdjStep < 1.30)
					{
						fAdjStep = 1.01; // 1.025;
					}
					else if(fAdjStep < 1.93)
					{
						fAdjStep = 1.05;
					}
					else 	//if(fAdjStep < 3.93)
					{
						fAdjStep = 1.12;
					}
					//else
					//{
					//	fAdjStep = 1.25;
					//}
				}
			}

			// The exposure time adjust is split into two times, the first time at AE statics interrupt, the second time at next frame data transfer start
			fAE_Value = fAdjStep*g_fAEC_EtGain;	// the first adjust target
			g_fAEC_SetEtgain = fAdjStep*fAE_Value;	// the second adjust target
#endif

			if((fabs(fAE_Value-g_fAEC_EtGain) > 0.0001) || (g_byAEForce_Adjust == 1))
			{
				g_byAEForce_Adjust = 0;
				wComp = (U16)((fAE_Value*16)/g_fAEC_EtGain);		// the current exposure time step
				SetExpTime_Gain(fAE_Value);
				XBYTE[ISP_SHIFTREG_FSYNC_B] = ClipWord(wComp, 1, 255);	// save the current exposure time step into shift register for AE working delay compensation
			}
		}
	}
}

/*
*********************************************************************************************************
*											Auto Exposure Control
* FUNCTION AEC_SmoothAdjust
**********************************************************************************************************
*/
/*
  This function will be called at data start interrupt. Once an AE statistical interrupt occurs, AE will
  be adjusted twice. The first adjustment is in the function AutoExposure, and the second one is done by
  by this function.

  \param None

  \retval None
*********************************************************************************************************
*/
void AEC_SmoothAdjust(void)
{
	U16 wComp;

	// second exposure time adjust with the small step
	if((ExposureTimeAutoItem.Last == EXPOSURE_TIME_AUTO_MOD_APERTPRO) && (g_byAEC_AdjustStatus == AE_NOSTABLE_STATUS))
	{
		//if(g_fAEC_SetEtgain != g_fAEC_EtGain)
		if(fabs(g_fAEC_SetEtgain-g_fAEC_EtGain) > 0.0001)
		{
			wComp = (U16)((g_fAEC_SetEtgain*16)/g_fAEC_EtGain);
			SetExpTime_Gain(g_fAEC_SetEtgain);
			XBYTE[ISP_SHIFTREG_FSYNC_B] = ClipWord(wComp, 1, 255);
			g_fAEC_SetEtgain = g_fAEC_EtGain;
		}
	}
}

//Static Start
void ISP_AEStaticsStart(void)
{
	// AE start statics with two frame interval
	// 		When AE statics interrupt occur, clear frame end flag. And start next AE statics until next frame end flag
	// 		Hardware start new frame AE statics at four lines data recevied, hardware end AE statics at the several line time after ccs frame end

//	if( (XBYTE[ISP_INT_FLAG1]&ISP_DATA_END_INT)==ISP_DATA_END_INT)	// hemonel 2012-04-09: delete this code for enhance AE speed
																	//  AE statics with two frame interval at 30fps and with one frame interval at below 30fps
	{
	//	XBYTE[ISP_INT_FLAG1] = ISP_DATA_END_INT;
		
		if(g_byISPAEStaticsEn == 1)
		{
			// hemonel 2009-02-25: delete statis delay because hardware will do this.
			g_byISPAEStaticsEn = 0;	// set ST flag before HW start ST

			XBYTE[ISP_AE_CTRL] |= ISP_AE_STAT_START;
		}
	}
}
