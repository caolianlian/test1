#include "pc_cam.h"
#include "camutil.h"
#include "CamReg.h"
#include "CamUvc.h"
#include "CamCtl.h"
//#include "camisp.h"
//#include "CamSensor.h"
//#include "CamI2C.h"
//#include "CamProcess.h"
#include "Global_vars.h"
#include "ISP_vars.h"
#include "CamVdCfg.h"
//#include "CamIspPro.h"
#include "CamAECPro.h"
//#include "CamAWBPro.h"
#include "CamABPro.h"
#include "CamASPro.h"

#ifdef _ENABLE_ANTI_FLICK_
#ifdef _AUTOBANDING_FFT_TEST_
U8 code matlab_diff[] =
{
	1,0,1,-1,0,0,-1,0,0,0,-1,-1,0,-1,0,-1,-1,-1,-1,-2,-2,-1,-1,0,-2,-3,-2,-1,-2,-2,-1,-1,-1,-1,-1,-1,-1,-1,-1,0,-1,-1,
	-1,-1,-1,0,1,1,1,1,0,1,0,0,0,1,1,0,1,1,1,1,1,1,2,2,2,1,1,1,0,0,1,1,0,1,1,1,-1,0,0,0,1,0,-1,-1,-1,0,0,0,0,-1,-1,-1,
	0,-1,-1,-1,-2,-1,-1,-1,0,-1,0,-1,-1,-1,-1,-1,-1,-1,0,0,-1,0,0,-1,-1,0,1,0,0,0,0,0,0,0,1,0,1,0,1,2,1,2,2,2,1,1,1,2,
	1,1,1,1,0,1,1,0,1,0,0,0,1,1,1,1,0,0,0,-1,0,0,-1,0,0,-1,-1,-1,-1,-2,-1,-1,0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-2,-1,0,
	-1,0,-1,0,-1,0,0,0,1,1,1,0,1,2,1,1,1,1,0,1,0,1,1,1,1,1,0,2,1,0,1,2,2,1,0,0,1,1,1,0,0,-1,0,0,0,1,-1,-1,0,-1,-1,0,-1,
	0,0,0,-1,0,-1,-1,0,-1,-1,-2,-2
};

U8 change_order(U8 input)
{
	U8 t;
	U8 result;

	for(t=0; t<8; t++)
	{
		if((input&(1<<t))!=0)
			result|=(1<<(7-t));
		else
			result&=~(1<<(7-t));
	}

	return result;
}

void InitAutoBandingSetting(U16 width, U16 height)
{
	int t = 0;

	XBYTE[FLICK_HORIZONTAL_SAMPLE_STEP] = 0x28;
	XBYTE[FLICK_VERTICAL_SAMPLE_STEP] = 0x2d;
	XBYTE[FLICK_HORIZONTAL_SAMPLE_START_L] = 0;
	XBYTE[FLICK_HORIZONTAL_SAMPLE_START_H] = 0;
	XBYTE[FLICK_VERTICAL_SAMPLE_START_L] = 0;
	XBYTE[FLICK_VERTICAL_SAMPLE_START_H] = 0;
	XBYTE[FLICK_PRE_SHIFT_DATA_LEFT_BITS] = 6;

	for (t=0; t<256; t++)
	{
		XBYTE[FLICK_FLICK_MEM_DATA_0] = 0;
		XBYTE[FLICK_FLICK_MEM_DATA_1] = matlab_diff[t] << 2;
		XBYTE[FLICK_FLICK_MEM_DATA_2] = matlab_diff[t] >> 6;

		XBYTE[FLICK_FFT_RESULT_ADDRESS] = change_order(t);

		XBYTE[FLICK_MCU_RW_CTRL] |= FLICK_MCU_WRITE;

		if(!WaitTimeOut(FLICK_MCU_RW_CTRL, FLICK_MCU_READ_WRITE_DONE, 1, 10))
		{
			AB_MSG(("fft sum write failed\n"));
			return;
		}
	}

	XBYTE[FLICK_FFT_RESULT_CONTRL] |= 0x2;
	XBYTE[FLICK_CTRL] |= FLICK_STATIS_START;

	return;
}

void AutoBanding(void)
{
	U32 sumx = 0, sum127 = 0, result;
	U8 t1 = 0;

	AB_MSG(("AutoBanding\n"));

	if(XBYTE[FLICK_CTRL] & FLICK_FFT_ERROR)
	{
		AB_MSG(("fft calc error\n"));
		return;
	}

	ASSIGN_LONG(sum127, XBYTE[FLICK_FFT_RESUT_SUM2_127_4], XBYTE[FLICK_FFT_RESUT_SUM2_127_3],
	            XBYTE[FLICK_FFT_RESUT_SUM2_127_2], XBYTE[FLICK_FFT_RESUT_SUM2_127_1]);

	for (; t1<126; t1++)
	{
		XBYTE[FLICK_FFT_RESULT_ADDRESS] = t1+2;
		XBYTE[FLICK_MCU_RW_CTRL] |= FLICK_MCU_READ;

		if(!WaitTimeOut(FLICK_MCU_RW_CTRL, FLICK_MCU_READ_WRITE_DONE, 1, 10))
		{
			AB_MSG(("fft sum read failed\n"));
			return;
		}

		ASSIGN_LONG(result, 0, XBYTE[FLICK_FLICK_MEM_DATA_2], XBYTE[FLICK_FLICK_MEM_DATA_1],
		            XBYTE[FLICK_FLICK_MEM_DATA_0]);

		if (t1 < 6)	sumx += result;

		AB_MSG(("fftsum %03bu %03bu %03bu: %04lu\n", XBYTE[FLICK_FLICK_MEM_DATA_0], XBYTE[FLICK_FLICK_MEM_DATA_1],
		        XBYTE[FLICK_FLICK_MEM_DATA_2], result));
	}


	result = (sumx*100) / sum127;

	AB_MSG(("%04lu:%04lu:%02lu\n", sumx, sum127, result));

	return;
}

void ABStaticsStart(void)
{
}

#else
void GetABRatioThreshold()
{
	U8 BandingTime;

	if(	g_byABCurPowerLine == PWR_LINE_FRQ_60)
	{
		BandingTime = 120;
	}
	else
	{
		BandingTime = 100;
	}

#ifdef _FLICK_DETECT_FPS_THRESHOLD_
	if (g_byABCurrentStep > (BandingTime/g_byABDetectMinFps))
	{
		g_byABIFExceedFpsThreshold = 1;
		return;
	}
	else
	{
		g_byABIFExceedFpsThreshold = 0;

	}
#endif

	if (g_byABCurrentStep <(BandingTime/AB_FPS_THRESHOLD_MAX))
	{
		g_byABRatioThreshold = g_byABRatioMaxThreshold;
	}
	else if (g_byABCurrentStep <(BandingTime/AB_FPS_THRESHOLD_MID))
	{
		g_byABRatioThreshold = g_byABRatioThirdThreshold;
	}
	else if (g_byABCurrentStep < (BandingTime/AB_FPS_THRESHOLD_MIN))
	{
		g_byABRatioThreshold = g_byABRatioSecThreshold;

	}
	else
	{
		g_byABRatioThreshold = g_byABRatioMinThreshold;
	}

	AB_MSG(("th:%bu %bu\n", g_byABCurrentStep, g_byABRatioThreshold));
}
void StaticsStart_ForAB()
{
	if(PwrLineFreqItem.Last != PWR_LINE_FRQ_DIS)
	{
		return;
	}

	if (!g_byABStart && g_byAEC_AdjustStatus== AE_NOSTABLE_STATUS)
	{
		AB_MSG(("NOSTABLE to WAIT_AE\n"));
		g_byABDetectType = DYNAMIC_FLICK_DETECT_STATE;
		g_byABDetectStatis = 0;
		g_byABDetectCount = 0;

		g_byABStart = AB_START_WAIT_AE;

		return;
	}
	// we start fft calu after ae status become stable from nostable
	else if(g_byABStart==AB_START_WAIT_AE && g_byAEC_AdjustStatus== AE_STABLE_STATUS)
	{

#ifdef _FLICK_DETECT_FPS_THRESHOLD_
		if (g_byABIFExceedFpsThreshold)
		{
			AB_MSG(("low fps\n"));
			g_byABStart = AB_START_AE_INITIAL;
			return;
		}
#endif

#ifdef _FLICK_DETECT_GAIN_THRESHOLD_
		if (g_wAEC_Gain > g_byABDetectMaxGain)
		{
			AB_MSG(("big gain\n"));
			g_byABStart = AB_START_AE_INITIAL;
			return;
		}
#endif
		if (!g_byABIfFlickExist)
		{
			g_byABDetectType = DYNAMIC_FLICK_DETECT_STATE;
			g_byABDetectStatis = 0;
			g_byABDetectCount = 0;

			AB_MSG(("Flick must not exist\n"));
			g_byABStart = AB_START_AE_INITIAL;
			return;
		}
		AB_MSG(("WAIT_AE to START_RUN\n"));
		g_byABStart = AB_START_RUN;
	}
	else if (g_byABStart==AB_START_SKIP_AE_UNSTABLE
	         && g_byAEC_AdjustStatus== AE_NOSTABLE_STATUS)
	{
		AB_MSG(("SKIP_UNSTABLE to SKIP_STABLE\n"));
		g_byABStart = AB_START_SKIP_AE_STABLE;
		return;
	}
	else if (g_byABStart==AB_START_SKIP_AE_STABLE
	         && g_byAEC_AdjustStatus== AE_STABLE_STATUS)
	{
		AB_MSG(("SKIP_STABLE to AE_INITIAL\n"));
		g_byABStart = AB_START_AE_INITIAL;
		return;
	}

	if (g_byABStart == AB_START_RUN)
	{
		AB_MSG(("AB start run!\n"));

		g_byABRatioSum = 0;
		g_byABLastRatioSum = 0;

		g_byABStart = AB_START_AE_INITIAL;
		g_byABDetectType = DYNAMIC_FLICK_DETECT_STATE;

		g_byABDetectCount = g_byABDynamicDctCntSetting;
		g_byABDetectStatis = 0;
	}

	// we do fft calu only we are in ae stable status
	if (g_byAEC_AdjustStatus!= AE_STABLE_STATUS	)
	{
		return;
	}

	if(g_byISPABStaticsEn == 1)
	{
		if (!g_byABStart)
		{
			if( g_byABDetectCount)
			{
				if (XBYTE[FLICK_CTRL] & FLICK_IDLE)
				{
					g_byISPABStaticsEn = 0;
					XBYTE[FLICK_CTRL] |= FLICK_STATIS_START;
				}
				else
				{
					AB_MSG(("AB busy\n"));
				}
			}
		}
	}
}


void AutoBanding(void)
{
	//U32  sum127 = 0, result;
	U8  ratio;

#ifdef _DEFENT_OBJECT_MOVEMENT_
	if (JudgeObjectMovement())
	{
		AB_MSG(("AB bypass image\n"));
		return;
	}
#endif

	if(PwrLineFreqItem.Last != PWR_LINE_FRQ_DIS)
	{
		return;
	}

	if(XBYTE[FLICK_CTRL] & FLICK_FFT_ERROR)
	{
		AB_MSG(("fft calc error\n"));
		XBYTE[FLICK_CTRL] &= ~FLICK_FFT_ERROR;
		return;
	}

	if((XBYTE[FLICK_CTRL] & FLICK_IDLE)==0)
	{
		AB_MSG(("fft calc busy\n"));
		return;
	}

	if (g_byABDetectType== FLICK_DETECT_END_STATE)
	{
		AB_MSG(("fft end state\n"));
		g_byABStart = AB_START_AE_INITIAL;
		g_byABDetectType = DYNAMIC_FLICK_DETECT_STATE;
		g_byABDetectStatis = 0;
		g_byABIfFlickExist = FLICK_POSSIBLE_EXIST;
		g_byABDetectCount = 0;

		return;
	}

	GetABRatioThreshold();
	g_byABDetectCount--;

	ratio = GetBandRatio();

	if (ratio > g_byABRatioThreshold)
	{
//		AB_MSG(("%04lu:%04lu:%02bu:%02bu:%02bu\n", sumx, sum127, ratio, g_byABDetectStatis,g_byABRatioThreshold));
		g_byABDetectStatis++;
		g_byABRatioSum += ratio;
	}
	else
	{
		g_byABRatioSum = 0;
		g_byABDetectStatis=0;
	}

	if (g_byABDetectStatis>=g_byABDctCntThreshold)
	{
		if (g_byABCurPowerLine == PWR_LINE_FRQ_60)
		{
			g_byABCurPowerLine = PWR_LINE_FRQ_50;
		}
		else
		{
			g_byABCurPowerLine = PWR_LINE_FRQ_60;
		}

		if (g_byABDetectType == DYNAMIC_FLICK_DETECT_STATE)
		{
			AB_MSG(("DYNAMIC to DOUBLE_DETECT\n"));

			g_byABLastRatioSum = g_byABRatioSum;
			g_byABRatioSum = 0;
			g_byABDetectType = DYNAMIC_FLICK_DOUBLE_DETECT_STATE;
			g_byABDetectCount =	g_byABDynamicDctCntSetting;
		}
		// because of the lamplight or some other problems, we need do double fft check to
		// prevent erroneous judgement
		else if (g_byABDetectType == DYNAMIC_FLICK_DOUBLE_DETECT_STATE)
		{
			g_byABDetectType = FLICK_DETECT_END_STATE;
			if (g_byABRatioSum <= g_byABLastRatioSum)
			{
				if (g_byABCurPowerLine == PWR_LINE_FRQ_60)
				{
					g_byABCurPowerLine = PWR_LINE_FRQ_50;
				}
				else
				{
					g_byABCurPowerLine = PWR_LINE_FRQ_60;
				}			
				AB_MSG(("DYNAMIC_DOUBLE to END, %lu < %lu\n", g_byABRatioSum, g_byABLastRatioSum));
				return;
			}
			else
			{
				AB_MSG(("DYNAMIC_DOUBLE to END, %lu > %lu\n", g_byABRatioSum, g_byABLastRatioSum));
			}
		}
		// if no dynamic flick is found, we start static flick detect
		else if (g_byABDetectType == STATIC_FLICK_DETECT_STATE)
		{
			AB_MSG(("STATIC to STATIC_DOUBLE\n"));
			g_byABLastRatioSum = g_byABRatioSum;
			g_byABRatioSum = 0;
			g_byABDetectType = STATIC_FLICK_DOUBLE_DETECT_STATE;
			g_byABDetectCount =	g_byABStaticDctCntSetting;

		}
		// because of the lamplight or some other problems, we need do double fft check to
		// prevent erroneous judgement
		else if (g_byABDetectType == STATIC_FLICK_DOUBLE_DETECT_STATE)
		{
			g_byABDetectType = FLICK_DETECT_END_STATE;
			if (g_byABRatioSum <= g_byABLastRatioSum)
			{
				if (g_byABCurPowerLine == PWR_LINE_FRQ_60)
				{
					g_byABCurPowerLine = PWR_LINE_FRQ_50;
				}
				else
				{
					g_byABCurPowerLine = PWR_LINE_FRQ_60;
				}			
				AB_MSG(("STATIC_DOUBLE to END, %lu < %lu\n", g_byABRatioSum, g_byABLastRatioSum));
				return;
			}
			else
			{
				AB_MSG(("STATIC_DOUBLE to END, %lu > %lu\n", g_byABRatioSum, g_byABLastRatioSum));
			}
		}

		AB_MSG(("AB set pl:%buhz at state %bu\n",
		        g_byABCurPowerLine==PWR_LINE_FRQ_50?50:60, g_byABDetectType));

		g_byAEForce_Adjust= 0x1;
		g_byABStart = AB_START_SKIP_AE_UNSTABLE;

		g_byABDetectStatis= 0;

		return;
	}

#ifdef _STATIC_FLICK_DETECT_
	if(!g_byABDetectCount)
	{
		if (g_byABDetectType == DYNAMIC_FLICK_DETECT_STATE)
		{
			AB_MSG(("AB:DYNAMIC to STATIC\n"));
			g_byABDetectType = STATIC_FLICK_DETECT_STATE;
			g_byABDetectCount = g_byABStaticDctCntSetting;
			//force tune down fps little, 2011/5/27, Peter Sun
			g_byAEForce_Adjust=1;
			g_byABStart = AB_START_SKIP_AE_UNSTABLE;
			g_byABDetectStatis = 0;

		}
		else if ( g_byABDetectType == DYNAMIC_FLICK_DOUBLE_DETECT_STATE)
		{
			AB_MSG(("AB:DOUBLE_DETECT to END\n"));
			g_byABDetectType= FLICK_DETECT_END_STATE;
			g_byABDetectStatis = 0;
		}
		else if (g_byABDetectType == STATIC_FLICK_DETECT_STATE)
		{
			AB_MSG(("AB:STATIC to END\n"));
			g_byABDetectType= FLICK_DETECT_END_STATE;
			g_byAEForce_Adjust=1;
			g_byABStart = AB_START_SKIP_AE_UNSTABLE;
			g_byABDetectStatis = 0;
		}
		else if (g_byABDetectType == STATIC_FLICK_DOUBLE_DETECT_STATE)
		{
			AB_MSG(("AB:STATIC to END\n"));
			g_byABDetectType= FLICK_DETECT_END_STATE;
			g_byAEForce_Adjust=1;
			g_byABStart = AB_START_SKIP_AE_UNSTABLE;
			g_byABDetectStatis = 0;
		}
		else
		{
			AB_MSG(("AB:else to END\n"));
			g_byABDetectType= FLICK_DETECT_END_STATE;
		}
	}
#endif
	return;
}
#endif
#endif//ENABLE_ANTI_FLICK

#if ((defined _DEFENT_OBJECT_MOVEMENT_) ||(defined _ENABLE_OUTDOOR_DETECT_))
U8 JudgeObjectMovement(void)
{
	S16 sdiff;
	U8 byblockcount;
	U8 i;

	byblockcount =0;

	for(i=0; i<25; i++)
	{
		sdiff = (S16)g_byABPresentWinYMean[i]-(S16)g_byABLastWinYMean[i];
		if(abs(sdiff)<=g_byABStableBlockDiffTh)
		{
			byblockcount++;
		}
	}

	//AB_MSG(("OM:%bu %bu\n", byblockcount, g_byABStableBlockDiffTh));

	//use Y average diff to decide if object move
	if (byblockcount < g_byABBypassThreshold)
	{
		return 1;
	}
	else
	{
		return 0;
	}

}
#endif

#ifdef _ENABLE_OUTDOOR_DETECT_
// if flicker exist ,the scene must be indoor ;
//else maybe outdoor 
void  FlickerDetect()
{
	U8  ratio;

	if (JudgeObjectMovement())
	{
		g_bySceneChangeDelay =1;
		AB_MSG(("AB bypass image\n"));
		return;
	}
	else
	{
		g_bySceneChangeDelay =0;
	}		

	ratio = GetBandRatio();
	
	if (ratio > 60)
	{
		g_byFlickerDetectCnt++;
		g_byFlickerUnDetectCnt=0;
		g_bySceneChangeDelay =1;
	}
	else
	{
		g_byFlickerUnDetectCnt++;
		g_byFlickerDetectCnt=0;
		g_bySceneChangeDelay =0;

	}

	if (g_byFlickerDetectCnt>5)
	{
		g_bySceneChangeDelay =0;

		g_byExistFlicker =1;
	}
	else if(g_byFlickerUnDetectCnt>2)
	{
		g_byExistFlicker=0;
	}
}

void StaticsStart_ForFlickerDetect()
{
	if(g_byISPABStaticsEn == 1)
	{
		if (XBYTE[FLICK_CTRL] & FLICK_IDLE)
		{
			g_byISPABStaticsEn = 0;
			XBYTE[FLICK_CTRL] |= FLICK_STATIS_START;
		}
		else
		{
			AB_MSG(("AB busy\n"));
		}
	}

}

#endif//_ENABLE_OUTDOOR_DETECT_
#if ((defined _ENABLE_ANTI_FLICK_) ||(defined _ENABLE_OUTDOOR_DETECT_))
void InitAutoBandingSetting(U16 width, U16 height)
{
#if  ((defined _ENABLE_ANTI_FLICK_ )||(defined _ENABLE_OUTDOOR_DETECT_))

	float a;
	U8 b;

	//AB_MSG(("InitAutoBandingSetting w:%u h:%u\n", width, height));

	if (XBYTE[ISP_CONTROL2] & ISP_HALFSAMPLE_EN) {
		width = width >> 1;
		height = height >> 1;
	}

	a = (width-80)/512.0;
	b = (U8)a;
	XBYTE[FLICK_HORIZONTAL_SAMPLE_STEP] = b<<4;
	b = (a-b)*10;
	XBYTE[FLICK_HORIZONTAL_SAMPLE_STEP] |= b;

	//deduce 80 to prevent fft calu error because of sampling limit
	a = (height-80)/256.0;
	b = (U8)a;
	XBYTE[FLICK_VERTICAL_SAMPLE_STEP] = b<<4;
	b = (a-b)*10;
	XBYTE[FLICK_VERTICAL_SAMPLE_STEP] |= b;

	XBYTE[FLICK_HORIZONTAL_SAMPLE_START_L] = 0;
	XBYTE[FLICK_HORIZONTAL_SAMPLE_START_H] = 0;
	XBYTE[FLICK_VERTICAL_SAMPLE_START_L] = 0;
	XBYTE[FLICK_VERTICAL_SAMPLE_START_H] = 0;
	XBYTE[FLICK_PRE_SHIFT_DATA_LEFT_BITS] = 6;
#endif

#ifdef _ENABLE_ANTI_FLICK_

	g_byABStart = AB_START_AE_INITIAL;
	g_byABDetectType = DYNAMIC_FLICK_DETECT_STATE;
	g_byABDetectStatis = 0;
	g_byABIfFlickExist = FLICK_POSSIBLE_EXIST;

	if (PwrLineFreqItem.Last == PWR_LINE_FRQ_DIS)
	{
		g_byABDetectCount =	g_byABDynamicDctCntSetting;
	}
	else
	{
		g_byABDetectCount = 0;
	}

	g_byABRatioSum = 0;
	g_byABLastRatioSum = 0;
#endif 

	return;
}


U8 GetBandRatio(void)
{
	U32 sumx = 0, sum127 = 0, result;
	U8 t1 = 0;
	U8 ratio;
	ASSIGN_LONG(sum127, XBYTE[FLICK_FFT_RESUT_SUM2_127_4], XBYTE[FLICK_FFT_RESUT_SUM2_127_3],
	            XBYTE[FLICK_FFT_RESUT_SUM2_127_2], XBYTE[FLICK_FFT_RESUT_SUM2_127_1]);

	for (; t1<Anti_Flick_BANDING_POINT_COUNT; t1++)
	{
		XBYTE[FLICK_FFT_RESULT_ADDRESS] = t1+2;
		XBYTE[FLICK_MCU_RW_CTRL] |= FLICK_MCU_READ;

		if(!WaitTimeOut(FLICK_MCU_RW_CTRL, FLICK_MCU_READ_WRITE_DONE, 1, 10))
		{
			AB_MSG(("fft sum read failed\n"));
			return 0;
		}

		ASSIGN_LONG(result, 0, XBYTE[FLICK_FLICK_MEM_DATA_2], XBYTE[FLICK_FLICK_MEM_DATA_1],
		            XBYTE[FLICK_FLICK_MEM_DATA_0]);
		sumx += result;
	}

	ratio =  (U8)((sumx*100) / sum127);
	AB_MSG(("%lu:%lu:%bu:%bu:%bu\n", sumx, sum127, ratio, g_byABDetectStatis,g_byABRatioThreshold));
	return ratio;
}

void ABStaticsStart(void)
{
#ifdef _ENABLE_OUTDOOR_DETECT_
	if(g_fAEC_EtGain< AS_ET_FLICK_LEVEL)
	{
		StaticsStart_ForFlickerDetect();
	}
	else
#endif	
	{
#ifdef _ENABLE_ANTI_FLICK_
		StaticsStart_ForAB();
#endif 

#ifdef _ENABLE_OUTDOOR_DETECT_
		g_byFlickerDetectCnt=0;
		g_byFlickerUnDetectCnt=0;
		g_byExistFlicker=0;
		g_bySceneChangeDelay=0;
#endif 
	}

}
#endif


