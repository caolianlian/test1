#include "pc_cam.h"
#include "Camutil.h"
#include "CamLSC.h"
#include "CamReg.h"
#include "CamReg2.h"
#include "Global_vars.h"
#include "ISP_vars.h"

#ifndef _SCENE_DETECTION_

#ifdef _NEW_AUTO_LSC_FUNCTION_	
float R_LSC_D65[8]={2.800000,1.448276,3.000000,2.100000,2.333333,2.625000,1.400000,3.000000};
float R_LSC_CWF[8]={2.555556,1.393939,2.875000,1.840000,2.190476,2.300000,1.314286,2.705882};
float R_LSC_A[8]={3.130435,1.565217,3.600000,2.250000,2.666667,2.880000,1.440000,3.428571};
//float R_LSC_U30[8]={2.576923,1.425532,3.190476,1.810811,2.233333,2.310345,1.218182,2.791667};

float B_LSC_D65[8]={2.000000,1.254902,2.206897,1.641026,1.828571,1.882353,1.207547,2.133333};
float B_LSC_CWF[8]={1.947368,1.275862,2.176471,1.608696,1.850000,1.947368,1.233333,2.176471};
float B_LSC_A[8]={2.000000,1.280000,2.285714,1.684211,1.882353,1.882353,1.230769,2.133333};
//float B_LSC_U30[8]={2.000000,1.263158,2.400000,1.500000,1.846154,1.714286,1.142857,2.181818};


U8 RGBToU(U8 byR, U8 byG, U8 byB)
{
	return (U8)(((S32)byR*(-150) + (S32)byG*(-260) + (S32)byB*410)/256 + 128);
}

U8 RGBToV(U8 byR, U8 byG, U8 byB)
{
	return (U8)	(((S32)byR*280 + (S32)byG*(-220) + (S32)byB*(-60))/256 + 128);
}

void SetDynamicLSCbyCT_New(void)
{
	U8 byCenterU, byCenterV;
	U8 byWinU, byWinV;
	U8 byWinIndex;
	
	float fsum_D65=0;
	float fsum_CWF=0;
	float fsum_A=0;
	float fcenterrb=0;
	float fwinrb=0;	
	
	U8 byind=LIGHT_UNKNOWN_TEMPERATURE;
	U8 i,j;

	byCenterU = RGBToU(g_byWinMeanR_BeforeLSC[2][2],g_byWinMeanG_BeforeLSC[2][2],
				g_byWinMeanB_BeforeLSC[2][2]);
	byCenterV = RGBToV(g_byWinMeanR_BeforeLSC[2][2],g_byWinMeanG_BeforeLSC[2][2],
				g_byWinMeanB_BeforeLSC[2][2]);

	for(i=0; i<5; i+=2)
	{
		for(j=0; j<5; j+=2)
		{
			if (i==2 && j==2) 
			{
				continue;
			}
			
			byWinIndex = (i>>1)*3+(j>>1);
			if (byWinIndex>4)
			{
				byWinIndex-=1;
			}	

			byWinU = RGBToU(g_byWinMeanR_BeforeLSC[i][j],g_byWinMeanG_BeforeLSC[i][j],
				g_byWinMeanB_BeforeLSC[i][j]);
			byWinV = RGBToV(g_byWinMeanR_BeforeLSC[i][j],g_byWinMeanG_BeforeLSC[i][j],
				g_byWinMeanB_BeforeLSC[i][j]);
		
			if((abs((S16)byWinU-(S16)byCenterU)<10) && (abs((S16)byWinV-(S16)byCenterV)<10))
			{
				fcenterrb=(float)g_byWinMeanR_BeforeLSC[2][2]/(float)g_byWinMeanB_BeforeLSC[2][2];

				fwinrb = (float)g_byWinMeanR_BeforeLSC[i][j]*R_LSC_D65[byWinIndex]/((float)g_byWinMeanB_BeforeLSC[i][j]*B_LSC_D65[byWinIndex]);
				if(fcenterrb>fwinrb)
				{
					fsum_D65 += (fcenterrb-fwinrb);
				}
				else
				{
					fsum_D65 += (fwinrb-fcenterrb);
				}

				fwinrb = (float)g_byWinMeanR_BeforeLSC[i][j]*R_LSC_CWF[byWinIndex]/((float)g_byWinMeanB_BeforeLSC[i][j]*B_LSC_CWF[byWinIndex]);
				if(fcenterrb>fwinrb)
				{
					fsum_CWF += (fcenterrb-fwinrb);
				}
				else
				{
					fsum_CWF += (fwinrb-fcenterrb);
				}		

				fwinrb = (float)g_byWinMeanR_BeforeLSC[i][j]*R_LSC_A[byWinIndex]/((float)g_byWinMeanB_BeforeLSC[i][j]*B_LSC_A[byWinIndex]);	
				if(fcenterrb>fwinrb)
				{
					fsum_A += (fcenterrb-fwinrb);
				}
				else
				{
					fsum_A += (fwinrb-fcenterrb);
				}		
	
			}
		}
	}

	if((fsum_D65>0)&&(fsum_CWF>0)&&(fsum_A>0))
	{
		if(fsum_D65<fsum_CWF)
		{
			if(fsum_D65<fsum_A)
			{
				byind = LIGHT_D65_TEMPERATURE;
			}
			else
			{
				byind = LIGHT_A_TEMPERATURE;
			}
		}
		else
		{
			if(fsum_CWF<fsum_A)
			{
				byind = LIGHT_CWF_TEMPERATURE;
			}
			else
			{
				byind = LIGHT_A_TEMPERATURE;
			}
		}
	}
	else
	{
		byind=LIGHT_UNKNOWN_TEMPERATURE;
	}	

	if(g_byPre_ind==byind)
	{
		g_byind_count++;
	}
	else
	{
		g_byPre_ind = byind;
		g_byind_count = 0;
	}

	if(g_byind_count>3)
	{
		g_byind = byind;
		g_byind_count = 0;
	}
}

#endif

void SetDynamicLSC(void)
{
	U8 i;	
	U16 wWhitePointNumber;
	U16 wMaxWPNumber=0;
	U16 wMaxWPRate;

	for(i=0;i<6;i++)
	{
		ASSIGN_INT(wWhitePointNumber, XBYTE[ISP_AWB_WP_NUM1_H+2*i], XBYTE[ISP_AWB_WP_NUM1_L+2*i]);
		if(wWhitePointNumber>wMaxWPNumber)
		{
			wMaxWPNumber =wWhitePointNumber;			
		}
	}
	
	wMaxWPRate = (U16)((U32)wMaxWPNumber*(U32)1000/((U32)g_wSensorCurFormatHeight*(U32)g_wSensorCurFormatWidth/(U32)64));
	if((wMaxWPRate>g_sLscDyn.byStartThd))
	{
		g_byDynamicLSCSet=1;
	}
	else if((wMaxWPRate<g_sLscDyn.byStopThd))
	{
		g_byDynamicLSCSet=0;
	}
}


void SetDynamicLSCbyCT(U16 wColorTempature)
{
	U16 wTheshold;
	U8 i;

	if (( g_wDynamicISPEn&DYNAMIC_LSC_CT_EN) == DYNAMIC_LSC_CT_EN)
	{
#ifdef _NEW_AUTO_LSC_FUNCTION_	
		if(g_byind<LIGHT_UNKNOWN_TEMPERATURE)
		{
			g_byLastIllumCT = g_byind;
		}
		else
#endif			
		{
			if(g_byDynamicLSCSet==1)
			{
				for(i=LIGHT_A_TEMPERATURE;i<LIGHT_D75_TEMPERATURE;i++)
				{
					if (i == g_byLastIllumCT-1)
					{
						wTheshold = g_sLscDyn.awColorTempTh[i] - g_sLscDyn.wColorTempBuffer;
					}
					else if (i == g_byLastIllumCT)
					{
						wTheshold = g_sLscDyn.awColorTempTh[i] + g_sLscDyn.wColorTempBuffer;
					}
					else
					{
						wTheshold = g_sLscDyn.awColorTempTh[i];
					}
			
					if(wColorTempature < wTheshold)
					{				
						break;
					}
				}

				g_byLastIllumCT=i;
			}
			else
			{
				g_byLastIllumCT=LIGHT_D65_TEMPERATURE;
			}
		}
	}
}

#endif


void SetNLSCRateOfLightSource(U8 ls)
{
	U8 byLscRateDelta=0;

	if (( g_wDynamicISPEn&DYNAMIC_LSC_EN) == DYNAMIC_LSC_EN)
	{
		if (ExposureTimeAutoItem.Last == EXPOSURE_TIME_AUTO_MOD_APERTPRO)
		{
			byLscRateDelta = 0x20 - LinearIntp_Byte_Bound(g_sLscDyn.byGainThL,  g_sLscDyn.byAdjustRateL, g_sLscDyn.byGainThH,  g_sLscDyn.byAdjustRateH, ClipWord((U16)(g_fAEC_Gain*16.0),0,255));
			byLscRateDelta = MinByte(MinByte(g_sLscDyn.abyRatebyCT[ls][0],g_sLscDyn.abyRatebyCT[ls][1]),MinByte(g_sLscDyn.abyRatebyCT[ls][2],byLscRateDelta));
		}	
	
		XBYTE[ISP_NLSC_R_ADJ_RATE] = g_sLscDyn.abyRatebyCT[ls][0] - byLscRateDelta;
		XBYTE[ISP_NLSC_G_ADJ_RATE] = g_sLscDyn.abyRatebyCT[ls][1] - byLscRateDelta;
		XBYTE[ISP_NLSC_B_ADJ_RATE] = g_sLscDyn.abyRatebyCT[ls][2] - byLscRateDelta;
		XBYTE[ISP_NLSC_GAIN_CTRL] = NLSC_GAIN_CTRL_START_LOAD | NLSC_GAIN_CTRL_CHANGE_STEP;
	}
	else if (( g_wDynamicISPEn&DYNAMIC_LSC_CT_EN) == DYNAMIC_LSC_CT_EN)
	{
		XBYTE[ISP_NLSC_R_ADJ_RATE] = g_sLscDyn.abyRatebyCT[ls][0];
		XBYTE[ISP_NLSC_G_ADJ_RATE] = g_sLscDyn.abyRatebyCT[ls][1];
		XBYTE[ISP_NLSC_B_ADJ_RATE] = g_sLscDyn.abyRatebyCT[ls][2];
		XBYTE[ISP_NLSC_GAIN_CTRL] = NLSC_GAIN_CTRL_START_LOAD | NLSC_GAIN_CTRL_CHANGE_STEP;
	}

}


