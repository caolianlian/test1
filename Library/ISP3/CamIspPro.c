#include "pc_cam.h"
#include "camutil.h"
#include "CamReg.h"
#include "CamUvc.h"
#include "CamCtl.h"
#include "camisp.h"
#include "CamSensor.h"
//#include "CamI2C.h"
//#include "CamProcess.h"
#include "Global_vars.h"
#include "ISP_vars.h"
#include "CamVdCfg.h"
#include "CamIspPro.h"
#include "CamAECPro.h"
#include "CamAWBPro.h"
#include "CamAFPro.h"
#include "ISP_TuningTable.h"
#if  ((defined _ENABLE_ANTI_FLICK_ )||(defined _ENABLE_OUTDOOR_DETECT_))
#include "CamABPro.h"
#endif
#include "CamHDR.h"
#include "CamASPro.h"
#include "Camspi.h"
#include "Camvdcmd.h"
#ifdef _RTK_EXTENDED_CTL_
#include "CamRtkExtIsp.h"
#endif

void SoftInterruptOpenBfrConnect(U8 intType);
void InitJpegTable();


U8 code g_byZoomFilter_MEAN[12] = 
{
	 85,	 85, 85, 85,
	 85,	 85, 85, 85,
	 86, 86, 86, 86
};

// 2011-02-25: temple give these following zoom filter coefient
// 2011-03-14: when pass MSOC edge roughness CIF format , g_byZoomFilter_PASS_LOW filter is needed ,and half-subsample should be disabled .
U8 code g_byZoomFilter_PASS_LOW[12] = 
{
	12, 16, 24, 37,
	54, 73, 93, 113,
	131, 147, 159, 165
};

U8 code g_byZoomFilter_PASS_1_2[12] = 
{
	4, 9, 15, 26,
	43,	63, 87, 113,
	139, 160, 178, 187
};


U8 code g_byZoomFilter_PASS_2_3[12] = 
{
   	0, 2, 7, 16,
	30,	52, 80, 112,
	144, 174, 197, 210
};

U8 code g_byZoomFilter_PASS_All[12] = 
{/*	// Albert use these parameter for chicony MSOC test
	   1,	  1,   1,   1,
	   1,	  1,   1,   1,
	254,254,254,254
	*/
	0, 0, 0, 4,
	16, 37,	68, 107,
	149, 188, 219, 236

};

void CalZoomRemainderWH(U16 wScale_v,U16 wScale_h, U16 wSnrWidth, U16 wSnrHeight, U16 wUsbWidth, U16 wUsbHeight)
{
	U16 data wZoomUsedWidth,wZoomUsedHeight;

	wZoomUsedWidth = (U16)((((U32)wUsbWidth*(U32)wScale_h)>>6)+1);	// add 1 for upper integer
	wZoomUsedHeight = (U16)((((U32)wUsbHeight*(U32)wScale_v)>>6)+1);	// add 1 for upper integer
	if(wZoomUsedWidth > wSnrWidth)
	{
		wZoomUsedWidth = wSnrWidth;
	}
	if(wZoomUsedHeight > wSnrHeight)
	{
		wZoomUsedHeight = wSnrHeight;
	}
	g_wZoomRemainderWidth = (wSnrWidth - wZoomUsedWidth);
	g_wZoomRemainderHeight = (wSnrHeight - wZoomUsedHeight);
}

void SetZoomStart(S8 sbyPan, S8 sbyTilt, S8 sbyTrapeziumSet)
{
	U16 data wStartX,wStartY;
	S16 swPanRange,swTiltRange;
	
	sbyTrapeziumSet = sbyTrapeziumSet;
	swPanRange = PanItem.Max - PanItem.Min;
	swTiltRange = TiltItem.Max - TiltItem.Min;

	wStartX = (U16)((sbyPan+(S16)(swPanRange/2))*((S16)g_wZoomRemainderWidth)/swPanRange);
	wStartY = (U16)((sbyTilt+(S16)(swTiltRange/2))*((S16)g_wZoomRemainderHeight)/swTiltRange);

	if(wStartX > 1023) wStartX = 1023;
	if(wStartY > 1023) wStartY = 1023;

#ifdef _TCORRECTION_EN_
	if (g_wExtUIspCtl  & EXU1_CTL_SEL_TCORRECTION)
	{
		if(sbyTrapeziumSet<0)
		{
			wStartX += g_wZoomXCorrectValue;
		}
	}
#endif
#ifdef _FACIAL_EXPOSURE_
	g_wZoomStartX = wStartX;
	g_wZoomStartY = wStartY;
#endif
	//write scale start point
	XBYTE[ISP_ZOOM_START_X_L]  =  INT2CHAR(wStartX,0);
	XBYTE[ISP_ZOOM_START_X_H]  =  INT2CHAR(wStartX,1);
	XBYTE[ISP_ZOOM_START_Y_L]  =  INT2CHAR(wStartY,0);
	XBYTE[ISP_ZOOM_START_Y_H]  =  INT2CHAR(wStartY,1);
}

void SetBackendScaleFilters(U16 scale)
{
	U8 code * p;
	U8 i;

	if(g_byHalfSubSample_En ==1)
	{
		scale = scale/2+scale;
	}

	if (scale >190)
	{
		p=g_byZoomFilter_PASS_LOW;
	}
	else if (scale > 128)
	{
		p=g_byZoomFilter_PASS_LOW;
	}
	else if(scale>100)
	{
		p=g_byZoomFilter_PASS_1_2;
	}
	else if (scale>=80)
	{
		p=g_byZoomFilter_PASS_2_3;
	}
	else
	{
		p = g_byZoomFilter_PASS_All;
	}

	for(i=0; i<12; i++)
	{
		XBYTE[ISP_ZOOM_COEF0+i] = p[i];
	}
}

U16 ZoomIn(U16 wOrgScale, U8 byZoomSet, U16 wUsbWidth)
{
	U16 wDesScale;
	U8 code cbyStepAry[4] = {32,25,20,16};

	// if resolution < VGA support PTZ, otherwise, not support PTZ
	if(wUsbWidth < 640)
	{
		if(byZoomSet > 3)
		{
			byZoomSet = 3;
		}
		wDesScale = (wOrgScale*(U16)cbyStepAry[byZoomSet])>>5;

		//For ccs 1/3 subsample CIF PTZ
		// hemonel 2011-03-03: resolve image hang when CIF and QVGA zoom to 3
		if(wDesScale < 64)
		{
			wDesScale = 64;
		}
		return wDesScale;
	}
	else
	{
		return wOrgScale;
	}
}

#ifdef _TCORRECTION_EN_

void SetTrapeziumCorrect(S8 sbyTrapeziumSet,U16 wUsbWidth, U16 wUsbHeight, U16 wScale_H, U16 wScale_V)
{
	U16 wStart_X_Tune;
	U32 dwHoriz_Step_Tune;
	U16 wDestScale_H;
	U32 dwHoriz_Step_Tune_Total;


	wStart_X_Tune = (abs(sbyTrapeziumSet)* wScale_V)>>6;

	dwHoriz_Step_Tune = (((U32)wStart_X_Tune)<<18)/(U32)wUsbWidth;

	g_wZoomXCorrectValue = (wStart_X_Tune*(wUsbHeight-1)+63)/64; //+63 for integer ceiling


	if(sbyTrapeziumSet>0)
	{
		wStart_X_Tune &= 0x7ff; //positive, start x label increase row by row
		dwHoriz_Step_Tune |= 0x800000; //negative, horizontal step decrease row by row
		wDestScale_H= wScale_H;
	}
	else //if(sbyTrapeziumSet<0)
	{
		dwHoriz_Step_Tune_Total = ((U32)g_wZoomXCorrectValue *2*64+wUsbWidth-1)/wUsbWidth;	// integer ceiling

		wDestScale_H = (U16)(wScale_H -dwHoriz_Step_Tune_Total);

		wStart_X_Tune |= 0x800; //negative, start x label decrease row by row
		dwHoriz_Step_Tune &= 0x7fffff; //positive, horizontal step increase row by row
	}

	XBYTE[ZOOM_START_X_TUNE_L] = INT2CHAR(wStart_X_Tune, 0);
	XBYTE[ZOOM_START_X_TUNE_H] = INT2CHAR(wStart_X_Tune, 1);

	XBYTE[ZOOM_HORIZ_STEP_TUNE_L] = LONG2CHAR(dwHoriz_Step_Tune, 0);
	XBYTE[ZOOM_HORIZ_STEP_TUNE_M] = LONG2CHAR(dwHoriz_Step_Tune, 1);
	XBYTE[ZOOM_HORIZ_STEP_TUNE_H] = LONG2CHAR(dwHoriz_Step_Tune, 2);

	XBYTE[ISP_ZOOM_STEP_X_L] = INT2CHAR(wDestScale_H, 0);		// HORZ_STEP Low
	XBYTE[ISP_ZOOM_STEP_X_H] = INT2CHAR(wDestScale_H, 1);		// HORZ_STEP High
	XBYTE[ISP_ZOOM_STEP_Y_L] = INT2CHAR(wScale_V, 0);		// VETI_STEP Low
	XBYTE[ISP_ZOOM_STEP_Y_H] = INT2CHAR(wScale_V, 1);		// VETI_STEP High

	XBYTE[ISP_CONTROL3] |= ISP_TRAPEZIUMCORRECT_EN;

}

#endif

U32 GetIspClk(void)
{
	U8 byClkDiv;
	
	byClkDiv = (XBYTE[CLKDIV]&ISP_CLKDIV_MASK)>>4;
	switch(XBYTE[CLKSEL]&ISP_CLKSEL_MASK)
	{
		case ISP_CLKSEL_80M:
			return ((U32)80*1000000)>>byClkDiv;
			//break;
		case ISP_CLKSEL_96M:
			return ((U32)96*1000000)>>byClkDiv;
			//break;
		default:
			return ((U32)60*1000000)>>byClkDiv;
			//break;
	}
}

//Float to U8.  when bydir ==0, FLOOR; when bydir== 1,ceil
U8 Float2U8(float ftemp,U8 bydir)
{
	U8 byTemp;

	// hemonel 2011-02-22: scale speed div over than 31 bug.
	if(ftemp>30)
	{
		return 30;
	}

	byTemp = (U8) ftemp;
	if (bydir == 0)
	{
		return byTemp;
	}
	else
	{
		if ( ftemp != (float) byTemp)
		{
			return (byTemp+1);
		}
		else
		{
			return byTemp;
		}
	}
}

static U8 ScaleSpeed(U32 dwZoomReadClk, U32 dwIspClk/*, U8 byFastOp*/)
{
	U8 data i;
	float fZoomDivIsp,fZoomDivIsp1;
	float fTemp,fdiff,fdiff_temp;
	U8 data byTemp1;
	U8 data div,mult;
	U16 code cwTemp[7] = {420,210,140,105,84,70,60};


	fZoomDivIsp = (float)dwIspClk/(float)dwZoomReadClk;
	fZoomDivIsp1 = fZoomDivIsp* (float)420;

	mult = 1;
	div = 1;
	fdiff = 13020.1; //(31*42+0.1)

	/*
	if(byFastOp == 1)
	{
		for (i=0; i<=6; i++)
		{
			fTemp = fZoomDivIsp*(float)(i+1);	//fZoomDivIsp*multiple
			byTemp1 = Float2U8(fTemp,0); 		//find the floor
			fdiff_temp = fZoomDivIsp1 - (float)((U16)byTemp1*cwTemp[i]);
			if( fdiff_temp<fdiff)
			{
				mult = i+1;
				div = byTemp1;
				fdiff = fdiff_temp;
			}
		}
	}
	else
	*/
	{
		for (i=0; i<=6; i++)
		{
			fTemp = fZoomDivIsp*(float)(i+1);			//fZoomDivIsp*multiple
			byTemp1 = Float2U8(fTemp,1); 				//find the ceil
			fdiff_temp =(float)((U16)byTemp1*cwTemp[i])-fZoomDivIsp1;
			if(( fdiff_temp<fdiff) && (fdiff_temp > 0))
			{
				mult = i+1;
				div = byTemp1;
				fdiff = fdiff_temp;
			}
		}
	}

	return ((mult <<5)|div);
}

void SetBackendISPSpeedParameters(U16 wSnrWidth)
{
	U8 byBefZoomSpeed;
	U32 dwISPClk,dwPclk_last;

	if ( (XBYTE[ISP_CLK_HALF_ENABLE] == 0x01) && (g_bySensor_YuvMode == YUV422_MODE))
	{
		dwISPClk = GetIspClk()/2;
	}
	else
	{
		dwISPClk = GetIspClk();
	}

	dwPclk_last = (g_dwPclk/(U32)g_wSensorHsyncWidth)*(U32)wSnrWidth;

	// hemonel 2010-04-09: zoom normal clock use fast clock and others use slow clock
	byBefZoomSpeed = ScaleSpeed(dwPclk_last, dwISPClk/*, 0*/);
	XBYTE[ISP_DPC_SPEED_CTRL] = byBefZoomSpeed;
	XBYTE[ISP_INTP_SPEED_CTRL] = byBefZoomSpeed;
	XBYTE[ISP_ZOOM_SPEED_CTRL] = byBefZoomSpeed;
	XBYTE[ISP_EEH_SPEED_CTRL] = byBefZoomSpeed;
}

void SetBKScale(U8 byZoomSet, U16 wSnrWidth, U16 wSnrHeight, U16 wUsbWidth, U16 wUsbHeight, S8 sbyTrapeziumSet)
{
	U16 scale_h,scale_v,scale_tmp;

	// calculate scale coeficient
	// select the minimum one of scale coeficient
	scale_h = (((U32)wSnrWidth)<<6)/(U32)wUsbWidth;	// horizontal scale step
	scale_v = (((U32)wSnrHeight)<<6)/(U32)wUsbHeight; // vertical scale step

#ifndef _IMX076_320x240_60FPS_
	if ( (wUsbWidth == 352)&&(g_byCIFscaleParam_En == 1) )
	{
		// Horizontal scale step = vertical scale step * 13/12
		scale_tmp = scale_v*13/12;
		if(scale_tmp <= scale_h)
		{
			scale_h= scale_tmp;
		}
		else
		{
			scale_v = scale_h*12/13;
		}
	}
	else
	{
		scale_h= scale_h<scale_v?scale_h:scale_v;
		scale_v = scale_h;
	}

	// save the result before zoom to select zoom filter coefient
	// select the max one of scale coeficient
	scale_tmp = scale_h;
#else
	if(VGA_FRM == g_wSensorCurFormat)
	{
		scale_tmp = scale_h/2;
		scale_v= scale_tmp<scale_v?scale_tmp:scale_v;
	}
	else
	{
		if ( (wUsbWidth == 352)&&(g_byCIFscaleParam_En == 1) )
		{
			// Horizontal scale step = vertical scale step * 13/12
			scale_tmp = scale_v*13/12;
			if(scale_tmp <= scale_h)
			{
				scale_h= scale_tmp;
			}
			else
			{
				scale_v = scale_h*12/13;
			}
		}
		else
		{
			scale_h= scale_h<scale_v?scale_h:scale_v;
			scale_v = scale_h;
		}
	}
	scale_tmp = scale_v;
#endif

	if (g_bIsHighSpeed)
	{
		// hemonel 2010-04-09: zoomin step 1.25
		scale_h = ZoomIn(scale_h, byZoomSet, wUsbWidth);
		scale_v = ZoomIn(scale_v, byZoomSet, wUsbWidth);
	}
#ifdef _FACIAL_EXPOSURE_
	g_wZoomScale_h=scale_h;
	g_wZoomScale_v=scale_v;
#endif

#ifdef _TCORRECTION_EN_
	if (g_wExtUIspCtl  & EXU1_CTL_SEL_TCORRECTION)
	{
		SetTrapeziumCorrect(sbyTrapeziumSet, wUsbWidth, wUsbHeight,scale_h,scale_v);
	}
	else
#endif
	{
		XBYTE[ISP_ZOOM_STEP_X_L] = INT2CHAR(scale_h, 0);		// HORZ_STEP Low
		XBYTE[ISP_ZOOM_STEP_X_H] = INT2CHAR(scale_h, 1);		// HORZ_STEP High
		XBYTE[ISP_ZOOM_STEP_Y_L] = INT2CHAR(scale_v, 0);		// VETI_STEP Low
		XBYTE[ISP_ZOOM_STEP_Y_H] = INT2CHAR(scale_v, 1);		// VETI_STEP High
	}

	// scale start point
	CalZoomRemainderWH(scale_v,scale_h, wSnrWidth, wSnrHeight, wUsbWidth, wUsbHeight);
	SetZoomStart(PanItem.Last, TiltItem.Last,sbyTrapeziumSet);

	// set Zoom speed
	SetBackendISPSpeedParameters(wSnrWidth);	// use scale coefient as scale speed calculation parameter because use the bigger usb width

	// modify zoom filter coefficient
	SetBackendScaleFilters(scale_tmp);
}

#ifdef _RTK_EXTENDED_CTL_
void PostZoom2PreZoom(U16 *wX1, U16 *wY1, U16 *wX2, U16 *wY2)
{
	U16 wZoomScaleX, wZoomScaleY, wZoomStartX, wZoomStartY;
	
	ASSIGN_INT(wZoomScaleX, XBYTE[ISP_ZOOM_STEP_X_H], XBYTE[ISP_ZOOM_STEP_X_L]);
	ASSIGN_INT(wZoomScaleY, XBYTE[ISP_ZOOM_STEP_Y_H], XBYTE[ISP_ZOOM_STEP_Y_L]);
	ASSIGN_INT(wZoomStartX, XBYTE[ISP_ZOOM_START_X_H], XBYTE[ISP_ZOOM_START_X_L]);
	ASSIGN_INT(wZoomStartY, XBYTE[ISP_ZOOM_START_Y_H], XBYTE[ISP_ZOOM_START_Y_L]);
	*wX1 = wZoomStartX + ((((U32)(*wX1))*((U32)wZoomScaleX))>>6);
	*wY1 = wZoomStartY + ((((U32)(*wY1))*((U32)wZoomScaleY))>>6);
	*wX2 = wZoomStartX + ((((U32)(*wX2))*((U32)wZoomScaleX))>>6);
	*wY2 = wZoomStartY + ((((U32)(*wY2))*((U32)wZoomScaleY))>>6);
}
#endif

void SetBackendScale(U16 wSnrWidth, U16 wSnrHeight, U16 wUsbWidth, U16 wUsbHeight)
{
#ifdef 	_LENOVO_JAPAN_PROPERTY_PAGE_
	SetBKScale(ZoomItem.Last/100-1, wSnrWidth, wSnrHeight, wUsbWidth, wUsbHeight,TCorrectionItem.Last);
#else
	SetBKScale(ZoomItem.Last, wSnrWidth, wSnrHeight, wUsbWidth, wUsbHeight,TCorrectionItem.Last);
#endif
	// write scale back window
	XBYTE[ISP_BK_WIDTH_L]= INT2CHAR(wUsbWidth, 0);
	XBYTE[ISP_BK_WIDTH_H] = INT2CHAR(wUsbWidth, 1);
	XBYTE[ISP_BK_HEIGHT_L] = INT2CHAR(wUsbHeight, 0);
	XBYTE[ISP_BK_HEIGHT_H] = INT2CHAR(wUsbHeight, 1);

	XBYTE[ISP_CONTROL1] |= ISP_ZOOM_EN;
}

void SetCCSFrameSize(U16 wWidth,U16 wHeight)
{
	XBYTE[CCS_HORIZON_NUM0] = INT2CHAR(wWidth, 0);
	XBYTE[CCS_HORIZON_NUM1] = INT2CHAR(wWidth, 1);
	XBYTE[CCS_VERTICAL_NUM0] = INT2CHAR(wHeight, 0);
	XBYTE[CCS_VERTICAL_NUM1] = INT2CHAR(wHeight, 1);
}

void SetBackendISPFrameSize(U16 wWidth_bef, U16 wHeight_bef,U16 wStartX,U16 wStartY)
{
	// set ISP fornt frame size
	XBYTE[ISP_FT_WIDTH_L] = INT2CHAR(wWidth_bef, 0);
	XBYTE[ISP_FT_WIDTH_H] = INT2CHAR(wWidth_bef, 1);
	XBYTE[ISP_FT_HEIGHT_L] = INT2CHAR(wHeight_bef, 0);
	XBYTE[ISP_FT_HEIGHT_H] = INT2CHAR(wHeight_bef, 1);

	//BLC Start_X,Start_Y Coordinates
	XBYTE[ISP_BLC_START_X_L] = INT2CHAR(wStartX, 0);
	XBYTE[ISP_BLC_START_X_H] = INT2CHAR(wStartX, 1);
	XBYTE[ISP_BLC_START_Y_L] = INT2CHAR(wStartY, 0);
	XBYTE[ISP_BLC_START_Y_H] = INT2CHAR(wStartY, 1);
}

void SetBackendBLC(void)
{
	XBYTE[ISP_BLC_OFFSET_R] = g_byoffsetR;
	XBYTE[ISP_BLC_OFFSET_GR] = g_byoffsetG1;
	XBYTE[ISP_BLC_OFFSET_GB] = g_byoffsetG2;
	XBYTE[ISP_BLC_OFFSET_B] = g_byoffsetB;

	XBYTE[ISP_BLC_GAIN_R] = (((U16)g_byoffsetR<<8)/((U16)(1023 -g_byoffsetR)) +1)/2;
	XBYTE[ISP_BLC_GAIN_GR] = (((U16)g_byoffsetG1<<8)/((U16)(1023 -g_byoffsetG1)) +1)/2;
	XBYTE[ISP_BLC_GAIN_GB] = (((U16)g_byoffsetG2<<8)/((U16)(1023 -g_byoffsetG2)) +1)/2;
	XBYTE[ISP_BLC_GAIN_B] = (((U16)g_byoffsetB<<8)/((U16)(1023 -g_byoffsetB)) +1)/2;
}

void SetBackendLSCCenter(U16 wCenterX, U16 wCenterY, U8 byColor)
{
	U32 neg_distance2,real_distance2;
	U16 distance;
	S16 diff;

	// hemonel 2011-05-11: need transform if sub resolution
	if(g_bySubResolution_For_LSC == 1)
	{
		wCenterX >>= 1;
		wCenterY >>= 1;
	}

	real_distance2 = (U32)wCenterX*(U32)wCenterX+(U32)wCenterY*(U32)wCenterY;
	distance = (U16)sqrt(real_distance2);
	neg_distance2 = (U32)distance*(U32)distance;
	diff = (S16)((S32)neg_distance2-(S32)real_distance2);
	if(byColor == COLOR_R)
	{
		XBYTE[ISP_NLSC_CENTER_X_R_L]  = INT2CHAR(wCenterX,0);
		XBYTE[ISP_NLSC_CENTER_X_R_H]  = INT2CHAR(wCenterX,1);
		XBYTE[ISP_NLSC_CENTER_Y_R_L]  = INT2CHAR(wCenterY,0);
		XBYTE[ISP_NLSC_CENTER_Y_R_H]  = INT2CHAR(wCenterY,1);
		XBYTE[ISP_NLSC_DISTANCE_R_L] = INT2CHAR(distance, 0);
		XBYTE[ISP_NLSC_DISTANCE_R_H] = INT2CHAR(distance, 1);
		XBYTE[ISP_NLSC_ERROR_R_L] = INT2CHAR(diff, 0);
		XBYTE[ISP_NLSC_ERROR_R_H] = INT2CHAR(diff, 1);
	}
	else if(byColor == COLOR_G)
	{
		XBYTE[ISP_NLSC_CENTER_X_G_L]  = INT2CHAR(wCenterX,0);
		XBYTE[ISP_NLSC_CENTER_X_G_H]  = INT2CHAR(wCenterX,1);
		XBYTE[ISP_NLSC_CENTER_Y_G_L]  = INT2CHAR(wCenterY,0);
		XBYTE[ISP_NLSC_CENTER_Y_G_H]  = INT2CHAR(wCenterY,1);
		XBYTE[ISP_NLSC_DISTANCE_G_L] = INT2CHAR(distance, 0);
		XBYTE[ISP_NLSC_DISTANCE_G_H] = INT2CHAR(distance, 1);
		XBYTE[ISP_NLSC_ERROR_G_L] = INT2CHAR(diff, 0);
		XBYTE[ISP_NLSC_ERROR_G_H] = INT2CHAR(diff, 1);
	}
	else
	{
		XBYTE[ISP_NLSC_CENTER_X_B_L]  = INT2CHAR(wCenterX,0);
		XBYTE[ISP_NLSC_CENTER_X_B_H]  = INT2CHAR(wCenterX,1);
		XBYTE[ISP_NLSC_CENTER_Y_B_L]  = INT2CHAR(wCenterY,0);
		XBYTE[ISP_NLSC_CENTER_Y_B_H]  = INT2CHAR(wCenterY,1);
		XBYTE[ISP_NLSC_DISTANCE_B_L] = INT2CHAR(distance, 0);
		XBYTE[ISP_NLSC_DISTANCE_B_H] = INT2CHAR(distance, 1);
		XBYTE[ISP_NLSC_ERROR_B_L] = INT2CHAR(diff, 0);
		XBYTE[ISP_NLSC_ERROR_B_H] = INT2CHAR(diff, 1);
	}
}

#ifdef _IQ_TABLE_CALIBRATION_
#ifdef IQ_CAL_CLSC_FHD
static void IQTablePatchCLSC1(void )
{
	U16 wCenterX, wCenterY;
	SPI_WB_Real_Read((U32)(IQ_TABLE_PATCH_BASE+CLSC_FHD_PATCH_OFFSET), 48, g_byReadMode);	
	memcpy((U8 *)&XBYTE[ISP_NLSC_CURVE_R_BASE],(U8 *)&XBYTE[SPI_I2C_BUFFER_BASE],48 );
	SPI_WB_Real_Read((U32)(IQ_TABLE_PATCH_BASE+CLSC_FHD_PATCH_OFFSET+48), 48, g_byReadMode);	
	memcpy((U8 *)&XBYTE[ISP_NLSC_CURVE_G_BASE],(U8 *)&XBYTE[SPI_I2C_BUFFER_BASE],48 );
	SPI_WB_Real_Read((U32)(IQ_TABLE_PATCH_BASE+CLSC_FHD_PATCH_OFFSET+48+48), 48, g_byReadMode);
	memcpy((U8 *)&XBYTE[ISP_NLSC_CURVE_B_BASE],(U8 *)&XBYTE[SPI_I2C_BUFFER_BASE],48 );	
	SPI_WB_Real_Read((U32)(IQ_TABLE_PATCH_BASE+CLSC_FHD_PATCH_OFFSET+48+48+48), 12, g_byReadMode);	
	ASSIGN_INT(wCenterX,XBYTE[SPI_I2C_BUFFER_BASE+1],XBYTE[SPI_I2C_BUFFER_BASE]);
	ASSIGN_INT(wCenterY,XBYTE[SPI_I2C_BUFFER_BASE+3],XBYTE[SPI_I2C_BUFFER_BASE+2]);	
	SetBackendLSCCenter(wCenterX,wCenterY, COLOR_R);
	ASSIGN_INT(wCenterX,XBYTE[SPI_I2C_BUFFER_BASE+5],XBYTE[SPI_I2C_BUFFER_BASE+4]);
	ASSIGN_INT(wCenterY,XBYTE[SPI_I2C_BUFFER_BASE+7],XBYTE[SPI_I2C_BUFFER_BASE+6]);	
	SetBackendLSCCenter(wCenterX,wCenterY, COLOR_G);
	ASSIGN_INT(wCenterX,XBYTE[SPI_I2C_BUFFER_BASE+9],XBYTE[SPI_I2C_BUFFER_BASE+8]);
	ASSIGN_INT(wCenterY,XBYTE[SPI_I2C_BUFFER_BASE+11],XBYTE[SPI_I2C_BUFFER_BASE+10]);	
	SetBackendLSCCenter(wCenterX,wCenterY, COLOR_B);	
	
}
#endif

#ifdef IQ_CAL_MLSC_FHD
static void IQTablePatchMLSC1(void )
{

	U16 i,j;	
	U8 byTmp;
	U16 wTmp;
	
	U8 byaTmp[3][64];

	for (i=0; i<5;i++)
	{
		wTmp = (U16)64*i;	
		SPI_WB_Real_Read((U32)(IQ_TABLE_PATCH_BASE+MLSC_FHD_PATCH_OFFSET+1+wTmp), 64, g_byReadMode);
		memcpy(byaTmp[0],(U8 *)&XBYTE[SPI_I2C_BUFFER_BASE],64 );
		SPI_WB_Real_Read((U32)(IQ_TABLE_PATCH_BASE+MLSC_FHD_PATCH_OFFSET+1+320+wTmp), 64, g_byReadMode);
		memcpy(byaTmp[1],(U8 *)&XBYTE[SPI_I2C_BUFFER_BASE],64 );
		SPI_WB_Real_Read((U32)(IQ_TABLE_PATCH_BASE+MLSC_FHD_PATCH_OFFSET+1+320+320+wTmp), 64, g_byReadMode);
		memcpy(byaTmp[2],(U8 *)&XBYTE[SPI_I2C_BUFFER_BASE],64 );
		for (j=0; j<64; j++)
		{
			XBYTE[ISP_MICRO_LSC_PARA_0] = byaTmp[0][j];
			XBYTE[ISP_MICRO_LSC_PARA_1] = byaTmp[1][j];
			XBYTE[ISP_MICRO_LSC_PARA_2] = byaTmp[1][j];
			XBYTE[ISP_MICRO_LSC_PARA_3] = byaTmp[2][j];
			XBYTE[ISP_MICRO_LSC_CTRL] = ISP_MLSC_WRITE;
		}	
	}
	
	SPI_WB_Real_Read((U32)(IQ_TABLE_PATCH_BASE+MLSC_FHD_PATCH_OFFSET), 1, g_byReadMode);		
	byTmp = XBYTE[SPI_I2C_BUFFER_BASE];
	if(byTmp > 0)
	{
		XBYTE[ISP_MICRO_LSC_CTRL] = byTmp -g_bySubResolution_For_LSC;
	}
	else
	{
		XBYTE[ISP_MICRO_LSC_CTRL] = 0;
	}	
	
}
#endif

#ifdef IQ_CAL_DYN_LSC
void IQTablePatchDynLSCRate1(void )
{
	SPI_WB_Real_Read((U32)(IQ_TABLE_PATCH_BASE+DYN_NLSC_PATCH_OFFSET), DYN_NLSC_PATCH_LEN, g_byReadMode);	
	memcpy((U8*)&(g_sLscDyn.abyRatebyCT), (U8 *)&XBYTE[SPI_I2C_BUFFER_BASE], DYN_NLSC_PATCH_LEN);
}
#endif
#endif

void SetBackendLSC(U16 width)
{
	U8 j;
	
	if ((width<=800)||(g_bySubResolution_For_LSC == 1))
	{
		XBYTE[ISP_NLSC_MODE] = NLSC_STEP_MODE_32;
	}
	else
	{
		XBYTE[ISP_NLSC_MODE] = NLSC_STEP_MODE_64;
	}
#ifdef _IQ_TABLE_CALIBRATION_
#ifdef IQ_CAL_CLSC_FHD
	if (IQ_CAL_CLSC_FHD == (g_byIQPatchGrpOnePatchOne&IQ_CAL_CLSC_FHD))
	{
		IQTablePatchCLSC1();
	}
	else
#endif	
#endif	
	{
		for (j=0; j<48; j++)
		{
			XBYTE[ISP_NLSC_CURVE_R_BASE+j]  = ct_IQ_Table.lsc.circle.abyCurve[0][j];
			XBYTE[ISP_NLSC_CURVE_G_BASE+j]  = ct_IQ_Table.lsc.circle.abyCurve[1][j];
			XBYTE[ISP_NLSC_CURVE_B_BASE+j]  = ct_IQ_Table.lsc.circle.abyCurve[2][j];
		}

		SetBackendLSCCenter(	ct_IQ_Table.lsc.circle.awCenter[0],	ct_IQ_Table.lsc.circle.awCenter[1], COLOR_R);
		SetBackendLSCCenter(	ct_IQ_Table.lsc.circle.awCenter[2],	ct_IQ_Table.lsc.circle.awCenter[3], COLOR_G);
		SetBackendLSCCenter(	ct_IQ_Table.lsc.circle.awCenter[4],	ct_IQ_Table.lsc.circle.awCenter[5], COLOR_B);
	}
	XBYTE[ISP_CONTROL1] |= ISP_NORMALSC_EN;

	XBYTE[ISP_NLSC_GAIN_STEP_R] = 0x1;
	XBYTE[ISP_NLSC_GAIN_STEP_G] = 0x1;
	XBYTE[ISP_NLSC_GAIN_STEP_B] = 0x1;
	XBYTE[ISP_NLSC_MODE] |= NLSC_R_CURVE_ADJ_EN | NLSC_G_CURVE_ADJ_EN | NLSC_B_CURVE_ADJ_EN;
	XBYTE[ISP_NLSC_R_ADJ_RATE] = 0x20;
	XBYTE[ISP_NLSC_G_ADJ_RATE] = 0x20;
	XBYTE[ISP_NLSC_B_ADJ_RATE] = 0x20;
	XBYTE[ISP_NLSC_GAIN_CTRL] = NLSC_GAIN_CTRL_START_LOAD | NLSC_GAIN_CTRL_CHANGE_DIRECTLY;
}

void SetBackendMicroLSC()
{
	U16 j;

	r_wISP_MICRO_LSC_ADDR = 0x00;	
#ifdef _IQ_TABLE_CALIBRATION_
#ifdef IQ_CAL_MLSC_FHD
	if (IQ_CAL_MLSC_FHD == (g_byIQPatchGrpOnePatchOne&IQ_CAL_MLSC_FHD))
	{
		IQTablePatchMLSC1();
	}
	else
#endif	
#endif	
	{
		for (j=0; j<320; j++)
		{
			XBYTE[ISP_MICRO_LSC_PARA_0] = ct_IQ_Table.lsc.micro.abyMatrix[0][j];
			XBYTE[ISP_MICRO_LSC_PARA_1] = ct_IQ_Table.lsc.micro.abyMatrix[1][j];
			XBYTE[ISP_MICRO_LSC_PARA_2] = ct_IQ_Table.lsc.micro.abyMatrix[1][j];
			XBYTE[ISP_MICRO_LSC_PARA_3] = ct_IQ_Table.lsc.micro.abyMatrix[2][j];
			XBYTE[ISP_MICRO_LSC_CTRL] = ISP_MLSC_WRITE;
		}			
		
		// hemonel 2011-05-11: step need decrease 1/2 if sub resolution
		if(ct_IQ_Table.lsc.micro.byGridmode > 0)
		{
			XBYTE[ISP_MICRO_LSC_CTRL] = ct_IQ_Table.lsc.micro.byGridmode -g_bySubResolution_For_LSC;
		}
		else
		{
			XBYTE[ISP_MICRO_LSC_CTRL] = 0;
		}
	}
	XBYTE[ISP_CONTROL1] |= ISP_MICROLSC_EN;
}

//Set ISP sharp  variational paramters depand on Image Fomat & illumination condition
//byI == 0: YUV normal illumination.	byI == 1: MJPEG normal illumination. byI == 2: Low lux.
void SetBKSharpPramVari(U8 i)
{
	U8 j=0;

	//noise reduction
	XBYTE[ISP_NR_EDGE_THD]  =  g_abyISParamVaris[i][j++];
	XBYTE[ISP_NR_MM_THD1] 	=  g_abyISParamVaris[i][j++];
	XBYTE[ISP_NR_MODE0_LPF] =  g_abyISParamVaris[i][j++];
	XBYTE[ISP_NR_MODE1_LPF]	=  g_abyISParamVaris[i][j++];
	XBYTE[ISP_NR_MODE] 	    = 	g_abyISParamVaris[i][j++];
	XBYTE[ISP_NR_COLOR_ENABLE_CTRL] = g_abyISParamVaris[i][j++];		
 	XBYTE[ISP_NR_COLOR_MAX]	=	g_abyISParamVaris[i][j++];		
	XBYTE[ISP_NR_CHAOS_MAX] = 	g_abyISParamVaris[i][j++];
	XBYTE[ISP_NR_CHAOS_THD] = 	g_abyISParamVaris[i][j++];
	XBYTE[ISP_NR_CHAOS_CFG] = 	g_abyISParamVaris[i][j++];
	XBYTE[ISP_NR_DTHD_CFG]  = 	g_abyISParamVaris[i][j++];
	XBYTE[ISP_NR_GRGB_CTRL] = g_abyISParamVaris[i][j++];

	//intp
	XBYTE[ISP_INTP_EDGE_THD0] = g_abyISParamVaris[i][j++];
	XBYTE[ISP_INTP_EDGE_THD1] = g_abyISParamVaris[i][j++];
	XBYTE[ISP_INTP_MODE] 	  = g_abyISParamVaris[i][j++];
	XBYTE[ISP_INTP_CHAOS_MAX] = g_abyISParamVaris[i][j++];
	XBYTE[ISP_INTP_CHAOS_THD] = g_abyISParamVaris[i][j++];
	XBYTE[ISP_INTP_CHAOS_CFG] = g_abyISParamVaris[i][j++];
	XBYTE[ISP_INTP_DTHD_CFG]  = g_abyISParamVaris[i][j++];

	//IIR filter
	XBYTE[ISP_RGB_IIR_CTRL]     = g_abyISParamVaris[i][j++];	

	g_bySharp_VIIR_Coef = g_abyISParamVaris[i][j++];
	g_bySharp_HIIR_Coef = g_abyISParamVaris[i][j++];

	XBYTE[ISP_RGB_DIIR_MAX]		= g_abyISParamVaris[i][j++];		
	XBYTE[ISP_RGB_DIIR_COEF]	= g_abyISParamVaris[i][j++];			
	XBYTE[ISP_RGB_BRIGHT_COEF]	= g_abyISParamVaris[i][j++];		
	XBYTE[ISP_RGB_DIIR_COFF_CUT]	= g_abyISParamVaris[i][j++];	

	//edge enchance
//	XBYTE[ISP_EDG_DCT_THD1] = 	g_abyISParamVaris[i][23];
	// hemonel 2011-03-14: sharpness parameter change with AE gain and sharpness
	g_bySharp_EdgeDct_Thd1 = g_abyISParamVaris[i][j++];

	XBYTE[ISP_EEH_DTHD_CFG] = 	g_abyISParamVaris[i][j++];

	
}

void SetBKTexture2PramVari(U8 i)
{
	// corner denoise
	XBYTE[ISP_RDLOC_MAX] = ct_IQ_Table.texture2.corner.abyRate[i][0];
	XBYTE[ISP_RDLOC_RATE] = ct_IQ_Table.texture2.corner.abyRate[i][1];
	XBYTE[ISP_GLOC_MAX] = ct_IQ_Table.texture2.corner.abyRate[i][2];
	XBYTE[ISP_GLOC_RATE] = ct_IQ_Table.texture2.corner.abyRate[i][3];
	XBYTE[ISP_ILOC_MAX] = ct_IQ_Table.texture2.corner.abyRate[i][4];
	XBYTE[ISP_ILOC_RATE] = ct_IQ_Table.texture2.corner.abyRate[i][5];
	XBYTE[ISP_LOC_MAX] = ct_IQ_Table.texture2.corner.abyRate[i][6];
	XBYTE[ISP_LOC_RATE] = ct_IQ_Table.texture2.corner.abyRate[i][7];

	// uv denoise
	XBYTE[ISP_EEH_UVIIR_Y_CUTS] = ct_IQ_Table.texture2.uvdenoise.abyUvd[i][0];
	XBYTE[ISP_EEH_UVIIR_Y_CMIN] = ct_IQ_Table.texture2.uvdenoise.abyUvd[i][1];	
	XBYTE[ISP_EEH_IIR_COEF] = ct_IQ_Table.texture2.uvdenoise.abyUvd[i][2];
	XBYTE[ISP_EEH_IIR_CUTS] = ct_IQ_Table.texture2.uvdenoise.abyUvd[i][3];
	XBYTE[ISP_EEh_IIR_STEP] = ct_IQ_Table.texture2.uvdenoise.abyUvd[i][4];

	// noise reduction additional
	XBYTE[ISP_EEH_CRC_RATE] = ct_IQ_Table.texture2.nr.abyEehSm3CrcRate[i];
	XBYTE[ISP_RD_MM2_RATE] = ct_IQ_Table.texture2.nr.abyRdMm2Rate[i];
	XBYTE[ISP_NR_MMM_D0] = ct_IQ_Table.texture2.nr.abyNrMmm[i][0];
	XBYTE[ISP_NR_MMM_D1] = ct_IQ_Table.texture2.nr.abyNrMmm[i][1];
	XBYTE[ISP_NR_MMM_RATE] = ct_IQ_Table.texture2.nr.abyNrMmm[i][2];
	XBYTE[ISP_NR_MMM_MIN] = ct_IQ_Table.texture2.nr.abyNrMmm[i][3];
	XBYTE[ISP_NR_MMM_MAX] = ct_IQ_Table.texture2.nr.abyNrMmm[i][4];
}

void SetCornerLoc(void)
{
	XBYTE[ISP_RDLOC_DIST_START_L]= INT2CHAR(ct_IQ_Table.texture2.corner.wStart_RGB,0);
	XBYTE[ISP_RDLOC_DIST_START_H]= INT2CHAR(ct_IQ_Table.texture2.corner.wStart_RGB,1);
	XBYTE[ISP_RDLOC_DIST_END_L]= INT2CHAR(ct_IQ_Table.texture2.corner.wEnd_RGB,0);
	XBYTE[ISP_RDLOC_DIST_END_H]= INT2CHAR(ct_IQ_Table.texture2.corner.wEnd_RGB,1);
	XBYTE[ISP_LOC_DIST_START_L]= INT2CHAR(ct_IQ_Table.texture2.corner.wStart_YUV,0);
	XBYTE[ISP_LOC_DIST_START_H]= INT2CHAR(ct_IQ_Table.texture2.corner.wStart_YUV,1);
	XBYTE[ISP_LOC_DIST_END_L]= INT2CHAR(ct_IQ_Table.texture2.corner.wEnd_YUV,0);
	XBYTE[ISP_LOC_DIST_END_H]= INT2CHAR(ct_IQ_Table.texture2.corner.wEnd_YUV,1);
}

void SetEdgeEhance(void)
{
	XBYTE[ISP_BRIGHT_RATE]        = ct_IQ_Table.edgeenhance.byEdgeEhance[0];  
	XBYTE[ISP_BRIGHT_TRM_B1]      = ct_IQ_Table.edgeenhance.byEdgeEhance[1];  
	XBYTE[ISP_BRIGHT_TRM_B2]      = ct_IQ_Table.edgeenhance.byEdgeEhance[2];  
	XBYTE[ISP_BRIGHT_TRM_K]       = ct_IQ_Table.edgeenhance.byEdgeEhance[3];  
	XBYTE[ISP_BRIGHT_TRM_THD0]    = ct_IQ_Table.edgeenhance.byEdgeEhance[4];  
	XBYTE[ISP_BRIGHT_TRM_THD1]    = ct_IQ_Table.edgeenhance.byEdgeEhance[5];  
	XBYTE[ISP_DARK_RATE]          = ct_IQ_Table.edgeenhance.byEdgeEhance[6];  
	XBYTE[ISP_DARK_TRM_B1]        = ct_IQ_Table.edgeenhance.byEdgeEhance[7];  
	XBYTE[ISP_DARK_TRM_B2]        = ct_IQ_Table.edgeenhance.byEdgeEhance[8];  
	XBYTE[ISP_DARK_TRM_K]         = ct_IQ_Table.edgeenhance.byEdgeEhance[9];  
	XBYTE[ISP_DARK_TRM_THD0]      = ct_IQ_Table.edgeenhance.byEdgeEhance[10];  
	XBYTE[ISP_DARK_TRM_THD1]      = ct_IQ_Table.edgeenhance.byEdgeEhance[11];  
	XBYTE[ISP_EDG_DIFF_C0]        = ct_IQ_Table.edgeenhance.byEdgeEhance[12];  
	XBYTE[ISP_EDG_DIFF_C1]        = ct_IQ_Table.edgeenhance.byEdgeEhance[13];  
	XBYTE[ISP_EDG_DIFF_C2]        = ct_IQ_Table.edgeenhance.byEdgeEhance[14];  
	XBYTE[ISP_EDG_DIFF_C3]        = ct_IQ_Table.edgeenhance.byEdgeEhance[15];  
	XBYTE[ISP_EDG_DIFF_C4]        = ct_IQ_Table.edgeenhance.byEdgeEhance[16];  
	XBYTE[ISP_EEH_SHARP_ARRAY0]   = ct_IQ_Table.edgeenhance.byEdgeEhance[17];  
	XBYTE[ISP_EEH_SHARP_ARRAY10]  = ct_IQ_Table.edgeenhance.byEdgeEhance[18];  
	XBYTE[ISP_EEH_SHARP_ARRAY11]  = ct_IQ_Table.edgeenhance.byEdgeEhance[19];  
	XBYTE[ISP_EEH_SHARP_ARRAY1]   = ct_IQ_Table.edgeenhance.byEdgeEhance[20];  
	XBYTE[ISP_EEH_SHARP_ARRAY2]   = ct_IQ_Table.edgeenhance.byEdgeEhance[21];  
	XBYTE[ISP_EEH_SHARP_ARRAY3]   = ct_IQ_Table.edgeenhance.byEdgeEhance[22];  
	XBYTE[ISP_EEH_SHARP_ARRAY4]   = ct_IQ_Table.edgeenhance.byEdgeEhance[23];  
	XBYTE[ISP_EEH_SHARP_ARRAY5]   = ct_IQ_Table.edgeenhance.byEdgeEhance[24];  
	XBYTE[ISP_EEH_SHARP_ARRAY6]   = ct_IQ_Table.edgeenhance.byEdgeEhance[25];  
	XBYTE[ISP_EEH_SHARP_ARRAY7]   = ct_IQ_Table.edgeenhance.byEdgeEhance[26];  
	XBYTE[ISP_EEH_SHARP_ARRAY8]   = ct_IQ_Table.edgeenhance.byEdgeEhance[27];  
	XBYTE[ISP_EEH_SHARP_ARRAY9]   = ct_IQ_Table.edgeenhance.byEdgeEhance[28];  

}

void SetDPC(void)
{
	XBYTE[ISP_DDP_CTRL] = ct_IQ_Table.dpc.bydpc[0]; 	
	XBYTE[ISP_DDP_SEL]		 = ct_IQ_Table.dpc.bydpc[1];   	
	XBYTE[ISP_DP_THD_D1] = ct_IQ_Table.dpc.bydpc[2]; 		
	XBYTE[ISP_DP_BRIGHT_THD_MIN] 	 = ct_IQ_Table.dpc.bydpc[3]; 		
	XBYTE[ISP_DP_BRIGHT_THD_MAX]	 = ct_IQ_Table.dpc.bydpc[4]; 			
	XBYTE[ISP_DP_DARK_THD_MIN]	 = ct_IQ_Table.dpc.bydpc[5]; 		
	XBYTE[ISP_DP_DARK_THD_MAX]	 = ct_IQ_Table.dpc.bydpc[6];   	
	XBYTE[ISP_DDP_BRIGHT_RATE]			 = ct_IQ_Table.dpc.bydpc[7];	
	XBYTE[ISP_DDP_DARK_RATE] 			= ct_IQ_Table.dpc.bydpc[8];
}

void SetCCM(void)
{
	U8 i;
	
	for (i=0; i<9; i++)
	{
		XWORD[ISP_CCM_PARA_ADDR_BASE/2+i] = (U16)g_aCCM[i];
	}

	XBYTE[ISP_CCM_SYNC] = CCM_LOAD;
}


void SetDataSource(U8 type)
{
	U8 byBusWidth;
	U8 byImgSel;
	U8 byYmode;

	byBusWidth = ISP_BUS_WiDTH16_5827;
	byYmode = YUV_MODE;
	
	// 5827 and 5821's width register definition is not the same
#ifdef _ENABLE_MJPEG_
	if(type == FORMAT_TYPE_MJPG)
	{		
		byYmode = YCBCR_MODE;
		if(g_bySensor_YuvMode == YUV422_MODE)
		{
			byImgSel = ISP_IM_SOURCE_YUV|MJPEG_FORMAT|ISP_PATHC_SELECT;// jpeg format.
		}
		else if (g_bySensor_YuvMode == RAW_MODE)
		{
			byImgSel = ISP_IM_SOURCE_RAW|MJPEG_FORMAT;// jpeg format.			
		}
		else		//MJPEG BY PASS
		{
			byImgSel = ISP_IM_SOURCE_YUV | MJPG_BY_PASS;
#ifndef _MIPI_EXIST_
			byBusWidth = ISP_BUS_WiDTH8_JPEG_BYPASS;	
#endif
		}
	}
	else
#endif
#ifdef _ENABLE_M420_FMT_
	if(type == FORMAT_TYPE_M420)
	{
		if(g_bySensor_YuvMode == YUV422_MODE)
		{
			byImgSel = ISP_IM_SOURCE_YUV|MJPEG_FORMAT|ISP_PATHC_SELECT;// m420 format.
		}
		else
		{
			byImgSel = ISP_IM_SOURCE_RAW|MJPEG_FORMAT;// m420 format.
		}
	}
	else
#endif
	if(type == FORMAT_TYPE_YUV420)
	{	
		if(g_bySensor_YuvMode == YUV422_MODE)
		{
			byImgSel = ISP_IM_SOURCE_YUV|YUV420_VENDER_FORMAT|ISP_PATHC_SELECT;// m420 format.
		}
		else
		{
			byImgSel = ISP_IM_SOURCE_RAW|YUV420_VENDER_FORMAT;// m420 format.
		}
	}
	else
	{
		if(g_bySensor_YuvMode == YUV422_MODE)
		{
			byImgSel = ISP_IM_SOURCE_YUV|YUV422_FINAL_FORMAT|ISP_PATHC_SELECT;// jpeg format.
		}
		else
		{
			byImgSel = ISP_IM_SOURCE_RAW|YUV422_FINAL_FORMAT;// jpeg format.
		}
	}

	XBYTE[ISP_BUS_WIDTH] = byBusWidth;
	XBYTE[ISP_IMAGE_SEL]  = byImgSel;
	XBYTE[ISP_YMODE] = byYmode;
}

#ifdef _DECOLOR_TONE_ENABLE_
U8 code g_aGammaSample[28]={0, 4, 8, 12, 16, 20, 24, 28, 32, 40, 48, 56, 64, 72, 80, 88,   96,   104, 112, 120, 128, 144, 160, 176, 192, 208, 224, 240};

void SetRGBGamma(void)		
{	
	U8 i,j,k;
	U8 byaMergeGamma[28] = {0};
	
	k=1;
	for(i=0;i<28;i++)
	{
		for(j=k;j<28;j++)
		{
			if( g_aCTC_Gamma[i]<=g_aGammaSample[j])
			{
				byaMergeGamma[i] = LinearIntp_Byte_Bound(g_aGammaSample[j-1], g_aGamma_Def[j-1], g_aGammaSample[j], g_aGamma_Def[j], g_aCTC_Gamma[i]);
				k=j;
				break;
			}
		}

		if(j==28)
		{
			k=j;
			byaMergeGamma[i] = LinearIntp_Byte_Bound(g_aGammaSample[27], g_aGamma_Def[27], 255, 255, g_aCTC_Gamma[i]);
		}			
	}
	
	for(i =0; i<28; i++)
	{
		XBYTE[ISP_GAMMA_ADDR_BASE+i] = byaMergeGamma[i];
	}

	XBYTE[ISP_GAMMA_SYNC] = 1;

}
#else
void SetRGBGamma(void)
{
	U8 i;

	for(i =0; i<28; i++)
	{
		XBYTE[ISP_GAMMA_ADDR_BASE+i] = g_aGamma_Def[i];
	}

	XBYTE[ISP_GAMMA_SYNC] = 1;
}
#endif

#if !(_CHIP_ID_ & _RTS5829B_)	
void SetRGBGamma2(void)
{
	U8 i;

	for(i =0; i<28; i++)
	{
		XBYTE[ISP_GAMMA_R_PARAMETER+i] = (U8)g_asbyGamma2Cur[0][i];
	}

	for(i =0; i<28; i++)
	{
		XBYTE[ISP_GAMMA_B_PARAMETER+i] = (U8)g_asbyGamma2Cur[1][i];
	}

	XBYTE[ISP_GAMMA_SYNC] = 1;
}
#endif

#if (_CHIP_ID_ & _RTS5829B_)

void SetHWHDR(void)
{

#ifdef _HW_HDR_ENABLE_

	XBYTE[ISP_TGAMMA_CTRL] = ct_IQ_Table.hdr.hdrhw.bytgamma_th | ISP_TGAMMA_EN;
	XBYTE[ISP_TGAMMA_RATE] = ct_IQ_Table.hdr.hdrhw.bytgamma_rate;

	memcpy((U8*)&XBYTE[ISP_HDR_LPF_COEF], ct_IQ_Table.hdr.hdrhw.byhdr_lpf_coef, sizeof(ct_IQ_Table.hdr.hdrhw.byhdr_lpf_coef));

	XBYTE[ISP_HDR_LEVEL] =  0;
	XBYTE[ISP_HDR_HALO_THD] =    ct_IQ_Table.hdr.hdrhw.byhdr_halo_thd;
	XBYTE[ISP_HDR_STEP] = ct_IQ_Table.hdr.hdrhw.byhdr_step;
	
	memcpy((U8*)&XBYTE[ISP_HDR_PARAMETER], ct_IQ_Table.hdr.hdrhw.byhdr_curver, sizeof(ct_IQ_Table.hdr.hdrhw.byhdr_curver));   
	memcpy((U8*)&XBYTE[ISP_HDR_MAX_COEF], ct_IQ_Table.hdr.hdrhw.byhdr_max_curver, sizeof(ct_IQ_Table.hdr.hdrhw.byhdr_max_curver));      

#endif

	XBYTE[ISP_LOC_CONT_LEVEL] =0x40;
	XBYTE[ISP_LOC_CONT_RATE_MIN] = ct_IQ_Table.hdr.hdrhw.bylocal_constrast_rate_min;
	XBYTE[ISP_LOC_CONT_RATE_MAX] = ct_IQ_Table.hdr.hdrhw.bylocal_constrast_rate_max;   
	memcpy((U8*)&XBYTE[ISP_LOC_CONT_PARAMETER], ct_IQ_Table.hdr.hdrhw.bylocal_constrast_curver, sizeof(ct_IQ_Table.hdr.hdrhw.bylocal_constrast_curver));      

	XBYTE[ISP_LOC_CONT_STEP] = ct_IQ_Table.hdr.hdrhw.bylocal_constrast_step; 

	XBYTE[ISP_HDR_ADJ_CTRL] = ISP_START_LOAD_HDRCONT_LEVLE;

}

#endif
/*
*********************************************************************************************************
*                                         Initilize Color Temperature Table
* FUNCTION InitColorTemperatureTable
*********************************************************************************************************
*/
/**
 	Project A,U30,CWF,D50 and D65 AWB gain to K1*x+B1 in Bgain(x axis) Rgain(y axis) profile in order to generate Color Temperature
 	gain table.

  \param
  	none

  \retval
  	none
 *********************************************************************************************************
*/
void InitColorTemperatureTable(void)
{
	U8 i;
	S16 wTrans_deno_k1;

	wTrans_deno_k1 = 16+(S16)g_byAWB_K1*(S16)g_byAWB_K1/16;

	for(i=0; i<6; i++)
	{
		g_abyColorTemperatureTable_gain[i] = (U8)(TransferAWB_GB(-(S8)g_byAWB_K1,(S16)g_wAWB_B1,g_sLscDyn.abyGainB[i],g_sLscDyn.abyGainR[i],wTrans_deno_k1));
	}
}

/*
*********************************************************************************************************
*                                         Get Current Color Temperature
* FUNCTION GetColorTemperature
*********************************************************************************************************
*/
/**
 Estimate the current color temperature with AWB gain.

  \param
  	none

  \retval
  	the current color temperature.
 *********************************************************************************************************
*/
U16 code g_acColorTemperature[6] ={2856,3500,4150,5000,6500,7500};
U16 GetColorTemperature(U8 byBgain, U8 byRgain)
{
	U8 i;
	U8 byCurGain;
	S16 wTrans_deno_k1;
	U8 byIndex_H,byIndex_L;

	wTrans_deno_k1 = 16+(S16)g_byAWB_K1*(S16)g_byAWB_K1/16;
	byCurGain =(U8)(TransferAWB_GB(-(S8)g_byAWB_K1, (S16)g_wAWB_B1, byBgain, byRgain, wTrans_deno_k1));

	for(i=1; i<5; i++)
	{
		if(g_abyColorTemperatureTable_gain[i]<byCurGain)
		{
			break;
		}
	}
	byIndex_H = i;
	byIndex_L = i-1;

#if ((defined _RTK_EXTENDED_CTL_) && (!(defined _EXT_ISO_ONLY_)))
	g_wColorTemperature = LinearIntp_Word(g_abyColorTemperatureTable_gain[byIndex_L], g_acColorTemperature[byIndex_L], g_abyColorTemperatureTable_gain[byIndex_H], g_acColorTemperature[byIndex_H], byCurGain);
	return g_wColorTemperature;
#else
	return LinearIntp_Word(g_abyColorTemperatureTable_gain[byIndex_L], g_acColorTemperature[byIndex_L], g_abyColorTemperatureTable_gain[byIndex_H], g_acColorTemperature[byIndex_H], byCurGain);
#endif
}

/*
*********************************************************************************************************
*                                         Set U&V Offset For Customer Preferred Color
* FUNCTION SetUVOffset
*********************************************************************************************************
*/
/**
 Set U&V offset for customer preferred color adjust. Normal light prefers cold color tone and A light prefers warm color tone.

  \param
  	byU_Offset,	U offset value.
  	byV_Offset,	V offset value.

  \retval
  	none
 *********************************************************************************************************
*/
void SetUVOffset(void)
{
	XBYTE[ISP_U_OFFSET] = (U8)g_byU_Offset_Normal;
	XBYTE[ISP_V_OFFSET] = (U8)g_byV_Offset_Normal;
}

void SetUVColortune(U8 isALight)
{
	if (isALight)
	{
		XBYTE[ISP_UVT_UCENTER] = (U8)g_byIQUVColorTune.byUvColorTune_A[0];
		XBYTE[ISP_UVT_VCENTER] = (U8)g_byIQUVColorTune.byUvColorTune_A[1];
		XBYTE[ISP_UVT_UINC] = (U8)g_byIQUVColorTune.byUvColorTune_A[2];
		XBYTE[ISP_UVT_VINC] = (U8)g_byIQUVColorTune.byUvColorTune_A[3];	
	}	
	else
	{
		XBYTE[ISP_UVT_UCENTER] = (U8)g_byIQUVColorTune.byUvColorTune_D65[0];
		XBYTE[ISP_UVT_VCENTER] = (U8)g_byIQUVColorTune.byUvColorTune_D65[1];
		XBYTE[ISP_UVT_UINC] = (U8)g_byIQUVColorTune.byUvColorTune_D65[2];
		XBYTE[ISP_UVT_VINC] = (U8)g_byIQUVColorTune.byUvColorTune_D65[3];	
	}
}

void SetFalseColorAndMorie(U8 i)
{
	XBYTE[ISP_FCRD_MIN]		= ct_IQ_Table.falsecolor.byFalseColor[i][0];	
	XBYTE[ISP_FCRD_YCUTS]	= ct_IQ_Table.falsecolor.byFalseColor[i][1];			
	XBYTE[ISP_FCRD_CFG]		= ct_IQ_Table.falsecolor.byFalseColor[i][2];		
	XBYTE[ISP_FCRD_MAX]		= ct_IQ_Table.falsecolor.byFalseColor[i][3];		

	XBYTE[ISP_MOIRE_RATE]	= ct_IQ_Table.falsecolor.byFalseColor[i][4];
	g_byMoireThreshold  = ct_IQ_Table.falsecolor.byFalseColor[i][5];
}


void LoadIQDataToISP()
{
	InitIspParamsByResolution(g_wCurFrameWidth,g_wCurFrameHeight);
	
	//BLC module
	//SetBackendBLC();

	//Lens shading Corretion
	SetBackendLSC(g_wSensorCurFormatWidth);

	//Mirco Lens shading Correction
	//SetBackendMicroLSC();		

	// denoise-and sharpness parameter
	SetBKSharpPramVari(g_bySharpParamIndex_Last);

	// texture2
	//SetCornerLoc();
	SetBKTexture2PramVari(g_bySharpParamIndex_Last);
	XBYTE[ISP_RDLOC_ENABLE] = ISP_CRNR_NR_EN;
	XBYTE[ISP_GLOC_ENABLE] = ISP_CRNR_GRGB_EN;
	XBYTE[ISP_INTP_EDGE_SMOOTH] = ISP_CRNR_INTP_EN|ISP_INTP_SMOOTH_EN;
	XBYTE[ISP_LOC_ENABLE] = ISP_CRNR_EEH_EN;	
	XBYTE[ISP_NR_MMM_ENABLE] = ISP_NR_MMM_EN;
		
	// edge enhance
	//SetEdgeEhance();

	// false color and morie
	SetFalseColorAndMorie(g_bySharpParamIndex_Last);

	//dead pixel cancellation
	//SetDPC();
#if ((_CHIP_ID_ & _RTS5829B_) || (_CHIP_ID_ & _RTS5832_))
	//force close dpc backfill function for possible glitter
	XBYTE[ISP_DPC_SEL] = 0;
#endif
	XBYTE[ISP_CONTROL1] |= ISP_DEADPXL_EN;	

	//IIR Filter 
	XBYTE[ISP_CONTROL1] |= ISP_IIRFILTER_EN;	

	//INTP module

	// AE module
	InitAECSetting();

	// AWB module
	SetAWBWindow(g_wSensorCurFormatWidth,g_wSensorCurFormatHeight);
	InitAWBParams();
	InitColorTemperatureTable();
	InitAWBRoughTuneParameters();
		
	// ccm module
	SetCCM();

	// nlc
#if (_CHIP_ID_ & _RTS5829B_)
	memcpy((U8 *)&XBYTE[ISP_NLC_G_PARAMETER], (U8*)&(g_byIQNLC.byG), sizeof(g_byIQNLC.byG));
	memcpy((U8 *)&XBYTE[ISP_NLC_R_PARAMETER], (U8*)&(g_byIQNLC.byRDiff), sizeof(g_byIQNLC.byRDiff));
	memcpy((U8 *)&XBYTE[ISP_NLC_B_PARAMETER], (U8*)&(g_byIQNLC.byBDiff), sizeof(g_byIQNLC.byBDiff));

	XBYTE[ISP_NLC_SEL] = ISP_NLC_STANDALONE_CURVE;
	XBYTE[ISP_NLC_SYNC] = 1;
	XBYTE[ISP_CONTROL3] |= ISP_NLC_EN;
#endif

	// gamma module
	XBYTE[ISP_CONTROL1] |= ISP_GAMMA_EN;
	SetRGBGamma();

	//gamma2
#if !(_CHIP_ID_ & _RTS5829B_)	
	SetRGBGamma2();
	XBYTE[ISP_GAMMA_SEL] = ISP_GAMMA_STANDALONE_CURVE;
#endif

	// hw hdr
#if (_CHIP_ID_ & _RTS5829B_)
	SetHWHDR();
#ifdef _HW_HDR_ENABLE_
	XBYTE[ISP_HDR_ENABLE] = ISP_HDR_MAX_EN | ISP_HALO_REDUCTION_EN | ISP_LOCAL_CONTRAST_EN 
		| ISP_HDR_DOWNSAMPLE_LPF_EN | ISP_HDR_EN ;
#else
	XBYTE[ISP_HDR_ENABLE] = ISP_LOCAL_CONTRAST_EN;
#endif
#endif

	// zoom module
	// individual module place after SetBackendISP		

	// edge enhance

	XBYTE[ISP_CONTROL1] |=ISP_YGAMMA_EN;

	// U,V offset
	SetUVOffset();

	// UV color tune
	SetUVColortune(0);
	XBYTE[ISP_CONTROL3] |=ISP_UV_TUNE_EN;

}

void SetBackendISP(U16 width, U16 wheight)
{
	//AF module
#if (defined _AF_ENABLE_) || (defined _SCENE_DETECTION_)
	InitAFSetting(g_wSensorCurFormatWidth,g_wSensorCurFormatHeight);
#endif
#if  ((defined _ENABLE_ANTI_FLICK_ )||(defined _ENABLE_OUTDOOR_DETECT_))
	InitAutoBandingSetting(g_wSensorCurFormatWidth,g_wSensorCurFormatHeight);
#endif
	if (g_bySensor_YuvMode == RAW_MODE)
	{
		// window setting
		SetCCSFrameSize(g_wSensorWidthBefBLC, g_wSensorHeightBefBLC);
		SetBackendISPFrameSize(g_wSensorWidthBefBLC,g_wSensorHeightBefBLC,0,0);
		LoadIQDataToISP();
	}
	else
	{
		SetCCSFrameSize( width<<1, wheight);
		SetBackendISPFrameSize(width,wheight,0,0);

		// AWB module
		// hemonel 2010-04-09: must set AWB gain to zero

		// ccm module
		// darcy_lu 2011-08-18: must set ccm parameters for YUV sensor
		SetCCM();

		XBYTE[ISP_CONTROL1] |= ISP_YGAMMA_EN;
	}

	SetDataSource(g_byCurFormat);
	
#ifdef _BULK_ENDPOINT_
	if ((g_byCurFormat == FORMAT_TYPE_MJPG) && (g_bySensor_YuvMode != MJPG_MODE))
	{
		XBYTE[EPA_CFG] |= EPA_JPEG_MODE_BURST;
	}
	else
	{
		XBYTE[EPA_CFG] &= ~EPA_JPEG_MODE_BURST;
	}
#endif	


}

// hemonel 2011-01-10: this function to be debuged
void ISPmoireProcess(void)
{
	U8 byByte0,byByte1,byByte2;
	U16 wRnum,wGnum,wBnum;
	U32 dwRsum,dwGsum,dwBsum;
	U8 Rmean =0;
	U8 Gmean = 0;
	U8 Bmean = 0;
	U8 byRRatio,byBRatio;

	//R num
	byByte0 = XBYTE[ISP_MOIRE_NUM_R_L];
	byByte1 = XBYTE[ISP_MOIRE_NUM_R_H];
	ASSIGN_INT(wRnum,byByte1,byByte0);

	//G num
	byByte0 = XBYTE[ISP_MOIRE_NUM_G_L];
	byByte1 = XBYTE[ISP_MOIRE_NUM_G_H];
	ASSIGN_INT(wGnum,byByte1,byByte0);

	//B num
	byByte0 = XBYTE[ISP_MOIRE_NUM_B_L];
	byByte1 = XBYTE[ISP_MOIRE_NUM_B_H];
	ASSIGN_INT(wBnum,byByte1,byByte0);

	if ((wRnum+wGnum+wBnum)>100)
	{

		//R sum
		byByte0 = XBYTE[ISP_MOIRE_SUM_R_L];
		byByte1 = XBYTE[ISP_MOIRE_SUM_R_M];
		byByte2 = XBYTE[ISP_MOIRE_SUM_R_H];
		ASSIGN_LONG(dwRsum,0,byByte2,byByte1,byByte0);

		//G sum
		byByte0 = XBYTE[ISP_MOIRE_SUM_G_L];
		byByte1 = XBYTE[ISP_MOIRE_SUM_G_M];
		byByte2 = XBYTE[ISP_MOIRE_SUM_G_H];
		ASSIGN_LONG(dwGsum,0,byByte2,byByte1,byByte0);

		//B sum
		byByte0 = XBYTE[ISP_MOIRE_SUM_B_L];
		byByte1 = XBYTE[ISP_MOIRE_SUM_B_M];
		byByte2 = XBYTE[ISP_MOIRE_SUM_B_H];
		ASSIGN_LONG(dwBsum,0,byByte2,byByte1,byByte0);

		if ( (wRnum !=0)&&(wGnum !=0)&&(wBnum !=0) )
		{
			Rmean = dwRsum/wRnum ;
			Gmean = dwGsum/wGnum ;
			Bmean = dwBsum/wBnum ;

			byRRatio = (U8)ClipWord( ((U16 ) Rmean<<7)/(U16)Gmean,0,255);
			byBRatio = (U8)ClipWord( ((U16 ) Bmean<<7)/(U16)Gmean,0,255);

			XBYTE[ISP_MOIRE_RATE_R] = byRRatio;
			XBYTE[ISP_MOIRE_RATE_B] = byBRatio;
			XBYTE[ISP_MOIRE_MAX] =  0x14;
			XBYTE[ISP_MOIRE_CTRL] = 0x80 | g_byMoireThreshold;
		}	
	}
	else
	{
		XBYTE[ISP_MOIRE_CTRL] = g_byMoireThreshold;
	}
}

#ifdef _DECOLOR_TONE_ENABLE_
void AETargetDecrease()
{
	/*U8 byStablerange;
	g_byAECStableYUpper =g_byAECStableYUpper*(100-g_byAEDecreaseRate)/100;
	g_byAECStableYLower=g_byAECStableYLower*(100-g_byAEDecreaseRate)/100;
	byStablerange=g_byAEMean_H_Hist-g_byAEMean_L_Hist;
	g_byAEMean_L_Hist =g_byAEMean_L_Hist*(100-g_byAEDecreaseRate)/100;
	g_byAEMean_H_Hist = g_byAEMean_L_Hist+byStablerange;
	byStablerange=g_byAEMean_H_Hist_U-g_byAEMean_L_Hist_U;
	g_byAEMean_L_Hist_U=g_byAEMean_L_Hist_U*(100-g_byAEDecreaseRate)/100;
	g_byAEMean_H_Hist_U = g_byAEMean_L_Hist_U+byStablerange;*/

	// jack_hu 2012-08-16: AE4.0 has changed parameters
	g_byAEC_HistPos_Th_H = (U16)g_byAEC_HistPos_Th_H*(100-(U16)g_byAEDecreaseRate)/100;
	g_byAEC_HistPos_Th_L = (U16)g_byAEC_HistPos_Th_L*(100-(U16)g_byAEDecreaseRate)/100;
	g_byAEC_Mean_Target = (U16)g_byAEC_Mean_Target*(100-(U16)g_byAEDecreaseRate)/100;
	g_byAEC_Mean_Target_L = (U16)g_byAEC_Mean_Target_L*(100-(U16)g_byAEDecreaseRate)/100;
	g_byAEC_Mean_Target_H = (U16)g_byAEC_Mean_Target_H*(100-(U16)g_byAEDecreaseRate)/100;
}
#endif

void InitIspParams(void)
{
	//BLC
	g_byoffsetR = ct_IQ_Table.blc.normal.byOffsetR;
	g_byoffsetG1 = ct_IQ_Table.blc.normal.byOffsetG1;
	g_byoffsetG2 = ct_IQ_Table.blc.normal.byOffsetG2;
	g_byoffsetB = ct_IQ_Table.blc.normal.byOffsetB;	

	// dynamic LSC
	memcpy((U8*)&g_sLscDyn, (U8*)&(ct_IQ_Table.lsc.dynamic), sizeof(IQ_LSC_Dynamic_t));
#ifdef _IQ_TABLE_CALIBRATION_
#ifdef IQ_CAL_DYN_LSC
	if (IQ_CAL_DYN_LSC == (g_byIQPatchGrpOnePatchOne&IQ_CAL_DYN_LSC))
	{
		IQTablePatchDynLSCRate1();
	}
#endif	
#endif


	// AE
	memcpy((U8*)&g_wYpercentage_Th_L, (U8*)&ct_IQ_Table.ae, sizeof(IQ_AE_t));
	// jack_hu 2012-08-16: initiate g_fAEC_CurAdjust_Th, g_wAEC_CurGain_Threshold_15fps & g_wAEC_CurGain_Threshold_30fps
	g_wAEC_CurGain_Threshold_15fps = g_wAEC_Gain_Threshold_15fps;
	g_wAEC_CurGain_Threshold_30fps = g_wAEC_Gain_Threshold_30fps;
#ifdef _DECOLOR_TONE_ENABLE_
	AETargetDecrease();
#endif// _DECOLOR_TONE_ENABLE_

	g_fAEC_HistPos_LH_Target = pow(g_byAEC_HistPos_Th_L, g_fAEC_HighContrast_Exp_L) * pow(g_byAEC_HistPos_Th_H, 1.0-g_fAEC_HighContrast_Exp_L);
	g_fAEC_HistPos_LH_Target_Th = pow(2, g_fAEC_HighContrast_Exp_L) * pow(10, 1.0-g_fAEC_HighContrast_Exp_L);

#ifdef _RTK_EXTENDED_CTL_
	BackupAETarget();
#endif
	
	//AWB
	memcpy((U8*)g_aAWBRoughGain_R, (U8*)&ct_IQ_Table.awb, sizeof(IQ_AWB_t));

	// CCM	
	memcpy(	g_aCCM, ct_IQ_Table.ccm.abyD65, sizeof(g_aCCM));
	memcpy(	g_aCCM_Normal, g_aCCM, sizeof(g_aCCM_Normal));
	g_byCCMState = DAY_CCM;	
	
	//Gamma
	memcpy((U8*)&g_byIQGamma, (U8*)&(ct_IQ_Table.gamma), sizeof(IQ_Gamma_t));
	memcpy(g_aGamma_Def, ct_IQ_Table.gamma.abyNor, sizeof(g_aGamma_Def));
	
	//texture
	memcpy((U8*)&g_byIQSharpness, (U8*)&(ct_IQ_Table.texture.sharpness), sizeof(IQ_Texture_Sharpness_t));
	g_byIQDenoiseTh.byDynamicGainTh0 = ct_IQ_Table.texture.denoise.byDynamicGainTh0;
	g_byIQDenoiseTh.byDynamicGainTh1 = ct_IQ_Table.texture.denoise.byDynamicGainTh1;
	g_byIQDenoiseTh.byDynamicGainTh2 = ct_IQ_Table.texture.denoise.byDynamicGainTh2;		

	// U,V offset
	g_byU_Offset_Normal = ct_IQ_Table.uvoffset.byD65_U;
	g_byV_Offset_Normal = ct_IQ_Table.uvoffset.byD65_V;

	// Hdr
	memcpy((U8*)&g_tHdrTh, (U8*)&(ct_IQ_Table.hdr.hdrth), sizeof(IQ_Hdr_Th_t));
	memcpy((U8*)g_byHDRGamma, (U8*)&(ct_IQ_Table.hdr.hdrfw), sizeof(IQ_HDR_FW_t));

#if !(_CHIP_ID_ & _RTS5829B_)	
	// gamma 2
	memcpy((U8*)g_asbyGamma2Cur, (U8*)(ct_IQ_Table.gamma2.abyNor), sizeof(g_asbyGamma2Cur));	
#else
	// NLC
	memcpy((U8*)&g_byIQNLC, (U8*)&(ct_IQ_Table.nlc), sizeof(IQ_NLC_t));
#endif

	//UV color tune
	memcpy( (U8*)&g_byIQUVColorTune, (U8*)&(ct_IQ_Table.uvcolortune), sizeof(IQ_UV_Color_Tune_t));

#ifdef _ENABLE_ANTI_FLICK_
	g_byABCurPowerLine = PwrLineFreqItem.Def;
#endif
}


void InitIspParamsByResolution(U16 wCurWidth, U16 wCurHeight)
{
	U8 i,j;

	// sharpness parameter adjust based on resolution for MSOC test
	if((wCurWidth <= 424) && (wCurHeight <= 288))
	{
		g_bySharpness_Normal = ct_IQ_Table.texture.sharpness.byCIF;	// hemonel 2011-07-12: move from SetBackendEN() function
		for(i=0; i<4; i++)
		{
			for (j=0; j<28; j++)
			{
				g_abyISParamVaris[i][j] = ct_IQ_Table.texture.denoise.abyVaris_CIF[i][j];
			}
		}
	}
	else if((wCurWidth <= 640) && (wCurHeight <= 480))
	{
		g_bySharpness_Normal = ct_IQ_Table.texture.sharpness.byVGA;	// hemonel 2011-07-12: move from SetBackendEN() function
		for(i=0; i<4; i++)
		{
			for (j=0; j<28; j++)
			{
				g_abyISParamVaris[i][j] = ct_IQ_Table.texture.denoise.abyVaris_VGA[i][j];
			}
		}
	}
	else
	{
		g_bySharpness_Normal = ct_IQ_Table.texture.sharpness.byHD;	// hemonel 2011-07-12: move from SetBackendEN() function
		for(i=0; i<4; i++)
		{
			for (j=0; j<28; j++)
			{
				g_abyISParamVaris[i][j] = ct_IQ_Table.texture.denoise.abyVaris_HD[i][j];
			}
		}
	}

	// hemonel 2011-07-12: move from SetBackendEN() function
	// hemonel 2011-03-02: disable sharpness enable, sharpness bug
	if ((g_wDynamicISPEn&DYNAMIC_SHARPNESS_EN) != DYNAMIC_SHARPNESS_EN)
	{
		g_bySharpness_Def = g_bySharpness_Normal;
	}
}

void SetDynamicISP_FS(void)
{
	if  ( (g_wDynamicISPEn&DYNAMIC_HDR_EN) == DYNAMIC_HDR_EN)
	{
#ifdef _DEBUG_3A_USE_GPIO_
			GPIO_H_DRIVE_HIGH(11-8);
#endif
		HdrAdjust();
#ifdef _DEBUG_3A_USE_GPIO_
			GPIO_H_DRIVE_LOW(11-8);
#endif
	}
}

void SetDynamicISP(U8 byAEC_Gain)
{
	U8 i;

	// BLC
/*	
	if ( (g_wDynamicISPEn&DYNAMIC_BLC_EN) == DYNAMIC_BLC_EN)
	{
		SetBackendBLC();
	}
*/
	// denoise and sharpness parameter
	if ( (g_wDynamicISPEn&DYNAMIC_SHARPPARAM_EN) == DYNAMIC_SHARPPARAM_EN)
	{
		if(byAEC_Gain<=g_byIQDenoiseTh.byDynamicGainTh0)
		{
			g_bySharpParamIndex_Last = 0;
		}
		else if(byAEC_Gain<=g_byIQDenoiseTh.byDynamicGainTh1)
		{
			g_bySharpParamIndex_Last = 1;
		}
		else if(byAEC_Gain<=g_byIQDenoiseTh.byDynamicGainTh2)
		{
			g_bySharpParamIndex_Last = 2;
		}
		else
		{
			g_bySharpParamIndex_Last = 3;
		}
		SetBKSharpPramVari(g_bySharpParamIndex_Last);
		SetBKTexture2PramVari(g_bySharpParamIndex_Last);

		if(g_bySensor_YuvMode == RAW_MODE)
		{
			SetBkSharpness (SharpnessItem.Last);
		}
		else
		{
			SetSensorSharpness(SharpnessItem.Last);
		}
	}

	//Sharpness (0xFD92) may auto ajust according to AE Gain
	if ( (g_wDynamicISPEn&DYNAMIC_SHARPNESS_EN) == DYNAMIC_SHARPNESS_EN)
	{
		g_bySharpness_Def =  LinearIntp_Byte_Bound(g_byIQSharpness.byDynamicGainThL, g_bySharpness_Normal, g_byIQSharpness.byDynamicGainThH, g_byIQSharpness.byLowlux, byAEC_Gain);	

		if(g_bySensor_YuvMode == RAW_MODE)
		{
			SetBkSharpness (SharpnessItem.Last);
		}
		else
		{
			SetSensorSharpness(SharpnessItem.Last);
		}
	}

	// CCM
	if  ( (g_wDynamicISPEn&DYNAMIC_CCM_BRIGHT_EN) == DYNAMIC_CCM_BRIGHT_EN)
	{
		for(i=0; i<9; i++)
		{
			g_aCCM[i] = LinearIntp_Word_Bound_Signed(ct_IQ_Table.ccm.byDynamicLumaGainThL,  g_aCCM_Normal[i], ct_IQ_Table.ccm.byDynamicLumaGainThH,  ct_IQ_Table.ccm.abyLowlux[i], byAEC_Gain);		
		}
		SetCCM();
	}

	// hemonel 2012-02-24: add HDR function from LeoChou
	if  ( (g_wDynamicISPEn&DYNAMIC_HDR_EN) == DYNAMIC_HDR_EN)
	{
	//	HdrAdjust();	// hemonel 2012-04-09: move to SetDynamicISP_FS() function
	}
	else
	{	
		//Set Gamma Parameters 
		//Gamma may auto ajust according to AE Gain
		if  ( (g_wDynamicISPEn&DYNAMIC_GAMMA_EN) == DYNAMIC_GAMMA_EN)
		{
			for (i=0;i<28;i++)
			{	
				g_aGamma_Def[i] =  LinearIntp_Byte_Bound(g_byIQGamma.byDynamicGainThL,  g_byIQGamma.abyNor[i], g_byIQGamma.byDynamicGainThH,  g_byIQGamma.abyLowlux[i], byAEC_Gain);		
#if !(_CHIP_ID_ & _RTS5829B_)	
				g_asbyGamma2Cur[0][i] =  LinearIntp_Word_Bound_Signed(g_byIQGamma.byDynamicGainThL,  ct_IQ_Table.gamma2.abyNor[0][i], g_byIQGamma.byDynamicGainThH,  ct_IQ_Table.gamma2.abyLowlux[0][i], byAEC_Gain);	
				g_asbyGamma2Cur[1][i] =  LinearIntp_Word_Bound_Signed(g_byIQGamma.byDynamicGainThL,  ct_IQ_Table.gamma2.abyNor[1][i], g_byIQGamma.byDynamicGainThH,  ct_IQ_Table.gamma2.abyLowlux[1][i], byAEC_Gain);	
#endif			
			}		
			SetRGBGamma();	
#if !(_CHIP_ID_ & _RTS5829B_)	
			SetRGBGamma2();
#endif
		}	
	}

	SetSensorDynamicISP(byAEC_Gain);
}

void SetDynamicISP_AWB(U16 wColorTempature)
{
	U8 i;

	DBG(("wColorTempature = %u\n",wColorTempature));

	// CCM switch with A and D65
	if ( (g_wDynamicISPEn&DYNAMIC_CCM_CT_EN) == DYNAMIC_CCM_CT_EN)
	{
		if(wColorTempature < ct_IQ_Table.ccm.wDynamicCCMA_Th)// change for  3500k light is test in lync ,set g_OV9726_CCM_A  to fit 3500k condition , zhangbo 20110701
		{
			if(g_byCCMState != A_CCM)
			{
				// If dynamic CCM with AE gain, update g_aCCM_Normal
				// Else, update g_aCCM and SetCCM
				if  ( (g_wDynamicISPEn&DYNAMIC_CCM_BRIGHT_EN) == DYNAMIC_CCM_BRIGHT_EN)
				{
					for (i=0; i<9; i++)
					{
						g_aCCM_Normal[i]= ct_IQ_Table.ccm.abyA[i];
					}
				}
				else
				{
					for (i=0; i<9; i++)
					{
						g_aCCM[i]= ct_IQ_Table.ccm.abyA[i];				
					}
					SetCCM();
				}

				g_byCCMState = A_CCM;
			}
		}
		else if(wColorTempature > ct_IQ_Table.ccm.wDynamicCCMD65_th)
		{
			if(g_byCCMState != DAY_CCM)
			{
				// If dynamic CCM with AE gain, update g_aCCM_Normal
				// Else, update g_aCCM and SetCCM
				if  ( (g_wDynamicISPEn&DYNAMIC_CCM_BRIGHT_EN) == DYNAMIC_CCM_BRIGHT_EN)
				{
					for (i=0; i<9; i++)
					{
						g_aCCM_Normal[i]= ct_IQ_Table.ccm.abyD65[i];
					}
				}
				else
				{
					for (i=0; i<9; i++)
					{
						g_aCCM[i]= ct_IQ_Table.ccm.abyD65[i];				
					}
					SetCCM();
				}

				g_byCCMState = DAY_CCM;
			}
		}
	}

	// U,V offset
	if ( (g_wDynamicISPEn&DYNAMIC_UVOFFSET_EN) == DYNAMIC_UVOFFSET_EN)
	{
		if(wColorTempature < ct_IQ_Table.uvoffset.wDynamicUV_A_Th)
		{
			g_byU_Offset_Normal = ct_IQ_Table.uvoffset.byA_U;
			g_byV_Offset_Normal = ct_IQ_Table.uvoffset.byA_V;			
		}
		else if(wColorTempature > ct_IQ_Table.uvoffset.wDynamicUV_D65_Th)
		{
			g_byU_Offset_Normal = ct_IQ_Table.uvoffset.byD65_U;
			g_byV_Offset_Normal = ct_IQ_Table.uvoffset.byD65_V;
		}
		SetUVOffset();
	}

	// UV color tune
	if ( (g_wDynamicISPEn&DYNAMIC_UVCOLORTUNE_EN) == DYNAMIC_UVCOLORTUNE_EN)
	{
		if(wColorTempature < g_byIQUVColorTune.wDynamicUV_A_Th)
		{
			SetUVColortune(1);
		}
		else if(wColorTempature > g_byIQUVColorTune.wDynamicUV_D65_Th)
		{
			SetUVColortune(0);
		}
	}

	SetSensorDynamicISP_AWB(wColorTempature);
}


void InitAWBRoughTuneParameters(void)
{
	XBYTE[ISP_AWB_ROUGH_GAIN_R_1]=g_aAWBRoughGain_R[0];// A
	XBYTE[ISP_AWB_ROUGH_GAIN_R_2]=g_aAWBRoughGain_R[1];//U35
	XBYTE[ISP_AWB_ROUGH_GAIN_R_3]=g_aAWBRoughGain_R[2];//CWF
	XBYTE[ISP_AWB_ROUGH_GAIN_R_4]=g_aAWBRoughGain_R[3];//D50
	XBYTE[ISP_AWB_ROUGH_GAIN_R_5]=g_aAWBRoughGain_R[4];//D65
	XBYTE[ISP_AWB_ROUGH_GAIN_R_6]=g_aAWBRoughGain_R[5];//

	XBYTE[ISP_AWB_ROUGH_GAIN_B_1]=g_aAWBRoughGain_B[0];
	XBYTE[ISP_AWB_ROUGH_GAIN_B_2]=g_aAWBRoughGain_B[1];
	XBYTE[ISP_AWB_ROUGH_GAIN_B_3]=g_aAWBRoughGain_B[2];
	XBYTE[ISP_AWB_ROUGH_GAIN_B_4]=g_aAWBRoughGain_B[3];
	XBYTE[ISP_AWB_ROUGH_GAIN_B_5]=g_aAWBRoughGain_B[4];
	XBYTE[ISP_AWB_ROUGH_GAIN_B_6]=g_aAWBRoughGain_B[5];

	XBYTE[ISP_AWB_ROUGH_RG_MAX]=35;
	XBYTE[ISP_AWB_ROUGH_RG_MIN]=29;
	XBYTE[ISP_AWB_ROUGH_BG_MAX]=35;
	XBYTE[ISP_AWB_ROUGH_BG_MIN]=29;

}

void OpenISPMJPEGClock(void)
{
	ISP_Clock_Switch(SWITCH_ON);
	CHANGE_ISP_CLK(ISP_CLOCK_60M);
	JPEG_Clock_Switch(SWITCH_ON);
	CHANGE_JPEG_CLK(JPEG_CLOCK_60M);
}

void CloseISPMJPEGClock(void)
{
	ISP_Clock_Switch(SWITCH_OFF);
	JPEG_Clock_Switch(SWITCH_OFF);
}


void SF_IRQ_InitISP(void)
{
	InitIspParams();
}

void SF_IRQ_InitISPHW(void)
{
	// To speedup preview, move some ISP init to here
	OpenISPMJPEGClock();
	
	if (g_bySensor_YuvMode == RAW_MODE)
	{
		SetBackendBLC();
		SetBackendMicroLSC();
		SetEdgeEhance();
		SetCornerLoc();
		SetDPC();
	}	

#ifdef _ENABLE_MJPEG_
	// Init jpeg module
	InitJpegTable();
#endif

	CloseISPMJPEGClock();
}

void InitISP(U8 byPG_EN)
{
	if(byPG_EN == 0)
	{
		SoftInterruptOpenBfrConnect(SF_IRQ_INIT_ISP);
	}

	SoftInterruptOpenBfrConnect(SF_IRQ_INIT_ISP_HW);	
}


