/*
*********************************************************************************************************
*                                       Realtek's PcCamera Project
*
*											Layer: PCCAMERA
*											Module: GLOBAL VARIABLE DEFINITION MODULE
*									(c) Copyright 2003-2004, Realtek Corp.
*                                           All Rights Reserved
*
*                                                  V1.0
*
* File : GlobalAbsIspVar.h
*********************************************************************************************************
*/
/**
*******************************************************************************
  \file GlobalAbsIspVar.h
  \brief This file defines ISP global variable.

GENERAL DESCRIPTION:

   This file defines ISP global variable. These variables are at predefined global adrress. ISP adjust tool can translated this file into
   ISP configure.ini file for ISP IQ fine tune.
   For ISP adjust tool translating, it defines some string indicator.
   "//#show" : ISP adjust tool will show this global
   "//#hide" : ISP adjust tool will not show this global
   "//###[ISP base address table]" : this is followed by ISP variable base address, these base address must sync with the real address
   "//#[section name]" : ISP adjust tool will show section name and combine followed variable into one group
   "//#[section name] DEFINED_MACRO1 DEFINED_MACRO2 ...": ISP adjust tool will parse the following variable based on marco

   "//#struct[struct variable name]": struct define
   example:
   //#struct[g_byIQHdr]
    //# U8 byBL_L_TH;
    //# U8 byBL_U_TH;
    //# U8 byHDR_HistLB_U;
    //# U8 abyHDR_Gamma[28];
    //#struct_end

EXTERNALIZED FUNCTIONS:
	nothing

Copyright (c) 2010-2011 by Realtek Incorporated.  All Rights Reserved.


*  \version 1.0
*  \date    2010-2011

*******************************************************************************/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

when        who     what, where, why
--------    -----   ----------------------------------------------------------
2010-11-24 hemonel create this file


===========================================================================*/

/*===========================================================================

                     Global Variable Fix Address FOR MODULE

===========================================================================*/
#ifndef _GLOBALABSISPVAR_H_
#define _GLOBALABSISPVAR_H_

#define GLOBAL_ISP_ABS_VARIABLE_BASE_ADDRESS 0xD100

// ISP address space
#define G_ISP_SYS_SPACE		8
#ifdef _AWB_SAMEBLOCK_
#define G_ISP_AWB_SPACE		210+56
#else
#define G_ISP_AWB_SPACE		160+56
#endif

#if (defined _FACIAL_EXPOSURE_)
#define G_ISP_AE_SPACE			265+53
#elif (defined _FACIALAEWINSET_XU_)
#define G_ISP_AE_SPACE			252+53
#else
#define G_ISP_AE_SPACE			244+53
#endif

#ifdef _AF_ENABLE_
#define G_ISP_AF_SPACE			155
#else
#define G_ISP_AF_SPACE			0
#endif

#define G_ISP_3AR_SPACE		0x00
#define G_ISP_SHARP_SPACE		118
#define G_ISP_CCM_SPACE		37
#ifdef _DECOLOR_TONE_ENABLE_
#define G_ISP_GAMMA_SPACE		44+28
#else
#define G_ISP_GAMMA_SPACE		44
#endif

#if ((defined _ENABLE_ANTI_FLICK_) ||(defined _ENABLE_OUTDOOR_DETECT_) \
	|| (defined _LENOVO_IDEA_EYE_))
#define G_ISP_AB_SPACE			80
#else
#define G_ISP_AB_SPACE			0
#endif
#define G_ISP_OTHERS_SPACE	315

/*lint -save -e10*/

// ISP base address
#define G_ISPBASE_SYS			(0)
#define G_ISPBASE_AWB			(G_ISPBASE_SYS+G_ISP_SYS_SPACE)
#define G_ISPBASE_AE			(G_ISPBASE_AWB+G_ISP_AWB_SPACE)
#define G_ISPBASE_AF			(G_ISPBASE_AE+G_ISP_AE_SPACE)
#define G_ISPBASE_SHARP		(G_ISPBASE_AF+G_ISP_AF_SPACE+G_ISP_3AR_SPACE)
#define G_ISPBASE_CCM			(G_ISPBASE_SHARP+G_ISP_SHARP_SPACE)
#define G_ISPBASE_GAMMA		(G_ISPBASE_CCM+G_ISP_CCM_SPACE)
#define G_ISPBASE_AB			(G_ISPBASE_GAMMA+G_ISP_GAMMA_SPACE)
#define G_ISPBASE_OTHERISP	(G_ISPBASE_AB+G_ISP_AB_SPACE)

// AWB sub base address
#define G_ISPSIZE_AWB_IQ			(56)	
#define G_ISPBASE_AWB_OTHERS		(G_ISPBASE_AWB+G_ISPSIZE_AWB_IQ)	

// AE sub base address
#define G_ISPSIZE_AE_IQ			(53)	
#define G_ISPBASE_AE_OTHERS		(G_ISPBASE_AE+G_ISPSIZE_AE_IQ)

// Other ISP sub base address
#define G_ISPSIZE_BLC				(8)
#define G_ISPSIZE_MOIRE			(1)
#ifdef _SCENE_DETECTION_
#define G_ISPSIZE_AS				(58+10)
#else
#define G_ISPSIZE_AS				(10)
#endif
#ifdef _STILL_IMG_BACKUP_SETTING_
#define G_ISPSIZE_OTHER				(102)
#else
#define G_ISPSIZE_OTHER				(101)
#endif
#ifdef _NEW_AUTO_LSC_FUNCTION_
#define G_ISPSIZE_LSC				(85)
#else
#define G_ISPSIZE_LSC				(2)
#endif
#define G_ISPSIZE_HDR				(38)

#ifdef _RTK_EXTENDED_CTL_
#define G_ISPSIZE_RTKEXTISP		(100)
#else
#define G_ISPSIZE_RTKEXTISP		(0)
#endif

#if !(_CHIP_ID_ & _RTS5829B_)	
#define G_ISPSIZE_IQSTRUCT			(231)
#else
#define G_ISPSIZE_IQSTRUCT			(175)
#endif

#define G_ISPBASE_BLC				(G_ISPBASE_OTHERISP)
#define G_ISPBASE_MOIRE			(G_ISPBASE_BLC+G_ISPSIZE_BLC)
#define G_ISPBASE_AS				(G_ISPBASE_MOIRE+G_ISPSIZE_MOIRE)
#define G_ISPBASE_OTHER			(G_ISPBASE_AS+G_ISPSIZE_AS)
#define G_ISPBASE_LSC				(G_ISPBASE_OTHER+G_ISPSIZE_OTHER)
#define G_ISPBASE_HDR				(G_ISPBASE_LSC+G_ISPSIZE_LSC)
#define G_ISPBASE_RTKEXTISP		(G_ISPBASE_HDR+G_ISPSIZE_HDR)
#define G_ISPBASE_IQSTRUCT		(G_ISPBASE_RTKEXTISP+G_ISPSIZE_RTKEXTISP)

#define AT_ISP(addr)_at_ (GLOBAL_ISP_ABS_VARIABLE_BASE_ADDRESS+(addr))

#define AT_ISPBASE_SYS(addr)   		AT_ISP(addr+G_ISPBASE_SYS) 

#define AT_ISPBASE_AWB_IQ(addr)   	AT_ISP(addr+G_ISPBASE_AWB) 
#define AT_ISPBASE_AWB_OTHER(addr)   AT_ISP(addr+G_ISPBASE_AWB_OTHERS) 
                                      
#define AT_ISPBASE_AE_IQ(addr)   		AT_ISP(addr+G_ISPBASE_AE)
#define AT_ISPBASE_AE_OTHER(addr)   	AT_ISP(addr+G_ISPBASE_AE_OTHERS)

#define AT_ISPBASE_AF(addr)   		AT_ISP(addr+G_ISPBASE_AF)

#define AT_ISPBASE_SHARP(addr)  	AT_ISP(addr+G_ISPBASE_SHARP)
#define AT_ISPBASE_CCM(addr)   		AT_ISP(addr+G_ISPBASE_CCM)
#define AT_ISPBASE_GAMMA(addr)   	AT_ISP(addr+G_ISPBASE_GAMMA)
#define AT_ISPBASE_AB(addr) AT_ISP(addr+G_ISPBASE_AB)
#define AT_ISPBASE_BLC(addr)		AT_ISP(addr+G_ISPBASE_BLC)
#define AT_ISPBASE_MOIRE(addr)		AT_ISP(addr+G_ISPBASE_MOIRE)
#define AT_ISPBASE_AS(addr)		AT_ISP(addr+G_ISPBASE_AS)
#define AT_ISPBASE_OTHER(addr)		AT_ISP(addr+G_ISPBASE_OTHER)
#define AT_ISPBASE_LSC(addr)		AT_ISP(addr+G_ISPBASE_LSC)
#define AT_ISPBASE_HDR(addr)		AT_ISP(addr+G_ISPBASE_HDR)
#define AT_ISPBASE_RTKEXTISP(addr)	AT_ISP(addr+G_ISPBASE_RTKEXTISP)
#define AT_ISPBASE_IQSTRUCT(addr)	AT_ISP(addr+G_ISPBASE_IQSTRUCT)

// this section must be synced with the actual adrress when use ISP adjust tool
//###[ISP base address table]
//# AT_ISPBASE_SYS				g_wDynamicISPEn
//# AT_ISPBASE_AWB_IQ 			g_aAWBRoughGain_R[6] 
//# AT_ISPBASE_AWB_OTHER		g_byAWBWin_Rgain_FirstProject[5][5]
//# AT_ISPBASE_AE_IQ   			g_wYpercentage_Th_L
//# AT_ISPBASE_AE_OTHER			g_byAEC_Control
//# AT_ISPBASE_AF				g_byAFStatus
//# AT_ISPBASE_SHARP  			g_bySharpness_Def
//# AT_ISPBASE_CCM  			g_byCCMState
//# AT_ISPBASE_GAMMA   			g_aGamma_Def[28]
//# AT_ISPBASE_BLC				g_byoffsetR
//# AT_ISPBASE_MOIRE			g_byMoireThreshold
//# AT_ISPBASE_AS				g_byWhite_Edge
//# AT_ISPBASE_AB				g_byISPABStaticsEn	
//# AT_ISPBASE_OTHER			g_byU_Offset_Normal
//# AT_ISPBASE_LSC				g_byLastIllumCT
//# AT_ISPBASE_HDR				g_tHdrTh
//# AT_ISPBASE_RTKEXTISP		g_byAEMeanTarget_Backup
//# AT_ISPBASE_IQSTRUCT			g_sLscDyn



//#[ISP system control]
U16 g_wDynamicISPEn					AT_ISPBASE_SYS(0);
//U16 g_wAE_Hold_Timer					AT_ISPBASE_SYS(2);		// reserved for Face exposure AP
U8 g_byQtableCurScale						AT_ISPBASE_SYS(2);
U8 g_byQtableScale						AT_ISPBASE_SYS(4);
U8 g_byLPMSetbyAP						AT_ISPBASE_SYS(5);
U16 g_wHWMbyAp						AT_ISPBASE_SYS(6);

//[AWB]
//#[AWB grayworld]
U8 g_aAWBRoughGain_R[6]			AT_ISPBASE_AWB_IQ(0);
U8 g_aAWBRoughGain_B[6]			AT_ISPBASE_AWB_IQ(6);
U8 g_byAWB_K1						AT_ISPBASE_AWB_IQ(12);	
U16 g_wAWB_B1						AT_ISPBASE_AWB_IQ(13);	
U16 g_wAWB_B2						AT_ISPBASE_AWB_IQ(15);	
S8 g_byAWB_K3						AT_ISPBASE_AWB_IQ(17);	
S16 g_wAWB_B3						AT_ISPBASE_AWB_IQ(18);	
S8 g_byAWB_K4						AT_ISPBASE_AWB_IQ(20);	
S16 g_wAWB_B4						AT_ISPBASE_AWB_IQ(21);	
S8 g_byAWB_K5						AT_ISPBASE_AWB_IQ(23);
S16 g_wAWB_B5						AT_ISPBASE_AWB_IQ(24);
S8 g_byAWB_K6						AT_ISPBASE_AWB_IQ(26);
S16 g_wAWB_B6						AT_ISPBASE_AWB_IQ(27);
U16 g_wAWB_B_Up					AT_ISPBASE_AWB_IQ(29);
U16 g_wAWB_B_Down					AT_ISPBASE_AWB_IQ(31);
S16 g_swAWB_B_Left				AT_ISPBASE_AWB_IQ(33);
S16 g_swAWB_B_Right				AT_ISPBASE_AWB_IQ(35);
U8 g_byAWB_Win_Bright_Max		AT_ISPBASE_AWB_IQ(37);
U8 g_byAWB_Win_Bright_Min		AT_ISPBASE_AWB_IQ(38);
U16 g_wAWB_RGB_SumTh				AT_ISPBASE_AWB_IQ(39);
U16 g_wTemPixelNumThread_L		AT_ISPBASE_AWB_IQ(41);
U8 g_byAWBFineMax_RG				AT_ISPBASE_AWB_IQ(43);
U8 g_byAWBFineMin_RG				AT_ISPBASE_AWB_IQ(44);
U8 g_byAWBFineMax_BG				AT_ISPBASE_AWB_IQ(45);
U8 g_byAWBFineMin_BG				AT_ISPBASE_AWB_IQ(46);
U8 g_byAWBFineMax_Bright			AT_ISPBASE_AWB_IQ(47);
U8 g_byAWBFineMin_Bright			AT_ISPBASE_AWB_IQ(48);
U8 g_byFtGainTh					AT_ISPBASE_AWB_IQ(49);
U16 g_wAWBGainDiffTh				AT_ISPBASE_AWB_IQ(50);
U8 g_byAWBGainStep				AT_ISPBASE_AWB_IQ(52);
U8 g_byAWBFixed_YmeanTh			AT_ISPBASE_AWB_IQ(53);
U8 g_byAWBColorDiff_Th			AT_ISPBASE_AWB_IQ(54);
U8 g_byAWBDiffWindowsTh			AT_ISPBASE_AWB_IQ(55);

//#[AWB Other parameters]
U8 g_byAWBWin_Rgain_FirstProject[5][5]   	AT_ISPBASE_AWB_OTHER(0);
U8 g_byAWBWin_Bgain_FirstProject[5][5]		AT_ISPBASE_AWB_OTHER(25);
U8 g_byAWBWin_Brightness[5][5]				AT_ISPBASE_AWB_OTHER(50);
U16  g_wProjectGR					AT_ISPBASE_AWB_OTHER(75);
U16  g_wProjectGB					AT_ISPBASE_AWB_OTHER(77);
U16 g_wGWLastGR					AT_ISPBASE_AWB_OTHER(79);
U16 g_wGWLastGB					AT_ISPBASE_AWB_OTHER(81);
U16 g_wFtGR							AT_ISPBASE_AWB_OTHER(83);
U16 g_wFtGB							AT_ISPBASE_AWB_OTHER(85);
U16  g_wAWBFinalGainR				AT_ISPBASE_AWB_OTHER(87);
U16  g_wAWBFinalGainB				AT_ISPBASE_AWB_OTHER(89);
U16 g_wAWBRGain_Last				AT_ISPBASE_AWB_OTHER(91);
U16 g_wAWBGGain_Last				AT_ISPBASE_AWB_OTHER(93);
U16 g_wAWBBGain_Last				AT_ISPBASE_AWB_OTHER(95);
U8 g_byAWBWinInitRGain[5][5]		AT_ISPBASE_AWB_OTHER(97);
U8 g_byAWBWinInitBGain[5][5]		AT_ISPBASE_AWB_OTHER(122);
U8 g_byISPAWBStaticsEn				AT_ISPBASE_AWB_OTHER(147);
U8 g_byAWB_State					AT_ISPBASE_AWB_OTHER(148);
U8 g_byAWBEnterStableDelay		AT_ISPBASE_AWB_OTHER(149);
U8 g_byAWB_Hold					AT_ISPBASE_AWB_OTHER(150);
U8 g_byAWB_SameBlock_Hold			AT_ISPBASE_AWB_OTHER(151);
U8 g_byAWBDiffWindows				AT_ISPBASE_AWB_OTHER(152);	
U16 g_wAWBSTnum						AT_ISPBASE_AWB_OTHER(153);
U8 g_byAWBGainStep_Max			AT_ISPBASE_AWB_OTHER(155);
U8 g_byAWBStepNumbers				AT_ISPBASE_AWB_OTHER(156);
U8 g_byAWBExitStableDelay				AT_ISPBASE_AWB_OTHER(157);
U8 g_byAWBStableDelay_Cfg			AT_ISPBASE_AWB_OTHER(158);
U8 g_byAWB_AdjustMode				AT_ISPBASE_AWB_OTHER(159);
#ifdef _AWB_SAMEBLOCK_
U8 g_byAWBWinLastGR[5][5]			AT_ISPBASE_AWB_OTHER(160);
U8 g_byAWBWinLastGB[5][5]			AT_ISPBASE_AWB_OTHER(185);
#endif

//[AEC]
//#[AEC Target]
U16 g_wYpercentage_Th_L				AT_ISPBASE_AE_IQ(0);
U16 g_wYpercentage_Th_H				AT_ISPBASE_AE_IQ(2);
U8 g_byAEC_Mean_Target_L			AT_ISPBASE_AE_IQ(4);
U8 g_byAEC_Mean_Target				AT_ISPBASE_AE_IQ(5);
U8 g_byAEC_Mean_Target_H			AT_ISPBASE_AE_IQ(6);
U8 g_byAEC_HistPos_Th_L				AT_ISPBASE_AE_IQ(7);
U8 g_byAEC_HistPos_Th_H				AT_ISPBASE_AE_IQ(8);
U8 g_byAE_Hist_MeanTargetDec_Extent	AT_ISPBASE_AE_IQ(9);
U8 g_byAEMaxStep50Hz				AT_ISPBASE_AE_IQ(10);
U8 g_byAEMaxStep60Hz				AT_ISPBASE_AE_IQ(11);
U16 g_wAECGlobalgainmax			AT_ISPBASE_AE_IQ(12);
U16 g_wAEC_Fps_Coef				AT_ISPBASE_AE_IQ(14);
U16 g_wAEC_Gain_Threshold_15fps		AT_ISPBASE_AE_IQ(16);
U16 g_wAEC_Gain_Threshold_30fps		AT_ISPBASE_AE_IQ(18);
U8 g_byAEC_HighLight_Thread			AT_ISPBASE_AE_IQ(20);
U8 g_byAE_WinWeight[25]			AT_ISPBASE_AE_IQ(21);
float g_fAEC_Adjust_Th				AT_ISPBASE_AE_IQ(46);
U8 g_byAE_Delay_Cfg						AT_ISPBASE_AE_IQ(50);
U8 g_byAEC_Stable_BlockDiffTh			AT_ISPBASE_AE_IQ(51);	
U8 g_byAEC_Stable_BlockNumTh			AT_ISPBASE_AE_IQ(52);
U8 g_byFaceAEmode;

//#[AEC Limit] _DECOLOR_TONE_ENABLE_
U8 g_byAEC_Control						AT_ISPBASE_AE_OTHER(0);
#ifdef _DECOLOR_TONE_ENABLE_
U8 g_byAEDecreaseRate					AT_ISPBASE_AE_OTHER(1);	
#endif
U8 g_byAEMeanValue					AT_ISPBASE_AE_OTHER(2);
U8 g_byAEC_HistPos_L					AT_ISPBASE_AE_OTHER(3);
U8 g_byAEC_HistPos_H					AT_ISPBASE_AE_OTHER(4);
U8 g_byAE_OverExpTargetDecVal	 	AT_ISPBASE_AE_OTHER(5);
U16 g_wAE_Hist_StNum[64]			AT_ISPBASE_AE_OTHER(6);
U8 g_byAE_WinYMean[25]				AT_ISPBASE_AE_OTHER(134);
U8 g_byAEC_Backlight_Ratio				AT_ISPBASE_AE_OTHER(159);
U16 g_wAECExposureRowMax           		AT_ISPBASE_AE_OTHER(160);
U16 g_wAEC_LineNumber					AT_ISPBASE_AE_OTHER(162);	
U8 g_byAEC_Mode						AT_ISPBASE_AE_OTHER(164);
U8 g_byAECStatus						AT_ISPBASE_AE_OTHER(165);
float g_fSensorRowTimes					AT_ISPBASE_AE_OTHER(166);
float g_fAEC_EtGain						AT_ISPBASE_AE_OTHER(170);
U16 g_wAFRInsertDummylines			AT_ISPBASE_AE_OTHER(174);	
float g_fCurExpTime						AT_ISPBASE_AE_OTHER(176);	
float g_fAEC_SmoothExpectYmeanCmp_all	AT_ISPBASE_AE_OTHER(180);	
float g_fAEC_SetEtgain					AT_ISPBASE_AE_OTHER(184);	
U8 g_byAEC_AdjustStatus					AT_ISPBASE_AE_OTHER(188);	
float g_fAEC_Min_ExpTime					AT_ISPBASE_AE_OTHER(189);	
U8 g_byAEForce_Adjust					AT_ISPBASE_AE_OTHER(193);
U8 g_byISPAEStaticsEn					AT_ISPBASE_AE_OTHER(194);
U8 g_byAE_Stable_WinYMean[25]			AT_ISPBASE_AE_OTHER(195);	
U8 g_byAE_StableOut_DelayCnt			AT_ISPBASE_AE_OTHER(220);
float g_fAEC_Gain						AT_ISPBASE_AE_OTHER(221);
U16 g_wAEC_CurGain_Threshold_15fps		AT_ISPBASE_AE_OTHER(225);
U16 g_wAEC_CurGain_Threshold_30fps		AT_ISPBASE_AE_OTHER(227);
float g_fAEC_HistPos_LH_Target			AT_ISPBASE_AE_OTHER(229);
float g_fAEC_HistPos_LH_Target_Th		AT_ISPBASE_AE_OTHER(233);
float g_fAEC_HighContrast_Exp_L			AT_ISPBASE_AE_OTHER(237);

#ifdef _AE_NEW_SPEED_ENABLE_
U8 g_byAE_SpeedMode					AT_ISPBASE_AE_OTHER(241);
U8 g_byAE_LastStatus					AT_ISPBASE_AE_OTHER(242);
U8 g_byAE_StaticsDelay				AT_ISPBASE_AE_OTHER(243);
#else
U8 g_byAEC_AdjustMode					AT_ISPBASE_AE_OTHER(241);
#endif

#ifdef _FACIAL_EXPOSURE_
U16 g_wZoomStartX						AT_ISPBASE_AE_OTHER(244);
U16 g_wZoomStartY						AT_ISPBASE_AE_OTHER(246);
U16 g_wZoomScale_h						AT_ISPBASE_AE_OTHER(248);
U16 g_wZoomScale_v						AT_ISPBASE_AE_OTHER(250);
U16 g_wAE_WinStartX					AT_ISPBASE_AE_OTHER(252);
U16 g_wAE_WinStartY					AT_ISPBASE_AE_OTHER(254);
U16 g_wAE_WinWidth						AT_ISPBASE_AE_OTHER(256);
U16 g_wAE_WinHeight					AT_ISPBASE_AE_OTHER(258);
U8 g_byAEFaceWindowSet				AT_ISPBASE_AE_OTHER(260);
U8 g_bySimilar							AT_ISPBASE_AE_OTHER(261);
U8 g_bySimilar_last					AT_ISPBASE_AE_OTHER(262);
U8 g_byFaceExposure_En				AT_ISPBASE_AE_OTHER(263);
U8 g_byFaceLostTime					AT_ISPBASE_AE_OTHER(264);
#endif

#ifdef _FACIALAEWINSET_XU_
U8 g_byXU_FacialAEWin_Set 				AT_ISPBASE_AE_OTHER(244);
U8 g_byaFacial_AE_Parameters[7] 			AT_ISPBASE_AE_OTHER(245);
#endif

//#[AF] _AF_ENABLE_
#ifdef _AF_ENABLE_
U8 g_byAFStatus 							AT_ISPBASE_AF(0);
U8 g_byAFControl							AT_ISPBASE_AF(1);
U16 g_wAF_CurPosition 					AT_ISPBASE_AF(2);
U32 g_dwAFCurrentSharp					AT_ISPBASE_AF(4);
U8 g_bySearchingDir 						AT_ISPBASE_AF(8);
U8 g_byAF_RoughStep					AT_ISPBASE_AF(9);
U8 g_byAF_FineStep						AT_ISPBASE_AF(10);
U8 g_byAFSearchStep						AT_ISPBASE_AF(11);
U32 g_dwAFSharpLast1						AT_ISPBASE_AF(12);
U32 g_dwAFSharpLast2						AT_ISPBASE_AF(16);
U32 g_dwAFSharpLast3						AT_ISPBASE_AF(20);
U8 g_byAFPosIndex						AT_ISPBASE_AF(24);
U8 g_byAFPosIndexLast1					AT_ISPBASE_AF(25);
U8 g_byAFPosIndexLast2					AT_ISPBASE_AF(26);
U8 g_byAFSerachDirectionChangeTimes		AT_ISPBASE_AF(27);
U8 g_byAFSearchTimes					AT_ISPBASE_AF(28);
U8 g_byAFSharpPeakTh2					AT_ISPBASE_AF(29);
U8 g_byAFSharpPeakTh1					AT_ISPBASE_AF(30);
U8 g_byAFMaxSharpPos					AT_ISPBASE_AF(31);
U32 g_dwAFMaxSharpness					AT_ISPBASE_AF(32);
U8 g_byAF_Stable_Position 				AT_ISPBASE_AF(36);
U32 g_dwAF_Stable_Sharpness 				AT_ISPBASE_AF(37);
U8  g_byAFStatTime						AT_ISPBASE_AF(41);
U8  g_byAFStatTime2						AT_ISPBASE_AF(42);
float g_fAF_SharpDiff_High_Th				AT_ISPBASE_AF(43);
float g_fAF_SharpDiff_Low_Th				AT_ISPBASE_AF(47);
U8 g_byAF_ColorDiff_Th					AT_ISPBASE_AF(51);
U8  g_byAFStable_RMean[5][5]				AT_ISPBASE_AF(52);
U8  g_byAFStable_GMean[5][5]				AT_ISPBASE_AF(77);
U8  g_byAFStable_BMean[5][5]				AT_ISPBASE_AF(102);
U8 g_byAFEnterRoughSearch				AT_ISPBASE_AF(127);
U8 g_byAFDiffTimes_Th					AT_ISPBASE_AF(128);
U16 g_wAFRoughEntrance_Th				AT_ISPBASE_AF(129);
U8 g_byAFTotalSteps 						AT_ISPBASE_AF(131);
U8 g_byAFDirChangeTimes_Th				AT_ISPBASE_AF(132);
float g_fAF_SharpDiff_High_Th2				AT_ISPBASE_AF(133);
float g_fAF_SharpDiff_Low_Th2				AT_ISPBASE_AF(137);
float g_fAFSharpChange				AT_ISPBASE_AF(141);
U16 g_wAFColorChange			AT_ISPBASE_AF(145);
U8 g_byAFSharpPeakTh1_Fine			AT_ISPBASE_AF(147);
U8 g_byAFSharpPeakTh2_Fine			AT_ISPBASE_AF(148);
U8 g_byAFSharpPeakTh1_Rough			AT_ISPBASE_AF(149);
U8 g_byAFSharpPeakTh2_Rough			AT_ISPBASE_AF(150);
U8 g_byIsMoving						AT_ISPBASE_AF(151);
U8 g_byAFMoveBlockNum_Thd			AT_ISPBASE_AF(152);			
U8 g_byAFMoveColor_Thd				AT_ISPBASE_AF(153);
U8 g_byAFMoveCenterBlockNum_Thd		AT_ISPBASE_AF(154);
#endif

//#[sharpness & denoise]
U8 g_bySharpness_Def					AT_ISPBASE_SHARP(0);
U8 g_bySharpness_Normal				AT_ISPBASE_SHARP(1);
U8 g_bySharpParamIndex_Last				AT_ISPBASE_SHARP(2);
U8  g_abyISParamVaris[4][28]			AT_ISPBASE_SHARP(3);
U8 g_bySharp_VIIR_Coef				AT_ISPBASE_SHARP(115);
U8 g_bySharp_EdgeDct_Thd1			AT_ISPBASE_SHARP(116);
U8 g_bySharp_HIIR_Coef				AT_ISPBASE_SHARP(117);

//#[CCM]
U8 g_byCCMState							AT_ISPBASE_CCM(0);
S16 g_aCCM_Normal[9]					AT_ISPBASE_CCM(1);
S16 g_aCCM[9]							AT_ISPBASE_CCM(19);

//#[Gamma] _DECOLOR_TONE_ENABLE_
U8 g_aGamma_Def[28]					AT_ISPBASE_GAMMA(0);
U8 g_Ygamma[16]						AT_ISPBASE_GAMMA(28);
#ifdef _DECOLOR_TONE_ENABLE_
U8 g_aCTC_Gamma[28]					AT_ISPBASE_GAMMA(44);
#endif


//#[BLC]
U8 g_byoffsetR							AT_ISPBASE_BLC(0);
U8 g_byoffsetG1							AT_ISPBASE_BLC(1);
U8 g_byoffsetG2							AT_ISPBASE_BLC(2);
U8 g_byoffsetB							AT_ISPBASE_BLC(3);
U16 g_wSensorWidthBefBLC				AT_ISPBASE_BLC(4);
U16 g_wSensorHeightBefBLC				AT_ISPBASE_BLC(6);

//#[Moire]
U8 g_byMoireThreshold						AT_ISPBASE_MOIRE(0);

//#[AS] _SCENE_DETECTION_
U8 g_byOutdoorET 						AT_ISPBASE_AS(0);
U8 g_byIndoorET							AT_ISPBASE_AS(1);
U8 g_byOutDoorScene	  					AT_ISPBASE_AS(2);
U8 g_byOutdoorModeEnter_Thd  			AT_ISPBASE_AS(3);
U8 g_byOutdoorModeExit_Thd  			AT_ISPBASE_AS(4);
U8 g_byExistFlicker 						AT_ISPBASE_AS(5);
U8 g_byFlickerUnDetectCnt	 				AT_ISPBASE_AS(6);
U8 g_byFlickerDetectCnt					AT_ISPBASE_AS(7);
U8 g_bySceneChangeDelay				AT_ISPBASE_AS(8);
#ifdef _SCENE_DETECTION_
U8 g_byWhiteScene					AT_ISPBASE_AS(10);
U8 g_byWhite_Edge_TH				AT_ISPBASE_AS(11);
U8 g_byWhite_Edge_HD_TH				AT_ISPBASE_AS(12);
U8 g_byWhite_Edge_Dark_TH			AT_ISPBASE_AS(13);
U8 g_byWhite_Edge_Block_TH			AT_ISPBASE_AS(14);
U8 g_byWhite_Edge_Block_Dark_TH		AT_ISPBASE_AS(15);
U8 g_byAddAE_U						AT_ISPBASE_AS(16);
U8 g_byYellowScene					AT_ISPBASE_AS(17);
U16 g_wASWinSharpLast[5][5]		AT_ISPBASE_AS(18);
#endif

//#[AB] ENABLE_ANTI_FLICK
#if ((defined _ENABLE_ANTI_FLICK_) ||(defined _ENABLE_OUTDOOR_DETECT_))
U8 g_byISPABStaticsEn				AT_ISPBASE_AB(0);
U8 g_byABCurrentStep  				AT_ISPBASE_AB(1);
U8 g_byABRatioThreshold 				AT_ISPBASE_AB(2);
U8 g_byABRatioMaxThreshold			AT_ISPBASE_AB(3);
U8 g_byABRatioThirdThreshold			AT_ISPBASE_AB(4);
U8 g_byABRatioSecThreshold			AT_ISPBASE_AB(5);
U8 g_byABRatioMinThreshold			AT_ISPBASE_AB(6);
U8 g_byABDctCntThreshold			AT_ISPBASE_AB(7);
U8 g_byABIFExceedFpsThreshold		AT_ISPBASE_AB(8);
U8 g_byABDummylines				AT_ISPBASE_AB(9);
U8 g_byABDetectMinFps				AT_ISPBASE_AB(10);
#ifdef _FLICK_DETECT_GAIN_THRESHOLD_
U8 g_byABDetectMaxGain				AT_ISPBASE_AB(11);
#endif
U8 g_byABDynamicDctCntSetting		AT_ISPBASE_AB(12);
U8 g_byABStaticDctCntSetting			AT_ISPBASE_AB(13);
U8 g_byABCurPowerLine				AT_ISPBASE_AB(14);
U8 g_byABDetectStatis				AT_ISPBASE_AB(15);
U8 g_byABDetectType					AT_ISPBASE_AB(16);
U8 g_byABDetectCount				AT_ISPBASE_AB(17);
U8 g_byABStart 						AT_ISPBASE_AB(18);
U8 g_byABIfFlickExist					AT_ISPBASE_AB(19);
U32 g_byABRatioSum					AT_ISPBASE_AB(20);
U32 g_byABLastRatioSum				AT_ISPBASE_AB(24);
#endif
#if ((defined _DEFENT_OBJECT_MOVEMENT_) ||(defined _ENABLE_OUTDOOR_DETECT_) \
	|| (defined _LENOVO_IDEA_EYE_))
U8 g_byABLastWinYMean[25]			AT_ISPBASE_AB(28);
U8 g_byABPresentWinYMean[25]			AT_ISPBASE_AB(53);
U8 g_byABBypassThreshold				AT_ISPBASE_AB(78);
U8 g_byABStableBlockDiffTh			AT_ISPBASE_AB(79);
#endif

//#[Special ISP]
S8 g_byU_Offset_Normal							AT_ISPBASE_OTHER(0);
S8 g_byV_Offset_Normal							AT_ISPBASE_OTHER(1);
U8 g_bySaturation_Def					AT_ISPBASE_OTHER(2);
U8 g_byContrast_Def						AT_ISPBASE_OTHER(3);

//#[Other ISP]
U8 g_byDynISP_FrameInterval				AT_ISPBASE_OTHER(4);
U8 g_byFirstStatic						AT_ISPBASE_OTHER(5);
U8 g_abyColorTemperatureTable_gain[6]	AT_ISPBASE_OTHER(6);
U16 g_wSTnum							AT_ISPBASE_OTHER(12);
U8 g_byFirstPreview						AT_ISPBASE_OTHER(14);
U8 g_bySubResolution_For_LSC			AT_ISPBASE_OTHER(15);
U16 g_wZoomRemainderWidth  			AT_ISPBASE_OTHER(16);
U16 g_wZoomRemainderHeight 			AT_ISPBASE_OTHER(18);
#ifdef _ENABLE_MJPEG_
U8 g_byJpeg_ACRA_En					AT_ISPBASE_OTHER(20);
#endif
U16 g_wZoomXCorrectValue					AT_ISPBASE_OTHER(21);
U8 g_byCIFscaleParam_En				AT_ISPBASE_OTHER(23);
U8 g_bySetHalfSubSample_En			AT_ISPBASE_OTHER(24);
U8 g_byHalfSubSample_En				AT_ISPBASE_OTHER(25);
U8 g_byAWB_WinMeanR[5][5]				AT_ISPBASE_OTHER(26);
U8 g_byAWB_WinMeanG[5][5]				AT_ISPBASE_OTHER(51);
U8 g_byAWB_WinMeanB[5][5]				AT_ISPBASE_OTHER(76);
#ifdef _STILL_IMG_BACKUP_SETTING_
U8 g_byIsAEAWBFixed					AT_ISPBASE_OTHER(101);
#endif

//#[LSC]
U8 g_byLastIllumCT						AT_ISPBASE_LSC(0);
U8 g_byDynamicLSCSet					AT_ISPBASE_LSC(1);
#ifdef _NEW_AUTO_LSC_FUNCTION_
U8 g_byPre_ind							AT_ISPBASE_LSC(2);
U8 g_byind_count							AT_ISPBASE_LSC(3);
U8 g_byind 								AT_ISPBASE_LSC(4);
U16 g_wRGain_CT							AT_ISPBASE_LSC(5);
U16 g_wBGain_CT							AT_ISPBASE_LSC(7);
U8 g_byWinMeanR_BeforeLSC[5][5]			AT_ISPBASE_LSC(9);
U8 g_byWinMeanG_BeforeLSC[5][5]			AT_ISPBASE_LSC(34);
U8 g_byWinMeanB_BeforeLSC[5][5]			AT_ISPBASE_LSC(59);
#endif

//#[hdr]
//#struct[IQ_Hdr_Th_t]
//# U8 byHistBlkPxlTh;
//# U8 byHistBrtPxlTh;
//# U8 byHistBlkPxlMax;
//# U8 byHDRMaxTuneVaule;
//#struct_end
IQ_Hdr_Th_t g_tHdrTh						AT_ISPBASE_HDR(0);
U8 g_byHDRGamma[28]							AT_ISPBASE_HDR(4);
U8 g_byHistEqLB							AT_ISPBASE_HDR(32);
U8 g_byHistEqHB							AT_ISPBASE_HDR(33);
U8 g_bBacklightFlag						AT_ISPBASE_HDR(34);
U8 g_byTgamma_rate_max						AT_ISPBASE_HDR(35);
U8 g_byTgamma_rate_min 						AT_ISPBASE_HDR(36);
U8 g_byFX									AT_ISPBASE_HDR(37);

//#[Realtek Extended Unit] _RTK_EXTENDED_CTL_
#ifdef _RTK_EXTENDED_CTL_
// EV Compensation
U8 g_byAEMeanTarget_Backup			AT_ISPBASE_RTKEXTISP(0);
U8 g_byAEMeanTargetL_Backup			AT_ISPBASE_RTKEXTISP(1);
U8 g_byAEMeanTargetH_Backup			AT_ISPBASE_RTKEXTISP(2);
U8 g_byAEHistPosThH_Backup			AT_ISPBASE_RTKEXTISP(3);
U8 g_byAEHistPosThL_Backup			AT_ISPBASE_RTKEXTISP(4);

// ROI
U16 g_wROITop							AT_ISPBASE_RTKEXTISP(5);
U16 g_wROILeft							AT_ISPBASE_RTKEXTISP(7);
U16 g_wROIBottom						AT_ISPBASE_RTKEXTISP(9);
U16 g_wROIRight						AT_ISPBASE_RTKEXTISP(11);
U16 g_bmROIAutoControls				AT_ISPBASE_RTKEXTISP(13);

U8 g_byROIStableColorR[5][5]			AT_ISPBASE_RTKEXTISP(15);
U8 g_byROIStableColorG[5][5]			AT_ISPBASE_RTKEXTISP(40);
U8 g_byROIStableColorB[5][5]			AT_ISPBASE_RTKEXTISP(65);

U8 g_byROIColorDiffStatTimes			AT_ISPBASE_RTKEXTISP(90);
U8 g_byROIColorDiffTh					AT_ISPBASE_RTKEXTISP(91);
U8 g_byROIColorChangeBlockNumTh		AT_ISPBASE_RTKEXTISP(92);
U8 g_byROIDiffTimesTh					AT_ISPBASE_RTKEXTISP(93);

U8 g_byROIAEStatus						AT_ISPBASE_RTKEXTISP(94);
U8 g_byROIAFStatus						AT_ISPBASE_RTKEXTISP(95);
U8 g_byROIStatus						AT_ISPBASE_RTKEXTISP(96);

// Preview LED Off
U8 g_byPreviewLEDOff					AT_ISPBASE_RTKEXTISP(97);

// Color Temperature Estimation
U16 g_wColorTemperature				AT_ISPBASE_RTKEXTISP(98);
#endif


//#[IQ Struct]
IQ_LSC_Dynamic_t g_sLscDyn				AT_ISPBASE_IQSTRUCT(0);
IQ_Gamma_t g_byIQGamma				AT_ISPBASE_IQSTRUCT(48);
IQ_Texture_Sharpness_t g_byIQSharpness	AT_ISPBASE_IQSTRUCT(106);
IQ_Texture_Denoise_Th_t g_byIQDenoiseTh	AT_ISPBASE_IQSTRUCT(112);
IQ_UV_Color_Tune_t g_byIQUVColorTune	AT_ISPBASE_IQSTRUCT(115);
IQ_NLC_t g_byIQNLC			AT_ISPBASE_IQSTRUCT(127);	// next 487
#if !(_CHIP_ID_ & _RTS5829B_)	
S8 g_asbyGamma2Cur[2][28]				AT_ISPBASE_IQSTRUCT(175);
#endif

#ifdef _RTK_EXTENDED_CTL_
float g_fAEC_ISO_gain_min;
float g_fAEC_ISO_gain_max;
#endif

#endif //_GLOBALABSISPVAR_H_
