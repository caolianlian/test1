#include "pc_cam.h"
#include "camutil.h"
#include "camreg.h"
#include "camuvc.h"
#include "camctl.h"
#include "camsensor.h"
#include "CAMI2C.h"
#include "camvdcfg.h"
#include "CamSensorUtil.h"

/*lint -save -e10 */
/*-------------------------------------------data Variables---------------------------------------*/
#ifdef _S3_RESUMKEK_
U16 data g_wPollTimeOutCnt;
#else
U8 data g_byPollTimeOutCnt;
#endif
U8 data g_byDescIdx;
U8 data g_byDescOffset;
/*lint -restore*/


/********************************************************************************************/

/*-------------------------------------------Sensor Global Variables---------------------------------------*/
U8   g_bySnrType;
U8	g_byReadMode;
U8	g_bySensorIsOpen;
U8	g_byFpsLast;
U8 	g_byStillFPS;
U8	g_bySnrPowerOnSeq;
U8	g_bySnrPowerOffSeq;
U8 	g_bySensorSize;
U8 	g_bySensor_YuvMode;
U8 	g_byStartVideo;
U8 	g_byOVAEW_BLC;// Back light compensation
U8 	g_byOVAEB_BLC;
U8 	g_byOVAEW_Normal;
U8 	g_byOVAEB_Normal;
U8 	g_bySnrImgDir;
U8	g_byaFPSTable[16]; // 16 bytes
#ifdef _STILL_IMG_BACKUP_SETTING_
U16 	g_wPreviewBackupExposure;
U16 	g_wPreviewBackupGain;
#endif
U16	g_wSensor;
U16 	g_wSensorCurFormat;
U16	g_wSensorSPFormat;
U16	g_wSensorCurFormatWidth;
U16	g_wSensorCurFormatHeight;
U16 	g_wSensorHsyncWidth;
U32 	g_dwPclk;
SnrRegAccessProp_t g_SnrRegAccessProp;
VideoFormat_T g_VideoFormatFSYUY2;	//73Bytes
VideoFormat_T g_aVideoFormat[MAX_FORMAT_TYPE];// 292bytes=73*4
OV_CTT_t g_asOvCTT[CTT_INDEX_MAX]; //48 bytes// Manual white balance
#ifdef _BLACK_SCREEN_
U16 	g_wBlackPatternExposuretime_TH;
U8 	g_byBlackPatternYavg_TH;
U16 	g_wBlackPatternGain_TH;
U8 	g_byIspCtl;
U8 	g_byIsBlack;
U8 	g_byBackendIspSwitchPoint;
#endif
#ifdef _MIPI_EXIST_
U8	g_byMipiDphyCtrl;
#endif
#ifdef _RS0509_PREVIEW_ISSUE_
U16	g_wSOFCount;
#endif
/*-------------------------------------External Mem Global Variables---------------------------------------*/
#ifndef _CACHE_MODE_
U8	g_bySPIClockSinkSel;
U16 g_wSFCapacity;
U8   g_bySFProgModeSP;
U8   g_bySFChipEROpCode;
#endif
U8	g_bySFManuID;
U16	g_wSFDeviceID;

/*-------------------------------------USB Global Variables-----------------------------------------------*/
U8	g_bIsHighSpeed;
U8 	g_byVendorReqStatus;
U8 	g_byCurFps;
U8 	g_byCurFormat;
U8 	g_byConfigurationValue;
U8 	g_byVideoStrmIFSetting;
U8  	g_byIsHighSpeedDesc;
U8   g_byCommitFPS;
U8 	g_bySnrEffect;
U8   g_byVCLastError;
U8   g_byVSLastError;
U8 	g_byDataInOutCtlSize;
U8 	g_byPreviewDropFrameNumber;
U8 	g_byPreviewDelayTimeMax;
U8 	g_byStillimgDropFrameNumber;
U8 	g_byStillimgDelayTimeMax;
U8	g_byCpuClkIdx;
U8 	g_byCamTrmCtlSel_Ext;
U8   g_bybTrigger;
U8 	g_byVendorIDL;
U8 	g_byVendorIDH;
U8 	g_byProductIDL;
U8 	g_byProductIDH;
U8 	g_byPHYHSRxTxPwrDownMode;
U8   g_byDevVerID_L;
U8   g_byDevVerID_H;
U16 	g_wVdStrManuFactorAddr;
U16 	g_wVdStrProductAddr;
U16 	g_wVdStrSerialNumAddr;
U16 	g_wVdIADStrAddr;
U16 	g_wVCCSdescSize;
U16 	g_wVSCSdescSize;
U16 	g_wTotalCfgDescSize;
U16 	g_wCurFrameWidth;
U16 	g_wCurFrameHeight;
U16	g_wTimerCounterForUSBIF;
U16	g_wExtUIspCtl;
U16 	g_wCamTrmCtlSel;
U16 	g_wProcUnitCtlSel;
#if defined _RTK_EXTENDED_CTL_ || defined _LENOVO_IDEA_EYE_EXTENDEDUNIT_
U32 g_dwRtkExtUnitCtlSel;
#endif
U32 	g_dwPanFalseValue_ForXPbug;
U8 	g_byPG_EPA_CFG;
U8 	g_byPG_EPA_CTL;
U8 	g_byPG_EPA_IRQEN;
U16 	g_wPG_EPA_TRANSFER_SIZE;
U16 	g_wPG_LWM;
U16 	g_wPG_HWM;
U8 	g_byPG_USBSYS_IRQEN;
#if(defined _ENABLE_MJPEG_ || defined _ENABLE_M420_FMT_)
U16 	g_wCurMJPEGOutputWidth;
U16 	g_wCurMJPEGOutputHeight;
U16	g_wSensorMJPGByPass;
#endif
#ifdef _AF_ENABLE_
CtlItemU16_t  FocusAbsolutItem;
CtlItemU8_t  FocusAutoItem;
Ctl_t Ctl_FocusAbsolut;
Ctl_t Ctl_FocusAuto;
#endif
#ifdef _UVC_PPWB_
VSPropertyCommCtl_t  g_VsPropCommit;
#endif
#ifdef _UVC_COMMITWB_
VSBulkCommit_t g_VsBulkCommit;
#endif


VSProbCommCtl_t  g_VsProbe; // 16 bytes//probe control.
VSProbCommCtl_t  g_VsCommit; // 16 bytes//vs commit control.
VSStlProbCommCtl_t  g_VsStlProbe; // 11 bytes//vs still probe control.
VSStlProbCommCtl_t  g_VsStlCommit; // 11 bytes//vs still commit control.
VideoDimension_t g_cwaResolutionTable[MAX_RES_TABLE_SIZE];	//92Bytes=4*23
CtlItemU32_t  ExposureTimeAbsolutItem; 
CtlItemU8_t  ExposureTimeAutoItem;
CtlItemU8_t  LowLightCompItem;
CtlItemS16_t  BrightnessItem;
CtlItemU16_t  ContrastItem;
CtlItemS16_t  HueItem;
CtlItemU16_t  SaturationItem;
CtlItemU16_t GammaItem;
CtlItemU16_t  WhiteBalanceTempItem;
CtlItemU16_t  BackLightCompItem;
CtlItemU16_t GainItem;
CtlItemU8_t  PwrLineFreqItem;
CtlItemU8_t  WhiteBalanceTempAutoItem;
CtlItemS16_t  PanItem	;
CtlItemS16_t  TiltItem;
CtlItemU16_t  ZoomItem;
CtlItemS16_t  RollItem	;
CtlItemU16_t  SharpnessItem;		// 12bytes
#ifdef _RTK_EXTENDED_CTL_
CtlItemU16_t RtkExtISPSpecialEffectItem;
CtlItemS16_t RtkExtEVCompensationItem;
RtkExtROICtlItem_t RtkExtROIItem;
CtlItemU8_t RtkExtPreviewLEDOffItem;
CtlItemU16_t RtkExtISOItem;
#endif
#ifdef _LENOVO_IDEA_EYE_EXTENDEDUNIT_
CtlItemU16_t RtkExtIdeaEyeSensItem;
CtlItemU8_t RtkExtIdeaEyeStatItem;
CtlItemU8_t	RtkExtIdeaEyeModeItem;
#endif

Ctl_t Ctl_ExposureTimeAbsolut;
Ctl_t Ctl_ExposureTimeAuto;
Ctl_t Ctl_LowLightComp;
Ctl_t Ctl_Brightness;
Ctl_t Ctl_Contrast;
Ctl_t Ctl_Hue;
Ctl_t Ctl_Saturation;
Ctl_t Ctl_Sharpness;
Ctl_t Ctl_Gamma;
Ctl_t Ctl_WhiteBalanceTemp;
Ctl_t Ctl_BackLightComp;
Ctl_t Ctl_Gain;
Ctl_t Ctl_PwrLineFreq;
Ctl_t Ctl_WhiteBalanceTempAuto;
Ctl_t Ctl_PanTilt;
Ctl_t Ctl_Zoom;
Ctl_t Ctl_Roll;
Ctl_t Ctl_TrapeziumCorrection;
CtlItemS8_t TCorrectionItem;
#ifdef _RTK_EXTENDED_CTL_
Ctl_t Ctl_RtkExtISPSpecialEffect;
Ctl_t Ctl_RtkExtEVCompensation;
Ctl_t Ctl_RtkExtROI;
Ctl_t Ctl_RtkExtPreviewLEDOff;
Ctl_t Ctl_RtkExtISO;
#endif
#ifdef _LENOVO_IDEA_EYE_EXTENDEDUNIT_
Ctl_t Ctl_IdeaEye_Sens;
Ctl_t Ctl_IdeaEye_Stat;
Ctl_t Ctl_IdeaEye_Mode;
#endif

S8 	g_byThermalCalibrationOffset;
float g_fThermalCofe;

U16 	g_wProcessUnitChange_Flag;
U16 	g_wCameraTerminalChange_Flag;
U8 	g_byCommitChange_Flag;
#if defined _RTK_EXTENDED_CTL_ || defined _LENOVO_IDEA_EYE_EXTENDEDUNIT_
U32 	g_dwRtkExtCtlUnitChange_Flag;
#endif

#ifdef _UAC_EXIST_
U8	g_byConfigMicArray;
U16 	g_wConfigMicArrayCfgAddr;
U8 	g_wEPB_ADF_delay;
U8 	g_byADF_Chn_Switch;
U8 	g_byALC_Cfg;
U8 	g_byALC_FT_Boost;
U8 	g_byALC_NRGate_Cfg0;
U8 	g_byAudioStrmIFSetting;
U8 	g_byAudioAttrCurr;
U16 	g_wAudioVolumeCurr;
#endif

#ifdef _BULK_ENDPOINT_
U16 	g_wHWM;
U16 	g_wLWM;
U16 	g_wTransferSize;
#endif
#ifdef _UBIST_TEST_
U8 	g_byUBISTEnble;
U8 	g_byUbistTestProgress;
U8 	g_byUbistTestReturnCode;
U8 	g_byUbistTestID;
U8 	g_byUbistTestAbort;
U8 	g_byUbistTestEnable;
U16 	g_wVdUBISTStrAddr;
U16 	g_wUbistTestMemory;
#endif
#ifdef _DELL_EXU_
U8 	g_byLEDStatus;
U8 	g_byLEDBlinkCnt;
U8 	g_byStreamActive;
U8 	g_byMaxAutoGain;
U16 	g_wMaxAutoExpTime;
#endif

/*-------------------------------------System Global Variables-----------------------------------------------*/

U8 	g_byWhiteBalanceAutoLast_BackForUVCtest;//2010-03-25 hemonel: when disable gain function, manual white balance USB_IF UVCTest fail.
#if (defined (_SLB_TEST_))
U8 	g_bySLBTestMode;
U8	g_bySLB_FSTstCnt; //self loop back test variables
U8   g_bySLB_FixPattern;
U8   g_bySLB_Seed;
U8   g_bySLB_FSFailTh;
U16 	g_wSLB_HSFailTh;
U16 	g_wSLB_HSDlyTime;
U32 	g_dwSLB_HSTstCnt;
#endif
U8 	g_bySSCEnable;
U16 	g_wLEDToggleInterval;
U16 	g_wBarInfoOffset;
#ifdef _HID_BTN_
U8 	g_byHIDBtnLast;
#endif
#ifdef _EN_PHY_DBG_MODE_
U8 	g_byPhyAddrDbg;
#endif

#ifdef _SENSOR_ESD_ISSUE_
U8 g_byESDExist;
U8 g_byStreamOn;
U16 g_wESDTimerCounter;
#endif

#ifdef _NF_RESTORE_
U8  g_bySuspendFW_N;
U8  g_bySuspendFW_F;
U8 g_byBypassFirstSOF;
#endif
I2CSuperAccess_t g_tI2CSuperAccess;

#ifdef _LENOVO_JAPAN_PROPERTY_PAGE_
U8 g_byAWBRGain_Lenovo;
U8 g_byAWBGGain_Lenovo;
U8 g_byAWBBGain_Lenovo;
U8 g_byVendorDriver_Lenovo;
#endif
#ifdef _IQ_TABLE_CALIBRATION_
U8 g_byIQPatchGrpOnePatchOne;
#endif

#ifdef _JPEG_RATE_ADJ_
U8 g_byQtableScaleGTHD;
U8 g_byQtableScaleGTVGA;
U8 g_byQtableScaleLEQVGA;
#endif

U8 g_byVDCMDPVLEDOff;


U8 g_byRemoteWakeupSupport;
U8 g_byHostEnableRemoteWakeup;
U8 g_byRemoteWakeupNotify;
#ifdef _LENOVO_IDEA_EYE_
U8 g_byMTDDetectBackend;
U8 g_byMTDStartGetFrame;
U8 g_byUSBStreaming;
U8 g_byMTDWinDetected;
U8 g_byMTDSensCent;
U8 g_byMTDWinNCent;
U16 data g_wMTDTimeOutCnt;
#endif

U16 g_wDigitalGain;