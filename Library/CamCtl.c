#include "pc_cam.h"
#include "camutil.h"
#include "CamReg.h"
#include "CamUvc.h"
#include "CamCtl.h"
//#include "CamI2C.h"
#include "CamControls.h"
#include "camisp.h"
//#include "CamProcess.h"
#include "camvdcfg.h"
#include "ISP_vars.h"
#ifdef _UAC_EXIST_
#include "CamUac.h"
#endif
#ifdef _RTK_EXTENDED_CTL_
#include "CamUvcRtkExtCtl.h"
#endif

extern void USBProReset(void );
extern void VendorCmdProc(U8 CS,U8 req,U8 ReqLen);
extern  U8 GetFormatNum(U8 IsHighSpeed);
extern U8 GetFormatFrameNum(U8 IsVideo,U8 FmtIdx,U8 IsHighSpeed);
extern U32 GetMaxFrameSize(U8 IsVideo,U8 FMT, U8 Idx,U8 IsHighSpeed);
extern U8 GetFPSNum(U8 IsVideo,U8 FMT,U8 FRM,U8 IsHighSpeed);
extern U8 GetFPS(U8 IsVideo,U8 FMT,U8 FRM,U8 IsHighSpeed,U8 FpsIdx);
extern U32 GetFrameInterval(U8 IsVideo,U8 FMT,U8 FRM,U8 IsHighSpeed,U8 FpsIdx);
extern U32 GetDefaultFrameInterval(U8 IsVideo,U8 FMT,U8 FRM,U8 IsHighSpeed);
extern U32 GetMaxFrameInterval(U8 IsVideo,U8 FMT,U8 FRM,U8 IsHighSpeed);
extern U32 GetMinFrameInterval(U8 IsVideo,U8 FMT,U8 FRM,U8 IsHighSpeed);
#ifdef _USB2_LPM_
extern void GetSensorPclkHsync(U16 wSensorSPFormat,U8 byFps);
extern U8 GetFomatType(U8 FmtIdx,U8 IsHighSpeed);
extern U16 GetFrameWidth(U8 IsVideo,U8 FmtIdx, U8 FrmIdx,U8 IsHighSpeed);
extern U16 GetFrameHeight(U8 IsVideo,U8 FmtIdx, U8 FrmIdx,U8 IsHighSpeed);
extern U16 GetAppropriateSensorFormat(U16 width, U16 height);
extern U16 GetSensorFormatHeight(U16 const Format, bit const Flag);
extern U16 GetSensorFormatWidth(U16 const Format, bit const Flag);
#endif



#ifdef _UAC_EXIST_
extern U8 CopyVdCfgByte2SRam(U16 wSAddr,U8* pBuf,U8 byLen);
extern void StrDescProc(U8 ExtZero, U8 const * const pStr );
#endif
/*
*********************************************************************************************************
*high speed payload transfer size array
*********************************************************************************************************
*/
#ifndef _BULK_ENDPOINT_
code U32 g_cdwaMaxPayloadTransferSizeHS[7] =
{
	MAX_PAYLOADTRFERSIZE_HS_128,
	MAX_PAYLOADTRFERSIZE_HS_512,
	MAX_PAYLOADTRFERSIZE_HS_1024,
	MAX_PAYLOADTRFERSIZE_HS_1536,//768*2,
	MAX_PAYLOADTRFERSIZE_HS_2048,//1024*2,
	MAX_PAYLOADTRFERSIZE_HS_2688,//896*3,
	MAX_PAYLOADTRFERSIZE_HS_3072//1024*3
};
#endif

/*
*********************************************************************************************************
*full speed payload transfer size array
*********************************************************************************************************
*/
// hemonel 2010-09-19: optimize code
/*
code U32 g_cdwaMaxPayloadTransferSizeFS[7]=
{
	MAX_PAYLOADTRFERSIZE_FS_128,
	MAX_PAYLOADTRFERSIZE_FS_256,
	MAX_PAYLOADTRFERSIZE_FS_384,
	MAX_PAYLOADTRFERSIZE_FS_512,
	MAX_PAYLOADTRFERSIZE_FS_768,
	MAX_PAYLOADTRFERSIZE_FS_896,
	800	// hemonel 2009-09-16: decrease usb fs bandwidth for gold tree test full speed preview fail bug
};
*/
// hemonel 2009-08-14: this function has latent bug. use WaitEP0_DATA_Out replace this function
bit WaitEP0_DATA_Out()
{
	if(!WaitTimeOut(EP0_STAT, EP0_FIFO_EMPTY, 0, 20))
	{
		if (XBYTE[USBSYS_IRQSTAT] & PORT_RESET_INT)
		{
			USBProReset();
			return FALSE;
		}
//#ifdef _CACHE_MODE_	// hemonel 2010-06-01: if EP0 data wait fail, stall endpoint 0
		// hemonel 2009-08-14: use cache mode test whether this code is correct or not.
		// If cache mode test ok, mark this macro of _CACHE_MODE_
		else
		{
			STALL_EPCTL();
			return FALSE;
		}
//#endif
	}
	return TRUE;
}

#ifdef _USB2_LPM_
void GetBulkTransferSize(U8 bFormatIndex,U8 bFrameIndex,U8 byFps)
{
	U8 byFormat;
	U16 wUsbWidth;
	U16 wUsbHeight;
	U16 wSensorSPFormat;
	U16 wSensorCurFormatHeight;
	U16 wSensorCurFormatWidth;
	
	U8 byOption;
	U8 byVisp;
	U8 byJpegCmpRate;
	U16 wBESL= 1200;// Default=400us+250uS;
	//U16 wLWM,wHWM;
#ifdef _HASWELL_LPM_LAG_
	U16 wtmp;
#endif
	//U8 byVusb= 41;//40960000;
#if (_CHIP_ID_ & _RTS5829B_)	
#ifdef _L1_SAFE_TIME_
	U32	dwSafebyte;
	U16 wSafeTime;
#endif
	U32 dwBuffersize=(U32)88*1024;//44KByte
#else
	U32 dwBuffersize=(U32)44*1024;//44KByte
#endif	

	byFormat = GetFomatType(bFormatIndex,(U8)g_bIsHighSpeed);
	wUsbWidth =  GetFrameWidth(1,bFormatIndex, bFrameIndex,(U8)g_bIsHighSpeed);
	wUsbHeight = GetFrameHeight(1,bFormatIndex,bFrameIndex,(U8)g_bIsHighSpeed);
	wSensorSPFormat = GetAppropriateSensorFormat(wUsbWidth,wUsbHeight);
	wSensorCurFormatWidth = GetSensorFormatWidth(wSensorSPFormat, (bit)1);
	wSensorCurFormatHeight=GetSensorFormatHeight( wSensorSPFormat, (bit)1);
	GetSensorPclkHsync(wSensorSPFormat,byFps);
	
	if (byFormat==FORMAT_TYPE_MJPG)
	{
		if(g_byQtableScale<4)	// 1
		{
			byJpegCmpRate = 3;// byJpegCmpRate must >3, because when byJpegCmpRate=3, :byVisp>byVusb 
		}
		else if(g_byQtableScale<6)
		{
			byJpegCmpRate = 5;
		}
		else if(g_byQtableScale < 10)
		{
			byJpegCmpRate = 7;
		}
		else
		{
			byJpegCmpRate = 8;
		}			
	}
	else
	{
		byJpegCmpRate = 1;
	}

//Jimmy.20130607.always wBESL use large value=1200 to fix some platform wBESL not accord with spec
//lead to broken image;use 1200 some resolution may not be able to sleep on LPM L1
//#ifdef _HASWELL_LPM_LAG_
//	if (byFormat==FORMAT_TYPE_YUY2)
//	{
//		wBESL = 500;
//	}
//#endif

	
	byOption = ((U32)wUsbWidth*(U32)wSensorCurFormatHeight > (U32)wUsbHeight*(U32)wSensorCurFormatWidth) ? 1:0;

	if (1 == byOption)
	{
		byVisp = (U8)(((((g_dwPclk*2/(float)g_wSensorHsyncWidth)*(U32)wSensorCurFormatWidth/(U32)wSensorCurFormatWidth)*(U32)wUsbWidth/(U32)wSensorCurFormatWidth)*(U32)wUsbWidth/(U32)byJpegCmpRate)/1024/1024)+ 1;		

	}
	else
	{
		byVisp = (U8)(((((g_dwPclk*2/(float)g_wSensorHsyncWidth)*(U32)wSensorCurFormatWidth/(U32)wSensorCurFormatHeight)*(U32)wUsbHeight/(U32)wSensorCurFormatHeight)*(U32)wUsbHeight/(U32)byJpegCmpRate)/1024/1024) + 1;			
	}
	
	g_wHWM = (U16)(((dwBuffersize - (U32)byVisp*wBESL)/512)*256);  //up to 512*N;

//Jimmy.20140715.fix IMX132 preview Small(YUV or MJPG) to still YUYV FHD, HWM too big ,so still will over and can not still image
#if (_CHIP_ID_ & _RTS5829B_)
	if (g_wHWM >= 43008)	// fix at 84KByte, reserved 4KByte
	{
		g_wHWM = 43008;
	}
#endif

	if(g_byLPMSetbyAP==1)
	{
		if(g_wHWMbyAp<g_wHWM)
		{
			g_wHWM = g_wHWMbyAp;
		}
		//g_byLPMSetbyAP=0;
	}	
	
#if ((defined _RTS5829B_) && (defined  _L1_SAFE_TIME_))	// just for RTS5829B
	dwSafebyte = (U32)g_wHWM * 2 - byVisp * 500; 					// set T_nak to 500us	
	XBYTE[CFG_SAFE_BYTE] = (dwSafebyte >> 9);
	wSafeTime = dwSafebyte/byVisp;								//unit: us
	XBYTE[CFG_SAFE_TIME1] = CFG_SAFE_LINE_WAKE_EN |(INT2CHAR(wSafeTime, 1)); 		// set safe time  
	XBYTE[CFG_SAFE_TIME0] = INT2CHAR(wSafeTime, 0);
	g_wHWM = g_wHWM + (10 * 1024) /2;							// add 10K 
#endif

	//xiaozhizou 2012-6-29:transfer_size=LWM=HWM-0.5K,just for intel requirement
	g_wLWM = g_wHWM-256;
	g_wTransferSize = g_wLWM>>8;//g_wTransferSize = wLWM*2/512

#ifdef _HASWELL_LPM_LAG_
	if (byFormat==FORMAT_TYPE_YUY2)
	{
		wtmp = (U16)((((U32)byVisp*2000)/512)*256);  //Tnak+LPMRequest+L1 time= 1000uS+1000uS=2mS
		if (g_wHWM > (wtmp+(U16)256*4))		//FE38[5] =1 
		{
			g_wLWM = g_wHWM - wtmp;
			g_wTransferSize = g_wLWM>>8;
			XBYTE[EPA_CFG] |= EPA_BULK_MODE_BURST;
		}
		else									//FE38[5] =0
		{
			wtmp >>=1;	//L1 time= 1mS
			if (g_wHWM > (wtmp+(U16)256*4))
			{
				g_wLWM = g_wHWM - wtmp;
				g_wTransferSize = g_wLWM>>8;
			}
			else
			{
				g_wLWM = 1024;	// 2KByte
				g_wTransferSize = 4;	// 2KByte		
			}
			XBYTE[EPA_CFG] &= ~EPA_BULK_MODE_BURST;			
		}
	}

	if (byFormat==FORMAT_TYPE_MJPG)
	{
		XBYTE[EPA_CFG] |= EPA_BULK_MODE_BURST;
	}
#endif
	
	XBYTE[RF_LWM_0]=INT2CHAR(g_wLWM, 0);
	XBYTE[RF_LWM_1]=INT2CHAR(g_wLWM, 1);
	XBYTE[RF_EPA_TRANSFER_SIZE_0] =INT2CHAR(g_wTransferSize, 0); 
	XBYTE[RF_EPA_TRANSFER_SIZE_1] =INT2CHAR(g_wTransferSize, 1);			
	XBYTE[RF_HWM_0]=INT2CHAR(g_wHWM, 0);
	XBYTE[RF_HWM_1]=INT2CHAR(g_wHWM, 1);
}
#endif


U32 GetMaxPayloadTransferSize(U32 FrameSize, U8 Fps, U8 IsHighSpeed)
{
	FrameSize = FrameSize;
	Fps = Fps;
	IsHighSpeed = IsHighSpeed;

#ifdef _BULK_ENDPOINT_
	if(IsHighSpeed)
	{
		return  512*(U32)g_wTransferSize;
	}	
	else
	{
		return  64*(U32)g_wTransferSize;
	}
#else
	if(IsHighSpeed)
	{
		// 2009-12-17 , darcy_lu *: modify max payload transfer size when CIF, QVGA etc
#ifdef _AUTO_UVC_BANDWIDTH_
		// 2009-12-17 , darcy_lu *: modify max payload transfer size when CIF, QVGA etc
		switch(FrameSize)
		{
		case UVC_QQVGA_SIZE: // 160 * 120  crop from 640*480,  for 30 fps transfersize is no less then 768, we give it 1024
		case UVC_QCIF_SIZE: // 174 * 144  crop from 640*480,  for 30 fps transfersize is no less then 845, we give it 1024
			if (Fps <= 30)
			{
				return g_cdwaMaxPayloadTransferSizeHS[2];  	// max payload transfer size is 1024
			}
			else
			{
				return g_cdwaMaxPayloadTransferSizeHS[6];
			}

		case UVC_QVGA_SIZE: //320 * 240 crop from 640*480, for 30 fps transfersize is no less then 1536, we give it 1536
			if (Fps <= 30)
			{
				return g_cdwaMaxPayloadTransferSizeHS[3];  	// max payload transfer size is 1536
			}
			else
			{
				return g_cdwaMaxPayloadTransferSizeHS[6];
			}

		case UVC_CIF_SIZE	: //352 * 288 crop from 640*480, for 30 fps transfersize is no less then 1690, we give it 2048
			if (Fps <= 30)
			{
				return g_cdwaMaxPayloadTransferSizeHS[4]; //  max payload transfer size is 2048
			}
			else
			{
				return g_cdwaMaxPayloadTransferSizeHS[6];
			}

		default:
			return g_cdwaMaxPayloadTransferSizeHS[6];
		}
#else
		return g_cdwaMaxPayloadTransferSizeHS[6];
#endif
	}
	else
	{
		// hemonel 2010-09-19: optimize code
		//only UVC set usb fs bandwidth 800 for gold tree test full speed preview fail bug
		return MAX_PAYLOADTRFERSIZE_FS_512;//g_cdwaMaxPayloadTransferSizeFS[3];
	}
#endif
}

/*
*********************************************************************************************************
*										check control setting value
* FUNCTION ControlSetReqCheckProc
*********************************************************************************************************
*/
/**
  check control setting value.
  if setting value is in the range of control, save the value as the control DES value. otherwise stall EP0
  and save the cause.

  \param
        pCtl: pointer of CONTROL structure

  \retval
        TRUE/1: if setting value is in the control range
        FALSE/0: if setting value is out of the control range
*********************************************************************************************************
*/
bit ControlSetReqCheckProc(Ctl_t* pCtl)
{
	U8 len;
	U16 setLen;
	U32 setValue32=0;
	U16 setValue16=0;
	U8   setValue8=0;
	U8 i;

	ASSERT(pCtl!=NULL);

	if(pCtl->Info&CONTROL_INFO_DIS_BY_AUTO)// disable by auto.
	{
		//DBG(("\npCtl-INFO=%bx",pCtl->Info));
		VCSTALL_EPCTL(VC_ERR_WRNGSTAT);
		return FALSE;
	}

	// hemonel 2009-11-26: use new control struct
//	len=pCtl->pExt->Len;
	len=pCtl->Len;

	ASSERT((len==1)||(len==2)||(len==4));
	ASSIGN_INT(setLen,XBYTE[SETUP_PKT_wLEN_H],XBYTE[SETUP_PKT_wLEN_L]);
	// hemonel 2009-7-16: fix uvc test gain control fail bug.
	//if(setLen!=len)//invalid set current request
	if((setLen>len)||(setLen ==0))
	{
		VCSTALL_EPCTL(VC_ERR_UNKNOWN);
		return FALSE;
	}

	if(!WaitEP0_DATA_Out())
	{
		return FALSE;
	}
	//commit SET_CUR setting
	if(len==1)
	{
		setValue8=EP0_DATA_OUT();
	}
	else if(len==2)
	{
		// hemonel 2009-7-16: fix uvc test gain control fail bug.
		/*
		ASSIGN_U16BYTE(setValue16,0,EP0_DATA_OUT());
		ASSIGN_U16BYTE(setValue16,1,EP0_DATA_OUT());
		*/
		for(i=0; i<setLen; i++)
		{
			ASSIGN_U16BYTE(setValue16,i,EP0_DATA_OUT());
		}

	}
	else// if(len==4)
	{
		// hemonel 2009-7-16: fix uvc test gain control fail bug.
		/*
			ASSIGN_U32BYTE(setValue32,0,EP0_DATA_OUT());
			ASSIGN_U32BYTE(setValue32,1,EP0_DATA_OUT());
			ASSIGN_U32BYTE(setValue32,2,EP0_DATA_OUT());
			ASSIGN_U32BYTE(setValue32,3,EP0_DATA_OUT());
			*/
		for(i=0; i<setLen; i++)
		{
			ASSIGN_U32BYTE(setValue32,i,EP0_DATA_OUT());
		}
	}
	//check if valid parameter.
	// hemonel 2009-11-26: use new control struct
	/*
	if(len==1)
	{
		if(pCtl->pExt->OpMask&CONTROL_ATTR_SIGNED)
		{
			if((  *((S8*)&setValue8)  >  pCtl->pExt->pAttr->CtlItemS8.Max)
				||(*((S8*)&setValue8)  <  pCtl->pExt->pAttr->CtlItemS8.Min))
			{
				VCSTALL_EPCTL(VC_ERR_OUTOFRANGE);
				return FALSE;
			}
		}
		else   //unsigned value.
		{
			if((setValue8  >  pCtl->pExt->pAttr->CtlItemU8.Max)
				||(setValue8  <  pCtl->pExt->pAttr->CtlItemU8.Min))
			{
				VCSTALL_EPCTL(VC_ERR_OUTOFRANGE);
				return FALSE;
			}
		}
	}
	else if(len==2)
	{
		if(pCtl->pExt->OpMask&CONTROL_ATTR_SIGNED)
		{
			if((*((S16*)&setValue16)  >  pCtl->pExt->pAttr->CtlItemS16.Max)
				||(*((S16*)&setValue16)  <  pCtl->pExt->pAttr->CtlItemS16.Min))
			{
				VCSTALL_EPCTL(VC_ERR_OUTOFRANGE);
				return FALSE;
			}
		}
		else//unsigned value.
		{
			if((setValue16  >  pCtl->pExt->pAttr->CtlItemU16.Max)
				||(setValue16  <  pCtl->pExt->pAttr->CtlItemU16.Min))
			{
				VCSTALL_EPCTL(VC_ERR_OUTOFRANGE);
				return FALSE;
			}
		}
	}
	else// if(len==4)
	{
		if(pCtl->pExt->OpMask&CONTROL_ATTR_SIGNED)
		{
			if((*((S32*)&setValue32)  >  pCtl->pExt->pAttr->CtlItemS32.Max)
				||(*((S32*)&setValue32) <  pCtl->pExt->pAttr->CtlItemS32.Min))
			{
				VCSTALL_EPCTL(VC_ERR_OUTOFRANGE);
				return FALSE;
			}
		}
		else//unsigned value.
		{
			if(( setValue32 >  pCtl->pExt->pAttr->CtlItemU32.Max)
				||(setValue32  <  pCtl->pExt->pAttr->CtlItemU32.Min))
			{
				VCSTALL_EPCTL(VC_ERR_OUTOFRANGE);
				return FALSE;
			}
		}

	}
	//update changeflag
	//...
	if(len==1)
	{
		pCtl->pDes->CurU8 = setValue8;
		pCtl->ChangeFlag=CTL_CHNGFLG_NOTRDY;
		//pCtl->pLast->CurU8 = setValue8;
	}
	else if(len==2)
	{
		pCtl->pDes->CurU16 = setValue16;
		pCtl->ChangeFlag=CTL_CHNGFLG_NOTRDY;
		//pCtl->pLast->CurU16 = setValue16;
		//DBG(("\nsetValue16=%x",setValue16));
	}
	else// if(len==4
	{
		pCtl->pDes->CurU32 = setValue32;
		pCtl->ChangeFlag=CTL_CHNGFLG_NOTRDY;
		//pCtl->pLast->CurU32 = setValue32;
		//DBG(("\nsetValue32=%x",setValue32));
	}
	*/
	if(len==1)
	{
		if(pCtl->OpMask&CONTROL_ATTR_SIGNED)
		{
			if((  *((S8*)&setValue8)  >  pCtl->pAttr->CtlItemS8.Max)
			        ||(*((S8*)&setValue8)  <  pCtl->pAttr->CtlItemS8.Min))
			{
				VCSTALL_EPCTL(VC_ERR_OUTOFRANGE);
				return FALSE;
			}
		}
		else   //unsigned value.
		{
			if((setValue8  >  pCtl->pAttr->CtlItemU8.Max)
			        ||(setValue8  <  pCtl->pAttr->CtlItemU8.Min))
			{
				VCSTALL_EPCTL(VC_ERR_OUTOFRANGE);
				return FALSE;
			}
		}

		pCtl->pAttr->CtlItemU8.Des = setValue8;
	}
	else if(len==2)
	{
		if(pCtl->OpMask&CONTROL_ATTR_SIGNED)
		{
			if((*((S16*)&setValue16)  >  pCtl->pAttr->CtlItemS16.Max)
			        ||(*((S16*)&setValue16)  <  pCtl->pAttr->CtlItemS16.Min))
			{
				VCSTALL_EPCTL(VC_ERR_OUTOFRANGE);
				return FALSE;
			}
		}
		else//unsigned value.
		{
			if((setValue16  >  pCtl->pAttr->CtlItemU16.Max)
			        ||(setValue16  <  pCtl->pAttr->CtlItemU16.Min))
			{
				VCSTALL_EPCTL(VC_ERR_OUTOFRANGE);
				return FALSE;
			}
		}

		pCtl->pAttr->CtlItemU16.Des= setValue16;
	}
	else// if(len==4)
	{
		if(pCtl->OpMask&CONTROL_ATTR_SIGNED)
		{
			if((*((S32*)&setValue32)  >  pCtl->pAttr->CtlItemS32.Max)
			        ||(*((S32*)&setValue32) <  pCtl->pAttr->CtlItemS32.Min))
			{
				VCSTALL_EPCTL(VC_ERR_OUTOFRANGE);
				return FALSE;
			}
		}
		else//unsigned value.
		{
			if(( setValue32 >  pCtl->pAttr->CtlItemU32.Max)
			        ||(setValue32  <  pCtl->pAttr->CtlItemU32.Min))
			{
				VCSTALL_EPCTL(VC_ERR_OUTOFRANGE);
				return FALSE;
			}
		}

		pCtl->pAttr->CtlItemU32.Des = setValue32;
	}
	pCtl->ChangeFlag=CTL_CHNGFLG_NOTRDY;

	return TRUE;
}


/*
*********************************************************************************************************
*										control GET request processing
* FUNCTION ControlGetReqProc
*********************************************************************************************************
*/
/**
  Get Request to individual control process. include GET_INFO,GET_LEN,GET_MIN,GET_MAX, GET_MAX,GET_CUR.
  \param
        pCtl: pointer of CONTROL structure
         req: request ID(GET_INFO,GET_LEN,GET_MIN,GET_MAX, GET_MAX,GET_CUR)

  \retval None

*********************************************************************************************************
*/


void ControlGetReqProc(Ctl_t const * const pCtl, U8 const req)
{
	U8 OpMsk;
	U8 len;
	U16 getLen;
	U8 const * data pCh = NULL;
	//U16 wISPCfgDef = 0;
	U8 const * pMin = NULL;
	U8 const * pMax = NULL;
	U8 const * pRes = NULL;
	U8 const * pDef = NULL;
	U8 const * pLast = NULL;

	ASSERT(pCtl!=NULL);
	// hemonel 2009-11-26: use new control struct
//	len=pCtl->pExt->Len;
//	OpMsk=pCtl->pExt->OpMask;
	len=pCtl->Len;
	OpMsk=pCtl->OpMask;
	if(len==1)
	{
		pMin=(U8*)&(pCtl->pAttr->CtlItemU8.Min);
		pMax=(U8*)&(pCtl->pAttr->CtlItemU8.Max);
		pRes=(U8*)&(pCtl->pAttr->CtlItemU8.Res);
		pDef=(U8*)&(pCtl->pAttr->CtlItemU8.Def);
		pLast=(U8*)&(pCtl->pAttr->CtlItemU8.Last);
	}
	else if(len==2)
	{
		pMin=(U8*)&(pCtl->pAttr->CtlItemU16.Min);
		pMax=(U8*)&(pCtl->pAttr->CtlItemU16.Max);
		pRes=(U8*)&(pCtl->pAttr->CtlItemU16.Res);
		pDef=(U8*)&(pCtl->pAttr->CtlItemU16.Def);
		pLast=(U8*)&(pCtl->pAttr->CtlItemU16.Last);
	}
	else if(len==4)
	{
		pMin=(U8*)&(pCtl->pAttr->CtlItemU32.Min);
		pMax=(U8*)&(pCtl->pAttr->CtlItemU32.Max);
		pRes=(U8*)&(pCtl->pAttr->CtlItemU32.Res);
		pDef=(U8*)&(pCtl->pAttr->CtlItemU32.Def);
		pLast=(U8*)&(pCtl->pAttr->CtlItemU32.Last);
	}

	ASSERT((len==1)||(len==2)||(len==4));
	ASSIGN_INT(getLen,XBYTE[SETUP_PKT_wLEN_H],XBYTE[SETUP_PKT_wLEN_L]);

	pCh = pDef;	//Jimmy.20120428.add for delete pc-lint warning 613.Init use default value.
	
	switch(req) //prepare the character pointer, and length to send.
	{
	case GET_INFO:
		{
			EP0_DATA_IN(pCtl->Info);
			EP0_FFV_HSK();
			return;
		}
	case GET_LEN:
		{
			if(OpMsk & CONTROL_OP_GET_LEN)
			{
				// hemonel 2009-11-26: use new control struct
				//	EP0_DATA_IN(pCtl->pExt->Len);
				EP0_DATA_IN(pCtl->Len);

				if(getLen==2)
				{
					EP0_DATA_IN(0);
				}
				EP0_FFV_HSK();
				return;
			}
			else
			{
				VCSTALL_EPCTL(VC_ERR_INVDREQ);
				return;
			}
			//break;
		}
	case GET_CUR:
		{
			if(OpMsk&CONTROL_OP_GET_CUR)
			{
				//MSG(("\nChangeFlag=%bx",pCtl->ChangeFlag));
				if((pCtl->ChangeFlag==CTL_CHNGFLG_RDY)) //ready or change failed , send last value
				{
					// hemonel 2009-11-26: use new control struct
					//	pCh=(U8*)(pCtl->pLast);//@@@
					pCh = pLast;
				}
				else// if(pCtl->ChangeFlag==CTL_CHNGFLG_NOTRDY) //not ready, stall
				{
					VCSTALL_EPCTL(VC_ERR_NOTRDY);
					return;
				}
			}
			else
			{
				VCSTALL_EPCTL(VC_ERR_INVDREQ);
				return;
			}
			break;
		}
	case GET_MIN:
		if(OpMsk&CONTROL_OP_GET_MIN)
		{
			// hemonel 2009-11-26: use new control struct
			//	if(len==1)
			//	{
			//		pCh=(U8*)&(pCtl->pExt->pAttr->CtlItemU8.Min);
			//	}
			//	else if(len==2)
			//	{
			//		pCh=(U8*)&(pCtl->pExt->pAttr->CtlItemU16.Min);
			//	}
			//	else if(len==4)
			//	{
			//		pCh=(U8*)&(pCtl->pExt->pAttr->CtlItemU32.Min);
			//	}
			pCh = pMin;
		}
		else
		{
			VCSTALL_EPCTL(VC_ERR_INVDREQ);
			return;
		}
		break;
	case GET_MAX:
		if(OpMsk&CONTROL_OP_GET_MAX)
		{
			// hemonel 2009-11-26: use new control struct
			//	if(len==1)
			//	{
			//		pCh=(U8*)&(pCtl->pExt->pAttr->CtlItemU8.Max);
			//	}
			//	else if(len==2)
			//	{
			//		pCh=(U8*)&(pCtl->pExt->pAttr->CtlItemU16.Max);
			//	}
			//	else if(len==4)
			//	{
			//		pCh=(U8*)&(pCtl->pExt->pAttr->CtlItemU32.Max);
			//	}
			pCh = pMax;
		}
		else
		{
			VCSTALL_EPCTL(VC_ERR_INVDREQ);
			return;
		}
		break;
	case GET_RES:

		if(OpMsk&CONTROL_OP_GET_RES)
		{
			// hemonel 2009-11-26: use new control struct
			//	if(len==1)
			//	{
			//		pCh=(U8*)&(pCtl->pExt->pAttr->CtlItemU8.Res);
			//	}
			//	else if(len==2)
			//	{
			//		pCh=(U8*)&(pCtl->pExt->pAttr->CtlItemU16.Res);
			//	}
			//	else if(len==4)
			//	{
			//		pCh=(U8*)&(pCtl->pExt->pAttr->CtlItemU32.Res);
			//	}
			pCh = pRes;
		}
		else
		{
			VCSTALL_EPCTL(VC_ERR_INVDREQ);
			return;
		}
		break;
	case GET_DEF:
		if(OpMsk&CONTROL_OP_GET_DEF)
		{
			// hemonel 2009-11-26: use new control struct
			//	pCh=(U8*)(pCtl->pDef);
			pCh = pDef;
		}
		else
		{
			VCSTALL_EPCTL(VC_ERR_INVDREQ);
			return;
		}
		break;
	default:
		{
			VCSTALL_EPCTL(VC_ERR_INVDREQ);
			return;
		}
		//break;
	}
	
	if(getLen>=len)
	{
		if(len==1)
		{
			EP0_DATA_IN(pCh[0]);
		}
		else if(len==2)
		{
			EP0_DATA_IN(pCh[1]);
			EP0_DATA_IN(pCh[0]);
		}
		else if(len==4)
		{
			EP0_DATA_IN(pCh[3]);
			EP0_DATA_IN(pCh[2]);
			EP0_DATA_IN(pCh[1]);
			EP0_DATA_IN(pCh[0]);
		}
	}
	else   //error length parameter ,just stall.
	{
		VCSTALL_EPCTL(VC_ERR_WRNGSTAT);
		return;
	}

	EP0_FFV_HSK();
	return;
}


/*
*********************************************************************************************************
*									  Video Stream Probe Packet transmitting
* FUNCTION VideoStreamProbePacket
*********************************************************************************************************
*/
/**
  Fill EP0 with video stream probe or commit packet and valid EP0 pipe
  \param
        pVsCtl: pointer of VSProbCommCtl_t  structure


  \retval None

*********************************************************************************************************
*/

void VideoStreamProbePacket(VSProbCommCtl_t const* const pVsCtl, U8 const Req, U8 const IsNegotiating)
{
	U8 i;
	U8 FmtIdx,FrmIdx;
	U32 dwTmp;
	EP0_FFFLUSH();

	//bmHint
	EP0_DATA_IN(INT2CHAR(pVsCtl->bmHint,0));
	EP0_DATA_IN(INT2CHAR(pVsCtl->bmHint,1));
	//bFormatIndex
	EP0_DATA_IN(pVsCtl->bFormatIndex);
	//bFrameIndex

	EP0_DATA_IN(pVsCtl->bFrameIndex);
	FmtIdx = pVsCtl->bFormatIndex;
	FrmIdx = pVsCtl->bFrameIndex;
	//dwFrameInterval
	if(IsNegotiating)
	{
		switch(Req)
		{
		case GET_MIN:
			dwTmp = GetMinFrameInterval( 1,(FmtIdx), FrmIdx, g_bIsHighSpeed);
			break;
		case GET_MAX:
			dwTmp = GetMaxFrameInterval(  1,(FmtIdx), FrmIdx , g_bIsHighSpeed);
			break;
		case GET_DEF:
			dwTmp = GetDefaultFrameInterval( 1,(FmtIdx), FrmIdx , g_bIsHighSpeed);
			break;
		case GET_CUR:
		default:
			dwTmp = pVsCtl->dwFrameInterval;
			break;
		}
	}
	else
	{
		dwTmp = pVsCtl->dwFrameInterval;
	}

	EP0_DATA_IN(LONG2CHAR(dwTmp,0));
	EP0_DATA_IN(LONG2CHAR(dwTmp,1));
	EP0_DATA_IN(LONG2CHAR(dwTmp,2));
	EP0_DATA_IN(LONG2CHAR(dwTmp,3));
	//unsupport parameters set to zero.
	//U16 wKeyFrameRate;
	//U16 wPFrameRate;
	//U16 wCompQuality;
	//U16 wCompWindowSize;

	for(i=0; i<8; i++)
	{
		EP0_DATA_IN(0x00);
	}
	//U16 wDelay;
	if(g_bIsHighSpeed)
	{
		EP0_DATA_IN(32);
		EP0_DATA_IN(0);
	}
	else
	{
		EP0_DATA_IN(4);
		EP0_DATA_IN(0);
	}

	// dwMaxVideoFrameSize
	EP0_DATA_IN( LONG2CHAR(pVsCtl->dwMaxVideoFrameSize,0));
	EP0_DATA_IN( LONG2CHAR(pVsCtl->dwMaxVideoFrameSize,1));
	EP0_DATA_IN( LONG2CHAR(pVsCtl->dwMaxVideoFrameSize,2));
	EP0_DATA_IN( LONG2CHAR(pVsCtl->dwMaxVideoFrameSize,3));
	//dwMaxPayloadTransferSize
	EP0_DATA_IN(  LONG2CHAR(pVsCtl->dwMaxPayloadTransferSize,0));
	EP0_DATA_IN(  LONG2CHAR(pVsCtl->dwMaxPayloadTransferSize,1));
	EP0_DATA_IN(  LONG2CHAR(pVsCtl->dwMaxPayloadTransferSize,2));
	EP0_DATA_IN(  LONG2CHAR(pVsCtl->dwMaxPayloadTransferSize,3));
	//

	// hemonel 2010-05-25 code optimize: not support uvc 1.1
#ifdef _UVC_1V1_
	//if(g_byIsUVC1V1)
	{
		EP0_DATA_IN(CONSTLONG2BYTE(DEV_CLOCK_FRQ, 0));
		EP0_DATA_IN(CONSTLONG2BYTE(DEV_CLOCK_FRQ, 1));
		EP0_DATA_IN(CONSTLONG2BYTE(DEV_CLOCK_FRQ, 2));
		EP0_DATA_IN(CONSTLONG2BYTE(DEV_CLOCK_FRQ, 3));
		//unsupport parameters set to zero.
			//U8  bmFramingInfo;
			//U8  bPreferedVersion;
			//U8  bMinVersion;
			//U8  bMaxVersion;
		for(i=0;i<4;i++)
		{

			EP0_DATA_IN(0x00);
		}
	}
#endif
	EP0_FFV_HSK();
	return;
}
/*
*********************************************************************************************************
*									  Video Stream still image Probe Packet transmitting
* FUNCTION VideoStreamStlProbePacket
*********************************************************************************************************
*/
/**
  Fill EP0 with video still image probe or commit packet and valid EP0 pipe
  \param
        pVsCtl: pointer of VSStlProbCommCtl_t  structure


  \retval None

*********************************************************************************************************
*/
void VideoStreamStlProbePacket(VSStlProbCommCtl_t const* const pVsStlCtl)
{
	EP0_FFFLUSH();

	//bFormatIndex

	EP0_DATA_IN(pVsStlCtl->bFormatIndex);

	//bFrameIndex

	EP0_DATA_IN(pVsStlCtl->bFrameIndex);
	EP0_DATA_IN(0x00);
	//dwMaxVideoFrameSize
	EP0_DATA_IN(LONG2CHAR(pVsStlCtl->dwMaxVideoFrameSize,0));
	EP0_DATA_IN(LONG2CHAR(pVsStlCtl->dwMaxVideoFrameSize,1));
	EP0_DATA_IN(LONG2CHAR(pVsStlCtl->dwMaxVideoFrameSize,2));
	EP0_DATA_IN(LONG2CHAR(pVsStlCtl->dwMaxVideoFrameSize,3));

	//dwMaxPayloadTransferSize
	EP0_DATA_IN(LONG2CHAR(pVsStlCtl->dwMaxPayloadTransferSize,0));
	EP0_DATA_IN(LONG2CHAR(pVsStlCtl->dwMaxPayloadTransferSize,1));
	EP0_DATA_IN(LONG2CHAR(pVsStlCtl->dwMaxPayloadTransferSize,2));
	EP0_DATA_IN(LONG2CHAR(pVsStlCtl->dwMaxPayloadTransferSize,3));

	EP0_FFV_HSK();
	return;
}

/*
*********************************************************************************************************
*									  Video control interface request processing
* FUNCTION VideoControlIfReq
*********************************************************************************************************
*/
/**
  Video control interface request processing
  \param  None


  \retval None

*********************************************************************************************************
*/
void VideoControlIfReq()
{
	U8 req;
	req=XBYTE[SETUP_PKT_bREQUEST];
	switch(XBYTE[SETUP_PKT_wVALUE_H])
	{
	case VC_REQUEST_ERROR_CODE_CONTROL:
		if(req==GET_CUR)
		{

			EP0_DATA_IN(g_byVCLastError);

			g_byVCLastError=VC_ERR_NOERROR;//update error code.
		}
		else if(GET_INFO==req)
		{

			EP0_DATA_IN(	CONTROL_INFO_SP_GET);
		}
		else
		{

			VCSTALL_EPCTL(VC_ERR_INVDREQ);
			return;
		}
		break;
		/*	case VC_VIDEO_POWER_MODE_CONTROL:
				if(req==GET_CUR)
				{

				}
				else if(req==GET_INFO)
				{
					EP0_DATA_IN(CONTROL_INFO_SP_GET|CONTROL_INFO_SP_SET);
				}
				else
				{
					VCSTALL_EPCTL(VC_ERR_INVDREQ);
					return;
				}
				break;
		*/
	default:
		{
			VCSTALL_EPCTL(VC_ERR_INVDCTL);
			return;
		}
	}
	EP0_FFV_HSK();
	return;
}



/*
*********************************************************************************************************
*										control GET request processing
* FUNCTION ControlGetReqProc
*********************************************************************************************************
*/
/**
  Get Request to individual control process. include GET_INFO,GET_LEN,GET_MIN,GET_MAX, GET_MAX,GET_CUR.
  \param
        pCtl: pointer of CONTROL structure
         req: request ID(GET_INFO,GET_LEN,GET_MIN,GET_MAX, GET_MAX,GET_CUR)

  \retval None

*********************************************************************************************************
*/
void PanTiltGetReqProc(S32 sdwCtlPan, S32 sdwCtlTilt)
{
	U16 getLen;

	ASSIGN_INT(getLen,XBYTE[SETUP_PKT_wLEN_H],XBYTE[SETUP_PKT_wLEN_L]);

	sdwCtlPan *= 3600;
	sdwCtlTilt *= 3600;
	if(getLen>=8)
	{
		EP0_DATA_IN(LONG2CHAR(sdwCtlPan, 0));
		EP0_DATA_IN(LONG2CHAR(sdwCtlPan, 1));
		EP0_DATA_IN(LONG2CHAR(sdwCtlPan, 2));
		EP0_DATA_IN(LONG2CHAR(sdwCtlPan, 3));
		EP0_DATA_IN(LONG2CHAR(sdwCtlTilt, 0));
		EP0_DATA_IN(LONG2CHAR(sdwCtlTilt, 1));
		EP0_DATA_IN(LONG2CHAR(sdwCtlTilt, 2));
		EP0_DATA_IN(LONG2CHAR(sdwCtlTilt, 3));
	}
	else   //error length parameter ,just stall.
	{
		VCSTALL_EPCTL(VC_ERR_WRNGSTAT);
		return;
	}

	EP0_FFV_HSK();
	return;
}


/*
*********************************************************************************************************
*										check control setting value
* FUNCTION ControlSetReqCheckProc
*********************************************************************************************************
*/
/**
  check control setting value.
  if setting value is in the range of control, save the value as the control DES value. otherwise stall EP0
  and save the cause.

  \param
        pCtl: pointer of CONTROL structure

  \retval
        TRUE/1: if setting value is in the control range
        FALSE/0: if setting value is out of the control range
*********************************************************************************************************
*/
U8 PanTiltSetReqCheckProc(Ctl_t* pCtl)
{
	U16 setLen;
	S32 setPanValue=0;
	S32 setTiltValue=0;
	U8 i;
	bit bOutRange = 0;

	ASSIGN_INT(setLen,XBYTE[SETUP_PKT_wLEN_H],XBYTE[SETUP_PKT_wLEN_L]);

	if(setLen!=8)
	{
		VCSTALL_EPCTL(VC_ERR_UNKNOWN);
		return 0;
	}

	if(!WaitEP0_DATA_Out())
	{
		return 0;
	}

	// pan setting
	for(i=0; i<4; i++)
	{
		ASSIGN_U32BYTE(setPanValue,i,EP0_DATA_OUT());
	}

	//	setPanValue = setPanValue/3600;
	// hemonel 2010-01-15: delete for fix UVC test bug.	 uvc test res 1
	if((setPanValue  >  (S32)(PanItem.Max)*(S32)3600)
	        ||(setPanValue <  (S32)(PanItem.Min)*(S32)3600))
	{
		PanItem.Des = PanItem.Last;

		// hemonel 2010-01-15: Microsoft XP GET_CUR of tilt bug.
		// When GET_CUR Tilt value, host send out data packet
		// So host does not get pan current value, host use the false pan current value(PC memory value) as pan current value when set_cur
		// This will induce pan value out of range when set_cur for tilt
		// Resolution: firmware save the false pan value. And firmware does not think the saved pan value out of range
		if(setPanValue != g_dwPanFalseValue_ForXPbug)
		{
			bOutRange = 1;
		}
	}
	else
	{
		PanItem.Des = (S16)(setPanValue/(S32)3600);
	}


	// tilt  setting
	for(i=0; i<4; i++)
	{
		ASSIGN_U32BYTE(setTiltValue,i,EP0_DATA_OUT());
	}
//	setTiltValue = setTiltValue/3600;
	// hemonel 2010-01-15: delete for fix UVC test bug.	 uvc test res 1
	if((setTiltValue  >  (S32)(TiltItem.Max)*(S32)3600)
	        ||(setTiltValue < (S32)(TiltItem.Min)*(S32)3600))
	{
		TiltItem.Des = TiltItem.Last;
		bOutRange = 1;
	}
	else
	{
		TiltItem.Des = (S16)(setTiltValue/(S32)3600);
	}

	pCtl->ChangeFlag=CTL_CHNGFLG_NOTRDY;

	if(bOutRange == 1)
	{
		return 2;
	}

	return 1;
}

/*
*********************************************************************************************************
*									  Video camera terminal request processing
* FUNCTION VideoCtlCTReq
*********************************************************************************************************
*/
/**
  Video camera terminal request processing
  \param  None


  \retval None

*********************************************************************************************************
*/
void VideoCtlCTReq()
{

	U8 req;
	U8 setValue;
	U8 u8tmp;
	U8 i;

	req=XBYTE[SETUP_PKT_bREQUEST];
	switch(XBYTE[SETUP_PKT_wVALUE_H])
	{
	case CT_EXPOSURE_TIME_ABSOLUTE_CONTROL:
		{
			if((g_wCamTrmCtlSel&CT_CTL_SEL_EXPOSURE_TIME_ABS)==CT_CTL_SEL_EXPOSURE_TIME_ABS)
			{
				switch(req)
				{
				case GET_CUR:
				case GET_INFO:
				case GET_LEN:
				case GET_MAX:
				case GET_MIN:
				case GET_DEF:
				case GET_RES:
					ControlGetReqProc(&Ctl_ExposureTimeAbsolut, req);
					break;
				case SET_CUR:
					if(ControlSetReqCheckProc(&Ctl_ExposureTimeAbsolut)) // set value check succeed.
					{
						//exposure time.
						//if(
						ExposureTimeSet(Ctl_ExposureTimeAbsolut.pAttr->CtlItemU32.Des);//(Ctl_ExposureTimeAbsolut.pDes->CurU32);  // hemonel 2009-11-26: use new control struct
						//) //control change succeed
						{
							Ctl_ExposureTimeAbsolut.ChangeFlag=CTL_CHNGFLG_RDY;
							// hemonel 2009-11-26: use new control struct
							//	Ctl_ExposureTimeAbsolut.pLast->CurU32=Ctl_ExposureTimeAbsolut.pDes->CurU32;
							Ctl_ExposureTimeAbsolut.pAttr->CtlItemU32.Last = Ctl_ExposureTimeAbsolut.pAttr->CtlItemU32.Des;
							EP0_HSK();
						}
						/*
						else// control change failed, restore to old value.
						{
							Ctl_ExposureTimeAbsolut.ChangeFlag=CTL_CHNGFLG_FAILED_NEED_NOTIFY;
							EP0_HSK();
							ExposureTimeSet(Ctl_ExposureTimeAbsolut.pLast->CurU32);
							Ctl_ExposureTimeAbsolut.pDes->CurU32=Ctl_ExposureTimeAbsolut.pLast->CurU32;
						}
						*/
					}
					//	else // set value invalid.  stall  handshake in ControlSetReqCheckProc
					//	{
					//		;
					//	}
					break;
				default:
					{
						VCSTALL_EPCTL(VC_ERR_INVDREQ);
						return;
					}

				}
			}
			else
			{
				VCSTALL_EPCTL(VC_ERR_INVDCTL);
				return;
			}
			break;
		}
	case CT_AE_MODE_CONTROL:
		{
			if((g_wCamTrmCtlSel&CT_CTL_SEL_AUTO_EXP_MODE)==CT_CTL_SEL_AUTO_EXP_MODE)
			{
				switch(req)
				{
				case GET_CUR:
				case GET_INFO:
				case GET_LEN:
				case GET_MAX:
				case GET_MIN:
				case GET_DEF:
				case GET_RES:
					ControlGetReqProc(&Ctl_ExposureTimeAuto, req);
					break;
				case SET_CUR:
					if(!WaitEP0_DATA_Out())
					{
						return;
					}
					setValue=EP0_DATA_OUT();
					if((setValue==EXPOSURE_TIME_AUTO_MOD_APERTPRO)
					        ||(setValue==EXPOSURE_TIME_AUTO_MOD_MANUAL))
					{
						//@@@update last and des value.

						// hemonel 2009-11-26: use new control struct
						//	u8tmp=(Ctl_ExposureTimeAuto.pLast->CurU8);

						//	Ctl_ExposureTimeAuto.pDes->CurU8=setValue;
						//	Ctl_ExposureTimeAuto.pLast->CurU8=setValue;

						u8tmp=(Ctl_ExposureTimeAuto.pAttr->CtlItemU8.Last);
						Ctl_ExposureTimeAuto.pAttr->CtlItemU8.Des=setValue;
						Ctl_ExposureTimeAuto.pAttr->CtlItemU8.Last=setValue;

						Ctl_ExposureTimeAuto.ChangeFlag=CTL_CHNGFLG_RDY;
						EP0_HSK();

						if(u8tmp!=setValue)
						{
							if(setValue==EXPOSURE_TIME_AUTO_MOD_APERTPRO)
							{
								Ctl_ExposureTimeAbsolut.Info |=CONTROL_INFO_DIS_BY_AUTO|CONTROL_INFO_SP_AUTOUPDATA;
								//@@@

							}
							else
							{
								Ctl_ExposureTimeAbsolut.Info &= ~(CONTROL_INFO_DIS_BY_AUTO|CONTROL_INFO_SP_AUTOUPDATA);
								//@@@

							}
							Ctl_ExposureTimeAbsolut.ChangeFlag=CTL_CHNGFLG_RDY;
							VCStatusIntPacket_Int0(ENT_ID_CAMERA_IT,CT_EXPOSURE_TIME_ABSOLUTE_CONTROL,STS_PKT_ATTR_INFO_CHANGE,&(Ctl_ExposureTimeAbsolut.Info),1);
							WaitTimeOut_Delay(1);
							ExposureTimeAutoSet(setValue);
						}
					}
					else//invalid value
					{
						VCSTALL_EPCTL(VC_ERR_OUTOFRANGE);
						return;
					}
					break;
				default:
					{
						VCSTALL_EPCTL(VC_ERR_INVDREQ);
						return;
					}

				}
			}
			else
			{
				VCSTALL_EPCTL(VC_ERR_INVDCTL);
				return;
			}
			break;
		}
		// hemonel 2009-09-22: add low light compensation control
	case CT_AE_PRIORITY_CONTROL:
		{
			if((g_wCamTrmCtlSel&CT_CTL_SEL_AUTO_EXP_PRY)==CT_CTL_SEL_AUTO_EXP_PRY)
			{
				switch(req)
				{
				case GET_CUR:
				case GET_INFO:
				case GET_LEN:
				case GET_MAX:
				case GET_MIN:
				case GET_DEF:
				case GET_RES:
					ControlGetReqProc(&Ctl_LowLightComp, req);
					break;
				case SET_CUR:
					if(ControlSetReqCheckProc(&Ctl_LowLightComp)) // set value check succeed.
					{
						//exposure time.
						//if(
						LowLightCompSet(Ctl_LowLightComp.pAttr->CtlItemU8.Des);//(Ctl_LowLightComp.pDes->CurU8);	// hemonel 2009-11-26: use new control struct
						Ctl_LowLightComp.ChangeFlag=CTL_CHNGFLG_RDY;
						// hemonel 2009-11-26: use new control struct
						//	Ctl_LowLightComp.pLast->CurU8=Ctl_LowLightComp.pDes->CurU8;
						Ctl_LowLightComp.pAttr->CtlItemU8.Last= Ctl_LowLightComp.pAttr->CtlItemU8.Des;
						EP0_HSK();
					}
					// else // set value invalid.  stall  handshake in ControlSetReqCheckProc
					break;
				default:
					{
						VCSTALL_EPCTL(VC_ERR_INVDREQ);
						return;
					}

				}
			}
			else
			{
				VCSTALL_EPCTL(VC_ERR_INVDCTL);
				return;
			}
			break;
		}
		// hemonel 2009-12-18: add PanTilt control
	case CT_PANTILT_ABSOLUTE_CONTROL:
		{
			if((g_wCamTrmCtlSel&CT_CTL_SEL_PANTILT_ABS)==CT_CTL_SEL_PANTILT_ABS)
			{
				switch(req)
				{
				case GET_INFO:
				case GET_LEN:
					ControlGetReqProc(&Ctl_PanTilt, req);
					break;
				case GET_CUR:
					// hemonel 2010-01-15: Microsoft XP GET_CUR of tilt bug.
					// When GET_CUR Tilt value, host send out data packet
					// So host does not get pan current value, host use the false pan current value(PC memory value) as pan current value when set_cur
					// This will induce pan value out of range when set_cur for tilt
					// Resolution: firmware save the false pan value. And firmware does not think the saved pan value out of range
					if((XBYTE[SETUP_PKT_bmREQUST_TYPE] &0x80) == 0x00)
					{
						if(!WaitEP0_DATA_Out())
						{
							//	VCSTALL_EPCTL(VC_ERR_INVDREQ);
							// hemonel 2010-09-16: because WaitEP0_DATA_Out() has already stalled
							g_byVCLastError = VC_ERR_INVDREQ;
							return;
						}
						if(XBYTE[SETUP_PKT_wLEN_L]==8)
						{
							for(i=0; i<4; i++)
							{
								ASSIGN_U32BYTE(g_dwPanFalseValue_ForXPbug,i,EP0_DATA_OUT());
							}
						}

						VCSTALL_EPCTL(VC_ERR_OUTOFRANGE);
						return;
					}
					// hemonel 2010-01-15 end
					PanTiltGetReqProc(PanItem.Last, TiltItem.Last);
					break;
				case GET_MAX:
					PanTiltGetReqProc(PanItem.Max, TiltItem.Max);
					break;
				case GET_MIN:
					PanTiltGetReqProc(PanItem.Min, TiltItem.Min);
					break;
				case GET_DEF:
					PanTiltGetReqProc(PanItem.Def, TiltItem.Def);
					break;
				case GET_RES:
					PanTiltGetReqProc(PanItem.Res, TiltItem.Res);
					break;
				case SET_CUR:
					u8tmp = PanTiltSetReqCheckProc(&Ctl_PanTilt);
					if(u8tmp != 0)
					{
						PanTiltSet(PanItem.Des, TiltItem.Des);
						Ctl_PanTilt.ChangeFlag=CTL_CHNGFLG_RDY;
						PanItem.Last= PanItem.Des;
						TiltItem.Last= TiltItem.Des;
						// hemonel 2009-12-22: Microsoft XP GET_CUR of tilt bug.
						if(u8tmp == 1)
						{
							EP0_HSK();
						}
						else
						{
							VCSTALL_EPCTL(VC_ERR_OUTOFRANGE);
						}
					}
					break;
				default:
					{
						VCSTALL_EPCTL(VC_ERR_INVDREQ);
						return;
					}

				}
			}
			else
			{
				VCSTALL_EPCTL(VC_ERR_INVDCTL);
				return;
			}
			break;
		}
		// hemonel 2009-12-18: add Zoom control
	case CT_ZOOM_ABSOLUTE_CONTROL:
		{
			if((g_wCamTrmCtlSel&CT_CTL_SEL_ZOOM_ABS)==CT_CTL_SEL_ZOOM_ABS)
			{
				switch(req)
				{
				case GET_CUR:
				case GET_INFO:
				case GET_LEN:
				case GET_MAX:
				case GET_MIN:
				case GET_DEF:
				case GET_RES:
					ControlGetReqProc(&Ctl_Zoom, req);
					break;
				case SET_CUR:
					if(ControlSetReqCheckProc(&Ctl_Zoom)) // set value check succeed.
					{
#ifdef 	_LENOVO_JAPAN_PROPERTY_PAGE_
						ZoomSet(Ctl_Zoom.pAttr->CtlItemU16.Des/100-1);
#else
						ZoomSet(Ctl_Zoom.pAttr->CtlItemU16.Des);
#endif
					
						Ctl_Zoom.ChangeFlag=CTL_CHNGFLG_RDY;
						Ctl_Zoom.pAttr->CtlItemU16.Last= Ctl_Zoom.pAttr->CtlItemU16.Des;
						EP0_HSK();
					}
					break;
				default:
					{
						VCSTALL_EPCTL(VC_ERR_INVDREQ);
						return;
					}

				}
			}
			else
			{
				VCSTALL_EPCTL(VC_ERR_INVDCTL);
				return;
			}
			break;
		}
		// hemonel 2009-12-21: add Roll control
	case CT_ROLL_ABSOLUTE_CONTROL:
		{
			if((g_wCamTrmCtlSel&CT_CTL_SEL_ROLL_ABS)==CT_CTL_SEL_ROLL_ABS)
			{
				switch(req)
				{
				case GET_CUR:
				case GET_INFO:
				case GET_LEN:
				case GET_MAX:
				case GET_MIN:
				case GET_DEF:
				case GET_RES:
					ControlGetReqProc(&Ctl_Roll, req);
					break;
				case SET_CUR:
					if(ControlSetReqCheckProc(&Ctl_Roll)) // set value check succeed.
					{
						RollSet(Ctl_Roll.pAttr->CtlItemS16.Des);
						Ctl_Roll.ChangeFlag=CTL_CHNGFLG_RDY;
						Ctl_Roll.pAttr->CtlItemS16.Last= Ctl_Roll.pAttr->CtlItemS16.Des;
						EP0_HSK();
					}
					break;
				default:
					{
						VCSTALL_EPCTL(VC_ERR_INVDREQ);
						return;
					}

				}
			}
			else
			{
				VCSTALL_EPCTL(VC_ERR_INVDCTL);
				return;
			}
			break;
		}
#ifdef _AF_ENABLE_
	case CT_FOCUS_ABSOLUTE_CONTROL:
		{
			if((g_wCamTrmCtlSel&CT_CTL_SEL_FOCUS_ABS)==CT_CTL_SEL_FOCUS_ABS)
			{
				switch(req)
				{
				case GET_CUR:
				case GET_INFO:
				case GET_LEN:
				case GET_MAX:
				case GET_MIN:
				case GET_DEF:
				case GET_RES:
					ControlGetReqProc(&Ctl_FocusAbsolut, req);
					break;
				case SET_CUR:
					if(ControlSetReqCheckProc(&Ctl_FocusAbsolut))
					{
						FocusSet(Ctl_FocusAbsolut.pAttr->CtlItemU16.Des);
						Ctl_FocusAbsolut.ChangeFlag=CTL_CHNGFLG_RDY;
						Ctl_FocusAbsolut.pAttr->CtlItemU16.Last = Ctl_FocusAbsolut.pAttr->CtlItemU16.Des;
						EP0_HSK();
					}
					break;
				default:
					{
						VCSTALL_EPCTL(VC_ERR_INVDREQ);
						return;
					}

				}
			}
			else
			{
				VCSTALL_EPCTL(VC_ERR_INVDCTL);
				return;
			}
			break;
		}
	case CT_FOCUS_AUTO_CONTROL:
		{
			if((g_byCamTrmCtlSel_Ext&CT_CTL_SEL_FOCUS_AUTO)==CT_CTL_SEL_FOCUS_AUTO)
			{
				switch(req)
				{
				case GET_CUR:
				case GET_INFO:
				case GET_LEN:
				case GET_MAX:
				case GET_MIN:
				case GET_DEF:
				case GET_RES:
					ControlGetReqProc(&Ctl_FocusAuto, req);
					break;
				case SET_CUR:
					if(!WaitEP0_DATA_Out())
					{
						return;
					}
					setValue=EP0_DATA_OUT();
					if((setValue==1)
					        ||(setValue==0))
					{
						u8tmp=(Ctl_FocusAuto.pAttr->CtlItemU8.Last);
						Ctl_FocusAuto.pAttr->CtlItemU8.Des=setValue;
						Ctl_FocusAuto.pAttr->CtlItemU8.Last=setValue;

						Ctl_FocusAuto.ChangeFlag=CTL_CHNGFLG_RDY;
						EP0_HSK();

						if(u8tmp!=setValue)
						{
							if(setValue==1)
							{
								Ctl_FocusAbsolut.Info |=CONTROL_INFO_DIS_BY_AUTO|CONTROL_INFO_SP_AUTOUPDATA;
							}
							else
							{
								Ctl_FocusAbsolut.Info &= ~(CONTROL_INFO_DIS_BY_AUTO|CONTROL_INFO_SP_AUTOUPDATA);
							}
							Ctl_FocusAbsolut.ChangeFlag=CTL_CHNGFLG_RDY;
							VCStatusIntPacket_Int0(ENT_ID_CAMERA_IT,CT_FOCUS_ABSOLUTE_CONTROL,STS_PKT_ATTR_INFO_CHANGE,&(Ctl_FocusAbsolut.Info),1);
							WaitTimeOut_Delay(1);
							FocusAutoSet(setValue);
						}
					}
					else//invalid value
					{
						VCSTALL_EPCTL(VC_ERR_OUTOFRANGE);
						return;
					}
					break;
				default:
					{
						VCSTALL_EPCTL(VC_ERR_INVDREQ);
						return;
					}

				}
			}
			else
			{
				VCSTALL_EPCTL(VC_ERR_INVDCTL);
				return;
			}
			break;
		}
#endif

	default:
		{
			VCSTALL_EPCTL(VC_ERR_INVDCTL);
			return;
		}

	}
	return;
}
/*
*********************************************************************************************************
*									  Video processing unit request processing
* FUNCTION VideoCtlPUReq
*********************************************************************************************************
*/
/**
  Video processing unit request processing
  \param  None


  \retval None

*********************************************************************************************************
*/
void VideoCtlPUReq()
{
	U8 data req;
	U8 data setValue;
	U8 u8tmp;
	U8 byTmp2;

	req=XBYTE[SETUP_PKT_bREQUEST];

	switch(XBYTE[SETUP_PKT_wVALUE_H])
	{
	case PU_BRIGHTNESS_CONTROL:
		if((g_wProcUnitCtlSel&PU_CTL_SEL_BRIGHTNESS)==PU_CTL_SEL_BRIGHTNESS)
		{
			switch(req)
			{
			case GET_CUR:
			case GET_INFO:
			case GET_LEN:
			case GET_MAX:
			case GET_MIN:
			case GET_DEF:
			case GET_RES:
				ControlGetReqProc(&Ctl_Brightness, req);
				break;
			case SET_CUR:
				if(ControlSetReqCheckProc(&Ctl_Brightness))
				{

#ifdef _TCORRECTION_TEST_BY_BRIGHTNESS_				
					TrapeziumCorrectionSet((S8)(Ctl_Brightness.pAttr->CtlItemS16.Des) );
#else
					BrightnessSet(Ctl_Brightness.pAttr->CtlItemS16.Des);
#endif
					Ctl_Brightness.ChangeFlag=CTL_CHNGFLG_RDY;
					Ctl_Brightness.pAttr->CtlItemS16.Last= Ctl_Brightness.pAttr->CtlItemS16.Des;
					EP0_HSK();
				}
				break;
			default:
				{
					VCSTALL_EPCTL(VC_ERR_INVDREQ);
					return;
				}
			}
		}
		else
		{
			VCSTALL_EPCTL(VC_ERR_INVDCTL);
			return;
		}
		break;
	case PU_CONTRAST_CONTROL:
		if((g_wProcUnitCtlSel&PU_CTL_SEL_CONTRAST)==PU_CTL_SEL_CONTRAST)
		{
			switch(req)
			{
			case GET_CUR:
			case GET_INFO:
			case GET_LEN:
			case GET_MAX:
			case GET_MIN:
			case GET_DEF:
			case GET_RES:
				ControlGetReqProc(&Ctl_Contrast, req);
				break;
			case SET_CUR:
				if(ControlSetReqCheckProc(&Ctl_Contrast))
				{
					ContrastSet(Ctl_Contrast.pAttr->CtlItemU16.Des);
					Ctl_Contrast.ChangeFlag=CTL_CHNGFLG_RDY;
					Ctl_Contrast.pAttr->CtlItemU16.Last= Ctl_Contrast.pAttr->CtlItemU16.Des;
					EP0_HSK();					
				}
				break;
			default:
				{
					VCSTALL_EPCTL(VC_ERR_INVDREQ);
					return;
				}
			}
		}
		else
		{
			VCSTALL_EPCTL(VC_ERR_INVDCTL);
			return;
		}
		break;
	case PU_HUE_CONTROL:
		if((g_wProcUnitCtlSel&PU_CTL_SEL_HUE)==PU_CTL_SEL_HUE)
		{
			switch(req)
			{
			case GET_CUR:
			case GET_INFO:
			case GET_LEN:
			case GET_MAX:
			case GET_MIN:
			case GET_DEF:
			case GET_RES:
				ControlGetReqProc(&Ctl_Hue, req);
				break;
			case SET_CUR:
				if(ControlSetReqCheckProc(&Ctl_Hue))
				{
					HueSet(Ctl_Hue.pAttr->CtlItemS16.Des);
					Ctl_Hue.ChangeFlag=CTL_CHNGFLG_RDY;
					Ctl_Hue.pAttr->CtlItemS16.Last = Ctl_Hue.pAttr->CtlItemS16.Des;
					EP0_HSK();
				}
				break;
			default:
				{
					VCSTALL_EPCTL(VC_ERR_INVDREQ);
					return;
				}
			}
		}
		else
		{
			VCSTALL_EPCTL(VC_ERR_INVDCTL);
			return;
		}
		break;
	case PU_SATURATION_CONTROL:
		if((g_wProcUnitCtlSel&PU_CTL_SEL_SATURATION)==PU_CTL_SEL_SATURATION)
		{
			switch(req)
			{
			case GET_CUR:
			case GET_INFO:
			case GET_LEN:
			case GET_MAX:
			case GET_MIN:
			case GET_DEF:
			case GET_RES:
				ControlGetReqProc(&Ctl_Saturation, req);
				break;
			case SET_CUR:
				if(ControlSetReqCheckProc(&Ctl_Saturation))
				{
					SaturationSet(Ctl_Saturation.pAttr->CtlItemU16.Des);
					Ctl_Saturation.ChangeFlag=CTL_CHNGFLG_RDY;
					Ctl_Saturation.pAttr->CtlItemU16.Last= Ctl_Saturation.pAttr->CtlItemU16.Des;
					EP0_HSK();					
				}
				break;
			default:
				{
					VCSTALL_EPCTL(VC_ERR_INVDREQ);
					return;
				}
			}
		}
		else
		{
			VCSTALL_EPCTL(VC_ERR_INVDCTL);
			return;
		}
		break;
	case PU_SHARPNESS_CONTROL:
		if((g_wProcUnitCtlSel&PU_CTL_SEL_SHARPNESS)	== PU_CTL_SEL_SHARPNESS)
		{
			switch(req)
			{
			case GET_CUR:
			case GET_INFO:
			case GET_LEN:
			case GET_MAX:
			case GET_MIN:
			case GET_DEF:
			case GET_RES:
				ControlGetReqProc(&Ctl_Sharpness, req);
				break;
			case SET_CUR:
				if(ControlSetReqCheckProc(&Ctl_Sharpness))
				{
					SharpnessSet(Ctl_Sharpness.pAttr->CtlItemU16.Des);
					Ctl_Sharpness.ChangeFlag=CTL_CHNGFLG_RDY;						
					Ctl_Sharpness.pAttr->CtlItemU16.Last = Ctl_Sharpness.pAttr->CtlItemU16.Des;
					EP0_HSK();					
				}				
				break;
			default:
				{
					VCSTALL_EPCTL(VC_ERR_INVDREQ);
					return;
				}
			}
		}
		else
		{
			VCSTALL_EPCTL(VC_ERR_INVDCTL);
			return;
		}
		break;
	case PU_GAMMA_CONTROL:
		if((g_wProcUnitCtlSel&PU_CTL_SEL_GAMMA)	==PU_CTL_SEL_GAMMA)
		{
			switch(req)
			{
			case GET_CUR:
			case GET_INFO:
			case GET_LEN:
			case GET_MAX:
			case GET_MIN:
			case GET_DEF:
			case GET_RES:
				ControlGetReqProc(&Ctl_Gamma, req);
				break;
			case SET_CUR:
				if(ControlSetReqCheckProc(&Ctl_Gamma))
				{
					GammaSet(Ctl_Gamma.pAttr->CtlItemU16.Des);
					Ctl_Gamma.ChangeFlag=CTL_CHNGFLG_RDY;
					Ctl_Gamma.pAttr->CtlItemU16.Last = Ctl_Gamma.pAttr->CtlItemU16.Des;
					EP0_HSK();					
				}				
				break;
			default:
				{
					VCSTALL_EPCTL(VC_ERR_INVDREQ);
					return;
				}
			}
		}
		else
		{
			VCSTALL_EPCTL(VC_ERR_INVDCTL);
			return;
		}
		break;
	case PU_BACKLIGHT_COMPENSATION_CONTROL:
		if((g_wProcUnitCtlSel&PU_CTL_SEL_BACKLIGHT_COMPENSATION)==PU_CTL_SEL_BACKLIGHT_COMPENSATION)
		{
			switch(req)
			{
			case GET_CUR:
			case GET_INFO:
			case GET_LEN:
			case GET_MAX:
			case GET_MIN:
			case GET_DEF:
			case GET_RES:
				ControlGetReqProc(&Ctl_BackLightComp, req);
				break;
			case SET_CUR:
				if(ControlSetReqCheckProc(&Ctl_BackLightComp))
				{
					BackLightCompSet(Ctl_BackLightComp.pAttr->CtlItemU16.Des);
					Ctl_BackLightComp.ChangeFlag=CTL_CHNGFLG_RDY;
					Ctl_BackLightComp.pAttr->CtlItemU16.Last = Ctl_BackLightComp.pAttr->CtlItemU16.Des;
					EP0_HSK();
				}
				break;
			default:
				{
					VCSTALL_EPCTL(VC_ERR_INVDREQ);
					return;
				}
			}
		}
		else
		{
			VCSTALL_EPCTL(VC_ERR_INVDCTL);
			return;
		}
		break;
	case PU_GAIN_CONTROL	:
		if((g_wProcUnitCtlSel&PU_CTL_SEL_GAIN)==PU_CTL_SEL_GAIN)
		{
			switch(req)
			{
			case GET_CUR:
			case GET_INFO:
			case GET_LEN:
			case GET_MAX:
			case GET_MIN:
			case GET_DEF:
			case GET_RES:
				ControlGetReqProc(&Ctl_Gain, req);
				break;
			case SET_CUR:
				if(ControlSetReqCheckProc(&Ctl_Gain))
				{
					GainSet(Ctl_Gain.pAttr->CtlItemU16.Des);
					Ctl_Gain.ChangeFlag=CTL_CHNGFLG_RDY;
					Ctl_Gain.pAttr->CtlItemU16.Last = Ctl_Gain.pAttr->CtlItemU16.Des;
					EP0_HSK();
				}
				break;
			default:
				{
					VCSTALL_EPCTL(VC_ERR_INVDREQ);
					return;
				}
			}
		}
		else
		{
			VCSTALL_EPCTL(VC_ERR_INVDCTL);
			return;
		}
		break;
	case PU_POWER_LINE_FREQUENCY_CONTROL:
		if((g_wProcUnitCtlSel&PU_CTL_SEL_POWER_LINE_FREQUENCY)	==PU_CTL_SEL_POWER_LINE_FREQUENCY)
		{
			switch(req)
			{
			case GET_CUR:
			case GET_INFO:
			case GET_LEN:
			case GET_MAX:
			case GET_MIN:
			case GET_DEF:
			case GET_RES:
				ControlGetReqProc(&Ctl_PwrLineFreq, req);
				break;
			case SET_CUR:
				if(ControlSetReqCheckProc(&Ctl_PwrLineFreq))
				{
					PwrLinFreqSet(Ctl_PwrLineFreq.pAttr->CtlItemU8.Des);		
					Ctl_PwrLineFreq.ChangeFlag=CTL_CHNGFLG_RDY;
					Ctl_PwrLineFreq.pAttr->CtlItemU8.Last = Ctl_PwrLineFreq.pAttr->CtlItemU8.Des;
					EP0_HSK();
				}
				break;
			default:
				{
					VCSTALL_EPCTL(VC_ERR_INVDREQ);
					return;
				}
			}
		}
		else
		{
			VCSTALL_EPCTL(VC_ERR_INVDCTL);
			return;
		}
		break;
	case PU_WHITE_BALANCE_TEMPERATURE_CONTROL:
		if((g_wProcUnitCtlSel&PU_CTL_SEL_WHITE_BALANCE_TEMPERATURE)==	PU_CTL_SEL_WHITE_BALANCE_TEMPERATURE)
		{
			switch(req)
			{
			case GET_CUR:
			case GET_INFO:
			case GET_LEN:
			case GET_MAX:
			case GET_MIN:
			case GET_DEF:
			case GET_RES:
				ControlGetReqProc(&Ctl_WhiteBalanceTemp, req);
				break;
			case SET_CUR:
				if(ControlSetReqCheckProc(&Ctl_WhiteBalanceTemp))
				{
					WhiteBalanceTempSet(Ctl_WhiteBalanceTemp.pAttr->CtlItemU16.Des);
					Ctl_WhiteBalanceTemp.ChangeFlag=CTL_CHNGFLG_RDY;
					Ctl_WhiteBalanceTemp.pAttr->CtlItemU16.Last = Ctl_WhiteBalanceTemp.pAttr->CtlItemU16.Des;
					EP0_HSK();					
				}
				break;
			default:
				{
					VCSTALL_EPCTL(VC_ERR_INVDREQ);
					return;
				}
			}
		}
		else
		{
			VCSTALL_EPCTL(VC_ERR_INVDCTL);
			return;
		}
		break;
	case PU_WHITE_BALANCE_TEMPERATURE_AUTO_CONTROL:
		if((g_wProcUnitCtlSel&PU_CTL_SEL_WHITE_BALANCE_TEMPERATURE_AUTO)	==PU_CTL_SEL_WHITE_BALANCE_TEMPERATURE_AUTO)
		{
			switch(req)
			{
			case GET_CUR:
			case GET_INFO:
			case GET_LEN:
			case GET_MAX:
			case GET_MIN:
			case GET_DEF:
			case GET_RES:
				ControlGetReqProc(&Ctl_WhiteBalanceTempAuto, req);
				break;
			case SET_CUR:
				if(!WaitEP0_DATA_Out())
				{
					return ;
				}
				setValue=EP0_DATA_OUT();
				if((setValue==1)||(setValue==0))
				{
					// hemonel 2009-11-26: use new control struct
					//	u8tmp=Ctl_WhiteBalanceTempAuto.pLast->CurU8;

					//	Ctl_WhiteBalanceTempAuto.pDes->CurU8=setValue;
					//	Ctl_WhiteBalanceTempAuto.pLast->CurU8=setValue;

					u8tmp=Ctl_WhiteBalanceTempAuto.pAttr->CtlItemU8.Last;

					Ctl_WhiteBalanceTempAuto.pAttr->CtlItemU8.Des =setValue;
					Ctl_WhiteBalanceTempAuto.pAttr->CtlItemU8.Last =setValue;

					Ctl_WhiteBalanceTempAuto.ChangeFlag=CTL_CHNGFLG_RDY;

					//2010-03-25 hemonel: when disable gain function, manual white balance USB_IF UVCTest fail.
					g_byWhiteBalanceAutoLast_BackForUVCtest = WhiteBalanceTempAutoItem.Last;

					if(u8tmp!=setValue)
					{
						//@@@update last and desc value.
						if(setValue==1)
						{
							Ctl_WhiteBalanceTemp.Info |= CONTROL_INFO_DIS_BY_AUTO;
							byTmp2=1;
						}
						else
						{
							Ctl_WhiteBalanceTemp.Info  &=  ~(CONTROL_INFO_DIS_BY_AUTO);
							byTmp2=0;
						}

						WhiteBalanceTempAutoSet(byTmp2);
						EP0_HSK();
						Ctl_WhiteBalanceTemp.ChangeFlag = CTL_CHNGFLG_RDY;//@@@@@@@, change last and desc value at the same time.
						//	VCStatusIntPacket_Int0(ENT_ID_PROCESSING_UNIT,PU_WHITE_BALANCE_TEMPERATURE_CONTROL,STS_PKT_ATTR_INFO_CHANGE,&(Ctl_WhiteBalanceTemp.Info),1);
						VCStatusIntPacket_Int0(ENT_ID_PROCESSING_UNIT,PU_WHITE_BALANCE_TEMPERATURE_CONTROL,STS_PKT_ATTR_INFO_CHANGE,&(Ctl_WhiteBalanceTemp.Info),1);

					}
					else
					{
						EP0_HSK();
					}

				}
				else//invalid value
				{
					VCSTALL_EPCTL(VC_ERR_OUTOFRANGE);
					return;
				}
				break;
			default:
				{
					VCSTALL_EPCTL(VC_ERR_INVDREQ);
					return;
				}
			}
		}
		else
		{
			VCSTALL_EPCTL(VC_ERR_INVDCTL);
			return;
		}
		break;
	default:
		{
			VCSTALL_EPCTL(VC_ERR_INVDCTL);
			return;
		}
	}
	return;
}
/*
*********************************************************************************************************
*									  Video debug extension unit request processing
* FUNCTION VideoCtlEXUDbgReq
*********************************************************************************************************
*/
/**
  Video debug extension unit request processing
  \param  None


  \retval None

*********************************************************************************************************
*/

void VideoCtlEXUDbgReq()
{
	U8 req,CS,byReqLen;
//	static U16 data s_addr;
//	U8  t[4];
//	U16 wValue;
	U8  i;
	req=XBYTE[SETUP_PKT_bREQUEST];
	CS=XBYTE[SETUP_PKT_wVALUE_H];
	byReqLen=XBYTE[SETUP_PKT_wLEN_L];
	switch(CS)
	{
#ifdef 	_TCORRECTION_EN_
	case EXU1_TRAPEZIUM_CORRECTION:
		if((g_wExtUIspCtl&EXU1_CTL_SEL_TCORRECTION)==EXU1_CTL_SEL_TCORRECTION)
		{
			switch(req)
			{
			case GET_CUR:
			case GET_INFO:
			case GET_LEN:
			case GET_MAX:
			case GET_MIN:
			case GET_DEF:
			case GET_RES:
				ControlGetReqProc(&Ctl_TrapeziumCorrection, req);
				break;
			case SET_CUR:
				if(ControlSetReqCheckProc(&Ctl_TrapeziumCorrection))
				{
					TrapeziumCorrectionSet(Ctl_TrapeziumCorrection.pAttr->CtlItemS8.Des);
					Ctl_TrapeziumCorrection.ChangeFlag=CTL_CHNGFLG_RDY;
					Ctl_TrapeziumCorrection.pAttr->CtlItemS8.Last= Ctl_TrapeziumCorrection.pAttr->CtlItemS8.Des;
					EP0_HSK();
				}
				break;
			default:
				{
					VCSTALL_EPCTL(VC_ERR_INVDREQ);
					return;
				}
			}
		}
		else
		{
			VCSTALL_EPCTL(VC_ERR_INVDCTL);
			return;
		}
		break;
#endif
	case EXU1_CMD_STS:
	case EXU1_DATA_INOUT:
		{
			switch(req)
			{
			case GET_MIN:
			case GET_DEF:
				EP0_FFFLUSH();
				if(CS ==EXU1_CMD_STS)
				{
					for(i=0; i<MAX_RWFLASH_SIZE; i++)
					{
						EP0_DATA_IN(0);
					}
				}
				else
				{
					for(i=0; i<byReqLen; i++)
					{
						EP0_DATA_IN(0);
					}
				}
				EP0_FFV_HSK();
				break;
			case GET_MAX:
				EP0_FFFLUSH();
				if(CS ==EXU1_CMD_STS)
				{
					for(i=0; i<MAX_RWFLASH_SIZE; i++)
					{
						EP0_DATA_IN(0xff);
					}
				}
				else
				{
					for(i=0; i<byReqLen; i++)
					{
						EP0_DATA_IN(0xff);
					}
				}
				EP0_FFV_HSK();
				break;
			case GET_RES:
				EP0_FFFLUSH();
				EP0_DATA_IN(0x1);
				// hemonel 2009-08-18: linux driver isue
				// the number of bytes returned must be the same as the extension unit control size
				if(CS ==EXU1_CMD_STS)
				{
					for(i=0; i<MAX_RWFLASH_SIZE-1; i++)
					{
						EP0_DATA_IN(0);
					}
				}
				else
				{
					for(i=0; i<byReqLen-1; i++)
					{
						EP0_DATA_IN(0);
					}
				}

				EP0_FFV_HSK();
				break;
			case GET_INFO:
				EP0_FFFLUSH();
				EP0_DATA_IN(CONTROL_INFO_SP_GET|CONTROL_INFO_SP_SET);
				EP0_FFV_HSK();
				break;
			case GET_LEN:
				EP0_FFFLUSH();
				if(CS ==EXU1_CMD_STS)
				{
					EP0_DATA_IN(MAX_RWFLASH_SIZE);
				}
				else
				{
					EP0_DATA_IN(g_byDataInOutCtlSize);
				}
				EP0_DATA_IN(0);
				EP0_FFV_HSK();
				break;
			case GET_CUR:
				EP0_FFFLUSH();
				VendorCmdProc(CS, GET_CUR,byReqLen);
				break;
			case SET_CUR:
				if(!WaitEP0_DATA_Out())
				{
					return ;
				}
				VendorCmdProc(CS, SET_CUR,byReqLen);
				break;
			default:
				{
					VCSTALL_EPCTL(VC_ERR_INVDREQ);
					return;
				}
			}
			break;
		}
#ifdef _GPIO_STS_INT_TEST_
	case EXU1_GPIO_NOTITY:
		{
			switch(req)
			{
			case GET_MIN:
			case GET_DEF:
				EP0_FFFLUSH();
				EP0_DATA_IN(0);
				EP0_DATA_IN(0);
				EP0_DATA_IN(0);
				EP0_DATA_IN(0);
				EP0_FFV_HSK();
				break;
			case GET_MAX:
				EP0_FFFLUSH();
				EP0_DATA_IN(0xFF);
				EP0_DATA_IN(0xFF);
				EP0_DATA_IN(0xFF);
				EP0_DATA_IN(0xFF);
				EP0_FFV_HSK();
				break;
			case GET_RES:
				EP0_FFFLUSH();
				EP0_DATA_IN(0x1);
				EP0_FFV_HSK();
				break;
			case GET_INFO:
				EP0_FFFLUSH();
				EP0_DATA_IN(CONTROL_INFO_SP_AUTOUPDATA|CONTROL_INFO_SP_GET);
				EP0_FFV_HSK();
				break;
			case GET_LEN:
				EP0_FFFLUSH();
				EP0_DATA_IN(CONTROL_LEN_4);
				EP0_DATA_IN(0);
				EP0_FFV_HSK();
				break;
			case GET_CUR:
				EP0_FFFLUSH();
				EP0_DATA_IN(g_wGPIOLastValue%256);
				EP0_DATA_IN(g_wGPIOLastValue/256);
				EP0_DATA_IN(g_wGPIOCurValue%256);
				EP0_DATA_IN(g_wGPIOCurValue/256);
				EP0_FFV_HSK();
				break;
			case SET_CUR:  // do not support SET_CUR request.
			default:
				{
					VCSTALL_EPCTL(VC_ERR_INVDREQ);
					return;
				}
			}
			break;
		}
#endif
	default:
		{
			VCSTALL_EPCTL(VC_ERR_INVDCTL);
			return;
		}

	}

}

#ifdef _DELL_EXU_
static U8  XU1_AutoGainCtl(U8 option, U8 by_Gain)
{
	option = option;
	by_Gain = by_Gain;
	return TRUE;
}
static U8 XU1_AutoExpTimeCtl(U8 option, U16 w_ExpTime)
{
	option = option;
	w_ExpTime = w_ExpTime;
	return TRUE;
}

/*
*********************************************************************************************************
*									  Video  XU1 extension unit request processing for DELL
* FUNCTION VideoCtlXU1EXUReq
*********************************************************************************************************
*/
/**
  Video debug extension unit request processing
  \param  None


  \retval None

*********************************************************************************************************
*/
void VideoCtlXU1EXUReq()
{
	U8 req,CS;//,byReqLen;	//pc-lint.unused.so delete it temporarily.
//	static U16 data s_addr;
//	U8  t[4];
//	U16 wValue;
//	U8  i;
//	U8 status;
	req=XBYTE[SETUP_PKT_bREQUEST];
	CS=XBYTE[SETUP_PKT_wVALUE_H];
	//byReqLen=XBYTE[SETUP_PKT_wLEN_L];	//pc-lint.unused.so delete it temporarily.
	switch(CS)
	{
	case 0x01: //XU_DEVICE_INFO:
		switch(req)
		{
		case GET_LEN:
			EP0_FFFLUSH();
			EP0_DATA_IN(0x09);
			EP0_DATA_IN(0x00);
			EP0_FFV_HSK();
			break;
		case GET_CUR:
		case GET_MIN:
		case GET_MAX:
		case GET_DEF:
		case GET_RES:
			EP0_FFFLUSH();
			//0x1028 Fixed value as Dell's Register Vendor ID
			EP0_DATA_IN(0x28);
			EP0_DATA_IN(0x10);

			// 1 : Camera for Consumer and Business Client Laptops
			EP0_DATA_IN(0x01);
			EP0_DATA_IN(0x00);

			//The control support indicator (1 means supported).
			//D0:  XU_DEVICE_INFO
			//D1:  XU_LED_CONTROL
			//D3: XU_AE_MAX_AUTO_GAIN_CONTROL
			//D4: XU_AE_MAX_EXPOSURE_TIME_CONTROL
			//D5~D31:  (Reserved)
			EP0_DATA_IN(0x1B);
			EP0_DATA_IN(0xFF);
			EP0_DATA_IN(0xFF);
			EP0_DATA_IN(0xFF);

			//Default set as 0xFF
			EP0_DATA_IN(0xFF);
			EP0_FFV_HSK();

			break;

		case GET_INFO:
			EP0_FFFLUSH();
			EP0_DATA_IN(CONTROL_INFO_SP_GET|CONTROL_INFO_SP_SET);
			EP0_FFV_HSK();
			break;
		default:
			break;
		}
		break;

	case 0x02: //XU_LED_CONTROL:
		{
			switch(req)
			{
			case GET_LEN:
				EP0_FFFLUSH();
				EP0_DATA_IN(0x01);
				EP0_DATA_IN(0x00);
				EP0_FFV_HSK();
				break;

			case GET_INFO:
				EP0_FFFLUSH();
				EP0_DATA_IN(CONTROL_INFO_SP_GET|CONTROL_INFO_SP_SET);
				EP0_FFV_HSK();
				break;

			case GET_MIN:
				EP0_FFFLUSH();
				EP0_DATA_IN(0x00);
				EP0_FFV_HSK();
				break;

			case GET_MAX:
				EP0_FFFLUSH();
				EP0_DATA_IN(0x06);
				EP0_FFV_HSK();
				break;

			case GET_RES:
				EP0_FFFLUSH();
				EP0_DATA_IN(0x01);
				EP0_FFV_HSK();
				break;

			case GET_DEF:
				EP0_FFFLUSH();
				EP0_DATA_IN(0x01);
				EP0_FFV_HSK();
				break;

			case GET_CUR:
				EP0_FFFLUSH();
				EP0_DATA_IN(g_byLEDStatus);
				EP0_FFV_HSK();
				break;

			case SET_CUR:
				if(!WaitEP0_DATA_Out())
				{
					return ;
				}
				g_byLEDStatus = EP0_DATA_OUT();
				g_byLEDBlinkCnt = 0;
				EP0_HSK();
				break;
			default:
				break;
			}
		}
		break;

	case 0x03: //XU_AE_MAX_AUTO_GAIN_CONTROL:
		{
			switch(req)
			{
			case GET_LEN:
				EP0_FFFLUSH();
				EP0_DATA_IN(0x01);
				EP0_DATA_IN(0x00);
				EP0_FFV_HSK();
				break;

			case GET_INFO:
				EP0_FFFLUSH();
				EP0_DATA_IN(CONTROL_INFO_SP_GET|CONTROL_INFO_SP_SET);
				EP0_FFV_HSK();
				break;

			case GET_MIN:
				EP0_FFFLUSH();
				EP0_DATA_IN(0x01);
				EP0_FFV_HSK();
				break;

			case GET_MAX:
				EP0_FFFLUSH();
				EP0_DATA_IN(0x05);
				EP0_FFV_HSK();
				break;

			case GET_RES:
				EP0_FFFLUSH();
				EP0_DATA_IN(0x01);
				EP0_FFV_HSK();
				break;

			case GET_DEF:
				EP0_FFFLUSH();
				EP0_DATA_IN(0x01);
				EP0_FFV_HSK();
				break;

			case GET_CUR:
				if (TRUE == XU1_AutoGainCtl(GET_CUR, 0))
				{
					EP0_DATA_IN(g_byMaxAutoGain);
					EP0_FFV_HSK();
				}
				else
				{
					VCSTALL_EPCTL(VC_ERR_INVDCTL);
				}
				break;

			case SET_CUR:
				if(!WaitEP0_DATA_Out())
				{
					return ;
				}
				g_byMaxAutoGain = EP0_DATA_OUT();
				if (TRUE == XU1_AutoGainCtl(SET_CUR, g_byMaxAutoGain))
				{
					EP0_HSK();
				}
				else
				{
					VCSTALL_EPCTL(VC_ERR_INVDCTL);
				}
				break;
			default:
				break;
			}
		}
		break;
	case 0x04: //XU_AE_MAX_EXPOSURE_TIME_CONTROL:
		{
			switch(req)
			{
			case GET_LEN:
				EP0_FFFLUSH();
				EP0_DATA_IN(0x02);
				EP0_DATA_IN(0x00);
				EP0_FFV_HSK();
				break;

			case GET_INFO:
				EP0_FFFLUSH();
				EP0_DATA_IN(CONTROL_INFO_SP_GET|CONTROL_INFO_SP_SET);
				EP0_FFV_HSK();
				break;

			case GET_MIN:
				EP0_FFFLUSH();
				EP0_DATA_IN(0x04);
				EP0_DATA_IN(0x00);
				EP0_FFV_HSK();
				break;

			case GET_MAX:
				EP0_FFFLUSH();
				EP0_DATA_IN(0x08);
				EP0_DATA_IN(0x00);
				EP0_FFV_HSK();
				break;

			case GET_RES:
				EP0_FFFLUSH();
				EP0_DATA_IN(0x04);
				EP0_DATA_IN(0x00);
				EP0_FFV_HSK();
				break;

			case GET_DEF:
				EP0_FFFLUSH();
				EP0_DATA_IN(0x08);
				EP0_DATA_IN(0x00);
				EP0_FFV_HSK();
				break;

			case GET_CUR:
				if (TRUE == XU1_AutoExpTimeCtl(GET_CUR, 0))
				{
					EP0_DATA_IN(g_wMaxAutoExpTime&0xFF);
					EP0_DATA_IN(g_wMaxAutoExpTime>>8);
					EP0_FFV_HSK();
				}
				else
				{
					VCSTALL_EPCTL(VC_ERR_INVDCTL);
				}
				break;

			case SET_CUR:
				if(!WaitEP0_DATA_Out())
				{
					return ;
				}
				ASSIGN_U16BYTE(g_wMaxAutoExpTime, 0, EP0_DATA_OUT());
				ASSIGN_U16BYTE(g_wMaxAutoExpTime, 1, EP0_DATA_OUT());
				if (TRUE == XU1_AutoExpTimeCtl(SET_CUR, g_wMaxAutoExpTime))
				{
					EP0_HSK();
				}
				else
				{
					VCSTALL_EPCTL(VC_ERR_INVDCTL);
				}
				break;
			default:
				break;
			}
		}
		break;
	default:
		{
			VCSTALL_EPCTL(VC_ERR_INVDCTL);
			return;
		}
	}

	return;
}
#endif

#ifdef _FACIALAEWINSET_XU_
void FaceAECtlXUReq()
{
	U8 req,CS,byReqLen;
	U8 i;
	req=XBYTE[SETUP_PKT_bREQUEST];
	CS=XBYTE[SETUP_PKT_wVALUE_H];
	byReqLen=XBYTE[SETUP_PKT_wLEN_L];

	switch(CS)
	{
		case 0x01:
			switch(req)
			{					
				case SET_CUR:
					if((byReqLen>8)||(byReqLen ==0))
					{
						VCSTALL_EPCTL(VC_ERR_UNKNOWN);
						return ;
					}
					if(!WaitEP0_DATA_Out())
					{
						return;
					}
					for(i=0;i<byReqLen;i++)
					{
						g_byaFacial_AE_Parameters[i]=EP0_DATA_OUT();
					}					
					g_byXU_FacialAEWin_Set=1;
					EP0_HSK();
					break;
				case GET_CUR:
					EP0_FFFLUSH();
					for(i=0;i<byReqLen;i++)
					{
						EP0_DATA_IN(g_byaFacial_AE_Parameters[i]);
					}									
					EP0_FFV_HSK();						
					break;
				case GET_LEN:	
					EP0_FFFLUSH();
					EP0_DATA_IN(8);
					EP0_DATA_IN(0);
					EP0_FFV_HSK();
					break;
				case GET_MIN:
					EP0_FFFLUSH();
					EP0_DATA_IN(0);
					EP0_DATA_IN(0);
					EP0_DATA_IN(0);//Facial_AE_Parameters.byaValue[0]
					EP0_DATA_IN(0);//Facial_AE_Parameters.byaValue[1]
					EP0_DATA_IN(0);//Facial_AE_Parameters.byaValue[2]
					EP0_DATA_IN(0);//Facial_AE_Parameters.byaValue[3]
					EP0_DATA_IN(0);	//Facial_AE_Parameters.byCmd
					EP0_DATA_IN(0);					
					EP0_FFV_HSK();						
					break;
				case GET_MAX:
					EP0_FFFLUSH();
					EP0_DATA_IN(0);
					EP0_DATA_IN(0);
					EP0_DATA_IN(25);//Facial_AE_Parameters.byaValue[0]
					EP0_DATA_IN(0);//Facial_AE_Parameters.byaValue[1]
					EP0_DATA_IN(0);//Facial_AE_Parameters.byaValue[2]
					EP0_DATA_IN(0);//Facial_AE_Parameters.byaValue[3]
					EP0_DATA_IN(0);	//Facial_AE_Parameters.byCmd
					EP0_DATA_IN(0);					
					EP0_FFV_HSK();						
					break;
				case GET_INFO:
					EP0_FFFLUSH();						
					EP0_DATA_IN(CONTROL_INFO_SP_SET|CONTROL_INFO_SP_GET);//Facial_AE_Parameters.byaValue[0]										
					EP0_FFV_HSK();
					break;
				case GET_RES:
					EP0_FFFLUSH();
					EP0_DATA_IN(0);
					EP0_DATA_IN(0);
					EP0_DATA_IN(1);//Facial_AE_Parameters.byaValue[0]
					EP0_DATA_IN(0);//Facial_AE_Parameters.byaValue[1]
					EP0_DATA_IN(0);//Facial_AE_Parameters.byaValue[2]
					EP0_DATA_IN(0);//Facial_AE_Parameters.byaValue[3]
					EP0_DATA_IN(0);	//Facial_AE_Parameters.byCmd
					EP0_DATA_IN(0);					
					EP0_FFV_HSK();
					break;
				case GET_DEF:
					EP0_FFFLUSH();
					EP0_DATA_IN(0);
					EP0_DATA_IN(0);
					EP0_DATA_IN(0);//Facial_AE_Parameters.byaValue[0]
					EP0_DATA_IN(0);//Facial_AE_Parameters.byaValue[1]
					EP0_DATA_IN(0);//Facial_AE_Parameters.byaValue[2]
					EP0_DATA_IN(0);//Facial_AE_Parameters.byaValue[3]
					EP0_DATA_IN(0);	//Facial_AE_Parameters.byCmd
					EP0_DATA_IN(0);					
					EP0_FFV_HSK();	
					break;	
				default:
					{	
						VCSTALL_EPCTL(VC_ERR_INVDREQ);
						return;
					}
			}
			break;
		default:
		{	
			VCSTALL_EPCTL(VC_ERR_INVDCTL);
			return;
		}
	}
	return;
}
#endif


/*
*********************************************************************************************************
*									     Video CONTROL  request processing
* FUNCTION VideoControlReq
*********************************************************************************************************
*/
/**
    Video CONTROL  request dispatch routine
  \param  None


  \retval None

*********************************************************************************************************
*/
void VideoControlReq()
{
	switch(XBYTE[SETUP_PKT_wINDEX_H])
	{
	case 0x00:   //zero,  interface control requests
		VideoControlIfReq();
		break;
	case ENT_ID_CAMERA_IT:
		VideoCtlCTReq();
		break;
	case ENT_ID_PROCESSING_UNIT:
		VideoCtlPUReq();
		break;
	case ENT_ID_EXTENSION_UNIT_DBG:
		VideoCtlEXUDbgReq();
		break;
#if defined _RTK_EXTENDED_CTL_ || defined _LENOVO_IDEA_EYE_EXTENDEDUNIT_
	case ENT_ID_RTK_EXTENDED_CTL_UNIT:
		VideoCtlRtkExtReq();
		break;		
#endif	
	case ENT_ID_OUTPUT_TRM:
		{
			VCSTALL_EPCTL(VC_ERR_INVDCTL);
			break;
		}
#ifdef _DELL_EXU_
	case ENT_ID_EXTENSION_UNIT_XU1:
		VideoCtlXU1EXUReq();
		break;
#endif
#ifdef _FACIALAEWINSET_XU_
	case ENT_ID_EXTENSION_UNIT_FACEAE:
		FaceAECtlXUReq();
		break;
#endif		
	default:
		{
			VCSTALL_EPCTL(VC_ERR_INVDUNIT);
			break;
		}
	}
	return;
}

U8 checkformatFrameIdx(U8 const FmtIdx, U8 const FrmIdx, U8 IsVideo) 
{
	U8 byMaxFrmDescNum;
	U8 byMaxFmtDescNum;

	byMaxFrmDescNum = GetFormatFrameNum(IsVideo,FmtIdx,(U8)g_bIsHighSpeed);
	byMaxFmtDescNum =  GetFormatNum(g_bIsHighSpeed);

	if((FmtIdx>byMaxFmtDescNum)||(FmtIdx==0))
	{
		return FALSE;
	}

	if((FrmIdx>byMaxFrmDescNum)||(FrmIdx==0))
	{
		return FALSE;
	}

	return TRUE;
}

/*
*********************************************************************************************************
*									     Video stream PROBE request processing
* FUNCTION VideoStreamProbeReq
*********************************************************************************************************
*/
/**
    Video stream PROBE request processing
  \param
        req: request ID (GET_MIN,GET_MAX,GET_DEG,GET_CUR,SET_CUR)
  \retval None

*********************************************************************************************************
*/

void VideoStreamReq(U8 const req, U8 const CS)
{
	static U8 bysIsNegotiateFps=0;
	U8 i;
	U8 bmHint_L;
	U8 bmHint_H;
	U8 FmtIdx;
	U8 FrmIdx;
	U8 FpsIdx; //=100;  hemonel 2009-10-21: set fps the minimum one at init.
	U8 byTmp;
	U32 dwTmp;
	VSProbCommCtl_t  VsProbeCommitTmp;
	U8 fpsTmp;
	if(req==SET_CUR)
	{
		//while(EP0_TST_EMPTY());
		if(!WaitEP0_DATA_Out())
		{
			return;
		}
		bmHint_L=EP0_DATA_OUT(); //bmHint_L
		bmHint_H=EP0_DATA_OUT();//bmHint_H
		ASSIGN_INT(VsProbeCommitTmp.bmHint, bmHint_H, bmHint_L);
		//format
		FmtIdx= EP0_DATA_OUT();
		FrmIdx= EP0_DATA_OUT();

		if(!checkformatFrameIdx(FmtIdx, FrmIdx,UVC_IS_VIEDO))// check format idx and frame idx
		{
			VCSTALL_EPCTL(VC_ERR_OUTOFRANGE);
			return;
		}

		VsProbeCommitTmp.bFormatIndex=FmtIdx;
		VsProbeCommitTmp.bFrameIndex=FrmIdx;



		ASSIGN_U32BYTE(dwTmp,0,EP0_DATA_OUT());
		ASSIGN_U32BYTE(dwTmp,1,EP0_DATA_OUT());
		ASSIGN_U32BYTE(dwTmp,2,EP0_DATA_OUT());
		ASSIGN_U32BYTE(dwTmp,3,EP0_DATA_OUT());

		//interval
		//	ASSIGN_U32BYTE(VsProbeCommitTmp.dwFrameInterval,0,EP0_DATA_OUT());
		//	ASSIGN_U32BYTE(VsProbeCommitTmp.dwFrameInterval,1,EP0_DATA_OUT());
		//	ASSIGN_U32BYTE(VsProbeCommitTmp.dwFrameInterval,2,EP0_DATA_OUT());
		//	ASSIGN_U32BYTE(VsProbeCommitTmp.dwFrameInterval,3,EP0_DATA_OUT());
		VsProbeCommitTmp.dwFrameInterval=dwTmp;
		//DBG(("@9   %lu\n",VsProbeCommitTmp.dwFrameInterval));
		if(CS == VS_PROBE_CONTROL)
		{
			if(VsProbeCommitTmp.dwFrameInterval ==0)
			{
				bysIsNegotiateFps = 1;
			}
			else
			{
				bysIsNegotiateFps = 0;
			}
		}

		byTmp=GetFPSNum(1, FmtIdx, FrmIdx, g_bIsHighSpeed);

		FpsIdx = byTmp;	// hemonel 2009-10-21: set fps the minimum one at init.
		for(i=1; i<=byTmp; i++)
		{
			dwTmp = GetFrameInterval(UVC_IS_VIEDO,(FmtIdx), FrmIdx, g_bIsHighSpeed, i);
			//	DBG(("@12  %lu Vs %lu\n",VsProbeCommitTmp.dwFrameInterval,dwTmp));
			//	if(VsProbeCommitTmp.dwFrameInterval==dwTmp)
			// hemonel 2009-06-18: windows seven bug
			//	if(VsProbeCommitTmp.dwFrameInterval <= dwTmp)
			//20100628 Dave Fix Video reconding at Windows Media Encoder with 29.97fps
			//334:	//20100628 Dave Fix Video reconding at Windows Media Encoder with 29.97fps
			if(VsProbeCommitTmp.dwFrameInterval <= (dwTmp+ 334))
			{
				FpsIdx=i;
				break;
			}
		}

		//other fields, ignored.
		EP0_FFFLUSH();
		EP0_HSK();

		fpsTmp= GetFPS(UVC_IS_VIEDO,FmtIdx, FrmIdx, g_bIsHighSpeed, FpsIdx);
#ifdef _USB2_LPM_
		GetBulkTransferSize(FmtIdx,FrmIdx,fpsTmp);
#endif
		VsProbeCommitTmp.dwMaxVideoFrameSize
		    =GetMaxFrameSize(UVC_IS_VIEDO,FmtIdx,FrmIdx ,g_bIsHighSpeed);
		VsProbeCommitTmp.dwMaxPayloadTransferSize
		    =GetMaxPayloadTransferSize(VsProbeCommitTmp.dwMaxVideoFrameSize, fpsTmp, (U8)g_bIsHighSpeed);
		if(CS == VS_PROBE_CONTROL)
		{
			memcpy(&g_VsProbe,&VsProbeCommitTmp,sizeof(VSProbCommCtl_t));
		}
		else
		{

			g_byCommitFPS= fpsTmp;
			memcpy(&g_VsCommit,&VsProbeCommitTmp,sizeof(VSProbCommCtl_t));

#ifdef _BULK_ENDPOINT_
#ifdef _UVC_COMMITWB_
			if((g_VsCommit.bFormatIndex != g_VsBulkCommit.byCommitFormatIndex)
			        ||(g_VsCommit.bFrameIndex != g_VsBulkCommit.byCommitFrameIndex)
			        ||(g_byCommitFPS != g_VsBulkCommit.byCommitFPS)
			        ||(g_wLWM != g_VsBulkCommit.wLWM)
			        ||(g_wHWM != g_VsBulkCommit.wHWM)
			        ||(g_wTransferSize != g_VsBulkCommit.wTransferSize))
			{
				g_byCommitChange_Flag = 1;
			}		
#endif
#if (defined _UVC_PPWB_ || defined _UVC_COMMITWB_)
			ParaWriteBackToFlash();
#endif
#endif
		}
	}
	else
	{
		if(CS == VS_PROBE_CONTROL)
		{
			VideoStreamProbePacket(&g_VsProbe,req,bysIsNegotiateFps);
			bysIsNegotiateFps=0;
		}
		else
		{
			//	VideoStreamProbePacket(&g_VsProbe,req,0);
			// hemonel 2010-09-09: firmware bug, must return commit control
			VideoStreamProbePacket(&g_VsCommit,req,0);
		}
	}
}

/*
*********************************************************************************************************
*									     Video still PROBE request processing
* FUNCTION VideoStreamStillProbeReq
*********************************************************************************************************
*/
/**
    Video still image PROBE request processing
  \param
        req: request ID (GET_MIN,GET_MAX,GET_DEG,GET_CUR,SET_CUR)


  \retval None

*********************************************************************************************************
*/
void VideoStreamStillReq(U8 const req, U8 const CS)
{
	U8 FmtIdx = 0;
	U8 FrmIdx = 0;
	U8 CmpIdx = 0;
	VSStlProbCommCtl_t  VsStlProbeCommitTmp;
	if(SET_CUR==req)
	{
		//while(EP0_TST_EMPTY());
		if(!WaitEP0_DATA_Out())
		{
			return;
		}
		FmtIdx=EP0_DATA_OUT();
		FrmIdx= EP0_DATA_OUT();
		if(!checkformatFrameIdx(FmtIdx, FrmIdx,UVC_IS_STILL))// check format idx and frame idx
		{
			VCSTALL_EPCTL(VC_ERR_OUTOFRANGE);
			return;
		}

		VsStlProbeCommitTmp.bFormatIndex=FmtIdx;
		VsStlProbeCommitTmp.bFrameIndex=FrmIdx;
		CmpIdx=EP0_DATA_OUT();

		EP0_FFFLUSH();
		EP0_HSK();

		VsStlProbeCommitTmp.dwMaxVideoFrameSize
		    =GetMaxFrameSize(UVC_IS_STILL,FmtIdx,FrmIdx ,g_bIsHighSpeed);
		VsStlProbeCommitTmp.dwMaxPayloadTransferSize
		    =GetMaxPayloadTransferSize(VsStlProbeCommitTmp.dwMaxVideoFrameSize, 40,  (U8)g_bIsHighSpeed);
		if(CS ==VS_STILL_PROBE_CONTROL)
		{
			memcpy(&g_VsStlProbe,&VsStlProbeCommitTmp,sizeof(VSStlProbCommCtl_t));
		}
		else
		{
			memcpy(&g_VsStlCommit,&VsStlProbeCommitTmp,sizeof(VSStlProbCommCtl_t));
		}
	}
	else //Get Request
	{
		if(CS ==VS_STILL_PROBE_CONTROL)
		{
			VideoStreamStlProbePacket(&g_VsStlProbe);
		}
		else
		{
			VideoStreamStlProbePacket(&g_VsStlCommit);
		}
	}
}

/*
*********************************************************************************************************
*									  Video stream interface request processing
* FUNCTION VideoStreamIfReq
*********************************************************************************************************
*/
/**
  Video stream interface request processing
  \param  None


  \retval None

*********************************************************************************************************
*/
void VideoStreamIfReq()
{
	U8 req;
	req=XBYTE[SETUP_PKT_bREQUEST];
	switch(XBYTE[SETUP_PKT_wVALUE_H])//control selector
	{
	case VS_PROBE_CONTROL:
		{
			switch(req)
			{
			case GET_LEN:
				//	EP0_DATA_IN(g_byVSProbeCommitCtlSize);
				EP0_DATA_IN(CTL_SIZE_VS_PROBE_1V0);
				EP0_FFV_HSK();
				break;
			case GET_INFO:
				EP0_DATA_IN(CTL_INFO_VS_PROBE);
				EP0_FFV_HSK();
				break;
			case GET_DEF:
			case GET_MAX:
			case GET_MIN:
			case GET_RES:

			case GET_CUR:
			case SET_CUR:
				//VideoStreamProbeReq(req);
				VideoStreamReq(req,VS_PROBE_CONTROL);
				break;
			default:
				{
					VCSTALL_EPCTL(VC_ERR_INVDREQ);
					return;
				}
				//break;
			}
			break;
		}
	case VS_COMMIT_CONTROL:
		{
			switch(req)
			{
			case GET_LEN:
				//	EP0_DATA_IN(g_byVSProbeCommitCtlSize);
				EP0_DATA_IN(CTL_SIZE_VS_PROBE_1V0);
				EP0_FFV_HSK();
				break;
			case GET_INFO:
				EP0_DATA_IN(CTL_INFO_VS_COMMIT);
				EP0_FFV_HSK();
				break;
			case GET_CUR:
			case SET_CUR:
				//VideoStreamCommitReq(req);
				VideoStreamReq(req,VS_COMMIT_CONTROL);
				break;
			default:
				{
					VCSTALL_EPCTL(VC_ERR_INVDREQ);
					return;
				}
				//break;
			}
			break;
		}
	case VS_STILL_PROBE_CONTROL:
		{
			switch(req)
			{
			case GET_LEN:
				EP0_DATA_IN(CTL_SIZE_VS_STILL_PROBE);
				EP0_FFV_HSK();
				break;
			case GET_INFO:
				EP0_DATA_IN(CTL_INFO_VS_STILL_PROBE);
				EP0_FFV_HSK();
				break;
			case GET_DEF:
			case GET_MAX:
			case GET_MIN:
			case GET_CUR:
			case SET_CUR:
				//VideoStreamStillProbeReq(req);
				VideoStreamStillReq(req,VS_STILL_PROBE_CONTROL);
				break;
			default:
				{
					VCSTALL_EPCTL(VC_ERR_INVDREQ);
					return;
				}
				//break;
			}
			break;
		}

	case VS_STILL_COMMIT_CONTROL:
		{
			switch(req)
			{
			case GET_LEN:
				{
					EP0_DATA_IN(CTL_SIZE_VS_STILL_COMMIT);
					EP0_FFV_HSK();
					break;
				}
			case GET_INFO:
				{
					EP0_DATA_IN(CTL_INFO_VS_STILL_COMMIT);
					EP0_FFV_HSK();
					break;
				}
			case GET_CUR:
			case SET_CUR:
				{
					//VideoStreamStillCommitReq(req);
					VideoStreamStillReq(req,VS_STILL_COMMIT_CONTROL);
					break;
				}
			default:
				{
					VCSTALL_EPCTL(VC_ERR_INVDREQ);
					return;
				}
				//break;
			}
			break;
		}
	case VS_STILL_IMAGE_TRIGGER_CONTROL:
		{
			switch(req)
			{
			case GET_INFO:
				EP0_DATA_IN(CTL_INFO_VS_STILL_IMAGE_TRIGGER);
				EP0_FFV_HSK();
				break;
			case GET_CUR:
				EP0_DATA_IN(g_bybTrigger);
				EP0_FFV_HSK();
				break;
			case SET_CUR:
				if(!WaitEP0_DATA_Out())
				{
					return ;
				}
				g_bybTrigger=EP0_DATA_OUT();
				if(CTL_TRIGGER_STS_TRNS_STILL== g_bybTrigger)
				{
//#ifdef _USB2_LPM_				
//					XBYTE[USB_LPM] = LPM_SUPPORT| LPM_NAK;//zouxiaozhi 2012-4-11:close LPM during still image
//#endif					
					XBYTE[EPA_IRQEN] = EPA_IRQ_IN_TOKEN;	// Enable EPA in-token interrupt
					XBYTE[EPA_IRQSTAT] = 0xff;

					EP0_HSK();	

					XBYTE[ISP_CONTROL0] |=ISP_FRAME_STOP;
					WaitTimeOut(ISP_CONTROL0, ISP_FRAME_STOP, 0, 100);	// wait ISP busy
					WaitTimeOut(EPA_STAT, EPA_FIFO_EMPTY, 1, 10);	// wait SIE  fifo empty
					WaitTimeOut_Delay(1);
				
#ifdef _MIPI_EXIST_
					XBYTE[MIPI_DPHY_CTRL] &= ~MIPI_DATA_LANE_ALL_EN;
					XBYTE[MIPI_DPHY_PWDB] = MIPI_CLK_DATA_LANE_PWDB;
#else
					XBYTE[CCS_TRANSFER] = CCS_TRANSFER_RESET;
#endif							
					g_byStartVideo = 0;
					g_byISPAEStaticsEn = 0;
#if  ((defined _ENABLE_ANTI_FLICK_ )||(defined _ENABLE_OUTDOOR_DETECT_))
					g_byISPABStaticsEn = 0;
#endif
					g_byISPAWBStaticsEn = 0;
					XBYTE[ISP_CONTROL0] = ISP_STOP;
					XBYTE[ISP_SIE_CTRL] = SIE_STOP; 
					XBYTE[EPA_CTL] |= EPA_FIFO_FLUSH;					
				}
				else if(CTL_TRIGGER_STS_ABORT_STILL==g_bybTrigger)
				{
					EP0_HSK();
				}
				else if(CTL_TRIGGER_STS_NORMAL==g_bybTrigger)
				{
					EP0_HSK();
				}
				else
				{
					VCSTALL_EPCTL(VC_ERR_OUTOFRANGE);
					return;
				}

				break;
			default:
				{
					VCSTALL_EPCTL(VC_ERR_INVDREQ);
					return;
				}
				//break;
			}
			break;
		}
	case VS_STREAM_ERROR_CODE_CONTROL:
		{
			switch(req)
			{
			case GET_INFO:
				EP0_DATA_IN(CTL_INFO_VS_ERROR_CODE);
				EP0_FFV_HSK();
				break;
			case GET_CUR:
				EP0_DATA_IN(g_byVSLastError);
				EP0_FFV_HSK();
				//clear errors
				g_byVSLastError=VS_ERR_NOERROR;
				break;
			default:
				{
					VCSTALL_EPCTL(VC_ERR_INVDREQ);
					return;
				}
				//break;
			}
			break;
		}
		//case VS_GENERATE_KEY_FRAME_CONTROL:
		//case VS_SYNCH_DELAY_CONTROL:
		//case VS_UPDATE_FRAME_SEGMENT_CONTROL:
	default:
		{
			VCSTALL_EPCTL(VC_ERR_INVDCTL);
			break;
		}
	}
	return;
}
/*
*********************************************************************************************************
*									     Video stream  request processing
* FUNCTION VideoStreamingReq
*********************************************************************************************************
*/
/**
    Video stream  request dispatch routine
  \param  None


  \retval None

*********************************************************************************************************
*/
void VideoStreamingReq()
{
	switch(XBYTE[SETUP_PKT_wINDEX_H])
	{
	case 0x00:   //zero,  interface control requests
		VideoStreamIfReq();
		break;
	default:
		{
			VCSTALL_EPCTL(VC_ERR_INVDUNIT);
			break;
		}
	}
	return;

}

#ifdef _UAC_EXIST_
#if 0
void	AutoGainControl() //Rabi 2011-03-10,
{
	U8 bytemp = 0;
	switch(XBYTE[SETUP_PKT_bREQUEST]) //bRequest
	{
		//Get
	case GET_CUR:
		DBG_UAC(("Get AGC cur\n"));
		bytemp = (g_byAudioAttrCurr & UAC_AGC_CTRL)?1:0;
		EP0_DATA_IN(bytemp);
		EP0_FFV();
		break;

		//Set
	case SET_CUR:
		if(!WaitEP0_DATA_Out())
		{
			return ;
		}
		if( EP0_DATA_OUT())
		{
			//AGC On
			g_byAudioAttrCurr |= UAC_AGC_CTRL;
			XBYTE[ALC_CFG] =0xC1;
			XBYTE[ALC_CFG] =0xE1;
			XBYTE[ALC_CFG] =0xC1;
		}
		else
		{
			//AGC Off
			g_byAudioAttrCurr &= (~UAC_AGC_CTRL);
			XBYTE[ALC_CFG] =0x81;
			XBYTE[ALC_CFG] =0xA1;
			XBYTE[ALC_CFG] =0x81;
		}
		EP0_FFFLUSH();
		break;
	default:
		VCSTALL_EPCTL(VC_ERR_INVDREQ);
		return;
	}
	EP0_HSK();
	return;
}
#endif

void AudioLoudnessControl()
{
	U8 bytemp;
	switch(XBYTE[SETUP_PKT_bREQUEST]) //bRequest
	{
		//Get
	case GET_CUR:
		DBG_UAC(("Get boost cur\n"));
		bytemp = (g_byAudioAttrCurr & UAC_LOUDNEDD_CTRL)?1:0 ;
		EP0_DATA_IN(bytemp);
		EP0_FFV();
		break;

		//Set
	case SET_CUR:
		if(!WaitEP0_DATA_Out())
		{
			return ;
		}
		if( EP0_DATA_OUT())
		{
			//Boost On
			g_byAudioAttrCurr |= UAC_LOUDNEDD_CTRL;
			XBYTE[ADF_CTRL0] |= 0xA0;			//boost gain: 20dB

		}
		else
		{
			//Boost Off
			g_byAudioAttrCurr &= (~UAC_LOUDNEDD_CTRL);
			XBYTE[ADF_CTRL0] &= 0x0F;						
		}
		ADF_PARAM_UPDATE();
		EP0_FFFLUSH();
		break;
	default:
		VCSTALL_EPCTL(VC_ERR_INVDREQ);
		return;
	}
	EP0_HSK();
	return;
}
void AudioVolumeControl()
{
//	U8 cn;
//	U8 hb=0,lb=0;
	U8 db=0;
//	U8 bytemp1 = 0;
//	U8 bytemp2 = 0;
	/*	cn = XBYTE[SETUP_PKT_wVALUE_L];
		if(cn == VOLUME_CH_LEFT)
			DBG_UAC(("Left Cn\n"));
		else if (cn==VOLUME_CH_RIGHT)
			DBG_UAC(("Right Cn\n"));
		else
			DBG_UAC(("Unkown Cn 0x%x\n",cn));
	*/
	switch(XBYTE[SETUP_PKT_bREQUEST]) //bRequest
	{
		//Get
	case GET_CUR:
		DBG_UAC(("Get Volume cur\n"));
		EP0_DATA_IN(INT2CHAR(g_wAudioVolumeCurr, 0));
		EP0_DATA_IN(INT2CHAR(g_wAudioVolumeCurr, 1));
		EP0_FFV();
		break;
	case GET_MIN:
		DBG_UAC(("Get Volume min\n"));
		EP0_DATA_IN(VOLUME_MIN%256);
		EP0_DATA_IN(VOLUME_MIN/256);
		EP0_FFV();
		break;
	case GET_MAX:
		DBG_UAC(("Get Volume max\n"));
		EP0_DATA_IN(VOLUME_MAX%256);
		EP0_DATA_IN(VOLUME_MAX/256);
		EP0_FFV();
		break;
	case GET_RES:
		DBG_UAC(("Get Volume res\n"));
		EP0_DATA_IN(VOLUME_RES%256);
		EP0_DATA_IN(VOLUME_RES/256);
		EP0_FFV();
		break;
		//Set
	case SET_CUR:
		if(!WaitEP0_DATA_Out())
		{
			return ;
		}
//			bytemp1 = EP0_DATA_OUT();
//			bytemp2 = EP0_DATA_OUT();
		ASSIGN_U16BYTE(g_wAudioVolumeCurr,0,EP0_DATA_OUT());
		ASSIGN_U16BYTE(g_wAudioVolumeCurr,1,EP0_DATA_OUT());
		db = (U8)(g_wAudioVolumeCurr>>8);
		DBG_UAC(("Set Volume cur: 0x%x\n",db));
		//		2010-09-02 darcy_lu: set min volume to 0 dB for win7 test
		//		if(db>=(VOLUME_MIN>>8)) // -12db~0db
		//		{
		//			db = (0x100-db);
		//			db = (12 -db);
		//			XBYTE[ADF_BOOST_COMP_GAIN] = 0x00;
		//			DBG_UAC(("Boost : 0db\n"));
		//		}
		//		else // 0db ~ 42db
		{
//				db = db/256;
			/*
							if(db<=12)
							{
								db = 12+db;
								XBYTE[ADF_BOOST_COMP_GAIN] = 0x00;
								DBG_UAC(("Boost : 0db\n"));
							}
							else if(db<=22)
							{
								db = 12+(db-10);
								XBYTE[ADF_BOOST_COMP_GAIN] = 0x50;
								DBG_UAC(("Boost : 10db\n"));
							}
							else if(db<=32)
							{
								db = 12+(db-20);
								XBYTE[ADF_BOOST_COMP_GAIN] = 0xA0;
								DBG_UAC(("Boost : 20db\n"));
							}
							else if(db<=42)
							{
								db = 12+(db-30);
								XBYTE[ADF_BOOST_COMP_GAIN] = 0xF0;
								DBG_UAC(("Boost : 30db\n"));
							}
							else if (db==0x80) // -ininifine db
							{
								db = 0;
								XBYTE[ADF_BOOST_COMP_GAIN] = 0x00;
							}
							else
							{
								db = 12-(~db+1);
								XBYTE[ADF_BOOST_COMP_GAIN] = 0x00;
								DBG_UAC(("Boost : 0db\n"));
							}
			*/
			// darcy_lu , 2011-03-16: modify for only support  Dmic-to-UAC
			// loudness disabled, volume is from -12dB to 12dB,  loudness control add 20dB
			if(db<=12)
			{
				db = 12+db;
				DBG_UAC(("Boost : 0db\n"));
			}
			else if (db==0x80) // -ininifine db
			{
				db = 0;
			}
			else
			{
				db = 12-(~db+1);
				DBG_UAC(("Boost : 0db\n"));
			}
		}

		XBYTE[ADF_GAIN_L] = db;
		XBYTE[ADF_GAIN_R] = db;
		DBG_UAC(("Set Volume : %d db\n",db-12));
		ADF_PARAM_UPDATE();
		EP0_FFFLUSH();
		break;
	case SET_MIN:
	case SET_MAX:
	case SET_RES:
	default:
		VCSTALL_EPCTL(VC_ERR_INVDREQ);
		return;
	}
	EP0_HSK();
	return;

}
void AudioMuteControl()
{
	/*	U8 cn;

		cn = XBYTE[SETUP_PKT_wVALUE_L];
		if(cn == VOLUME_CH_LEFT)
			DBG_UAC(("Left Cn\n"));
		else if (cn==VOLUME_CH_RIGHT)
			DBG_UAC(("Right Cn\n"));
		else
			DBG_UAC(("Unkown Cn 0x%x\n",cn));
	*/
	U8 bytemp = 0;
	switch(XBYTE[SETUP_PKT_bREQUEST]) //bRequest
	{
	case GET_CUR:
		DBG_UAC(("Get Mute: Not Mute\n"));
		bytemp = (g_byAudioAttrCurr & UAC_MUTE_CTRL)?1:0 ;
		EP0_DATA_IN(bytemp);
		EP0_FFV();
		break;
	case SET_CUR:
		if(!WaitEP0_DATA_Out())
		{
			return ;
		}
		bytemp =( EP0_DATA_OUT()) &0x01;

		XBYTE[ADF_CTRL1] &= (AD_DMIC_SEL |HIGH_PASS_FILTER_EN | DMIC_LEFT_EDGE_SEL |DMIC_RIGHT_EDGE_SEL) ;
		if( bytemp == 1) //Mute
		{
			DBG_UAC(("Set Mute:TRUE\n"));
			XBYTE[ADF_CTRL1] |= (LEFT_CHANNEL_MUTE | RIGHT_CHANNEL_MUTE );				
			g_byAudioAttrCurr |= UAC_MUTE_CTRL;
		}
		else
		{
			DBG_UAC(("Set Mute: FALSE\n"));
			XBYTE[ADF_CTRL1] &= ~(LEFT_CHANNEL_MUTE | RIGHT_CHANNEL_MUTE );						
			g_byAudioAttrCurr &= (~UAC_MUTE_CTRL);
		}
		ADF_PARAM_UPDATE();
		EP0_FFFLUSH();
		break;
	default:
		VCSTALL_EPCTL(VC_ERR_INVDREQ);
		return;
	}

	EP0_HSK();
	return;
}

void AudioMicrophoneArray()
{
	/*	U8 cn;

		cn = XBYTE[SETUP_PKT_wVALUE_L];
		if(cn == VOLUME_CH_LEFT)
			DBG_UAC(("Left Cn\n"));
		else if (cn==VOLUME_CH_RIGHT)
			DBG_UAC(("Right Cn\n"));
		else
			DBG_UAC(("Unkown Cn 0x%x\n",cn));
	*/
//	U8 bytemp = 0;
	U8*   ucPtr;
	U16 getLen;
	ASSIGN_INT(getLen,XBYTE[SETUP_PKT_wLEN_H],XBYTE[SETUP_PKT_wLEN_L]);

	switch(XBYTE[SETUP_PKT_bREQUEST]) //bRequest
	{
	case GET_MEM:

		DBG_UAC(("Get Mem: \n"));
//			A unique ID that marks the beginning of the microphone array information in memory ( {07FE86C1-8948-4db5-B184-C5162D4AD314} ).

		if (g_wConfigMicArrayCfgAddr)
		{
			if(CopyVdCfgByte2SRam(g_wConfigMicArrayCfgAddr,(U8 *)&XBYTE[SPI_I2C_BUFFER_BASE],  (U8)getLen))
			{

				{
					ucPtr=(U8 *)&XBYTE[SPI_I2C_BUFFER_BASE];
					StrDescProc(0,ucPtr);
				}
			}
			else
			{
				VCSTALL_EPCTL(VC_ERR_INVDREQ);
				return;
			}
		}
		else
		{
			VCSTALL_EPCTL(VC_ERR_INVDREQ);
			return;
		}
		EP0_FFV();
		break;
	default:
		VCSTALL_EPCTL(VC_ERR_INVDREQ);
		return;
	}

	EP0_HSK();
	return;
}


void AudioFUControlReq()
{
	switch(XBYTE[SETUP_PKT_wVALUE_H])
	{
	case VOLUME_CONTROL:
		AudioVolumeControl();
		break;

	case MUTE_CONTROL:
		AudioMuteControl();
		break;

//		case BASS_BOOST_CONTROL:
//			AudioBassBoostControl();
//			break;

	case LOUDNESS_CONTROL:
		AudioLoudnessControl();
		break;

#if 0
	case AUTOMATIC_GAIN_CONTROL:
		if (!g_by5827IC)
		{
			AutoGainControl();
		}
		break;
#endif

	default:
		{
			VCSTALL_EPCTL(VC_ERR_INVDCTL);
			return;
		}
	}
	return;
}
void AudioITUControlReq()
{
	switch(XBYTE[SETUP_PKT_wVALUE_H])
	{
	case TE_CONTROL_UNDEFINED:
		AudioMicrophoneArray();
		break;

	default:
		{
			VCSTALL_EPCTL(VC_ERR_INVDCTL);
			return;
		}
	}
	return;
}
void AudioControlReq()
{
	switch(XBYTE[SETUP_PKT_wINDEX_H])
	{
	case ID_FU:   //  Entity ID, Feature Unit
		AudioFUControlReq();
		break;
	case ID_TERMINAL_IT: //Input Terminal
		DBG_UAC(("ID_TERMINAL_IT\n"));
		if (g_byConfigMicArray == 1)
		{
			AudioITUControlReq();
		}
		else
		{
			EP0_HSK();
		}
		break;
	case ID_TERMINAL_OT:
		DBG_UAC(("ID_TERMINAL_OT\n"));
		EP0_HSK();
		break;
	default:
		{
			VCSTALL_EPCTL(VC_ERR_INVDUNIT);
			return;
		}
	}
	return;
}
void AudioSamplingFreqControl()
{
	U32 dwSamplerate = 0;

	switch(XBYTE[SETUP_PKT_bREQUEST])
	{

	case GET_CUR:
		switch (g_byAudioStrmIFSetting)
		{
		case ALT_SETTING_IDX_22K05_16BIT:
			dwSamplerate = SAMPLING_FREQUENCY_22K05HZ;
			break;
					
		case ALT_SETTING_IDX_32K_16BIT:
			dwSamplerate = SAMPLING_FREQUENCY_32KHZ;
			break;
			
		case ALT_SETTING_IDX_48K_16BIT:
		case ALT_SETTING_IDX_48K_24BIT:
		case ALT_SETTING_IDX_48K_16BIT_MONO:
			dwSamplerate = SAMPLING_FREQUENCY_48KHZ;
			break;
			
		case ALT_SETTING_IDX_96K_16BIT:
		case ALT_SETTING_IDX_96K_24BIT:
			dwSamplerate = SAMPLING_FREQUENCY_96KHZ;
			break;
		case ALT_SETTING_IDX_44K1_24BIT:
			dwSamplerate = SAMPLING_FREQUENCY_44K1HZ;
			break;
		default:
			dwSamplerate = SAMPLING_FREQUENCY_48KHZ;
			break;

		}

		EP0_DATA_IN(dwSamplerate%256);
		EP0_DATA_IN((dwSamplerate/256)%256);
		EP0_DATA_IN((dwSamplerate/256)/256);
		DBG_UAC(("Get sampling freq: 48KHz\n"));
		EP0_FFV();
		break;
	case SET_CUR:
		if(!WaitEP0_DATA_Out())
		{
			return ;
		}
		ASSIGN_U32BYTE(dwSamplerate, 0,EP0_DATA_OUT());
		ASSIGN_U32BYTE(dwSamplerate, 1,EP0_DATA_OUT());
		ASSIGN_U32BYTE(dwSamplerate, 2,EP0_DATA_OUT());
		EP0_FFFLUSH();
		DBG_UAC(("Set sampling freq: 0x%lx\n",dw));
		XBYTE[ADF_CFG] &= (~ADF_SAMPLERATE_MASK);

		switch (dwSamplerate)
		{
		case SAMPLING_FREQUENCY_48KHZ:
			XBYTE[ADF_CFG] |= ADF_48K_CLK;
			break;
			
		case SAMPLING_FREQUENCY_16KHZ:					
			XBYTE[ADF_CFG] |= ADF_16K_CLK;
			break;
			
		case SAMPLING_FREQUENCY_96KHZ:					
			XBYTE[ADF_CFG] |= ADF_96K_CLK;
			break;
			
		case SAMPLING_FREQUENCY_44K1HZ:					
			XBYTE[ADF_CFG] |= ADF_44K1_CLK;
			break;
			
		case SAMPLING_FREQUENCY_32KHZ:					
			XBYTE[ADF_CFG] |= ADF_32K_CLK;
			break;
			
		case SAMPLING_FREQUENCY_22K05HZ:					
			XBYTE[ADF_CFG] |= ADF_22K05_CLK;
			break;
			
		case SAMPLING_FREQUENCY_11K025HZ:					
			XBYTE[ADF_CFG] |= ADF_11K025_CLK;
			break;
					
		default:
			{
				VCSTALL_EPCTL(VC_ERR_INVDUNIT);
				return;
			}
		}
		break;
	case SET_MAX:
	case SET_MIN:
	case SET_RES:
	case GET_MIN:
	case GET_MAX:
	case GET_RES:
	default:
		{
			VCSTALL_EPCTL(VC_ERR_INVDUNIT);
			return;
		}
	}
	EP0_HSK();
	return;
}
/*
void AudioPitchControl()
{

	U8 bytemp = 0;
	switch(XBYTE[SETUP_PKT_bREQUEST])
	{
		case SET_CUR:
			if(!WaitEP0_DATA_Out())
			{
				return ;
			}
			bytemp = EP0_DATA_OUT();
			if (bytemp) //True
			{
				DBG_UAC(("Set Pitch Control: True\n"));
			}
			else
			{
				DBG_UAC(("Set Pitch Control: False\n"));
			}
			EP0_FFFLUSH();
			break;
		case GET_CUR:
			EP0_DATA_IN(TRUE);
			DBG_UAC(("Get Pitch Control: True\n"));
			EP0_FFV();
			break;
		case SET_MAX:
		case SET_MIN:
		case SET_RES:
		case GET_MIN:
		case GET_MAX:
		case GET_RES:
		default:
			{
				VCSTALL_EPCTL(VC_ERR_INVDUNIT);
				return;
			}
	}

	EP0_HSK();
}
*/
void AudioStreamingReq()
{
	switch(XBYTE[SETUP_PKT_wVALUE_H])
	{
	case SAMPLING_FREQ_CONTROL:
		AudioSamplingFreqControl();
		break;

//			case PITCH_CONTROL:
//				AudioPitchControl();
//				break;

	default:
		{
			VCSTALL_EPCTL(VC_ERR_INVDUNIT);
			return;
		}
	}
	return;
}
#endif


/*
*********************************************************************************************************
*									     UVC class  request processing
* FUNCTION VideoClsReqProc
*********************************************************************************************************
*/
/**
     UVC class  request processing
  \param  None


  \retval None

*********************************************************************************************************
*/

void VideoClsReqProc()
{
	switch(XBYTE[SETUP_PKT_bmREQUST_TYPE]&0X83)
	{
	case 0x81:
	case 0x01:
		{
			switch(XBYTE[SETUP_PKT_wINDEX_L])
			{
			case IF_IDX_VIDEOCONTROL:
				VideoControlReq();
				break;
			case IF_IDX_VIDEOSTREAMING:
				VideoStreamingReq();
				break;
#ifdef _UAC_EXIST_
			case IF_IDX_AUDIOCONTROL:
				AudioControlReq();
				break;
#endif
			default:
				{
					STALL_EPCTL();
					break;
				}
			}
			break;
		}
	case 0x82://Get Request to VideoStreaming Endpoint
	case 0x02://Set Request to VideoStreaming Endpoint
#ifdef _UAC_EXIST_
		{
			switch(XBYTE[SETUP_PKT_wINDEX_L])
			{
			case (0x80|EP_IDX_AUDIO_STREAM):
				AudioStreamingReq();
				break;
			default:
				{
					STALL_EPCTL();
					break;
				}
			}
			break;
		}
#endif
	default:
		{
			STALL_EPCTL();
			break;
		}
	}
	return;
}

/*
*********************************************************************************************************
*								Video control status interrupt packet
* FUNCTION VCStatusIntPacket
*********************************************************************************************************
*/
/**
  Fill the status interrupt endpoint fifo and valid the pipe.

  \param
        Originator: the packet originator id
                CS: control selector
         Attribute: event attribute
            pValue: point of data buffer
              size: data size

  \retval None

*********************************************************************************************************
*/
void VCStatusIntPacket_Int0(U8 const Originator, U8 const CS, U8 const Attribute, U8 const * const pValue, U8 const size)
{
	EPC_FFFLUSH();

	EPC_DATA_IN(0X01);
	ASSERT((Originator==0X00)||(Originator==ENT_ID_CAMERA_IT)||(Originator==ENT_ID_PROCESSING_UNIT)
	       ||(Originator==ENT_ID_OUTPUT_TRM)||(Originator==ENT_ID_EXTENSION_UNIT_UTIL));
	EPC_DATA_IN(Originator);
	EPC_DATA_IN(0X00);
	EPC_DATA_IN(CS);
	ASSERT((Attribute==0X00)||(Attribute==0X01)||(Attribute==0X02));
	EPC_DATA_IN(Attribute);
	ASSERT((size==1)||(size==2)||(size==4));
	if(size==1)
	{
		EPC_DATA_IN(pValue[0]);
	}
	else if(size==2)
	{
		EPC_DATA_IN(pValue[1]);
		EPC_DATA_IN(pValue[0]);

	}
	else// size==4
	{
		EPC_DATA_IN(pValue[3]);
		EPC_DATA_IN(pValue[2]);
		EPC_DATA_IN(pValue[1]);
		EPC_DATA_IN(pValue[0]);
	}
	//	Delay(2);
	EPC_FFV();
	WaitTimeOut(EPC_CTL, EPC_FIFO_VALID,0, 15); //wait utill fifo not valid.

}


/*
*********************************************************************************************************
*								Video stream status interrupt packet
* FUNCTION VSStatusIntPacket
*********************************************************************************************************
*/
/**
  Fill the status interrupt endpoint fifo and valid the pipe.

  \param
        Originator: the packet originator id
             Event:event type
             Value:Event value

  \retval None

*********************************************************************************************************
*/
// hemonel 2009-12-29: delete GPIO interrupt
/*
void VSStatusIntPacket(U8 Originator,U8 Evet, U8 Value)
{
	EPC_FFFLUSH();
	EPC_DATA_IN(0X02);
	ASSERT(Originator==IF_IDX_VIDEOSTREAMING);
	EPC_DATA_IN(Originator);
	ASSERT(Evet==VS_STSINT_EVENT_BTNPRESS);

	EPC_DATA_IN(Evet);
	ASSERT((Value==VS_STSINT_VALUE_BTNPRESS)||(Value==VS_STSINT_VALUE_BTNRELEASE));

	EPC_DATA_IN(Value);

	EPC_FFV();

      //wait EP_INT FIFO EMPTY.

	WaitTimeOut(EPC_CTL, EPC_FIFO_VALID, 0, 10);
	EPC_FFFLUSH();


}
*/
