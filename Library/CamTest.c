#include "pc_cam.h"
#include "camutil.h"
#include "camreg.h"
#include "camuvc.h"
#include "camctl.h"
//#include "CamIsp.h"
//#include "CamProcess.h"
//#include "camsensor.h"
//#include "CAMI2C.h"
//#include "camspi.h"
#include "camvdcfg.h"
#include "Global_vars.h"
#include "ISP_vars.h"
//#include "ISP_lib.h"
//#include "CamUac.h"
#include "CamTest.h"
#include "CamSpi.h"

#if (defined (_SLB_TEST_) ||defined (_FT2_REMOVE_TEST))
/*
*********************************************************************************************************
*										Self Loop Back Test
* FUNCTION Self_Loop_Back_Test
*********************************************************************************************************
*/
/**
  Self loop back test for USB.

  \param None

  \retval None
*********************************************************************************************************
*/

void Self_Loop_Back_Test (void)
{

	XBYTE[SLB_BIST] = 0x40;	//Reset Self Loop Back
	XBYTE[SLB_BIST] = 0x00;
	//	If we do the reset procedure, it might be need to re-issue the hardware setting.
	//we decide to disable this procedure.
	//	XBYTE[TST_CTL] = 0x11;	//Enable Phy reset by SIE , and Force PY RESET

	//Load 0x01 to reg (0xF0)
	//PHYRegisterWrite(0x01, 0x00, 0x0F);
	PHYRegisterWrite(0x01, 0xF0);

}
/*
*********************************************************************************************************
*										Self Loop Back Delay
* FUNCTION SLB_DELAY
*********************************************************************************************************
*/
/**
  Self loop back delay to wait for result while proceed self loop testing.

  \param None

  \retval None
*********************************************************************************************************
*/
void SLB_DELAY(U8 time)
{
	U16 i,j;
	for (i=0 ; i < (SLB_HS_DELAY_TIME *time) ; i++)
	{
		j=i;
	}
}

U8  code  g_cSLB_pattern_mode[] =
{
	0x80,///*(All Zeros)*/,
	0x81,///*(Fix Pattern)*/,
	0x82,///*Seed for Psuedo Random pattern*/,
	0x83///*Incremental counter*/
};


U8 SLB_TEST(U8 Speed)
{

	U8 bySLBWaitCnt;
	U16 SLB_COUNTER;
	U16 SLB_FailTh;
	U16 SLB_FAIL_CNT=0;
	U16 i;
	U8 j;

//	DBG(("\n1_%bd\t",g_bySLB_FSTstCnt));
//	DBG(("2_%lu\t",g_dwSLB_HSTstCnt));
//	DBG(("3_%d\t",g_wSLB_HSFailTh));
//	DBG(("4_%bx\t",g_bySLB_FixPattern));
//	DBG(("5_%bd\t",g_bySLB_Seed));
//	DBG(("6_%bd\t",g_bySLB_FSFailTh));
//	DBG(("7_%d\n",g_wSLB_HSDlyTime));

	if(Speed == SLB_HS)
	{
		DBG(("SLB_HS_Test:\n"));
		bySLBWaitCnt= SLB_WAIT_HS;
		XBYTE[UTMI_TST] = 0x82;
		//	JF_040410: Reduce time of doing SLB
		SLB_COUNTER = SLB_HS_TEST_COUNT;;
		//SLB_COUNTER = 0x2000;
		SLB_FailTh= SLB_HS_FAIL_THRESHOLD;
	}
	else //SLB_FS
	{
		DBG(("SLB_FS_Test:\n"));
		bySLBWaitCnt= SLB_WAIT_FS;
		XBYTE[UTMI_TST] = 0x42;
		SLB_COUNTER = SLB_FS_TEST_COUNT;
		SLB_FailTh= SLB_FS_FAIL_THRESHOLD;
	}

	for (i=0; i < SLB_COUNTER; i++)
	{
		for(j=0; j<sizeof(g_cSLB_pattern_mode); j++)
		{
			Self_Loop_Back_Test();
			if( g_cSLB_pattern_mode[j]==0x81)//fix pattern
			{
				XBYTE[SLB_SEED] = SLB_FIX_PATTERN;
			}
			else  //seed and others
			{
				XBYTE[SLB_SEED] = SLB_SEED_PATTERN;
			}
			XBYTE[SLB_BIST] = g_cSLB_pattern_mode[j];			//Enable Self Loop Back
			SLB_DELAY(bySLBWaitCnt);					//delay for waiting RESULT
			if ((XBYTE[SLB_BIST] & 0x20) || (!(XBYTE[SLB_BIST] & 0x10)))	//fail
			{
				SLB_FAIL_CNT++;
			}

		}
	}
	DBG(( "SLB_FAIL_CNT=%d\n",SLB_FAIL_CNT));
	DBG(( "SLB_FAIL_THD=%d\n",SLB_FailTh));

	if (SLB_FAIL_CNT < SLB_FailTh)
	{
		DBG(("Test PASS\n"));
//		XBYTE[UTMI_TST] = 0x00;
		XBYTE[SLB_SEED] = 0x00;
		XBYTE[SLB_BIST] = 0x00;
		PHYRegisterWrite(0x7C, 0xF0);
		return TRUE;
	}
	DBG(("Test FAIL\n"));
	return FALSE;
}


#else

#ifdef	_ENABLE_OLT_
/*
*********************************************************************************************************
*										Self Loop Back Test
* FUNCTION Self_Loop_Back_Test
*********************************************************************************************************
*/
/**
  Self loop back test for USB.

  \param None

  \retval None
*********************************************************************************************************
*/

void Self_Loop_Back_Test (void)
{

	XBYTE[SLB_BIST] = 0x40;	//Reset Self Loop Back
	XBYTE[SLB_BIST] = 0x00;
	//	If we do the reset procedure, it might be need to re-issue the hardware setting.
	//we decide to disable this procedure.
	//	XBYTE[TST_CTL] = 0x11;	//Enable Phy reset by SIE , and Force PY RESET

	//Load 0x01 to reg (0xF0)
	//PHYRegisterWrite(0x01, 0x00, 0x0F);
	PHYRegisterWrite(0x01, 0xF0);

}
/*
*********************************************************************************************************
*										Self Loop Back Delay
* FUNCTION SLB_DELAY
*********************************************************************************************************
*/
/**
  Self loop back delay to wait for result while proceed self loop testing.

  \param None

  \retval None
*********************************************************************************************************
*/
void SLB_DELAY(U8 time)
{
	U16 i,j;
	for (i=0 ; i < time ; i++)
	{
		j=i;
	}
}

U8  code  g_cSLB_pattern_mode[] =
{
	0x80,///*(All Zeros)*/,
	0x81,///*(Fix Pattern)*/,
	0x82,///*Seed for Psuedo Random pattern*/,
	0x83///*Incremental counter*/
};


U8 SLB_TEST(U8 Speed)
{

	U8 bySLBWaitCnt;
	U16 SLB_COUNTER;
	U16 SLB_FailTh;
	U16 SLB_FAIL_CNT=0;
	U16 i;
	U8 j;
	Speed =Speed;

//	DBG(("\n1_%bd\t",g_bySLB_FSTstCnt));
//	DBG(("2_%lu\t",g_dwSLB_HSTstCnt));
//	DBG(("3_%d\t",g_wSLB_HSFailTh));
//	DBG(("4_%bx\t",g_bySLB_FixPattern));
//	DBG(("5_%bd\t",g_bySLB_Seed));
//	DBG(("6_%bd\t",g_bySLB_FSFailTh));
//	DBG(("7_%d\n",g_wSLB_HSDlyTime));


	DBG(("SLB_HS_Test:\n"));
	bySLBWaitCnt= SLB_WAIT_HS;
	XBYTE[UTMI_TST] = 0x82;
	//	JF_040410: Reduce time of doing SLB
	SLB_COUNTER = 0x200;
	//SLB_COUNTER = 0x2000;
	SLB_FailTh= 0x05;


	for (i=0; i < SLB_COUNTER; i++)
	{
		for(j=0; j<sizeof(g_cSLB_pattern_mode); j++)
		{
			Self_Loop_Back_Test();
			if( g_cSLB_pattern_mode[j]==0x81)//fix pattern
			{
				XBYTE[SLB_SEED] = 0xff;
			}
			else  //seed and others
			{
				XBYTE[SLB_SEED] = 0x01;
			}
			XBYTE[SLB_BIST] = g_cSLB_pattern_mode[j];			//Enable Self Loop Back
			SLB_DELAY(bySLBWaitCnt);					//delay for waiting RESULT
			if ((XBYTE[SLB_BIST] & 0x20) || (!(XBYTE[SLB_BIST] & 0x10)))	//fail
			{
				SLB_FAIL_CNT++;
			}

		}
	}
	DBG(( "SLB_FAIL_CNT=%d\n",SLB_FAIL_CNT));
	DBG(( "SLB_FAIL_THD=%d\n",SLB_FailTh));

	if (SLB_FAIL_CNT < SLB_FailTh)
	{
		DBG(("Test PASS\n"));
		return TRUE;
	}
	DBG(("Test FAIL\n"));
	return FALSE;
}

void OLT_main(void)
{
	U8 i;
	//DBG(("Into OLT main ...... \n "));

	// close interrupt 0
	IE = 0x0;
	//XBYTE[USBCTL] = USB_CONNECT;// |  USB_CHIRP_2MS;

	while(1)
	{
		//USB SLB
		SLB_TEST(0);

		// ISP
		ISP_Clock_Switch(SWITCH_ON);
		//open mipi clock anyway
		XBYTE[CLKCTL] |= ISP_CLK_CHNG;	
		XBYTE[BLK_CLK_EN] |= MIPI_CLK_EN; 
		XBYTE[CLKCTL] &=  (~ISP_CLK_CHNG);	
	//PIXDIN[9:0] and HSYNC select from different Package
#ifdef _PACKAGE_QFN40_
		XBYTE[PIN_SHARE_SEL] = PACKAGE_SEL_QFN40;
#else		//_PACKAGE_QFN32_
		XBYTE[PIN_SHARE_SEL] = PACKAGE_SEL_QFN32;
#endif		
#ifdef _ENABLE_MJPEG_
		JPEG_Clock_Switch(SWITCH_ON);

		XBYTE[ISP_BUS_WIDTH] = ISP_BUS_WiDTH16_5827;
		XBYTE[ISP_IMAGE_SEL] = MJPEG_FORMAT;//JPEG_FORMAT_16BIT;// jpeg format.
#endif
#ifdef _ENABLE_MJPEG_
		XBYTE[JENC_START]= JPEG_VIDEO_SEL|JPEG_SET;
#endif //_RTS5811_CODE_

		StartMipiAphy(); 
		XBYTE[MIPI_DPHY_PWDB] = MIPI_DATA_LANE0_EN|MIPI_DATA_LANE1_EN;
		XBYTE[MIPI_DPHY_CTRL] |= MIPI_PIX_DIN_SEL|MIPI_BIT_SWITCH|MIPI_DATA_LANE0_EN|MIPI_DATA_LANE1_EN;
		XBYTE[ISP_CONTROL0] = ISP_TRANSFER_START ;
		XBYTE[CCS_TRANSFER] = CCS_TRANSFER_EN;
		// Open SVA/SVIO
		POWER_ON_SV28();
		POWER_ON_SV18();
		// all gpio driving high
		SetGPIODirW(0xFFFF);// All gpio ouput
		XBYTE[GPIO_H] = 0xFF;
		XBYTE[GPIO_L] |= 0x7F;	//except gpoi7
		XBYTE[PG_GPIO_AL_CTRL0] = GPIO_AL1_OUTPUT|GPIO_AL0_OUTPUT|GPIO_AL0_DRIVING_HIGH|GPIO_AL1_DRIVING_HIGH;	
		
		Delay(500);
		//stop image processing
		StopMipiAphy();
		XBYTE[MIPI_DPHY_PWDB] = MIPI_CLK_DATA_LANE_PWDB;
		XBYTE[MIPI_DPHY_CTRL] &= ~MIPI_DATA_LANE_ALL_EN;
		XBYTE[CCS_TRANSFER] = CCS_TRANSFER_RESET;
		XBYTE[ISP_CONTROL0] = ISP_STOP;

#ifdef _ENABLE_MJPEG_
		XBYTE[JENC_START] = JPEG_CLEAR;
		XBYTE[JENC_START] = JPEG_MODULE_RST;
		JPEG_Clock_Switch(SWITCH_OFF);
#endif//_RTS5811_CODE_
		ISP_Clock_Switch(SWITCH_OFF);
		//close mipi clock anyway
		XBYTE[CLKCTL] |= ISP_CLK_CHNG;	
		XBYTE[BLK_CLK_EN] &= ~MIPI_CLK_EN; 
		XBYTE[CLKCTL] &=  (~ISP_CLK_CHNG);	
		
		GPIO_L_DRIVE_HIGH(SPI_WP_GPIO); 	// disable SPI write protect
		SET_GPIO_L_OUTPUT(SPI_WP_GPIO);
		SPI_WB_WriteStatus(0x00);	// disable serial flash block protect

		// sector erase
		SPI_WB_EraceSFSector(PP_WB_ADDR_BASE);	// sector erase
#if 0
		// SPI
		ChangeSPIClockSink(SPI_CLK_SINK_SF);
		START_SPI_CRC_CLK();
		SPI_AUTO_START();
		for( i= 0; i<200; i++)
		{
			XBYTE[SPI_XCHGM_ADDR0]=CONSTSHORT2BYTE(SPI_BUFFER_BASE_ADDR,0);
			XBYTE[SPI_CA_NUMBER] = SPI_COMMAND_BIT_8 | SPI_ADDRESS_BIT_24;

			// SPI read
			XBYTE[SPI_COMMAND] = SF_READ;
			XBYTE[SPI_ADDR2] = 0x00 ;
			XBYTE[SPI_ADDR1] = 0x10+i;
			XBYTE[SPI_ADDR0] = 0x55+i;
			XBYTE[SPI_LENGTH1] = 0;
			XBYTE[SPI_LENGTH0] = 16;
			XBYTE[SPI_TRANSFER] = SPI_TRANSFER_START | SPI_CADI_MODE0;
			uDelay(2);
			SPI_RESET_MODULE();
		}
		SPI_AUTO_END();
		STOP_SPI_CRC_CLK();
#endif
		// I2C
		START_I2C_CLK();
		for(i =0; i< 200; i++)
		{
			// I2C set address
			XBYTE[I2C_ADDR0] = 0x00 | I2C_WRITE_OP;
			XBYTE[I2C_ADDR1] = 0x55+i;
			XBYTE[I2C_ADDR_LEN] = 0x02;
			XBYTE[I2C_TRANSFER] = I2C_TRANSFER_START | I2C_SET_ADDRESS_MODE;
			uDelay(2);
			XBYTE[I2C_TRANSFER] = I2C_TRANSFER_STOP;

			// I2C Read
			XBYTE[I2C_ADDR0] = 0x00 | I2C_READ_OP;
			XBYTE[I2C_ADDR_LEN] = 0x01;
			XBYTE[I2C_DATA_LEN] = 0xAA+i;
			XBYTE[I2C_TRANSFER] = I2C_TRANSFER_START | I2C_READ_MODE;
			uDelay(2);
			XBYTE[I2C_TRANSFER] = I2C_TRANSFER_STOP;

			// I2C write
			XBYTE[I2C_ADDR0] = 0x00 | I2C_WRITE_OP;
			XBYTE[I2C_ADDR1] = 0x00+i;
			XBYTE[I2C_ADDR_LEN] = 0x02;
			XBYTE[I2C_SCRATCH_BUFFER_BASE_ADDR] = 0xFF+i;
			XBYTE[I2C_DATA_LEN] = 0x01;
			XBYTE[I2C_TRANSFER] = I2C_TRANSFER_START | I2C_WRITE_MODE;
			uDelay(2);
			XBYTE[I2C_TRANSFER] = I2C_TRANSFER_STOP;
		}
		STOP_I2C_CLK();

		//Power Off SVA/SVIO
		POWER_OFF_SV28();
		POWER_OFF_SV18();

		// all gpio driving high
		SetGPIODirW(0xFFFF);// All gpio ouput
		XBYTE[GPIO_H] = 0x00;
		XBYTE[GPIO_L] &= ~0x7F;	//except gpoi7
		XBYTE[PG_GPIO_AL_CTRL0] = GPIO_AL1_OUTPUT|GPIO_AL0_OUTPUT;	
		
		// LED toggle
		SHOW_OLT_WORK();
		DBG(("O.L.T.\n"));
	}
}
#endif
#endif

#if (defined (_SLB_TEST_))
void SLB_TEST_main(void )
{
	DBG(("Into SLB_TEST_main \n"));

	SetGPIODirB(7, 1);		//GPIO7
	SHOW_SLB_FAIL();

	//HIGH speed test
	if((g_bySLBTestMode&0x03)==0x03) //Full speed + High speed test.
	{
		DBG(("SLB_HS+FS test\n "));
		if(SLB_TEST(SLB_FS)&&SLB_TEST(SLB_HS))//
		{
			SHOW_SLB_PASS();
			DBG(("SLB_HS+FS pass\n"));
		}
		else
		{
			DBG(("SLB_HS+FS fail\n"));
		}

	}
	else //high speed test only.
	{
		if(SLB_TEST(SLB_HS))//
		{
			SHOW_SLB_PASS();
			DBG(("SLB_HS pass\n"));
		}
		else
		{
			DBG(("SLB_HS fail\n"));
		}
	}
	DBG(("SBL TEST finish\n"));
	while(1);
}

#endif

#if (defined (_FT2_REMOVE_TEST))

U8 Mem_RW_Test(void)
{
	U16 DATA_RAM_ADDR  = 0x0000;
	U16 SPI_BUFFER_ADDR = 0x0000;
	U8 data_ram_value;
	U8 spi_buffer_value;

	//5801B, unused data ram is 0xD762~0xD7FF
	//5801C, unused data ram is 0xD774~0xD7FF
	//for 5801B & 5801C unused SPI buffer is 0xDE48~DEFF
	DATA_RAM_ADDR = 0xDDA0;
	SPI_BUFFER_ADDR= 0xDE4A;

	//radam data
	data_ram_value = XBYTE[DATA_RAM_ADDR];
	spi_buffer_value = XBYTE[SPI_BUFFER_ADDR];

	XBYTE[DATA_RAM_ADDR] = data_ram_value;
	XBYTE[SPI_BUFFER_ADDR] = spi_buffer_value;
	if ((data_ram_value != XBYTE[DATA_RAM_ADDR]) || (spi_buffer_value != XBYTE[SPI_BUFFER_ADDR]))
	{
		return FALSE;
	}

	//data 0x55
	XBYTE[DATA_RAM_ADDR] = 0x55;
	XBYTE[SPI_BUFFER_ADDR] = 0x55;
	if ((0x55 != XBYTE[DATA_RAM_ADDR]) || (0x55 != XBYTE[SPI_BUFFER_ADDR]))
	{
		return FALSE;
	}

	//data 0x00
	XBYTE[DATA_RAM_ADDR] = 0x00;
	XBYTE[SPI_BUFFER_ADDR] = 0x00;
	if ((0x00 != XBYTE[DATA_RAM_ADDR]) || (0x00 != XBYTE[SPI_BUFFER_ADDR]))
	{
		return FALSE;
	}
	//data 0xAA
	XBYTE[DATA_RAM_ADDR] = 0xAA;
	XBYTE[SPI_BUFFER_ADDR] = 0xAA;
	if ((0xAA != XBYTE[DATA_RAM_ADDR]) || (0xAA != XBYTE[SPI_BUFFER_ADDR]))
	{
		return FALSE;
	}

	//data 0xAA
	XBYTE[DATA_RAM_ADDR] = 0xFF;
	XBYTE[SPI_BUFFER_ADDR] = 0xFF;
	if ((0xFF != XBYTE[DATA_RAM_ADDR]) || (0xFF != XBYTE[SPI_BUFFER_ADDR]))
	{
		return FALSE;
	}

	return TRUE;
}
/*
*********************************************************************************************************
						   get Gpio pin value
* FUNCTION GetGPIOValueB
*********************************************************************************************************
*/
/**
  get GPIO[15:0] pin value
  \param
       io: GPIO index

  \retval value of GPIO[io]
*********************************************************************************************************
*/
// hemonel 2010-04-13: delete dummy code
bit  GetGPIOValueB(U8 io) // test if GPIO 1 or 0
{
	U16 wTmp1;
	ASSERT(io<=1);
	wTmp1=XBYTE[PG_GPIO_AL_CTRL0];		//for GPIO_AL0 & GPIO_AL1 input value, FF9C[5:4]
	if (io == 0)
	{
		return ((wTmp1>>4)&(0x01));
	}
	else
	{
		return ((wTmp1>>5)&(0x01));
	}
}

bit	WaitBtnDetectDebounce(U8  gpio)
{
	U8 idata i=0,j=0;

RE_DETECT_IO:
	//WaitTimeOut(0,0,0,1);
	uDelay(1);
	if (GetGPIOValueB(gpio))   // test GPIO AL0 GPIO == 1
	{
		j=0;
		i++;
		if (i < VALID_BTN_PULS)
		{
			goto RE_DETECT_IO;
		}
	}
	else //GPIO==0
	{
		i=0;
		j++;
		if (j < VALID_BTN_PULS)
		{
			goto RE_DETECT_IO;
		}
	}
	if(i)	// button released
	{
		return 1;
	}
	else	// button pressed
	{
		return 0;
	}
}



void InitFTMode(void)
{
	U8 i = 0;
	U8 temp = 0;
	//set GPIO9 direction:output
	SetGPIODirB(7, 1);
	GPIO_L_DRIVE_LOW(7);
	//SHOW_SLB_FAIL();

	//set GPIOAL0 direction:input
	XBYTE[PG_GPIO_AL_CTRL0] &= (~GPIO_AL0_OUTPUT);
	//GPIO7_POWER_OFF();

	//polling GPIOAL0, if GPIOAL0 is HIGH, set start signal on GPIO9
	while(1)
	{
		if (WaitBtnDetectDebounce(0) == 1)
		{
			break;
		}
	}
	//start signal
	for (i=0; i<4; i++)
	{
		temp = 0x01 & (FT2_REMOVE_VERSION>>i);
		if (temp == 0x01)
		{
			GPIO_L_DRIVE_HIGH(7);
		}
		else
		{
			GPIO_L_DRIVE_LOW(7);
		}
		Delay(2);
	}
	GPIO_L_DRIVE_HIGH(7);
//	GPIO_H_DRIVE_HIGH(1);
//	uDelay(1);
//	GPIO_H_DRIVE_LOW(1);
//	uDelay(1);
//	GPIO_H_DRIVE_HIGH(1);
//	uDelay(1);
//	GPIO_H_DRIVE_LOW(1);
//	uDelay(1);
//	GPIO_H_DRIVE_HIGH(1);

	//polling GPIO_AL0, if GPIO_AL0 is LOW, GPIO7 to Low,
	while(1)
	{
		if (WaitBtnDetectDebounce(0) == 0)
		{
			break;
		}
	}
	GPIO_L_DRIVE_LOW(7);


}


void FT_TEST(void)
{
	U8 byTmp=0;
	DBG(("Into SLB_TEST_main \n"));



	DBG(("SLB_HS+FS test\n "));
//	if(/*SLB_TEST(SLB_FS)&&*/SLB_TEST(SLB_HS))
//	if ((SLB_TEST(SLB_HS) == TRUE) && (AutoK_Test() == TRUE) && (Mem_RW_Test() == TRUE))
//    autok test is moved to linux AP
	if ((SLB_TEST(SLB_HS) == TRUE) && (Mem_RW_Test() == TRUE))
	{
		//SLB test ,autok test, memory rw test ok
		DBG(("SLB_HS+FS pass\n"));
//		SLB_TEST(SLB_HS);
		//power on SV18&SV28
		//set SV18 to 1.8V
#if (_CHIP_ID_ & _RTS5840_)
	XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
	XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V79;
#elif (_CHIP_ID_ & _RTS5832_)
	XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
	XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V85;
#elif (_CHIP_ID_ & _RTS5829B_)
	XBYTE[REG_TUNESVA] =  SVA_VOL_2V82;
	XBYTE[REG_TUNESVIO] =  SVIO_VOL_1V80;
#endif
		POWER_ON_SV18();
		POWER_ON_SV28();


		PHYRegisterWrite(0x1B, 0xE6);	// disable autoK
//			PHYRegisterWrite(0x52, 0xE2); // change HSTX current
		XBYTE[UTMI_TST] = 0x82;  //darcy_lu: 2010-04-23  set [force high speed + force OP Mode normal]  for Z0 test
		XBYTE[USBTEST] = 0x01; // Test_SE0_NAK

		GPIO_L_DRIVE_HIGH(7);

		//polling GPIO7, if GPIO7 is from HIGH to LOW, set GPIO9 to Low, T8
		while(1)
		{
			if (WaitBtnDetectDebounce(0) == 1)
			{
				break;
			}
		}
		while(1)
		{
			if (WaitBtnDetectDebounce(0) == 0)
			{
				break;
			}
		}
		GPIO_L_DRIVE_LOW(7);

		//polling GPIO_AL0, if GPIO_AL0 is HiGH, clear SE0_NAK, close SV18&SV28 , set GPIO7 to HIGH
		while(1)
		{
			if (WaitBtnDetectDebounce(0) == 1)
			{
				break;
			}
		}
		XBYTE[USBTEST] = 0x00; // clear SE0_NAK
		POWER_OFF_SV18();		//power off sv18
		POWER_OFF_SV28();		//power off sv28

		//set GPIO7 to HIGH
		GPIO_L_DRIVE_HIGH(7);

		//polling GPIO_AL0, if GPIO_AL0 is LOW, GPIO7 to Low
		while(1)
		{
			if (WaitBtnDetectDebounce(0) == 0)
			{
				break;
			}
		}
		//set GPIO7 to LOW
		GPIO_L_DRIVE_LOW(7);
	}
	else
	{
		while(1);
		DBG(("SLB_HS+FS fail\n"));
	}
}

#endif