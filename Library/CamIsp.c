#include "pc_cam.h"
#include "camutil.h"
#include "CamReg.h"
#include "CamUvc.h"
#include "CamCtl.h"
#include "CamIsp.h"
//#include "CamProcess.h"
#include "CamSensor.h"
#include "Global_vars.h"
#include "ISP_vars.h"
#include "ISP_lib.h"
#include "CamVdCfg.h"
//#include "CamSPI.h"
#include "CamIspPro.h"
#if (defined _UVC_PPWB_ || defined _UVC_COMMITWB_ || defined _IQ_TABLE_CALIBRATION_)
#include "Camspi.h"
#endif
#include "camvdcmd.h"
#ifdef _RTK_EXTENDED_CTL_
#include "CamRtkExtIsp.h"
#include "CamUvcRtkExtCtl.h"
#endif

#ifdef _USE_BK_HSBC_ADJ_
void	SetBkHueRT (S16 swSetValue)
{
	S8 sbySinX, sbyCosX;
	S8 sbySinY, sbyCosY;
	float fAngleX,fAngleY;

	//heonel 2010-06-22 follow Dell V0.11
	//swSetValue /=100;

	// change to 0~360
	if(swSetValue<0)
	{
		swSetValue += 360;
	}

	// mapping to angle X and angle Y
	if(swSetValue<=120)
	{
		fAngleX = swSetValue*9/8;
		fAngleY = 360-swSetValue*3/4;
	}
	else if(swSetValue >= 240)
	{
		fAngleX = 270+(swSetValue-240)*3/4;
		fAngleY = (360-swSetValue)*9/8;
	}
	else
	{
		fAngleX = 135 +(swSetValue - 120)*9/8;
		fAngleY = 270 - (swSetValue - 120)*9/8;
	}

	// transform to radian
	fAngleX = ((float)(fAngleX)/(float)180)*3.1415926;
	fAngleY = ((float)(fAngleY)/(float)180)*3.1415926;

	// calculate sin and cos
	sbySinX = sin(fAngleX)*64;
	sbyCosX = cos(fAngleX)*64;
	sbySinY = sin(fAngleY)*64;
	sbyCosY = cos(fAngleY)*64;

	// write the sin and cos value to register
	XBYTE[ISP_HUE_SIN_X] = (U8)sbySinX;
	XBYTE[ISP_HUE_COS_X] = (U8)sbyCosX;
	XBYTE[ISP_HUE_SIN_Y] = (U8)sbySinY;
	XBYTE[ISP_HUE_COS_Y] = (U8)sbyCosY;
}

void SetBkSharpness (U8 bySetValue)
{
	U8 bysharpness;
	U8 byTemp;

#ifdef _LENOVO_JAPAN_PROPERTY_PAGE_
	U8 i;
	bysharpness = MappingSharpness(bySetValue);
#else
	//heonel 2010-06-22 follow Dell V0.11
	if(bySetValue >= SharpnessItem.Def)
	{
		bysharpness  = LinearIntp_Byte(SharpnessItem.Def, g_bySharpness_Def, SharpnessItem.Max, 255, bySetValue);
	}
	else
	{
		bysharpness  = LinearIntp_Byte(SharpnessItem.Def, g_bySharpness_Def, SharpnessItem.Min, 0, bySetValue);
	}	
#endif

	// hemonel 2011-03-14: sharpness parameter changed with sharpness
	byTemp = LinearIntp_Byte_Bound(g_bySharpness_Def, (g_bySharp_VIIR_Coef>>4), 255, 0, bysharpness);
	byTemp = (byTemp<<4) | (g_bySharp_VIIR_Coef&0x0F);
	XBYTE[ISP_RGB_VIIR_COEF] = byTemp;
	byTemp = LinearIntp_Byte_Bound(g_bySharpness_Def, ( g_bySharp_HIIR_Coef &0x0F), 255, 4, bysharpness);
	XBYTE[ISP_RGB_HLPF_COEF] = byTemp|(byTemp<<4);

	byTemp = LinearIntp_Byte_Bound(g_bySharpness_Def, g_bySharp_EdgeDct_Thd1, 255, 2, bysharpness);
	XBYTE[ISP_EDG_DCT_THD1] = byTemp;
#ifdef _LENOVO_JAPAN_PROPERTY_PAGE_
	for (i=0;i<12;i++)
	{
		if (bysharpness>g_bySharpness_Def)
		{
			XBYTE[ISP_EEH_SHARP_ARRAY0+i] = LinearIntp_Byte_Bound(g_bySharpness_Def, ct_IQ_Table.edgeenhance.byEdgeEhance[17+i], 255, 63, bysharpness);
		}	
		else
		{
			XBYTE[ISP_EEH_SHARP_ARRAY0+i] = LinearIntp_Byte_Bound(0, 0, g_bySharpness_Def, ct_IQ_Table.edgeenhance.byEdgeEhance[17+i], bysharpness);
		}	
	}		
#endif

	XBYTE[ISP_SHARPNESS]=bysharpness;
	XBYTE[ISP_CONTROL1] |= ISP_EDGEENHANCE_EN;
}
#endif


#ifdef _USE_BK_SPECIAL_EFFECT_
void SetBKSpecailEffect(U8 const byEffect)
{
	U8 byspecialeffect2 = 0x00;
	U8 data byUvalue = 0x80, byVvalue = 0x80;

	XBYTE[ISP_CONTROL2] &= ~ISP_SPECIAL_EFFECT_MASK;

	switch (byEffect)
	{
	case SNR_EFFECT_NEGATIVE:
		byspecialeffect2 = ISP_NEGATIVE_EN;
		break;
	case SNR_EFFECT_MONOCHROME:
		byspecialeffect2 = ISP_MONO_EN;
		break;
	case SNR_EFFECT_GRAY:
		byspecialeffect2 =ISP_GRAY_EN;
		break;
	case SNR_EFFECT_EDGEDRAW:
		byspecialeffect2 = ISP_EDGEDRAW_EN;
		break;
	case SNR_EFFECT_SEPIA:
		byspecialeffect2 = ISP_SPECIAL_EN;
		byUvalue = 0x40;
		byVvalue = 0xa0;
		break;
	case SNR_EFFECT_GREENISH:
		byspecialeffect2 = ISP_SPECIAL_EN;
		byUvalue = 0x60;
		byVvalue = 0x60;
		break;
	case SNR_EFFECT_REDDISH:
		byspecialeffect2 = ISP_SPECIAL_EN;
		byUvalue = 0x80;
		byVvalue = 0xc0;
		break;
	case SNR_EFFECT_BLUISH:
		byspecialeffect2 = ISP_SPECIAL_EN;
		byUvalue = 0xa0;
		byVvalue = 0x40;
		break;
	default: //no effect
		break;
	}

	XBYTE[ISP_SPECIAL_TUNE] =0x20;
	XBYTE[ISP_SPECIAL_TARGET_U] = byUvalue;
	XBYTE[ISP_SPECIAL_TARGET_V] = byVvalue;

	XBYTE[ISP_CONTROL2] |=byspecialeffect2;
}
#endif

#ifdef _BLACK_SCREEN_
void BlackPatternSwitchPro(U16 wGain, U16 wExposuretime, U8 byYavg)
{
	if((wGain>g_wBlackPatternGain_TH)&&(wExposuretime>g_wBlackPatternExposuretime_TH)&&(byYavg< g_byBlackPatternYavg_TH))
	{
		g_byIsBlack = 1;
	}
	else
	{
		g_byIsBlack = 0;
	}

	ISP_MSG(("black=%bd\n",g_byIsBlack));
//	SetBackendIspCCM(GainItem.Last);//(g_wGainLast);// hemonel 2009-11-26: use new control struct
	// hemonel 2009-12-10: replace ccm with gain because RTS5803 no CCM

	SetGainParam(GainItem.Last);

}

void IspSwitchPro(void)
{
	U16 wSensorGain, wSensorExposuretime;
	U8 bySensorYavg;

	ISP_MSG(("\n IspSwitchPro()\n"));

	if(g_byIspCtl & (BLACK_PATTERN_ENABLE))
	{
		wSensorGain = GetSensorAEGain();
		wSensorExposuretime =GetSensorExposureTime();
		bySensorYavg = GetSensorYavg();
		ISP_MSG(("Gain=%d,Exp=%x,Yavg=0x%bx\n",wSensorGain,wSensorExposuretime,bySensorYavg));
	}

	if(g_byIspCtl & (BLACK_PATTERN_ENABLE))
	{
		BlackPatternSwitchPro(wSensorGain, wSensorExposuretime, bySensorYavg);
	}
}
#endif

void ExposureTimeSet(U32 dwSetValue)
{
#ifdef _UVC_PPWB_
	if (g_VsPropCommit.dwExposureTimeAbsolut != dwSetValue)
	{
		g_wCameraTerminalChange_Flag |= CT_CTL_SEL_EXPOSURE_TIME_ABS ;
	}
	else
	{
		g_wCameraTerminalChange_Flag &= ~CT_CTL_SEL_EXPOSURE_TIME_ABS ;
	}
#endif
	// hemonel 2009-01-21: UVC test bug: not access sensor register if not transfer image.
	if(g_bySensorIsOpen == SENSOR_OPEN)
	{
		SetSensorIntegrationTime(dwSetValue);
	}
}

void ExposureTimeAutoSet(U8 bySetValue)
{
#ifdef _UVC_PPWB_
	if (g_VsPropCommit.byExposureTimeAuto != bySetValue)
	{
		g_wCameraTerminalChange_Flag |= CT_CTL_SEL_AUTO_EXP_MODE ;
	}
	else
	{
		g_wCameraTerminalChange_Flag &= ~CT_CTL_SEL_AUTO_EXP_MODE ;
	}
#endif

	if(g_bySensorIsOpen == SENSOR_OPEN)
	{
		SetSensorIntegrationTimeAuto(bySetValue);
	}
}

void LowLightCompSet(U8 bySetValue)
{
#ifdef _UVC_PPWB_
	if (g_VsPropCommit.byLowLightComp != bySetValue)
	{
		g_wCameraTerminalChange_Flag |= CT_CTL_SEL_AUTO_EXP_PRY ;
	}
	else
	{
		g_wCameraTerminalChange_Flag &= ~CT_CTL_SEL_AUTO_EXP_PRY ;
	}
#endif

	if(g_bySensorIsOpen == SENSOR_OPEN)
	{
		if(ExposureTimeAutoItem.Last == EXPOSURE_TIME_AUTO_MOD_APERTPRO)
		{
			if(g_bySensor_YuvMode != RAW_MODE)
			{
				SetSensorLowLightComp(bySetValue);
			}
			else
			{
				// hemonel 2010-03-15: force AE to adjust
				if(LowLightCompItem.Last != bySetValue)
				{
#ifdef 	_LENOVO_JAPAN_PROPERTY_PAGE_			
					if (bySetValue==2) 
					{
						g_wAEC_Gain_Threshold_15fps = ct_IQ_Table.ae.limit.wGainTh_15fps
							- (ct_IQ_Table.ae.limit.wGainTh_15fps>>2);
						g_wAEC_Gain_Threshold_30fps = ct_IQ_Table.ae.limit.wGainTh_30fps
							- (ct_IQ_Table.ae.limit.wGainTh_30fps>>2);
						g_wAEC_CurGain_Threshold_15fps = g_wAEC_Gain_Threshold_15fps;
						g_wAEC_CurGain_Threshold_30fps =  g_wAEC_Gain_Threshold_30fps;
					}
					else 
					{
						g_wAEC_CurGain_Threshold_15fps = g_wAEC_Gain_Threshold_15fps 
							=ct_IQ_Table.ae.limit.wGainTh_15fps;
						g_wAEC_CurGain_Threshold_30fps = g_wAEC_Gain_Threshold_30fps
							=ct_IQ_Table.ae.limit.wGainTh_30fps;

					}	
#endif				
					g_byAEForce_Adjust = 1;
				}
			}
		}
	}
}

/*
*********************************************************************************************************
*											Brightness SET_CUR Control
* FUNCTION BrightnessSet
*********************************************************************************************************
*/
/**
  Set current value of Brightness, change brightness offset value.

  \param wSetValue	Brightness control SET_CUR value.

  \retval 	TRUE	Succeed.
	       FALSE	Failed.
*********************************************************************************************************
*/
void BrightnessSet(S8 sbySetValue)
{
#ifdef _UVC_PPWB_
	if ((S8)g_VsPropCommit.wBrightness!= sbySetValue)
	{
		g_wProcessUnitChange_Flag |= PU_CTL_SEL_BRIGHTNESS ;
	}
	else
	{
		g_wProcessUnitChange_Flag &= ~PU_CTL_SEL_BRIGHTNESS ;
	}
#endif
	
	if(g_bySensorIsOpen == SENSOR_OPEN)
	{
#ifdef _USE_BK_HSBC_ADJ_
		SetBkBrightness( sbySetValue);
#else
		SetSensorBrightness( sbySetValue);
#endif
	}
}

/*
*********************************************************************************************************
*											Contrast SET_CUR Control
* FUNCTION ContrastSet
*********************************************************************************************************
*/
/**
  Set current value of Contrast, change Contrast offset value. This function is manual contrast.

  \param wSetValue	Contrast control SET_CUR value.

  \retval 	TRUE	Succeed.
	       FALSE	Failed.
*********************************************************************************************************
*/
void ContrastSet(U8 bySetValue)
{
#ifdef _UVC_PPWB_
	if ((U8)g_VsPropCommit.wContrast!= bySetValue)
	{
		g_wProcessUnitChange_Flag |= PU_CTL_SEL_CONTRAST ;
	}
	else
	{
		g_wProcessUnitChange_Flag &= ~PU_CTL_SEL_CONTRAST ;
	}
#endif
	
	if(g_bySensorIsOpen == SENSOR_OPEN)
	{
#ifdef _USE_BK_HSBC_ADJ_
		SetBkContrast( bySetValue);
#else
		SetSensorContrast(bySetValue);
#endif
	}
}

/*
*********************************************************************************************************
*											Saturation SET_CUR Control
* FUNCTION SaturationSet
*********************************************************************************************************
*/
/**
  Set current value of Saturation, change Saturation offset value.

  \param wSetValue	Saturation control SET_CUR value.

  \retval 	TRUE	Succeed.
	       FALSE	Failed.
*********************************************************************************************************
*/
void SaturationSet(U8 bySetValue)
{
#ifdef _UVC_PPWB_
	if ((U8)g_VsPropCommit.wSaturation!= bySetValue)
	{
		g_wProcessUnitChange_Flag |= PU_CTL_SEL_SATURATION ;
	}
	else
	{
		g_wProcessUnitChange_Flag &= ~PU_CTL_SEL_SATURATION ;
	}
#endif
	
	if(g_bySensorIsOpen == SENSOR_OPEN)
	{
#ifdef _USE_BK_HSBC_ADJ_
		SetBkSaturation( bySetValue);
#else
		SetSensorSaturation( bySetValue);
#endif
	}
}

void HueSet(S16 swSetValue)
{
#ifdef _UVC_PPWB_
	if (g_VsPropCommit.wHue!= swSetValue)
	{
		g_wProcessUnitChange_Flag |= PU_CTL_SEL_HUE ;
	}
	else
	{
		g_wProcessUnitChange_Flag &= ~PU_CTL_SEL_HUE ;
	}
#endif
	
	if(g_bySensorIsOpen == SENSOR_OPEN)
	{
#ifdef _USE_BK_HSBC_ADJ_
		SetBkHue ( swSetValue);
#else
		SetSensorHue ( swSetValue);
#endif
	}
}

void SharpnessSet(U8 bySetValue)
{
#ifdef _UVC_PPWB_
	if ((U8)g_VsPropCommit.wSharpness!= bySetValue)
	{
		g_wProcessUnitChange_Flag |= PU_CTL_SEL_SHARPNESS ;
	}
	else
	{
		g_wProcessUnitChange_Flag &= ~PU_CTL_SEL_SHARPNESS ;
	}
#endif
	
	if(g_bySensorIsOpen == SENSOR_OPEN)
	{
		if(g_bySensor_YuvMode == RAW_MODE)
		{
			SetBkSharpness (bySetValue);
		}
		else
		{
			SetSensorSharpness(bySetValue);
		}
	}
}

/*
*********************************************************************************************************
*											Gamma SET_CUR Control
* FUNCTION GammaSet
*********************************************************************************************************
*/
/**
  Set current value of Gamma, change gamma configure value. From HW register defined, the formula for Gamma configure value is:
  Gamma configure value = abs(Gamma setting - Gamma default)/1.25 .

  \param wSetValue	Gamma control SET_CUR value.

  \retval 	TRUE	Succeed.
	       FALSE	Failed.
*********************************************************************************************************
*/
void GammaSet(U16 wSetValue)
{
#ifdef _UVC_PPWB_
	if (g_VsPropCommit.wGamma!= wSetValue)
	{
		g_wProcessUnitChange_Flag |= PU_CTL_SEL_GAMMA ;
	}
	else
	{
		g_wProcessUnitChange_Flag &= ~PU_CTL_SEL_GAMMA ;
	}
#endif

	if(g_bySensorIsOpen == SENSOR_OPEN)
	{
		SetGammaParam(wSetValue);
	}
}

void BackLightCompSet(U8 bySetValue)
{
#ifdef _UVC_PPWB_
	if ((U8)g_VsPropCommit.wBackLightComp != bySetValue)
	{
		g_wProcessUnitChange_Flag |= PU_CTL_SEL_BACKLIGHT_COMPENSATION ;
	}
	else
	{
		g_wProcessUnitChange_Flag &= ~PU_CTL_SEL_BACKLIGHT_COMPENSATION ;
	}
#endif
	
	if(g_bySensorIsOpen == SENSOR_OPEN)
	{
		SetSensorBackLightComp(bySetValue);
	}
}

void GainSet(U8 bySetValue)
{
#ifdef _UVC_PPWB_
	if ((U8)g_VsPropCommit.wGain!= bySetValue)
	{
		g_wProcessUnitChange_Flag |= PU_CTL_SEL_GAIN ;
	}
	else
	{
		g_wProcessUnitChange_Flag &= ~PU_CTL_SEL_GAIN ;
	}
#endif

	if(g_bySensorIsOpen == SENSOR_OPEN)
	{
		SetGainParam(bySetValue);
	}
}

void PwrLinFreqSet(U8 bySetValue)
{
#ifdef _UVC_PPWB_
	if (g_VsPropCommit.byPwrLineFreq != bySetValue)
	{
		g_wProcessUnitChange_Flag |= PU_CTL_SEL_POWER_LINE_FREQUENCY ;
	}
	else
	{
		g_wProcessUnitChange_Flag &= ~PU_CTL_SEL_POWER_LINE_FREQUENCY ;
	}
#endif
	
	if(g_bySensorIsOpen == SENSOR_OPEN)
	{
#ifdef _ENABLE_ANTI_FLICK_
		AB_MSG(("PwrLinFreqSet pl:%bu\n",	bySetValue));

		if (bySetValue)
		{
				g_byABCurPowerLine = bySetValue;
#ifdef 	_LENOVO_JAPAN_PROPERTY_PAGE_	
			if (bySetValue == PWR_LINE_FRQ_OUTDOOR)
			{
				g_byABCurPowerLine = PWR_LINE_FRQ_60;
			}	
#endif			
		}
		else
		{
			g_byABDetectType = 0x1;
			g_byABDetectCount = g_byABDynamicDctCntSetting;
			g_byABDetectStatis = 0x0;
			g_byABStart = 0x0;

		}
#endif

		SetSensorPwrLineFreq(g_byCurFps, bySetValue);
	}
}

void WhiteBalanceTempSet(U16 wSetValue)
{
#ifdef _UVC_PPWB_
	if (g_VsPropCommit.wWhiteBalanceTemp != wSetValue)
	{
		g_wProcessUnitChange_Flag |= PU_CTL_SEL_WHITE_BALANCE_TEMPERATURE ;
	}
	else
	{
		g_wProcessUnitChange_Flag &= ~PU_CTL_SEL_WHITE_BALANCE_TEMPERATURE ;
	}
#endif
	
	if(g_bySensorIsOpen == SENSOR_OPEN)
	{
		SetSensorWBTemp(wSetValue);
	}
}

void WhiteBalanceTempAutoSet(U8 bySetValue)
{
#ifdef _UVC_PPWB_
	if (g_VsPropCommit.byWhiteBalanceTempAuto != bySetValue)
	{
		g_wProcessUnitChange_Flag |= PU_CTL_SEL_WHITE_BALANCE_TEMPERATURE_AUTO ;
	}
	else
	{
		g_wProcessUnitChange_Flag &= ~PU_CTL_SEL_WHITE_BALANCE_TEMPERATURE_AUTO ;
	}
#endif
	
	if(g_bySensorIsOpen == SENSOR_OPEN)
	{
		SetSensorWBTempAuto(bySetValue);
	}
}

/*
*********************************************************************************************************
*											PanTilt SET_CUR Control
* FUNCTION PanTiltSet
*********************************************************************************************************
*/
/**
  Set current value of PanTilt, change PanTilt offset value. This function is manual PanTilt.

  \param swPanSetValue	Pan control SET_CUR value.
  		swTiltSetValue		Tilt control SET_CUR value.

  \retval 	None.
*********************************************************************************************************
*/
void PanTiltSet(S8 sbyPanSetValue, S8 sbyTiltSetValue)
{
#ifdef _UVC_PPWB_
	if (((S8)g_VsPropCommit.wPan != sbyPanSetValue) || ((S8)g_VsPropCommit.wTilt != sbyTiltSetValue))
	{
		g_wCameraTerminalChange_Flag |= CT_CTL_SEL_PANTILT_ABS ;
	}
	else
	{
		g_wCameraTerminalChange_Flag &= ~CT_CTL_SEL_PANTILT_ABS ;
	}
#endif
	
	if(g_bySensorIsOpen == SENSOR_OPEN)
	{		
		XBYTE[ISP_ZOOM_SYNC] = 0;
		SetZoomStart(sbyPanSetValue, sbyTiltSetValue,0);
		XBYTE[ISP_ZOOM_SYNC] = 1; 		
	}
}

/*
*********************************************************************************************************
*											Zoom SET_CUR Control
* FUNCTION ZoomSet
*********************************************************************************************************
*/
/**
  Set current value of Zoom, change Zoom offset value. This function is manual Zoom.

  \param bySetValue	Zoom control SET_CUR value.

  \retval 	None.
*********************************************************************************************************
*/
void ZoomSet(U8 bySetValue)
{
#ifdef _UVC_PPWB_
	if ((U8)g_VsPropCommit.wZoom!= bySetValue)
	{
		g_wCameraTerminalChange_Flag |= CT_CTL_SEL_ZOOM_ABS ;
	}
	else
	{
		g_wCameraTerminalChange_Flag &= ~CT_CTL_SEL_ZOOM_ABS ;
	}
#endif
	
	if(g_bySensorIsOpen == SENSOR_OPEN)
	{

		XBYTE[ISP_ZOOM_SYNC] = 0;
		SetBKScale(bySetValue, g_wSensorCurFormatWidth>>g_byHalfSubSample_En, g_wSensorCurFormatHeight>>g_byHalfSubSample_En, g_wCurFrameWidth, g_wCurFrameHeight,TCorrectionItem.Last);
		XBYTE[ISP_ZOOM_SYNC] = 1;
	}
}

/*
*********************************************************************************************************
*											Roll SET_CUR Control
* FUNCTION RollSet
*********************************************************************************************************
*/
/**
  Set current value of Roll, change Roll offset value. This function is manual Roll.

  \param bySetValue	Roll control SET_CUR value.

  \retval 	None.
*********************************************************************************************************
*/
void RollSet(U8 const bySetValue)
{
#ifdef _UVC_PPWB_
	if ((U8)g_VsPropCommit.wRoll!= bySetValue)
	{
		g_wCameraTerminalChange_Flag |= CT_CTL_SEL_ROLL_ABS ;
	}
	else
	{
		g_wCameraTerminalChange_Flag &= ~CT_CTL_SEL_ROLL_ABS ;
	}
#endif
	
	if(g_bySensorIsOpen == SENSOR_OPEN)
	{
		SetSensorImgDir(bySetValue^g_bySnrImgDir);//flip: bit1,mirror bit0.
	}
}

#ifdef	_TCORRECTION_EN_

void TrapeziumCorrectionSet(S8 sbySetValue)
{
	if(g_bySensorIsOpen == SENSOR_OPEN)
	{
		XBYTE[ISP_ZOOM_SYNC] = 0;
		SetBKScale(ZoomItem.Last, g_wSensorCurFormatWidth>>g_byHalfSubSample_En, g_wSensorCurFormatHeight>>g_byHalfSubSample_En, g_wCurFrameWidth, g_wCurFrameHeight,sbySetValue);
		XBYTE[ISP_ZOOM_SYNC] = 1; 
	}
}

#endif
#ifdef _AF_ENABLE_
void SetFocusAbsolute(U16 wSetValue)
{
	//wSetValue = ClipWord(wSetValue*2+150, 1, 1023);
	VCM_Write(wSetValue+1);

}

void SetFocusAuto(U8 bySetValue)
{
	if(bySetValue == 1)
	{	
		VCM_Write(g_wAF_CurPosition);
#ifdef _CUSTOMER_ISP_
		CusAFStaticsStart();
#else		
		ISP_AFStaticsStart();
#endif
	}
}

void FocusSet(U16 wSetValue)
{
#ifdef _UVC_PPWB_
	if (g_VsPropCommit.wFocusAbsolut != wSetValue)
	{
		g_wCameraTerminalChange_Flag |= CT_CTL_SEL_FOCUS_ABS ;
	}
	else
	{
		g_wCameraTerminalChange_Flag &= ~CT_CTL_SEL_FOCUS_ABS ;
	}
#endif
	
	if(g_bySensorIsOpen == SENSOR_OPEN)
	{
		SetFocusAbsolute(wSetValue);
	}
}

void FocusAutoSet(U8 bySetValue)
{
#ifdef _UVC_PPWB_
	if (g_VsPropCommit.byFocusAuto != bySetValue)
	{
		g_wCameraTerminalChange_Flag |= CT_CTL_SEL_FOCUS_AUTO_FOR_PPWB ;
	}
	else
	{
		g_wCameraTerminalChange_Flag &= ~CT_CTL_SEL_FOCUS_AUTO_FOR_PPWB ;
	}
#endif

	if(g_bySensorIsOpen == SENSOR_OPEN)
	{
		SetFocusAuto(bySetValue);
	}
}
#endif

#if (defined _UVC_PPWB_ || defined _UVC_COMMITWB_)

//QFN40 GPIO[10] used as SPI_WP#
#define SFLASH_EEP_ENABLE()   {SET_GPIO_H_INPUT(SPI_WP_GPIO10);SET_GPIO_L_INPUT(SPI_WP_GPIO);}

void SFLASH_EEP_WP_DISABLE()
{
//QFN40 GPIO[10] used as SPI_WP#
	GPIO_H_DRIVE_HIGH(SPI_WP_GPIO10); 	// disable SPI write protect
	SET_GPIO_H_OUTPUT(SPI_WP_GPIO10);
//_PACKAGE_QFN32_	//GPIO[0] used as SPI_WP#
	GPIO_L_DRIVE_HIGH(SPI_WP_GPIO); 	// disable SPI write protect
	SET_GPIO_L_OUTPUT(SPI_WP_GPIO);
}

void SaveUVCPPParamToCram(U16 wDAddr)
{
#ifdef _UVC_PPWB_
	// process unit control
	g_VsPropCommit.wBrightness				= 	 BrightnessItem.Last;
	g_VsPropCommit.wContrast				= 	 ContrastItem.Last;
	g_VsPropCommit.wHue					= 	 HueItem.Last;
	g_VsPropCommit.wSaturation				= 	 SaturationItem.Last;
	g_VsPropCommit.wSharpness				= 	 SharpnessItem.Last;
	g_VsPropCommit.wGamma					= 	 GammaItem.Last;
	g_VsPropCommit.wWhiteBalanceTemp		= 	 WhiteBalanceTempItem.Last ;
	g_VsPropCommit.byWhiteBalanceTempAuto	=	 WhiteBalanceTempAutoItem.Last;
	g_VsPropCommit.wBackLightComp			= 	 BackLightCompItem.Last;
	g_VsPropCommit.wGain					= 	 GainItem.Last;
	g_VsPropCommit.byPwrLineFreq			= 	 PwrLineFreqItem.Last;

	// camera terminal control
	g_VsPropCommit.dwExposureTimeAbsolut	= 	 ExposureTimeAbsolutItem.Last;
	g_VsPropCommit.byExposureTimeAuto		= 	 ExposureTimeAutoItem.Last;
	g_VsPropCommit.byLowLightComp			= 	 LowLightCompItem.Last;
	g_VsPropCommit.wPan					= 	 PanItem.Last;
	g_VsPropCommit.wTilt					= 	 TiltItem.Last;
	g_VsPropCommit.wZoom					= 	 ZoomItem .Last;
	g_VsPropCommit.wRoll					= 	 RollItem.Last;
#ifdef _AF_ENABLE_
	g_VsPropCommit.wFocusAbsolut			= 	 FocusAbsolutItem.Last;
	g_VsPropCommit.byFocusAuto				= 	FocusAutoItem.Last;
#endif
#ifdef _LENOVO_JAPAN_PROPERTY_PAGE_
	 g_VsPropCommit.g_byAWBRGain_Lenovo = g_byAWBRGain_Lenovo;
	 g_VsPropCommit.g_byAWBGGain_Lenovo = g_byAWBGGain_Lenovo;
	 g_VsPropCommit.g_byAWBBGain_Lenovo = g_byAWBBGain_Lenovo;
#endif
#ifdef _RTK_EXTENDED_CTL_
	g_VsPropCommit.wRtkExtISPSpecialEffect	=	RtkExtISPSpecialEffectItem.Last;
	g_VsPropCommit.wRtkExtEVCompensation = RtkExtEVCompensationItem.Last;
	g_VsPropCommit.byRtkExtPreviewLEDOff = RtkExtPreviewLEDOffItem.Last;
	g_VsPropCommit.wRtkExtISO	=	RtkExtISOItem.Last;
#endif
#endif

#ifdef _UVC_COMMITWB_
	// commit control
	g_VsBulkCommit.byCommitFormatIndex = g_VsCommit.bFormatIndex;
	g_VsBulkCommit.byCommitFrameIndex = g_VsCommit.bFrameIndex;
	g_VsBulkCommit.byCommitFPS = g_byCommitFPS;
	//xiaozhizou 2012-6-27:save g_wTransferSize and wLWM to flash which will not re-calculate(no probe&commit in Win8) afer wakeup from S3
	g_VsBulkCommit.wTransferSize = g_wTransferSize;
	g_VsBulkCommit.wLWM = g_wLWM;
	g_VsBulkCommit.wHWM = g_wHWM;
#endif

#ifdef _UVC_PPWB_
	memcpy(((U8 *)&XBYTE[wDAddr]),&g_VsPropCommit,sizeof(VSPropertyCommCtl_t));
#endif
#ifdef _UVC_COMMITWB_
	memcpy(((U8 *)&XBYTE[wDAddr+sizeof(VSPropertyCommCtl_t)]),&g_VsBulkCommit,sizeof(VSBulkCommit_t));
#endif

}

void LoadUVCPPParam_Last(void)
{
#ifdef _UVC_PPWB_
	// copy back to control current value
	BrightnessItem.Last           	= g_VsPropCommit.wBrightness						;
	ContrastItem.Last             	= g_VsPropCommit.wContrast							;
	HueItem.Last                  	= g_VsPropCommit.wHue										;
	SaturationItem.Last           	= g_VsPropCommit.wSaturation						;
	SharpnessItem.Last            	= g_VsPropCommit.wSharpness							;
	GammaItem.Last                	= g_VsPropCommit.wGamma									;
	WhiteBalanceTempItem.Last     	= g_VsPropCommit.wWhiteBalanceTemp			;
	WhiteBalanceTempAutoItem.Last  	=	g_VsPropCommit.byWhiteBalanceTempAuto	;
	BackLightCompItem.Last        	= g_VsPropCommit.wBackLightComp					;
	GainItem.Last                 	= g_VsPropCommit.wGain									;
	PwrLineFreqItem.Last          	= g_VsPropCommit.byPwrLineFreq					;
	ExposureTimeAbsolutItem.Last  	= g_VsPropCommit.dwExposureTimeAbsolut	;
	ExposureTimeAutoItem.Last     	= g_VsPropCommit.byExposureTimeAuto			;
	LowLightCompItem.Last	       		= g_VsPropCommit.byLowLightComp					;
	PanItem.Last                  	= g_VsPropCommit.wPan										;
	TiltItem.Last                 	= g_VsPropCommit.wTilt									;
	ZoomItem .Last                	= g_VsPropCommit.wZoom									;
	RollItem.Last                 	= g_VsPropCommit.wRoll	;
#ifdef _AF_ENABLE_
	FocusAbsolutItem.Last	=	g_VsPropCommit.wFocusAbsolut		;
	FocusAutoItem.Last		=g_VsPropCommit.byFocusAuto			;
#endif
#ifdef _LENOVO_JAPAN_PROPERTY_PAGE_
	g_byAWBRGain_Lenovo = g_VsPropCommit.g_byAWBRGain_Lenovo;
	g_byAWBGGain_Lenovo = g_VsPropCommit.g_byAWBGGain_Lenovo;
	g_byAWBBGain_Lenovo = g_VsPropCommit.g_byAWBBGain_Lenovo;
#endif
#ifdef _RTK_EXTENDED_CTL_
	RtkExtISPSpecialEffectItem.Last = g_VsPropCommit.wRtkExtISPSpecialEffect;
	RtkExtEVCompensationItem.Last = g_VsPropCommit.wRtkExtEVCompensation;
	RtkExtPreviewLEDOffItem.Last = g_VsPropCommit.byRtkExtPreviewLEDOff;
	RtkExtISOItem.Last = g_VsPropCommit.wRtkExtISO;
#endif
#endif

#ifdef _UVC_COMMITWB_
	// commit control
	g_VsCommit.bFormatIndex = g_VsBulkCommit.byCommitFormatIndex;
	g_VsCommit.bFrameIndex = g_VsBulkCommit.byCommitFrameIndex;
	g_byCommitFPS = g_VsBulkCommit.byCommitFPS;
	g_wTransferSize = g_VsBulkCommit.wTransferSize;
	g_wLWM = g_VsBulkCommit.wLWM;
	g_wHWM = g_VsBulkCommit.wHWM;
	XBYTE[RF_EPA_TRANSFER_SIZE_0] =INT2CHAR(g_wTransferSize, 0); 
	XBYTE[RF_EPA_TRANSFER_SIZE_1] =INT2CHAR(g_wTransferSize, 1);
	XBYTE[RF_LWM_0] =  INT2CHAR(g_wLWM,0);
	XBYTE[RF_LWM_1] =  INT2CHAR(g_wLWM,1);
	XBYTE[RF_HWM_0] =  INT2CHAR(g_wHWM,0);
	XBYTE[RF_HWM_1] =  INT2CHAR(g_wHWM,1);
#endif
}

void LoadUVCPPParam(void)
{
#ifdef _PPWB_ADDR_0X10000_
	SPI_WB_Real_Read((U32)(PP_WB_ADDR_BASE), 64, g_byReadMode);
	if((XBYTE[SPI_I2C_BUFFER_BASE+PP_WB_EXIST_ADDR_OFFSET]==0x55)&&(XBYTE[SPI_I2C_BUFFER_BASE+PP_WB_EXIST_ADDR_OFFSET+1]==0xAA))
	{
#ifdef _UVC_PPWB_
		memcpy(&g_VsPropCommit, (U8 xdata *)SPI_I2C_BUFFER_BASE,sizeof(VSPropertyCommCtl_t));
#endif
#ifdef _UVC_COMMITWB_
		memcpy(&g_VsBulkCommit, (U8 xdata *)(SPI_I2C_BUFFER_BASE+sizeof(VSPropertyCommCtl_t)),sizeof(VSBulkCommit_t));
#endif
		LoadUVCPPParam_Last();
	}
	
#else

	if((CBYTE[PP_WB_ADDR_BASE+PP_WB_EXIST_ADDR_OFFSET]==0x55)&&(CBYTE[PP_WB_ADDR_BASE+PP_WB_EXIST_ADDR_OFFSET+1]==0xAA))
	{
#ifdef _UVC_PPWB_
		memcpy(&g_VsPropCommit, (U8 code *)PP_WB_ADDR_BASE,sizeof(VSPropertyCommCtl_t));
#endif
#ifdef _UVC_COMMITWB_
		memcpy(&g_VsBulkCommit, (U8 code *)(PP_WB_ADDR_BASE+sizeof(VSPropertyCommCtl_t)),sizeof(VSBulkCommit_t));
#endif
		LoadUVCPPParam_Last();
	}
	
#endif
}

void PropertyPageWritebackToFlash(void)
{
	// disable serial flash write protect
	SFLASH_EEP_WP_DISABLE();	// disable serial flash hardware write protect
#ifndef _ENABLE_OLT_	
	SPI_WB_WriteStatus(0x00);	// disable serial flash block protect
	// sector erase
	SPI_WB_EraceSFSector((U32)PP_WB_ADDR_BASE);	// sector erase
#endif
	// prepare PP write back data
	memset((U8 *)&XBYTE[SPI_I2C_BUFFER_BASE],0xFF,62);	// hemonel 2012-04-13: unused property page space reserved 0xff
	SaveUVCPPParamToCram(SPI_I2C_BUFFER_BASE);
	XBYTE[SPI_I2C_BUFFER_BASE+PP_WB_EXIST_ADDR_OFFSET]= 0x55;
	XBYTE[SPI_I2C_BUFFER_BASE+PP_WB_EXIST_ADDR_OFFSET+1]= 0xAA;

	// write
	SPI_WB_Program((U32)PP_WB_ADDR_BASE, 0x00, 64);
#ifndef _ENABLE_OLT_	
	// enable serial flash write protect
	SPI_WB_WriteStatus(0x8C);	// enable serial flash block protect
#endif	
	SFLASH_EEP_ENABLE();	// enable serial flash hardware write protect
}

void ParaWriteBackToFlash(void )
{
#ifdef _RTK_EXTENDED_CTL_
	if((g_wProcessUnitChange_Flag !=0)||(g_wCameraTerminalChange_Flag !=0)||(g_byCommitChange_Flag!=0)||(g_dwRtkExtCtlUnitChange_Flag!=0))
#else
	if((g_wProcessUnitChange_Flag !=0)||(g_wCameraTerminalChange_Flag !=0)||(g_byCommitChange_Flag!=0))
#endif
	{
		g_wProcessUnitChange_Flag=0;
		g_wCameraTerminalChange_Flag=0;
		g_byCommitChange_Flag=0;
#ifdef _RTK_EXTENDED_CTL_
		g_dwRtkExtCtlUnitChange_Flag=0;
#endif
		PropertyPageWritebackToFlash();
	}	
}

#endif
#ifdef _IQ_TABLE_CALIBRATION_
void LoadIQTablePatch(void )
{
	SPI_WB_Real_Read((U32)(IQ_TABLE_PATCH_BASE), 16, g_byReadMode);
	if((XBYTE[SPI_I2C_BUFFER_BASE]==0x55)&&(XBYTE[SPI_I2C_BUFFER_BASE+1]==0xAA))
	{
		if ((XBYTE[SPI_I2C_BUFFER_BASE+2]==IQ_TABLE_PATCH_MVersion)&&(XBYTE[SPI_I2C_BUFFER_BASE+3]==IQ_TABLE_PATCH_SVersion))	//Must Check Version
		{
			//offset = 4
			g_byIQPatchGrpOnePatchOne = ~(XBYTE[SPI_I2C_BUFFER_BASE+4]);
			//offset = 5
			//...Reserved
		}
	}	
}
#endif

