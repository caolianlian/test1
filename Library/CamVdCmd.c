#include "pc_cam.h"
#include "camutil.h"
#include "CamReg.h"
#include "CamUvc.h"
#include "CamCtl.h"
#include "CamI2C.h"
#include "camisp.h"
#include "camvdcmd.h"
#include "camspi.h"
#include "Global_vars.h"
#include "camvdcfg.h"
#include "ISP_vars.h"
#include "CamSFR.h"
#include "CamAwbPro.h"
#include "CamReset.h"

U8 code QualificationString[8]="REALSIL";
U8 code  FwCompDateString[9] = __DATE2__;
void StartBackgroundStreaming(void);
void StopBackgroundStreaming(void);
void GetDevStatus(U8 bySection,U8  byLen)
{
	U8 i;
	switch(bySection)
	{
	case 0:	// system related info
		EP0_DATA_IN(_RT_VID_L_);
		EP0_DATA_IN(_RT_VID_H_);
		EP0_DATA_IN(_5840_PID_L_);
		EP0_DATA_IN(_5840_PID_H_);
		EP0_DATA_IN(FW_CUS_LIB_VER);
		EP0_DATA_IN(FW_TRK_VER);
		EP0_DATA_IN(XBYTE[HW_VER_ID1]);
		EP0_DATA_IN(XBYTE[HW_VER_ID2]);
		break;
	case 1: // sensor related info
		EP0_DATA_IN(INT2CHAR(g_wSensor,0));
		EP0_DATA_IN(INT2CHAR(g_wSensor,1));
		EP0_DATA_IN(g_bySensorSize);
		EP0_DATA_IN(g_SnrRegAccessProp.byI2CID);
		EP0_DATA_IN(INT2CHAR(g_wSensorCurFormat,0));
		EP0_DATA_IN(INT2CHAR(g_wSensorCurFormat,1));
		EP0_DATA_IN(INT2CHAR(g_wSensorSPFormat,0));
		EP0_DATA_IN(INT2CHAR(g_wSensorSPFormat,1));
		break;
	case 2: //rom code compiler date
	case 11: // customer fw compiler date
		for(i=0; i<8; i++)
		{
			EP0_DATA_IN(FwCompDateString[i]); //"MM/DD/YY"
		}
		break;
	case 4: //feature bitmap,BYTE[32..39]
#ifdef _REAR_CAMERA_
		EP0_DATA_IN(0x01);
#else
		EP0_DATA_IN(0);
#endif
		EP0_DATA_IN(g_bySensorIsOpen);
		EP0_DATA_IN(AP_MAIN_VERSION);
		EP0_DATA_IN(AP_SUB_VERSION);
		for(i=4; i<8; i++)
		{
			EP0_DATA_IN(0);
		}
		break;
	case 8:
    	EP0_DATA_IN(g_cVdFwVerSection0.byHeader);
    	EP0_DATA_IN(g_cVdFwVerSection0.byLen);
    	EP0_DATA_IN(LONG2CHAR(g_cVdFwVerSection0.dwMagictag,0));
    	EP0_DATA_IN(LONG2CHAR(g_cVdFwVerSection0.dwMagictag,1));
    	EP0_DATA_IN(LONG2CHAR(g_cVdFwVerSection0.dwMagictag,2));
    	EP0_DATA_IN(LONG2CHAR(g_cVdFwVerSection0.dwMagictag,3));
    	EP0_DATA_IN(INT2CHAR(g_cVdFwVerSection0.wICName,0));
    	EP0_DATA_IN(INT2CHAR(g_cVdFwVerSection0.wICName,1));
    	break;
	default: // for extension and back capatiable.RESERVE 8 bytes for more status info.
		for(i=0; i<8; i++)
		{
			EP0_DATA_IN(DUMMY_BYTE);
		}
		break;
	}

	for(i=0; i<(byLen-8); i++)
	{
		EP0_DATA_IN(DUMMY_BYTE);
	}

}

/*
*********************************************************************************************************
						UVC Vendor DBG command  data in sub command data processing
* FUNCTION CmdDbgSdataIn
*********************************************************************************************************
*/
/**
  UVC vendor DBG command processing
  fill the ep0 fifo buffer with the data specified by addr and len.
  \param
       bySubCmd: sub command of Vendor DBG command
          wAddr: operation address, may useless for some sub commands
            len: data_in length

  \retval None
*********************************************************************************************************
*/
U8 CmdDbgSdataIn(U8 bySubCmd,U16 wAddr,U8 len)
{
	U8 i;
	U16 wValue;
#ifdef _I2C_NORMAL_
	U32 dwValue;
#endif
	U8 abyI2CDataBuf[8];

	switch(bySubCmd)
	{
	case VSCMD_CODE_R:
	case VSCMD_IMEM_R:
	case VSCMD_XMEM_R:
		{
			for(i=0; i<len; i++)
			{
				if(bySubCmd==VSCMD_CODE_R)
				{
					EP0_DATA_IN(CBYTE[wAddr+i]);
				}
				else if(bySubCmd==VSCMD_IMEM_R )
				{
					EP0_DATA_IN(DBYTE[wAddr+i]);
				}
				else if(bySubCmd==VSCMD_XMEM_R )
				{

					if((wAddr+i)==EP0_DAT)
					{
						EP0_DATA_IN(DUMMY_BYTE);
					}
					else
					{
						EP0_DATA_IN(XBYTE[wAddr+i]);
					}

				}
			}
			break;
		}
	case VSCMD_PHY_R:
		{
			EP0_DATA_IN(PHYRegisterRead(wAddr));
			break;
		}
	case VSCMD_BAR_INFO_READ:
		{
			for(i=0; i<len; i++)
			{
				if(g_wBarInfoOffset==0)
				{
					EP0_DATA_IN(DUMMY_BYTE);
				}
				else
				{
					EP0_DATA_IN(GetVdCfgByte(g_wBarInfoOffset+wAddr+i));
				}
			}
			break;
		}

//		case VSCMD_SNR_ID_R:
//			{
//				EP0_DATA_IN(INT2CHAR(g_wSensor,0));
//				EP0_DATA_IN(INT2CHAR(g_wSensor,1));
//				EP0_DATA_IN(g_bySensorSize);
//				EP0_DATA_IN(DUMMY_BYTE);
//				break;
//			}

	case VSCMD_I2C_R:
		{
			Read_SenReg(wAddr, &wValue);
			EP0_DATA_IN(INT2CHAR(wValue,0));
			EP0_DATA_IN(INT2CHAR(wValue,1));
			break;
		}
#ifdef _I2C_NORMAL_
	case VSCMD_I2C_NORMAL_R:
		I2C_NORMAL_Read((U8)wAddr, &dwValue, len);
		for(i=0;i<len;i++)
		{
			EP0_DATA_IN(LONG2CHAR(dwValue,(3-i)));		//first come first out,,dw type from 0 to 3	
		}
		break;
#endif	
	case VSCMD_I2C_SUPER_R:
		I2C_Super_Read(&g_tI2CSuperAccess, abyI2CDataBuf,len);
		for(i=0;i<len;i++)
		{
			EP0_DATA_IN(abyI2CDataBuf[i]);		//first come first out,,dw type from 0 to 3	
		}
		break;
#ifdef _SFR_ACCESS_
	case VSCMD_SFR_R:
		{
			EP0_DATA_IN(SFRRead((U8)wAddr));
			break;
		}
#endif
#ifdef _MIPI_EXIST_
	case VSCMD_MIPI_APHY_I2C_R:
		{
			Read_MIPIAPHYReg(wAddr, &wValue);
			EP0_DATA_IN(INT2CHAR(wValue,0));
			EP0_DATA_IN(INT2CHAR(wValue,1));
			break;
		}
#endif

		// hemonel 2010-08-27: delete micron read variable for optimize code
//		case VSCMD_MI_DRV_VAR_R:
//			{
//			#ifdef RTS58XX_SP_MICRON
//				Read_MI_Vairable(wAddr,&wValue);
//			#endif
		//DBG(("@1wAddr=%4x\n",wAddr));
		//DBG(("@3wValue=%4x\n",wValue));
//				EP0_DATA_IN(INT2CHAR(wValue,0));
//				EP0_DATA_IN(INT2CHAR(wValue,1));
//				break;
//			}
		// hemonel 2010-08-27: optimize because getdevstatus has reported
//		case VSCMD_SENSOR_I2CADDR_GET:
//			{
//				EP0_DATA_IN(g_SnrRegAccessProp.byI2CID);
//				break;
//			}
	case VSCMD_LEDMODE_GET:
		{
			EP0_DATA_IN(LED_WORKMODE_SWITCH);
			break;
		}
//	case VSCMD_FWBYPASS_GET:
//		{
//			EP0_DATA_IN(g_byFWBypass);
//			break;
//		}
		// hemonel 2010-08-27: delete dummy code
//#ifdef _SP_VENDOR_INS_CMD_
//		case VSCMD_EXCUTE_VDINS_GET:
//			{
//				EP0_DATA_IN(g_byVdInsExecResult);
//				break;
//			}
//#endif
//		case VSCMD_TEST_PATTERN_GET:
//			{
//				EP0_DATA_IN(g_bySensorOperationMode);
//				EP0_DATA_IN(0);
//				break;
//			}
	case VSCMD_DEV_STS_R:
		{
			GetDevStatus((U8)wAddr,len);
			break;
		}
		// hemonel 2010-08-27: delete dummy code
		//	case VSCMD_GAMMA_PARM_GET:
		//		{
		/*
		if(wAddr==0) //first 8 bytes
		{
			for(i=0;i<8;i++)
			{
				EP0_DATA_IN(XBYTE[ISP_GAMMA_P1+i]);
			}
		}
		else //last 7 bytes
		{
			for(i=0;i<7;i++)
			{
				EP0_DATA_IN(XBYTE[ISP_GAMMA_P1+8+i]);
			}
		}
		*/
		//			break;
		//		}
	case VSCMD_DEV_QUALIFICATION_GET:
		{
			for(i=0; i<8; i++)
			{
				EP0_DATA_IN(QualificationString[i]);
			}

			break;
		}
	default:
		{
			return (U8)VCMD_STS_UNKNOWN_SCMD;
		}
	}
	return (U8)VCMD_STS_RET_NO_ERR ;
}

/*
*********************************************************************************************************
						UVC Vendor DBG command  data out sub command data processing
* FUNCTION CmdDbgSdataOut
*********************************************************************************************************
*/
/**
  UVC vendor DBG command processing
  Get data from EP0 fifo buffer and call related routines to save them.
  \param
       bySubCmd: sub command of Vendor DBG command
          wAddr: operation address, may useless for some sub commands
            len: data_out length

  \retval None
*********************************************************************************************************
*/
U8 CmdDbgSdataOut(U8 bySubCmd,U16 wAddr,U8 len)
{
	U8 i;
	U16 wValue;
#ifdef _I2C_NORMAL_
	U32 dwValue = 0;
#endif
//#ifdef _ENABLE_MJPEG_
//	U32 dwTmp;
//#endif
//	U8 t[3];
	U8 byTemp1,byTemp2;
	U8 abyI2CDataBuf[8];
	static U8 byI2CSuperCmdFlag = 0;	
	
	switch(bySubCmd)
	{
	case VSCMD_XMEM_W:
		{
			for(i=0; i<len; i++)
			{
				if(bySubCmd==VSCMD_XMEM_W)
				{
					XBYTE[wAddr+i]=EP0_DATA_OUT();
				}
			}
			break;
		}
//		case VSCMD_TEST_PATTERN_SET:
//			{
//
//				g_bySensorOperationMode =EP0_DATA_OUT();
//			//	SensorOutputTestPattern(g_bySensorOperationMode);
//			}
	case VSCMD_PHY_W:
		{
			PHYRegisterWrite(EP0_DATA_OUT(), wAddr);
			break;
		}
		// hemonel 2010-08-27: delete becasue maybe use readmem/writemem replace this command
//#ifdef _ENABLE_MJPEG_
//		case VSCMD_JPEG_QTABLE_SET:
//			{
//				t[0] = EP0_DATA_OUT();
//				t[1] = EP0_DATA_OUT();
//				t[2] = EP0_DATA_OUT();
//				ASSIGN_LONG(dwTmp, 0, t[2],t[1], t[0]);
//				JPEG_Qtable_write((U8)(wAddr), dwTmp);
//				break;
//			}
//#endif //#ifdef _ENABLE_MJPEG_
	case VSCMD_I2C_W:
		{
			byTemp1=EP0_DATA_OUT();
			byTemp2=EP0_DATA_OUT();
			ASSIGN_U16BYTE(wValue, 0, byTemp1);
			ASSIGN_U16BYTE(wValue, 1, byTemp2);
			Write_SenReg(wAddr,wValue);
			break;
		}
#ifdef _I2C_NORMAL_
		case VSCMD_I2C_NORMAL_W:
			for(i=0;i<len;i++)
			{
				dwValue |= (U32)EP0_DATA_OUT()<<((3-i)*8);	//first come,first write,dw type from 0 to 3
			}
			I2C_NORMAL_Write((U8)wAddr,dwValue, len);
			break;
#endif
		case VSCMD_I2C_SUPER_W:
			if (0 == byI2CSuperCmdFlag)
			{
				g_tI2CSuperAccess.byI2CID = (U8)wAddr;
				g_tI2CSuperAccess.byMode = EP0_DATA_OUT();
				g_tI2CSuperAccess.byAddrLen = EP0_DATA_OUT();
				g_tI2CSuperAccess.byAddr1 = EP0_DATA_OUT();	
				g_tI2CSuperAccess.byAddr2 = EP0_DATA_OUT();	
				g_tI2CSuperAccess.byAddr3 = EP0_DATA_OUT();	
				g_tI2CSuperAccess.byAddr4 = EP0_DATA_OUT();	
				g_tI2CSuperAccess.byAddr5 = EP0_DATA_OUT();
				byTemp1 = EP0_DATA_OUT();
				
				if (len >8 )
				{			
					byTemp1 = len -8;
					for(i=0;i<byTemp1;i++)
					{
						abyI2CDataBuf[i] = EP0_DATA_OUT();
					}
					
					if (I2CSuper_Mode_W == g_tI2CSuperAccess.byMode)
					{
						if (FALSE == I2C_Super_Write(&g_tI2CSuperAccess,abyI2CDataBuf,byTemp1))
						{
							return (U8)VCMD_STS_HW_ERR;					
						}	
					}
				}
				else if (I2CSuper_Mode_W == g_tI2CSuperAccess.byMode)
				{
					byI2CSuperCmdFlag = 1;					
				}
				else
				{
					;
				}
			}
			else
			{
				for(i=0;i<len;i++)
				{
					abyI2CDataBuf[i] = EP0_DATA_OUT();
				}
				
				if (FALSE == I2C_Super_Write(&g_tI2CSuperAccess,abyI2CDataBuf,len))
				{
					byI2CSuperCmdFlag = 0;				
					return (U8)VCMD_STS_HW_ERR;					
				}
				else
				{
					byI2CSuperCmdFlag = 0;					
				}
			}
			break;	
#ifdef _SFR_ACCESS_
	case VSCMD_SFR_W:
		{
			SFRWrite((U8)(wAddr) , EP0_DATA_OUT());
			break;
		}
#endif
#ifdef _MIPI_EXIST_
	case VSCMD_MIPI_APHY_I2C_W:
		{
			byTemp1=EP0_DATA_OUT();
			byTemp2=EP0_DATA_OUT();
			ASSIGN_U16BYTE(wValue, 0, byTemp1);
			ASSIGN_U16BYTE(wValue, 1, byTemp2);
			Write_MIPIAPHYReg(wAddr,wValue);
			break;
		}
#endif

		// hemonel 2010-08-27: delete micron write variable for optimize code
		//	case VSCMD_MI_DRV_VAR_W:
		//		{
		//			t[0]=EP0_DATA_OUT();
		//			t[1]=EP0_DATA_OUT();
		//			ASSIGN_U16BYTE(wValue, 0, t[0]);
		//			ASSIGN_U16BYTE(wValue, 1, t[1]);
		//			//DBG(("@2wValue=%x\n",wValue));
		//		#ifdef RTS58XX_SP_MICRON
		//			Write_MI_Vairable(wAddr,wValue);
		//		#endif
		//			break;
		//		}
	case VSCMD_SENSOR_I2CADDR_SET:
		{

			g_SnrRegAccessProp.byI2CID= EP0_DATA_OUT();

			break;
		}
	case VSCMD_LEDMODE_SET:
		{
			byTemp1=EP0_DATA_OUT();
			if(byTemp1==LED_WORKMODE_TURN_ON)
			{
				TURN_LED_ON();
			}
			else if(byTemp1==LED_WORKMODE_TURN_OFF)
			{
				TURN_LED_OFF();
			}
			break;
		}
//	case VSCMD_FWBYPASS_SET:
//		{
//			g_byFWBypass = EP0_DATA_OUT();
//			break;
//		}
	default:
		{
			return (U8)VCMD_STS_UNKNOWN_SCMD;
		}
	}
	return (U8)VCMD_STS_RET_NO_ERR;
}

U8 CmdDbgSDbgcmd(U8 bySubCmd,U16 wLen,U8 byCmdFlag)
{
	U8 data ret= (U8)VCMD_STS_CMD;
	switch(bySubCmd)
	{
		// type 1 command: read data command
	case VSCMD_CODE_R:
	case VSCMD_IMEM_R:
	case VSCMD_XMEM_R:
		//	case VSCMD_GAMMA_PARM_GET:	// hemonel 2010-08-27: delete dummy code
	case VSCMD_BAR_INFO_READ:
//#ifdef _SP_VENDOR_INS_CMD_
//		case	 VSCMD_EXCUTE_VDINS_GET:
//#endif
		{
			if(wLen ==0 )
			{
				ret=(U8)VCMD_STS_ERR_LEN;
			}
			else
			{
				ret=(U8)VCMD_STS_DATA_IN;
			}
			break;
		}

	case VSCMD_I2C_R:
#ifdef _MIPI_EXIST_
	case VSCMD_MIPI_APHY_I2C_R:
#endif

		{
			if(wLen!=2 )
			{
				ret=(U8)VCMD_STS_ERR_LEN;
			}
			else
			{
				ret=(U8)VCMD_STS_DATA_IN;
			}
			break;
		}
		// hemonel 2010-08-27: delete becasue maybe use readmem/writemem replace this command
//		case VSCMD_JPEG_QTABLE_GET:
//		case VSCMD_SNR_ID_R:

//			{
//				if(wLen!=4 )
//				{ret=VCMD_STS_ERR_LEN;}
//				else
//				{ret=VCMD_STS_DATA_IN;}
//				break;
//			}
#ifdef _SFR_ACCESS_
	case VSCMD_SFR_R:
#endif
	case VSCMD_PHY_R:
		//	case VSCMD_SENSOR_I2CADDR_GET:	// hemonel 2010-08-27: optimize because getdevstatus has reported
	case VSCMD_LEDMODE_GET:
//	case VSCMD_FWBYPASS_GET:
		{
			if(wLen!=1 )
			{
				ret=(U8)VCMD_STS_ERR_LEN;
			}
			else
			{
				ret=(U8)VCMD_STS_DATA_IN;
			}
			break;
		}
	case VSCMD_DEV_STS_R:
	case VSCMD_DEV_QUALIFICATION_GET:
		{
			if(wLen < 8 )
			{
				ret=(U8)VCMD_STS_ERR_LEN;
			}
			else
			{
				ret=(U8)VCMD_STS_DATA_IN;
			}
#ifdef 	_LENOVO_JAPAN_PROPERTY_PAGE_		
			if (byCmdFlag == 0x55)	
			{
				g_byVendorDriver_Lenovo = 1;
			}
#endif	
			break;
		}

		// type 2 command: write data command
//		case VSCMD_GAMMA_PARM_SET:
	case VSCMD_XMEM_W:
//#ifdef _SP_VENDOR_INS_CMD_
//		case	 VSCMD_EXCUTE_VDINS_SET:
//#endif

		{
			if(wLen ==0 )
			{
				ret=(U8)VCMD_STS_ERR_LEN;
			}
			else
			{
				ret=(U8)VCMD_STS_DATA_OUT;
			}
			break;
		}
	case VSCMD_PHY_W:
	case VSCMD_SENSOR_I2CADDR_SET:
	case VSCMD_LEDMODE_SET:
#ifdef _SFR_ACCESS_
	case VSCMD_SFR_W:
#endif
//	case VSCMD_FWBYPASS_SET:
		{
			if(wLen!=1 )
			{
				ret=(U8)VCMD_STS_ERR_LEN;
			}
			else
			{
				ret=(U8)VCMD_STS_DATA_OUT;
			}
			break;
		}
	case VSCMD_I2C_W:
#ifdef _MIPI_EXIST_
	case VSCMD_MIPI_APHY_I2C_W:
#endif

		{
			if(wLen!=2 )
			{
				ret=(U8)VCMD_STS_ERR_LEN;
			}
			else
			{
				ret=(U8)VCMD_STS_DATA_OUT;
			}
			break;
		}
		// hemonel 2010-08-27: delete becasue maybe use readmem/writemem replace this command
		//	case VSCMD_JPEG_QTABLE_SET:
		//		{
		//			if(wLen!=4 )
		//			{ret=VCMD_STS_ERR_LEN;}
		//			else
		//			{ret=VCMD_STS_DATA_OUT;}
		//			break;
		//		}

		// type 3: no data command
	case VSCMD_LED_TOGGLE:
		g_wLEDToggleInterval =wLen;
		ret=(U8)VCMD_STS_CMD;
		break;
		/*		case VSCMD_INIT_VD_FUNC_POINTER:
					{
						Init_VdfunctionPointers();
						if(g_pfVdReadSensorID!=NULL) //Vendor AP reload vendor code ,so we need reinitialize Sensor ID and I2C ID_address according to vendor code.
						{
							(*g_pfVdReadSensorID)();
						}
						ret=VCMD_STS_CMD;
						break;
					}
		*/
	case VSCMD_INOUT_CTL_SIZE_SET:
		{
			g_byDataInOutCtlSize = INT2CHAR(wLen,0);
			ret=(U8)VCMD_STS_CMD;
			break;
		}
	case VSCMD_RST_2_UPDATEFW:
		{
			EP0_HSK();	
			switch(byCmdFlag)
			{
				case 4:
					XBYTE[SCRATCH0]=SF_FAST_DWN_SPICS_DELINK_FLAG0;
					XBYTE[SCRATCH1]=SF_FAST_DWN_SPICS_DELINK_FLAG1;
#ifdef _PG_EN_	
					XBYTE[PG_FSM_RST] |= PG_STATE_RST;	//mantis[0002204]
#endif	
#ifdef _USB2_LPM_
					XBYTE[USB_LPM] = (LPM_SUPPORT | LPM_NAK);
#endif	
					//Jimmy.20131106.Fix mantis[0004470] fastdownload spi controller abnormal
					//XBYTE[AL_CACHE_CFG] = MCU_RESET_ONLY;
					PatchMCUReset();					
					MCUReset();
					break;
				case 2:
					XBYTE[SCRATCH0]=SF_FAST_DOWNLOAD_FLAG0;
					XBYTE[SCRATCH1]=SF_FAST_DOWNLOAD_FLAG1;
#ifdef _PG_EN_	
					XBYTE[PG_FSM_RST] |= PG_STATE_RST;	//mantis[0002204]
#endif	
#ifdef _USB2_LPM_
					XBYTE[USB_LPM] = (LPM_SUPPORT | LPM_NAK);
#endif	
					//Jimmy.20131106.Fix mantis[0004470] fastdownload spi controller abnormal
					//XBYTE[AL_CACHE_CFG] = MCU_RESET_ONLY;
					PatchMCUReset();					
					MCUReset();						
					break;
				case 1:
					XBYTE[SCRATCH0] = SF_DOWNLOAD_FLAG0;
					XBYTE[SCRATCH1] = SF_DOWNLOAD_FLAG1;
					WaitTimeOut_Delay(5);
					XBYTE[USBCTL] = USB_DISCONNECT;
					WaitTimeOut_Delay(10);
#ifdef _PG_EN_	
					XBYTE[PG_FSM_RST] |= PG_STATE_RST;	//mantis[0002204]
#endif						
					//Jimmy.20121205.Fix mantis[0002412]
					//mode_hs always 1, When DP go high,fw can not receive suspend interrupt
					XBYTE[UTMI_TST] = 0x42;	//Force PHY into normal and full speed mode
					XBYTE[UTMI_TST] = 0x00;	//Then Clear	
#ifdef _USB2_LPM_
					XBYTE[USB_LPM] = 0x00;	//Jimmy.20130326.Rom Code not support LPM L1
#endif							
					XBYTE[AL_BOOTCTL] = BOOT_FROM_ROM;  
					break;
				case 3:
					XBYTE[SCRATCH0] = SF_DWN_SPICI_DELINK_FLAG0;
					XBYTE[SCRATCH1] = SF_DWN_SPICI_DELINK_FLAG1;
					WaitTimeOut_Delay(5);
					XBYTE[USBCTL] = USB_DISCONNECT;
					WaitTimeOut_Delay(10);
#ifdef _PG_EN_	
					XBYTE[PG_FSM_RST] |= PG_STATE_RST;	//mantis[0002204]
#endif						
					//Jimmy.20121205.Fix mantis[0002412]
					//mode_hs always 1, When DP go high,fw can not receive suspend interrupt
					XBYTE[UTMI_TST] = 0x42;	//Force PHY into normal and full speed mode
					XBYTE[UTMI_TST] = 0x00;	//Then Clear	
#ifdef _USB2_LPM_
					XBYTE[USB_LPM] = 0x00;	//Jimmy.20130326.Rom Code not support LPM L1
#endif						
					XBYTE[AL_BOOTCTL] = BOOT_FROM_ROM;  
					break;
				default:
					break;
			}
			ret=(U8)VCMD_STS_CMD;
			return ret;
		}
	case VSCMD_RST_2_NEWCODE:
		{
			EP0_HSK();
			ret=(U8)VCMD_STS_CMD;
			return ret;
		}
	case VSCMD_RST_2_ROM:
		{
			EP0_HSK();
			//WaitTimeOut(0, 0, 0, 10);
			WaitTimeOut_Delay(10);
			XBYTE[USBCTL] = USB_DISCONNECT;
			//WaitTimeOut(0, 0, 0, 10);
			WaitTimeOut_Delay(10);
#ifdef _PG_EN_	
			XBYTE[PG_FSM_RST] |= PG_STATE_RST;	//mantis[0002204]
#endif			
			//Jimmy.20121205.Fix mantis[0002412]
			//mode_hs always 1, When DP go high,fw can not receive suspend interrupt
			XBYTE[UTMI_TST] = 0x42;	//Force PHY into normal and full speed mode
			XBYTE[UTMI_TST] = 0x00;	//Then Clear	
#ifdef _USB2_LPM_
			XBYTE[USB_LPM] = 0x00;	//Jimmy.20130326.Rom Code not support LPM L1
#endif				
			XBYTE[AL_BOOTCTL] = BOOT_FROM_ROM;
			ret=(U8)VCMD_STS_CMD;
			return ret;
		}
#ifdef _I2C_NORMAL_
	case VSCMD_I2C_NORMAL_R:
		if(wLen > 4 )
		{
			ret=(U8)VCMD_STS_ERR_LEN;
		}
		else
		{
			ret=(U8)VCMD_STS_DATA_IN;
		}		
		break;
#endif
#ifdef _I2C_NORMAL_
	case VSCMD_I2C_NORMAL_W:
		if(wLen > 4 )
		{
			ret=(U8)VCMD_STS_ERR_LEN;
		}
		else
		{
			ret=(U8)VCMD_STS_DATA_OUT;
		}		
		break;
#endif
	case VSCMD_I2C_SUPER_R:
		if(wLen > 8 )
		{
			ret=(U8)VCMD_STS_ERR_LEN;
		}
		else
		{
			ret=(U8)VCMD_STS_DATA_IN;
		}		
		break;
	case VSCMD_I2C_SUPER_W:
		if(wLen > 16 )
		{
			ret=(U8)VCMD_STS_ERR_LEN;
		}
		else
		{
			ret=(U8)VCMD_STS_DATA_OUT;
		}		
		break;
#ifdef _IQ_TABLE_CALIBRATION_
	case VSCMD_IQTABLE_PATCH_EN:
		LoadIQTablePatch();
		break;
#endif
	default:
		{
			ret=(U8)VCMD_STS_UNKNOWN_SCMD;
			break;
		}
	}
	return ret;
}

/*
*********************************************************************************************************
						UVC Vendor DBG command  data in sub command data processing
* FUNCTION CmdDbgSdataIn
*********************************************************************************************************
*/
/**
  UVC vendor DBG command processing
  fill the ep0 fifo buffer with the data specified by addr and len.
  \param
       bySubCmd: sub command of Vendor DBG command
          wAddr: operation address, may useless for some sub commands
            len: data_in length   <= 8;

  \retval None
*********************************************************************************************************
*/
U8 CmdNVolRWSdataIn(U8 bySubCmd,U32 dwAddr,U8 len)
{
	U8 i;
	U8 ret;

	switch(bySubCmd)
	{
		case VSCMD_SPI_SF_R:
		case VSCMD_SPI_SF_FAST_R:
		case VSCMD_SPI_SF_FAST_DUAL_OUT_R:
		case VSCMD_SPI_SF_FAST_DUAL_INOUT_R:
			{
				if (dwAddr <= (U32)0xFFFF)
				{
					ret=SPI_WB_Read(dwAddr, len, bySubCmd);

				}
#if ((defined _PPWB_ADDR_0X10000_)||(defined _SPI_FAST_READ_)||(defined _IQ_TABLE_CALIBRATION_))				
				else
				{
					ret = SPI_WB_Real_Read(dwAddr, len, g_byReadMode);
				}	
#endif				
				break;
			}
		case VSCMD_SPI_SF_RID:
			{
				ret=SPI_ReadFlashID(SPI_BUFFER_BASE_ADDR);
				break;
			}
		case VSCMD_SPI_SF_RDSR:
			{
				ret=SPI_WB_ReadStatus();
				break;
			}
		default:
			{
				return (U8)VCMD_STS_UNKNOWN_SCMD;
			}
	}

	if(ret)
	{
		for(i=0; i<len; i++)
		{
			EP0_DATA_IN(XBYTE[SPI_BUFFER_BASE_ADDR+i]);
		}

		return (U8)VCMD_STS_RET_NO_ERR;
	}

	return (U8)VCMD_STS_HW_ERR;

}

/*
*********************************************************************************************************
						UVC Vendor DBG command  data out sub command data processing
* FUNCTION CmdDbgSdataOut
*********************************************************************************************************
*/
/**
  UVC vendor DBG command processing
  Get data from EP0 fifo buffer and call related routines to save them.
  \param
       bySubCmd: sub command of Vendor DBG command
          wAddr: operation address, may useless for some sub commands
            len: data_out length

  \retval None
*********************************************************************************************************
*/
U8 CmdNVolRWSdataOut(U8 bySubCmd,U32 dwAddr,U8 len)
{
	U8 i;
	U8 ret;

	for(i=0; i<len; i++)
	{
		XBYTE[SPI_BUFFER_BASE_ADDR+i]=EP0_DATA_OUT();
	}

	switch(bySubCmd)
	{
		case VSCMD_SPI_SF_W:
			{
				ret=SPI_WB_Program(dwAddr, 0x00, len);
				break;
			}
		case VSCMD_SPI_SF_WRSR:		
			{
#ifndef _ENABLE_OLT_				
				ret=SPI_WB_WriteStatus(XBYTE[SPI_BUFFER_BASE_ADDR]);
#endif				
				break;
			}
		default:
			{
				return (U8)VCMD_STS_UNKNOWN_SCMD;
			}
	}

	if(ret)
	{
		return (U8)VCMD_STS_RET_NO_ERR;
	}
	return (U8)VCMD_STS_HW_ERR;
}

U8 CmdNVolRWSDbgcmd(U8 bySubCmd,U16 wLen,U32 dwAddr)
{
	U8 ret= (U8)VCMD_STS_CMD;
 	dwAddr = dwAddr;

	switch(bySubCmd)
	{
	case VSCMD_SPI_SF_R:
	case VSCMD_SPI_SF_FAST_R:
	case VSCMD_SPI_SF_FAST_DUAL_OUT_R:
	case VSCMD_SPI_SF_FAST_DUAL_INOUT_R:
		{
			ret=(U8)VCMD_STS_DATA_IN;
			break;
		}
	case VSCMD_SPI_SF_RID:
		{
			if(wLen!=8 )
			{ret=(U8)VCMD_STS_ERR_LEN;}
			else
			{ret=(U8)VCMD_STS_DATA_IN;}
			break;
		}
	case VSCMD_SPI_SF_W:
		{
			ret=(U8)VCMD_STS_DATA_OUT;
			break;
		}
	case VSCMD_SPI_SF_RDSR:
		{
			if(wLen!=1 )
			{
				ret=(U8)VCMD_STS_ERR_LEN;
			}
			else
			{
				ret=(U8)VCMD_STS_DATA_IN;
			}
			break;
		}
	case VSCMD_SPI_SF_WRSR:
		{
			if(wLen!=1 )
			{
				ret=(U8)VCMD_STS_ERR_LEN;
			}
			else
			{
				ret=(U8)VCMD_STS_DATA_OUT;
			}
			break;
		}
	case VSCMD_SPI_SF_ERBLK:
		{
#ifndef _ENABLE_OLT_		
			if(!SPI_WB_EraceSFSector(dwAddr))
			{
				ret=(U8)VCMD_STS_HW_ERR;
			}
			else

			{
				ret=(U8)VCMD_STS_CMD;
			}
#endif				
			break;
		
		}
	default:
		{
			ret=(U8)VCMD_STS_UNKNOWN_SCMD;
			break;
		}
	}

	return ret;
}

/*
*********************************************************************************************************
						UVC Vendor ISP command  data in sub command data processing
* FUNCTION CmdISPSdataIn
*********************************************************************************************************
*/
/**
  UVC vendor ISP command processing
  fill the EP0 fifo buffer with the current control value of specified vendor control
  \param
       bySubCmd: sub command of Vendor isp command
            len: data_in length

  \retval None
*********************************************************************************************************
*/
U8 CmdISPSdataIn(U8 bySubCmd)
{
	switch(bySubCmd)
	{
	case SNR_SPECIAL_EFFECT_GET:
		{
			EP0_DATA_IN(g_bySnrEffect);
			break;
		}
		// hemonel 2010-05-05: add flip/mirror
	case SNR_IMG_DIR_GET:
		{
			EP0_DATA_IN(g_bySnrImgDir);
			break;
		}
	case VSCMD_FRAME_STS_GET:
		{
			EP0_DATA_IN(0);	//g_bySensorCurFormat	// hemonel 2010-08-27: getdevstatus has reported this value
			EP0_DATA_IN(0);
			EP0_DATA_IN(INT2CHAR(g_wSensorCurFormatWidth,0));
			EP0_DATA_IN(INT2CHAR(g_wSensorCurFormatWidth,1));
			EP0_DATA_IN(INT2CHAR(g_wSensorCurFormatHeight,0));
			EP0_DATA_IN(INT2CHAR(g_wSensorCurFormatHeight,1));
			EP0_DATA_IN(DUMMY_BYTE);
			EP0_DATA_IN(DUMMY_BYTE);
			break;
		}
#ifdef _LENOVO_JAPAN_PROPERTY_PAGE_
	case AWB_GAIN_SUPPORT: 
		{
			EP0_DATA_IN(1);
			break;
		}
	case ISP_AWB_GAIN_GET: 
		{
			EP0_DATA_IN(g_byAWBRGain_Lenovo);
			EP0_DATA_IN(0);
			EP0_DATA_IN(g_byAWBGGain_Lenovo);
			EP0_DATA_IN(0);
			EP0_DATA_IN(g_byAWBBGain_Lenovo);
			EP0_DATA_IN(0);			
			break;		
		}
#endif	
	default:
		{
			return (U8)VCMD_STS_UNKNOWN_SCMD;
		}
	}
	return (U8)VCMD_STS_RET_NO_ERR;
}

/*
*********************************************************************************************************
						UVC Vendor ISP command  data out sub command data processing
* FUNCTION CmdISPSdataOut
*********************************************************************************************************
*/
/**
  UVC vendor ISP command processing
  Get vendor specified control settings from ep0 fifo buffer, and make them effective.
  \param
       bySubCmd: sub command of Vendor isp command
            len: data_in length

  \retval None
*********************************************************************************************************
*/
U8 CmdISPSdataOut(U8 bysubCmd)
{
#ifdef _FACIAL_EXPOSURE_
	U8 bylowbyte,byhighbyte;
#endif
	switch(bysubCmd)
	{
	case SNR_SPECIAL_EFFECT_SET:
		{
			g_bySnrEffect=EP0_DATA_OUT();
#ifndef _USE_BK_SPECIAL_EFFECT_
#else
			SetBKSpecailEffect(g_bySnrEffect);
#endif
			break;
		}
		// hemonel 2010-05-05: add flip/mirror
	case SNR_IMG_DIR_SET:
		{
			g_bySnrImgDir = EP0_DATA_OUT();
			if(g_bySensorIsOpen == SENSOR_OPEN)
			{
				SetSensorImgDir(RollItem.Last^g_bySnrImgDir);
			}
			break;
		}
#ifdef _FACIAL_EXPOSURE_
		case VSCMD_AE_WINDOW_SET:
			{
				byhighbyte =  EP0_DATA_OUT();
				bylowbyte = EP0_DATA_OUT();
				ASSIGN_INT(g_wAE_WinStartX,(byhighbyte>>4)&0x0f,bylowbyte);
				bylowbyte = EP0_DATA_OUT();
				ASSIGN_INT(g_wAE_WinStartY,byhighbyte&0x0f,bylowbyte);
				byhighbyte =  EP0_DATA_OUT();
				bylowbyte = EP0_DATA_OUT();
				ASSIGN_INT(g_wAE_WinWidth,(byhighbyte>>4)&0x0f,bylowbyte);
				bylowbyte = EP0_DATA_OUT();
				ASSIGN_INT(g_wAE_WinHeight,byhighbyte&0x0f,bylowbyte);
				g_bySimilar_last = g_bySimilar;
				g_bySimilar = EP0_DATA_OUT();
				g_byFaceExposure_En= EP0_DATA_OUT();
				g_byAEFaceWindowSet = 1;
				break;
			}
#endif
#ifdef _LENOVO_JAPAN_PROPERTY_PAGE_
	case ISP_AWB_GAIN_SET:
			{
				U16 wRgain, wGgain, wBgain;

				g_byAWBRGain_Lenovo =  EP0_DATA_OUT();
				EP0_DATA_OUT();
				g_byAWBGGain_Lenovo =  EP0_DATA_OUT();
				EP0_DATA_OUT();
				g_byAWBBGain_Lenovo =  EP0_DATA_OUT();
				EP0_DATA_OUT();				


				if (WhiteBalanceTempAutoItem.Last == 0)
				{
					wRgain = LinearIntp_Word(0, 256, 255, 2047, g_byAWBRGain_Lenovo);
					wGgain = LinearIntp_Word(0, 256, 255, 2047, g_byAWBGGain_Lenovo);				
					wBgain = LinearIntp_Word(0, 256, 255, 2047, g_byAWBBGain_Lenovo);
				
					SetBKAWBGain(wRgain, wGgain, wBgain, 0);
				}	
				break;
			}
#endif
	default:
		{
			return (U8)VCMD_STS_UNKNOWN_SCMD;
		}
	}
	return (U8)VCMD_STS_RET_NO_ERR;
}


U8 CmdISPSDbgcmd(U8 bySubCmd,U16 wLen)
{
	U8 ret= (U8)VCMD_STS_CMD;
	switch(bySubCmd)
	{

//		case	 VSCMD_JPEG_QUALITY_GET:
	case SNR_SPECIAL_EFFECT_GET:
	case SNR_IMG_DIR_GET:	// hemonel 2010-05-05: add flip/mirror
//		case VSCMD_VDWIN_EN_GET:
		{
			if(wLen!=1 )
			{
				ret=(U8)VCMD_STS_ERR_LEN;
			}
			else
			{
				ret=(U8)VCMD_STS_DATA_IN;
			}
			break;
		}
	case VSCMD_FRAME_STS_GET:
		{
			if(wLen!=8 )
			{
				ret=(U8)VCMD_STS_ERR_LEN;
			}
			else
			{
				ret=(U8)VCMD_STS_DATA_IN;
			}
			break;
		}
//		case VSCMD_VDWIN_ORG_SIZE_GET:
//		case VSCMD_VDWIN_POS_GET:
		//		{
		//			if(wLen!=4 )
		//			{ret=VCMD_STS_ERR_LEN;}
		//			else
		//			{ret=VCMD_STS_DATA_IN;}
		//			break;
		//		}
//		case VSCMD_JPEG_QUALITY_SET:
#ifdef _FACIAL_EXPOSURE_
	case VSCMD_AE_WINDOW_SET:
		{
			if(wLen!=8 )
			{
				ret=VCMD_STS_ERR_LEN;
			}
			else
			{
				ret=VCMD_STS_DATA_OUT;
			}
			break;
		}
#endif
	case SNR_SPECIAL_EFFECT_SET:
	case SNR_IMG_DIR_SET:	// hemonel 2010-05-05: add flip/mirror
		{
			if(wLen!=1 )
			{
				ret=(U8)VCMD_STS_ERR_LEN;
			}
			else
			{
				ret=(U8)VCMD_STS_DATA_OUT;
			}
			break;
		}
#ifdef _LENOVO_JAPAN_PROPERTY_PAGE_
	case ISP_AWB_GAIN_SET:	
		{
			if(wLen!=6 )
			{
				ret=(U8)VCMD_STS_ERR_LEN;
			}
			else
			{
				ret=(U8)VCMD_STS_DATA_OUT;
			}
			break;
		}
	case ISP_AWB_GAIN_GET:
		{
			if(wLen!=6 )
			{
				ret=(U8)VCMD_STS_ERR_LEN;
			}
			else
			{
				ret=(U8)VCMD_STS_DATA_IN;
			}
			break;
		}
	case AWB_GAIN_SUPPORT:
		{
			if(wLen!=1 )
			{
				ret=(U8)VCMD_STS_ERR_LEN;
			}
			else
			{
				ret=(U8)VCMD_STS_DATA_IN;
			}
			break;
		}
#endif
	default:
		{
			ret=(U8)VCMD_STS_UNKNOWN_SCMD;
			break;
		}

	}
	return ret;
}

U8 CmdOtherSDbgcmd(U8 bySubCmd,U16 wLen)
{
    U8 byRet= VCMD_STS_CMD;

		wLen = wLen;
	
    switch(bySubCmd)
    {
    	case PREVIEW_LED_OFF:
        {
            g_byVDCMDPVLEDOff = 1;            
            break;
        }
#ifdef _LENOVO_IDEA_EYE_
		case VSCMD_MTD_SENSITIVITY_SET:
			if (wLen!=3 )
			{
				byRet = (U8)VCMD_STS_ERR_LEN;
			}
			else
			{
				byRet = (U8)VCMD_STS_DATA_OUT;
			}
			break;
		case VSCMD_MTD_SENSITIVITY_GET:
			if (wLen!=3 )
			{
				byRet = (U8)VCMD_STS_ERR_LEN;
			}
			else
			{
				byRet = (U8)VCMD_STS_DATA_IN;
			}
			break;
		case VSCMD_BACKGROUND_MTD_SET:
			if (wLen!=3)
			{
				byRet = (U8)VCMD_STS_ERR_LEN;
			}
			else
			{
				byRet = (U8)VCMD_STS_DATA_OUT;
			}
			break;
		case VSCMD_CIRCUM_STATUS:
			if (wLen!=1 )
			{
				byRet = (U8)VCMD_STS_ERR_LEN;
			}
			else
			{
				byRet = (U8)VCMD_STS_DATA_IN;
			}
			break;
		case VSCMD_BACKGROUND_MTD_STOP:
			StopBackgroundStreaming();
			break;
#endif
        default:
        {
            byRet=VCMD_STS_UNKNOWN_SCMD;
            break;
        }
    }

    return byRet;
}


// hemonel 2011-04-26: delete for optimize code because customer do not use
/*
U8 CmdCustomerSdataIn(U8 bySubCmd,U32 dwAddr,U8 bylen)
{
	U8 i;
	U8 byDataBuf[8];

	if( CusXuCmdReadData(bySubCmd, dwAddr, bylen, byDataBuf))
	{
		for(i=0; i<bylen; i++)
		{
			EP0_DATA_IN(byDataBuf[i]);
		}
		return VCMD_STS_RET_NO_ERR ;
	}
	else
	{
		return VCMD_STS_HW_ERR;
	}
}

U8 CmdCustomerSdataOut(U8 bySubCmd,U32 dwAddr,U8 bylen)
{
	U8 i;
	U8 byDataBuf[8];

	// move usb data to buffer
	for(i=0;i<bylen;i++)
	{
		byDataBuf[i] = EP0_DATA_OUT();
	}

	if( CusXuCmdWriteData(bySubCmd, dwAddr, bylen, byDataBuf))
	{
		return VCMD_STS_RET_NO_ERR ;
	}
	else
	{
		return VCMD_STS_HW_ERR;
	}
}

U8 CmdCustomerSDbgcmd(U8 bySubCmd,U32 dwAddr, U16 wLen)
{
	U8 ret;
	U8 byCmdType;

	byCmdType = bySubCmd&0xC0;
	if(byCmdType == (SCMD_DATA_VLD|SCMD_DIR_IN))
	{
		// type 1 command: read data command
		if(wLen ==0 )
		{
			ret=VCMD_STS_ERR_LEN;
		}
		else
		{
			ret=VCMD_STS_DATA_IN;
		}
	}
	else if(byCmdType == (SCMD_DATA_VLD|SCMD_DIR_OUT))
	{
		// type 2 command: write data command
		if(wLen ==0 )
		{
			ret=VCMD_STS_ERR_LEN;
		}
		else
		{
			ret=VCMD_STS_DATA_OUT;
		}
	}
	else
	{
		// type 3: no data command
		CusXuCmd(bySubCmd, dwAddr, wLen);
		ret=VCMD_STS_CMD;
	}

	return ret;
}
*/


#ifdef _LENOVO_IDEA_EYE_

U8 CmdIPCAMdataIn(U8 bySubCmd, U32 dwAddr,U8 byLen)
{
	dwAddr = dwAddr;
	byLen = byLen;
	switch(bySubCmd)
	{
		case VSCMD_MTD_SENSITIVITY_GET:
			EP0_DATA_IN(g_byMTDWinNCent);
			EP0_DATA_IN(g_byMTDSensCent);
			EP0_DATA_IN(0);			
			break;		
		case VSCMD_CIRCUM_STATUS:
			if((g_byMTDDetectBackend) && g_byMTDWinDetected)
			{
				g_byMTDWinDetected = 0;
				EP0_DATA_IN(0x02);	
			}
			else
			{
				EP0_DATA_IN(0x00);	
			}
			break;
		default:
			{
				return (U8)VCMD_STS_UNKNOWN_SCMD;
			}
	}
	return (U8)VCMD_STS_RET_NO_ERR;	
}

U8 CmdIPCAMdataOut(U8 bySubCmd, U32 dwAddr, U8 byLen)
{
	dwAddr = dwAddr;	
	byLen = byLen;

	switch(bySubCmd)
	{
		case VSCMD_MTD_SENSITIVITY_SET:
			{
				g_byMTDSensCent = EP0_DATA_OUT();
				g_byMTDWinNCent = EP0_DATA_OUT();			
				EP0_DATA_OUT();

				IDEAEYE_MSG(("SP:%bu %bu\n", g_byMTDSensCent, g_byMTDWinNCent));

				if (g_byMTDSensCent<=0 || g_byMTDSensCent>100 ||
					g_byMTDWinNCent<=0 || g_byMTDWinNCent>100)
				{
					break;
				}

			}	
			break;				
		case VSCMD_BACKGROUND_MTD_SET:
			EP0_DATA_OUT();
			EP0_DATA_OUT();
			EP0_DATA_OUT();
			StartBackgroundStreaming();
			break;
		default:	
			return (U8)VCMD_STS_UNKNOWN_SCMD;
	}
	return (U8)VCMD_STS_RET_NO_ERR;	
}
#endif


/*
*********************************************************************************************************
*											UVC Vendor command processing
* FUNCTION VendorCmdProc2
*********************************************************************************************************
*/
/**
  UVC vendor command processing
  \param
        CS:control selector,must be EXU1_CMD_STS or EXU1_DATA_INOUT
        req: uvc control requset , must be GET_CUR or SET_CUR

  \retval None
*********************************************************************************************************
*/
void VendorCmdProc(U8 CS,U8 req,U8 ReqLen)
{
	static U16 wLen;
	static U8 byCmd;
	static U8 bySubCmd;
	static U32 dwAddr;
	static U8 CmdStatus = (U8)VCMD_STS_CMD;
	U8 data bytmp;
	U8 bytmp2;
	U8 i;

	ASSERT((CS==EXU1_CMD_STS)||(CS==EXU1_DATA_INOUT));
	ASSERT((req==GET_CUR)||(req==SET_CUR));

	if(CS==EXU1_CMD_STS)  //command, status control size always eq 8
	{
		if(req==GET_CUR) //get cmd status, come here anytime.
		{
			EP0_FFFLUSH();
			EP0_DATA_IN(CmdStatus);
			EP0_DATA_IN(g_byDataInOutCtlSize);
			for(bytmp=2; bytmp<8; bytmp++)
			{
				EP0_DATA_IN(DUMMY_BYTE);
			}
			EP0_FFV_HSK();
		}
		else //SET_CUR     set command
		{
			byCmd=EP0_DATA_OUT(); //offset 0 cmd
			bySubCmd=EP0_DATA_OUT(); // offset 1 sub cmd

			if(byCmd==VCMD_RST)// RESET CMD STATUS.
			{
				CmdStatus=(U8)VCMD_STS_CMD;
				EP0_HSK();
				return;
			}

			if(CmdStatus!=(U8)VCMD_STS_CMD) // not in CMD state, stall
			{
				VCSTALL_EPCTL(VC_ERR_WRNGSTAT);
				CmdStatus=(U8)VCMD_STS_NOT_FINISH;
				return;
			}
			// offset 2,3  address, low word.
			dwAddr=EP0_DATA_OUT();
			bytmp=EP0_DATA_OUT();
			dwAddr|=((U32)bytmp)<<8;
			// offset 4,5 data length
			wLen = EP0_DATA_OUT();
			bytmp = EP0_DATA_OUT();
			wLen |=((U16)bytmp)<<8;
			//offset 6,7 address, high word
			bytmp=EP0_DATA_OUT();
			dwAddr |= ((U32)bytmp)<<16;
			bytmp=EP0_DATA_OUT();
			dwAddr |= ((U32)bytmp)<<24;

			EP0_FFFLUSH();

			switch(byCmd)
			{
			case VCMD_DBG:
			case VCMD_ISP:
			case VCMD_NVOL_RW:
		    case VCMD_OTHER: 				
				//	case VCMD_CUSTOMER:	// hemonel 2011-04-26: delete for optimize code because customer do not use
				if(byCmd==VCMD_DBG)
				{
					CmdStatus = CmdDbgSDbgcmd(bySubCmd, wLen,(U8)dwAddr);
				}
				else if( byCmd==VCMD_ISP)
				{
					CmdStatus = CmdISPSDbgcmd(bySubCmd, wLen);
				}
				else if( byCmd==VCMD_OTHER)
				{
					CmdStatus = CmdOtherSDbgcmd(bySubCmd, wLen);
				}				
				// hemonel 2011-04-26: delete for optimize code because customer do not use
				//	else if(byCmd==VCMD_CUSTOMER)
				//	{
				//		CmdStatus = CmdCustomerSDbgcmd(bySubCmd, dwAddr, wLen);
				//	}
				else// if(byCmd==VCMD_NVOL_RW)
				{
					CmdStatus = CmdNVolRWSDbgcmd(bySubCmd, wLen, dwAddr);
				}


				if((CmdStatus ==(U8)VCMD_STS_CMD)
				        ||(CmdStatus ==(U8)VCMD_STS_DATA_IN)
				        ||(CmdStatus ==(U8)VCMD_STS_DATA_OUT))
				{
					break;
				}
				else
				{
					VCSTALL_EPCTL(VC_ERR_WRNGSTAT);
					return ;
				}
				//break;
			default:
				{
					VCSTALL_EPCTL(VC_ERR_WRNGSTAT);
					CmdStatus=(U8)VCMD_STS_UNKNOWN_CMD;
					return;
				}

			}
			EP0_FFFLUSH();
			EP0_HSK();
		}
	}
	else //EXU1_DATA_INOUT
	{
		if(wLen <= ReqLen)
		{
			bytmp=(U8)wLen;
		}
		else //wLen>8
		{
			bytmp=ReqLen;
		}
		if(req==GET_CUR) //in data
		{
			EP0_FFFLUSH();
			//DBG(("@13 ep0_bc=%bx\n",XBYTE[EP0_BC]));
			if(CmdStatus != (U8)VCMD_STS_DATA_IN) // not in data in state , just stall
			{
				VCSTALL_EPCTL(VC_ERR_WRNGSTAT);
				CmdStatus=(U8)VCMD_STS_ERR_DIR;
				return;
			}

			//data in /out function.
			switch(byCmd)
			{
			case VCMD_DBG:
			case VCMD_ISP:
			case VCMD_NVOL_RW:
			case VCMD_OTHER:		
				//	case VCMD_CUSTOMER: // hemonel 2011-04-26: delete for optimize code because customer do not use
				{
					if((bySubCmd&SCMD_DATA_VLD)&&(SCMD_DIR_IN==(bySubCmd&SCMD_DIR_MSK)))//data in
					{
						if(byCmd==VCMD_DBG)
						{
							bytmp2=CmdDbgSdataIn(bySubCmd, (U16)dwAddr, bytmp);
						}
						else if(byCmd==VCMD_ISP)
						{
							bytmp2=CmdISPSdataIn(bySubCmd);
						}
#ifdef _LENOVO_IDEA_EYE_	
						else if(byCmd == VCMD_OTHER)
						{
							bytmp2=CmdIPCAMdataIn(bySubCmd, dwAddr,bytmp);							
						}
#endif						
						// hemonel 2011-04-26: delete for optimize code because customer do not use
						//	else if(byCmd==VCMD_CUSTOMER)
						//	{
						//		bytmp2=CmdCustomerSdataIn(bySubCmd, dwAddr, bytmp);
						//	}
						else// if(byCmd==VCMD_NVOL_RW)
						{
							bytmp2=CmdNVolRWSdataIn(bySubCmd,dwAddr,bytmp);
						}

						if((U8)VCMD_STS_RET_NO_ERR!=bytmp2)
						{
							VCSTALL_EPCTL(VC_ERR_WRNGSTAT);
							CmdStatus=bytmp2;
							return;
						}
					}
					else
					{
						VCSTALL_EPCTL(VC_ERR_WRNGSTAT);
						CmdStatus=(U8)VCMD_STS_ERR_DIR;
						return;
					}
					break;
				}
			case VCMD_RST:
				{
					VCSTALL_EPCTL(VC_ERR_WRNGSTAT);
					CmdStatus=(U8)VCMD_STS_ERR_DIR;
					return;
				}
			default:
				{
					VCSTALL_EPCTL(VC_ERR_WRNGSTAT);
					CmdStatus=(U8)VCMD_STS_UNKNOWN_CMD;
					return;
				}
			}

			for(i=bytmp; i<ReqLen; i++)// fill other pads with dummy bytes
			{
				EP0_DATA_IN(DUMMY_BYTE);
			}
			EP0_FFV_HSK();
		}
		else // SET_CUR  out data
		{
			if(CmdStatus!=(U8)VCMD_STS_DATA_OUT)
			{
				VCSTALL_EPCTL(VC_ERR_WRNGSTAT);
				CmdStatus=(U8)VCMD_STS_ERR_DIR;
				return;
			}
			switch(byCmd)
			{
			case VCMD_DBG:
			case VCMD_ISP:
			case VCMD_NVOL_RW:
#ifdef _LENOVO_IDEA_EYE_					
			case VCMD_OTHER:
#endif				
				//	case VCMD_CUSTOMER:	// hemonel 2011-04-26: delete for optimize code because customer do not use
				{
					if((bySubCmd&SCMD_DATA_VLD)&&(SCMD_DIR_OUT==(bySubCmd&SCMD_DIR_MSK)))//data out
					{
						if(byCmd==VCMD_DBG)
						{
							bytmp2=CmdDbgSdataOut(bySubCmd, (U16)dwAddr, bytmp);
						}
						else if(byCmd==VCMD_ISP)
						{
							bytmp2=CmdISPSdataOut(bySubCmd/*, bytmp*/);
						}
#ifdef _LENOVO_IDEA_EYE_							
						else if(byCmd == VCMD_OTHER)
						{
							bytmp2=CmdIPCAMdataOut(bySubCmd, dwAddr, bytmp);
						}
#endif						
						// hemonel 2011-04-26: delete for optimize code because customer do not use
						//	else if(byCmd ==VCMD_CUSTOMER)
						//	{
						//		bytmp2=CmdCustomerSdataOut(bySubCmd, dwAddr, bytmp);
						//	}
						else// if(byCmd==VCMD_NVOL_RW)
						{
							bytmp2=CmdNVolRWSdataOut(bySubCmd, dwAddr,bytmp);
						}

						if((U8)VCMD_STS_RET_NO_ERR!=bytmp2)
						{
							VCSTALL_EPCTL(VC_ERR_WRNGSTAT);
							CmdStatus=bytmp2;
							return;
						}
					}
					else
					{
						VCSTALL_EPCTL(VC_ERR_WRNGSTAT);
						CmdStatus=(U8)VCMD_STS_ERR_DIR;
						return;
					}
					break;
				}
			case VCMD_RST:
				{
					VCSTALL_EPCTL(VC_ERR_WRNGSTAT);
					CmdStatus=(U8)VCMD_STS_ERR_DIR;
					return;
				}
			default:
				{
					VCSTALL_EPCTL(VC_ERR_WRNGSTAT);
					CmdStatus=(U8)VCMD_STS_UNKNOWN_CMD;
					return;
				}

			}
			EP0_FFFLUSH();
			EP0_HSK();
		}
		wLen   -= bytmp;
		dwAddr += bytmp;

		if(wLen==0)// last data segment, change command status.
		{
			CmdStatus=(U8)VCMD_STS_CMD;
		}
	}
	return;
}

