#ifndef _CAMSFR_H_
#define _CAMSFR_H_

U8 SFRRead(U8 byAddr);
void SFRWrite(U8 byAddr, U8 byData);

#endif
