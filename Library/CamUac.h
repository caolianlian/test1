#ifndef _CAMUAC_H_
#define _CAMUAC_H_

//Rabi 20090615
#define _UAC_VER_1V0 0x0100
#define _UAC_VER_2V0 0x0200

/*
*--------------------------------------------------------------------------
* UAC constants
*----------------------------------------------------------------------------
*/
#define VOLUME_DEF 	0x0500 // 5db
#define VOLUME_MAX 	0x0C00 //12db
#define VOLUME_MIN	0xF400 //0db 		2010-09-02 darcy_lu: set min volume to 0 dB for win7 test
#define VOLUME_RES	0x0100// 1db/step
#define VOLUME_CH_LEFT	0x00
#define VOLUME_CH_RIGHT	0x01
#define VOLUME_MUTE			TRUE
#define VOLUME_NOT_MUTE	FALSE

//UAC constants 
//#define DEV_CLS_MULTI              0xEF
//#define DEV_SUBCLS_COMMON 0x02
//#define DEV_PROTOCOL_IAD      0x01
//#define  USB_IAD_DESCRIPTOR_TYPE 0x0b
// Interface idx
#define  IF_IDX_AUDIOCONTROL     0x02
#define  IF_IDX_AUDIOSTREAMING 0x03

#define  ALT_SETTING_IDX_22K05_16BIT		0x01
#define  ALT_SETTING_IDX_32K_16BIT			0x02
#define  ALT_SETTING_IDX_48K_16BIT_MONO		0x03
#define  ALT_SETTING_IDX_48K_16BIT			0x04
#define  ALT_SETTING_IDX_96K_16BIT			0x05
#define  ALT_SETTING_IDX_44K1_24BIT		0x06
#define  ALT_SETTING_IDX_48K_24BIT			0x07
#define  ALT_SETTING_IDX_96K_24BIT			0x08
#define  UAC_HS_ALT_SETTING_NUM		0x08
#define  UAC_FS_ALT_SETTING_NUM		0x04
//Endpoint idx
#define EP_IDX_AUDIO_INT             0x04
#define EP_IDX_AUDIO_STREAM     0x02

/* A.1 Audio Interface Class Code */
#define CC_AUDIO 0x01

/*A.2 Audio Interface Subclass codes */
#define SC_UNDEFINED 	0x00
#define SC_AUDIOCONTROL 		0x01
#define SC_AUDIOSTREAMING		0x02
#define SC_MIDISTREAMING		0x03

/*A.3 Audio Interface Protocol Codes */
#define PR_PROTOCAL_UNDEFINED 0x00

/*A.4 Audio Class-Specific Descriptor Types */
#define CS_UNDEFINED		0x20
#define CS_DEVICE			0x21
#define CS_CONFIGURATION 0x22
#define CS_STRING		0x23
//#define CS_INTERFACE		0x24
//#define CS_ENDPOINT		0x25

/*A.5 Audio Class-Specific AC Interface Descriptor Subtypes*/
#define AC_DESCRIPTOR_UNDEFINED  0x00
#define AC_HEADER					0x01
#define AC_INPUT_TERMINAL			0x02
#define AC_OUTPUT_TERMINAL			0x03
#define AC_MIXER_UNIT				0x04
#define AC_SELECTOR_UNIT			0x05
#define AC_FEATURE_UNIT				0x06
#define AC_PROCESSING_UNIT			0x07
#define AC_EXTENSION_UNIT			0x08

/*A.6 Audio Class-Specific AS Interface Descriptor Subtypes*/
#define AS_DESCRIPTOR_UNDEFINED 0x00
#define AS_GENERAL				0x01
#define AS_FORMAT_TYPE				0x02
#define AS_FORMAT_SPECIFIC			0x03

/*A.7 Processing Unit Process Types */
#define PROCESS_UNDEFINED		0x00
#define UP_DOWN_MIX_PROCESS	0x01
#define DOLBY_PROLOGIC_PROCESS	0x02
#define _3D_STEREO_EXTENDER_PROCESS 0x03
#define REVERBERATION_PROCESS	0x04
#define CHORUS_PROCESS			0x05
#define DYN_RANGE_COMP_PROCESS	0x06

/*A.8 Audio Class-Specific Endpoint Descriptor Subtypes*/
#define DESCRIPTOR_UNDEFINED	0x00
#define EP_GENERAL				0x01

/*A.9 Audio Class-Specific Request Codes*/
#define REQUEST_CODE_UNDEFINED	0x00
#define SET_CUR					0x01
#define GET_CUR					0x81
#define SET_MIN					0x02
#define GET_MIN					0x82
#define SET_MAX					0x03
#define GET_MAX					0x83
#define SET_RES					0x04
#define GET_RES					0x84
#define SET_MEM					0x05
#define GET_MEM					0x85
#define GET_STAT					0xFF

/*A.10 Control Selector Codes */
/*A.10.1 Terminal Control Selectors */
#define TE_CONTROL_UNDEFINED 0x00
#define COPY_PROTECT_CONTROL 0x01

/*A.10.2 Feature Unit Control Selectors */
#define FU_CONTROL_UNDEFINED  	0x00
#define MUTE_CONTROL			0x01
#define VOLUME_CONTROL			0x02
#define BASS_CONTROL				0x03
#define MID_CONTROL				0x04
#define TREBLE_CONTROL			0x05
#define GRAPHIC_EQUALIZER_CONTROL 0x06
#define AUTOMATIC_GAIN_CONTROL	0x07
#define DELAY_CONTROL			0x08
#define BASS_BOOST_CONTROL		0x09
#define LOUDNESS_CONTROL		0x0A

/*A.10.3 Processing Unit Control Selectors */

/*A.10.4 Extension Unit Control Selectors*/
#define XU_CONTROL_UNDEFINED	0x00
#define XU_ENABLE_CONTROL		0x01

/*A.10.5 Endpoint Control Selectors */
#define EP_CONTROL_UNDEFINED	0x00
#define SAMPLING_FREQ_CONTROL	0x01
#define PITCH_CONTROL			0x02

/*Constant */
#define SAMPLING_FREQUENCY_96KHZ	0x017700
#define SAMPLING_FREQUENCY_48KHZ	0x00BB80
#define SAMPLING_FREQUENCY_24KHZ	0x005DC0
#define SAMPLING_FREQUENCY_16KHZ	0x003E80
#define SAMPLING_FREQUENCY_32KHZ	0x007D00
#define SAMPLING_FREQUENCY_44K1HZ	0x00AC44
#define SAMPLING_FREQUENCY_22K05HZ	0x005622
#define SAMPLING_FREQUENCY_11K025HZ	0x002B11

#define ID_TERMINAL_IT		0x01
#define ID_TERMINAL_OT		0x02
#define ID_FU					0x03

/*USB Device Class Definition for Terminal Types  v1.0*/
#define TT_USB_UNDEFINED 		0x0100
#define TT_USB_STREAMING			0x0101
#define TT_USB_VENDOR_SPECIFIC	0x01FF
#define TT_IT_INPUT_UNDEFINED		0x0200
#define TT_IT_MICROPHONE			0x0201
#define TT_IT_MICROPHONE_ARRAY	0x0205

/*USB Device Class Defnition for Audio Data Format v1.0*/
#define AD_FORMAT_TYPE_UNDEFINED	0x00
#define AD_FORMAT_TYPE_I				0x01
#define AD_FORMAT_TYPE_II			0x02
#define AD_FORMAT_TYPE_III			0x03

/*Audio Data Format Type I Codes */
#define TYPE_I_UNDEFINED			0x0000
#define TYPE_I_PCM				0x0001
#define TYPE_I_PCM8				0x0002
#define TYPE_I_IEEE_FLOAT			0x0003
#define TYPE_I_ALAW				0x0004
#define TYPE_I_MULAW				0x0005

/*Audio Data Format Type II Codes */
#define TYPE_II_UNDEFINED			0x1000
#define TYPE_II_MPEG				0x1001
#define TYPE_II_AC3				0x1002

/*Audio Data Format Type III Codes */
#define TYPE_III_UNDEFINED		0x2000
#define TYPE_III_IEC1937_AC3		0x2001
#define TYPE_III_IEC1937_MPEG1_LAYER1		0x2002
#define TYPE_III_IEC1937_MPEG2_NOEXT		0x2003
#define TYPE_III_IEC1937_MPEG2_EXT		0x2004
#define TYPE_III_IEC1937_MPEG2_LAYER1_LS	0x2005
#define TYPE_III_IEC1937_MPEG2_LAYER2_3_LS 0x2006

#define UAC_MUTE_CTRL	0x01
#define UAC_LOUDNEDD_CTRL	0x02
#define UAC_AGC_CTRL	0x04
/*

//          ===>IAD Descriptor<===
U8 code AudioIADDesc[]=
{
	DESC_SIZE_IAD, //bLength
	USB_IAD_DESCRIPTOR_TYPE,//bDescriptorType
	IF_IDX_AUDIOCONTROL, //bFirstInterface
	0x02, //bInterfaceCount
	CC_AUDIO, //bFunctionClass;
	SC_AUDIOSTREAMING,//bFunctionSubClass
	PR_PROTOCAL_UNDEFINED, //bFunctionProtocol
	0x00 //iFunction
};

//          ===>Audio Control Interface Descriptor<===
U8 code AudioControlIfDesc[]=
{
	0x09,//bLength
	USB_INTERFACE_DESCRIPTOR_TYPE,//bDescriptorType
	IF_IDX_AUDIOCONTROL, //bInterfaceNumber
	0x00, //bAlternateSetting;
	0x00,//bNumEndpoints
	CC_AUDIO,//bInterfaceClass
	SC_AUDIOCONTROL,//bInterfaceSubClass
	PR_PROTOCAL_UNDEFINED,//bInterfaceProtocal
	0x00// iInterface
};

//          ===>Audio Control Interface Header Descriptor<===
U8 code AudioControlIfHeaderDesc[]=
{
	0x09, //bLength
	CS_INTERFACE, //bDescriptorType
	AC_HEADER, //bDescriptorSubType
	_UAC_VER_1V0%256, //bcdADC, UAC spec1.0
	_UAC_VER_1V0/256,
	0x27,
	0x00,// wTotalLength=0x27, Header + IT +OT +FU length
	0x01,//bInCollection, Number of streaming interfaces.
	IF_IDX_AUDIOSTREAMING //baInterfaceNr[1], AS IF 3 belongs to this AC IF
};

//          ===>Audio Control Interface Input Terminal Descriptor<===
U8 code AudioControlITDesc[]=
{
	0x0C,//bLength
	CS_INTERFACE,//bDescriptor Type
	AC_INPUT_TERMINAL,//bDescriptorSub Type
	ID_TERMINAL_IT, //bTerminalID
	TT_IT_MICROPHONE%256,
	TT_IT_MICROPHONE/256, //wTermialType
	0x00, //bAssocTerminal
	0x02,// bNrChannels, 2 channel
	0x03,
	0x00, //wChannelsConfig, Left front/Right front
	0x00, //iChannelNames
	0x00	//iTerminal
};

//          ===>Audio Control Interface Output Terminal Descriptor<===
U8 code AudioControlOTDesc[]=
{
	0x09, //bLength
	CS_INTERFACE, //bDescriptor Type
	AC_OUTPUT_TERMINAL, //bDescriptorSubtype
	ID_TERMINAL_OT, //bTerminalID
	TT_USB_STREAMING%256,
	TT_USB_STREAMING/256,//wTerminalType
	ID_TERMINAL_IT, //bAssocTermial, identifying the Input Terminal to which this Output Terminal is associated.
	ID_FU, //bSourceID
	0x00 //iTerminal
};

//          ===>Audio Control Interface Feature Unit Descriptor<===
U8 code AudioControlFUDesc[]=
{
	0x09, //bLength
	CS_INTERFACE, //bDescriptorType
	AC_FEATURE_UNIT, //bDescriptorSubtype
	ID_FU, //bUnitID
	ID_TERMINAL_IT,//bSourceID
	0x01, //bControlSize, in bytes of an element of the bmaControls array
	0x03, //bmaControls[0], master channel 0
		//D0 : Mute
		//D1: Volume
		//D2: Bass
		//D3: Mid
		//D4: Treble
		//D5: Graphic Equalizer
		//D6: Automatic Gain
		//D7: Delay
		//...
	0x03,//bmaControls[1], master channel 1
	0x00
};

//          ===>Audio Streaming Interface Descriptor<===
U8 code AudioStreamingIfDesc_AltSet0[]=
{
	0x09, //bLength
	USB_INTERFACE_DESCRIPTOR_TYPE, //bDescriptorType
	IF_IDX_AUDIOSTREAMING, //bInterfaceNumber
	0x00, //bAlternateSetting
	0x00,//bNumEndpoints
	CC_AUDIO, //bInterfaceClass
	SC_AUDIOSTREAMING, //bInterfaceSubClass
	PR_PROTOCAL_UNDEFINED, //bInterfaceProtocol
	0x00, //iInterface
};
U8 code AudioStreamingIfDesc_AltSet1[]=
{
	0x09, //bLength
	USB_INTERFACE_DESCRIPTOR_TYPE, //bDescriptorType
	IF_IDX_AUDIOSTREAMING, //bInterfaceNumber
	0x00, //bAlternateSetting
	0x01,//bNumEndpoints, 1 iso endpoint
	CC_AUDIO, //bInterfaceClass
	SC_AUDIOSTREAMING, //bInterfaceSubClass
	PR_PROTOCAL_UNDEFINED, //bInterfaceProtocol
	0x00, //iInterface
};

//          ===>Audio Streaming Class Specific Interface Descriptor<===
U8 code AudioStreamingCsIfDesc[]=
{
	0x07, //bLength
	CS_INTERFACE, //bDescriptorType
	AS_GENERAL,//bDescriptorSubtype
	ID_TERMINAL_OT,//bTerminalLink, Unit ID of the Output Terminal
	0x01,//bDelay, Inter Channel Synchroniztion, number of frames
	TYPE_I_PCM %256,
	TYPE_I_PCM /256,//wForamtTag
};

//          ===>Audio Streaming Format Type Descriptor<===
U8 code AudioStreamingFTDesc[]=
{
	0x0B, //bLength
	CS_INTERFACE,//bDescripterType
	AS_FORMAT_TYPE,//bDescripterSubtype
	AD_FORMAT_TYPE_I,//bFormatType
	0x02,//bNrChannels ,2 channel
	0x02,//bSubFrameSize, 2 bytes per audio subframe
	0x10,//bBitResolution, 16bit resolution
	0x01,//bSamFreqType, One frequency supported
	SAMPLING_FREQUENCY_48KHZ%256,
	(SAMPLING_FREQUENCY_48KHZ/256)%256,
	SAMPLING_FREQUENCY_48KHZ/256/256//tSamFreq, 3bytes
};

//          ===>Audio Streaming Standard EP Descriptor<===
U8 code AudioStreamingEpDesc[]=
{
	0x09,//bLength
	USB_ENDPOINT_DESCRIPTOR_TYPE,//bDescriptroType
	0x80|EP_IDX_AUDIO_STREAM,//bEndpointAddress, Direction:IN, EP:2
	0x05,	//bmAttributes:    -> Isochronous Transfer Type
			//Sync Type = Async.  Can be sync with Video Streaming?
	0xC0,
	0x00, //wMaxPacketSize , 192bytes per packet
	0x04,//bInterval, 1 packet per 8 uFrame --->192bytes/1ms
	0x00,//bRefresh, unused
	0x00 //bSynchAddress, unused. Can be sync with EP_IDX_VIDEO_STREAM?
};

//          ===>Audio Streaming Class Specific Audio data EP Descriptor<===
U8 code AudioStreamingCsEpDesc[]=
{
	0x07, //bLength
	CS_ENDPOINT,//bDescriptorType
	AS_GENERAL,//bDescriptorSubType
	0x01,//bmAttributes, Samping Freq Supported?
	0x00,//bLockDelayUnits
	0x00,
	0x00 //wLockDelay
};


*/
#endif
