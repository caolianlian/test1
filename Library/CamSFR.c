#include "pc_cam.h"
#include "camutil.h"
#include "camsfr.h"

// Sample code
// If you definition new sfr variable,you should add address here 
// Both SFRRead and SFRWrite

#ifdef _CACHE_MODE_

#ifdef _SFR_ACCESS_
U8 SFRRead(U8 byAddr)
{
	switch(byAddr)
	{
		case 0x93:
#pragma asm
			mov R7,0x93
#pragma endasm		
			break;	
		case 0x94:
#pragma asm
			mov R7,0x94
#pragma endasm		
			break;	
		default:
#pragma asm
			mov R7,#0xBB		//Retrun 0xBB as error warning
#pragma endasm		
			break;
	}
#pragma asm
			RET
#pragma endasm
}

void SFRWrite(U8 byAddr, U8 byData)
{
	byData = byData;
	
	switch(byAddr)
	{
		case 0x93:
#pragma asm
			mov 0x93,R5
#pragma endasm		
			break;	
		case 0x94:
#pragma asm
			mov 0x94,R5
#pragma endasm		
			break;
		default:
			break;
	}
}

#endif
#endif

