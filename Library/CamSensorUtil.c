#include "pc_cam.h"
#include "camutil.h"
#include "CamReg.h"
#include "CamUvc.h"
#include "CamCtl.h"
//#include "camisp.h"
#include "CamSensor.h"
//#include "camprocess.h"
#include "CamI2C.h"
#include "camvdcfg.h"
#include "Global_vars.h"
#include "ISP_vars.h"

U8 code g_FT2_Module_CCM[21] = {49,1,26,8,23,8,67,8,61,1,5,0,10,0,100,8,90,1,0,0,0};
OV_CTT_t code gc_FT2_CTT[3] =
{
	{3000,0x100,0x100,0x100},
	{4150,0x100,0x100,0x100},
	{6500,0x100,0x100,0x100},
};

void FT2ModelSetFormatFps(U8 const Fps)
{
#ifdef _NEW_FT2_MODEL_
	U16 sensor_width;
	U16 sensor_height;
	U16 start_x;
	U16 start_y;
	U16 block_width;
	U16 block_height;
	U16 margin_width;
	U16 margin_height;
	U16 frame_num;
	U16 dummy_pixel;
	U16 dummu_line;

	CHANGE_CCS_CLK(CCS_CLK_SEL_96M|CCS_CLK_DIVIDER_2);

	//(1600 + 1600)*(240+10)*30 = 24000000
	frame_num = 0xFFFF;

	sensor_width = 1600;
	sensor_height = 240;

	start_x = 45;
	start_y = 15;

	block_width = 240;
	block_height = 40;

	margin_width = 10;
	margin_height = 10;

	if (g_byCurFormat == FORMAT_TYPE_MJPG)
	{
		//	Write_SenReg_Mask(0x01, 0x00, 0x02);
		g_bySensor_YuvMode = RAW_MODE;
		g_dwPclk = 24000000;
		dummy_pixel = 13;
		dummu_line = 8;
		Write_SenReg( 0x02, INT2CHAR(dummy_pixel, 0));
		Write_SenReg( 0x03, INT2CHAR(dummy_pixel, 1));
		Write_SenReg( 0x04, INT2CHAR(dummu_line, 0));
		Write_SenReg( 0x05, INT2CHAR(dummu_line, 1));
		Write_SenReg(0x01, 0x01);

		g_wSensorHsyncWidth = sensor_width + dummy_pixel;
		g_wAECExposureRowMax = sensor_height + dummu_line;
		g_wAEC_LineNumber =sensor_height + dummu_line;

	}
	else
	{
		g_bySensor_YuvMode =YUV422_MODE;
		//	Write_SenReg_Mask(0x01, 0x02, 0x02);
		dummy_pixel = 106;
		g_dwPclk = 12000000;
		dummu_line = 2;
		Write_SenReg( 0x02, INT2CHAR(dummy_pixel, 0));
		Write_SenReg( 0x03, INT2CHAR(dummy_pixel, 1));
		Write_SenReg( 0x04, INT2CHAR(dummu_line, 0));
		Write_SenReg( 0x05, INT2CHAR(dummu_line, 1));
		Write_SenReg(0x01, 0x03);
		g_wSensorHsyncWidth = sensor_width + (dummy_pixel/2);
		g_wAECExposureRowMax = sensor_height + dummu_line;
		g_wAEC_LineNumber = sensor_height + dummu_line;
	}

	Write_SenReg( 0x07, INT2CHAR(frame_num, 0));
	Write_SenReg( 0x08, INT2CHAR(frame_num, 1));

	Write_SenReg( 0x30, INT2CHAR(sensor_width, 0));
	Write_SenReg( 0x31, INT2CHAR(sensor_height, 0));
	Write_SenReg_Mask( 0x32, (INT2CHAR(sensor_width, 1)) << 4,   0xF0);
	Write_SenReg_Mask( 0x32, (INT2CHAR(sensor_height, 1))&0x0F, 0x0f );

	Write_SenReg( 0x33, INT2CHAR(start_x, 0));
	Write_SenReg( 0x34, INT2CHAR(start_y, 0));
	Write_SenReg_Mask( 0x35, (INT2CHAR(start_x, 1)) << 4,   0xF0);
	Write_SenReg_Mask( 0x35, (INT2CHAR(start_y, 1))&0x0F, 0x0f );

	Write_SenReg( 0x36, INT2CHAR(margin_width, 0));
	Write_SenReg( 0x37, INT2CHAR(margin_height, 0));
	Write_SenReg_Mask( 0x38, (INT2CHAR(margin_width, 1)) << 4,   0xF0);
	Write_SenReg_Mask( 0x38, (INT2CHAR(margin_height, 1))&0x0F, 0x0f );

	Write_SenReg( 0x39, INT2CHAR(block_width, 0));
	Write_SenReg( 0x3a, INT2CHAR(block_height, 0));
	Write_SenReg_Mask( 0x3b, (INT2CHAR(block_width, 1)) << 4,   0xF0);
	Write_SenReg_Mask( 0x3b, (INT2CHAR(block_height, 1))&0x0F, 0x0f );


	g_wSensorWidthBefBLC = 1600;
	g_wSensorHeightBefBLC = 240;

#else
	// dummy pixel and dummy line
	Write_SenReg(0x0002, 0x13); //dummy pixel perline 275
	Write_SenReg(0x0003, 0x01);
	Write_SenReg(0x0004, 0x50); //dummy lines per frame 80
	Write_SenReg(0x0005, 0x00);

	g_wSensorHsyncWidth = 1600 + 275;
	g_wAECExposureRowMax = 1200 + 80;
	g_wAEC_LineNumber = 1200 + 80;

	if (Fps>= 10)	// 10fps
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_96M|CCS_CLK_DIVIDER_2);
		g_dwPclk = 24000000;
	}
	else // 5fps
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_96M|CCS_CLK_DIVIDER_4);
		g_dwPclk = 12000000;
	}

	g_wSensorWidthBefBLC = 1600;
	g_wSensorHeightBefBLC = 1200;

	// 1600x1200 raw output
	Write_SenReg(0x0001, 0x01);

	// manual white balance
	WhiteBalanceTempAutoItem.Last = 0;
#endif
}

void CfgFT2ControlAttr(void)
{
	U8 i;

	g_bySensorSize = SENSOR_SIZE_UXGA;
	g_wSensorSPFormat = UXGA_FRM;

	// hemonel 2010-11-10: fix FT2 module preview image color error
	memcpy(g_asOvCTT, gc_FT2_CTT,sizeof(gc_FT2_CTT));

	// hemonel 2010-09-01: support 1.8V sensor IO
	// sensor module rom code must configure SVIO 1.8V, don't configure 2.8V
	// cache code and vd code can configure SVIO as other voltage
#if (_CHIP_ID_ & _RTS5840_)
	XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
#elif (_CHIP_ID_ & _RTS5832_)
	XBYTE[REG_TUNESVA] =  SVA_VOL_2V79;
#elif (_CHIP_ID_ & _RTS5829B_)
	XBYTE[REG_TUNESVA] =  SVA_VOL_2V82;
#endif

#ifdef _FT2_SENSOR_TEST_EN_
#if (_CHIP_ID_ & _RTS5840_)
	XBYTE[REG_TUNESVIO] =  SVIO_VOL_3V40;
#elif (_CHIP_ID_ & _RTS5832_)
	XBYTE[REG_TUNESVIO] =  SVIO_VOL_3V32;
#elif (_CHIP_ID_ & _RTS5829B_)
	XBYTE[REG_TUNESVIO] =  SVIO_VOL_3V27;
#endif
#else
#if (_CHIP_ID_ & _RTS5840_)
	XBYTE[REG_TUNESVIO] =  SVIO_VOL_3V40;
#elif (_CHIP_ID_ & _RTS5832_)
	XBYTE[REG_TUNESVIO] =  SVIO_VOL_3V32;
#elif (_CHIP_ID_ & _RTS5829B_)
	XBYTE[REG_TUNESVIO] =  SVIO_VOL_3V27;
#endif
#endif


#ifdef _NEW_FT2_MODEL_
	g_aVideoFormat[0].byaVideoFrameTbl[1]=F_SEL_1280_192;
	g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_1280_192;
//		g_aVideoFormat[0].byaVideoFrameTbl[1]=F_SEL_1600_240;
//		g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_1600_240;
	g_aVideoFormat[0].byaVideoFrameTbl[0] = 1;
	g_aVideoFormat[0].byaStillFrameTbl[0] = 1;
	g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_1280_192]= FPS_30;
	g_aVideoFormat[0].byFormatType = FORMAT_TYPE_YUY2;
#ifdef _ENABLE_MJPEG_
	g_aVideoFormat[1].byaVideoFrameTbl[1]=F_SEL_1600_240;
	g_aVideoFormat[1].byaStillFrameTbl[1]=F_SEL_1600_240;
	g_aVideoFormat[1].byaVideoFrameTbl[0] = 1;
	g_aVideoFormat[1].byaStillFrameTbl[0] = 1;
	g_aVideoFormat[1].waFrameFpsBitmap[F_SEL_1600_240]=FPS_60;
	g_aVideoFormat[1].byFormatType = FORMAT_TYPE_MJPG;
#endif
#else
	g_aVideoFormat[0].byaVideoFrameTbl[1]=F_SEL_1600_1200;
	g_aVideoFormat[0].byaStillFrameTbl[1]=F_SEL_1600_1200;
	g_aVideoFormat[0].byaVideoFrameTbl[0] = 1;
	g_aVideoFormat[0].byaStillFrameTbl[0] = 1;
	g_aVideoFormat[0].waFrameFpsBitmap[F_SEL_1600_1200]= FPS_5;
	g_aVideoFormat[0].byFormatType = FORMAT_TYPE_YUY2;
#ifdef _ENABLE_MJPEG_
	g_aVideoFormat[1].byaVideoFrameTbl[1]=F_SEL_1600_1200;
	g_aVideoFormat[1].byaStillFrameTbl[1]=F_SEL_1600_1200;
	g_aVideoFormat[1].byaVideoFrameTbl[0] = 1;
	g_aVideoFormat[1].byaStillFrameTbl[0] = 1;
	g_aVideoFormat[1].waFrameFpsBitmap[F_SEL_1600_1200]= FPS_10;
	g_aVideoFormat[1].byFormatType = FORMAT_TYPE_MJPG;
#endif
#endif

#ifdef _ENABLE_MJPEG_
	// hemonel 2011-01-30: fix MJPEG FT2 test fail
	g_byJpeg_ACRA_En = 0;
#endif
	// hemonel 2011-04-27: ISP parameter configure
	for (i=0; i<9; i++)
	{
		g_aCCM[i]= g_FT2_Module_CCM[i];
	}
}

void Write_SenReg_Mask(U16 address, U16 wData, U16 wMask)
{
	U16 wTmp;

	Read_SenReg(address,&wTmp); // read the original value of the register to be writen
	wTmp &= (~wMask);	// clear these bits to be modified
	wTmp |= (wData&wMask);	// set these bits to the desired value
	Write_SenReg(address, wTmp);	// write the reigster to the desired value
}

#if (defined(RTS58XX_SP_MI1330)||defined(RTS58XX_SP_MI2020))
void Write_MI_Vairable_Mask(U16 address, U16 wData, U16 wMask)
{
	U16 wTmp;

	Read_MI_Vairable(address,&wTmp); // read the original value of the register to be writen
	wTmp &= (~wMask);	// clear these bits to be modified
	wTmp |= (wData&wMask);	// set these bits to the desired value
	Write_MI_Vairable(address, wTmp);	// write the reigster to the desired value
}

U8 Poll_MIVAR_Timeout(U16 wRegAddr, U16 wRegValue, U16 wRegMask,U8 byTimeout)
{
	U16 wTmp;
	U8 i;
	for(i=0; i<byTimeout; i++)
	{
		if(!Read_MI_Vairable(wRegAddr, &wTmp))
		{
			return FALSE;
		}
		if((wTmp&wRegMask)==(wRegValue&wRegMask))
		{
			return TRUE;
		}
		WaitTimeOut_Delay(1);
	}
	return FALSE;
}

#endif

#if (defined(RTS58XX_SP_MI1330)||defined(RTS58XX_SP_MI2020))
void MI_RefreshSeq()
{
	Write_MI_Vairable(MI_DRV_ID_SEQ|0X03|MIVAR_DATA_WIDTH_8_LOG, 0x0005);
}
void MI_RefreshMode()
{
	Write_MI_Vairable(MI_DRV_ID_SEQ|0X03|MIVAR_DATA_WIDTH_8_LOG, 0x0006);
}
U8 MI_PollWaitRefreshDone(U8 byTimeout)
{
	U16 wTmp;
	U8 i;
	for(i=0; i<byTimeout; i++)
	{

		Read_MI_Vairable(MI_DRV_ID_SEQ|0X03|MIVAR_DATA_WIDTH_8_LOG, &wTmp);
		if(wTmp==0)
		{
			return TRUE;
		}
		WaitTimeOut_Delay(1);
	}
	return FALSE;
}

U8 MapWBTemp2MicronSensorWBPositon(U16 wSetValue)
{
	return (U8)((((U32)wSetValue-2800)*127)/(6500-2800));
}

U16 MapMicronSensorWBPositon2WBTemp(U8 byPosition)
{
	return (U16)(((U32)byPosition*(6500-2800))/127  +2800);
}
#endif //RTS5820_SP_MICRON 

// hemonel 2011-03-30: delete for warning

void WriteSensorSettingBB(U16 length, t_RegSettingBB const pSetting[])
{
	U16 i;

	for ( i = 0; i < length; i++)
	{
		Write_SenReg(pSetting[i].byAddress, pSetting[i].byData);
	}
}

void WriteSensorSettingWB(U16 length, t_RegSettingWB const pSetting[])
{
	U16 i;

	for ( i = 0; i < length; i++)
	{
		Write_SenReg(pSetting[i].wAddress, pSetting[i].byData);
	}
}

void WriteSensorSettingWW(U16 length, t_RegSettingWW const pSetting[])
{
	U16 i;

	for ( i = 0; i < length; i++)
	{
		Write_SenReg(pSetting[i].wAddress, pSetting[i].wData);
	}
}

#if (defined(RTS58XX_SP_MI1330)||defined(RTS58XX_SP_MI2020))
void WriteMIVarSettingWW(U16 length, t_RegSettingWW pSetting[])
{
	U16 i;

	for ( i = 0; i < length; i++)
	{
		Write_MI_Vairable(pSetting[i].wAddress, pSetting[i].wData);
	}
}
#endif

#ifdef _MIPI_EXIST_
#else
void Init_CCS(void)
{
	XBYTE[CCS_PIN_PULL_CTL1] = VSYNC_NO_PULL| HSYNC_NO_PULL |DOUT_NO_PULL;
	XBYTE[CCS_PIN_PULL_CTL2] = PWDN_NO_PULL | SYSCLK_NO_PULL | PIXCLK_NO_PULL;

	XBYTE[CCS_CONTROL] = PIXEL_SAMPLE_RISING| HSYNC_ACTIVE_HIGH |VSYNC_ACTIVE_HIGH;
	XBYTE[CCS_TRANSFER] = CCS_TRANSFER_RESET;
}
#endif

// low: reset, high: not reset
void ENTER_SENSOR_RESET(void)
{
	XBYTE[PG_GPIO_AL_CTRL0] |= GPIO_AL1_OUTPUT;
	XBYTE[PG_GPIO_AL_CTRL0] &= ~GPIO_AL1_DRIVING_HIGH; 		// RTS5822 use GPIOAL1 as sensor reset, set sensor reset low
}

void LEAVE_SENSOR_RESET(void)
{
#ifdef _IC_CODE_
	// hemonel 2010-09-16: ASIC modify to open drain for sensor IO power level compaliance
	XBYTE[PG_GPIO_AL_CTRL0] &= ~GPIO_AL1_OUTPUT;
#else
	XBYTE[PG_GPIO_AL_CTRL0] |= GPIO_AL1_DRIVING_HIGH; 	// RTS5822 use GPIOAL1 as sensor reset, set sensor reset high
#endif
}


void Sensor_POR_Common(void)
{
	// Initialize CCS Controller Timing
#ifdef _MIPI_EXIST_
	Init_MIPI();
#else
	Init_CCS();
#endif
	ENTER_SENSOR_RESET();

	// power on
	SensorPowerControl(SWITCH_ON,EN_DELAYTIME);
#ifdef _FT2_REMOVE_TEST
#else
	uDelay(1);
#endif
	// Initialize I2C Controller Timing
	LEAVE_SENSOR_RESET();

	// clock select 24M
	if( g_bySSCEnable== TRUE)
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_SSC_24M);
	}
	else
	{
		CHANGE_CCS_CLK(CCS_CLK_SEL_96M|CCS_CLK_DIVIDER_4);
	}

	EnableSensorHCLK();

	ENTER_SENSOR_RESET();
#ifdef _FT2_REMOVE_TEST
#else
	WaitTimeOut_Delay(2);
#endif
	LEAVE_SENSOR_RESET();

	g_bySensorIsOpen = SENSOR_OPEN;
	// hemonel 2008-07-22: fix write sensor register fail bug
#ifdef _FT2_REMOVE_TEST
#else
	WaitTimeOut_Delay(10);
#endif
}

#ifdef _MIPI_EXIST_
void SetBLCWindowStart(U16 wHorStart, U16 wVerStart)
{
	XBYTE[ISP_BLC_START_X_L] = INT2CHAR(wHorStart, 0);
	XBYTE[ISP_BLC_START_X_H] = INT2CHAR(wHorStart, 1);
	XBYTE[ISP_BLC_START_Y_L] = INT2CHAR(wVerStart, 0);
	XBYTE[ISP_BLC_START_Y_H] = INT2CHAR(wVerStart, 1);
}
#else

void SetBkWindowStart(U16 wHorStart, U16 wVerStart)
{
	XBYTE[CCS_HSYNC_TCTL0] = INT2CHAR(wHorStart, 0);
	XBYTE[CCS_HSYNC_TCTL1] = INT2CHAR(wHorStart, 1);
	XBYTE[CCS_VSYNC_TCTL0] = INT2CHAR(wVerStart, 0);
	XBYTE[CCS_VSYNC_TCTL1] = INT2CHAR(wVerStart, 1);
}
#endif

OV_CTT_t OV_SetColorTemperature(U16 wSetValue)
{
	U8 data byTableIndex0,byTableIndex1;
	U8 i;
	OV_CTT_t ctt;

	for(i = 1; i < CTT_INDEX_MAX; i++)
	{
		if(g_asOvCTT[i].wCT== 0)
		{
			// color temperature table end
			i--;
			break;
		}

		if(g_asOvCTT[i].wCT >  wSetValue)
		{
			break;
		}
	}

	if(i==CTT_INDEX_MAX)
	{
		byTableIndex0 = CTT_INDEX_MAX-2;
		byTableIndex1 = CTT_INDEX_MAX-1;
	}
	else
	{
		byTableIndex0 = i-1;
		byTableIndex1 = i;
	}

	// hemonel 2010-09-15: use linear interpolation for optimize code
	ctt.wRgain= LinearIntp_Word(g_asOvCTT[byTableIndex0].wCT, g_asOvCTT[byTableIndex0].wRgain, g_asOvCTT[byTableIndex1].wCT, g_asOvCTT[byTableIndex1].wRgain, wSetValue);
	ctt.wGgain = LinearIntp_Word(g_asOvCTT[byTableIndex0].wCT, g_asOvCTT[byTableIndex0].wGgain, g_asOvCTT[byTableIndex1].wCT, g_asOvCTT[byTableIndex1].wGgain, wSetValue);
	ctt.wBgain = LinearIntp_Word(g_asOvCTT[byTableIndex0].wCT, g_asOvCTT[byTableIndex0].wBgain, g_asOvCTT[byTableIndex1].wCT, g_asOvCTT[byTableIndex1].wBgain, wSetValue);

	return ctt;
}

// 2010-06-22 darcy_lu : remove waring for RTS5820X , only support OV9726
/*
U16 CalAntiflickerStep(U8 byFps, U8 byFrq, U16 wRowNumber)
{
	return ((U16)byFps * wRowNumber/(U16)byFrq+1)/2; // round to integer
}

OV_BandingFilter_t  OV_SetBandingFilter(U8 byFps)
{
	OV_BandingFilter_t BdFilter;

	BdFilter.wD50Base= CalAntiflickerStep(byFps, 50, g_wAECExposureRowMax);//((U16)byFps*g_wAECExposureRowMax/(U16)50+1)/2;
	BdFilter.byD50Step= 100/byFps -1;

	BdFilter.wD60Base= CalAntiflickerStep(byFps, 60, g_wAECExposureRowMax);//((U16)byFps*g_wAECExposureRowMax/(U16)60+1)/2;
	BdFilter.byD60Step= 120/byFps -1;

	if(BdFilter.wD50Base<0x10)
	{
		BdFilter.wD50Base= CalAntiflickerStep(byFps, 25, g_wAECExposureRowMax);//(U16)byFps*g_wAECExposureRowMax/(U16)(100/2);
		BdFilter.byD50Step= (100/2)/byFps -1;
	}

	if(BdFilter.wD60Base<0x10)
	{
		BdFilter.wD60Base= CalAntiflickerStep(byFps, 30, g_wAECExposureRowMax);//(U16)byFps*g_wAECExposureRowMax/(U16)(120/2);
		BdFilter.byD60Step= (120/2)/byFps -1;
	}

	return BdFilter;

}
*/

void WaitFrameSync(U8 byISPINTNum, U8 byWaitSignal)
{
	if(g_byStartVideo)
	{
		if(byISPINTNum == ISP_INT0)    //ISP INT0
		{
			XBYTE[ISP_INT_FLAG0] = byWaitSignal;	// clear wait signal
			WaitTimeOut(ISP_INT_FLAG0, byWaitSignal, 1, 30);	// wait signal coming next time
		}
		else    //ISP INT1
		{
			XBYTE[ISP_INT_FLAG1] = byWaitSignal;	// clear wait signal
			WaitTimeOut(ISP_INT_FLAG1, byWaitSignal, 1, 30);	// wait signal coming next time
		}
	}
}
//zouxiaozhi 2012-3-29:just use ISP_INT1 flag for sync and comment this function
/*
void WaitFrameSync(U8 byWaitSignal)
{
	if(g_byStartVideo)
	{
		XBYTE[CCS_INT_STS] = byWaitSignal;	// clear wait signal
		WaitTimeOut(CCS_INT_STS, byWaitSignal, 1, 30);	// wait signal coming next time
	}
}
*/


#ifdef _MIPI_EXIST_
void Init_MIPI(void)
{
#ifdef _IC_CODE_
	XBYTE[MIPI_DPHY_CTRL] = MIPI_PIX_DIN_SEL|MIPI_BIT_SWITCH;
#else
	XBYTE[MIPI_DPHY_CTRL] = MIPI_PIX_DIN_SEL;
#endif
	XBYTE[ISP_MIPI_TRANSFER_EN] = SEL_MIPI_DATA;
	
	// CLK LANE0_PWDB LANE0_TERMEN all high active on ASIC    
#ifdef _IC_CODE_	
	//XBYTE[MIPI_DPHY_ACTIVE_LEVEL] = 0x03;
#else
	XBYTE[MIPI_DPHY_ACTIVE_LEVEL] = 0x03; 	
#endif
}

void SetMipiDphy(U8 byLaneSel, U8 byFormat, U8 byDataType, U8 byHSTerm)
{
	// data lane high speed term pull up time
	XBYTE[MIPI_DPHY_HSTERM] = byHSTerm;
	g_byMipiDphyCtrl = byLaneSel;
	XBYTE[MIPI_DPHY_DATA_FORMAT] = byFormat;
	XBYTE[MIPI_DPHY_DATA_TYPE] = byDataType;
	XBYTE[MIPI_DPHY_DEGLITCH] = 0x04|byLaneSel;	// Sean Say need to set them to high
}

void StartMipiAphy(void )
{
//-----------------------------APHY Setting---------------------------------
#ifdef _IC_CODE_
#else	//RLE0509
	Write_MIPIAPHYReg(0x050C, 0x80);
	Write_MIPIAPHYReg(0x050D, 0x03);
#endif
}

void StopMipiAphy(void )
{
//-----------------------------APHY Setting---------------------------------
#ifdef _IC_CODE_
#else	//RLE0509
	Write_MIPIAPHYReg(0x050C, 0x00);
	Write_MIPIAPHYReg(0x050D, 0x00);	
#endif
}
#endif

void InitSensor(void)
{
	ReadSensorID();
	CfgSensorControlAttr();

	g_wAWBFinalGainR = ((U32)g_wAWBRGain_Last)*256/g_wAWBGGain_Last;
	g_wAWBFinalGainB = ((U32)g_wAWBBGain_Last)*256/g_wAWBGGain_Last;
}
