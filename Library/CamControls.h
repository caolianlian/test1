#include "pc_cam.h"
#include "CamReg.h"
#include "CamUvc.h"
#include "CamCtl.h"
#include "CamI2C.h"
#include "global_vars.h"


/*
*********************************************************************************************************
* Exposuret time control.
*********************************************************************************************************
*/

/*
 CtlItemU32_t code  ExposureTimeAbsolutItem=
{
	CTL_MIN_CT_EXPOSURE_TIME_ABSOLUTE,     //min
	CTL_MAX_CT_EXPOSURE_TIME_ABSOLUTE,    //max
	CTL_RES_CT_EXPOSURE_TIME_ABSOLUTE     //res
	//	CTL_DEF_CT_EXPOSURE_TIME_ABSOLUTE       //def
};
//g_dwExposureTimeAbsolutDef = CTL_DEF_CT_EXPOSURE_TIME_ABSOLUTE ;      //def

 CtlExt_t  ExposureTimeAbsolutExt=
{
	ENT_ID_CAMERA_IT,
	CT_EXPOSURE_TIME_ABSOLUTE_CONTROL,
	CONTROL_OP_SET_CUR
		|CONTROL_OP_GET_CUR
		|CONTROL_OP_GET_MAX
		|CONTROL_OP_GET_MIN
		|CONTROL_OP_GET_RES
		|CONTROL_OP_GET_DEF,         //opmask
	CONTROL_LEN_4 ,         //len
	&ExposureTimeAbsolutItem        //pAttr
};

Ctl_t Ctl_ExposureTimeAbsolut=
{
	CONTROL_INFO_SP_SET
	|CONTROL_INFO_SP_GET
	|CONTROL_INFO_DIS_BY_AUTO
	|CONTROL_INFO_SP_AUTOUPDATA,       	//info
	CTL_CHNGFLG_RDY, 		          //changeflag
	&g_dwExposureTimeAbsolutDes,           //pCur
	&g_dwExposureTimeAbsolutLast,            //pLast
	&ExposureTimeAbsolutExt,      	 //pCtlExt
	&g_dwExposureTimeAbsolutDef
};


CtlItemU32_t  ExposureTimeAbsolutItem=
{
	CTL_MIN_CT_EXPOSURE_TIME_ABSOLUTE,     	//min
	CTL_MAX_CT_EXPOSURE_TIME_ABSOLUTE,    	//max
	CTL_RES_CT_EXPOSURE_TIME_ABSOLUTE,     	//res
	CTL_DEF_CT_EXPOSURE_TIME_ABSOLUTE,		//def
	CTL_DEF_CT_EXPOSURE_TIME_ABSOLUTE,       //des
	CTL_DEF_CT_EXPOSURE_TIME_ABSOLUTE,       //last
};

Ctl_t Ctl_ExposureTimeAbsolut=
{
	CONTROL_INFO_SP_SET
	|CONTROL_INFO_SP_GET
	|CONTROL_INFO_DIS_BY_AUTO
	|CONTROL_INFO_SP_AUTOUPDATA,       	//info
	CTL_CHNGFLG_RDY, 		          //changeflag
	CONTROL_OP_SET_CUR
		|CONTROL_OP_GET_CUR
		|CONTROL_OP_GET_MAX
		|CONTROL_OP_GET_MIN
		|CONTROL_OP_GET_RES
		|CONTROL_OP_GET_DEF,         //opmask
	CONTROL_LEN_4 ,         //len
	&ExposureTimeAbsolutItem        //pAttr
};*/
/*
*********************************************************************************************************
* exposure time AUTO control.
*********************************************************************************************************
*/
/*
 CtlItemU8_t  code   ExposureTimeAutoItem=
{
	0,     //min
   	1,       //max
   	EXPOSURE_TIME_AUTO_MOD_MANUAL
   	|EXPOSURE_TIME_AUTO_MOD_APERTPRO         //res
   	//EXPOSURE_TIME_AUTO_MOD_APERTPRO//def
};
//g_wExposureTimeAutoDef = EXPOSURE_TIME_AUTO_MOD_APERTPRO;//def

 CtlExt_t  ExposureTimeAutoExt=
{
	ENT_ID_CAMERA_IT,
	CT_AE_MODE_CONTROL,
	CONTROL_OP_SET_CUR
		|CONTROL_OP_GET_CUR
		|CONTROL_OP_GET_MIN
		|CONTROL_OP_GET_MAX
		|CONTROL_OP_GET_LEN
		|CONTROL_OP_GET_RES
		|CONTROL_OP_GET_DEF	,         //opmask
	CONTROL_LEN_1,        						 //len
	&ExposureTimeAutoItem       			 //pAttr
};
Ctl_t Ctl_ExposureTimeAuto=
{
	CONTROL_INFO_SP_SET
	|CONTROL_INFO_SP_GET,       		//info
	CTL_CHNGFLG_RDY,                 //changeflag
	&g_byExposureTimeAutoDes,           //pDes
	&g_byExposureTimeAutoLast,            //pLast
	&ExposureTimeAutoExt,            //pCtlExt
	&g_byExposureTimeAutoDef,
};


CtlItemU8_t  ExposureTimeAutoItem=
{
	0,     //min
   	1,       //max
   	EXPOSURE_TIME_AUTO_MOD_MANUAL
   	|EXPOSURE_TIME_AUTO_MOD_APERTPRO,         //res
   	EXPOSURE_TIME_AUTO_MOD_APERTPRO,		//def
   	EXPOSURE_TIME_AUTO_MOD_APERTPRO,		// des
   	EXPOSURE_TIME_AUTO_MOD_APERTPRO,		// last
};

Ctl_t Ctl_ExposureTimeAuto=
{
	CONTROL_INFO_SP_SET
	|CONTROL_INFO_SP_GET,       		//info
	CTL_CHNGFLG_RDY,                 //changeflag
	CONTROL_OP_SET_CUR
		|CONTROL_OP_GET_CUR
		|CONTROL_OP_GET_MIN
		|CONTROL_OP_GET_MAX
		|CONTROL_OP_GET_LEN
		|CONTROL_OP_GET_RES
		|CONTROL_OP_GET_DEF	,         //opmask
	CONTROL_LEN_1,        						 //len
	&ExposureTimeAutoItem       			 //pAttr
};*/
/*
*********************************************************************************************************
* Low light compensation control.
*********************************************************************************************************
*/
/*
 CtlItemU8_t  code LowLightCompItem=
{
	CTL_MIN_PU_LOWLIGHTCOMP,     //min
   	CTL_MAX_PU_LOWLIGHTCOMP,      //max
   	CTL_RES_PU_LOWLIGHTCOMP     //res
   	//CTL_DEF_PU_PWRLINEFRQ     //def
};
//g_byPwrLineFreqDef = CTL_DEF_PU_PWRLINEFRQ;     //def
 CtlExt_t  LowLightCompExt=
{
	ENT_ID_CAMERA_IT,
	CT_AE_PRIORITY_CONTROL,
	CONTROL_OP_SET_CUR
		|CONTROL_OP_GET_CUR
		|CONTROL_OP_GET_RES
		|CONTROL_OP_GET_DEF
		|CONTROL_OP_GET_MIN
		|CONTROL_OP_GET_MAX
		|CONTROL_ATTR_UNSIGNED,          //opmask
	CONTROL_LEN_1,        						 //len
	&LowLightCompItem       			 //pAttr
};
Ctl_t Ctl_LowLightComp=
{
	CONTROL_INFO_SP_SET
	|CONTROL_INFO_SP_GET,       		//info
	CTL_CHNGFLG_RDY,                 //changeflag
	&g_byLowLightCompDes,           //pDes
	&g_byLowLightCompLast,            //pLast
	&LowLightCompExt         ,   //pCtlExt
	&g_byLowLightCompDef
};

CtlItemU8_t  LowLightCompItem=
{
	CTL_MIN_PU_LOWLIGHTCOMP,     //min
   	CTL_MAX_PU_LOWLIGHTCOMP,      //max
   	CTL_RES_PU_LOWLIGHTCOMP,     //res
   	CTL_DEF_PU_LOWLIGHTCOMP,     //def
   	CTL_DEF_PU_LOWLIGHTCOMP,     //des
   	CTL_DEF_PU_LOWLIGHTCOMP,     //last
};

Ctl_t Ctl_LowLightComp=
{
	CONTROL_INFO_SP_SET
	|CONTROL_INFO_SP_GET,       		//info
	CTL_CHNGFLG_RDY,                 //changeflag
	CONTROL_OP_SET_CUR
		|CONTROL_OP_GET_CUR
		|CONTROL_OP_GET_RES
		|CONTROL_OP_GET_DEF
		|CONTROL_OP_GET_MIN
		|CONTROL_OP_GET_MAX
		|CONTROL_ATTR_UNSIGNED,          //opmask
	CONTROL_LEN_1,        						 //len
	&LowLightCompItem       			 //pAttr
};*/
/*
*********************************************************************************************************
* Brightness control.
*********************************************************************************************************
*/
/*
 CtlItemS16_t  code BrightnessItem=
{
	CTL_MIN_PU_BRIGHTNESS,     //min
   	CTL_MAX_PU_BRIGHTNESS,       //max
   	CTL_RES_PU_BRIGHTNESS         //res
   	//CTL_DEF_PU_BRIGHTNESS         //def
};
//g_swBrightnessDef =CTL_DEF_PU_BRIGHTNESS;         //def
 CtlExt_t  BrightnessExt=
{
	ENT_ID_PROCESSING_UNIT,
	PU_BRIGHTNESS_CONTROL,
	CONTROL_OP_SET_CUR
		|CONTROL_OP_GET_CUR
		|CONTROL_OP_GET_MAX
		|CONTROL_OP_GET_MIN
		|CONTROL_OP_GET_RES
		|CONTROL_OP_GET_DEF
		|CONTROL_ATTR_SIGNED,         //opmask
	CONTROL_LEN_2,        			 //len
	&BrightnessItem       			 //pAttr
};
Ctl_t Ctl_Brightness=
{
	CONTROL_INFO_SP_SET
	|CONTROL_INFO_SP_GET,       		//info
	CTL_CHNGFLG_RDY,                 //changeflag
	&g_swBrightnessDes,           //pDes
	&g_swBrightnessLast,            //pLast
	&BrightnessExt,            //pCtlExt
	&g_swBrightnessDef
};

CtlItemS16_t  BrightnessItem=
{
	CTL_MIN_PU_BRIGHTNESS,     //min
   	CTL_MAX_PU_BRIGHTNESS,       //max
   	CTL_RES_PU_BRIGHTNESS,         //res
   	CTL_DEF_PU_BRIGHTNESS,         //def
   	CTL_DEF_PU_BRIGHTNESS,         //des
   	CTL_DEF_PU_BRIGHTNESS,         //last
};

Ctl_t Ctl_Brightness=
{
	CONTROL_INFO_SP_SET
	|CONTROL_INFO_SP_GET,       		//info
	CTL_CHNGFLG_RDY,                 //changeflag
	CONTROL_OP_SET_CUR
		|CONTROL_OP_GET_CUR
		|CONTROL_OP_GET_MAX
		|CONTROL_OP_GET_MIN
		|CONTROL_OP_GET_RES
		|CONTROL_OP_GET_DEF
		|CONTROL_ATTR_SIGNED,         //opmask
	CONTROL_LEN_2,        			 //len
	&BrightnessItem       			 //pAttr
};*/
/*
*********************************************************************************************************
* contrast  control.
*********************************************************************************************************
*/
/*
 CtlItemU16_t  code  ContrastItem=
{
	CTL_MIN_PU_CONTRAST,     //min
   	CTL_MAX_PU_CONTRAST,       //max
   	CTL_RES_PU_CONTRAST         //res
   	//CTL_DEF_PU_CONTRAST         //def
  };
//g_wContrastDef =CTL_DEF_PU_CONTRAST;         //def
 CtlExt_t  ContrastExt=
{
	ENT_ID_PROCESSING_UNIT,
	PU_CONTRAST_CONTROL,
	CONTROL_OP_SET_CUR
		|CONTROL_OP_GET_CUR
		|CONTROL_OP_GET_MAX
		|CONTROL_OP_GET_MIN
		|CONTROL_OP_GET_RES
		|CONTROL_OP_GET_DEF
		|CONTROL_ATTR_UNSIGNED,         //opmask
	CONTROL_LEN_2,        			 //len
	&ContrastItem       			 //pAttr
};
Ctl_t Ctl_Contrast=
{
	CONTROL_INFO_SP_SET
	|CONTROL_INFO_SP_GET,       		//info
	CTL_CHNGFLG_RDY,                 //changeflag
	&g_wContrastDes,           //pDes
	&g_wContrastLast,            //pLast
	&ContrastExt,            //pCtlExt
	&g_wContrastDef
};

CtlItemU16_t  ContrastItem=
{
	CTL_MIN_PU_CONTRAST,     //min
   	CTL_MAX_PU_CONTRAST,       //max
   	CTL_RES_PU_CONTRAST,         //res
   	CTL_DEF_PU_CONTRAST,         //def
   	CTL_DEF_PU_CONTRAST,         //des
   	CTL_DEF_PU_CONTRAST,         //last
};

Ctl_t Ctl_Contrast=
{
	CONTROL_INFO_SP_SET
	|CONTROL_INFO_SP_GET,       		//info
	CTL_CHNGFLG_RDY,                 //changeflag
	CONTROL_OP_SET_CUR
		|CONTROL_OP_GET_CUR
		|CONTROL_OP_GET_MAX
		|CONTROL_OP_GET_MIN
		|CONTROL_OP_GET_RES
		|CONTROL_OP_GET_DEF
		|CONTROL_ATTR_UNSIGNED,         //opmask
	CONTROL_LEN_2,        			 //len
	&ContrastItem       			 //pAttr
};*/
/*
*********************************************************************************************************
* Hue  control.
*********************************************************************************************************
*/
/*
 CtlItemS16_t  code  HueItem=
{
	CTL_MIN_PU_HUE,     //min
   	CTL_MAX_PU_HUE,       //max
   	CTL_RES_PU_HUE         //res
//   	CTL_DEF_PU_HUE         //def
};
//g_swHueDef =   	CTL_DEF_PU_HUE   ;      //def
 CtlExt_t  HueExt=
{
	ENT_ID_PROCESSING_UNIT,
	PU_HUE_CONTROL,
	CONTROL_OP_SET_CUR
		|CONTROL_OP_GET_CUR
		|CONTROL_OP_GET_MAX
		|CONTROL_OP_GET_MIN
		|CONTROL_OP_GET_RES
		|CONTROL_OP_GET_DEF
		|CONTROL_ATTR_SIGNED,         //opmask
	CONTROL_LEN_2,        			 //len
	&HueItem       			 //pAttr
};
Ctl_t Ctl_Hue=
{
	CONTROL_INFO_SP_SET
	|CONTROL_INFO_SP_GET,       		//info
	CTL_CHNGFLG_RDY,                 //changeflag
	&g_swHueDes,           //pDes
	&g_swHueLast,            //pLast
	&HueExt        ,    //pCtlExt
	&g_swHueDef
};

CtlItemS16_t  HueItem=
{
	CTL_MIN_PU_HUE,     //min
   	CTL_MAX_PU_HUE,       //max
   	CTL_RES_PU_HUE,         //res
  	CTL_DEF_PU_HUE,         //def
  	CTL_DEF_PU_HUE,         //des
  	CTL_DEF_PU_HUE,         //last
};

Ctl_t Ctl_Hue=
{
	CONTROL_INFO_SP_SET
	|CONTROL_INFO_SP_GET,       		//info
	CTL_CHNGFLG_RDY,                 //changeflag
	CONTROL_OP_SET_CUR
		|CONTROL_OP_GET_CUR
		|CONTROL_OP_GET_MAX
		|CONTROL_OP_GET_MIN
		|CONTROL_OP_GET_RES
		|CONTROL_OP_GET_DEF
		|CONTROL_ATTR_SIGNED,         //opmask
	CONTROL_LEN_2,        			 //len
	&HueItem       			 //pAttr
};*/
/*
*********************************************************************************************************
* saturation  control.
*********************************************************************************************************
*/
/*
 CtlItemU16_t  code  SaturationItem=
{
	CTL_MIN_PU_SATURATION,     //min
   	CTL_MAX_PU_SATURATION,       //max
   	CTL_RES_PU_SATURATION         //res
   	//CTL_DEF_PU_SATURATION         //def
};
//g_wSaturationDef =CTL_DEF_PU_SATURATION   ;      //def
 CtlExt_t  SaturationExt=
{
	ENT_ID_PROCESSING_UNIT,
	PU_SATURATION_CONTROL,
	CONTROL_OP_SET_CUR
		|CONTROL_OP_GET_CUR
		|CONTROL_OP_GET_MAX
		|CONTROL_OP_GET_MIN
		|CONTROL_OP_GET_RES
		|CONTROL_OP_GET_DEF
		|CONTROL_ATTR_UNSIGNED,         //opmask
	CONTROL_LEN_2,        			 //len
	&SaturationItem       			 //pAttr
};
Ctl_t Ctl_Saturation=
{
	CONTROL_INFO_SP_SET
	|CONTROL_INFO_SP_GET,       		//info
	CTL_CHNGFLG_RDY,                 //changeflag
	&g_wSaturationDes,           //pDes
	&g_wSaturationLast,            //pLast
	&SaturationExt        ,    //pCtlExt
	&g_wSaturationDef
};

CtlItemU16_t  SaturationItem=
{
	CTL_MIN_PU_SATURATION,     //min
   	CTL_MAX_PU_SATURATION,       //max
   	CTL_RES_PU_SATURATION,         //res
   	CTL_DEF_PU_SATURATION,         //def
   	CTL_DEF_PU_SATURATION,         //des
   	CTL_DEF_PU_SATURATION,         //last
};

Ctl_t Ctl_Saturation=
{
	CONTROL_INFO_SP_SET
	|CONTROL_INFO_SP_GET,       		//info
	CTL_CHNGFLG_RDY,                 //changeflag
	CONTROL_OP_SET_CUR
		|CONTROL_OP_GET_CUR
		|CONTROL_OP_GET_MAX
		|CONTROL_OP_GET_MIN
		|CONTROL_OP_GET_RES
		|CONTROL_OP_GET_DEF
		|CONTROL_ATTR_UNSIGNED,         //opmask
	CONTROL_LEN_2,        			 //len
	&SaturationItem       			 //pAttr
};*/
/*
*********************************************************************************************************
* Sharpness  control.
*********************************************************************************************************
*/
/*
 CtlItemU16_t  code SharpnessItem=
{
	CTL_MIN_PU_SHARPNESS,     //min
   	CTL_MAX_PU_SHARPNESS,       //max
   	CTL_RES_PU_SHARPNESS         //res
//   	CTL_DEF_PU_SHARPNESS         //def
};
//g_wSharpnessDef=CTL_DEF_PU_SHARPNESS      ;   //def
 CtlExt_t  SharpnessExt=
{
	ENT_ID_PROCESSING_UNIT,
	PU_SHARPNESS_CONTROL,
	CONTROL_OP_SET_CUR
		|CONTROL_OP_GET_CUR
		|CONTROL_OP_GET_MAX
		|CONTROL_OP_GET_MIN
		|CONTROL_OP_GET_RES
		|CONTROL_OP_GET_DEF
		|CONTROL_ATTR_UNSIGNED,         //opmask
	CONTROL_LEN_2,        			 //len
	&SharpnessItem       			 //pAttr
};
Ctl_t Ctl_Sharpness=
{
	CONTROL_INFO_SP_SET
	|CONTROL_INFO_SP_GET,       		//info
	CTL_CHNGFLG_RDY,                 //changeflag
	&g_wSharpnessDes,           //pDes
	&g_wSharpnessLast,            //pLast
	&SharpnessExt        ,    //pCtlExt
	&g_wSharpnessDef
};
*/

/*
// _moved to global_vars.c__jqg_20091219__
CtlItemU16_t  SharpnessItem=
{
	CTL_MIN_PU_SHARPNESS,     //min
   	CTL_MAX_PU_SHARPNESS,       //max
   	CTL_RES_PU_SHARPNESS,         //res
   	CTL_DEF_PU_SHARPNESS,         //def
   	CTL_DEF_PU_SHARPNESS,         //des
   	CTL_DEF_PU_SHARPNESS,         //last
};

Ctl_t Ctl_Sharpness=
{
	CONTROL_INFO_SP_SET
	|CONTROL_INFO_SP_GET,       		//info
	CTL_CHNGFLG_RDY,                 //changeflag
	CONTROL_OP_SET_CUR
		|CONTROL_OP_GET_CUR
		|CONTROL_OP_GET_MAX
		|CONTROL_OP_GET_MIN
		|CONTROL_OP_GET_RES
		|CONTROL_OP_GET_DEF
		|CONTROL_ATTR_UNSIGNED,         //opmask
	CONTROL_LEN_2,        			 //len
	&SharpnessItem       			 //pAttr
};
*/
/*
*********************************************************************************************************
* Gamma  control.
*********************************************************************************************************
*/
/*
 CtlItemU16_t code  GammaItem=
{
	CTL_MIN_PU_GAMMA,     //min
   	CTL_MAX_PU_GAMMA,       //max
   	CTL_RES_PU_GAMMA         //res
//   	CTL_DEF_PU_GAMMA         //def
};
//g_wGammaDef =   	CTL_DEF_PU_GAMMA     ;    //def
 CtlExt_t  GammaExt=
{
	ENT_ID_PROCESSING_UNIT,
	PU_GAMMA_CONTROL,
	CONTROL_OP_SET_CUR
		|CONTROL_OP_GET_CUR
		|CONTROL_OP_GET_MAX
		|CONTROL_OP_GET_MIN
		|CONTROL_OP_GET_RES
		|CONTROL_OP_GET_DEF
		|CONTROL_ATTR_UNSIGNED,         //opmask
	CONTROL_LEN_2,        			 //len
	&GammaItem       			 //pAttr
};
Ctl_t Ctl_Gamma=
{
	CONTROL_INFO_SP_SET
	|CONTROL_INFO_SP_GET,       		//info
	CTL_CHNGFLG_RDY,                 //changeflag
	&g_wGammaDes,           //pDes
	&g_wGammaLast,            //pLast
	&GammaExt        ,    //pCtlExt
	&g_wGammaDef
};

CtlItemU16_t GammaItem=
{
	CTL_MIN_PU_GAMMA,     //min
   	CTL_MAX_PU_GAMMA,       //max
   	CTL_RES_PU_GAMMA,         //res
   	CTL_DEF_PU_GAMMA,         //def
   	CTL_DEF_PU_GAMMA,         //des
   	CTL_DEF_PU_GAMMA,         //last
};

Ctl_t Ctl_Gamma=
{
	CONTROL_INFO_SP_SET
	|CONTROL_INFO_SP_GET,       		//info
	CTL_CHNGFLG_RDY,                 //changeflag
	CONTROL_OP_SET_CUR
		|CONTROL_OP_GET_CUR
		|CONTROL_OP_GET_MAX
		|CONTROL_OP_GET_MIN
		|CONTROL_OP_GET_RES
		|CONTROL_OP_GET_DEF
		|CONTROL_ATTR_UNSIGNED,         //opmask
	CONTROL_LEN_2,        			 //len
	&GammaItem       			 //pAttr
};*/
/*
*********************************************************************************************************
* WhiteBalanceTemp control.
*********************************************************************************************************
*/

/*
 CtlItemU16_t  code WhiteBalanceTempItem=
{
	CTL_MIN_PU_WHITE_BALANCE_TEMPERATURE,     //min
   	CTL_MAX_PU_WHITE_BALANCE_TEMPERATURE,       //max
   	CTL_RES_PU_WHITE_BALANCE_TEMPERATURE         //res
//   	CTL_DEF_PU_WHITE_BALANCE_TEMPERATURE//def
};
//g_wWhiteBalanceTempDef =    	CTL_DEF_PU_WHITE_BALANCE_TEMPERATURE ;//def
 CtlExt_t  WhiteBalanceTempExt=
{
	ENT_ID_PROCESSING_UNIT,
	PU_WHITE_BALANCE_TEMPERATURE_CONTROL,
	CONTROL_OP_SET_CUR
		|CONTROL_OP_GET_CUR
		|CONTROL_OP_GET_MAX
		|CONTROL_OP_GET_MIN
		|CONTROL_OP_GET_LEN
		|CONTROL_OP_GET_RES
		|CONTROL_OP_GET_DEF
		|CONTROL_ATTR_UNSIGNED,          //opmask
	CONTROL_LEN_2,        						 //len
	&WhiteBalanceTempItem       			 //pAttr
};
Ctl_t Ctl_WhiteBalanceTemp=
{
	CONTROL_INFO_SP_SET
	|CONTROL_INFO_SP_GET
	|CONTROL_INFO_DIS_BY_AUTO
	|CONTROL_INFO_SP_AUTOUPDATA
	,       		//info

	CTL_CHNGFLG_RDY,                 //changeflag

	&g_wWhiteBalanceTempDes,           //pDes
	&g_wWhiteBalanceTempLast,            //pLast
	&WhiteBalanceTempExt        ,    //pCtlExt
	&g_wWhiteBalanceTempDef
};

CtlItemU16_t  WhiteBalanceTempItem=
{
	CTL_MIN_PU_WHITE_BALANCE_TEMPERATURE,     //min
   	CTL_MAX_PU_WHITE_BALANCE_TEMPERATURE,       //max
   	CTL_RES_PU_WHITE_BALANCE_TEMPERATURE,         //res
   	CTL_DEF_PU_WHITE_BALANCE_TEMPERATURE,		//def
   	CTL_DEF_PU_WHITE_BALANCE_TEMPERATURE,		//des
   	CTL_DEF_PU_WHITE_BALANCE_TEMPERATURE,		//last
};

Ctl_t Ctl_WhiteBalanceTemp=
{
	CONTROL_INFO_SP_SET
	|CONTROL_INFO_SP_GET
	|CONTROL_INFO_DIS_BY_AUTO
	|CONTROL_INFO_SP_AUTOUPDATA
	,       		//info

	CTL_CHNGFLG_RDY,                 //changeflag
	CONTROL_OP_SET_CUR
		|CONTROL_OP_GET_CUR
		|CONTROL_OP_GET_MAX
		|CONTROL_OP_GET_MIN
		|CONTROL_OP_GET_LEN
		|CONTROL_OP_GET_RES
		|CONTROL_OP_GET_DEF
		|CONTROL_ATTR_UNSIGNED,          //opmask
	CONTROL_LEN_2,        						 //len
	&WhiteBalanceTempItem       			 //pAttr
};*/
/*
*********************************************************************************************************
* Backlight compensation  control.
*********************************************************************************************************
*/
/*
 CtlItemU16_t  code BackLightCompItem=
{
	CTL_MIN_PU_BACKLIGHTCOMP,     //min
   	CTL_MAX_PU_BACKLIGHTCOMP,       //max
   	CTL_RES_PU_BACKLIGHTCOMP         //res
//   	CTL_DEF_PU_BACKLIGHTCOMP         //def
};
//g_wBackLightCompDef =   	CTL_DEF_PU_BACKLIGHTCOMP     ;    //def
 CtlExt_t  BackLightCompExt=
{
	ENT_ID_PROCESSING_UNIT,
	PU_BACKLIGHT_COMPENSATION_CONTROL,
	CONTROL_OP_SET_CUR
		|CONTROL_OP_GET_CUR
		|CONTROL_OP_GET_MAX
		|CONTROL_OP_GET_MIN
		|CONTROL_OP_GET_RES
		|CONTROL_OP_GET_DEF
		|CONTROL_ATTR_UNSIGNED,         //opmask
	CONTROL_LEN_2,        			 //len
	&BackLightCompItem       			 //pAttr
};
Ctl_t Ctl_BackLightComp=
{
	CONTROL_INFO_SP_SET
	|CONTROL_INFO_SP_GET,       		//info
	CTL_CHNGFLG_RDY,                 //changeflag
	&g_wBackLightCompDes,           //pDes
	&g_wBackLightCompLast,            //pLast
	&BackLightCompExt        ,    //pCtlExt
	&g_wBackLightCompDef
};

CtlItemU16_t  BackLightCompItem=
{
	CTL_MIN_PU_BACKLIGHTCOMP,     //min
   	CTL_MAX_PU_BACKLIGHTCOMP,       //max
   	CTL_RES_PU_BACKLIGHTCOMP,         //res
   	CTL_DEF_PU_BACKLIGHTCOMP,         //def
   	CTL_DEF_PU_BACKLIGHTCOMP,         //des
   	CTL_DEF_PU_BACKLIGHTCOMP,         //last
};

Ctl_t Ctl_BackLightComp=
{
	CONTROL_INFO_SP_SET
	|CONTROL_INFO_SP_GET,       		//info
	CTL_CHNGFLG_RDY,                 //changeflag
	CONTROL_OP_SET_CUR
		|CONTROL_OP_GET_CUR
		|CONTROL_OP_GET_MAX
		|CONTROL_OP_GET_MIN
		|CONTROL_OP_GET_RES
		|CONTROL_OP_GET_DEF
		|CONTROL_ATTR_UNSIGNED,         //opmask
	CONTROL_LEN_2,        			 //len
	&BackLightCompItem       			 //pAttr
};*/
/*
*********************************************************************************************************
* Gain  control.
*********************************************************************************************************
*/
/*
code CtlItemU16_t GainItem=
{
	CTL_MIN_PU_GAIN,     //min
   	CTL_MAX_PU_GAIN,       //max
   	CTL_RES_PU_GAIN,         //res
   	//CTL_DEF_PU_GAIN         //def
};
code CtlExt_t  GainExt=
{
	ENT_ID_PROCESSING_UNIT,
	PU_GAIN_CONTROL,
	CONTROL_OP_SET_CUR
		|CONTROL_OP_GET_CUR
		|CONTROL_OP_GET_MAX
		|CONTROL_OP_GET_MIN
		|CONTROL_OP_GET_RES
		|CONTROL_OP_GET_DEF
		|CONTROL_ATTR_UNSIGNED,         //opmask
	CONTROL_LEN_2,        			 //len
	&GainItem       			 //pAttr
};
Ctl_t Ctl_Gain=
{
	CONTROL_INFO_SP_SET
	|CONTROL_INFO_SP_GET,       		//info
	CTL_CHNGFLG_RDY,                 //changeflag
	&g_wGainDes,           //pDes
	&g_wGainLast,            //pLast
	&GainExt,            //pCtlExt
	&g_wGainDef,
};

CtlItemU16_t GainItem=
{
	CTL_MIN_PU_GAIN,     //min
   	CTL_MAX_PU_GAIN,       //max
   	CTL_RES_PU_GAIN,         //res
   	CTL_DEF_PU_GAIN,         //def
   	CTL_DEF_PU_GAIN,         //des
   	CTL_DEF_PU_GAIN,         //last
};

Ctl_t Ctl_Gain=
{
	CONTROL_INFO_SP_SET
	|CONTROL_INFO_SP_GET,       		//info
	CTL_CHNGFLG_RDY,                 //changeflag
	CONTROL_OP_SET_CUR
		|CONTROL_OP_GET_CUR
		|CONTROL_OP_GET_MAX
		|CONTROL_OP_GET_MIN
		|CONTROL_OP_GET_RES
		|CONTROL_OP_GET_DEF
		|CONTROL_ATTR_UNSIGNED,         //opmask
	CONTROL_LEN_2,        			 //len
	&GainItem       			 //pAttr
};*/
/*
*********************************************************************************************************
* Power Line control.
*********************************************************************************************************
*/
/*
 CtlItemU8_t  code PwrLineFreqItem=
{
	CTL_MIN_PU_PWRLINEFRQ,     //min
   	CTL_MAX_PU_PWRLINEFRQ,      //max
   	CTL_RES_PU_PWRLINEFRQ     //res
   	//CTL_DEF_PU_PWRLINEFRQ     //def
};
//g_byPwrLineFreqDef = CTL_DEF_PU_PWRLINEFRQ;     //def
 CtlExt_t  PwrLineFreqExt=
{
	ENT_ID_PROCESSING_UNIT,
	PU_POWER_LINE_FREQUENCY_CONTROL,
	CONTROL_OP_SET_CUR
		|CONTROL_OP_GET_CUR
		|CONTROL_OP_GET_RES
		|CONTROL_OP_GET_DEF
		|CONTROL_OP_GET_MIN
		|CONTROL_OP_GET_MAX
		|CONTROL_ATTR_UNSIGNED,          //opmask
	CONTROL_LEN_1,        						 //len
	&PwrLineFreqItem       			 //pAttr
};
Ctl_t Ctl_PwrLineFreq=
{
	CONTROL_INFO_SP_SET
	|CONTROL_INFO_SP_GET,       		//info
	CTL_CHNGFLG_RDY,                 //changeflag
	&g_byPwrLineFreqDes,           //pDes
	&g_byPwrLineFreqLast,            //pLast
	&PwrLineFreqExt         ,   //pCtlExt
	&g_byPwrLineFreqDef
};

CtlItemU8_t  PwrLineFreqItem=
{
	CTL_MIN_PU_PWRLINEFRQ,     //min
   	CTL_MAX_PU_PWRLINEFRQ,      //max
   	CTL_RES_PU_PWRLINEFRQ,     //res
   	CTL_DEF_PU_PWRLINEFRQ,     //def
   	CTL_DEF_PU_PWRLINEFRQ,     //des
   	CTL_DEF_PU_PWRLINEFRQ,     //last
};

Ctl_t Ctl_PwrLineFreq=
{
	CONTROL_INFO_SP_SET
	|CONTROL_INFO_SP_GET,       		//info
	CTL_CHNGFLG_RDY,                 //changeflag
	CONTROL_OP_SET_CUR
		|CONTROL_OP_GET_CUR
		|CONTROL_OP_GET_RES
		|CONTROL_OP_GET_DEF
		|CONTROL_OP_GET_MIN
		|CONTROL_OP_GET_MAX
		|CONTROL_ATTR_UNSIGNED,          //opmask
	CONTROL_LEN_1,        						 //len
	&PwrLineFreqItem       			 //pAttr
};*/
/*
*********************************************************************************************************
* WhiteBalanceTempAuto control.
*********************************************************************************************************
*/
/*
 CtlItemU8_t  code WhiteBalanceTempAutoItem=
{
	0,     //min
   	1,       //max
   	1         //res
 //  	1    //def
};
// g_byWhiteBalanceTempAutoDef = 1;
 CtlExt_t  WhiteBalanceTempAutoExt=
{
	ENT_ID_PROCESSING_UNIT,
	PU_WHITE_BALANCE_TEMPERATURE_AUTO_CONTROL,
	CONTROL_OP_SET_CUR
		|CONTROL_OP_GET_CUR
		|CONTROL_OP_GET_MAX
		|CONTROL_OP_GET_MIN
		|CONTROL_OP_GET_LEN
		|CONTROL_OP_GET_DEF
		|CONTROL_OP_GET_RES,         //opmask
	CONTROL_LEN_1,        						 //len
	&WhiteBalanceTempAutoItem       			 //pAttr
};
Ctl_t Ctl_WhiteBalanceTempAuto=
{
	CONTROL_INFO_SP_SET
	|CONTROL_INFO_SP_GET
	//|CONTROL_INFO_SP_AUTOUPDATA
	//|CONTROL_INFO_DIS_BY_AUTO
	,
	//info
	CTL_CHNGFLG_RDY,                 //changeflag
	&g_byWhiteBalanceTempAutoDes,           //pDes
	&g_byWhiteBalanceTempAutoLast,            //pLast
	&WhiteBalanceTempAutoExt         ,   //pCtlExt
	&g_byWhiteBalanceTempAutoDef
};

CtlItemU8_t  WhiteBalanceTempAutoItem=
{
	0,     //min
   	1,       //max
   	1,         //res
   	1,    //def
   	1,    //des
   	1,    //last
};

Ctl_t Ctl_WhiteBalanceTempAuto=
{
	CONTROL_INFO_SP_SET
	|CONTROL_INFO_SP_GET
	//|CONTROL_INFO_SP_AUTOUPDATA
	//|CONTROL_INFO_DIS_BY_AUTO
	,
	//info
	CTL_CHNGFLG_RDY,                 //changeflag
	CONTROL_OP_SET_CUR
		|CONTROL_OP_GET_CUR
		|CONTROL_OP_GET_MAX
		|CONTROL_OP_GET_MIN
		|CONTROL_OP_GET_LEN
		|CONTROL_OP_GET_DEF
		|CONTROL_OP_GET_RES,         //opmask
	CONTROL_LEN_1,        						 //len
	&WhiteBalanceTempAutoItem       			 //pAttr
};
*/
// hemonel 2009-12-17: add PTZ control
/*
*********************************************************************************************************
* PanTilt Absolute Control.
*********************************************************************************************************

CtlItemS16_t  PanItem=
{
	CTL_MIN_PU_PAN,     //min
   	CTL_MAX_PU_PAN,       //max
   	CTL_RES_PU_PAN,         //res
   	CTL_DEF_PU_PAN,    //def
   	CTL_DEF_PU_PAN,    //des
   	CTL_DEF_PU_PAN,    //last
};

CtlItemS16_t  TiltItem=
{
	CTL_MIN_PU_TILT,     //min
   	CTL_MAX_PU_TILT,       //max
   	CTL_RES_PU_TILT,         //res
   	CTL_DEF_PU_TILT,    //def
   	CTL_DEF_PU_TILT,    //des
   	CTL_DEF_PU_TILT,    //last
};

Ctl_t Ctl_PanTilt=
{
	CONTROL_INFO_SP_SET
	|CONTROL_INFO_SP_GET,       		//info
	CTL_CHNGFLG_RDY,                 //changeflag
	CONTROL_OP_SET_CUR
		|CONTROL_OP_GET_CUR
		|CONTROL_OP_GET_MAX
		|CONTROL_OP_GET_MIN
		|CONTROL_OP_GET_RES
		|CONTROL_OP_GET_DEF
		|CONTROL_ATTR_SIGNED,         //opmask
	CONTROL_LEN_8,        			 //len
	NULL       			 //pAttr
};
*/
/*
*********************************************************************************************************
* Zoom Absolute Control.
*********************************************************************************************************

CtlItemU16_t  ZoomItem=
{
	CTL_MIN_PU_ZOOM,     //min
   	CTL_MAX_PU_ZOOM,       //max
   	CTL_RES_PU_ZOOM,         //res
   	CTL_DEF_PU_ZOOM,    //def
   	CTL_DEF_PU_ZOOM,    //des
   	CTL_DEF_PU_ZOOM,    //last
};

Ctl_t Ctl_Zoom=
{
	CONTROL_INFO_SP_SET
	|CONTROL_INFO_SP_GET,       		//info
	CTL_CHNGFLG_RDY,                 //changeflag
	CONTROL_OP_SET_CUR
		|CONTROL_OP_GET_CUR
		|CONTROL_OP_GET_MAX
		|CONTROL_OP_GET_MIN
		|CONTROL_OP_GET_RES
		|CONTROL_OP_GET_DEF
		|CONTROL_ATTR_UNSIGNED,         //opmask
	CONTROL_LEN_2,        			 //len
	&ZoomItem       			 //pAttr
};
*/
/*
*********************************************************************************************************
* Roll Absolute Control.
*********************************************************************************************************

CtlItemS16_t  RollItem=
{
	CTL_MIN_PU_ROLL,     //min
   	CTL_MAX_PU_ROLL,       //max
   	CTL_RES_PU_ROLL,         //res
   	CTL_DEF_PU_ROLL,    //def
   	CTL_DEF_PU_ROLL,    //des
   	CTL_DEF_PU_ROLL,    //last
};

Ctl_t Ctl_Roll=
{
	CONTROL_INFO_SP_SET
	|CONTROL_INFO_SP_GET,       		//info
	CTL_CHNGFLG_RDY,                 //changeflag
	CONTROL_OP_SET_CUR
		|CONTROL_OP_GET_CUR
		|CONTROL_OP_GET_MAX
		|CONTROL_OP_GET_MIN
		|CONTROL_OP_GET_RES
		|CONTROL_OP_GET_DEF
		|CONTROL_ATTR_SIGNED,         //opmask
	CONTROL_LEN_2,        			 //len
	&RollItem       			 //pAttr
};*/