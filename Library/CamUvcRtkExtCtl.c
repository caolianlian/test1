/*
*******************************************************************************
*                                       Realtek's Camera Project
*
*                                       Layer: UVC
*                                       Module: UVC Control
*                                   (c) Copyright 2011-2012, Realtek Corp.
*                                           All Rights Reserved
*
*                                                  V1.0
*
*
* File : CamUvcRtkExtCtl.c
*******************************************************************************
*/
/**
*******************************************************************************
  \file CamUvcRtkExtCtl.c
  \Realtek Extended Control request process.
  
  
  
Copyright (c) 2011-2012 by Realtek Incorporated.  All Rights Reserved.


*  \version 1.0
*  \date    8/5/2013

*******************************************************************************/

/*=============================================================================

                            EDIT HISTORY FOR MODULE

 when           who         what, where, why
 -----------    ---------   ---------------------------------------------------

==============================================================================*/

#include "CamUvcRtkExtCtl.h"
#include "CamRtkExtIsp.h"

#ifdef _RTK_EXTENDED_CTL_

void InitRtkExtISPSpecialEffectCtl(void)
{	
	Ctl_RtkExtISPSpecialEffect.Info = CONTROL_INFO_SP_SET|CONTROL_INFO_SP_GET;							
	Ctl_RtkExtISPSpecialEffect.ChangeFlag = CTL_CHNGFLG_RDY;
	Ctl_RtkExtISPSpecialEffect.OpMask = CONTROL_OP_SET_CUR
								|CONTROL_OP_GET_CUR
								|CONTROL_OP_GET_MAX
								|CONTROL_OP_GET_MIN
								|CONTROL_OP_GET_RES
								|CONTROL_OP_GET_DEF
								|CONTROL_ATTR_UNSIGNED
								|CONTROL_OP_GET_LEN;
	Ctl_RtkExtISPSpecialEffect.Len = CONTROL_LEN_2;
	Ctl_RtkExtISPSpecialEffect.pAttr = (CtlAttr_t*)&RtkExtISPSpecialEffectItem;
}

void InitRtkExtEVCompensationCtl(void)
{
	Ctl_RtkExtEVCompensation.Info = CONTROL_INFO_SP_SET|CONTROL_INFO_SP_GET;							
	Ctl_RtkExtEVCompensation.ChangeFlag = CTL_CHNGFLG_RDY;
	Ctl_RtkExtEVCompensation.OpMask = CONTROL_OP_SET_CUR
										|CONTROL_OP_GET_CUR
										|CONTROL_OP_GET_MAX
										|CONTROL_OP_GET_MIN
										|CONTROL_OP_GET_RES
										|CONTROL_OP_GET_DEF
										|CONTROL_ATTR_SIGNED
										|CONTROL_OP_GET_LEN;
	Ctl_RtkExtEVCompensation.Len = CONTROL_LEN_2;
	Ctl_RtkExtEVCompensation.pAttr = (CtlAttr_t*)&RtkExtEVCompensationItem;
}

void SetRtkExtROIMax(void)
{
	RtkExtROIItem.Max.wTop = RtkExtROIItem.Max.wBottom = g_wCurFrameHeight;
	RtkExtROIItem.Max.wLeft = RtkExtROIItem.Max.wRight = g_wCurFrameWidth;
}

void InitRtkExtROICtl(void)
{
	memset(&(RtkExtROIItem.Des), 0, sizeof(RtkExtROICtlParams_t));
	memset(&(RtkExtROIItem.Last), 0, sizeof(RtkExtROICtlParams_t));

	SetRtkExtROIMax();

	Ctl_RtkExtROI.Info = CONTROL_INFO_SP_SET|CONTROL_INFO_SP_GET;							
	Ctl_RtkExtROI.ChangeFlag = CTL_CHNGFLG_RDY;
	Ctl_RtkExtROI.OpMask = CONTROL_OP_SET_CUR
							|CONTROL_OP_GET_CUR
							|CONTROL_OP_GET_MAX
							|CONTROL_OP_GET_MIN
							|CONTROL_OP_GET_RES
							|CONTROL_OP_GET_DEF
							|CONTROL_ATTR_UNSIGNED
							|CONTROL_OP_GET_LEN;
	Ctl_RtkExtROI.Len = 10;
	Ctl_RtkExtROI.pAttr = NULL;
}

void InitRtkExtPreviewLEDOffCtl(void)
{
	RtkExtPreviewLEDOffItem.Min = 0;
	RtkExtPreviewLEDOffItem.Max = 1;
	RtkExtPreviewLEDOffItem.Res = 1;
	RtkExtPreviewLEDOffItem.Def = 0;
	RtkExtPreviewLEDOffItem.Des = 0;
	RtkExtPreviewLEDOffItem.Last = 0;

	Ctl_RtkExtPreviewLEDOff.Info = CONTROL_INFO_SP_SET|CONTROL_INFO_SP_GET;							
	Ctl_RtkExtPreviewLEDOff.ChangeFlag = CTL_CHNGFLG_RDY;
	Ctl_RtkExtPreviewLEDOff.OpMask = CONTROL_OP_SET_CUR
										|CONTROL_OP_GET_CUR
										|CONTROL_OP_GET_MAX
										|CONTROL_OP_GET_MIN
										|CONTROL_OP_GET_RES
										|CONTROL_OP_GET_DEF
										|CONTROL_ATTR_UNSIGNED
										|CONTROL_OP_GET_LEN;
	Ctl_RtkExtPreviewLEDOff.Len = CONTROL_LEN_1;
	Ctl_RtkExtPreviewLEDOff.pAttr = (CtlAttr_t*)&RtkExtPreviewLEDOffItem;
}


void InitRtkExtISOCtl(void)
{
	Ctl_RtkExtISO.Info = CONTROL_INFO_SP_SET|CONTROL_INFO_SP_GET;							
	Ctl_RtkExtISO.ChangeFlag = CTL_CHNGFLG_RDY;
	Ctl_RtkExtISO.OpMask = CONTROL_OP_SET_CUR
										|CONTROL_OP_GET_CUR
										|CONTROL_OP_GET_MAX
										|CONTROL_OP_GET_MIN
										|CONTROL_OP_GET_RES
										|CONTROL_OP_GET_DEF
										|CONTROL_ATTR_UNSIGNED
										|CONTROL_OP_GET_LEN;
	Ctl_RtkExtISO.Len = CONTROL_LEN_2;
	Ctl_RtkExtISO.pAttr = (CtlAttr_t*)&RtkExtISOItem;
}

#ifdef _USE_BK_SPECIAL_EFFECT_
void RtkExtISPSpecailEffectCtl(U8 req)
{
	if((g_dwRtkExtUnitCtlSel&RTK_EXT_CTL_SEL_ISP_SPECIAL_EFFECT) == RTK_EXT_CTL_SEL_ISP_SPECIAL_EFFECT)	
	{
		switch(req)
		{
			case GET_CUR:
			case GET_INFO:
			case GET_LEN:
			case GET_MAX:
			case GET_MIN:
			case GET_DEF:
			case GET_RES:
				ControlGetReqProc(&Ctl_RtkExtISPSpecialEffect, req);
				break;
				
			case SET_CUR:
				if(ControlSetReqCheckProc(&Ctl_RtkExtISPSpecialEffect))
				{
					SetISPSpecailEffect(Ctl_RtkExtISPSpecialEffect.pAttr->CtlItemU16.Des);				
					Ctl_RtkExtISPSpecialEffect.ChangeFlag = CTL_CHNGFLG_RDY;
					Ctl_RtkExtISPSpecialEffect.pAttr->CtlItemU16.Last = Ctl_RtkExtISPSpecialEffect.pAttr->CtlItemU16.Des;
#ifdef _UVC_PPWB_
					if (g_VsPropCommit.wRtkExtISPSpecialEffect != Ctl_RtkExtISPSpecialEffect.pAttr->CtlItemU16.Des)
					{
						g_dwRtkExtCtlUnitChange_Flag |= RTK_EXT_CTL_SEL_ISP_SPECIAL_EFFECT;
					}
					else
					{
						g_dwRtkExtCtlUnitChange_Flag &= ~RTK_EXT_CTL_SEL_ISP_SPECIAL_EFFECT;
					}
#endif					
					EP0_HSK();	
				}
				break;
				
			default:
				VCSTALL_EPCTL(VC_ERR_INVDREQ);
				break;
		}	
	}
	else
	{
		VCSTALL_EPCTL(VC_ERR_INVDCTL);
	}
}
#endif

void RtkExtEVCompensationCtl(U8 req)
{
	if((g_dwRtkExtUnitCtlSel&RTK_EXT_CTL_SEL_EVCOM) == RTK_EXT_CTL_SEL_EVCOM)
	{
		switch(req)
		{
			case GET_CUR:
			case GET_INFO:
			case GET_LEN:
			case GET_MAX:
			case GET_MIN:
			case GET_DEF:
			case GET_RES:
				ControlGetReqProc(&Ctl_RtkExtEVCompensation, req);
				break;
				
			case SET_CUR:
				if(ControlSetReqCheckProc(&Ctl_RtkExtEVCompensation))
				{
					SetEVCompensation(Ctl_RtkExtEVCompensation.pAttr->CtlItemS16.Des, Ctl_RtkExtEVCompensation.pAttr->CtlItemS16.Res);
					Ctl_RtkExtEVCompensation.ChangeFlag=CTL_CHNGFLG_RDY;
					Ctl_RtkExtEVCompensation.pAttr->CtlItemS16.Last = Ctl_RtkExtEVCompensation.pAttr->CtlItemS16.Des;
#ifdef _UVC_PPWB_
					if (g_VsPropCommit.wRtkExtEVCompensation != Ctl_RtkExtEVCompensation.pAttr->CtlItemS16.Des)
					{
						g_dwRtkExtCtlUnitChange_Flag |= RTK_EXT_CTL_SEL_EVCOM;
					}
					else
					{
						g_dwRtkExtCtlUnitChange_Flag &= ~RTK_EXT_CTL_SEL_EVCOM;
					}
#endif
					EP0_HSK();	
				}
				break;
				
			default:
				VCSTALL_EPCTL(VC_ERR_INVDREQ);
				break;
		}	
	}
	else
	{
		VCSTALL_EPCTL(VC_ERR_INVDCTL);
	}
}

void RtkExtColorTemperatureCtl(U8 req)
{
	U16 getLen;
	U16 wColorTemp;

	if((g_dwRtkExtUnitCtlSel&RTK_EXT_CTL_SEL_CTE) == RTK_EXT_CTL_SEL_CTE)
	{
		ASSIGN_INT(getLen,XBYTE[SETUP_PKT_wLEN_H],XBYTE[SETUP_PKT_wLEN_L]);
		
		switch(req)
		{
			case GET_INFO:
				EP0_DATA_IN(CONTROL_INFO_SP_GET);
				EP0_FFV_HSK();
				break;
				
			case GET_LEN:
				EP0_DATA_IN(CONTROL_LEN_2);
				if(getLen==2)
				{
					EP0_DATA_IN(0);
				}				
				EP0_FFV_HSK();
				break;
				
			case GET_CUR:
				if(getLen >= CONTROL_LEN_2)
				{
					wColorTemp = ColorTemperature();
					EP0_DATA_IN(INT2CHAR(wColorTemp, 0));
					EP0_DATA_IN(INT2CHAR(wColorTemp, 1));
					EP0_FFV_HSK();
				}
				else
				{
					VCSTALL_EPCTL(VC_ERR_WRNGSTAT);
				}
				break;
				
			case GET_MIN:
			case GET_MAX:
			case GET_DEF:
			case GET_RES:
				EP0_DATA_IN(0);
				EP0_DATA_IN(0);
				EP0_FFV_HSK();
				break;				
				
			default:
				VCSTALL_EPCTL(VC_ERR_INVDREQ);
				break;
		}	
	}
	else
	{
		VCSTALL_EPCTL(VC_ERR_INVDCTL);
	}
}

static void RtkExtROIGetReqProc(RtkExtROICtlParams_t *ROIParams)
{
	U8 i;
	U16 getLen;
	ASSIGN_INT(getLen,XBYTE[SETUP_PKT_wLEN_H],XBYTE[SETUP_PKT_wLEN_L]);

	if(getLen >= 10 && ROIParams != NULL)
	{
		for(i = 0; i < 10; i += 2)
		{
			EP0_DATA_IN(*(((U8*)ROIParams)+i+1));
			EP0_DATA_IN(*(((U8*)ROIParams)+i));
		}
		
		EP0_FFV_HSK();
	}
	else   //error length parameter ,just stall.
	{
		VCSTALL_EPCTL(VC_ERR_WRNGSTAT);
	}
}

static bit RtkExtROISetReqCheckProc(Ctl_t* pCtl)
{
	U16 setLen;
	U8 i;
	RtkExtROICtlParams_t setROIParams;

	ASSIGN_INT(setLen,XBYTE[SETUP_PKT_wLEN_H],XBYTE[SETUP_PKT_wLEN_L]);

	if(setLen!=10)
	{
		VCSTALL_EPCTL(VC_ERR_UNKNOWN);
		return 0;
	}

	if(!WaitEP0_DATA_Out())
	{
		return 0;
	}

	for(i = 0; i < 10; i += 2)
	{
		*(((U8*)(&setROIParams)+i+1)) = EP0_DATA_OUT();
		*(((U8*)(&setROIParams)+i)) = EP0_DATA_OUT();
	}

	if(setROIParams.bmAutoControls != 0)
	{
		if((setROIParams.wTop >= setROIParams.wBottom) || (setROIParams.wLeft >= setROIParams.wRight)
			|| (setROIParams.wBottom > RtkExtROIItem.Max.wBottom) || (setROIParams.wRight > RtkExtROIItem.Max.wRight)
			|| ((RtkExtROIItem.Max.bmAutoControls|setROIParams.bmAutoControls) != RtkExtROIItem.Max.bmAutoControls))
		{
			VCSTALL_EPCTL(VC_ERR_OUTOFRANGE);
			return 0;
		}
	}

	RtkExtROIItem.Des = setROIParams;
	pCtl->ChangeFlag = CTL_CHNGFLG_NOTRDY;
	return 1;	
}

void RtkExtROICtl(U8 req)
{
	U16 getLen;
	RtkExtROICtlParams_t def = {0, 0, 0, 0, 0};
	
	if((g_dwRtkExtUnitCtlSel&RTK_EXT_CTL_SEL_ROI) == RTK_EXT_CTL_SEL_ROI)
	{
		ASSIGN_INT(getLen,XBYTE[SETUP_PKT_wLEN_H],XBYTE[SETUP_PKT_wLEN_L]);
		
		switch(req)
		{				
			case GET_INFO:
			case GET_LEN:
				ControlGetReqProc(&Ctl_RtkExtROI, req);
				break;
		
			case GET_CUR:
				if(Ctl_RtkExtROI.ChangeFlag == CTL_CHNGFLG_RDY)
				{
					RtkExtROIGetReqProc(&(RtkExtROIItem.Last));
				}
				else
				{
					VCSTALL_EPCTL(VC_ERR_NOTRDY);
				}				
				break;
				
			case GET_MAX:
				RtkExtROIGetReqProc(&(RtkExtROIItem.Max));
				break;

			case GET_DEF:
			case GET_MIN:
			case GET_RES:
				RtkExtROIGetReqProc(&def);
				break;
				
			case SET_CUR:
				if(RtkExtROISetReqCheckProc(&Ctl_RtkExtROI))
				{
					SetROI(RtkExtROIItem.Des.wTop, RtkExtROIItem.Des.wLeft, RtkExtROIItem.Des.wBottom, RtkExtROIItem.Des.wRight, RtkExtROIItem.Des.bmAutoControls);
					Ctl_RtkExtROI.ChangeFlag = CTL_CHNGFLG_RDY;
					RtkExtROIItem.Last = RtkExtROIItem.Des;
					EP0_HSK();
				}
				break;
				
			default:
				VCSTALL_EPCTL(VC_ERR_INVDREQ);
				break;
		}	
	}
	else
	{
		VCSTALL_EPCTL(VC_ERR_INVDCTL);
	}
}

void RtkExtROIStatusCtl(U8 req)
{
	U16 getLen;

	if((g_dwRtkExtUnitCtlSel&RTK_EXT_CTL_SEL_ROISTS) == RTK_EXT_CTL_SEL_ROISTS)
	{
		ASSIGN_INT(getLen,XBYTE[SETUP_PKT_wLEN_H],XBYTE[SETUP_PKT_wLEN_L]);
		
		switch(req)
		{	
			case GET_INFO:
				EP0_DATA_IN(CONTROL_INFO_SP_GET);
				EP0_FFV_HSK();
				break;
				
			case GET_LEN:
				EP0_DATA_IN(CONTROL_LEN_4);
				if(getLen==2)
				{
					EP0_DATA_IN(0);
				}				
				EP0_FFV_HSK();
				break;
				
			case GET_CUR:
				if(getLen >= CONTROL_LEN_4)
				{					
					EP0_DATA_IN(GetROIAEStatus());
					
#ifdef _AF_ENABLE_
					EP0_DATA_IN(GetROIAFStatus());
#else
					EP0_DATA_IN(0);
#endif
					
					EP0_DATA_IN(0);
					EP0_DATA_IN(0);
					EP0_FFV_HSK();
				}
				else
				{
					VCSTALL_EPCTL(VC_ERR_WRNGSTAT);
				}
				break;
				
			case GET_MIN:
			case GET_MAX:
			case GET_DEF:
			case GET_RES:
				EP0_DATA_IN(0);
				EP0_DATA_IN(0);
				EP0_DATA_IN(0);
				EP0_DATA_IN(0);
				EP0_FFV_HSK();
				break;
				
			default:
				VCSTALL_EPCTL(VC_ERR_INVDREQ);
				break;
		}	
	}
	else
	{
		VCSTALL_EPCTL(VC_ERR_INVDCTL);
		return;
	}
}

void RtkExtPreviewLEDOffCtl(U8 req)
{
	if((g_dwRtkExtUnitCtlSel&RTK_EXT_CTL_SEL_PREVIEW_LED_OFF) == RTK_EXT_CTL_SEL_PREVIEW_LED_OFF)
	{
		switch(req)
		{
			case GET_CUR:
			case GET_INFO:
			case GET_LEN:
			case GET_MAX:
			case GET_MIN:
			case GET_DEF:
			case GET_RES:
				ControlGetReqProc(&Ctl_RtkExtPreviewLEDOff, req);
				break;
				
			case SET_CUR:
				if(ControlSetReqCheckProc(&Ctl_RtkExtPreviewLEDOff))
				{
					SetPreviewLEDOff(RtkExtPreviewLEDOffItem.Des);
					Ctl_RtkExtPreviewLEDOff.ChangeFlag = CTL_CHNGFLG_RDY;
					RtkExtPreviewLEDOffItem.Last = RtkExtPreviewLEDOffItem.Des;
					EP0_HSK();
				}
				break;
				
			default:
				VCSTALL_EPCTL(VC_ERR_INVDREQ);
				break;
		}	
	}
	else
	{
		VCSTALL_EPCTL(VC_ERR_INVDCTL);
	}
}

void RtkExtISOCtl(U8 req)
{
	if((g_dwRtkExtUnitCtlSel&RTK_EXT_CTL_SEL_ISO) == RTK_EXT_CTL_SEL_ISO)
	{
		switch(req)
		{
			case GET_CUR:
			case GET_INFO:
			case GET_LEN:
			case GET_MAX:
			case GET_MIN:
			case GET_DEF:
			case GET_RES:
				ControlGetReqProc(&Ctl_RtkExtISO, req);
				break;
				
			case SET_CUR:
				if(ControlSetReqCheckProc(&Ctl_RtkExtISO))
				{
					if(Ctl_RtkExtISO.pAttr->CtlItemU16.Des != Ctl_RtkExtISO.pAttr->CtlItemU16.Last)
					{
						SetISO(Ctl_RtkExtISO.pAttr->CtlItemU16.Des);
					}
					
					Ctl_RtkExtISO.ChangeFlag=CTL_CHNGFLG_RDY;
					Ctl_RtkExtISO.pAttr->CtlItemU16.Last = Ctl_RtkExtISO.pAttr->CtlItemU16.Des;
#ifdef _UVC_PPWB_
					if (g_VsPropCommit.wRtkExtISO != Ctl_RtkExtISO.pAttr->CtlItemU16.Des)
					{
						g_dwRtkExtCtlUnitChange_Flag |= RTK_EXT_CTL_SEL_ISO;
					}
					else
					{
						g_dwRtkExtCtlUnitChange_Flag &= ~RTK_EXT_CTL_SEL_ISO;
					}
#endif
					EP0_HSK();	
				}
				break;
				
			default:
				VCSTALL_EPCTL(VC_ERR_INVDREQ);
				break;
		}	
	}
	else
	{
		VCSTALL_EPCTL(VC_ERR_INVDCTL);
	}
}

#endif

#ifdef _LENOVO_IDEA_EYE_EXTENDEDUNIT_
void InitIdeaEyeCtl(void)
{
	RtkExtIdeaEyeSensItem.Min = (1<<8)|(1);//min
	RtkExtIdeaEyeSensItem.Max = (100<<8)|(100);
	RtkExtIdeaEyeSensItem.Res = 1;//res
	RtkExtIdeaEyeSensItem.Def = 10;//def
	RtkExtIdeaEyeSensItem.Des = 10;//des
	RtkExtIdeaEyeSensItem.Last = 10;//last

	RtkExtIdeaEyeStatItem.Min = 0;//min
	RtkExtIdeaEyeStatItem.Max = 1;//max
	RtkExtIdeaEyeStatItem.Res = 1;//res
	RtkExtIdeaEyeStatItem.Def = 0;//def
	RtkExtIdeaEyeStatItem.Des = 0;//des
	RtkExtIdeaEyeStatItem.Last = 0;//last

	RtkExtIdeaEyeModeItem.Min = 0;//min
	RtkExtIdeaEyeModeItem.Max = 1;//max
	RtkExtIdeaEyeModeItem.Res = 1;//res
	RtkExtIdeaEyeModeItem.Def = 0;//def
	RtkExtIdeaEyeModeItem.Des = 0;//des
	RtkExtIdeaEyeModeItem.Last = 0;//last


	Ctl_IdeaEye_Sens.Info = CONTROL_INFO_SP_SET|CONTROL_INFO_SP_GET;							
	Ctl_IdeaEye_Sens.ChangeFlag = CTL_CHNGFLG_RDY;
	Ctl_IdeaEye_Sens.OpMask = CONTROL_OP_SET_CUR
								|CONTROL_OP_GET_CUR
								|CONTROL_OP_GET_MAX
								|CONTROL_OP_GET_MIN
								|CONTROL_OP_GET_RES
								|CONTROL_OP_GET_DEF
								|CONTROL_ATTR_UNSIGNED
								|CONTROL_OP_GET_LEN;
	Ctl_IdeaEye_Sens.Len = CONTROL_LEN_2;
	Ctl_IdeaEye_Sens.pAttr = (CtlAttr_t*)&RtkExtIdeaEyeSensItem;

	Ctl_IdeaEye_Stat.Info = CONTROL_INFO_SP_SET|CONTROL_INFO_SP_GET;							
	Ctl_IdeaEye_Stat.ChangeFlag = CTL_CHNGFLG_RDY;
	Ctl_IdeaEye_Stat.OpMask = CONTROL_OP_SET_CUR
								|CONTROL_OP_GET_CUR
								|CONTROL_OP_GET_MAX
								|CONTROL_OP_GET_MIN
								|CONTROL_OP_GET_RES
								|CONTROL_OP_GET_DEF
								|CONTROL_ATTR_UNSIGNED
								|CONTROL_OP_GET_LEN;
	Ctl_IdeaEye_Stat.Len = CONTROL_LEN_1;
	Ctl_IdeaEye_Stat.pAttr = (CtlAttr_t*)&RtkExtIdeaEyeStatItem;

	Ctl_IdeaEye_Mode.Info = CONTROL_INFO_SP_SET|CONTROL_INFO_SP_GET;							
	Ctl_IdeaEye_Mode.ChangeFlag = CTL_CHNGFLG_RDY;
	Ctl_IdeaEye_Mode.OpMask = CONTROL_OP_GET_CUR
								|CONTROL_OP_GET_MAX
								|CONTROL_OP_GET_MIN
								|CONTROL_OP_GET_RES
								|CONTROL_OP_GET_DEF
								|CONTROL_ATTR_UNSIGNED
								|CONTROL_OP_GET_LEN;
	Ctl_IdeaEye_Mode.Len = CONTROL_LEN_1;
	Ctl_IdeaEye_Mode.pAttr = (CtlAttr_t*)&RtkExtIdeaEyeModeItem;	
	
}

bit RtkIdeaEyeCtrlSetReqChkProc(Ctl_t* pCtl)
{
	U8 len;
	U16 setLen;
	U16 setValue16=0;
	U8 i;

	ASSERT(pCtl!=NULL);

	if(pCtl->Info&CONTROL_INFO_DIS_BY_AUTO)// disable by auto.
	{
		//DBG(("\npCtl-INFO=%bx",pCtl->Info));
		VCSTALL_EPCTL(VC_ERR_WRNGSTAT);
		return FALSE;
	}

	// hemonel 2009-11-26: use new control struct
//	len=pCtl->pExt->Len;
	len=pCtl->Len;

	ASSERT((len==1)||(len==2)||(len==4));
	ASSIGN_INT(setLen,XBYTE[SETUP_PKT_wLEN_H],XBYTE[SETUP_PKT_wLEN_L]);
	// hemonel 2009-7-16: fix uvc test gain control fail bug.
	//if(setLen!=len)//invalid set current request
	if((setLen>len)||(setLen ==0))
	{
		VCSTALL_EPCTL(VC_ERR_UNKNOWN);
		return FALSE;
	}

	if(!WaitEP0_DATA_Out())
	{
		return FALSE;
	}
	
	for(i=0; i<setLen; i++)
	{
		ASSIGN_U16BYTE(setValue16,i,EP0_DATA_OUT());
	}

	pCtl->pAttr->CtlItemU16.Des= setValue16;

	pCtl->ChangeFlag=CTL_CHNGFLG_NOTRDY;

	return TRUE;
}

static void RtkExtIdeaEyeSensCtl(U8 req)
{
	if((g_dwRtkExtUnitCtlSel&RTK_EXT_CTL_SEL_IDEAEYE_SENSITIVITY) == RTK_EXT_CTL_SEL_IDEAEYE_SENSITIVITY)
	{
		switch(req)
		{
			case GET_CUR:
			case GET_INFO:
			case GET_LEN:
			case GET_MAX:
			case GET_MIN:
			case GET_DEF:
			case GET_RES:
				ControlGetReqProc(&Ctl_IdeaEye_Sens, req);
				break;
			case SET_CUR:
				if(RtkIdeaEyeCtrlSetReqChkProc(&Ctl_IdeaEye_Sens))
				{
					Ctl_IdeaEye_Sens.ChangeFlag=CTL_CHNGFLG_RDY;
					Ctl_IdeaEye_Sens.pAttr->CtlItemU16.Last = Ctl_IdeaEye_Sens.pAttr->CtlItemU16.Des;
					g_byMTDSensCent = INT2CHAR(Ctl_IdeaEye_Sens.pAttr->CtlItemU16.Des,0);
					g_byMTDWinNCent = INT2CHAR(Ctl_IdeaEye_Sens.pAttr->CtlItemU16.Des,1);			
					EP0_HSK();	
				}
				break;
				
			default:
				VCSTALL_EPCTL(VC_ERR_INVDREQ);
				break;
		}	
	}
	else
	{
		VCSTALL_EPCTL(VC_ERR_INVDCTL);
	}	
}

static void RtkExtIdeaEyeStatCtl(U8 req)
{
	if((g_dwRtkExtUnitCtlSel&RTK_EXT_CTL_SEL_IDEAEYE_STATUS) == RTK_EXT_CTL_SEL_IDEAEYE_STATUS)
	{
		switch(req)
		{
			case GET_CUR:
			case GET_INFO:
			case GET_LEN:
			case GET_MAX:
			case GET_MIN:
			case GET_DEF:
			case GET_RES:
				if (req==GET_CUR)
				{
					if((g_byMTDDetectBackend) && g_byMTDWinDetected)
					{
						RtkExtIdeaEyeStatItem.Last = 2;
					}	
				}
				ControlGetReqProc(&Ctl_IdeaEye_Stat, req);
				if (req==GET_CUR)
				{
					g_byMTDWinDetected = 0;
					RtkExtIdeaEyeStatItem.Last = 0;
				}	
				break;
			default:
				VCSTALL_EPCTL(VC_ERR_INVDREQ);
				break;
		}	
	}
	else
	{
		VCSTALL_EPCTL(VC_ERR_INVDCTL);
	}	
}

static void RtkExtIdeaEyeModeCtl(U8 req)
{
	if((g_dwRtkExtUnitCtlSel&RTK_EXT_CTL_SEL_IDEAEYE_MODE) == RTK_EXT_CTL_SEL_IDEAEYE_MODE)
	{
		switch(req)
		{
			case GET_CUR:
			case GET_INFO:
			case GET_LEN:
			case GET_MAX:
			case GET_MIN:
			case GET_DEF:
			case GET_RES:
				ControlGetReqProc(&Ctl_IdeaEye_Mode, req);
				break;
				
			case SET_CUR:
				if(ControlSetReqCheckProc(&Ctl_IdeaEye_Mode))
				{
					Ctl_IdeaEye_Mode.ChangeFlag=CTL_CHNGFLG_RDY;
					Ctl_IdeaEye_Mode.pAttr->CtlItemU8.Last = Ctl_IdeaEye_Mode.pAttr->CtlItemU8.Des;
					if (Ctl_IdeaEye_Mode.pAttr->CtlItemU8.Last)
					{
						StartBackgroundStreaming();
					}	
					else
					{
						StopBackgroundStreaming();
					}	
					EP0_HSK();	
				}
				break;
				
			default:
				VCSTALL_EPCTL(VC_ERR_INVDREQ);
				break;
		}	
	}
	else
	{
		VCSTALL_EPCTL(VC_ERR_INVDCTL);
	}	
}
#endif

#if defined _RTK_EXTENDED_CTL_ || defined _LENOVO_IDEA_EYE_EXTENDEDUNIT_

void VideoCtlRtkExtReq(void)
{	
	U8 req=XBYTE[SETUP_PKT_bREQUEST];
	
	switch(XBYTE[SETUP_PKT_wVALUE_H])
	{
#ifdef 	_UVC_REALTEK_EXTENDEDUNIT_
		case RTK_EXT_ISP_SPECIAL_EFFECT_CTL:
			RtkExtISPSpecailEffectCtl(req);
			break;
			
		case RTK_EXT_EVCOM_CTL:
			RtkExtEVCompensationCtl(req);
			break;
			
		case RTK_EXT_CTE_CTL:
			RtkExtColorTemperatureCtl(req);
			break;
			
		case RTK_EXT_ROI_CTL:
			RtkExtROICtl(req);
			break;
			
		case RTK_EXT_ROISTS_CTL:
			RtkExtROIStatusCtl(req);
			break;
			
		case RTK_EXT_PREVIEW_LED_OFF_CTL:
			RtkExtPreviewLEDOffCtl(req);
			break;
		case RTK_EXT_ISO_CTL:
			RtkExtISOCtl(req);
			break;
			
#endif
#ifdef _LENOVO_IDEA_EYE_EXTENDEDUNIT_
		case RTK_EXT_IDEAEYE_SENSITIVITY_CTL:
			RtkExtIdeaEyeSensCtl(req);
			break;
		case RTK_EXT_IDEAEYE_STATUS_CTL:	
			RtkExtIdeaEyeStatCtl(req);
			break;
		case RTK_EXT_IDEAEYE_MODE_CTL:	
			RtkExtIdeaEyeModeCtl(req);
			break;
#endif			
		default:
			VCSTALL_EPCTL(VC_ERR_INVDCTL);
			break;			
	}
}

#endif

