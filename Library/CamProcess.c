#include "pc_cam.h"
#include "camutil.h"
#include "CamReg.h"
#include "CamUvc.h"
#include "CamCtl.h"
#include "camisp.h"
#include "CamSensor.h"
#include "CamProcess.h"
#include "Global_vars.h"
#include "ISP_vars.h"
#include "CamVdCfg.h"
#include "Jpeg.h"
#include "ISP_lib.h"
#ifdef _RTK_EXTENDED_CTL_
#include "CamUvcRtkExtCtl.h"
#include "CamRtkExtIsp.h"
#endif

extern U8 GetFomatType(U8 FmtIdx,U8 IsHighSpeed);
extern U16 GetFrameHeight(U8 IsVideo,U8 FmtIdx, U8 FrmIdx,U8 IsHighSpeed);
extern U16 GetFrameWidth(U8 IsVideo,U8 FmtIdx, U8 FrmIdx,U8 IsHighSpeed);
extern U8 GetDefaultFPS(U8 IsVideo,U8 FmtIdx, U8 FrmIdx,U8 IsHighSpeed);
extern void CacheSpeed(U8 const byMcuClk);

U16 GetSensorFormatWidth(U16 const Format, bit const Flag)  //if flag==1, default (width,height)=VGA (width,height), if flage==0, default (width,height) =(0,0)
{
#ifdef 	_IDEA_EYE_QVGA_RESOLUTION_	
	if (g_byMTDStartGetFrame)
	{
		return VIDEO_WIDTH_320;
	}
#endif
	switch (Format)
	{
		//	case CIF_FRM:
		//		return VIDEO_WIDTH_352;
		//		break;
	case VGA_FRM:
#ifdef _IMX076_320x240_60FPS_
		return 1280;
#else
		return VIDEO_WIDTH_640;
#endif
		//break;
	case SVGA_FRM:
		return VIDEO_WIDTH_800;
		//break;
	case XGA_FRM:
		return VIDEO_WIDTH_1024;
		//break;
	case HD720P_FRM:
	case HD800P_FRM: 	// hemonel 2010-01-21: add HD800P
	case XVGA_FRM:
	case SXGA_FRM:
		return VIDEO_WIDTH_1280;
		//break;
	case UXGA_FRM:
		return VIDEO_WIDTH_1600;
		//break;
	case HD1080P_FRM:
		return VIDEO_WIDTH_1920;
		//break;
	case QXGA_FRM:
		return VIDEO_WIDTH_2048;
		//break;
	case QSXGA_FRM:
		return VIDEO_WIDTH_2592;
		//break;
	default:
		if(Flag==1)
		{
			return VIDEO_WIDTH_640;
		}
		else
		{
			return 0;
		}
		//break;
	}
}

U16 GetSensorFormatHeight(U16 const Format, bit const Flag)  //if flag==1, default (width,height)=VGA (width,height), if flage==0, default (width,height) =(0,0)
{
#ifdef 	_IDEA_EYE_QVGA_RESOLUTION_	
	if (g_byMTDStartGetFrame)
	{
		return VIDEO_HEIGHT_192;
	}
#endif

	switch (Format)
	{
		//	case CIF_FRM:
		//		return VIDEO_HEIGHT_288;
		//		break;
	case VGA_FRM:
#ifdef _IMX076_320x240_60FPS_
		return 480;
#else
		return VIDEO_HEIGHT_480;
#endif
		//break;
	case SVGA_FRM:
		return VIDEO_HEIGHT_600;
		//break;
	case HD720P_FRM:
		return VIDEO_HEIGHT_720;
		//break;
	case XGA_FRM:
		return VIDEO_HEIGHT_768;
		//break;
	case HD800P_FRM:	// hemonel 2010-01-21: add HD800P
		return VIDEO_HEIGHT_800;
		//break;
	case XVGA_FRM:
		return VIDEO_HEIGHT_960;
		//break;
	case SXGA_FRM:
		return VIDEO_HEIGHT_1024;
		//break;
#ifdef _NEW_FT2_MODEL_
	case UXGA_FRM:
		return VIDEO_HEIGHT_240;
		break;
#else
	case UXGA_FRM:
		return VIDEO_HEIGHT_1200;
		//break;
#endif
	case HD1080P_FRM:
		return VIDEO_HEIGHT_1080;
		//break;
	case QXGA_FRM:
		return VIDEO_HEIGHT_1536;
		//break;
	case QSXGA_FRM:
		return VIDEO_HEIGHT_1944;
		//break;
	default:
		if(Flag==1)
		{
			return VIDEO_HEIGHT_480;
		}
		else
		{
			return 0;
		}
		//break;
	}
}

static U8 checkframesize(U16 const format, U16 const width, U16 const height)
{
	if(( GetSensorFormatWidth(format,0) >= width)&&(GetSensorFormatHeight( format,0) >= height))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

U16 GetAppropriateSensorFormat(U16 width, U16 height)
{
	U16 format;
	U8 i;

	// only VGA format ---scale up-->1280*960
	if(g_wSensorSPFormat == VGA_FRM)
	{
		return g_wSensorSPFormat;
	}

	// find sensor supported min frame size more than usb frame size
	for(i=0; i<16; i++)
	{
		format = g_wSensorSPFormat&(0x01<<i);	// from little to big
		if(checkframesize(format, width, height)==TRUE)
		{
			break;
		}
	}

	return format;
}

static void SetPorperPageLast(U8 byFps)
{
#ifdef _STILL_IMG_BACKUP_SETTING_
	if(g_byIsAEAWBFixed ==0)
#endif
	{
		SetSensorIntegrationTimeAuto(ExposureTimeAutoItem.Last);
		SetSensorWBTempAuto(WhiteBalanceTempAutoItem.Last);
		if(ExposureTimeAutoItem.Last != EXPOSURE_TIME_AUTO_MOD_APERTPRO)
		{
			SetSensorIntegrationTime(ExposureTimeAbsolutItem.Last);
		}
		if(WhiteBalanceTempAutoItem.Last == 0)
		{
#ifdef _LENOVO_JAPAN_PROPERTY_PAGE_		
			if (g_byVendorDriver_Lenovo)	
			{
				U16 wRgain, wGgain, wBgain;
				wRgain = LinearIntp_Word(0, 256, 255, 2047, g_byAWBRGain_Lenovo);
				wGgain = LinearIntp_Word(0, 256, 255, 2047, g_byAWBGGain_Lenovo);				
				wBgain = LinearIntp_Word(0, 256, 255, 2047, g_byAWBBGain_Lenovo);
				SetBKAWBGain(wRgain, wGgain, wBgain, 0);	
			}
			else
#endif				
			{
				SetSensorWBTemp(WhiteBalanceTempItem.Last);
			}	
		}
	}

#ifdef _USE_BK_HSBC_ADJ_
	SetBkBrightness( BrightnessItem.Last);
	SetBkContrast( ContrastItem.Last);
	SetBkSaturation( SaturationItem.Last);
	SetBkHue( HueItem.Last);
#else
	SetSensorBrightness( BrightnessItem.Last);
	SetSensorContrast(ContrastItem.Last);
	SetSensorSaturation( SaturationItem.Last);
	SetSensorHue ( HueItem.Last);
#endif

	if(g_bySensor_YuvMode == RAW_MODE)
	{
		SetBkSharpness (SharpnessItem.Last);
	}
	else
	{
		SetSensorSharpness(SharpnessItem.Last);
	}
	SetGammaParam(GammaItem.Last);
	SetSensorBackLightComp(BackLightCompItem.Last);
	SetGainParam(GainItem.Last);
	SetSensorPwrLineFreq(byFps, PwrLineFreqItem.Last);
#ifndef _USE_BK_SPECIAL_EFFECT_
#else
	SetBKSpecailEffect(g_bySnrEffect);
#ifdef _RTK_EXTENDED_CTL_	
	SetISPSpecailEffect(RtkExtISPSpecialEffectItem.Last);
	SetEVCompensation(RtkExtEVCompensationItem.Last, RtkExtEVCompensationItem.Res);
	SetRtkExtROIMax();
	SetROI(0, 0, 0, 0, 0);
#endif
#endif
	SetSensorImgDir(RollItem.Last^g_bySnrImgDir);
#ifdef _AF_ENABLE_
	SetFocusAuto(FocusAutoItem.Last);
	if(FocusAutoItem.Last == 0)
	{
		SetFocusAbsolute(FocusAbsolutItem.Last);
	}
#endif
}

static U32 GetMCUClk(void)
{
	switch(XBYTE[MCUCLK]&MCU_CLK_MASK)
	{
		case  0:
			return 120000000;
			//break;
		case  1:
			return  60000000;
			//break;
		case  2:
			return  30000000;
			//break;
		case  3:
			return  15000000;
			//break;
		case  4:
			return   7500000;
			//break;
		case  5:
			return   3750000;
			//break;
		case  6:
			return   1875000;
			//break;
		default:
			return  60000000;
			//break;
	}
}

static void ChangeBkClock(U8 const byScale,U8 const byHalfISPclk)
{
	U8 byIspClk, byMjpegClk;
	U8 i;
	U32 dwPclk;
	U32 dwIspClkThL,dwIspCurrentClk;
	U16 wlinewidthTemp;
	static U32 code cdwIspClk[8]= {20000000,24000000,30000000,40000000,
						48000000,60000000,80000000,96000000};
	static U32 code cdwMjpgClk[10]= {20000000,24000000,30000000,40000000,
						48000000,60000000,80000000,96000000,120000000,160000000};
	static U8 code cbyIspClkSlt[8] = {ISP_CLOCK_20M,ISP_CLOCK_24M,ISP_CLOCK_30M,ISP_CLOCK_40M,
						ISP_CLOCK_48M,ISP_CLOCK_60M,ISP_CLOCK_80M,ISP_CLOCK_96M};
	static U8 code cbyJpgClkSlt[10] = {JPEG_CLOCK_20M,JPEG_CLOCK_24M,JPEG_CLOCK_30M,JPEG_CLOCK_40M,
						JPEG_CLOCK_48M,JPEG_CLOCK_60M,JPEG_CLOCK_80M,JPEG_CLOCK_96M,JPEG_CLOCK_120M,JPEG_CLOCK_160M};


	dwIspClkThL = GetMCUClk()*1/3;		//Reck modify 2010_09_09 The relationship between MUC & ISP clock should satisfy 1/3
	// hemonel 2009-06-11:
	//Fixed ISP/JPEG clock caculating BUG for YUV input
	//  raw mode : pixelclk = PCLK , YUV mode: pixelclk = PCLK/2
	// the relation between ISP/JPEG clock and sensor pixel clk is at below:
	// YUV input and not scale up: ISP clock> sensor pixelclk*2, MJPEG clock > sensor pixelclk*2
	// raw mode and not scale up: ISP clock > sensor pixelclk, MJPEG clock > sensor pixelclk*2
	// scale up: ISP clock > sensor pixelclk*4, MJPEG clock > sensor pixelclk*8

	//if((g_bySensor_YuvMode)&&(g_bySensorCurFormat != VGA_FRM))
	// hemonel 2010-03-08 modify judge conditon: previous code ignore VGA YUV format scale down condition. At the condition, dwPclk = dwPclk*2;
#ifdef	_TCORRECTION_EN_
	if (g_wExtUIspCtl  & EXU1_CTL_SEL_TCORRECTION)
	{
		dwPclk = g_dwPclk*byScale*5/4;	//darcy_lu 2011-06-30  adjust for trapezium correction maybe scaleup
#if (_CHIP_ID_ & _RTS5829B_)
		for ( i= 0; i < 6; i++)	// RTS5829B ISP Clock DIC	STRECH=80MHZ only
#else		
		for ( i= 0; i < 7; i++)
#endif		
		{
			if ( (dwPclk<=cdwIspClk[i])&&(cdwIspClk[i]>dwIspClkThL)) 
			{
				break;
			}
		}
	}
	else
#endif
	{
		dwPclk = g_dwPclk*byScale;

#if (_CHIP_ID_ & _RTS5829B_)
		for ( i= 0; i < 6; i++)	// RTS5829B ISP Clock DIC	STRECH=80MHZ only
#else		
		for ( i= 0; i < 7; i++)
#endif
		{
			if ( (dwPclk<cdwIspClk[i])&&(cdwIspClk[i]>dwIspClkThL)) // hemonel 2011-04-07: when HCLK use SSC clock, ISP clock must be greater than sensor pclk
			{
				break;
			}
		}
	}


	byIspClk = cbyIspClkSlt[i];
	CHANGE_ISP_CLK(byIspClk);

	//xiaozhizou 2012-6-13: calculate current "PCLK" after zoom module,which will be used for determine MJPG clock
	dwIspCurrentClk = cdwIspClk[i];

	if ((g_wSensorHsyncWidth * 7) < ( g_wSensorCurFormatWidth * 8))
	{
		wlinewidthTemp = g_wSensorHsyncWidth *7/8;
	}
	else
	{
		wlinewidthTemp = g_wSensorCurFormatWidth;
	}


	if (wlinewidthTemp > g_wCurFrameWidth)
	{
		dwPclk = ((float)g_wCurFrameWidth*2/wlinewidthTemp)*g_dwPclk;
	}
	else
	{
		dwPclk = 2* g_dwPclk;
	}
	
	for ( i= 0; i < 9; i++)
	{
		if ((cdwMjpgClk[i]>=dwIspCurrentClk) && (cdwMjpgClk[i]>=dwPclk)) 
		{
			break;
		}
	}
	byMjpegClk = cbyJpgClkSlt[i];

	CHANGE_JPEG_CLK(byMjpegClk);

//#ifdef _FT2_RS_MODEL_
	CHANGE_ISP_CLK(ISP_CLOCK_96M);
	CHANGE_JPEG_CLK(JPEG_CLOCK_160M);
//#endif

	if ((byHalfISPclk==1)&&(dwIspCurrentClk>(2*dwIspClkThL)))
	{
		XBYTE[ISP_CLK_HALF_ENABLE] = 0x01;
	}
	else
	{
		XBYTE[ISP_CLK_HALF_ENABLE] = 0x00;
	}
}

static void SetHalfSample(U16 const width, U16 const wheight)
{
	// 320*180 need sub sample to pass MTF Lync test, but 420*240 don't
#ifndef _IMX076_320x240_60FPS_
	if((width<=320 && wheight <=180) &&
		((g_wSensorCurFormatWidth > 2*width) || (g_wSensorCurFormatHeight > 2*wheight)) )
#else
	if((g_wSensorCurFormatWidth > 2*width) || (g_wSensorCurFormatHeight > 2*wheight))
#endif
	{
		g_byHalfSubSample_En = 1;
		XBYTE[ISP_CONTROL2] |= ISP_HALFSAMPLE_EN;
	}	
	else
	{
		g_byHalfSubSample_En = 0;
		XBYTE[ISP_CONTROL2] &= ~ISP_HALFSAMPLE_EN;
	}
}

#ifdef _ENABLE_M420_FMT_
static void StartM420Module(void )
{
	XBYTE[ISP_M420_INTERNAL_SPEED_CTL] =0x34;
	XBYTE[ISP_M420_LASTROW_SPEED_CTL] =0x25;

	XBYTE[JENC_START] =JPEG_MODULE_RST;

	XBYTE[JENC_IMAGESIZE_XL] = INT2CHAR(g_wCurFrameWidth,0);//INT2CHAR(g_wCurMJPEGOutputWidth,0);
	XBYTE[JENC_IMAGESIZE_XH] = INT2CHAR(g_wCurFrameWidth,1);//INT2CHAR(g_wCurMJPEGOutputWidth,1);

	XBYTE[JENC_IMAGESIZE_YL] = INT2CHAR(g_wCurFrameHeight,0);//INT2CHAR(g_wCurMJPEGOutputHeight,0);
	XBYTE[JENC_IMAGESIZE_YH] =INT2CHAR(g_wCurFrameHeight,1); //INT2CHAR(g_wCurMJPEGOutputHeight,1);

	XBYTE[JENC_FRAMEWIDTH_L] = INT2CHAR(g_wCurFrameWidth,0);
	XBYTE[JENC_FRAMEWIDTH_H] = INT2CHAR(g_wCurFrameWidth,1);

	XBYTE[JENC_FRAMEHEIGHT_L] = INT2CHAR(g_wCurFrameHeight,0);
	XBYTE[JENC_FRAMEHEIGHT_H] = INT2CHAR(g_wCurFrameHeight,1);

	XBYTE[ISP_M420_CTRL] = 0x01;
}

static void StopM420Module()
{
	XBYTE[ISP_M420_CTRL] = 0x00;
	XBYTE[JENC_START] =JPEG_MODULE_RST;
}
#endif

void SetFrameSize(U16 width, U16 wheight)
{
	U16 data format;
#ifdef _AE_ST_WIN_CROP_
	U16 wZoomCropX,wZoomCropY;
#endif
	
	// get sensor format before scale function
	format =GetAppropriateSensorFormat(width, wheight);

	//zouxiaozhi 20109-1-2:avoid to rewrite sensor register when nothing had been changed
	if( (format != g_wSensorCurFormat)||(g_byCurFps != g_byFpsLast) 
#ifdef 	_LENOVO_IDEA_EYE_	
		|| g_byMTDStartGetFrame
#endif
	)
	{
		// configure sensor core output current resolution and fps
		SetSensorFormatFps(format, g_byCurFps);
		g_byFpsLast = g_byCurFps;
	}
	
	//Jimmy.20120606.Only after here can safely use g_bySensor_YuvMode,
	//Some Sensor at SetSensorFormatFps to change g_bySensor_YuvMode
	
	// sensor isp output, maybe use sensor ISP scale function
	g_wSensorCurFormat = format;
	g_wSensorCurFormatWidth=GetSensorFormatWidth( format, 1);
	g_wSensorCurFormatHeight=GetSensorFormatHeight( format, 1);

#ifdef _ENABLE_MJPEG_
	if((g_byCurFormat == FORMAT_TYPE_MJPG)&&(g_bySensor_YuvMode != MJPG_MODE))
	{
		JPEG_Clock_Switch(SWITCH_ON);
#ifdef _JPEG_RATE_ADJ_		
#ifndef _USB2_LPM_
		if (width > VIDEO_WIDTH_1280)	
		{
			g_byQtableScale = g_byQtableScaleGTHD;
		}
		else if (width > VIDEO_WIDTH_640)
		{
			g_byQtableScale = g_byQtableScaleGTVGA;			
		}
		else 
		{
			g_byQtableScale = g_byQtableScaleLEQVGA;
		}	
#endif
#endif
		if (g_byQtableCurScale != g_byQtableScale)
		{
			InitJpegTable();
		}
		StartJpegEncoder(JPEG_VIDEO_SEL|JPEG_SET);
	}
#endif
#ifdef _ENABLE_M420_FMT_
	if(g_byCurFormat == FORMAT_TYPE_M420)
	{
		JPEG_Clock_Switch(SWITCH_ON);
		StartM420Module();
	}
#endif


	//Reck add  2010-5-18: 1/2 subsample function
	if (g_bySetHalfSubSample_En == 1)
	{
		SetHalfSample(width, wheight);
	}

	// hemonel 2010-04-02: add dynamic switch ISP and MJPEG clock
	if((g_wSensorCurFormatWidth < width)||(g_wSensorCurFormatHeight<wheight))
	{
		// scale up
		ChangeBkClock(4,0);
	}
	else
	{
		if(g_bySensor_YuvMode==YUV422_MODE)
		{
#ifdef _MIPI_EXIST_
			ChangeBkClock(1,0);//zouxiaozhi 2012-4-24:ISP width for YUYV and MJPEGbyPass is 16bit now,
#else
			ChangeBkClock(2,1);
#endif
		}
		else if (g_bySensor_YuvMode==MJPG_MODE)
		{
#ifdef _MIPI_EXIST_
			ChangeBkClock(1,0);//zouxiaozhi 2012-4-24:ISP width for YUYV and MJPEGbyPass is 16bit now,
#else
			ChangeBkClock(2,0);
#endif
		}
		else		//g_bySensor_YuvMode==RAW_MODE
		{
			ChangeBkClock(1,0);	// 1 not scale up
		}
	}

	SetBackendISP(g_wSensorCurFormatWidth,g_wSensorCurFormatHeight);
	SetBackendScale( (g_wSensorCurFormatWidth>>g_byHalfSubSample_En),(g_wSensorCurFormatHeight>>g_byHalfSubSample_En), width,wheight);

#ifdef _AE_ST_WIN_CROP_
	// hemonel 2012-11-23: AE statics window be same as image input widow after zoom cropped
	ASSIGN_INT(wZoomCropX, XBYTE[ISP_ZOOM_START_X_H], XBYTE[ISP_ZOOM_START_X_L]);
	ASSIGN_INT(wZoomCropY, XBYTE[ISP_ZOOM_START_Y_H], XBYTE[ISP_ZOOM_START_Y_L]);
	SetAECWindow(wZoomCropX, wZoomCropY, (g_wSensorCurFormatWidth-wZoomCropX*2), (g_wSensorCurFormatHeight-wZoomCropY*2));
#else //_AE_ST_WIN_CROP_
	// hemonel 2012-11-23: AE statics window be same as image input window without zoom cropped
	SetAECWindow(0, 0, g_wSensorCurFormatWidth, g_wSensorCurFormatHeight);
#endif //_AE_ST_WIN_CROP_

	SetPorperPageLast(g_byCurFps);
}


// This function is called by StopImageTransfer()
void StopSensor(U8 byDelayCtrl)
{
	// hemonel 2009-07-10: reset low before HCLK off for 6AA and SIV120B spec
	ENTER_SENSOR_RESET();	// drive sensor reset low when sensor
#ifdef _FT2_REMOVE_TEST
#else
	if (byDelayCtrl == EN_DELAYTIME)
	{
		uDelay(1);
	}	
#endif
#ifdef _USE_SENSOR_HI165_
	uDelay(201);//101
#endif
	if (g_bySSCEnable == TRUE)
	{
		XBYTE[SSC_DIV_F_SEL] 	&= ~SSCPLL_POW;
	}
	DisableSensorHCLK();	// close sensor hclk
#ifdef _FT2_REMOVE_TEST
#else
	if (byDelayCtrl == EN_DELAYTIME)
	{
		uDelay(1);
	}
#endif
	SensorPowerControl(SWITCH_OFF,byDelayCtrl);

	// pull control sensor pin
	XBYTE[CCS_PIN_PULL_CTL1] = VSYNC_PULL_DOWN| HSYNC_PULL_DOWN |DOUT_PULL_DOWN;
	XBYTE[CCS_PIN_PULL_CTL2] = PWDN_NO_PULL | SYSCLK_PULL_DOWN | PIXCLK_PULL_DOWN;

	g_bySensorIsOpen = SENSOR_CLOSED;
	g_byVDCMDPVLEDOff = 0;
}


// This function is called by Suspend and set interface 0.
void StopImageTransfer(U8 byDelayCtrl)
{
#ifdef _MIPI_EXIST_
	StopMipiAphy();
	XBYTE[MIPI_DPHY_PWDB] = MIPI_CLK_DATA_LANE_PWDB;
	XBYTE[MIPI_DPHY_CTRL] &= ~MIPI_DATA_LANE_ALL_EN;
#else
	XBYTE[CCS_TRANSFER] = CCS_TRANSFER_RESET;
#endif
	XBYTE[ISP_CONTROL0] = ISP_STOP;
	XBYTE[ISP_SIE_CTRL] = SIE_STOP;
	// clear ISP and CCS interrupt
	XBYTE[ISP_INT_FLAG0] = 0xFF;
	XBYTE[ISP_INT_FLAG1] = 0xFF;
	XBYTE[CCS_INT_STS] = 0xFF;

	g_byStartVideo = 0;
	g_byISPAEStaticsEn = 0;
#if  ((defined _ENABLE_ANTI_FLICK_ )||(defined _ENABLE_OUTDOOR_DETECT_))
	g_byISPABStaticsEn = 0;
#endif
	g_byISPAWBStaticsEn = 0;


#ifdef _BLACK_SCREEN_
	StopBackendIspSwitch();
#endif

#ifdef _ENABLE_MJPEG_
	StopJpegEncoder();
#endif

#ifdef _ENABLE_M420_FMT_
	StopM420Module();
#endif

	StopSensor(byDelayCtrl);
#if ((defined _ENABLE_MJPEG_)||(defined _ENABLE_M420_FMT_))
	JPEG_Clock_Switch(SWITCH_OFF);
#endif

	XBYTE[ISP_CLK_HALF_ENABLE] = 0x00;
	ISP_Clock_Switch(SWITCH_OFF);
#ifdef _DELL_EXU_
	g_byStreamActive = 0;
#endif
#ifdef _SENSOR_ESD_ISSUE_
	g_byStreamOn = 0;
	g_wESDTimerCounter = 0;
#endif
	TURN_LED_OFF();

	// hemonel 2009-08-05: add for save still image time
	// when sensor power off, initialize sensor current state
	g_wCurFrameWidth=0;
	g_wCurFrameHeight=0;
	g_byCurFps = 0;
	g_byCurFormat = 0;
	g_wSensorCurFormat = 0;
	g_byFpsLast = 0;
}

static void InitIspInterrupt(void)
{
#if (defined _AF_ENABLE_ || defined _SCENE_DETECTION_)
	XBYTE[ISP_INT_EN0] = ISP_AE_STAT_INT|ISP_AWB_STAT_INT|ISP_AF_STAT_INT;	// enable ISP Statictis interrupt	
#else
	XBYTE[ISP_INT_EN0] = ISP_AE_STAT_INT|ISP_AWB_STAT_INT;
#endif 	

#ifdef _NOLPML1_HANG_
	XBYTE[ISP_INT_EN0] |= ISP_FRAMEABORT_INT;
#endif

#if  ((defined _ENABLE_ANTI_FLICK_ )||(defined _ENABLE_OUTDOOR_DETECT_))
	XBYTE[ISP_INT_EN1] = ISP_FLICK_DETECT_INT;
#endif
		
	if(g_bySensor_YuvMode ==RAW_MODE)
	{
		XBYTE[ISP_INT_EN1] |= ISP_DATA_START_INT;
		XBYTE[ISP_INT_EN0] |= ISP_FRM_START_INT;
	}

	// clear ISP and CCS interrupt
	XBYTE[ISP_INT_FLAG0] = 0xFF;	
	XBYTE[ISP_INT_FLAG1] = 0xFF;	
	XBYTE[CCS_INT_STS] = 0xFF;
}

// This functin is called by StarVideoImage() and StartStillImage().
void StartImageTransfer(void)
{
	// hemonel 2009-11-25: move LED on to StartImageTransfer for fix Chapter9 test LED on bug
#ifdef _DELL_EXU_
	g_byStreamActive = 1;
#else
	
#ifdef _RTK_EXTENDED_CTL_
	if(g_byPreviewLEDOff == 0)
	{
		if (g_byVDCMDPVLEDOff==0)
		{
#ifdef _LENOVO_IDEA_EYE_	
			if (g_byUSBStreaming)
			{
#endif			
				TURN_LED_ON();
#ifdef _LENOVO_IDEA_EYE_	
			}	
#endif	
		}	
	}
	else
	{
		g_byPreviewLEDOff = 0;
	}
#else
	if (g_byVDCMDPVLEDOff==0)
	{
#ifdef _LENOVO_IDEA_EYE_	
		if (g_byUSBStreaming)
		{
#endif		
			TURN_LED_ON();
#ifdef _LENOVO_IDEA_EYE_	
		}	
#endif	
	}	
#endif
#endif

	ISP_Clock_Switch(SWITCH_ON);

#ifndef _MIPI_EXIST_
	//PIXDIN[9:0] and HSYNC select from different Package
#ifdef _PACKAGE_QFN40_
	XBYTE[PIN_SHARE_SEL] = PACKAGE_SEL_QFN40;
#else		//_PACKAGE_QFN32_
	XBYTE[PIN_SHARE_SEL] = PACKAGE_SEL_QFN32;
#endif
#endif

	if(g_bySensorIsOpen == SENSOR_CLOSED)
	{
		Reset_Sensor();
	}

#ifdef _BLACK_SCREEN_
	StartBackendIspSwitch();
#endif

//	// hemonel 2011-12-09: move ISP interrupt enable at start image transfer
//	InitIspInterrupt();
}



void StartVideoTransfer(void )
{

	// Jimmy 2012-06-06: move ISP interrupt enable at StartVideoTransfer
	InitIspInterrupt();
	
	//zouxiaozhi 2012-3-30:CCS_TRANSFER must be configured before enable operation
	if (g_bySensor_YuvMode == MJPG_MODE)
	{
		XBYTE[CCS_CONTROL] = PIXEL_SAMPLE_RISING| VSYNC_ACTIVE_LOW |HSYNC_ACTIVE_HIGH;
		XBYTE[CCS_TRANSFER] = CCS_JPG_BYPASS_EN|CCS_JPG_BYPASS_MODE2;
	}

#ifdef _MIPI_EXIST_
	StartMipiAphy(); 
	XBYTE[MIPI_APHY_REG6] &=~(MIPI_AUTO_SKEW_EN|MIPI_SKEW_EN);		//Close MIPI skew and auto skew
	XBYTE[MIPI_DPHY_PWDB] = g_byMipiDphyCtrl;	//APHY work
	XBYTE[MIPI_DPHY_CTRL] |= g_byMipiDphyCtrl;	//50ohm pull
	Delay(5);						//Wait for 50ohm pull to high speed to auto skew	
	XBYTE[MIPI_APHY_REG4] = 0x00;				//Manumal Skew Value = 0;
	XBYTE[MIPI_APHY_REG5] = 0x00;
	XBYTE[MIPI_APHY_REG6] |= MIPI_SKEW_EN;		//Enable MIPI skew and manumal skew	
	//Jimmy.20130129.Fix use manumal skew replace auto skew
	/*
	StartMipiAphy(); 
	XBYTE[MIPI_APHY_REG6] &=~(MIPI_AUTO_SKEW_EN|MIPI_SKEW_EN);		//Close MIPI skew and auto skew
	XBYTE[MIPI_DPHY_PWDB] = g_byMipiDphyCtrl;	//APHY work
	XBYTE[MIPI_DPHY_CTRL] |= g_byMipiDphyCtrl;	//50ohm pull
	Delay(5);						//Wait for 50ohm pull to high speed to auto skew
	XBYTE[MIPI_APHY_REG6] |= (MIPI_AUTO_SKEW_EN|MIPI_SKEW_EN);		//Enable MIPI skew and auto skew
	if (MIPI_DATA_LANE0_EN == (g_byMipiDphyCtrl&MIPI_DATA_LANE0_EN))	//Wait Lane0 auto skew finished
	{
		WaitTimeOut(MIPI_APHY_REG18, LANE0_AUTOSKEW_DONE, 1, 2);
	}
	if (MIPI_DATA_LANE1_EN == (g_byMipiDphyCtrl&MIPI_DATA_LANE1_EN))	////Wait Lane1 auto skew finished
	{
		WaitTimeOut(MIPI_APHY_REG18, LANE1_AUTOSKEW_DONE, 1, 2);
	}	
	if ((((XBYTE[MIPI_APHY_REG15]&0x0F) + (XBYTE[MIPI_APHY_REG15]>>4)) > 0x0A) 
		||(((XBYTE[MIPI_APHY_REG16]&0x0F) + (XBYTE[MIPI_APHY_REG16]>>4)) > 0x0A)) //Auto skew value range <0x0C
	{
		XBYTE[MIPI_APHY_REG4] = 0x00;						//Manumal Skew Value = 0;
		XBYTE[MIPI_APHY_REG5] = 0x00;
		XBYTE[MIPI_APHY_REG6] &=~MIPI_AUTO_SKEW_EN;		//Enable MIPI skew and manumal skew
	}
	*/
#else	
	if (g_bySensor_YuvMode == MJPG_MODE)
	{
		//XBYTE[CCS_CONTROL] = PIXEL_SAMPLE_RISING| VSYNC_ACTIVE_LOW |HSYNC_ACTIVE_HIGH;
		//XBYTE[CCS_TRANSFER] = CCS_TRANSFER_EN|CCS_JPG_BYPASS_EN|CCS_JPG_BYPASS_MODE2;
		XBYTE[CCS_TRANSFER] |= CCS_TRANSFER_EN;
	}
	else
	{
		XBYTE[CCS_TRANSFER] = CCS_TRANSFER_EN;
	}
#endif
	//Jimmy.20130524.Separate from this function for still image delay function,which wait ISP_DATA_START_INT
	//need to first start mipi/ccs to transmit data and drop 1 frame,then to enable them
	// because MIPI data is wrong possible at first, we start data transfer at here
	//XBYTE[ISP_SIE_CTRL] = SIE_START;
	//XBYTE[ISP_CONTROL0] = byTransfer ;
}

void StartVideoImage(U8 byUSBStreaming)
{
	U16 wWidth;
	U16 wHeight;
	U8 byFormat;

#ifdef _LENOVO_IDEA_EYE_	
	IDEAEYE_MSG(("StartVideoImage:%bd\n", byUSBStreaming));

	if (g_byStartVideo)
	{
		StopImageTransfer(EN_DELAYTIME);
		g_byStartVideo = 0;
	}		

	if (byUSBStreaming)
	{	
		g_byMTDStartGetFrame = 0;
	}	

	g_byUSBStreaming = byUSBStreaming;
#endif

#ifdef _STILL_IMG_BACKUP_SETTING_
	// hemonel 2009-11-18: add for fast WIA capture
	g_byIsAEAWBFixed = 0;
#endif
	wWidth =  GetFrameWidth(1,g_VsCommit.bFormatIndex, g_VsCommit.bFrameIndex,(U8)g_bIsHighSpeed);
	wHeight = GetFrameHeight(1,g_VsCommit.bFormatIndex,g_VsCommit.bFrameIndex,(U8)g_bIsHighSpeed);
	byFormat = GetFomatType(g_VsCommit.bFormatIndex, g_bIsHighSpeed);

#ifdef _ENABLE_MJPEG_
	if((byFormat == FORMAT_TYPE_MJPG ) && (!(g_wSensorMJPGByPass&GetAppropriateSensorFormat(wWidth, wHeight))))	
	{
		g_wCurMJPEGOutputWidth = wWidth;
		g_wCurMJPEGOutputHeight = wHeight;
		
		if((wWidth%16) != 0)	//MJPEG Module input widht must a multiple of 16
		{
			wWidth += (16 - (wWidth%16));
		}
		
		if((wHeight%8) != 0)	//MJPEG Module input height must a multiple of 8
		{
			wHeight += (8 - (wHeight%8));
		}
	}
#endif

	StartImageTransfer();
	// hemonel 2009-08-05: add for save time at still image
	// if format and resolution and fps not change, do not set sensor register setting
	if((wWidth !=g_wCurFrameWidth)||(wHeight != g_wCurFrameHeight)||(byFormat != g_byCurFormat)||(g_byCommitFPS != g_byCurFps))
	{
		g_wCurFrameWidth = wWidth;
		g_wCurFrameHeight =wHeight;
		g_byCurFormat = byFormat;
		g_byCurFps = g_byCommitFPS;

		SetFrameSize(wWidth, wHeight);

		StartVideoTransfer();

#ifdef _STILL_IMG_BACKUP_SETTING_
		CaptureToPreview();
#endif

#ifdef _FT2_REMOVE_TEST
#else
		if(g_bySensor_YuvMode ==RAW_MODE)	// hemonel 2009-07-16: raw mode use AE
		{
			if(g_byFirstPreview == 0)
			{
#ifndef _LENOVO_IDEA_EYE_				
				// ISP_DATA_DELAY should be set before ISP_TRANSFER_START
				XBYTE[ISP_DATA_ENABLE] = 0;
				XBYTE[ISP_DATA_DELAY] = 0x80|g_byPreviewDropFrameNumber;
#else		

				WaitTimeOut(ISP_INT_FLAG1,ISP_DATA_START_INT, ISP_DATA_START_INT, 10);	//Jimmy.20130524.at least delay 1 frame.
#endif

				g_byFirstPreview = 1;
			}
			else
			{
#ifndef _LENOVO_IDEA_EYE_	
				//Jimmy.20140411.Note
				//when this delay not finished, but host set still trigger will stop frame and go to stillimage
				//no still image will transferred.this change to fix superfast press stillpin.	
				//FristPreview have problem now, now not edit it.
				//XBYTE[ISP_DATA_ENABLE] = 0;
				//XBYTE[ISP_DATA_DELAY] = 0x80;	
				XBYTE[ISP_INT_FLAG1] = ISP_DATA_START_INT;
				WaitTimeOut(ISP_INT_FLAG1,ISP_DATA_START_INT, ISP_DATA_START_INT, 50);	//Jimmy.20130524.at least delay 1 frame.
#else		
				WaitTimeOut(ISP_INT_FLAG1,ISP_DATA_START_INT, ISP_DATA_START_INT, 10);	//Jimmy.20130524.at least delay 1 frame.
#endif
			}	
		}
		else
		{
			VideoDelay(g_byPreviewDelayTimeMax, g_byPreviewDropFrameNumber, g_byCommitFPS);
		}
#endif
	}
	// hemonel 2010-11-09: fix video preview hang after still image at the same MJPEG resolution of still and preview.
#ifdef _ENABLE_MJPEG_
	else if((byFormat ==FORMAT_TYPE_MJPG)&&(g_bySensor_YuvMode != MJPG_MODE))
	{
		JPEG_Clock_Switch(SWITCH_ON);
		StartJpegEncoder(JPEG_VIDEO_SEL|JPEG_SET);
		StartVideoTransfer();
	}
#endif
#ifdef _ENABLE_M420_FMT_
	else if(byFormat==FORMAT_TYPE_M420)
	{
		JPEG_Clock_Switch(SWITCH_ON);
		StartM420Module();
		StartVideoTransfer();
	}
#endif
	else
	{
		StartVideoTransfer();		
	}

	if(g_bySensor_YuvMode ==RAW_MODE)	// hemonel 2009-07-16: raw mode use AE
	{
		g_byISPAEStaticsEn = 1;
#if  ((defined _ENABLE_ANTI_FLICK_ )||(defined _ENABLE_OUTDOOR_DETECT_))
		g_byISPABStaticsEn = 1;
#endif
		g_byISPAWBStaticsEn = 1;
	}

	//StartVideoTransfer();
	
#ifdef _LENOVO_IDEA_EYE_
	if (byUSBStreaming == EN_USB_STREAMING)
	{
#endif
		XBYTE[ISP_SIE_CTRL] = SIE_START;
#ifdef _LENOVO_IDEA_EYE_
	}	
#endif	
	XBYTE[ISP_CONTROL0] = ISP_TRANSFER_START;
	
#if (defined _AF_ENABLE_ || defined _SCENE_DETECTION_)
#ifdef _CUSTOMER_ISP_
	CusAFStaticsStart();
#else
	ISP_AFStaticsStart();
#endif
#endif

	// AWB Gain auto load when isp start now
	g_byStartVideo = 1;
	g_byAEForce_Adjust = 0;
	g_byFirstStatic = 1;
#ifdef _SENSOR_ESD_ISSUE_
	g_byStreamOn = 1;
	g_wESDTimerCounter = 0;	
#endif
}

void StartStillImage(void )
{
	U16 wWidth;
	U16 wHeight;
	U8 byFormat;

	// hemonel 2009-01-21: DTM blink test bug at OV9665: still image trigger can come before in token.
	wWidth =  GetFrameWidth(0,g_VsStlCommit.bFormatIndex,g_VsStlCommit.bFrameIndex,(U8)g_bIsHighSpeed);
	wHeight = GetFrameHeight(0,g_VsStlCommit.bFormatIndex,g_VsStlCommit.bFrameIndex,(U8)g_bIsHighSpeed);
	byFormat = GetFomatType(g_VsStlCommit.bFormatIndex, g_bIsHighSpeed);

#ifdef _ENABLE_MJPEG_
	if((byFormat == FORMAT_TYPE_MJPG) && (!(g_wSensorMJPGByPass&GetAppropriateSensorFormat(wWidth, wHeight))))
	{
		g_wCurMJPEGOutputWidth = wWidth;
		g_wCurMJPEGOutputHeight = wHeight;
		
		if((wWidth%16) != 0)	//MJPEG Module input widht must a multiple of 16
		{
			wWidth += (16 - (wWidth%16));
		}
		
		if((wHeight%8) != 0)	//MJPEG Module input height must a multiple of 8
		{
			wHeight += (8 - (wHeight%8));
		}
	}
#endif

	StartImageTransfer();

	// hemonel 2009-08-05: add for save time at still image
	// if format and resolution not change, do not set sensor register setting
	if((wWidth !=g_wCurFrameWidth)||(wHeight != g_wCurFrameHeight)||(byFormat != g_byCurFormat))
	{
		g_byStillFPS = GetDefaultFPS(UVC_IS_STILL,g_VsStlCommit.bFormatIndex, g_VsStlCommit.bFrameIndex, (U8)g_bIsHighSpeed);

#ifdef _STILL_IMG_BACKUP_SETTING_
		PreviewToCapture(g_wCurFrameWidth, wWidth);
#endif
		g_wCurFrameWidth = wWidth;
		g_wCurFrameHeight =wHeight;
		g_byCurFormat = byFormat;
		g_byCurFps = g_byStillFPS;

		SetFrameSize(wWidth, wHeight);
		//Jimmy.20130524.first start mipi/ccs to transmit data and drop 1 frame,which wait ISP_DATA_START_INTthen to enable isp and sie
		StartVideoTransfer();
		// hemonel 2011-02-18: decrease still image delay time at raw mode because exposure time and white balance use the last value at preview
		if (g_bySensor_YuvMode == RAW_MODE)
		{
			XBYTE[ISP_INT_FLAG1] = ISP_DATA_START_INT;
			WaitTimeOut(ISP_INT_FLAG1,ISP_DATA_START_INT, ISP_DATA_START_INT, 50);	//Jimmy.20130524.at least delay 1 frame.
		}
		else
		{
			VideoDelay(g_byStillimgDelayTimeMax, g_byStillimgDropFrameNumber, g_byStillFPS);
		}		
	}
	else
	{
		StartVideoTransfer();
	}

	XBYTE[PH_INFO] |= PH_INFO_STILL_IMG;	
	//StartVideoTransfer(ISP_TRANSFER_START | ISP_TRANSFER_GET_STILLIMG);
	XBYTE[ISP_SIE_CTRL] = SIE_START;
	XBYTE[ISP_CONTROL0] = ISP_TRANSFER_START | ISP_TRANSFER_GET_STILLIMG ;	
	// AWB Gain auto load when isp start now
	//XBYTE[ISP_AWB_GAIN_CTRL] = ISP_AWB_LOADGAIN; // hemonel 2010-11-05: fix start green because AWB gain load after ISP start
	// hemonel 2008-02-27
	WaitTimeOut(ISP_CONTROL0, ISP_TRANSFER_GET_STILLIMG, 0, 100);	// wait ISP busy
	WaitTimeOut(EPA_STAT, EPA_FIFO_EMPTY, 1, 10);	// wait SIE FIFO Empty
	WaitTimeOut_Delay(1);
	
#ifdef _MIPI_EXIST_
	XBYTE[MIPI_DPHY_CTRL] &= ~MIPI_DATA_LANE_ALL_EN;
	XBYTE[MIPI_DPHY_PWDB] = MIPI_CLK_DATA_LANE_PWDB;
#else
	XBYTE[CCS_TRANSFER] = CCS_TRANSFER_RESET;
#endif
	XBYTE[ISP_CONTROL0] = ISP_STOP ;		
	XBYTE[ISP_SIE_CTRL] = SIE_STOP; 
	XBYTE[PH_INFO] &= (~PH_INFO_STILL_IMG);
	XBYTE[EPA_CTL] |= EPA_FIFO_FLUSH;

	// video variable initialization
	g_byStartVideo = 0;
	g_byISPAEStaticsEn = 0;
#if  ((defined _ENABLE_ANTI_FLICK_ )||(defined _ENABLE_OUTDOOR_DETECT_))
	g_byISPABStaticsEn = 0;
#endif
	g_byISPAWBStaticsEn = 0;

#ifdef _ENABLE_M420_FMT_
	StopM420Module();
#endif
#ifdef _ENABLE_MJPEG_
	StopJpegEncoder();
#endif
#if (defined _ENABLE_MJPEG_ || defined _ENABLE_M420_FMT_)
	JPEG_Clock_Switch(SWITCH_OFF); 
#endif
}


#ifdef _UAC_EXIST_
void StartAudioTransfer(void)
{
	XBYTE[EPB_CTL] = EPB_FIFO_FLUSH;
	XBYTE[EPB_FLOW_CTL] = EPB_TRANSFER_EN;
}
#endif

#ifdef _SENSOR_ESD_ISSUE_
void ResetTransfer(void)
{
	XBYTE[ISP_CONTROL0] = ISP_STOP;
	XBYTE[ISP_SIE_CTRL] = SIE_STOP; 
	XBYTE[EPA_CTL] |= EPA_FIFO_FLUSH;	
	g_wCurFrameWidth = 0;
	g_wCurFrameHeight = 0;
	g_byCurFormat = 0;
	g_bySensorIsOpen = 0;
	g_byFpsLast = 0;
	g_wSensorCurFormat = 0;
	
#ifdef _USB2_LPM_
	XBYTE[LPM_CFG] |= FORCE_FW_REMOTE_WAKEUP; 
#endif	
	XBYTE[EPA_IRQSTAT] = EPA_IRQ_IN_TOKEN;
	XBYTE[EPA_IRQEN] |= EPA_IRQ_IN_TOKEN;
}
#endif

#ifdef _LENOVO_IDEA_EYE_

void StartBackgroundStreaming(void)
{
	IDEAEYE_MSG(("StartBackgroundStreaming\n"));

	g_byMTDDetectBackend = 1;

	if(!g_byStartVideo)
	{
		StartVideoBackend();
	}
}

void StartVideoBackend(void)
{
	IDEAEYE_MSG(("StartVideoBackend\n"));

	g_byMTDStartGetFrame=1;
	g_VsCommit.bFormatIndex =  1;
	g_VsCommit.bFrameIndex = 1;
	g_byCommitFPS = 5;	
}

void StopBackgroundStreaming(void)
{
	IDEAEYE_MSG(("StopBackgroundStreaming\n"));

	g_byMTDDetectBackend = 0;
	
	if (g_byStartVideo&& g_byMTDStartGetFrame)
	{
		StopImageTransfer(EN_DELAYTIME);
	}	

	g_byMTDStartGetFrame = 0;
	NotifyHostNoMotionDetected();
}

void NotifyHostMotionDetected(void)
{
	XBYTE[PG_DELINK_CTRL] |= PAD_DELINK_OUT_DRIVE_HIGH;	
}	

void NotifyHostNoMotionDetected(void)
{
	XBYTE[PG_DELINK_CTRL] &= ~PAD_DELINK_OUT_DRIVE_HIGH;	
}	
#endif


