#include "pc_cam.h"
#include "camutil.h"
#include "CamReg.h"
#include "CamUvc.h"
#include "CamCtl.h"
//#include "camisp.h"
//#include "CamSensor.h"
//#include "CamI2C.h"
//#include "CamProcess.h"
#include "Global_vars.h"
#include "ISP_vars.h"
#include "CamVdCfg.h"

#ifdef _ENABLE_MJPEG_
U8 code g_aJPegQtableLowQ[]=
{

	/*	0x10, 0x0A, 0x0A, 0x10, 0x18, 0x28, 0x32, 0x3C,
		0x0C, 0x0C, 0x0E, 0x12, 0x1A, 0x3A, 0x3C, 0x36,
		0x0E, 0x0C, 0x10, 0x18, 0x28, 0x38, 0x44, 0x38,
		0x0E, 0x10, 0x16, 0x1C, 0x32, 0x56, 0x50, 0x3E,
		0x12, 0x16, 0x24, 0x38, 0x44, 0x6C, 0x66, 0x4C,
		0x18, 0x22, 0x36, 0x40, 0x50, 0x68, 0x70, 0x5C,
		0x30, 0x40, 0x4E, 0x56, 0x66, 0x78, 0x78, 0x64,
		0x48, 0x5C, 0x5E, 0x62, 0x70, 0x64, 0x66, 0x62,

		0x10, 0x12, 0x18, 0x2E, 0x62, 0x62, 0x62, 0x62,
		0x12, 0x14, 0x1A, 0x42, 0x62, 0x62, 0x62, 0x62,
		0x18, 0x1A, 0x38, 0x62, 0x62, 0x62, 0x62, 0x62,
		0x2E, 0x42, 0x62, 0x62,0x62, 0x62, 0x62, 0x62,
		0x62, 0x62, 0x62, 0x62, 0x62, 0x62, 0x62, 0x62,
		0x62, 0x62, 0x62, 0x62, 0x62, 0x62, 0x62, 0x62,
		0x62, 0x62, 0x62, 0x62, 0x62, 0x62, 0x62, 0x62,
		0x62, 0x62, 0x62, 0x62, 0x62, 0x62, 0x62, 0x62,*/
	0x10, 0x0A, 0x0C, 0x0E, 0x0C, 0x0A, 0x10, 0x0e,
	0x0c, 0x0e, 0x12, 0x10, 0x10, 0x12, 0x18, 0x28,
	0x1a, 0x18, 0x16, 0x16, 0x18, 0x30, 0x22, 0x24,
	0x1c, 0x28, 0x3a, 0x32, 0x3c, 0x3c, 0x38, 0x32,
	0x38, 0x36, 0x40, 0x48, 0x5c, 0x4e, 0x40, 0x44,
	0x56, 0x44, 0x36, 0x38, 0x50, 0x6c, 0x50, 0x56,
	0x5e, 0x62, 0x66, 0x68, 0x66, 0x3e, 0x4c, 0x70,
	0x78, 0x70, 0x64, 0x78, 0x5c, 0x64, 0x66, 0x62,
	
	0x10, 0x12, 0x12, 0x18, 0x14, 0x18, 0x2e, 0x1a,
	0x1a, 0x2e, 0x62, 0x42, 0x38, 0x42, 0x62, 0x62,
	0x62, 0x62, 0x62, 0x62, 0x62, 0x62, 0x62, 0x62,
	0x62, 0x62, 0x62, 0x62, 0x62, 0x62, 0x62, 0x62,
	0x62, 0x62, 0x62, 0x62, 0x62, 0x62, 0x62, 0x62,
	0x62, 0x62, 0x62, 0x62, 0x62, 0x62, 0x62, 0x62,
	0x62, 0x62, 0x62, 0x62, 0x62, 0x62, 0x62, 0x62,
	0x62, 0x62, 0x62, 0x62, 0x62, 0x62, 0x62, 0x62
};

// hemonel 2010-08-27: optimize code
/*
U32 JPEG_Qtable_read(U8 addr)
{
	U32 dwTmp;
	XBYTE[JENC_QTABLE_ADDR] = addr;
	XBYTE[JENC_QTABLE_ACCESS] = JPEG_QT_READ;
	ASSIGN_LONG(dwTmp, 0,XBYTE[JENC_QTABLE_DATA2],XBYTE[JENC_QTABLE_DATA1],XBYTE[JENC_QTABLE_DATA0]);
	return dwTmp;
}
*/

static void JPEG_Qtable_write(U8 byAddr,U32 dwTableItem)
{
	XBYTE[JENC_QTABLE_DATA2]=LONG2CHAR(dwTableItem, 2);
	XBYTE[JENC_QTABLE_DATA1]=LONG2CHAR(dwTableItem, 1);
	XBYTE[JENC_QTABLE_DATA0]=LONG2CHAR(dwTableItem, 0);
	XBYTE[JENC_QTABLE_ADDR] = byAddr;
	XBYTE[JENC_QTABLE_ACCESS] = JPEG_QT_WRITE;
}

/*********************************************************
//
//    (1<<(E+11))/Q  =  M
*********************************************************/
static U32 CalQtableItem(U8 byQ)
{
	U32 dwM;
	S8 data i;

	if(byQ<2) //Q<=1   E=0, M=2^11 -1
	{
		byQ = 2;
	}

	for(i=7; i>=0; i--)
	{
		dwM = (((U32)1)<<(i+11)) /  byQ ;
		if(dwM<((U32)2048))
		{
			return ( (((U32)i)<<19) | (dwM<<8)|((U32)byQ));
		}
	}

	return 0;
}

void InitJpegTable(void)
{
	U8 data bytmp;
	XBYTE[JENC_START]=JPEG_MODULE_RST;

	for(bytmp=0; bytmp<128; bytmp++)
	{
		JPEG_Qtable_write(bytmp, CalQtableItem(ClipWord((((U16)g_aJPegQtableLowQ[bytmp] )*g_byQtableScale)>>5, 1, 254)));
	}

	g_byQtableCurScale = g_byQtableScale;

	XBYTE[ISP_JPEG_ACRA_ENBALE] =g_byJpeg_ACRA_En;

	if (g_byJpeg_ACRA_En == 0)
	{
		XBYTE[JENC_ACRA_RATE] = 0x22;
		XBYTE[JENC_ACRA_INV_HIGH_L] = 0xff;
		XBYTE[JENC_ACRA_INV_HIGH_H] = 0xff;
		XBYTE[JENC_ACRA_INV_NORM_L] = 0xff;
		XBYTE[JENC_ACRA_INV_NORM_H] = 0xff;
		XBYTE[ISP_ACRA_RATE_LOAD] = 0x01;
	}	
}

static void SetJpegModule(void)
{
	//U8 data bytmp;
	U16 data wTemp;

	XBYTE[JENC_START]=JPEG_MODULE_RST;
	XBYTE[JENC_CONFIGURE]= JPEG_CFG_CMPONT_NUM3
	                       |JPEG_CFG_QT_NUM2
	                       |JPEG_CFG_HFT_EXIST
	                       |JPEG_CFG_MRK_RESTRT_EN	// hemonel 2011-06-23: WLP requirement that insert a Restart Marker for every 8 scanned lines when encoding YUV422. But linux LUvcView AP compatiable need disable this bit.
	                       |JPEG_CFG_CLR_COMP_NUM3;

	XBYTE[JENC_COMPONENT0_H] = (2<<4)|1;//Y  2:1
	XBYTE[JENC_COMPONENT0_L] =  0x10;

	XBYTE[JENC_COMPONENT1_H] = (1<<4)|1;//U 1:1
	XBYTE[JENC_COMPONENT1_L] =  0x07;

	XBYTE[JENC_COMPONENT2_H] = (1<<4)|1;//V 1:1
	XBYTE[JENC_COMPONENT2_L] =  0x07;

	/*
	if(type==JPEG_DATA_TYPE_VSTREAM)
	{
		wWidth =  GetFrameWidth(1,g_VsCommit.bFormatIndex, g_VsCommit.bFrameIndex,(U8)g_bIsHighSpeed);
		wHeight = GetFrameHeight(1,g_VsCommit.bFormatIndex ,g_VsCommit.bFrameIndex,(U8)g_bIsHighSpeed);
	}
	else// (type==JPEG_DATA_TYPE_STILLIMG)
	{
		wWidth =  GetFrameWidth(0,g_VsStlCommit.bFormatIndex,g_VsStlCommit.bFrameIndex,(U8)g_bIsHighSpeed);
		wHeight = GetFrameHeight(0,g_VsStlCommit.bFormatIndex,g_VsStlCommit.bFrameIndex,(U8)g_bIsHighSpeed);
	}
	*/
	XBYTE[JENC_IMAGESIZE_XL] = INT2CHAR(g_wCurMJPEGOutputWidth,0);
	XBYTE[JENC_IMAGESIZE_XH] = INT2CHAR(g_wCurMJPEGOutputWidth,1);

	XBYTE[JENC_IMAGESIZE_YL] = INT2CHAR(g_wCurMJPEGOutputHeight,0);
	XBYTE[JENC_IMAGESIZE_YH] = INT2CHAR(g_wCurMJPEGOutputHeight,1);

	XBYTE[JENC_FRAMEWIDTH_L] = INT2CHAR(g_wCurFrameWidth,0);
	XBYTE[JENC_FRAMEWIDTH_H] = INT2CHAR(g_wCurFrameWidth,1);

	XBYTE[JENC_FRAMEHEIGHT_L] = INT2CHAR(g_wCurFrameHeight,0);
	XBYTE[JENC_FRAMEHEIGHT_H] = INT2CHAR(g_wCurFrameHeight,1);

	wTemp =  (g_wCurFrameWidth>>4) * (g_wCurFrameHeight>>3)   -1;
	XBYTE[JENC_MCUNUM_L]  = INT2CHAR(wTemp,0);
	XBYTE[JENC_MCUNUM_H] = INT2CHAR(wTemp,1);
	
	// hemonel 2011-06-23: WLP requirement that insert a Restart Marker for every 8 scanned lines when encoding YUV422
	wTemp = (g_wCurFrameWidth>>4) - 1;
	XBYTE[JENC_RST_INTERVAL_L] = INT2CHAR(wTemp,0);
	XBYTE[JENC_RST_INTERVAL_H] = INT2CHAR(wTemp,1);	
	
	//need tune ......
	XBYTE[JENC_FRAME_INTERVAL_L] = 0x00;
	XBYTE[JENC_FRAME_INTERVAL_H] = 0X02;

}

void StartJpegEncoder(U8 byMode)
{
	SetJpegModule();	
	XBYTE[JENC_START]= byMode;
}

void StopJpegEncoder(void)
{
	XBYTE[JENC_START] = JPEG_CLEAR;
	XBYTE[JENC_START] = JPEG_MODULE_RST;
}
#endif //_ENABLE_MJPEG_

