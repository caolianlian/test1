#include "pc_cam.h"
#include "camutil.h"
#include "CamReg.h"
#include "CamUvc.h"
#include "CamCtl.h"
//#include "CamSensor.h"
#include "CamI2C.h"
#include "Global_vars.h"


// Initialize I2C Controller Timing
void Init_I2C(void)
{
	START_I2C_CLK();
	//I2C interface should be open drain and pull up by external power
	// hemonel 2011-06-07: use internal pull up 2Kohm resistance to SVIO
	XBYTE[I2C_PULL_CTL] = SDA_PULL_HIGH | SCL_PULL_HIGH;
	
	//////////////////////////////////////////
	// sensor timing
	//////////////////////////////////////////
	// Mobien I2C timing: SCL low 1.2us, high 1.0us, Data hold 1.3us, start/stop setup 1.2us, hold time 1.0us
	// Omnivision SCCB timing: SCL low 1.3us, high 0.6us, data hold 0uS, data setup time 0.1us, start/stop setup 0.6us, hold time 0.6us, bus free 1.3us
	// Samsung  	I2c timing :  SCL low 1.3us, high 0.6, data hold 10ns, data setup time 0.1us, start/stop setup 0.6us, hold time 0.6us, bus free 1.3us

	// Final: SCL low 1.4uS, high 1.1uS; Data Setup 0.7uS, Data Hold 0.7uS,;start/stop setup 1.4uS, hold 0.7uS;free 0.7+1.4uS
	// 394Khz
	
	//When MCU Run at 120/60/30/15MHz, HW I2C Module Clock fix at 15Mhz;
	//When MCU run <15Mhz, I2C Module clock equal to mcu clock
	//Default not need to setting them,HW have the same default value.
	//XBYTE[I2C_TCTL_SCL_LOW]  = 0x2A;		// 1.4 us, 42 cycles
	//XBYTE[I2C_TCTL_SCL_HIGH] = 0x22;		// 1.1us, 34 cycles
	//XBYTE[I2C_TCTL_DAT_HD] = 0x15;		// 0.7us, 21 cycles, setup time 0.7us
	//XBYTE[I2C_TCTL_SX_SU] = 0x2A;		// 1.4us, 42 cycles
	//XBYTE[I2C_TCTL_SX_HD] = 0x15;		// 0.7us, 21 cycles


	STOP_I2C_CLK();
}

bit I2C_CheckTransferEnd(void)
{
	if(!WaitTimeOut(I2C_TRANSFER, I2C_TRANSFER_END, 1, 10))
	{
		// timeout
		XBYTE[I2C_TRANSFER] = I2C_TRANSFER_STOP;
		return FALSE;
	}

	// hemonel 2008-03-20: error process
	if(XBYTE[I2C_STATUS]&I2C_TRANSFER_ERROR)
	{
		return FALSE;
	}

	return TRUE;
}

static bit I2C_SetAddressByte(U8 const byID_Addr, U8 const byAddr)
{
	XBYTE[I2C_ADDR0] = byID_Addr | I2C_WRITE_OP;
	XBYTE[I2C_ADDR1] = byAddr;
	XBYTE[I2C_ADDR_LEN] = 0x02;

	XBYTE[I2C_TRANSFER] = I2C_TRANSFER_START | I2C_SET_ADDRESS_MODE;

	return I2C_CheckTransferEnd();
}

static bit I2C_ReadMode(U8 const byID_Addr, U16 * const wData, U8 const byLen)
{
	bit ret;

	// setting sensor ID address
	XBYTE[I2C_ADDR0] = byID_Addr | I2C_READ_OP;
	XBYTE[I2C_ADDR_LEN] = 0x01;

	// setting read length
	XBYTE[I2C_DATA_LEN] = byLen;
	XBYTE[I2C_TRANSFER] = I2C_TRANSFER_START | I2C_READ_MODE;

	ret = I2C_CheckTransferEnd();

	if(ret == TRUE)
	{
		if(byLen == 2)
		{
			ASSIGN_INT(*wData, XBYTE[I2C_DATA_SCRATCH0], XBYTE[I2C_DATA_SCRATCH1]);	// I2C_DATA0 save MSByte of register
		}
		else
		{
			*wData = XBYTE[I2C_DATA_SCRATCH0];
		}
	}
	
	return ret;
}

static bit I2C_WriteMode(U16 const wData, U8 const byLen)
{
	// setting data to write to register
	if(byLen == 2)
	{
		XBYTE[I2C_DATA_SCRATCH0] = INT2CHAR(wData, 1);
		XBYTE[I2C_DATA_SCRATCH1] = INT2CHAR(wData, 0);
	}
	else		// byte len = 1
	{
		XBYTE[I2C_DATA_SCRATCH0] = INT2CHAR(wData, 0);
	}
	XBYTE[I2C_DATA_LEN] = byLen;
	XBYTE[I2C_TRANSFER] = I2C_TRANSFER_START | I2C_WRITE_MODE;

	return I2C_CheckTransferEnd();
}

// First, set address. Secondly, read register
static bit I2C_ReadByte(U8 byID_Addr, U8 byAddr, U16 * wData, U8 byLen)
{
	bit ret=FALSE;
	
	START_I2C_CLK();

	// setting sensor register address
	if(I2C_SetAddressByte(byID_Addr, byAddr) == TRUE)
	{
		ret = I2C_ReadMode(byID_Addr,wData, byLen);
	}	

	STOP_I2C_CLK();
	return ret;
}

static bit I2C_WriteByte(U8 byID_Addr, U8 const byAddr, U16 const wData, U8 const byLen)
{
	// MICRON sensor: register 16-bit wide, I2C r/w MSByte first
	// Other sensor: register 8-bit wide, I2C r/w msbit first
	bit ret;
	
	START_I2C_CLK();

	// setting sensor ID address and sensor register address
	XBYTE[I2C_ADDR0] = byID_Addr | I2C_WRITE_OP;
	XBYTE[I2C_ADDR1] = byAddr;
	XBYTE[I2C_ADDR_LEN] = 0x02;

	ret = I2C_WriteMode(wData, byLen);
	
	STOP_I2C_CLK();
	return ret;
}

static bit I2C_SetAddressWord(U8 const byID_Addr, U16 const wAddr)
{
	XBYTE[I2C_ADDR0] = byID_Addr | I2C_WRITE_OP;
	XBYTE[I2C_ADDR1] = wAddr>>8;	// high address first
	XBYTE[I2C_ADDR2] = (U8)wAddr;	// low address second
	XBYTE[I2C_ADDR_LEN] = 0x03;

	XBYTE[I2C_TRANSFER] = I2C_TRANSFER_START | I2C_SET_ADDRESS_MODE;

	return I2C_CheckTransferEnd();
}

// First, set address. Secondly, read register
static bit I2C_ReadWord(U8 byID_Addr, U16 wAddr, U16 * wData, U8 byLen)
{
	bit ret=FALSE;
	
	START_I2C_CLK();
	
	// setting sensor register address
	if(I2C_SetAddressWord(byID_Addr, wAddr) == TRUE)
	{
		ret = I2C_ReadMode(byID_Addr,wData, byLen);
	}

	STOP_I2C_CLK();
	return ret;
}

bit I2C_WriteWord(U8 const byID_Addr, U16 const wAddr, U16 const wData, U8 const byLen)
{
	// MICRON sensor: register 16-bit wide, I2C r/w MSByte first
	// Other sensor: register 8-bit wide, I2C r/w msbit first
	bit ret;
	
	START_I2C_CLK();	

	// setting sensor ID address and sensor register address
	XBYTE[I2C_ADDR0] = byID_Addr | I2C_WRITE_OP;
	XBYTE[I2C_ADDR1] = wAddr>>8;	// high address first
	XBYTE[I2C_ADDR2] = (U8)wAddr;	// low address second
	XBYTE[I2C_ADDR_LEN] = 0x03;
	
	ret = I2C_WriteMode(wData, byLen);
	STOP_I2C_CLK();
	return ret;
}

// wAddr: high address: register page address, low address: register address in page
static bit I2C_ReadPage(U8 byID_Addr,U8 const byPageSelAddr,U16 const wAddr, U16 * const pwValue, U8 const byLen)
{
	if(I2C_WriteByte(byID_Addr,byPageSelAddr, INT2CHAR(wAddr, 1), byLen)==FALSE)
	{
		return FALSE	;
	}

	return I2C_ReadByte(byID_Addr,INT2CHAR(wAddr, 0), pwValue, byLen);
}

static bit I2C_WritePage(U8 byID_Addr,U8 const  byPageSelAddr,U16 const wAddr, U16 const wValue, U8 const byLen)
{
	if(I2C_WriteByte(byID_Addr,byPageSelAddr, INT2CHAR(wAddr, 1), byLen)==FALSE)
	{
		return FALSE	;
	}

	return I2C_WriteByte(byID_Addr,INT2CHAR(wAddr, 0), wValue, byLen);
}


bit Read_SenReg(U16 address, U16 * pwData)
{
	U8 data byTmp;
	
	byTmp = g_SnrRegAccessProp.byMode_DataWidth & I2C_ACCESS_DATAWIDTH_MASK;
	switch(g_SnrRegAccessProp.byMode_DataWidth & I2C_ADDR_MODE_MASK)
	{
		case I2C_ADDR_MODE_WA:
			return I2C_ReadWord(g_SnrRegAccessProp.byI2CID, address,pwData,byTmp);
			//break;
		case I2C_ADDR_MODE_PA:
			return I2C_ReadPage(g_SnrRegAccessProp.byI2CID, g_SnrRegAccessProp.byI2CPageSelAddr,address,pwData,byTmp);
			//break;
		case I2C_ADDR_MODE_BA:
		default:
			return I2C_ReadByte(g_SnrRegAccessProp.byI2CID,address,pwData,byTmp);
			//break;
	}
}

bit Write_SenReg(U16 address, U16 wData)
{
	U8 data byTmp;
	
	byTmp = g_SnrRegAccessProp.byMode_DataWidth & I2C_ACCESS_DATAWIDTH_MASK;
	switch(g_SnrRegAccessProp.byMode_DataWidth & I2C_ADDR_MODE_MASK)
	{
		case I2C_ADDR_MODE_WA:
			return I2C_WriteWord(g_SnrRegAccessProp.byI2CID, address,wData,byTmp);
			//break;
		case I2C_ADDR_MODE_PA:
			return I2C_WritePage(g_SnrRegAccessProp.byI2CID,g_SnrRegAccessProp.byI2CPageSelAddr,address,wData,byTmp);
			//break;
		case I2C_ADDR_MODE_BA:
		default:
			return I2C_WriteByte(g_SnrRegAccessProp.byI2CID,address,wData,byTmp);
			//break;
	}
}

#ifdef _MIPI_EXIST_
//RLE0509 GPIO[0]=0:0xF4 GPIO[0]=1:0xD2
// 0xD2 for RX, 0xF4 for FT2
U8 Read_MIPIAPHYReg(U16 wAddr, U16 * pwData)
{
	return I2C_ReadWord(0xD2, wAddr, pwData, 1);
}

U8 Write_MIPIAPHYReg(U16 wAddr, U16 wData)
{
	return I2C_WriteWord(0xD2, wAddr, wData, 1);
}
#endif

#ifdef _I2C_NORMAL_
bit I2C_NORMAL_Read(U8 const byID_Addr, U32 * const pdwData, U8 const byLen)
{
	bit ret;
	
	START_I2C_CLK();
	
	// setting sensor ID address
	XBYTE[I2C_ADDR0] = byID_Addr | I2C_READ_OP;
	XBYTE[I2C_ADDR_LEN] = 0x01;

	XBYTE[I2C_DATA_LEN] = byLen;
	
	XBYTE[I2C_TRANSFER] = I2C_TRANSFER_START | I2C_READ_MODE;

	ret = I2C_CheckTransferEnd();

	if(ret == TRUE)
	{
		ASSIGN_LONG(*pdwData,XBYTE[I2C_DATA_SCRATCH0], XBYTE[I2C_DATA_SCRATCH1],\
								XBYTE[I2C_DATA_SCRATCH2],XBYTE[I2C_DATA_SCRATCH3]);	
	}

	STOP_I2C_CLK();
	
	return ret;	
}

bit I2C_NORMAL_Write(U8 const byID_Addr, U32 const dwData, U8 const byLen )
{
	bit ret;

	START_I2C_CLK();

	// setting sensor ID address and sensor register address
	XBYTE[I2C_ADDR0] = byID_Addr | I2C_WRITE_OP;
	XBYTE[I2C_ADDR_LEN] = 0x01;

	//first come first write
	XBYTE[I2C_DATA_SCRATCH0] = LONG2CHAR(dwData, 3);
	XBYTE[I2C_DATA_SCRATCH1] = LONG2CHAR(dwData, 2);
	XBYTE[I2C_DATA_SCRATCH2] = LONG2CHAR(dwData, 1);
	XBYTE[I2C_DATA_SCRATCH3] = LONG2CHAR(dwData, 0);

	XBYTE[I2C_DATA_LEN] = byLen;

	XBYTE[I2C_TRANSFER] = I2C_TRANSFER_START | I2C_WRITE_MODE;

	ret = I2C_CheckTransferEnd();
	
	STOP_I2C_CLK();
	
	return ret;
}
#endif

bit I2C_Super_Write(I2CSuperAccess_t *ptI2CSuperAccess, U8 *pData , U8 byDataLen)
{
	bit ret;
	
	if (byDataLen > 8)
	{
		return FALSE;
	}

	if (ptI2CSuperAccess->byAddrLen > 5)
	{
		return FALSE;
	}

	if (ptI2CSuperAccess->byMode != I2CSuper_Mode_W)
	{
		return FALSE;
	}

	START_I2C_CLK();	
	XBYTE[I2C_ADDR0] = ptI2CSuperAccess->byI2CID| I2C_WRITE_OP;
	XBYTE[I2C_ADDR1] = ptI2CSuperAccess->byAddr1;
	XBYTE[I2C_ADDR2] = ptI2CSuperAccess->byAddr2;
	XBYTE[I2C_ADDR3] = ptI2CSuperAccess->byAddr3;
	XBYTE[I2C_ADDR4] = ptI2CSuperAccess->byAddr4;
	XBYTE[I2C_ADDR5] = ptI2CSuperAccess->byAddr5;	
	XBYTE[I2C_ADDR_LEN] = 0x01+ptI2CSuperAccess->byAddrLen;

	memcpy((U8 *)&XBYTE[I2C_DATA_SCRATCH0], pData, byDataLen);
	XBYTE[I2C_DATA_LEN] = byDataLen;

	XBYTE[I2C_TRANSFER] = I2C_TRANSFER_START | I2C_WRITE_MODE;

	ret = I2C_CheckTransferEnd();
	STOP_I2C_CLK();
	
	return ret;	

}

bit I2C_Super_Read(I2CSuperAccess_t *ptI2CSuperAccess, U8 *pData ,U8 byDataLen)
{
	bit ret;
	
	if ((byDataLen > 8)||(byDataLen == 0))
	{
		return FALSE;
	}

	if (ptI2CSuperAccess->byAddrLen > 5)
	{
		return FALSE;
	}

	if ((ptI2CSuperAccess->byMode != I2CSuper_Mode_R)&&(ptI2CSuperAccess->byMode != I2CSuper_Mode_RR))
	{
		return FALSE;
	}	
	
	START_I2C_CLK();

	if (ptI2CSuperAccess->byMode  == I2CSuper_Mode_R)	//SetAddress+Read Or current address read
	{
		if (0 != ptI2CSuperAccess->byAddrLen)
		{
			XBYTE[I2C_ADDR0] = ptI2CSuperAccess->byI2CID| I2C_WRITE_OP;
			XBYTE[I2C_ADDR1] = ptI2CSuperAccess->byAddr1;
			XBYTE[I2C_ADDR2] = ptI2CSuperAccess->byAddr2;
			XBYTE[I2C_ADDR3] = ptI2CSuperAccess->byAddr3;
			XBYTE[I2C_ADDR4] = ptI2CSuperAccess->byAddr4;
			XBYTE[I2C_ADDR5] = ptI2CSuperAccess->byAddr5;	
			XBYTE[I2C_ADDR_LEN] = 0x01+ptI2CSuperAccess->byAddrLen;
			XBYTE[I2C_TRANSFER] = I2C_TRANSFER_START | I2C_SET_ADDRESS_MODE;
			ret = I2C_CheckTransferEnd();	
			if (FALSE == ret)
			{
				return FALSE;
			}
		}
		
		// setting sensor ID address
		XBYTE[I2C_ADDR0] = ptI2CSuperAccess->byI2CID | I2C_READ_OP;
		XBYTE[I2C_ADDR_LEN] = 0x01;

		// setting read length
		XBYTE[I2C_DATA_LEN] = byDataLen;
		XBYTE[I2C_TRANSFER] = I2C_TRANSFER_START | I2C_READ_MODE;

		ret = I2C_CheckTransferEnd();	
		if (FALSE == ret)
		{
			return FALSE;
		}
		else
		{
			memcpy( pData, (U8 *)&XBYTE[I2C_DATA_SCRATCH0], byDataLen);			
		}
	}
	else	 // I2CSuper_Mode_RR
	{
		if (ptI2CSuperAccess->byAddrLen > 4)
		{
			return FALSE;
		}
	
		XBYTE[I2C_ADDR0] = ptI2CSuperAccess->byI2CID| I2C_WRITE_OP;
		XBYTE[I2C_ADDR1] = ptI2CSuperAccess->byAddr1;
		XBYTE[I2C_ADDR2] = ptI2CSuperAccess->byAddr2;
		XBYTE[I2C_ADDR3] = ptI2CSuperAccess->byAddr3;
		XBYTE[I2C_ADDR4] = ptI2CSuperAccess->byAddr4;
		XBYTE[I2C_ADDR5] = ptI2CSuperAccess->byAddr5;	
		XBYTE[I2C_ADDR0 + ptI2CSuperAccess->byAddrLen +1] = ptI2CSuperAccess->byI2CID| I2C_READ_OP;	
		XBYTE[I2C_ADDR_LEN] = 0x10|(0x01+ptI2CSuperAccess->byAddrLen);
		XBYTE[I2C_DATA_LEN] = byDataLen;		
		XBYTE[I2C_TRANSFER] = I2C_TRANSFER_START | I2C_RANDOM_READ_MODE;
		ret = I2C_CheckTransferEnd();	
		if (FALSE == ret)
		{
			return FALSE;
		}	
		else
		{
			memcpy( pData, (U8 *)&XBYTE[I2C_DATA_SCRATCH0], byDataLen);			
		}		
	}
	
	STOP_I2C_CLK();
	
	return ret;		
}

