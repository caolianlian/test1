#include "pc_cam.h"
#include "camutil.h"
#include "camreg.h"
#include "camuvc.h"
#include "camctl.h"
#include "camvdcfg.h"
#include "global_vars.h"
#ifdef _RS232_DEBUG_
#include <stdarg.h>
#endif

void PwrOnAccordingSeq(U8 bySeq)
{
	switch(bySeq)
	{
	case SNR_PWRCTL_SEQ_SV18:
		POWER_ON_SV18();
		break;
	case SNR_PWRCTL_SEQ_SV28:
		POWER_ON_SV28();
		break;
	case SNR_PWRCTL_SEQ_GPIO8:
		POWER_ON_GPIO8();
		break;
	default:
		break;
	}
}

void PwrOffAccordingSeq(U8 bySeq)
{
	switch(bySeq)
	{
	case SNR_PWRCTL_SEQ_SV18:
		POWER_OFF_SV18();
		break;
	case SNR_PWRCTL_SEQ_SV28:
		POWER_OFF_SV28();
		break;
	case SNR_PWRCTL_SEQ_GPIO8:
		POWER_OFF_GPIO8();		
		break;
	default:
		break;
	}
}

static void Sensor_PowerOn(void)
{
	PwrOnAccordingSeq(g_bySnrPowerOnSeq&SNF_PWRCTL_SEQ_MSK_1ST);
	usDelay(15);
	PwrOnAccordingSeq((g_bySnrPowerOnSeq&SNF_PWRCTL_SEQ_MSK_2ND)>>2);
	usDelay(10);
	PwrOnAccordingSeq((g_bySnrPowerOnSeq&SNF_PWRCTL_SEQ_MSK_3RD)>>4);	
}

static void Sensor_PowerOff(U8 byDelayCtrl)
{
	PwrOffAccordingSeq(g_bySnrPowerOffSeq&SNF_PWRCTL_SEQ_MSK_1ST);
	if (byDelayCtrl == EN_DELAYTIME)	
	{	
		usDelay(10);
	}	
	PwrOffAccordingSeq((g_bySnrPowerOffSeq&SNF_PWRCTL_SEQ_MSK_2ND)>>2);
	if (byDelayCtrl == EN_DELAYTIME)	
	{		
		usDelay(5);
	}
	PwrOffAccordingSeq((g_bySnrPowerOffSeq&SNF_PWRCTL_SEQ_MSK_3RD)>>4);
#ifdef _USE_SENSOR_HI165_
	//CHIP_EN pull up
	//XBYTE[0xFF9E] = 0x10;	//XBYTE[PG_DELINK_CTRL] = PAD_DELINK_OUT
	//XBYTE[0xFF9E] = 0x80;	//XBYTE[PG_DELINK_CTRL] = PAD_DELINK_PULL_UP
	XBYTE[0xFF9E] = 0x40;
#endif
}

void SensorPowerControl(U8 bySwitch ,U8 byDelayCtrl)
{
	if(bySwitch == SWITCH_ON)
	{
		Sensor_PowerOn();

#ifdef _FT2_REMOVE_TEST
#else
		uDelay(200);
#endif
	}
	else //SWITCH_OFF
	{
		Sensor_PowerOff(byDelayCtrl);

#ifdef _FT2_REMOVE_TEST
#else
		if (byDelayCtrl == EN_DELAYTIME)
		{
			uDelay(100);
		}	
#endif
	}
}

/*
*********************************************************************************************************
*											Delay time for instruction cycles
* FUNCTION Delay
*********************************************************************************************************
*/
/**
  Wait time for instruction cycles

  \param ms		Waiting time with unit of instruction cycles.

  \retval 0			If time out.
		  1         Desired value matched.
*********************************************************************************************************
*/
void Delay(U16 ms)
{
	U16 xdata i;
	while(ms)
	{
		i = 2998;		// delay 1ms
		while(i)
		{
			i--;
		}
		ms--;
	}
}

void uDelay(U16 w100us)
{
	U16 xdata i;
	while(w100us)
	{
		i = 298;		// delay 100us
		while(i)
		{
			i--;
		}
		w100us--;
	}
}

void usDelay(U8 byus)
{
	U8 xdata i;
	while(byus)
	{
		i = 7;		// delay 1us
		while(i)
		{
			i--;
		}		
		byus--;
	}
}

void VideoDelay(U8 byDelayMax100Ms, U8 byFrameCnt, U8 byFPS)
{
	U16 wDelay10Ms;
	U8 byDelay;
	U8 i;

	wDelay10Ms = (U16)byFrameCnt*(U16)100/(U16)byFPS;	// delay frame transfer to delay time
	wDelay10Ms = ClipWord(wDelay10Ms, 1, (U16)byDelayMax100Ms*(U16)10); // delay time not over max delay time

	for(i=0; i< 25; i++)
	{
		byDelay = ClipWord(wDelay10Ms, 0, 250);
		WaitTimeOut_Delay(byDelay);
		wDelay10Ms -= byDelay;
		if(wDelay10Ms==0)
		{
			break;
		}
	}
}

////////////////////////////////////////////////////////////////
//GPIO operation routines
////////////////////////////////////////////////////////////////
#ifdef _HID_BTN_
/*
*********************************************************************************************************
						get Gpio pin value
* FUNCTION GetGPIOValueW
*********************************************************************************************************
*/
/**
  get GPIO[15:0] pin value
  \param None

  \retval GPIO[15:0] value
*********************************************************************************************************
*/
// hemonel 2010-04-13: delete dummy code

U16 GetGPIOValueW()
{
	return (XBYTE[GPIO_H]<<8)+XBYTE[GPIO_L];
}


/*
*********************************************************************************************************
						   get Gpio pin value
* FUNCTION GetGPIOValueB
*********************************************************************************************************
*/
/**
  get GPIO[15:0] pin value
  \param
       io: GPIO index

  \retval value of GPIO[io]
*********************************************************************************************************
*/
// hemonel 2010-04-13: delete dummy code

bit  GetGPIOValueB(U8 io) // test if GPIO 1 or 0
{
	U16 wTmp1, wTmp2;
	ASSERT(io<16);
	wTmp1=GetGPIOValueW();
	wTmp2=(0x0001<<io);
	if(wTmp2&wTmp1)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}
#endif

/*
*********************************************************************************************************
						   Set Gpio pin value
* FUNCTION SetGPIOValueB
*********************************************************************************************************
*/
/**
  set GPIO[15:0] pin value
  \param
       wValue: GPIO[15:0] value

  \retval none
*********************************************************************************************************
*/
//#define  SetGpioValueW(wValue) { XBYTE[GPIO_H] = (U8) (wValue/256),XBYTE[GPIO_L] = (U8) (wValue%256);}

/*
*********************************************************************************************************
						   set Gpio pin value
* FUNCTION SetGPIOValueB
*********************************************************************************************************
*/
/**
  Set GPIO[15:0] pin value
  \param
       io: GPIO index
    value: set value

  \retval None
*********************************************************************************************************
*/
/*
void SetGpioValueB(U8 io,U8 value)
{

	U16 wRegAddr =0;
	if(io>7)
	{
		io -= 8;
		wRegAddr = GPIO_H;

	}
	else
	{
		wRegAddr = GPIO_L;

	}

	if(value)
	{
		XBYTE[wRegAddr] |= (0X01<<io);
	}
	else
	{
		XBYTE[wRegAddr] |= (~(0X01<<io));
	}
}
*/
/*
*********************************************************************************************************
						   set Gpio pull control
* FUNCTION SetGPIOPullCtlW
*********************************************************************************************************
*/
/**
  set Gpio[15:0] pull control
  \param
       pullEn: GPIO[15:0] pull enable, 1->enable,0->disable
      pullDir: pull directcion  1-> pull up, 0->pull down

  \retval None
*********************************************************************************************************
*/
/*
void SetGPIOPullCtlW(U16 pullEn,U16 pullDir)
{
	U16 wbit1,wbit0;               //bit1= pullEn and pullDir
	U32 lTmp1=0;                    //bit0= pullEn and ~pullDir
	U32 lTmp2=0;
	U8 i;
	wbit1=pullEn&pullDir;
	wbit0=pullEn&(~pullDir);
	for(i=0;i<16;i++)
	{
		lTmp1 = ((U32)((0x0001<<i)&wbit0))<<(i*2)   + ((U32)((0x0001)&wbit1 ))<<(i*2+1);
		lTmp2 =lTmp1+lTmp2;
	}

       XBYTE[GPIO_PULL_CTL_L0] = (U8)(lTmp2&0x000000ff);
       XBYTE[GPIO_PULL_CTL_H0] = (U8)((lTmp2&0x0000ff00)>>8);

       XBYTE[GPIO_PULL_CTL_L1]  =  (U8)((lTmp2&0x00ff0000)>>16);


}
*/
/*
*********************************************************************************************************
						   set Gpio directon
* FUNCTION SetGPIODirW
*********************************************************************************************************
*/
/**
  set Gpio[15:0] directon
  \param
       wDir:GPIO[15:0] direction    1->output 0:->input

  \retval None
*********************************************************************************************************
*/
#if (defined (_SLB_TEST_) ||defined (_FT2_REMOVE_TEST))
/*
*********************************************************************************************************
						   set Gpio directon
* FUNCTION SetGPIODirB
*********************************************************************************************************
*/
/**
  set individaul GPIO directon
  \param
            io: gpio index
       byValue:GPIO[15:0] direction    1->output 0:->input

  \retval None
*********************************************************************************************************
*/
void SetGPIODirB(U8 io,U8 byValue) //set GPIO direction 1: output 0: input
{
	static U16 wTmp1;		// define static to delete mutli-call warning
	static U16 wTmp2;		// define static to delete mutli-call warning
	ASSERT(io<16);
	wTmp1 = (XBYTE[GPIO_DIR_H] <<8) + XBYTE[GPIO_DIR_L] ;
	if(byValue)
	{
		wTmp2 = (0x0001<<io);
		SetGPIODirW(wTmp2|wTmp1);
	}
	else
	{
		wTmp2=~(0x0001<<io);
		SetGPIODirW(wTmp1&wTmp2);
	}
}
#endif

/****************************************************************
**GPIO int enable/disable  routines
*****************************************************************/
/*
   edge: 1-> rising edge ,0-> falling edge.
   io:  0->15;
*/
#ifdef _HID_BTN_
/*
*********************************************************************************************************
						   set Gpio interrupt enable
* FUNCTION SetGPIOIntEnW
*********************************************************************************************************
*/
/**
  set Gpio[15:0] interrupt enable
  \param
          wValue: GPIO[15:0] interrupt enable setting.
            edge: 1-> rising edge 0->falling edge

  \retval None
*********************************************************************************************************
*/

void SetGPIOIntEnW(U16 wValue, U8 edge)  //1// 1:enable  0: disable
{
	if(edge) // rising edge
	{
		XBYTE[GPIO_IRQEN_RISE_H] = wValue/256;
		XBYTE[GPIO_IRQEN_RISE_L]  = wValue%256;
	}
	else     // falling edge
	{
		XBYTE[GPIO_IRQEN_FALL_H] = wValue/256;
		XBYTE[GPIO_IRQEN_FALL_L]  = wValue%256;
	}
}

/*
*********************************************************************************************************
						   Get Gpio interrupt enable setting
* FUNCTION TestGPIOIntEnW
*********************************************************************************************************
*/
/**
  Get Gpio[15:0] interrupt enable setting
  \param
          edge: 1-> rising edge 0->falling edge

  \retval
          interrupt enable setting
*********************************************************************************************************
*/
// hemonel 2009-12-29: delete GPIO interrupt

U16 TestGPIOIntEnW(U8 edge)
{
	if(edge) //// rising edge
	{
		return (XBYTE[GPIO_IRQEN_RISE_H]<<8)+XBYTE[GPIO_IRQEN_RISE_L];
	}
	else// falling edge
	{
		return (XBYTE[GPIO_IRQEN_FALL_H]<<8)+XBYTE[GPIO_IRQEN_FALL_L];
	}
}
#endif
/*
*********************************************************************************************************
						   Get individual Gpio interrupt enable setting
* FUNCTION TestGPIOIntEnB
*********************************************************************************************************
*/
/**
  Get individual GPIO interrupt enable setting
  \param
            io: GPIO index
          edge: 1-> rising edge 0->falling edge

  \retval
          interrupt enable setting
*********************************************************************************************************
*/
/*
bit TestGPIOIntEnB(U8 io,U8 edge)
{
	U16 wTmp1;
	U16 wTmp2;
	ASSERT(io<16);
	wTmp1=TestGPIOIntEnW(edge);
	wTmp2=(0x0001<<io);
	if(wTmp2&wTmp1)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}
*/
/*
*********************************************************************************************************
						   set individual Gpio interrupt enable setting
* FUNCTION SetGPIOIntEnB
*********************************************************************************************************
*/
/**
  set individual GPIO interrupt enable
  \param
            io: GPIO index
          edge: 1-> rising edge 0->falling edge

  \retval None
*********************************************************************************************************
*/
/*
void SetGPIOIntEnB(U8 io, U8 edge)
{
	U16 wTmp1;
	U16 wTmp2;
	ASSERT(io<16);
	wTmp1=TestGPIOIntEnW(edge);
	wTmp2=(0x0001<<io)|wTmp1;
	SetGPIOIntEnW(wTmp2,edge);
}
*/
//////////////////////////////////////////////////////////////////////////////////////
// test and clear GPIO int flag routines
//////////////////////////////////////////////////////////////////////////////////////
#ifdef _HID_BTN_
/*
*********************************************************************************************************
						   Get  Gpio interrupt flag
* FUNCTION TestGpioIntFlagW
*********************************************************************************************************
*/
/**
  Get  GPIO interrupt flag
  \param
           edge: 1-> rising edge 0->falling edge

  \retval interrupt flag
*********************************************************************************************************
*/
// hemonel 2009-12-29: delete GPIO interrupt

U16 TestGpioIntFlagW(U8 edge)
{
	if(edge)//// rising edge
	{
		return (XBYTE[GPIO_IRQSTAT_RISE_H]<<8)+XBYTE[GPIO_IRQSTAT_RISE_L];
	}
	else// falling edge
	{
		return (XBYTE[GPIO_IRQSTAT_FALL_H]<<8)+XBYTE[GPIO_IRQSTAT_FALL_L];
	}
}

//////////////////////////////////////////////////////////////////////////////////////
/*
*********************************************************************************************************
						   clear  Gpio interrupt flag
* FUNCTION ClearGpioIntFlagW
*********************************************************************************************************
*/
/**
  clear  GPIO interrupt flag
  \param
           edge: 1-> rising edge 0->falling edge
         wValue: clear bit mask
  \retval interrupt flag
*********************************************************************************************************
*/
// hemonel 2009-12-29: delete GPIO interrupt

void ClearGpioIntFlagW(U16 wValue,U8 edge)
{
	if(edge)
	{
		XBYTE[GPIO_IRQSTAT_RISE_H]=wValue/256;
		XBYTE[GPIO_IRQSTAT_RISE_L]=wValue%256;
	}
	else
	{
		XBYTE[GPIO_IRQSTAT_FALL_H]=wValue/256;
		XBYTE[GPIO_IRQSTAT_FALL_L]=wValue%256;
	}
}
#endif
/*
*********************************************************************************************************
						   Get  indivadual Gpio interrupt flag
* FUNCTION TestGpioIntFlagB
*********************************************************************************************************
*/
/**
  get indivadual  GPIO interrupt flag
  \param
           io:GPIO index
         edge:1-> rising edge 0->falling edge
  \retval interrupt flag
*********************************************************************************************************
*/
/*
bit TestGpioIntFlagB(U8 io, U8 edge)
{
	U16 wTmp1;
	U16 wTmp2;
	ASSERT(io<16);
	wTmp1=TestGpioIntFlagW(edge);
	//DBG(("\nGPIO int flage = %x ",wTmp1));
	wTmp2=(0x0001<<io);
	if(wTmp2 & wTmp1)
	{
		return 1;
	}
	else
	{
		return 0;
	}

}
*/
#ifdef _HID_BTN_
/*
*********************************************************************************************************
						   clear  indivadual Gpio interrupt flag
* FUNCTION ClearGpioIntFlagB
*********************************************************************************************************
*/
/**
  clear indivadual  GPIO interrupt flag
  \param
           io:GPIO index
         edge:1-> rising edge 0->falling edge
  \retval interrupt flag
*********************************************************************************************************
*/
// hemonel 2009-12-29: delete GPIO interrupt

void ClearGpioIntFlagB(U8 io, U8 edge)
{
//	U16 wTmp1;
	U16 wTmp2;
	ASSERT(io<16);
	//wTmp1=TestGpioIntFlagW(edge);
	wTmp2=(0x0001<<io);// write 1 to clear int flag
	ClearGpioIntFlagW(wTmp2, edge);
}
#endif

U16 ClipWord(U16 wCur, U16 wMin, U16 wMax)
{
	if(wCur>wMax)
	{
		return wMax;
	}

	if(wCur<wMin)
	{
		return wMin;
	}
	return wCur;
}

float ClipFloat(float fCur, float fMin, float fMax)
{
	if(fCur > fMax)
	{
		return fMax;
	}
	else if(fCur < fMin)
	{
		return fMin;
	}
	else
	{
		return fCur;
	}
}

U8 LinearIntp_Byte(U8 byX1,U8 byY1, U8 byX2, U8 byY2, U8 byX)
{
	return (((S16)byX1-(S16)byX)*(S32)byY2+((S16)byX-(S16)byX2)*(S32)byY1)/((S16)byX1-(S16)byX2);
}

U8 LinearIntp_Byte_Bound(U8 byX1,U8 byY1, U8 byX2, U8 byY2, U8 byX)
{
	if(byX < byX1)
	{
		return byY1;
	}
	else if(byX > byX2)
	{
		return byY2;
	}
	else
	{
		return LinearIntp_Byte( byX1, byY1,  byX2,  byY2,  byX);
	}
}

S8 LinearIntp_Byte_Signed(S8 byX1,S8 byY1, S8 byX2, S8 byY2, S8 byX)
{
	return (((S16)byX1-(S16)byX)*(S32)byY2+((S16)byX-(S16)byX2)*(S32)byY1)/((S16)byX1-(S16)byX2);
}

S8 LinearIntp_Byte_Bound_Signed(S8 byX1,S8 byY1, S8 byX2, S8 byY2, S8 byX)
{
	if(byX < byX1)
	{
		return byY1;
	}
	else if(byX > byX2)
	{
		return byY2;
	}
	else
	{
		return LinearIntp_Byte_Signed( byX1, byY1,  byX2,  byY2,  byX);
	}
}

U16 LinearIntp_Word(U16 wX1,U16 wY1, U16 wX2, U16 wY2, U16 wX)
{
	return (((S32)wX1-(S32)wX)*(S32)wY2+((S32)wX-(S32)wX2)*(S32)wY1)/((S32)wX1-(S32)wX2);
}

S16 LinearIntp_Word_Signed(S16 wX1,S16 wY1, S16 wX2, S16 wY2, S16 wX)
{
	return (((S32)wX1-(S32)wX)*(S32)wY2+((S32)wX-(S32)wX2)*(S32)wY1)/((S32)wX1-(S32)wX2);
}

S16 LinearIntp_Word_Bound_Signed(S16 wX1,S16 wY1, S16 wX2, S16 wY2, S16 wX)
{
	if(wX < wX1)
	{
		return wY1;
	}
	else if(wX > wX2)
	{
		return wY2;
	}
	else
	{
		return LinearIntp_Word_Signed( wX1, wY1, wX2, wY2, wX);
	}
}

U8 MaxByte(U8 byX1, U8 byX2)
{
	if(byX1>byX2)
	{
		return byX1;
	}
	else
	{
		return byX2;
	}
}

U8 MinByte(U8 byX1, U8 byX2)
{
	if(byX1<byX2)
	{
		return byX1;
	}
	else
	{
		return byX2;
	}
}

U16 MinWord(U16 wX1, U16 wX2)
{
	if(wX1>wX2)
	{
		return wX2;
	}
	else
	{
		return wX1;
	}
}

float MaxFloat(float fX1, float fX2)
{
	if(fX1>fX2)
	{
		return fX1;
	}
	else
	{
		return fX2;
	}
}

float MinFloat(float fX1, float fX2)
{
	if(fX1<fX2)
	{
		return fX1;
	}
	else
	{
		return fX2;
	}
}

#ifdef _RS232_DEBUG_

static U8 printbuffer[CONFIG_PRINT_SIZE] = {0};

void ub_putc(U8 c)
{
	if(!WaitTimeOut(RS232_UCTL, RS232_BUSY, 0, 20))
	{
		return;
	}

	XBYTE[RS232_UTXD] = c;
	XBYTE[RS232_UCTL] |= RS232_WRITE;

	return;
}

void ub_puts(U8 len)
{
	U8 i = 0;

	while(i<len)
	{
		if (printbuffer[i] == '\n')
		{
			ub_putc('\r');
		}

		ub_putc(printbuffer[i]);

		i++;
	}

	return;
}

void rlprintf (const char *fmt, ...)
{
	va_list args;
	U8 len;

	va_start (args, fmt);

	/* For this to work, printbuffer must be larger than
	 * anything we ever want to print.
	 */
	len = vsprintf (printbuffer, fmt, args);
	va_end (args);

	/* Print the string */
	ub_puts (len);

	return;
}

#endif
