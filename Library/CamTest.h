#ifndef _CAMTEST_H_
#define _CAMTEST_H_

// macros for SLB test
#define SLB_WAIT_HS  1
#define SLB_WAIT_FS  40
#define SLB_HS   0
#define SLB_FS    1

#if ((defined _FT2_REMOVE_TEST) || (defined _SLB_TEST_))
#define SLB_FS_TEST_COUNT 		3
#define SLB_HS_TEST_COUNT 		512
#define SLB_HS_FAIL_THRESHOLD 5
#define SLB_FIX_PATTERN 			0XFF
#define SLB_SEED_PATTERN 			0X01
#define SLB_FS_FAIL_THRESHOLD	1
#define SLB_HS_DELAY_TIME		1
#define FT2_REMOVE_VERSION		0x01
#endif

void OLT_main(void);
void SLB_TEST_main(void );
void 	InitFTMode(void);
void 	FT_TEST(void);
#endif