/*
*********************************************************************************************************
*                                       Realtek's PcCamera Project
*
*											Layer: PCCAMERA
*											Module: MAIN MODULE
*									(c) Copyright 2011-2012, Realtek Corp.
*                                           All Rights Reserved
*
*                                                  V1.0
*
* File : Pc_cam.C
*********************************************************************************************************
*/
/**
*******************************************************************************
  \file pc_cam.c
  \brief The control module of the system, it initializes PC Camera chip and implements interrupt service
  		 routines.

GENERAL DESCRIPTION:2011-12-20 13:45:02

   The file completes system power-on initialization and put system to idle waiting for interrupts occur
   and handling them. System initialization contains MCU initialization and all of registers
   initialization. ISR(Interrupt Service Routine) is called by MCU when the corresponding interrupt
   occurs. MCU communicates with peripherals by external interrupt 0 pin, all the incoming interrupts
   will induce MCU invoking ISR in which the interrupts will be decoded to the exact interrupt and
   be handle properly. This is the main module and the entry point of the entire firmware.

EXTERNALIZED FUNCTIONS:

  PHYRegisterWrite --
	Configure some working mode of IC. It is added to firmware for IC designer's demands.

  WaitTimeOut --
	Wait till time out.

Copyright (c) 2011-2012 by Realtek Incorporated.  All Rights Reserved.


*  \version 1.0
*  \date    2011-2012

*******************************************************************************/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

when        who     what, where, why
--------    -----   ----------------------------------------------------------


===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include "pc_cam.h"
#include "camutil.h"
#include "camreg.h"
#include "camuvc.h"
#include "camctl.h"
#include "CamIsp.h"
#include "CamProcess.h"
#include "camsensor.h"
#include "CAMI2C.h"
#include "camspi.h"
#include "camvdcfg.h"
#include "Global_vars.h"
#include "ISP_vars.h"
#include "ISP_lib.h"
//#include "CamUac.h"
#include "CamTest.h"
#include "CamIspPro.h"
#include "CamAWBPro.h"
#include "CamFun.h"
#include "CamLSC.h"
#include "CamASPro.h"
#if (defined _UVC_PPWB_ || defined _UVC_COMMITWB_)
#include "CamIsp.h"
#endif
#ifdef _UAC_EXIST_
#include "CamUac.h"
#endif

#ifdef _AE_NEW_SPEED_ENABLE_
#include "CamAECPro.h"
#endif

#ifdef _RTK_EXTENDED_CTL_
#include "CamRtkExtIsp.h"
#endif

// MCU_CLOCK_60,30,15,1.875; 80,40,20,2.5;96,48,24,3;
//U8 code  g_pTH0ClkTable[12] = {0x48,0x9f,0xd0,0xF9,0x00,0x7d,0xbe,0xF7,0x00, 0x6c,0xb2,0xF6};
//U8 code  g_pTL0ClkTable[12] = {0x70,0x34,0x9d,0xE5,0x00,0xcb,0xe5,0xDC,0x00, 0x22,0x64,0x3c};
//U8 code  g_pTH1ClkTable[12] = {0xEC,0xF6,0xFB,0xFF,0x00,0xF3,0xF9,0xFF,0x00, 0xF0,0xF8,0xFF};
//U8 code  g_pTL1ClkTable[12] = {0x86,0x4B,0x2E,0x64,0x00,0x0A,0x8D,0x2F,0x00, 0x6E,0x40,0x06};

//Jimmy.20120401.New printf use hw to replace timer2 to generate.
//19200 baudrate
// MCU_CLOCK_60,30,15,1.875; 80,40,20,2.5;96,48,24,3;
//U8 code  g_pRCAP2HBaudTable[12] = {0xff,0xff,0xff,0xff,    0xff,0xff,0xff,0xff,    0xff,0xff,0xff,0xff};
//U8 code  g_pRCAP2LBaudTable[12] = {0x9f,0xd0,0xe8,0xfd,   0x7e ,0xbf ,0xdf,0xfc,    0x64, 0xb2,0xd9,0xfb};
#ifdef _S3_RESUMKEK_
// MCU_CLOCK_120,60,30,15,7.5,3.75,1.875
// 5mS
U8 code g_pTH0ClkTable[7] = {0x3C,0x9E,0xCF,0xE7,0xF3,0xF9,0xFC};
U8 code g_pTL0ClkTable[7] = {0xAF,0x57,0x2B,0x95,0xCA,0xE7,0xF3};

// MCU_CLOCK_120,60,30,15,7.5,3.75,1.875
// 0.5mS Setting
U8 code g_aHWTMInitValueLL[7] = {0x9F,0xcf,0x67,0xB3,0x59,0xAC,0x56};
U8 code g_aHWTMInitValueLH[7] = {0x15,0x8a,0xC5,0xE2,0xF1,0xF8,0xFC};
U8 code g_aHWTMInitValueHL[7] = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
U8 code g_aHWTMInitValueHH[7] = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
U8 data l_bySOFCount;
U8 data l_byChangeFlag;
#else
// MCU_CLOCK_120,60,30,15,7.5,3.75,1.875
// 10mS Setting
U8 code g_aHWTMInitValueLL[7] = {0x80,0x40,0x20,0x10,0x08,0x84,0xC2};
U8 code g_aHWTMInitValueLH[7] = {0xB0,0xD8,0x6C,0xB6,0xDB,0x6D,0xB6};
U8 code g_aHWTMInitValueHL[7] = {0xED,0xF6,0xFB,0xFD,0xFE,0xFF,0xFF};
U8 code g_aHWTMInitValueHH[7] = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
#endif


extern void PatchUvcPP(void);
void StopSensor(U8 byDelayCtrl);

/*
*********************************************************************************************************
*											Main Function
* FUNCTION main
*********************************************************************************************************
*/
/**
  Initialize the whole chip and go to idle state waiting for interrupts and handle it. In idle state it
  control LEDs on or off according to the current status of cards. It is the entry point of the entire
  firmware.

  \param None

  \retval None
*********************************************************************************************************
*/
void	main(void)
{
#ifdef _OSC_RC_MODE_
	//LC switch to RC
	XBYTE[RC_OSC_CFG] =0x21;	// RC enable
	XBYTE[RC_OSC_CFG] =0x23;	// RC select
	PHYRegisterWrite(0x03, 0xC1);	// LC disable
#endif

	XBYTE[REG_BANDGAP] = REG_BG5V_1V20|REG_VBG_SEL_1V23;	// REG_BG5V adjust to 1.2V
#if (_CHIP_ID_ & _RTS5840_)
	if((XBYTE[HW_VER_ID1]&0x0F) == 0x00)
	{
		XBYTE[REG_TUNE] = REG_TUNED33_3V3|REG_TUNED12_1V35;
	}
	else
	{
		XBYTE[REG_TUNE] = REG_TUNED33_3V3|REG_TUNED12_1V25;
	}
#else
	XBYTE[REG_TUNE] = REG_TUNED33_3V3|REG_TUNED12_1V25;	
#endif

#ifdef _PG_EN_
	if(XBYTE[PG_FSM_RST] & SUSPEND_FLAG)
	{
		//HW default MCU = 30Mhz
		g_byCpuClkIdx = MCU_CLOCK_30M;
		
		XBYTE[WTDG_CTL] = WDG_TIME_2S|WDG2RST|WDG_RST_REG; //disable watch dog first.
		InitMCU();
#ifdef _NF_RESTORE_
		//Jimmy.20130326.Before here can't enable global interrupt and SOF interrupt
		if(g_byBypassFirstSOF == 1)
		{
			XBYTE[FREQ_TOP_CONFIG_8] = FRE_EXT_NF_SEL|FRE_EXT_CTRL_EN;//force frequency adjust to manual mode
			XBYTE[FREQ_TOP_CONFIG_9] = g_bySuspendFW_N;
			XBYTE[FREQ_TOP_CONFIG_10] = g_bySuspendFW_F;
		}
#endif
		InitCAM(1);		
	}
	else
#endif
	{
		//HW default MCU = 30Mhz
		g_byCpuClkIdx = MCU_CLOCK_30M;
		
		//by jin: Initialize MCU
		XBYTE[WTDG_CTL] = WDG_TIME_2S|WDG2RST|WDG_RST_REG; //disable watch dog first.
		InitMCU();

#if (defined (_FT2_REMOVE_TEST))
//		InitFTMode();
//		FT_TEST();
#endif
		// hemonel 2009-12-29: move to here after MCU clock change to high frequency in order to decrease initilization time.
		// hemonel 2010-11-16: CALL function must be called after global_vars_init()
		global_vars_init();//------jqg080707

#if (defined (_SLB_TEST_))
#ifdef _IC_CODE_
		XBYTE[REG_HSTX_CFG] = 0x52;
#endif
		SLB_TEST_main();
#endif

		// hemonel 2011-04-26: delete for optimize code because customer do not use
		//	InitCustomerParam();	// hemonel 2009-12-02: add for customer modify some camera variable or parameter such as PID, VID and device name

		//by jin: Initialize the rest parts of RTS5151 other than MCU
		InitCAM(0);

#ifdef _CUSTOMER_ISP_
		CusISPInit();
#endif

	}

	// if IC is 5827, we must enable EPA, EPE transfer first
//	XBYTE[EPA_FLOW_CTL] = 0x1;	// hemonel 2012-02-06: delete because this register reset value is 1.

	XBYTE[PG_FSM_RST] &= ~ SUSPEND_FLAG;

	for(;;)
	{
#ifdef _CUSTOMER_ISP_
		if(WhiteBalanceTempAutoItem.Last == 1)
		{
			CusAWBStaticsStart();
		}

		if(ExposureTimeAutoItem.Last == EXPOSURE_TIME_AUTO_MOD_APERTPRO)
		{
			CusAEStaticsStart();
		}	
#else
		EnterCritical();
		if( (ExposureTimeAutoItem.Last == EXPOSURE_TIME_AUTO_MOD_APERTPRO)||(WhiteBalanceTempAutoItem.Last == 1) )
		{
			ISP_AEStaticsStart();
		}
		ExitCritical();

#ifdef _LENOVO_IDEA_EYE_
		if (g_byMTDStartGetFrame && g_wMTDTimeOutCnt==0)
		{
			EnterCritical();
			// to read sensor ID, set software interrupt 0.
			XBYTE[SYS_SOFTIRQ_STS]= 0x00;
			XBYTE[SYS_SOFTIRQ_EN] = SF_IRQ_MTD;
			XBYTE[SYS_SOFTIRQ_STS] = SF_IRQ_MTD;
			ExitCritical();
			g_wMTDTimeOutCnt = 100;
		}	
#endif


		EnterCritical();
#ifdef _AF_ENABLE_
		if( (WhiteBalanceTempAutoItem.Last == 1)||(FocusAutoItem.Last == CTL_DEF_CT_FOCUS_AUTO) )
#else
		if( WhiteBalanceTempAutoItem.Last == 1)
#endif
		{
			ISP_AWBStaticsStart();
		}
		ExitCritical();

		EnterCritical();
#ifdef _ENABLE_OUTDOOR_DETECT_
		{
			ABStaticsStart();
		}
#else
#ifdef _ENABLE_ANTI_FLICK_
		if( PwrLineFreqItem.Last == PWR_LINE_FRQ_DIS)
		{
			ABStaticsStart();
		}
#endif
#endif
		ExitCritical();
#endif


		// hemonel 2009-11-23: add for download firmware to indicate stage
		if(g_wLEDToggleInterval)
		{
			LED_TOGGLE();
			Delay(g_wLEDToggleInterval);
		}

#ifdef _UBIST_TEST_
		if(g_byUbistTestEnable == 1)
		{
			if(g_byUbistTestProgress & 0x80)
			{
				// test entering
				switch (g_byUbistTestID)
				{
				case 0x01:
					UbistTest_SensorCommunication();
					break;

				case 0x02:
					UbistTest_FlashChecking();
					break;

				case 0x03:
					UbistTest_Register();
					break;

				case 0x04:
					UbistTest_Controller();
					break;

				case 0x05:
					UbistTest_Luminance();
					break;

				case 0x06:
					UbistTest_BlinkLED();
					break;

				default:
					g_byUbistTestReturnCode =3;
					break;
				}

				// test abort
				if(( g_byUbistTestAbort == 1)&& (g_byUbistTestReturnCode !=3))
				{
					g_byUbistTestProgress &= ~0x80;//UBIST_STAT_RUNNING;
					g_byUbistTestReturnCode = 1; //UBIST_RC_ABORT;
					g_byUbistTestAbort = 0;
				}
			}
			else if( g_byUbistTestAbort == 1)
			{
				g_byUbistTestProgress &= ~0x80;//UBIST_STAT_RUNNING;
				g_byUbistTestReturnCode = 1; //UBIST_RC_ABORT;
				g_byUbistTestAbort = 0;
			}
		}
#endif //_UBIST_TEST_
	}
}
#ifdef _HID_BTN_
U8 WaitBtnDetectDebounce(U8  byGpio, U8 byGpioSel, U8 byEdge)
{
	U8 idata i = 0;
	U8 idata byaValue[VALID_BTN_PULS] = {0};

	for (i=0;i<VALID_BTN_PULS;i++)
	{
		Delay(20);
		if(byGpioSel == GPIO_SEL_NORMAL)
		{
			byaValue[i] = GetGPIOValueB(byGpio);
		}
		else if(byGpioSel == GPIO_SEL_AL)
		{
			byaValue[i] = XBYTE[PG_GPIO_AL_CTRL0] & (0x01 << (byGpio+4));
		}
		else		//GPIO_SEL_MIC
		{
			byaValue[i] = XBYTE[DMIC_CTL] & (0x01 << (byGpio+2));		
		}
	}

	if(byEdge == RISING_EDGE)
	{
		if(byaValue[0] == 0)
		{
			return FALSE;
		}
	}
	else
	{
		if(byaValue[0] != 0)
		{
			return FALSE;
		}		
	}
	
	for (i=1;i<VALID_BTN_PULS;i++)
	{
		if(byaValue[0] != byaValue[i])
		{
			return FALSE;
		}
		
	}

	return TRUE;

}
#endif

/*
*********************************************************************************************************
*										Interrupt Service Routine
* FUNCTION interrupt0
*********************************************************************************************************
*/
/**
  This is ISR to serve external interrupt 0, which includes Port Reset Interrupt, Suspend Interrupt,
  Setup Packet Interrupt, Endpoint Out Interrupt and all of Card Extracting/Inserting Interrupt. If any
  above interrupts occurs MCU will enter this ISR and do something by called the related functions.

  \param None

  \retval None
*********************************************************************************************************
*/
void	interrupt0	(void)	interrupt 0
{
	volatile    U8  data GpioInt = (XBYTE[GPIO_INT_CTL]&GPIO_GLB_INT_EN)&&(XBYTE[GPIO_INT_CTL]&GPIO_GLB_INT_STS);// GPIO INTERRUPT
	volatile	U8	data GpioALInt = XBYTE[PG_GPIO_AL_INT_STS] & XBYTE[PG_GPIO_AL_INT_EN];
	//Jimmy.20120607.RTS5840 not have DMIC about.
	//volatile	U8	data GpioMICInt = XBYTE[DMIC_PAD_INT_EN] & XBYTE[DMIC_PAD_INT_STS];
	volatile	U8	data	usbirqstat = XBYTE[USBSYS_IRQSTAT] & XBYTE[USBSYS_IRQEN];
	volatile	U8	data	epirqstat = XBYTE[EPTOP_IRQSTAT] & XBYTE[EPTOP_IRQEN];
	volatile	U8	data	epbirqstat = XBYTE[EPB_IRQEN] & XBYTE[EPB_IRQSTAT];
	volatile	U8	data ispirqstat = XBYTE[ISP_INT_FLAG0] & XBYTE[ISP_INT_EN0];
	volatile	U8	data ispirqstat1 = XBYTE[ISP_INT_FLAG1] & XBYTE[ISP_INT_EN1];
	volatile    U8   data softInt =  XBYTE[SYS_SOFTIRQ_STS]&XBYTE[SYS_SOFTIRQ_EN] ;
	volatile	U8 	data fcntInt = XBYTE[FCNT_INT] &(XBYTE[FCNT_INT]>>7);

#ifdef _HID_BTN_
	U16 idata wGPIORiseIntVld = 0;
	U16 idata wGPIOFallIntVld = 0;
#endif
#ifdef _RS0509_PREVIEW_ISSUE_
	U16 data wtemp = 0;
#endif
	U16 wIllum_CT;

	DBG(("."));
	DBG2(("."));

	// switch off 	SIE auto power down function.
	XBYTE[BLK_CLK_EN] &=(~SIE_PWRDWN_EN);

	// hemonel 2009-11-04 fix callepla chipset bug:
	// because read sensor ID will lead to VBUS drop to 2.6V and the device become to unknown device
	// solution: fixed sensor ID through EEPROM configure or serial flash, or read sensor ID at usb SOF interrupt
	// if EEPROM has not sensor ID, read sensor ID
	if(usbirqstat & SOF_INT)
	{
		XBYTE[USBSYS_IRQSTAT] = SOF_INT;	// clear interrupt
#ifdef _NF_RESTORE_		
		uDelay(5);//delay 500us,about 4 SOF
		XBYTE[FREQ_TOP_CONFIG_8] &= ~(FRE_EXT_NF_SEL|FRE_EXT_CTRL_EN);//set atuo adjust mode for frequency
#endif
#ifndef _RS0509_PREVIEW_ISSUE_
#ifdef _NF_RESTORE_	
		if(g_byBypassFirstSOF == 1)
		{
			XBYTE[USBSYS_IRQEN] &= ~SOF_INT;
		}
		else
#endif		
#endif			
		{
#ifdef _NF_RESTORE_			
			g_byBypassFirstSOF = 1;	
#endif			
#ifdef _RS0509_PREVIEW_ISSUE_
			if ((g_bySensorIsOpen == SENSOR_OPEN) && (1000 == g_wSOFCount)) 
			{	//Jimmy.20111220.RLE0509 have a bug, need to restart again always.
				if(TRUE == Read_SenReg(0x0210, &wtemp))
				{
					if((0x80&wtemp) == 0x00)
					{
						Write_SenReg_Mask(0x0210, 0x00, 0x80);
						Write_SenReg_Mask(0x0210, 0x80, 0x80);
					}
				}
				
			}
#else
			XBYTE[USBSYS_IRQEN] &= ~SOF_INT;	// disable interrupt
#endif
			
#ifdef _RS0509_PREVIEW_ISSUE_
			if (g_wSOFCount <1000)		//255/60 = 4s, 125uSx1000 =125ms
			{
				g_wSOFCount++;
			}
			else
			{
				g_wSOFCount = 1;
			}
#endif
		}
	}

	if(softInt)
	{
		if(softInt&SF_IRQ_LOAD_EEPROM)
		{
			XBYTE[SYS_SOFTIRQ_STS] &= ~ SF_IRQ_LOAD_EEPROM;
			XBYTE[SYS_SOFTIRQ_EN] &= (~SF_IRQ_LOAD_EEPROM);
			LoadNVMCfg();
		}
		if(softInt&PATCH_UVC_PP)
		{
			XBYTE[SYS_SOFTIRQ_STS] &= ~ PATCH_UVC_PP;
			XBYTE[SYS_SOFTIRQ_EN] &= (~PATCH_UVC_PP);
			PatchUvcPP();
		}
		if(softInt&SF_IRQ_INIT_ISP)
		{
			XBYTE[SYS_SOFTIRQ_STS] &= ~ SF_IRQ_INIT_ISP;
			XBYTE[SYS_SOFTIRQ_EN] &= (~SF_IRQ_INIT_ISP);
			SF_IRQ_InitISP();
		}
		if(softInt&SF_IRQ_INIT_ISP_HW)
		{
			XBYTE[SYS_SOFTIRQ_STS] &= ~ SF_IRQ_INIT_ISP_HW;
			XBYTE[SYS_SOFTIRQ_EN] &= (~SF_IRQ_INIT_ISP_HW);
			SF_IRQ_InitISPHW();
		}
#ifdef _LENOVO_IDEA_EYE_
		if(softInt&SF_IRQ_MTD)
		{
			XBYTE[SYS_SOFTIRQ_STS] &= ~ SF_IRQ_MTD;
			XBYTE[SYS_SOFTIRQ_EN] &= (~SF_IRQ_MTD);
			StartVideoImage(DIS_USB_STREAMING);
			XBYTE[SYS_SOFTIRQ_EN]=0;
		}
#endif		
#ifdef _IQ_TABLE_CALIBRATION_
		if(softInt&PATCH_IQ_TABLE)
		{
			XBYTE[SYS_SOFTIRQ_STS] &= ~ PATCH_IQ_TABLE;
			XBYTE[SYS_SOFTIRQ_EN] &= (~PATCH_IQ_TABLE);
			LoadIQTablePatch();
		}		
#endif
#ifdef _UBIST_TEST_
		if (softInt&SF_IRQ_READ_SENSORID)
		{
			XBYTE[SYS_SOFTIRQ_STS] &= ~ SF_IRQ_READ_SENSORID;
			XBYTE[SYS_SOFTIRQ_EN] &= (~SF_IRQ_READ_SENSORID);
			ISP_Clock_Switch(SWITCH_ON);
			ReadSensorID();

			if(g_bySensorIsOpen == SENSOR_OPEN)
			{
				StopImageTransfer(EN_DELAYTIME);
			}
		}
#endif
	}

	//	Setup packet interrupt, occurs in USB enumeration process
	if (epirqstat & SETUP_PKT_INT)
	{
		EP0_FFFLUSH();

		//if the setup data is SYNC_FRAME,stall it
		if((XBYTE[SETUP_PKT_bmREQUST_TYPE] == 0x82) && (XBYTE[SETUP_PKT_bREQUEST] == 0x0C))
		{
			STALL_EPCTL();
		}
		// Clear Interrupt pendding flag
		XBYTE[EPTOP_IRQSTAT] = SETUP_PKT_INT;
		
#ifdef _EP0_LPM_L1_
		// add FW force remote wakeup, for maybe  enter LPM L1 status when EP0 data out
		XBYTE[LPM_CFG] |=FORCE_FW_REMOTE_WAKEUP;
#endif

#ifdef	_IC_CODE_
#else	//_IC_CODE_
		if(XBYTE[USBSTAT] & HIGH_SPEED)
		{
			//PHYRegisterWrite(0x85, 0x02, 0x0E);
			PHYRegisterWrite(0x85, 0xe2);
		}
#endif	//!_IC_CODE_
		USBProSetupPKT();
	}

	if (epirqstat & EPA_INT)
	{
		// EPA interrupt
		if(XBYTE[EPA_IRQSTAT] & EPA_IRQ_IN_TOKEN)
		{
			XBYTE[EPA_IRQEN] &= ~EPA_IRQ_IN_TOKEN; 	// disable EPA in-token interrupt
			XBYTE[EPA_IRQSTAT] = EPA_IRQ_IN_TOKEN;	// clear EPA in-token interrupt
			if(g_bybTrigger==CTL_TRIGGER_STS_NORMAL)
			{
#ifdef _WIN81METRO_BALCKSCREEN_
#ifdef _SENSOR_ESD_ISSUE_
				if (1 == g_byESDExist)
				{
					XBYTE[EPA_CTL] |= EPA_FIFO_FLUSH;
					// start video transfer
					StartVideoImage(EN_USB_STREAMING);
					g_byESDExist = 0;					
				}	
#endif
#else
				XBYTE[EPA_CTL] |= EPA_FIFO_FLUSH;
				// start video transfer
				StartVideoImage(EN_USB_STREAMING);				
#endif				
			}
			else if(g_bybTrigger==CTL_TRIGGER_STS_TRNS_STILL)
			{
				//start still image transfer
				StartStillImage();

				//finish still image transfer
				g_bybTrigger = CTL_TRIGGER_STS_NORMAL;

				StartVideoImage(EN_USB_STREAMING);
			}
		}
		else
		{
			XBYTE[EPA_IRQSTAT] = 0xff;		// clear EPA in-token interrupt
		}
	}

#ifdef _UAC_EXIST_
	if(epbirqstat & EPB_IRQ_IN_TOKEN)
	{
		XBYTE[EPB_IRQEN] &= ~EPB_IRQ_IN_TOKEN;  // disable EPB in-token interrupt
		XBYTE[EPB_IRQSTAT] = EPB_IRQ_IN_TOKEN;  // clear EPB in-token interrupt
		StartAudioTransfer();
	}
#endif

	// hemonel 2008-03-27: statictis interrupt process
	if(g_byStartVideo == 1)
	{

#ifdef _NOLPML1_HANG_
		if (ispirqstat & ISP_FRAMEABORT_INT)
		{
			XBYTE[RF_LWM_0] = 0;
			XBYTE[RF_LWM_1] = 0;
			XBYTE[ISP_DATA_ENABLE] = 0x00;	//disable isp data trasmit to sie buffer		
			WaitTimeOutTwoReg(RF_WM_ST, BULK_SIE_FIFO_EMPTY, 1,USBSYS_IRQSTAT, SUSPEND_INT,1, 20);	// wait bulk mode SIE  fifo empty			
			XBYTE[EPA_CTL] |= EPA_FIFO_FLUSH;
			XBYTE[RF_LWM_0]=INT2CHAR(g_wLWM, 0);
			XBYTE[RF_LWM_1]=INT2CHAR(g_wLWM, 1);		
			XBYTE[ISP_DATA_ENABLE] = 0x01;	//enable isp data trasmit to sie buffer
			XBYTE[ISP_INT_FLAG0] = ISP_FRAMEABORT_INT;	//clear int status

		}
#endif


#ifdef _CUSTOMER_ISP_
#if (_CHIP_ID_ & _RTS5840_)
		if (!(ispirqstat & ISP_AF_STAT_INT))
#else
		if (ispirqstat & ISP_AF_STAT_INT)
#endif	
		{
			XBYTE[ISP_INT_FLAG0] = ISP_AF_STAT_INT;
			CusAFAlgorithm();
		}

		if(ispirqstat & ISP_AE_STAT_INT)
		{
			XBYTE[ISP_INT_FLAG0] = ISP_AE_STAT_INT;	// clear ISP AE Statictis interrupt
			CusAEAlgorithm();
		}


		if (ispirqstat & ISP_AWB_STAT_INT)
		{
			XBYTE[ISP_INT_FLAG0] = ISP_AWB_STAT_INT;
			CusAWBAlgorithm();
		}

		if(ispirqstat1 & ISP_DATA_START_INT)
		{
			XBYTE[ISP_INT_FLAG0] = ISP_DATA_START_INT;	// clear interrupt
		}


		if(ispirqstat & ISP_FRM_START_INT)
		{
			XBYTE[ISP_INT_FLAG0] = ISP_FRM_START_INT;	// clear interrupt
			CusCalledPerFrame();
		}

#else

#if  ((defined _ENABLE_ANTI_FLICK_ )||(defined _ENABLE_OUTDOOR_DETECT_))
		if(ispirqstat1 & ISP_FLICK_DETECT_INT)
		{
			XBYTE[ISP_INT_FLAG1] = ISP_FLICK_DETECT_INT;
#ifdef _ENABLE_OUTDOOR_DETECT_
			if(g_fAEC_EtGain< AS_ET_FLICK_LEVEL)
			{
				FlickerDetect();
			}
			else
#endif				
			{
#ifdef _ENABLE_ANTI_FLICK_
				AutoBanding();
#endif
			}
			g_byISPABStaticsEn = 1;
		}
#endif


#if (defined _AF_ENABLE_ || defined _SCENE_DETECTION_)
#if (_CHIP_ID_ & _RTS5840_)
		if (!(ispirqstat & ISP_AF_STAT_INT))
#else
		if (ispirqstat & ISP_AF_STAT_INT)
#endif		
		{
#ifdef _DEBUG_3A_USE_GPIO_
			GPIO_L_DRIVE_HIGH(5);
#endif
			XBYTE[ISP_INT_FLAG0] = ISP_AF_STAT_INT;

			GetFrameEdgeSharp();

#ifdef _AF_ENABLE_
			if (FocusAutoItem.Last == CTL_DEF_CT_FOCUS_AUTO)
			{
#ifdef _AF_TEST_
				AF_TRAVERSAL();
#else
				AutoFocus();
#endif
			}		
#endif				

#ifdef _SCENE_DETECTION_
			ASDetectWhiteScene();
#endif

#ifdef _RTK_EXTENDED_CTL_
			RtkExtROIAF();
#endif

			ISP_AFStaticsStart();

#ifdef _DEBUG_3A_USE_GPIO_
			GPIO_L_DRIVE_LOW(5);
#endif
		}
#endif	
	
		if(ispirqstat & ISP_AE_STAT_INT)
		{
#ifdef _DEBUG_3A_USE_GPIO_
			GPIO_L_DRIVE_HIGH(6);
#endif
			XBYTE[ISP_INT_FLAG0] = ISP_AE_STAT_INT;	// clear ISP AE Statictis interrupt
		//	XBYTE[ISP_INT_FLAG1] = ISP_DATA_END_INT;	// clear ccs frame end flag for AE start with the two frame interval

			// bypass the first AE statics
			if(g_byFirstStatic == 1)
			{
				g_byFirstStatic = 0;
			}
			else
			{
#ifdef _LENOVO_IDEA_EYE_
				if (g_byMTDStartGetFrame)
				{
					StopSensor(EN_DELAYTIME);
				}	
#endif	
			
				// AE will split into twice setting exposure and gain at the one AE statics,
				// This is the first AE adjust.
				AutoExposure();
#ifdef _ENABLE_OUTDOOR_DETECT_
				//judge indoor or outdoor mode for AWB boundary adjust.zhangbo ,2012-6-29
				OutdoorModeDetect();
#endif

#ifdef _LENOVO_IDEA_EYE_
				if (g_byMTDStartGetFrame)
				{
					IDEAEYE_MSG(("StopImage\n"));
					StopImageTransfer(EN_DELAYTIME);
				}	
#endif				
			}

#ifdef _FACIAL_EXPOSURE_
			//if driver update window size ,resize it :zhangbo 20110908
			if(g_byFaceExposure_En==0)
			{
				SetAECWindow(0, 0, g_wSensorCurFormatWidth, g_wSensorCurFormatHeight);
			}
			else if(g_byAEFaceWindowSet ==1)
			{
				g_byAEFaceWindowSet=0;
				SetAECFaceExposure();
			}
#endif

#ifdef _FACIALAEWINSET_XU_
			if(g_byXU_FacialAEWin_Set ==1) //hp solution
			{
				g_byXU_FacialAEWin_Set=0;
				FacialAEWinSet_XU();
			}		
#endif			

#ifdef _RTK_EXTENDED_CTL_
			RtkExtROIAE();
			RtkExtROIProcess();
#endif

			XBYTE[ISP_INT_FLAG1]= ISP_DATA_START_INT;	// clear data start flag for next frame AE adjust

#ifdef _AE_NEW_SPEED_ENABLE_
			if(g_byAE_SpeedMode != AE_NORMAL_SPEED)
			{
				g_byAE_StaticsDelay = 2;
			}
			else
#endif
			{
				g_byISPAEStaticsEn = 1;
#ifdef _AE_NEW_SPEED_ENABLE_
				g_byAE_StaticsDelay = 0;
#endif
			}
#ifdef _DEBUG_3A_USE_GPIO_
			GPIO_L_DRIVE_LOW(6);
#endif
		}

		if (ispirqstat & ISP_AWB_STAT_INT)
		{
#ifdef _DEBUG_3A_USE_GPIO_
			GPIO_H_DRIVE_HIGH(9-8);
#endif
			XBYTE[ISP_INT_FLAG0] = ISP_AWB_STAT_INT;


#ifdef _ENABLE_OUTDOOR_DETECT_
			AWBDynamicBoundarybyScene();
#endif


			if(XBYTE[ISP_AWB_STATIS_LOC] == AWB_STATIS_BEFORE_LSC)
			{
#ifdef _NEW_AUTO_LSC_FUNCTION_	
				GetAWBStatic_BeforeLSC();
				SetDynamicLSCbyCT_New();
#else
				GetAWBWindowSt();
#endif
				
				if (WhiteBalanceTempAutoItem.Last == 1)
				{
#ifndef _NEW_AUTO_LSC_FUNCTION_	
					// rough tune AWB
					GetAWBGrayworldGain();	
					GetAWBGrayworldGain2();
#endif

					// Auto LSC
#ifndef	_SCENE_DETECTION_
					SetDynamicLSC();
#else
					ASDetectYellowScene();
#endif

				}				
			}

			if(XBYTE[ISP_AWB_STATIS_LOC] == AWB_STATIS_AFTER_LSC)
			{
#ifdef _NEW_AUTO_LSC_FUNCTION_	
				GetAWBWindowSt();
#endif
			
				if (WhiteBalanceTempAutoItem.Last == 1)
				{
#ifdef _NEW_AUTO_LSC_FUNCTION_	
					// rough tune AWB
					GetAWBGrayworldGain();	
					GetAWBGrayworldGain2();
#endif				
					// fine tune AWB
					GetAWBFineTuneGain();

					// get final AWB gain and set
					GetAWBFinalGain();
					SetAWBGain();
				}
			}

			// hemonel 2012-01-19: switch AWB statistics location
			XBYTE[ISP_AWB_STATIS_LOC] ^= AWB_STATIS_LOC_MASK;
			
			g_byISPAWBStaticsEn = 1;
#ifdef _DEBUG_3A_USE_GPIO_
			GPIO_H_DRIVE_LOW(9-8);
#endif
		}


		if(ispirqstat1 & ISP_DATA_START_INT)
		{
			// AE will split into twice setting exposure and gain at the one AE statics,
			// This is the second AE adjust. First AE adjust at AE interrupt
			// hemonel 2012-04-09: if AE interrupt process done delay to Data start interrupt coming, bypass AE smooth adjust at this data start
			if ((ExposureTimeAutoItem.Last == EXPOSURE_TIME_AUTO_MOD_APERTPRO)&&(XBYTE[ISP_INT_FLAG1] & ISP_DATA_START_INT))
			{
#ifdef _DEBUG_3A_USE_GPIO_
			GPIO_H_DRIVE_HIGH(10-8);
#endif

#ifdef _AE_NEW_SPEED_ENABLE_
				if(g_byAE_SpeedMode == AE_NORMAL_SPEED)
#endif
				{
					AEC_SmoothAdjust();
				}

#ifdef _AE_NEW_SPEED_ENABLE_
				if(g_byAE_StaticsDelay != 0)
				{
					g_byAE_StaticsDelay--;
					if(g_byAE_StaticsDelay == 0)
					{
						g_byISPAEStaticsEn = 1;	
					}
				}				
#endif

#ifdef _DEBUG_3A_USE_GPIO_
			GPIO_H_DRIVE_LOW(10-8);
#endif
			}			

			XBYTE[ISP_INT_FLAG1]= ISP_DATA_START_INT; 
		}

		if(ispirqstat & ISP_FRM_START_INT)
		{
			XBYTE[ISP_INT_FLAG0] = ISP_FRM_START_INT;	// clear interrupt
			
			// dynamice ISP parameter adjust
			g_byDynISP_FrameInterval++;
			if(g_byDynISP_FrameInterval >= 3)
			{
				g_byDynISP_FrameInterval = 0;

				// moire process
				ISPmoireProcess();

				// dynamic ISP parameter based on AWB
				if (WhiteBalanceTempAutoItem.Last == 1)
				{
#ifdef _NEW_AUTO_LSC_FUNCTION_				
					wIllum_CT =GetColorTemperature(g_wBGain_CT/8, g_wRGain_CT/8);
#else
					wIllum_CT =GetColorTemperature(g_wProjectGB/8, g_wProjectGR/8);
#endif
					SetDynamicISP_AWB(wIllum_CT);

					if (( g_wDynamicISPEn&DYNAMIC_LSC_CT_EN) == DYNAMIC_LSC_CT_EN)
					{
#ifdef _SCENE_DETECTION_
						ASGetLightSource();
#else
						SetDynamicLSCbyCT(wIllum_CT);
#endif				
					}
				}

				//when awb disabled, dynamic lsc still work
				SetNLSCRateOfLightSource(g_byLastIllumCT);
	
				// dynamic ISP parameter based on AE
				if (ExposureTimeAutoItem.Last == EXPOSURE_TIME_AUTO_MOD_APERTPRO)
				{
					if(g_wDynamicISPEn != 0)
					{
						SetDynamicISP(ClipWord((U16)(g_fAEC_Gain*16.0),0,255));
					}
				}
			}

			SetDynamicISP_FS();
		}
#endif
		
	}

		
	//by jin: Port reset interrupt
	if (usbirqstat & PORT_RESET_INT)
	{
		//DBG(("@_1\n"));
		//	Interrupt flag is cleared in function
		// Process port reset condition and clear the interrupt status
		USBProReset();

		//XBYTE[0xFE97] = 0x00; // enable PG

#ifdef	_IC_CODE_
#else	//_IC_CODE_
		//PHYRegisterWrite(0x91, 0x02, 0x0E);	//	For SIS chip set
		PHYRegisterWrite(0x91, 0xE2);	//	For SIS chip set
#endif	//!_IC_CODE_
	}

	//by jin: Suspend USB interrupt
	if (usbirqstat & SUSPEND_INT)
	{
#ifdef _LENOVO_IDEA_EYE_
		if (!g_byMTDDetectBackend)
		{
#endif		
	
			XBYTE[USBSYS_IRQSTAT] = SUSPEND_INT;		//by jin: Clear interrupt flag

			//	Pull down to save suspend current
			USBProSuspend();
			//forMAC_Suspend = 0xff;

			XBYTE[USBCTL] |= USB_GO_SUSPEND;
			XBYTE[USBCTL] &= ~(USB_GO_SUSPEND);

			USBProResume();
#ifdef _LENOVO_IDEA_EYE_
		}	
#endif				
	}
#ifdef _HID_BTN_

	if(GpioInt)
	{
		wGPIORiseIntVld = TestGPIOIntEnW(RISING_EDGE)&TestGpioIntFlagW(RISING_EDGE);	
		wGPIOFallIntVld  = TestGPIOIntEnW(FALLING_EDGE)&TestGpioIntFlagW(FALLING_EDGE);	
		
		if((wGPIOFallIntVld |wGPIORiseIntVld)&(0x0001<<HID_BTN_IDX))
		{
			if(wGPIORiseIntVld&(0x0001<<HID_BTN_IDX))
			{
				if(WaitBtnDetectDebounce(HID_BTN_IDX,GPIO_SEL_NORMAL,RISING_EDGE))    // button release		
				{
					g_byHIDBtnLast &= ~HID_BTN_1;
					EPD_DATA_IN(g_byHIDBtnLast);
					EPD_FFV();					
				}
				ClearGpioIntFlagB(HID_BTN_IDX, RISING_EDGE);
			}
			else //Fall Int
			{
				if(WaitBtnDetectDebounce(HID_BTN_IDX,GPIO_SEL_NORMAL,FALLING_EDGE))
				{
					g_byHIDBtnLast |= HID_BTN_1;
					EPD_DATA_IN(g_byHIDBtnLast);
					EPD_FFV();						
				}
				ClearGpioIntFlagB(HID_BTN_IDX, FALLING_EDGE);
			}
		}		
	}

	if (GpioALInt)
	{
		//printf("gpioalint value is 0x%bx\n",GpioALInt);
		
		wGPIORiseIntVld = GpioALInt & GPIO_AL_REDGE_INT;	
		wGPIOFallIntVld  = GpioALInt & GPIO_AL_FEDGE_INT;	

		if (wGPIORiseIntVld)
		{
			if(wGPIORiseIntVld & GPIO_AL0_REDGE_INT)	//GPIO_AL0
			{
				if(WaitBtnDetectDebounce(GPIO_AL0,GPIO_SEL_AL,RISING_EDGE))    // button release		
				{
					g_byHIDBtnLast &= ~HID_BTN_2;
					EPD_DATA_IN(g_byHIDBtnLast);
					EPD_FFV();					
				}
				XBYTE[PG_GPIO_AL_INT_STS] |= GPIO_AL0_REDGE_INT;
			}
			else		//GPIO_AL1
			{
				if(WaitBtnDetectDebounce(GPIO_AL1,GPIO_SEL_AL,RISING_EDGE))    // button release		
				{
					g_byHIDBtnLast &= ~HID_BTN_3;
					EPD_DATA_IN(g_byHIDBtnLast);
					EPD_FFV();					
				}
				XBYTE[PG_GPIO_AL_INT_STS] |= GPIO_AL1_REDGE_INT;				
			}
		}

		if(wGPIOFallIntVld)
		{
			if(wGPIOFallIntVld & GPIO_AL0_FEDGE_INT)	//GPIO_AL0
			{
				if(WaitBtnDetectDebounce(GPIO_AL0,GPIO_SEL_AL,FALLING_EDGE))	//Button Pressed
				{
					g_byHIDBtnLast |= HID_BTN_2;
					EPD_DATA_IN(g_byHIDBtnLast);
					EPD_FFV();						
				}
				XBYTE[PG_GPIO_AL_INT_STS] |= GPIO_AL0_FEDGE_INT;		
			}
			else		//GPIO_AL1
			{
				if(WaitBtnDetectDebounce(GPIO_AL1,GPIO_SEL_AL,FALLING_EDGE))	//Button Pressed
				{
					g_byHIDBtnLast |= HID_BTN_3;
					EPD_DATA_IN(g_byHIDBtnLast);
					EPD_FFV();						
				}
				XBYTE[PG_GPIO_AL_INT_STS] |= GPIO_AL1_FEDGE_INT;						
			}
		}			
	}
//Jimmy.20120607.RTS5840 not have DMIC about.
/*
	if (GpioMICInt)
	{
		//printf("gpio MIC  value is 0x%bx\n",GpioMICInt);
		wGPIORiseIntVld = GpioMICInt & GPIO_MIC_REDGE_INT;	
		wGPIOFallIntVld  = GpioMICInt & GPIO_MIC_FEDGE_INT;	

		if (wGPIORiseIntVld)
		{
			if(wGPIORiseIntVld & GPIO_MICDATA_REDGE_INT)	//GPIO_MIC_DATA
			{
				if(WaitBtnDetectDebounce(GPIO_MIC_DATA,GPIO_SEL_DMIC,RISING_EDGE))    // button release		
				{
					g_byHIDBtnLast &= ~HID_BTN_4;
					EPD_DATA_IN(g_byHIDBtnLast);
					EPD_FFV();		
						//printf(("release \n"));
				}
				else
				{
						//printf(("release glitch\n"));
				}
				XBYTE[DMIC_PAD_INT_STS] |= GPIO_MICDATA_REDGE_INT;
			}
			else		//GPIO_MIC_CLK
			{
				if(WaitBtnDetectDebounce(GPIO_MIC_CLK,GPIO_SEL_DMIC,RISING_EDGE))    // button release		
				{
					g_byHIDBtnLast &= ~HID_BTN_5;
					EPD_DATA_IN(g_byHIDBtnLast);
					EPD_FFV();	
					//printf(("release \n"));
				}
				else
				{
					//printf(("release glitch\n"));
				}
				XBYTE[DMIC_PAD_INT_STS] |= GPIO_MICCLK_REDGE_INT;				
			}
		}

		if(wGPIOFallIntVld)
		{
			if(wGPIOFallIntVld & GPIO_MICDATA_FEDGE_INT)	//GPIO_MIC_DATA
			{
				if(WaitBtnDetectDebounce(GPIO_MIC_DATA,GPIO_SEL_DMIC,FALLING_EDGE))
				{
					g_byHIDBtnLast |= HID_BTN_4;
					EPD_DATA_IN(g_byHIDBtnLast);
					EPD_FFV();	
					//printf(("press \n"));
				}
				else
				{
					//printf(("press glitch\n"));
				}
				XBYTE[DMIC_PAD_INT_STS] |= GPIO_MICDATA_FEDGE_INT;		
			}
			else		//GPIO_AL1
			{
				if(WaitBtnDetectDebounce(GPIO_MIC_CLK,GPIO_SEL_DMIC,FALLING_EDGE))
				{
					g_byHIDBtnLast |= HID_BTN_5;
					EPD_DATA_IN(g_byHIDBtnLast);
					EPD_FFV();	
					//printf(("press \n"));
				}
				else
				{
					//printf(("press glitch\n"));
				}
				XBYTE[DMIC_PAD_INT_STS] |= GPIO_MICCLK_FEDGE_INT;						
			}
		}	
	}
*/
#endif
	//Jimmy.20120710.g_bySIEClkAutoDisEnable=0.so delete it.
	//if((g_bySIEClkAutoDisEnable)&&(g_bySET_CFG==1))
	//{
	//	XBYTE[BLK_CLK_EN] |=SIE_PWRDWN_EN;
	//}

	// hemonel 2009-12-21: RTS5803 delete DW mode
}

//This function will be called at startup.a51
//This function used to init XDATA Space
//XDATA Space:0xD000->0xDDFF (Size:0x0E00)
void InitSRAM(void)
{
#ifdef _PG_EN_
	if(XBYTE[PG_FSM_RST] & SUSPEND_FLAG)
	{
		// Power Gating not init sram again.
	}
	else
#endif
	{
		// init sram
		memset((U8 xdata *)0xD000,0,0x0E00);
	}
}

void SoftInterruptOpenBfrConnect(U8 intType)
{
	XBYTE[SYS_SOFTIRQ_EN]=0;
	// to read sensor ID, set software interrupt 0.
	XBYTE[SYS_SOFTIRQ_STS]= 0x00;
	XBYTE[SYS_SOFTIRQ_EN] = intType;
	IE |= 0x81;
	//XBYTE[SYS_SOFTIRQ_TRIGGER] = intType; //_5803 delt this reg
	XBYTE[SYS_SOFTIRQ_STS] = intType;
	//MCU should go into INT0 ISR
	Delay(1);
	IE &= ~(0x81);
	XBYTE[SYS_SOFTIRQ_EN]=0;
}

#ifdef _UBIST_TEST_
void SoftInterruptOpenAftConnect(U8 intType)
{
	XBYTE[SYS_SOFTIRQ_EN]=0;
	// to read sensor ID, set software interrupt 0.
	XBYTE[SYS_SOFTIRQ_STS]= 0x00;
	XBYTE[SYS_SOFTIRQ_EN] = intType;
//	IE |= 0x81;
	//XBYTE[SYS_SOFTIRQ_TRIGGER] = intType; //_5803 delt this reg
	XBYTE[SYS_SOFTIRQ_STS] = intType;
	//MCU should go into INT0 ISR
	Delay(1);
//	IE &= ~(0x81);
	XBYTE[SYS_SOFTIRQ_EN]=0;
}
#endif
/*
*********************************************************************************************************
*										Initialize MCU
* FUNCTION InitMCU
*********************************************************************************************************
*/
/**
  Initialize MCU: timer1 is 8bit autoload mode and timer0 is 16bit counter mode; initial value of counter
  is 0 and start counting; setting r/w pulse width to default value of four clock cycles.

  \param None

  \retval None
*********************************************************************************************************
*/
void InitMCU(void)
{
	IE = 0x0;			//by jin: In level sensitive mode, firmware can not clear this flag; needn't it

#ifdef _S3_RESUMKEK_
	// Terry: Initialize Timer0, 300us interrupt
	TMOD = 0x11;	//: Timer0 set as 16 bit-counter
	TL0 = g_pTL0ClkTable[g_byCpuClkIdx];
	TH0 = g_pTH0ClkTable[g_byCpuClkIdx];
	PT0 = 1;			// Set Timer Interrupt Priority 'High'
	PX0 = 0;            // Set External INT 0 Priority Low	
	ET0 = 1;			// Enable Timer0 Interrupt
	TR0 = 1;			// Enable Timer0
#endif

	
//Jimmy.20120401.hw timer replace them
/*
	// Terry: Timer0,1 all set as 16 bit-counter
	TMOD = 0x11;

	// Terry: Initialize Timer0, 10ms interrupt
	TL0 = g_pTL0ClkTable[g_byCpuClkIdx];
	TH0 = g_pTH0ClkTable[g_byCpuClkIdx];
	PT0 = 1;			// Set Timer Interrupt Priority 'High'
	ET0 = 1;			// Enable Timer0 Interrupt
	TR0 = 1;			// Enable Timer0

	// Initialize Timer1, 1ms interrupt
	TL1 = g_pTL1ClkTable[g_byCpuClkIdx];
	TH1 = g_pTH1ClkTable[g_byCpuClkIdx];
	PT1 = 1;			// Set Timer Interrupt Priority 'High'
	TR1 = 0;			// Enable Timer1
	ET1 = 0;
*/
	//Use powerfail interrupt to replace timer0 and timer 1
	XBYTE[HW_TM_INIT_LL] = g_aHWTMInitValueLL[g_byCpuClkIdx];
	XBYTE[HW_TM_INIT_LH] = g_aHWTMInitValueLH[g_byCpuClkIdx];
	XBYTE[HW_TM_INIT_HL] = g_aHWTMInitValueHL[g_byCpuClkIdx];
	XBYTE[HW_TM_INIT_HH] = g_aHWTMInitValueHH[g_byCpuClkIdx];
	CLR_TM_INT_STS();	//W1C-clear hw timer interrupt satuts
	PFI = 0;
	HW_TM_INT_EN();	//enable hw timer interrupt

	CKCON = 0;			//by jin: Stretch = 1, R/W pulse width 4 clock cycles

}

void XtalAutoDetect(void)
{
	XBYTE[NON_CRYSTAL_CTL] |= XTAL_FREE_EN|XTAL_ENABLE;	// xtal auto detect reset

	//wait crytal run
	uDelay(5);

	// hemonel 2010-06-08: auto detect xtal bug 0000020
	XBYTE[NON_CRYSTAL_CTL] |= XTAL_AUTO_DETECT_RST;	// xtal auto detect reset
	XBYTE[NON_CRYSTAL_CTL] &= ~XTAL_AUTO_DETECT_RST;

	//wait crytal detect done
	uDelay(1);

	if((XBYTE[NON_CRYSTAL_CTL] & XTAL_MODE_MASK) == XTAL_MODE)	// read xtal mode
	{
		XBYTE[NON_CRYSTAL_CTL] &= ~XTAL_FREE_EN;	// set XTAL_FREE 0 in xtal mode
	}
	else
	{
		XBYTE[NON_CRYSTAL_CTL] &=~ XTAL_ENABLE;	// set xtal enable 0 in non-xtal mode
	}
}

void InitGPIO(void )
{
	// set GPIO driving capacity
	XBYTE[GPIO_SPI_DRV_CTL] = 0x0f;	// Jimmy 2013-03-18: SPI driving capacity 12mA, GPIO driving capacity 12mA

	// GPIO0 external pull down
	XBYTE[GPIO_PULL_CTL_L0] =((GPIO_PULL_DOWN<<6)	// SPI-SCK
	                         			|(GPIO_PULL_DOWN<<4)	// SPI-MOSI
	                        			|(GPIO_PULL_DOWN<<2)	// SPI-MISO
	                          		|(GPIO_PULL_DOWN)); 		// SPI_WP#
	XBYTE[GPIO_PULL_CTL_L1] = ((GPIO_PULL_DOWN<<6)	//GPIO7/LED/TXD	
							|(GPIO_PULL_DOWN<<4)	//GPIO6
							|(GPIO_PULL_DOWN<<2)	// GPIO5
							|(GPIO_PULL_UP));		// GPIO 4(SPI_CS#)
	XBYTE[GPIO_PULL_CTL_H0] = ((GPIO_PULL_DOWN<<6)	// GPIO11
							|(GPIO_PULL_DOWN<<4)	// GPIO10
							|(GPIO_PULL_DOWN<<2)	// GPIO9
							|GPIO_PULL_DOWN); 		// GPIO8
	XBYTE[PG_GPIO_AL_CTRL1] =(PG_GPIO_AL_SLEWRATE_FAST|PG_GPIO_AL_DRIVING_8MA
							|(GPIO_NO_PULL<<2)	//  GPIO_AL 1(SSOR_RST#) no pull
							|GPIO_PULL_DOWN);		// GPIO_AL 0 pull down
#ifdef _IC_CODE_
	XBYTE[GPIO_L] = 0x00;		//GPIO7/LED/TXD Drive low
#else
	XBYTE[GPIO_L] = 0x80;		//Jimmy.FPGA LED opposite vs ASIC
#endif
	XBYTE[GPIO_H] = 0x00;
#ifdef _DEBUG_3A_USE_GPIO_
	SetGPIODirW(0x0FE0);
#else
	SetGPIODirW(0x0080);// /GPIO7/LED output, others input : 0 input, 1 output
#endif

	XBYTE[PG_GPIO_AL_CTRL0] = GPIO_AL1_OUTPUT;	// GPIO_AL0 as input and pull down,GPIO_AL1 as output and drive low
	
#ifdef _RS232_DEBUG_
	XBYTE[SR_MCU_CTL] = 0X03; // TXD/GPIO7 share , Enable TXD function.
#endif

	XBYTE[GPIO_INT_CTL] &= (~GPIO_GLB_INT_EN); //  global gpio int interrupt disable.
#ifdef _HID_BTN_
	//GPIO0 Example
	XBYTE[GPIO_DIR_L] &= 0xFE;  //GPIO 0 set 0 for input
	XBYTE[GPIO_PULL_CTL_L0] &= ~0xFC;	//GPIO not  pull up,extern pull up
	XBYTE[GPIO_IRQEN_FALL_L] |= 0x01;  //GPIO 0 falling interrupt enable
	XBYTE[GPIO_IRQEN_RISE_L] |= 0x01;  //GPIO 0 rising interrupt enable
	XBYTE[GPIO_IRQSTAT_FALL_L] = 0xFF;	//Clear int status
	XBYTE[GPIO_IRQSTAT_FALL_H] = 0xFF;	//Clear int status
	XBYTE[GPIO_INT_CTL] = 0x80;  //global interrupt interrupt enable

	//GPIO_AL0 Example
	XBYTE[PG_GPIO_AL_CTRL1] 	= (PG_GPIO_AL_SLEWRATE_FAST|PG_GPIO_AL_DRIVING_8MA
							|(GPIO_NO_PULL<<2)	
							|GPIO_NO_PULL);	
	XBYTE[PG_GPIO_AL_CTRL0] = GPIO_AL1_OUTPUT;	// GPIO_AL0 and as input
	XBYTE[PG_GPIO_AL_INT_STS] = GPIO_AL0_FEDGE_INT | GPIO_AL0_REDGE_INT
								| GPIO_AL1_FEDGE_INT | GPIO_AL1_REDGE_INT;	//Clear Interrupt Status
	XBYTE[PG_GPIO_AL_INT_EN] = GPIO_AL0_FEDGE_INT | GPIO_AL0_REDGE_INT;	//Interrupt Enable

	//Jimmy.20120607.RTS5840 not have DMIC about.
	////GPIO_DMIC Example
	//XBYTE[DMIC_CTL] = 0x00;
	//XBYTE[DMIC_PAD_CTL] = 0x03;
	//XBYTE[DMIC_PAD_INT_STS] = 0x0F;
	//XBYTE[DMIC_PAD_INT_EN] = 0x0F;
#endif

#ifdef _LENOVO_IDEA_EYE_
	XBYTE[PG_DELINK_CTRL] = PAD_DELINK_OUT;	
#endif
}

#ifdef _UAC_EXIST_
void InitUAC(void)
{
	XBYTE[DMIC_CTL] = DMIC2UAC_EN;		//default path:dmic to uac
	XBYTE[ADF_CFG] = g_byADF_Chn_Switch |AD_DATA_WIDTH_16;
	
	//Loudness
	if (g_byAudioAttrCurr&UAC_LOUDNEDD_CTRL)
	{
	 	XBYTE[ADF_CTRL0] |= 0xA0;			
	}
	else
	{
	 	XBYTE[ADF_CTRL0] &= 0x0F;			

	}

	//Mute	
	if(g_byAudioAttrCurr&UAC_MUTE_CTRL) //Mute
	{
		XBYTE[ADF_CTRL1] = (AD_DMIC_SEL |HIGH_PASS_FILTER_EN |DMIC_LEFT_RISING_EDGE_SEL|DMIC_RIGHT_FALLING_EDGE_SEL|LEFT_CHANNEL_MUTE | RIGHT_CHANNEL_MUTE );				
	}
	else//unmute
	{
		XBYTE[ADF_CTRL1] =AD_DMIC_SEL |HIGH_PASS_FILTER_EN  |DMIC_LEFT_RISING_EDGE_SEL|DMIC_RIGHT_FALLING_EDGE_SEL;
	}

	ADF_PARAM_UPDATE();
}
#endif

void InitSIE(U8 byPG_EN)
{
#ifdef	_IC_CODE_
	U8 bytmp;
#endif

	// hemonel 2010-05-11: power gating need not initialize them
	if(byPG_EN == 0)
	{
		XBYTE[USBCTL] = USB_DISCONNECT;

		//	USB normal test mode
		XBYTE[USBTEST] = 0;

#ifdef _FT2_REMOVE_TEST
		XBYTE[UTMI_TST] = 0x42;	//	Return full speed
		XBYTE[UTMI_TST] = 0x00;
#endif
		// hemonel 2010-05-11: delete for DP glitch before usb connect
		//	XBYTE[UTMI_TST] = 0x42;	//	Return full speed
		//	XBYTE[UTMI_TST] = 0x00;

		//XBYTE[EP0_CFG] = EP_EN;//EP0 always enable
		XBYTE[EP0_CTL] =EP0_FIFO_FLUSH;
		//	XBYTE[EP0_MAXPKT] = MAX_EP0_PKT_SIZE;	// hemonel 2010-05-11: modify register default value to 0x40
		XBYTE[EP0_STAT] = 0xFF;
		XBYTE[EP0_IRQSTAT] = 0xFF;
		//EPA
#ifndef _BULK_ENDPOINT_
		XBYTE[EPA_CFG] = EPA_NUMBER1;
#endif
		XBYTE[EPA_CTL] = EPA_FIFO_FLUSH;
		XBYTE[EPA_IRQSTAT] = 0xFF;


		XBYTE[EPC_CFG] = EPC_NUMBER3;
		XBYTE[EPC_CTL] = EPC_FIFO_FLUSH;
		XBYTE[EPC_IRQSTAT] = 0xFF;
#ifdef _HID_BTN_
		XBYTE[EPD_CFG] = EPD_NUMBER4;
		XBYTE[EPD_CTL] = EPD_FIFO_FLUSH;
		XBYTE[EPD_IRQSTAT] = 0xFF;
#endif
	}
#ifdef _HID_BTN_
	else
	{
		XBYTE[EPD_MAXPKT] = 0x01;	//PowerGating not store it.
	}
#endif

	//	Enable interrupt needed -- setup packet interrupt, endpoint A(bulk out) interrupt
#ifdef _UAC_EXIST_
	XBYTE[EPTOP_IRQEN] = SETUP_PKT_INT| EPA_INT|EPB_INT;
#else
	XBYTE[EPTOP_IRQEN] = SETUP_PKT_INT| EPA_INT;
#endif

	// hemonel 2010-05-11: power gating need not initialize them
	if(byPG_EN == 0)
	{
		// at power gating, don't clear SIE interrupt because SIE run before MCU run.
		XBYTE[EPTOP_IRQSTAT] = 0XFF;
	}
	//	Enable port reset interrupt, suspend interrupt
	// hemonel 2010-05-11: power gating need not initialize them
	if(byPG_EN == 1)
	{
		// at power gating, don't read sensor ID so that don't enable SOF interrupt
#ifdef _NF_RESTORE_
		XBYTE[USBSYS_IRQEN] = g_byPG_USBSYS_IRQEN|SOF_INT;
#else
		XBYTE[USBSYS_IRQEN] = g_byPG_USBSYS_IRQEN;
#endif
		XBYTE[EPA_CFG] =g_byPG_EPA_CFG;
		XBYTE[EPA_CTL] =g_byPG_EPA_CTL;
		XBYTE[EPA_IRQEN] = g_byPG_EPA_IRQEN;	// Enable EPA in-token interrupt
		XBYTE[EPA_IRQSTAT] = 0xff;
		
		XBYTE[RF_EPA_TRANSFER_SIZE_0]=INT2CHAR(g_wPG_EPA_TRANSFER_SIZE, 0); 
		XBYTE[RF_EPA_TRANSFER_SIZE_1]=INT2CHAR(g_wPG_EPA_TRANSFER_SIZE, 1);
		XBYTE[RF_LWM_0]=INT2CHAR(g_wPG_LWM, 0);
		XBYTE[RF_LWM_1]=INT2CHAR(g_wPG_LWM, 1);
		XBYTE[RF_HWM_0]=INT2CHAR(g_wPG_HWM, 0);
		XBYTE[RF_HWM_1]=INT2CHAR(g_wPG_HWM, 1);

	}
	else
	{
		XBYTE[USBSYS_IRQEN] = PORT_RESET_INT| SUSPEND_INT |SOF_INT;	// hemonel 2009-11-20: add SOF interrupt for readsensorID
		XBYTE[USBSYS_IRQSTAT ] =0XFF;
	}

#ifdef	_IC_CODE_
	// for new phy ,no value need setting except for 0xE4 bit 1 FORCECAL , new PHY address for usbcam APHY
	// TOGGLE PHY register 0xE4 bit[1].
	// hemonel 2010-05-11: power gating need not initialize them
	if(byPG_EN == 0)
	{
		// hemonel 2011-06-10: adjust slew rate for eye-diagram and slew rate
		//					   adjust output swing for test J level over than spec
		XBYTE[REG_HSTX_CFG] = 0x52;

		// power gating not recalibration sensitivity
	//	PHYRegisterWrite_Mask(0x00, 0xE4, 0x02);
	//	PHYRegisterWrite_Mask(0x02, 0xE4, 0x02);
		XBYTE[REG_PHY_CFG_0] &= ~0x08;
		XBYTE[REG_PHY_CFG_0] |= 0x08;
	}

	switch(g_byPHYHSRxTxPwrDownMode)
	{
	case (SP_USB_HSRX_PWRDWN_EN|SP_USB_HSTX_PWRDWN_EN):
		bytmp= 0X07;		// hemonel 2010-12-10: TX-RX power down saving current 3.72mA
		break;
	case SP_USB_HSRX_PWRDWN_EN:
		bytmp=0x02;
		break;
	case SP_USB_HSTX_PWRDWN_EN:
		bytmp=0x05;
		break;
	default:
		bytmp=0;
		break;
	}
	PHYRegisterWrite(bytmp, 0xC3);

	//2008-12-23,cheney.cai , fix DTM Selective suspend bug. DPHY bug.
	//	PHYRegisterWrite(0x78, 0xf1);	// hemonel 2010-05-11: RTS5822 already modify default value to 0x78 .

	// hemonel 2010-01-18: set zo to 0x1B for debug Prano full speed bug
	//PHYRegisterWrite(0x1B, 0xE6);
	XBYTE[REG_PHY_CFG_1] = 0X17;//Jimmy.20121015.change from 0x1b to 0x17

	//	PHYRegisterWrite(0x52, 0xE2); // change HSTX current

	//do vendor config PHY register setting.
	// hemonel 2010-09-15: use cache mode modify DPHY setting, sodelete this function
	/*
	for(j=0;j<4;j++)
	{
		if(g_byPhySettingList[j<<1])
		{
			PHYRegisterWrite(g_byPhySettingList[(j<<1) +1], g_byPhySettingList[j<<1]);
		}
	}
	*/
#else	//_IC_CODE_
	//
	//PHYRegisterWrite(0x91, 0x02, 0x0E);
	PHYRegisterWrite(0x91, 0xE2);
	//	PLL1 disable, bandgap 1.1, weaker XTAL strength
	//PHYRegisterWrite(0x64, 0x02, 0x0C);
	PHYRegisterWrite(0x64, 0xC2);
	//	HSTX output swing level: 406.6mV
	//PHYRegisterWrite(0xB9, 0x03, 0x0F);
	PHYRegisterWrite(0xB9, 0xF3);
#endif	//!_IC_CODE_

#ifdef _PH_PTS_EN_
	XBYTE[PH_INFO] = PH_INFO_PTS_EN|PH_INFO_SCR_EN|PH_INFO_EOFHEADER;
#else
	XBYTE[PH_INFO] = PH_INFO_SCR_EN|PH_INFO_EOFHEADER;
#endif
	XBYTE[PH_CTL]  = PH_CTL_PH_WHEN_EMPTY;

#ifdef    _EN_PHY_DBG_MODE_
	XBYTE[PHY_DEBUG_MODE] = 0X01;
#endif

#ifdef _USB2_LPM_ // JQG_add_2010_0415_
	if(byPG_EN == 0)
	{	
#ifdef _EP0_LPM_L1_
		XBYTE[USB_LPM] = (LPM_SUPPORT | LPM_ACK);
#else
		XBYTE[USB_LPM] = (LPM_SUPPORT/* | LPM_NAK*/);
#endif
	}
#ifdef _EP0_LPM_L1_
	XBYTE[LPM_CFG] = 0x10|CFG_EP0_L1_EN;//EP0_VALID_REMOTE_WAKEUP/*|EPC_VALID_REMOTE_WAKEUP*/|CFG_EP0_L1_EN;
#else
	XBYTE[LPM_CFG] = 0x10;//EP0_VALID_REMOTE_WAKEUP/*|EPC_VALID_REMOTE_WAKEUP*/|CFG_EP0_L1_EN;
#endif
#endif

	if(byPG_EN == 0)
	{		
#ifdef _PG_EN_
		XBYTE[PG_FSM_RST] = 0x00;
		XBYTE[SYSCTL] |= RC400K_EN;		//Jimmy.20121206.5840/5829B/5832 have problem when close and up,it maybe run other freq.so RC400K always enable.
		XBYTE[PG_EN] = ISO_EN | DV12S_POWOFF_EN |RESUME_RST_EN | PLL_SW_ON_EN|APHY_HANDLER_EN;

#ifndef _IC_CODE_
		// FPGA use 2M external clock input configure
		XBYTE[PG_TCFG2] = 0xC8;
		XBYTE[PG_TCFG3] = 0x6E;
#endif //_IC_CODE_

		// Jimmy.20120607.Delink just used as gpio or SPI FT2 Cache cs_N
		// delink enable
		//XBYTE[PG_DELINK_CTRL] = PAD_DELINK_PULL_DOWN|DELINK_HIGH_ACTIVE|DELINK_ENABLE;
#else
		XBYTE[PG_FSM_RST] = 0x01;	//Close power gating
#endif

	}
}

void PatchUvcPP(void)
{

		// hemonel 2011-04-26: move after CfgSensorControlAttr() because resolution and fps initialization is in CfgSensorControlAttr() function
		// hemonel 2009-07-30: fix ICH4 S4 resume bug at default resolution and not fix ICH4 S4 resume at other resolution
		g_VsCommit.bmHint=0;
#ifndef _BULK_ENDPOINT_
		g_VsCommit.bFormatIndex=1;
		g_VsCommit.bFrameIndex=1;
		g_byCommitFPS =GetDefaultFPS(UVC_IS_VIEDO, 1, 1, 1);
#endif
		g_VsCommit.dwFrameInterval= GetFrameInterval(UVC_IS_VIEDO, 1, 1, 1, 1);	// only for high speed
		g_VsCommit.dwMaxVideoFrameSize = GetMaxFrameSize(UVC_IS_VIEDO, 1, 1, 1);  // only for high speed
		g_VsCommit.dwMaxPayloadTransferSize=MAX_PAYLOADTRFERSIZE_HS_3072;
		g_VsStlCommit.bFormatIndex= 1;
		g_VsStlCommit.bFrameIndex= 1;
		g_VsStlCommit.bCompressionIndex = 1;
		g_VsStlCommit.dwMaxVideoFrameSize= GetMaxFrameSize(UVC_IS_STILL, 1, 1, 1);

#if (defined _UVC_PPWB_ || defined _UVC_COMMITWB_)
		LoadUVCPPParam();	// property page write back parameter restore
#endif

		//---Add for WhiteBalance Blink test fail bug, when ENABLE UVC PP WB__20100507__
		if(ExposureTimeAutoItem.Last==EXPOSURE_TIME_AUTO_MOD_APERTPRO)
		{
			Ctl_ExposureTimeAbsolut.Info |=CONTROL_INFO_DIS_BY_AUTO|CONTROL_INFO_SP_AUTOUPDATA;
		}
		else
		{
			Ctl_ExposureTimeAbsolut.Info &= ~(CONTROL_INFO_DIS_BY_AUTO|CONTROL_INFO_SP_AUTOUPDATA);
		}
		if(WhiteBalanceTempAutoItem.Last==1)
		{
			Ctl_WhiteBalanceTemp.Info |= CONTROL_INFO_DIS_BY_AUTO;
		}
		else
		{
			Ctl_WhiteBalanceTemp.Info  &=  ~(CONTROL_INFO_DIS_BY_AUTO);
		}

		//2010-03-25 hemonel: when disable gain function, manual white balance USB_IF UVCTest fail.
		g_byWhiteBalanceAutoLast_BackForUVCtest = WhiteBalanceTempAutoItem.Last;

#ifdef _AF_ENABLE_
		if(FocusAutoItem.Last==1)
		{
			Ctl_FocusAbsolut.Info |=CONTROL_INFO_DIS_BY_AUTO|CONTROL_INFO_SP_AUTOUPDATA;
		}
		else
		{
			Ctl_FocusAbsolut.Info &= ~(CONTROL_INFO_DIS_BY_AUTO|CONTROL_INFO_SP_AUTOUPDATA);
		}
#endif
}

//Read mode:		Fast Read(0x0B)  |  Fast Read Dual Out(0x3B)  |  Fast Read Dual InOut(0xBB)  |  MaxClock
//A25L512 ->		yes					yes							yes					100Mhz
//EN25F05 ->		yes					no							no					100Mhz
//MX25L512		yes					no							no					85Mhz
//W25X20B->		yes					yes							yes					50Mhz
//MX25V006E->    yes					yes							no					80Mhz
//PM25LD512C->    yes				yes							no					100Mhz	
/*
*********************************************************************************************************
*											CacheSpeedSpiDiv Function
* FUNCTION CacheSpeedSpiDiv
*********************************************************************************************************
*/
/**
  Change spi sclk clock decided by spi serial flash type.

  \param	byReadMode		spi read mode decided by serial falsh type.
  		byMcuClk			mcu clock need to run.
  		bySpiDiv			div parameter to decide spi sclk freq.

  \retval None
*********************************************************************************************************
*/
static void CacheSpeedSpiDiv(U8 const byReadMode, U8 const byMcuClk, U8 const bySpiDiv)
{
	if (byReadMode == SPI_NORMAL_READ)
	{
		if(byMcuClk >= MCU_CLOCK_30M)		//mcuclk<=30Mhz
		{
			XBYTE[SPI_CLK_DIVIDER0] = 0x00;	//SCLK<=15Mhz
		}
		else if(byMcuClk == MCU_CLOCK_60M)	//mcuclk==60Mhz
		{
			XBYTE[SPI_CLK_DIVIDER0] = 0x01;	//SCLK==15Mhz
		}
		else		//byMcuClk == MCU_CLOCK_120M	//mcuclk==120Mhz
		{
			XBYTE[SPI_CLK_DIVIDER0] = 0x03;	//SCLK==15Mhz				
		}
	}
	else
	{
		if(byMcuClk >= MCU_CLOCK_60M)		//mcuclk<=60Mhz
		{
			XBYTE[SPI_CLK_DIVIDER0] = 0x00;	//SCLK<=30Mhz
		}
		else		//byMcuClk == MCU_CLOCK_120M	//mcuclk==120Mhz
		{
			XBYTE[SPI_CLK_DIVIDER0] = bySpiDiv;	//decided by serial flash				
		}
	}	
}

/*
*********************************************************************************************************
*											CacheSpeed Function
* FUNCTION CacheSpeed
*********************************************************************************************************
*/
/**
  Change mcu clock,spi sclk clock and spi read mode decided by spi serial flash type.

  \param byMcuClk		to decide which mcu clock freq to run,can use macro MCU_CLOCK_xxxM

  \retval None
*********************************************************************************************************
*/
void CacheSpeed(U8 const byMcuClk)
{
	U8 byReadMode = SPI_NORMAL_READ;	//Default Normal Read
	U8 bySpiDiv = 0;	
	
	U8 byClk = (XBYTE[MCUCLK] & MCU_CLK_MASK);

	g_byReadMode = SPI_CADI_MODE0;

	if(byMcuClk == byClk)	//The Same mcu clock,do nothing, just return
	{
		return;
	}

#ifndef _SPI_CLK_FIX_15MHZ_
	//ReadMode and max freq
	//Tidy by Document:\\172.29.17.1\pc\Camera\Datasheet\Flash&EEPROM
	//SerialFlash Operation Freq CheckAndSupportList_20151029.xls
	//Adesto
	if((g_bySFManuID == ADESTO_MANUFACT_ID)&&(g_wSFDeviceID == 0x65))		//AT25F512B-64KB
	{
		byReadMode = SPI_FAST_READ;
		bySpiDiv = 0;	//<=60Mhz
	}
	
	if((g_bySFManuID == ADESTO_MANUFACT_ID)&&(g_wSFDeviceID == 0x12))		//AT25SF041-512KB
	{
		byReadMode = SPI_FAST_READ_DUAL_INOUT;
		bySpiDiv = 0;	//<=60Mhz
	}

	//MXIC
	if((g_bySFManuID == MXIC_MANUFACT_ID)&&(g_wSFDeviceID == 0x05))		// MX25L512EZUI-10G
	{
		byReadMode = SPI_FAST_READ_DUAL_OUT;
		bySpiDiv = 0;	//<=60Mhz
	}			

	if((g_bySFManuID == MXIC_MANUFACT_ID)&&(g_wSFDeviceID == 0x10))		//KH25L1006EMI-10G;MX25L1006EMI-10G;MX25L1006EZUI-10G;MX25V1006F;MX25V512F
	{
		byReadMode = SPI_FAST_READ_DUAL_OUT;
		bySpiDiv = 0;	//<=60Mhz
	}	

	if((g_bySFManuID == MXIC_MANUFACT_ID)&&(g_wSFDeviceID == 0x11))		//MX25V2006E-13G;
	{
		byReadMode = SPI_FAST_READ_DUAL_OUT;
		bySpiDiv = 0;	//<=60Mhz
	}	

	if((g_bySFManuID == MXIC_MANUFACT_ID)&&(g_wSFDeviceID == 0x12))		//KH25L4006EM1L-12G;MX25L4006EM1L-12G;
	{
		byReadMode = SPI_FAST_READ_DUAL_OUT;
		bySpiDiv = 0;	//<=60Mhz
	}	

	if((g_bySFManuID == MXIC_MANUFACT_ID)&&(g_wSFDeviceID == 0x22))		//MX25L512EMC-20G;KH25L5121EOC-20G;MX25L5121EOC-20G;KH25L1021EMC-20G;
	{
		byReadMode = SPI_FAST_READ;
		bySpiDiv = 1;	//<=30Mhz
	}		

	//PMC
	if(g_bySFManuID == PMC_MANU_ID)			//PM25LQ512B-64KB; Pm25LQ010B-128KB
	{
		byReadMode = SPI_FAST_READ_DUAL_OUT;		
        bySpiDiv = 0;	//<=60Mhz
	}

	//Winbond
	if(g_bySFManuID == WINBOND_MANU_ID)	//W25X05CL-64KB/W25X10CL-128KB
	{
		byReadMode = SPI_FAST_READ_DUAL_INOUT;
		bySpiDiv = 0;	//<=60Mhz
	}	

	//GigaDevice
	if(g_bySFManuID == GD_MANUFACT_ID)		//GD25D10BOIG-128KB/GD25D05BOIG-64KB
	{
		byReadMode = SPI_FAST_READ_DUAL_OUT;
		bySpiDiv = 0;	//<=60Mhz
	}	

	//FMDevice
	if(g_bySFManuID == FM_MANUFACT_ID)  //FM25F005-TS-T-G;FM25F005-SO-T-G;   
	{
		byReadMode = SPI_FAST_READ_DUAL_OUT;
		bySpiDiv = 0;	//<=60Mhz
	}

	//PuyaDevice
	if((g_bySFManuID == PUYA_MANUFACT_ID) && (g_wSFDeviceID == 0x09))
	{
		byReadMode = SPI_FAST_READ_DUAL_OUT;
		bySpiDiv = 0;	//<=60Mhz
	}

	switch(byReadMode)
	{
		case SPI_FAST_READ:
			g_byReadMode = SPI_FAST_READ_MODE;
			break;	
		case SPI_FAST_READ_DUAL_OUT:
			g_byReadMode = SPI_FAST_READ_DUAL_OUT_MODE;		
			break;
		case SPI_FAST_READ_DUAL_INOUT:
			g_byReadMode = SPI_FAST_READ_DUAL_INOUT_MODE;		
			break;
		case SPI_NORMAL_READ:
		default:
			g_byReadMode = SPI_CADI_MODE0;
			break;			
	}
#endif
	
	if (byMcuClk < byClk)	//Speed Up
	{
		XBYTE[CACHE_CTL] = CACHE_SPI_CS | byReadMode;		
		CacheSpeedSpiDiv(byReadMode, byMcuClk, bySpiDiv);	
		CHANGE_MCU_CLK(byMcuClk);
	}
	else //Speed Down (byMcuClk > byClk)
	{
		CHANGE_MCU_CLK(byMcuClk);
		CacheSpeedSpiDiv(byReadMode, byMcuClk, bySpiDiv);
		XBYTE[CACHE_CTL] = CACHE_SPI_CS | byReadMode;
	}

}
/*
*********************************************************************************************************
*									Initialize Other Part Of RTS5151
* FUNCTION InitCAM
*********************************************************************************************************
*/
/**
  Initialize all of the control registers in RTSXXX1. The initialization includes setting download mode
  which determine if MCU will boot from external program; setting USB to disconnect state and enable
  resume; setting USB normal test model; enabling endpoint 0 only; disabling DMA; configuration of GPIO;
  enabling setup packet interrupt, bulk-out interrupt, port reset interrupt, suspend interrupt and
  interrupts of cards; clearing all interrupt flags; setting some working mode of IC; configuration of
  LUN according to vendor informations, if there are no informations in EEPROM or Nor flash, setting
  default configuration; controlling LEDs; and finally connect USB and open global interrupt and external
  interrupt 0.

  \param None

  \retval None
*********************************************************************************************************
*/
void	InitCAM(U8 byPG_EN)
{
	if(byPG_EN == 0)
	{
		XBYTE[BLK_CLK_EN] &=(~SIE_PWRDWN_EN); //disable SIE Pwr down function when access SIE registers.

		XtalAutoDetect();
		
	}

	//------------ GPIO initilize-----------------------
	InitGPIO();

	//////////////////////////////////////////
	// Init hardware block
	//////////////////////////////////////////
	Init_I2C();

	// hemonel 2010-04-13: clear interrupt enable before soft interrupt switch to cache mode
	// ------------ clear interruput enable-----------------------
	//clear all USB interrupt enable register
	XBYTE[EPTOP_IRQEN] = 0;
	XBYTE[USBSYS_IRQEN] = 0;

	// clear OCP interrupt
	XBYTE[OCP_INT_EN] = 0x00; //Add; jqg 20091222_ this is must. Clear over current interrupt flag, and disable the int.
	XBYTE[SV28_OCP_CTL] &= ~(SV28_OCP_DETECT_EN|SV28_OCP_AUTO_PWROFF); // disable SV28/ SV18 detect OCP, auto power down
	XBYTE[SV18_OCP_CTL] &= ~(SV18_OCP_DETECT_EN|SV18_OCP_AUTO_PWROFF); // disable SV28/ SV18 detect OCP, auto power down
	// -------------- clear interrutp enable end-----------------

	// hemonel 2010-05-11: power gating need not initialize them
	if(byPG_EN == 0)
	{

		g_bySFManuID  =  XBYTE[SCRATCH0];
		g_wSFDeviceID =  XBYTE[SCRATCH1];
		XBYTE[SCRATCH0]=0;
		XBYTE[SCRATCH1]=0;
		// ------------------- cache mode detect end--------------------
	}

	// hemonel 2010-05-11: power gating need not initialize them
	if(byPG_EN == 0)
	{
		// clear all soft interrupt
		XBYTE[SYS_SOFTIRQ_STS] = 0x00;  // clear all soft interrupt
		XBYTE[SYS_SOFTIRQ_EN] = 0x00;   // disable all soft interrupt

		{
#ifdef _ENABLE_OLT_
			OLT_main();
#endif
			SoftInterruptOpenBfrConnect(SF_IRQ_LOAD_EEPROM);
		}
			SoftInterruptOpenBfrConnect(PATCH_UVC_PP);
#ifdef _IQ_TABLE_CALIBRATION_			
			SoftInterruptOpenBfrConnect(PATCH_IQ_TABLE);
#endif			
			//PatchUvcPP();
		InitSensor();	
	}

	// hemonel 2010-12-10: close MCU PAD OE for saving current 0.64mA
	XBYTE[MCU_OE_CTL] = 0x00;		// close MCU PAD OE

#ifdef _MCU_FAST_
	CacheSpeed(MCU_CLOCK_120M);
#endif

	InitISP(byPG_EN);

#ifdef _UAC_EXIST_
	InitUAC();
#endif
	InitSIE(byPG_EN);

#ifdef _FT2_REMOVE_TEST
	//set GPIO7 to LOW
	GPIO_L_DRIVE_LOW(7);

	POWER_ON_SV28();
	POWER_ON_SV18();
#endif
	XBYTE[USBCTL] = USB_CONNECT;
	// hemonel 2011-01-05: enhance MCU speed for decrease time of AE and AWB calculation

#ifdef RS232_TEST
	printf("Run Cache Now\n");
	uDelay(10);
#endif

	// hemonel 2011-12-15: close MIPI/ISP/MJPEG/Cache I2C clock for save idle power
	XBYTE[BLK_CLK_EN] = SPI_CRC_CLK_EN;

	IE |= 0x81;			//enable external interrupt 0
	HW_TM_EN();		//enable hw tiemer
	EPFI = 1;			//enable power fail interrupt

	return;
}

/*
*********************************************************************************************************
*											Suspend USB
* FUNCTION USBProSuspend
*********************************************************************************************************
*/
/**
  Do what we need to do when USB suspends: disable cards, power off cards, set MCU and card clock rate to
  the slowest frequency, change the pull resistance of relative pins from pull high to pull low. It is
  called by ISR while suspend interrupt occurs.

  \param None

  \retval None
*********************************************************************************************************
*/

void	USBProSuspend(void)
{
	if(g_bySensorIsOpen == SENSOR_OPEN) // hemonel 2009-11-16: add this code to decrease suspend and resume time for hamilton platform winpvt test hibernate focus device be removed bug
	{
		StopImageTransfer(DIS_DELAYTIME);	// not run this code
#ifdef _BULK_ENDPOINT_
		XBYTE[EPA_CTL] = EPA_PACKET_EN|EPA_FIFO_FLUSH;
		
		XBYTE[EPA_IRQEN] = EPA_IRQ_IN_TOKEN;	// Enable EPA in-token interrupt
		XBYTE[EPA_IRQSTAT] = 0xff;
#endif		
	}

	//darcy_lu 2009-12-01: diable tx-rx power down function when suspend
	if (g_byPHYHSRxTxPwrDownMode != 0)
	{
		PHYRegisterWrite(0, 0xC3);
	}

#ifdef _PG_EN_
	{
		// save hardware state to always power on domain
		XBYTE[PG_DEVADDR] = XBYTE[DEVADDR];
		XBYTE[PG_SIE_STATUS] = XBYTE[SIE_STATUS];
		XBYTE[PG_EPA_MAXPKT0] = XBYTE[EPA_MAXPKT0];
		XBYTE[PG_EPA_MAXPKT1] = XBYTE[EPA_MAXPKT1];
		XBYTE[PG_EPB_MAXPKT0] = XBYTE[EPB_MAXPKT0];
		XBYTE[PG_EPB_MAXPKT1] = XBYTE[EPB_MAXPKT1];


		XBYTE[AL_EP_EN_STALL] = ((XBYTE[EPD_CTL]& EPD_STALL_EP)<<3)|
		                        ((XBYTE[EPC_CTL]& EPC_STALL_EP)<<2)|
		                        ((XBYTE[EPB_CTL]& EPB_STALL_EP)<<1)|
		                        ((XBYTE[EPA_CTL]& EPA_STALL_EP))|
		                        ((XBYTE[EPD_CFG]&EPD_EN)>>4)|
		                        ((XBYTE[EPC_CFG]&EPC_EN)>>5)|
		                        ((XBYTE[EPB_CFG]&EPB_EN)>>6)|
		                        ((XBYTE[EPA_CFG]&EPA_EN)>>7);
#ifdef _USB2_LPM_
		XBYTE[AL_LPM_REG] = XBYTE[USB_LPM];
#endif
		// save hardware register to xdata memory for suspend power gating restore
		g_byPG_USBSYS_IRQEN = XBYTE[USBSYS_IRQEN];
		g_byPG_EPA_CFG=XBYTE[EPA_CFG];
#ifdef _S3_RESUMKEK_	
		g_byPG_EPA_CFG |=EPA_TYPE_BULK;
#endif		
		g_byPG_EPA_CTL=XBYTE[EPA_CTL];
		g_byPG_EPA_IRQEN = XBYTE[EPA_IRQEN];
		ASSIGN_INT(g_wPG_EPA_TRANSFER_SIZE, XBYTE[RF_EPA_TRANSFER_SIZE_1], XBYTE[RF_EPA_TRANSFER_SIZE_0])
		ASSIGN_INT(g_wPG_LWM, XBYTE[RF_LWM_1], XBYTE[RF_LWM_0]);
		ASSIGN_INT(g_wPG_HWM, XBYTE[RF_HWM_1], XBYTE[RF_HWM_0]);
#ifdef _NF_RESTORE_
		g_bySuspendFW_N = PHYRegisterRead(0xB7);
		g_bySuspendFW_F = PHYRegisterRead(0xC7);
#endif
		// set suspend flag
		XBYTE[PG_FSM_RST] |= SUSPEND_FLAG;
	}
#endif

//	XBYTE[PG_GPIO_AL_CTRL0] &= ~GPIO_AL0_DRIVING_HIGH;

	// hemonel 2010-12-07: decrease DV12 and DV33 for decrease suspend current
	XBYTE[REG_TUNE] = REG_TUNED33_3V2|REG_TUNED12_1V20;
	
	EPFI=0;
	HW_TM_INT_DIS();
	//ET0 = 0;
	//TR0 = 0;
	//TR1 = 0;
}


/*
*********************************************************************************************************
*											Resume USB
* FUNCTION USBProResume
*********************************************************************************************************
*/
/**
  Do what we need to do when USB resumes: enable cards, recover the pull resistance to high, clear
  interrupt flag, set MCU and card clock rate and check USB LED. It is called by ISR while MCU resumes to
  running.
  If power gating enable, this function will not called. MCU will restart from address 0, so resume code will add in InitCAM() function.

  \param None

  \retval None
*********************************************************************************************************
*/
void	USBProResume(void)
{
	U8 bytmp;

	//We shall enable Timer0 interrupt after resume.
	//TR0 = 1;
	//ET0 = 1;
	//TR1 = 1;
	EPFI=1;
	HW_TM_INT_EN();
	
	//darcy_lu 2009-12-01: enable tx-rx power down function when resume
	switch(g_byPHYHSRxTxPwrDownMode)
	{
	case (SP_USB_HSRX_PWRDWN_EN|SP_USB_HSTX_PWRDWN_EN):
		bytmp= 0X07;
		break;
	case SP_USB_HSRX_PWRDWN_EN:
		bytmp=0x02;
		break;
	case SP_USB_HSTX_PWRDWN_EN:
		bytmp=0x05;
		break;
	default:
		bytmp=0;
		break;
	}
	PHYRegisterWrite(bytmp, 0xC3);
}


/*
*********************************************************************************************************
*											Wait Time Out
* FUNCTION WaitTimeOut
*********************************************************************************************************
*/
/**
  Wait for the register's value equaling to desired value, if not, keep waiting till time out.

  \param wReg		Target register.
  \param byVal		Desired value.
  \param byPolarity	Compare the register with the desired value or the opposite value.
  \param byMSEC		Waiting time with unit of 10-millisecond.

  \retval 0			If time out.
		  1         Desired value matched.
*********************************************************************************************************
*/

bit	WaitTimeOut(U16 wReg, U8 byVal, U8 byPolarity, U8 byMSEC)
{
	//TL0 = g_pTL0ClkTable[g_byCpuClkIdx];
	//TH0 = g_pTH0ClkTable[g_byCpuClkIdx];

	// Set Timeout period
#ifdef _S3_RESUMKEK_
	g_wPollTimeOutCnt = (U16)byMSEC*2+1;
#else	
	g_byPollTimeOutCnt = (byMSEC+1);
#endif

	// Polling condition during timeout period
	do
	{
		if( wReg)
		{
			if (byPolarity)
			{
				if (XBYTE[wReg] & byVal)
				{
					return 1;
				}
			}
			else
			{
				if (!(XBYTE[wReg] & byVal))
				{
					return 1;
				}
			}
		}
	}
#ifdef _S3_RESUMKEK_
	while(g_wPollTimeOutCnt);
#else	
	while(g_byPollTimeOutCnt);
#endif


	// Timeout Occurred
	return	0;
}

#ifdef _NOLPML1_HANG_
bit	WaitTimeOutTwoReg(U16 wReg1, U8 byVal1, U8 byPolarity1, U16 wReg2, U8 byVal2, U8 byPolarity2, U8 byMSEC)
{
	// Set Timeout period
#ifdef _S3_RESUMKEK_
	g_wPollTimeOutCnt = (U16)byMSEC*2+1;
#else	
	g_byPollTimeOutCnt = (byMSEC+1);
#endif

	// Polling condition during timeout period
	do
	{
		if( wReg1)
		{
			if (byPolarity1)
			{
				if (XBYTE[wReg1] & byVal1)
				{
					return 1;
				}
			}
			else
			{
				if (!(XBYTE[wReg1] & byVal1))
				{
					return 1;
				}
			}	
		}
		
		if( wReg2)
		{
			if (byPolarity2)
			{
				if (XBYTE[wReg2] & byVal2)
				{
					return 1;
				}
			}
			else
			{
				if (!(XBYTE[wReg2] & byVal2))
				{
					return 1;
				}
			}	
		}		
	}
#ifdef _S3_RESUMKEK_
	while(g_wPollTimeOutCnt);
#else	
	while(g_byPollTimeOutCnt);
#endif

	// Timeout Occurred
	return	0;
}
#endif

void WaitTimeOut_Delay(U8 by10MSec)
{
	WaitTimeOut(0,0,0,by10MSec);
}

/*
*********************************************************************************************************
*										Hardware Setting
* FUNCTION PHYRegisterWrite
*********************************************************************************************************
*/
/**
  Set some working mode of IC.

  \param setvalue		Value to be set into target register.
  \param lowbyte		Its low nibble is the low nibble of the target register's address.
  \param highbyte		Its high nibble is the high nibble of the target register's address.

  \retval None

  \note			It is added to firmware for IC designer's demands.
*********************************************************************************************************
*/
/*
void PHYRegisterWrite(U8 setvalue, U8 lowbyte,U8 highbyte)
{

	XBYTE[VSTATUSIN] = setvalue;
	XBYTE[VCONTROL] = lowbyte;
	XBYTE[VLOADM] = 0x00;
	XBYTE[VLOADM] = 0x01;

	XBYTE[VCONTROL] = highbyte;
	XBYTE[VLOADM] = 0x00;
	XBYTE[VLOADM] = 0x01;
	return;
}
*/

void PHYRegisterWrite(U8 setvalue, U8 byAddr)
{

	XBYTE[VSTATUSIN] = setvalue;
	XBYTE[VCONTROL] = byAddr & 0x0F;
	XBYTE[VLOADM] = 0x00;
	XBYTE[VLOADM] = 0x01;

	XBYTE[VCONTROL] = (byAddr & 0xF0) >> 4;
	XBYTE[VLOADM] = 0x00;
	XBYTE[VLOADM] = 0x01;
#ifdef _EN_PHY_DBG_MODE_
	XBYTE[VCONTROL] = 0x07;
	XBYTE[VLOADM] = 0x00;
	XBYTE[VLOADM] = 0x01;

	XBYTE[VCONTROL] = (g_byPhyAddrDbg & 0xF0) >> 4;
	XBYTE[VLOADM] = 0x00;
	XBYTE[VLOADM] = 0x01;

	XBYTE[VCONTROL] = g_byPhyAddrDbg & 0x0F;
	XBYTE[VLOADM] = 0x00;
	XBYTE[VLOADM] = 0x01;

#endif
	return;
}


/*
*********************************************************************************************************
*										Hardware Setting
* FUNCTION PHYRegisterread
*********************************************************************************************************
*/
/**
  Set some working mode of IC.


  \param lowbyte		Its low nibble is the low nibble of the target register's address.
  \param highbyte		Its high nibble is the high nibble of the target register's address.

  \retval
  	Value read from target register.

  \note			It is added to firmware for IC designer's demands.
*********************************************************************************************************
*/
/*
U8 PHYRegisterRead(U8 lowbyte,U8 highbyte)
{
	 XBYTE[VCONTROL]  = 0x07;
  	 XBYTE[VLOADM] = 0x00;
	 XBYTE[VLOADM] = 0x00;
	 XBYTE[VLOADM] = 0x01;
	 XBYTE[VCONTROL]  = highbyte;
	 XBYTE[VLOADM] = 0x00;
	 XBYTE[VLOADM] = 0x00;
	 XBYTE[VLOADM] = 0x01;
	 XBYTE[VCONTROL]  = lowbyte;
	 XBYTE[VLOADM] = 0x00;
	 XBYTE[VLOADM] = 0x00;
	 XBYTE[VLOADM] = 0x01;

	 return XBYTE[VSTATUSOUT];

}
*/
U8 PHYRegisterRead(U8 byAddr)
{
#ifdef _EN_PHY_DBG_MODE_
	U8 byTmp;
#endif
	XBYTE[VCONTROL] = 0x07;
	XBYTE[VLOADM] = 0x00;
	XBYTE[VLOADM] = 0x01;

	XBYTE[VCONTROL] = (byAddr & 0xF0) >> 4;
	XBYTE[VLOADM] = 0x00;
	XBYTE[VLOADM] = 0x01;

	XBYTE[VCONTROL] = byAddr & 0x0F;
	XBYTE[VLOADM] = 0x00;
	XBYTE[VLOADM] = 0x01;

	//	delay to make it safe
	_nop_();
	_nop_();
#ifdef 	_EN_PHY_DBG_MODE_
	byTmp=XBYTE[VSTATUSOUT] ;
	XBYTE[VCONTROL] = 0x07;
	XBYTE[VLOADM] = 0x00;
	XBYTE[VLOADM] = 0x01;

	XBYTE[VCONTROL] = (g_byPhyAddrDbg & 0xF0) >> 4;
	XBYTE[VLOADM] = 0x00;
	XBYTE[VLOADM] = 0x01;

	XBYTE[VCONTROL] = g_byPhyAddrDbg & 0x0F;
	XBYTE[VLOADM] = 0x00;
	XBYTE[VLOADM] = 0x01;
	return (byTmp);
#else
	return (XBYTE[VSTATUSOUT]);
#endif
}
/*
#ifdef _IC_CODE_
void PHYRegisterWrite_Mask(U8 setvalue, U8 byAddr,U8 byMask)
{
	U8 byTmp;
	byTmp = PHYRegisterRead(byAddr);
	byTmp &= (~byMask);	// clear these bits to be modified
	byTmp|= (setvalue&byMask);	// set these bits to the desired value
	PHYRegisterWrite(byTmp, byAddr);
}
#endif
*/
/*
//	Timer0 interrupt ISR
//	This ISR is used for WaitTimeOut function .
void Timer_0_Int(void) interrupt 1
{
	bit access_flag = 0;

	if(CHECK_ACCESS_FLASH)
	{
		CHANGE_TO_RAM;
		access_flag = 1;
	}

	// Reload Timer0 initial value
	TL0 = g_pTL0ClkTable[g_byCpuClkIdx];
	TH0 = g_pTH0ClkTable[g_byCpuClkIdx];

	// Terry: This Counter used to wait for timeout polling
	if( g_byPollTimeOutCnt)
	{
		g_byPollTimeOutCnt--;
	}
#ifndef _CACHE_MODE_
	if(g_byPollSpiStuatuTimeOutCnt)
	{
		g_byPollSpiStuatuTimeOutCnt--;
	}
#endif
	//Bjkchen: Timer Counter don't increase more then 650 s
	if(g_wTimerCounterForUSBIF < 65000)
	{
		g_wTimerCounterForUSBIF++;
	}

	if(g_byAE_StableOut_DelayCnt)
	{
		g_byAE_StableOut_DelayCnt--;
	}
#ifdef _DELL_EXU_
	if (g_byStreamActive == 1)
	{
		switch (g_byLEDStatus)
		{
			case 0:
				TURN_LED_OFF();
				break;
			case 1:
				TURN_LED_ON();
				break;
			case 2:
				if (g_byLEDBlinkCnt < 50)
				{
					g_byLEDBlinkCnt ++;
				}
				else
				{
					g_byLEDBlinkCnt = 0;
					LED_TOGGLE();
				}
				break;
			case 3:
				if (g_byLEDBlinkCnt < 25)
				{
					g_byLEDBlinkCnt ++;
				}
				else
				{
					g_byLEDBlinkCnt = 0;
					LED_TOGGLE();
				}
				break;
			case 4:
				if (g_byLEDBlinkCnt < 12)
				{
					g_byLEDBlinkCnt ++;
				}
				else
				{
					g_byLEDBlinkCnt = 0;
					LED_TOGGLE();
				}
				break;
			case 5:
				if (g_byLEDBlinkCnt < 6)
				{
					g_byLEDBlinkCnt ++;
				}
				else
				{
					g_byLEDBlinkCnt = 0;
					LED_TOGGLE();
				}
				break;
			case 6:
				if (g_byLEDBlinkCnt < 3)
				{
					g_byLEDBlinkCnt ++;
				}
				else
				{
					g_byLEDBlinkCnt = 0;
					LED_TOGGLE();
				}
				break;
			default:
				TURN_LED_ON();
				break;
		}
	}
	else
	{
		TURN_LED_OFF();
	}
#endif

#ifdef _SMALL_CACHE_SP_
	CALL_Timer0_BKDoor();
#endif

}

void Timer_1_Int(void) interrupt 3
{
	bit access_flag = 0;

	if(CHECK_ACCESS_FLASH)
	{
		CHANGE_TO_RAM;
		access_flag = 1;
	}
#ifdef _WATCH_DOG_ENABLE_
	//	We clear watch-dog timer every 10ms.
	// Terry: We shall not clear Watch-Dog at Timer0 ISR,
	//        Because TH0,TL0 will be reloaded in WaitTimeOut(),
	//		  This will cause Timer0 sometimes can't be interruptted.
	XBYTE[WTDG_CTL] |= WDG_CNT_CLR|WDG_EN;

#endif

	// Reload Timer1 initial value
	TL1 = g_pTL1ClkTable[g_byCpuClkIdx];
	TH1 = g_pTH1ClkTable[g_byCpuClkIdx];

	if(access_flag) CHANGE_TO_FLASH;
	return;
}
*/
#ifdef _S3_RESUMKEK_

//	Timer0 interrupt ISR
void Timer_0_Int(void) interrupt 1
{

	// Reload Timer0 initial value
	TL0 = g_pTL0ClkTable[g_byCpuClkIdx];
	TH0 = g_pTH0ClkTable[g_byCpuClkIdx];

#ifdef _WATCH_DOG_ENABLE_
	XBYTE[WTDG_CTL] |= WDG_CNT_CLR|WDG_EN;
#endif
	if( g_wPollTimeOutCnt)
	{
		g_wPollTimeOutCnt--;
	}

	//Bjkchen: Timer Counter don't increase more then 650 s
	if(g_wTimerCounterForUSBIF < 65000)
	{
		g_wTimerCounterForUSBIF++;
	}

	if(g_byAE_StableOut_DelayCnt)
	{
		g_byAE_StableOut_DelayCnt--;
	}

	if(g_byAWBExitStableDelay)
	{
		g_byAWBExitStableDelay--;
	}
	
#ifdef _SENSOR_ESD_ISSUE_
	if (g_byStreamOn == 1)
	{
		if (XBYTE[ISP_INT_FLAG1] & ISP_FRM_END_INT)
		{
			XBYTE[ISP_INT_FLAG1] = ISP_FRM_END_INT;
			g_wESDTimerCounter = 0;		
		}
		else
		{
			if(g_wESDTimerCounter < 300)	
			{
				g_wESDTimerCounter++;
			}
			else
			{
				g_wESDTimerCounter = 0;
				ResetTransfer();
				g_byESDExist = 1;				
			}	
		
		}	
	}
#endif
	
#ifdef _DELL_EXU_
	if (g_byStreamActive == 1)
	{
		switch (g_byLEDStatus)
		{
			case 0:
				TURN_LED_OFF();
				break;
			case 1:
				TURN_LED_ON();
				break;
			case 2:
				if (g_byLEDBlinkCnt < 50)
				{
					g_byLEDBlinkCnt ++;
				}
				else
				{
					g_byLEDBlinkCnt = 0;
					LED_TOGGLE();
				}
				break;
			case 3:
				if (g_byLEDBlinkCnt < 25)
				{
					g_byLEDBlinkCnt ++;
				}
				else
				{
					g_byLEDBlinkCnt = 0;
					LED_TOGGLE();
				}
				break;
			case 4:
				if (g_byLEDBlinkCnt < 12)
				{
					g_byLEDBlinkCnt ++;
				}
				else
				{
					g_byLEDBlinkCnt = 0;
					LED_TOGGLE();
				}
				break;
			case 5:
				if (g_byLEDBlinkCnt < 6)
				{
					g_byLEDBlinkCnt ++;
				}
				else
				{
					g_byLEDBlinkCnt = 0;
					LED_TOGGLE();
				}
				break;
			case 6:
				if (g_byLEDBlinkCnt < 3)
				{
					g_byLEDBlinkCnt ++;
				}
				else
				{
					g_byLEDBlinkCnt = 0;
					LED_TOGGLE();
				}
				break;
			default:
				TURN_LED_ON();
				break;
		}
	}
	else
	{
		TURN_LED_OFF();
	}
#endif

}
#endif


void HWTMIntProc(void ) interrupt 6
{
#ifdef _S3_RESUMKEK_
	U8 data bySOFCount;
#endif

	CLR_TM_INT_STS();	//W1C--clear hw timer interrupt status
	PFI = 0;				//clear power fail interrupt

#ifdef _S3_RESUMKEK_
	//when dp/dm go to idle and keep 3mS idle (have no SOF,sof count will not changed )means will go to suspend
	if (1 == g_byStartVideo)
	{
		if (0x00 ==(XBYTE[UTMI_STA]&0xC0))		// DP/DM= 00, go to idle
		{	
			bySOFCount = XBYTE[FNUM0];
			if (l_bySOFCount == bySOFCount)
			{
				l_byChangeFlag ++;
			}
			else
			{
				l_bySOFCount = bySOFCount;
				l_byChangeFlag = 0;
			}

			if (l_byChangeFlag >= 4)		// at least (4+1)*0.5 mS idle 
			{	
				XBYTE[EPA_CFG] &= ~EPA_TYPE_BULK;
				l_byChangeFlag = 0;	
			}
		}
		else
		{
			bySOFCount = XBYTE[FNUM0];
			l_bySOFCount = bySOFCount;
			l_byChangeFlag = 0;			
		}
	}
#else
	
#ifdef _WATCH_DOG_ENABLE_
	XBYTE[WTDG_CTL] |= WDG_CNT_CLR|WDG_EN;
#endif

	if( g_byPollTimeOutCnt)
	{
		g_byPollTimeOutCnt--;
	}
	//Bjkchen: Timer Counter don't increase more then 650 s
	if(g_wTimerCounterForUSBIF < 65000)
	{
		g_wTimerCounterForUSBIF++;
	}

	if(g_byAE_StableOut_DelayCnt)
	{
		g_byAE_StableOut_DelayCnt--;
	}

	if(g_byAWBExitStableDelay)
	{
		g_byAWBExitStableDelay--;
	}


#ifdef _LENOVO_IDEA_EYE_
	if (g_wMTDTimeOutCnt)
	{
		g_wMTDTimeOutCnt--;
	}	
#endif
	
#ifdef _SENSOR_ESD_ISSUE_
	if (g_byStreamOn == 1)
	{
		if (XBYTE[ISP_INT_FLAG1] & ISP_FRM_END_INT)
		{
			XBYTE[ISP_INT_FLAG1] = ISP_FRM_END_INT;
			g_wESDTimerCounter = 0;
		}
		else
		{
			if(g_wESDTimerCounter < 300)
			{
				g_wESDTimerCounter++;
			}
			else
			{
				g_wESDTimerCounter = 0;

				ResetTransfer();
				g_byESDExist = 1;				
			}
		}	
	}
#endif
	
#ifdef _DELL_EXU_
	if (g_byStreamActive == 1)
	{
		switch (g_byLEDStatus)
		{
			case 0:
				TURN_LED_OFF();
				break;
			case 1:
				TURN_LED_ON();
				break;
			case 2:
				if (g_byLEDBlinkCnt < 50)
				{
					g_byLEDBlinkCnt ++;
				}
				else
				{
					g_byLEDBlinkCnt = 0;
					LED_TOGGLE();
				}
				break;
			case 3:
				if (g_byLEDBlinkCnt < 25)
				{
					g_byLEDBlinkCnt ++;
				}
				else
				{
					g_byLEDBlinkCnt = 0;
					LED_TOGGLE();
				}
				break;
			case 4:
				if (g_byLEDBlinkCnt < 12)
				{
					g_byLEDBlinkCnt ++;
				}
				else
				{
					g_byLEDBlinkCnt = 0;
					LED_TOGGLE();
				}
				break;
			case 5:
				if (g_byLEDBlinkCnt < 6)
				{
					g_byLEDBlinkCnt ++;
				}
				else
				{
					g_byLEDBlinkCnt = 0;
					LED_TOGGLE();
				}
				break;
			case 6:
				if (g_byLEDBlinkCnt < 3)
				{
					g_byLEDBlinkCnt ++;
				}
				else
				{
					g_byLEDBlinkCnt = 0;
					LED_TOGGLE();
				}
				break;
			default:
				TURN_LED_ON();
				break;
		}
	}
	else
	{
		TURN_LED_OFF();
	}
#endif
#endif
}


void ISP_Clock_Switch(U8 bySwitch)
{
	if(((XBYTE[BLK_CLK_EN]&ISP_CCS_CLK_EN)==ISP_CCS_CLK_EN)^(bySwitch==SWITCH_ON))
	{
		EnterCritical(); 
		XBYTE[CLKCTL] |= ISP_CLK_CHNG;	
		if(bySwitch ==SWITCH_ON)
		{	
			XBYTE[BLK_CLK_EN] |= ISP_CCS_CLK_EN;	
#ifdef _MIPI_EXIST_
			XBYTE[BLK_CLK_EN] |= MIPI_CLK_EN; 
#endif
		}
		else
		{
			XBYTE[BLK_CLK_EN] &= (~ISP_CCS_CLK_EN);
#ifdef _MIPI_EXIST_
			XBYTE[BLK_CLK_EN] &= ~MIPI_CLK_EN; 
#endif 
		}
		XBYTE[CLKCTL] &=  (~ISP_CLK_CHNG);
		ExitCritical();
	}
}

#if ((defined _ENABLE_MJPEG_)||(defined _ENABLE_M420_FMT_))
void JPEG_Clock_Switch(U8 bySwitch)
{
	if(((XBYTE[BLK_CLK_EN]&JPEG_CLK_EN)==JPEG_CLK_EN)^(bySwitch==SWITCH_ON))
	{
		EnterCritical();
		XBYTE[CLKCTL] |= JPEG_CLK_CHNG;
		if(bySwitch == SWITCH_ON)
		{
			XBYTE[BLK_CLK_EN] |= JPEG_CLK_EN;
		}
		else
		{
			XBYTE[BLK_CLK_EN] &= (~JPEG_CLK_EN);
		}
		XBYTE[CLKCTL] &=  (~JPEG_CLK_CHNG);
		ExitCritical();
	}
}
#endif//_RTS5811_CODE_

void CHANGE_MCU_CLK(U8 MCU_CLK)
{
	EnterCritical();

	if((XBYTE[MCUCLK] & MCU_CLK_MASK) != MCU_CLK)
	{
		XBYTE[MCUCLK] = MCU_CLK;

		_nop_();
		_nop_();
		_nop_();
		_nop_();
		
		g_byCpuClkIdx = MCU_CLK;

		HW_TM_INT_DIS();
		XBYTE[HW_TM_INIT_LL] = g_aHWTMInitValueLL[g_byCpuClkIdx];
		XBYTE[HW_TM_INIT_LH] = g_aHWTMInitValueLH[g_byCpuClkIdx];
		XBYTE[HW_TM_INIT_HL] = g_aHWTMInitValueHL[g_byCpuClkIdx];
		XBYTE[HW_TM_INIT_HH] = g_aHWTMInitValueHH[g_byCpuClkIdx];
		HW_TM_INT_EN();
	}

	ExitCritical();
}

void CHANGE_ISP_CLK(U8 ISP_CLK)
{
	//bit7:4---always zero; bit3:2--- CLKSEL; bit1:0---CLKDIV
	if (
	    ((XBYTE[CLKSEL]&ISP_CLKSEL_MASK) !=( (ISP_CLK<<2)&ISP_CLKSEL_MASK))//check if need change clock
	    ||((XBYTE[CLKDIV]&ISP_CLKDIV_MASK) != ((ISP_CLK<<4)&ISP_CLKDIV_MASK))
	)

	{
		EnterCritical();
		XBYTE[CLKCTL] |= ISP_CLK_CHNG;

		XBYTE[CLKSEL] = (XBYTE[CLKSEL] &(~ISP_CLKSEL_MASK) ) |((ISP_CLK<<2)&ISP_CLKSEL_MASK);

		XBYTE[CLKDIV] = (XBYTE[CLKDIV] &(~ISP_CLKDIV_MASK)) |((ISP_CLK<<4)&ISP_CLKDIV_MASK);

		_nop_();
		_nop_();
		_nop_();
		_nop_();

		XBYTE[CLKCTL] &=  (~ISP_CLK_CHNG);
		ExitCritical();
	}
}

#if ((defined _ENABLE_MJPEG_)||(defined _ENABLE_M420_FMT_))
void CHANGE_JPEG_CLK(U8 JPEG_CLK)
{
	//bit7:4---always zero; bit3:2--- CLKSEL; bit1:0---CLKDIV
	if (
	    ((XBYTE[CLKSEL]&JPEG_CLKSEL_MASK) !=( (JPEG_CLK<<4)&JPEG_CLKSEL_MASK))//check if need change clock
	    ||((XBYTE[CLKDIV]&JPEG_CLKDIV_MASK) != ((JPEG_CLK<<6)&JPEG_CLKDIV_MASK))
	)
	{
		EnterCritical();
		XBYTE[CLKCTL] |= JPEG_CLK_CHNG;

		XBYTE[CLKSEL] = (XBYTE[CLKSEL] &(~JPEG_CLKSEL_MASK) ) |((JPEG_CLK<<4)&JPEG_CLKSEL_MASK);

		XBYTE[CLKDIV] = (XBYTE[CLKDIV] &(~JPEG_CLKDIV_MASK)) |((JPEG_CLK<<6)&JPEG_CLKDIV_MASK);


		_nop_();
		_nop_();
		_nop_();
		_nop_();

		XBYTE[CLKCTL] &=  (~JPEG_CLK_CHNG);
		ExitCritical();
	}

}
#endif //#ifndef _RTS5811_CODE_

U8 CHANGE_CCS_CLK(U8 CCS_CLK)
{
	U8 hclk_en = XBYTE[CCS_CLK_SWITCH] & CCS_CLK_ENABLE;

	if ((CCS_CLK & SSC_CLK_SEL) == SSC_CLK_SEL)
	{
		XBYTE[SSCPLL_RS] 		= SSC_8X_EN|SSCPLL_RS_1;

		//5821 SSC_Root_CLK=24Mhz,5827 SSC_Root_CLK=48Mhz need to div 2 by FW
		if( (CCS_CLK&CCS_CLK_DIVIDER_128)<CCS_CLK_DIVIDER_128) //5827
		{
			CCS_CLK += 1;
		}


	}

	if((XBYTE[CCS_CLK_SWITCH] & CCS_CLK_MASK) != CCS_CLK)
	{
		EnterCritical();

		XBYTE[CCS_CLK_SWITCH] = CCS_CLK_CHANGE;
		XBYTE[CCS_CLK_SWITCH] = CCS_CLK_CHANGE|CCS_CLK; //clock
		_nop_();
		_nop_();
		_nop_();
		_nop_();
		XBYTE[CCS_CLK_SWITCH] = CCS_CLK;
		XBYTE[CCS_CLK_SWITCH] = hclk_en | CCS_CLK;
		ExitCritical();
		return TRUE;
	}
	return FALSE;
}

