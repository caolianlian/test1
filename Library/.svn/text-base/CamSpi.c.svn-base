/*
*******************************************************************************
*                           Cardreader RTS5161 project
*
*                           Layer:
*                           Module:
*
*
*
*
* File : SPI.c
*******************************************************************************
*/
/**
*******************************************************************************
  \file SPI.c
  \brief Serial flash and serial eeprom operation.



* \version 0.1
* \date 2006

*******************************************************************************/

/*=============================================================================

                            EDIT HISTORY FOR MODULE

 when           who         what, where, why
 -----------    ---------   ---------------------------------------------------
 2006/9/7      hemonel    Create this file

==============================================================================*/

#include "pc_cam.h"
#include "camutil.h"
#include "CamReg.h"
#include "CamSpi.h"
#include "camvdcfg.h"
#include "global_vars.h"
#include "CamVdCmd.h"

#ifndef _CACHE_MODE_
// MCU_CLOCK_120,60,30,15,7.5,3.75,1.875,60;
// actual spi clock divider = (SPI_CLK_DIV+1) *2
// SPI_CLK = MCU_CLK/((SPI_CLK_DIV+1) *2)
// SPI_CLK <=15MHz
// hemonel 2009-12-29: delete spi eeprom support
//U8 code g_byaSPICLKDIV4EEP[12] = {5,2,1,0,   7,3,1,0,  9,4,2,0};
static U8 code g_byaSPICLKDIV4SF[7] = {3,1,0,0, 0,0,0};
extern void FlushVdCfgBuf(void );

static U8 SPI_RDSR(U8*pData)
{
	U8 ret=FALSE;

	XBYTE[SPI_COMMAND] = SF_RDSR;
	XBYTE[SPI_CA_NUMBER] = SPI_COMMAND_BIT_8 | SPI_ADDRESS_BIT_24;

	XBYTE[SPI_LENGTH0] = 1;
	XBYTE[SPI_LENGTH1] = 0;

	XBYTE[SPI_XCHGM_ADDR0]=(SPI_SCRATCH_BUFFER_BASE%256);

	XBYTE[SPI_TRANSFER] = SPI_TRANSFER_START | SPI_CDI_MODE0;

	if(WaitTimeOut(SPI_TRANSFER, SPI_TRANSFER_END, 1, 10))
	{
		ret=TRUE;
	}
	else
	{
		SPI_RESET_MODULE();
		ret=FALSE;
	}

	*pData = XBYTE[SPI_SCRATCH_BUFFER_BASE];
	
	return ret;
}

/*
*******************************************************************************
*						SPI Polling Status
* FUNCTION sf_PollingStaus
*******************************************************************************
*/
/**
  Serial flash controller polling ready.

  \param msec	Waiting time with unit of 10-millisecond.

  \retval 1 , if operation succeed.
*******************************************************************************
*/
static bit SPI_PollingStaus(U8 const ms10)
{
	U8 byTmp;
	g_byPollSpiStuatuTimeOutCnt = ms10;
	while(g_byPollSpiStuatuTimeOutCnt)
	{
		SPI_RDSR(&byTmp);
		if((byTmp&0x01)==0x00)
			return TRUE;
	}
	return FALSE;
}

void ChangeSPIClockDivider()
{
	U8 bySpiClkStatus=0;

	if(CHECK_SPI_CRC_CLK())
	{
		bySpiClkStatus =1;
	}

	START_SPI_CRC_CLK();
	// change SPI clock divider acording to MCU clock.
	// hemonel 2009-12-29: delete spi eeprom support
	if(SPI_CLK_SINK_SF==g_bySPIClockSinkSel)
	{
		XBYTE[SPI_CLK_DIVIDER0] = g_byaSPICLKDIV4SF[g_byCpuClkIdx];
	}
	//else  //EEPROM
	//{
	//	XBYTE[SPI_CLK_DIVIDER0] = g_byaSPICLKDIV4EEP[g_byCpuClkIdx];
	//}
	if(bySpiClkStatus==0)
	{
		STOP_SPI_CRC_CLK();
	}
}

void ChangeSPIClockSink(U8 const byType)
{
	if(byType!= g_bySPIClockSinkSel)
	{
		g_bySPIClockSinkSel=byType;
		ChangeSPIClockDivider();
	}
}

/*
*******************************************************************************
*						Set SPI Initial Parameter
* FUNCTION SPI_SetInitPara
*******************************************************************************
*/
/**
  Set serial flash and serial eeprom initial parameters.

  \param None

  \retval None
*******************************************************************************
*/
void SPI_InitPara(void)
{
	START_SPI_CRC_CLK();
	SPI_RESET_MODULE();
	XBYTE[SPI_CLK_DIVIDER1] = 0 ;

	XBYTE[SPI_CONTROL] = CS_POLARITY_LOW|DTO_MSB_FIRST|SPI_MODE0|SPI_AUTO_MODE;

	XBYTE[SPI_TCTL] |= 0x01; // by hemonel: 2006-09-14 for EDO = 1  delay half SCK clock cycle to sample.
	XBYTE[SPI_PULL_CTL] = 0x95;	//hemonel 2010-08-09: CS pull up| SCK pull down|MOSI pull down |MISO pull down for SPI mode 0

	ChangeSPIClockDivider();
	STOP_SPI_CRC_CLK();
}

static U8 SPI_CMD(U8 const OpCode)
{
	XBYTE[SPI_COMMAND] = OpCode;
	XBYTE[SPI_CA_NUMBER] = SPI_COMMAND_BIT_8 | SPI_ADDRESS_BIT_24;
	XBYTE[SPI_TRANSFER] = SPI_TRANSFER_START | SPI_C_MODE0;

	if(WaitTimeOut(SPI_TRANSFER, SPI_TRANSFER_END, 1, 10))
	{
		return TRUE;
	}
	else
	{
		SPI_RESET_MODULE();
		return FALSE;
	}
}



static U8 SPI_WRSR(U8 const bySR)
{
	U8 ret=FALSE;

	if(!SPI_CMD(SF_WREN))
	{
		return FALSE;
	}

	if(g_bySFManuID == SST_MANUFACT_ID)
	{
		if(!SPI_CMD(SST_SF_EWSR))
		{
			return FALSE;
		}
	}

	XBYTE[SPI_COMMAND] = SF_WRSR;
	XBYTE[SPI_CA_NUMBER] = SPI_COMMAND_BIT_8 | SPI_ADDRESS_BIT_24;

	XBYTE[SPI_LENGTH0] = 1;
	XBYTE[SPI_LENGTH1] = 0;

	XBYTE[SPI_SCRATCH_BUFFER_BASE]=bySR;

	XBYTE[SPI_XCHGM_ADDR0]=(SPI_SCRATCH_BUFFER_BASE%256);

	XBYTE[SPI_TRANSFER] = SPI_TRANSFER_START | SPI_CDO_MODE0;

	if(WaitTimeOut(SPI_TRANSFER, SPI_TRANSFER_END, 1, 10))
	{
		// hemonel 2009-08-20: add polling status
		ret = SPI_PollingStaus(200);
	}
	else
	{
		SPI_RESET_MODULE();
		ret=FALSE;
	}

	return ret;
}

#ifdef SP_ATMEL_SPI_FLASH
U8 SPI_UnProtectSector(U8 OpCode,U32 dwAddr)
{
	U8 ret=FALSE;

	XBYTE[SPI_COMMAND] = OpCode;
	XBYTE[SPI_CA_NUMBER] = SPI_COMMAND_BIT_8 | SPI_ADDRESS_BIT_24;

	XBYTE[SPI_ADDR0]=LONG2CHAR(dwAddr,0);
	XBYTE[SPI_ADDR1]=LONG2CHAR(dwAddr,1);
	XBYTE[SPI_ADDR2]=LONG2CHAR(dwAddr,2);

	XBYTE[SPI_LENGTH0] = 0;
	XBYTE[SPI_LENGTH1] = 0;

	XBYTE[SPI_TRANSFER] = SPI_TRANSFER_START | SPI_CA_MODE0;

	if(WaitTimeOut(SPI_TRANSFER, SPI_TRANSFER_END, 1, 10))
	{
		ret=TRUE;
	}
	else
	{
		SPI_RESET_MODULE();
		ret=FALSE;
	}

	return ret;
}

U8 SPI_ReadSectorProtectReg(U8 OpCode,U32 dwAddr,U8* pData)
{
	U8 ret=FALSE;

	XBYTE[SPI_COMMAND] = OpCode;
	XBYTE[SPI_CA_NUMBER] = SPI_COMMAND_BIT_8 | SPI_ADDRESS_BIT_24;

	XBYTE[SPI_ADDR0]=LONG2CHAR(dwAddr,0);
	XBYTE[SPI_ADDR1]=LONG2CHAR(dwAddr,1);
	XBYTE[SPI_ADDR2]=LONG2CHAR(dwAddr,2);

	XBYTE[SPI_XCHGM_ADDR0]=(SPI_SCRATCH_BUFFER_BASE%256);

	XBYTE[SPI_LENGTH0] = 1;
	XBYTE[SPI_LENGTH1] = 0;

	XBYTE[SPI_TRANSFER] = SPI_TRANSFER_START | SPI_CADI_MODE0;

	if(WaitTimeOut(SPI_TRANSFER, SPI_TRANSFER_END, 1, 10))
	{
		ret=TRUE;
	}
	else
	{
		SPI_RESET_MODULE();
		ret=FALSE;
	}
	*pData = XBYTE[SPI_SCRATCH_BUFFER_BASE];

	return ret;
}
#endif

/*
*******************************************************************************
*						SPI Read ID instruction processing
* FUNCTION SPI_Write
*******************************************************************************
*/
/**
  SPI controller set parameter of card.

  \param pParameter, the pointer of parameter.

  \retval 1 , if operation succeed.
*******************************************************************************
*/
static U8 SFProg(U8 const OpCode, U8 const AddrLen, U16 const length,U16 const SAddr, U32 const DAddr)
{
	U8 ret=FALSE;
	U8 i;

	XBYTE[SPI_COMMAND] = OpCode;
	if(AddrLen==3)
	{
		XBYTE[SPI_CA_NUMBER] = SPI_COMMAND_BIT_8 | SPI_ADDRESS_BIT_24;
	}
	else //Addr len=2
	{
		XBYTE[SPI_CA_NUMBER] = SPI_COMMAND_BIT_8 | SPI_ADDRESS_BIT_16;
	}

	XBYTE[SPI_LENGTH0] = INT2CHAR(length, 0);
	XBYTE[SPI_LENGTH1] = INT2CHAR(length, 1);

	XBYTE[SPI_XCHGM_ADDR0] = INT2CHAR(SAddr, 0);

	if(AddrLen)// AddrLen>0
	{
		XBYTE[SPI_ADDR0] = LONG2CHAR(DAddr, 0);	// LSB
		XBYTE[SPI_ADDR1] = LONG2CHAR(DAddr, 1);
		XBYTE[SPI_ADDR2] = LONG2CHAR(DAddr, 2);	// MSB
		XBYTE[SPI_TRANSFER] = SPI_TRANSFER_START | SPI_CADO_MODE0;
	}
	else
	{
		XBYTE[SPI_TRANSFER] = SPI_TRANSFER_START | SPI_CDO_MODE0;
	}

	if(WaitTimeOut(SPI_TRANSFER, SPI_TRANSFER_END, 1, 10))
	{
		// write succeed, then polling status
		for(i=0; i<250; i++)
		{
			ret = SPI_PollingStaus(2);
			if(ret)
			{
				break;
			}
		}
	}
	else
	{
		SPI_RESET_MODULE();
		ret=FALSE;
	}

	return ret;
}

static U8 SF_ByteProg(U16 SAddr,U32 DAddr)
{
	return SFProg(SF_PAGE_PROG, 3,1,SAddr, DAddr);
}

static U8 SF_AAIProg(U16 len,U16 SAddr,U32 DAddr)
{
	ASSERT(len>=1)

	if(!SFProg(SST_SF_AAI_PROG, 3, 1, SAddr++, DAddr))
	{
		return FALSE;
	}

	len--;
	while(len)
	{
		if(!SFProg(SST_SF_AAI_PROG, 0,1, (SAddr++), (U32)0))
		{
			return FALSE;
		}
		len--;
	}
	if(SPI_CMD(SF_WRDI))
	{
		if(SPI_PollingStaus(100))
		{
			return TRUE;
		}
	}

	return FALSE;
}

static U8 SF_PageProg(U16 len,U16 SAddr,U32 DAddr)
{
	return SFProg(SF_PAGE_PROG, 3, len, SAddr, DAddr);
}

/*
*******************************************************************************
*						SPI Read ID instruction processing
* FUNCTION SPI_ReadID
*******************************************************************************
*/
/**
  SPI controller set parameter of card.

  \param pParameter, the pointer of parameter.

  \retval 1 , if operation succeed.
*******************************************************************************
*/
//sst25LF020       (0xAB,2,3)
//w25p10              (0x90,2,3)
//AT25FS040         (0x9f,3,0)
static U8 ReadSFID(U8 const OpCode,U8 const IDLen,U8 const AddrLen)
{
	U8 ret=FALSE;
	U8 i;

	ASSERT((IDLen==2)||(IDLen==3));

	XBYTE[SPI_COMMAND] = OpCode;
	XBYTE[SPI_ADDR2] = 0;
	XBYTE[SPI_ADDR1] = 0;
	XBYTE[SPI_ADDR0] = 0;
	XBYTE[SPI_CA_NUMBER] = SPI_COMMAND_BIT_8 | SPI_ADDRESS_BIT_24;
	XBYTE[SPI_LENGTH1] = 0;
	XBYTE[SPI_LENGTH0] = IDLen;

	// clear data buffer first.
	for(i=0; i<8; i++)
	{
		XBYTE[SPI_SCRATCH_BUFFER_BASE+i]=0;
	}
	XBYTE[SPI_XCHGM_ADDR0] = (SPI_SCRATCH_BUFFER_BASE % 256);

	if(AddrLen)	//some Serial flash need dummy address
	{
		XBYTE[SPI_TRANSFER] = SPI_TRANSFER_START | SPI_CADI_MODE0;
	}
	else
	{
		XBYTE[SPI_TRANSFER] = SPI_TRANSFER_START | SPI_CDI_MODE0;
	}

	if(WaitTimeOut(SPI_TRANSFER, SPI_TRANSFER_END, 1, 10))
	{
		g_bySFManuID=XBYTE[SPI_SCRATCH_BUFFER_BASE];
		if(IDLen==2)
		{
			g_wSFDeviceID=(U16) XBYTE[SPI_SCRATCH_BUFFER_BASE+1];
		}
		else // IDLen=3
		{
			g_wSFDeviceID = ((U16)(XBYTE[SPI_SCRATCH_BUFFER_BASE+1])<<8)+XBYTE[SPI_SCRATCH_BUFFER_BASE+2];
		}
		if((g_bySFManuID==0xFF)||(g_bySFManuID==0x00))// miso signal  never toggle, take it as failed
		{
			g_bySFManuID=0;
			ret=FALSE;
		}
		else
		{
			ret=TRUE;
		}
	}
	else
	{
		SPI_RESET_MODULE();
		ret= FALSE;
	}

	return ret;
}

/*
*******************************************************************************
*						SPI Read ID instruction processing
* FUNCTION SPI_Write
*******************************************************************************
*/
/**
  SPI controller set parameter of card.

  \param pParameter, the pointer of parameter.

  \retval 1 , if operation succeed.
*******************************************************************************
*/
static U8 SFErase( void )
{
	U8 ret=FALSE;
	U8 i ;

	XBYTE[SPI_COMMAND] = g_bySFChipEROpCode;
	XBYTE[SPI_CA_NUMBER] = SPI_COMMAND_BIT_8 | SPI_ADDRESS_BIT_24;
	XBYTE[SPI_TRANSFER] = SPI_TRANSFER_START | SPI_C_MODE0;

	if(WaitTimeOut(SPI_TRANSFER, SPI_TRANSFER_END, 1, 10))
	{
		// write succeed, then polling status
		for(i=0; i<4; i++)
		{
			ret=SPI_PollingStaus(250);
			if(ret)
			{
				break;
			}
		}
	}
	else
	{
		SPI_RESET_MODULE();
		ret=FALSE;
	}

	return ret;
}

/*
*******************************************************************************
*						SPI Read ID instruction processing
* FUNCTION SPI_Read
*******************************************************************************
*/
/**
  SPI controller set parameter of card.

  \param pParameter, the pointer of parameter.

  \retval 1 , if operation succeed.
*******************************************************************************
*/
static U8 SFRead(U16 const DLen,U16 const DAddr,U32 const SAddr,U8 const byRMode)
{
	U8 ret=FALSE;

	switch(byRMode)
	{
		case SPI_FAST_READ_MODE:
			XBYTE[SPI_COMMAND] =SF_FAST_READ;
			break;
		case SPI_FAST_READ_DUAL_OUT_MODE:
			XBYTE[SPI_COMMAND] =SF_FAST_READ_DUAL_OUT;
			break;
		case SPI_FAST_READ_DUAL_INOUT_MODE:
			XBYTE[SPI_COMMAND] =SF_FAST_READ_DUAL_INOUT;	
			break;
		case SPI_CADI_MODE0:
		default:
			XBYTE[SPI_COMMAND] =SF_READ;				
			break;
	}

	//DBG_BOOT(("SF Read byRmode value is %bu\n",byRMode));
	
	// Slow read
	XBYTE[SPI_ADDR2] =LONG2CHAR(SAddr,2) ;
	XBYTE[SPI_ADDR1] =LONG2CHAR(SAddr,1) ;
	XBYTE[SPI_ADDR0] =LONG2CHAR(SAddr,0) ;

	XBYTE[SPI_CA_NUMBER] = SPI_COMMAND_BIT_8 | SPI_ADDRESS_BIT_24;

	XBYTE[SPI_LENGTH1] = INT2CHAR(DLen, 1);
	XBYTE[SPI_LENGTH0] = INT2CHAR(DLen, 0);

	XBYTE[SPI_XCHGM_ADDR0]=INT2CHAR(DAddr,0);

	XBYTE[SPI_TRANSFER] = SPI_TRANSFER_START | byRMode;		

	if(WaitTimeOut(SPI_TRANSFER, SPI_TRANSFER_END, 1, 10))
	{
		ret=TRUE;
	}
	else
	{
		SPI_RESET_MODULE();
		ret=FALSE;
	}

	return ret;
}

/*
*******************************************************************************
*						SPI Read ID instruction processing
* FUNCTION SPI_WriteFlash
*******************************************************************************
*/
/**
  SPI controller set parameter of card.

  \param pParameter, the pointer of parameter.

  \retval 1 , if operation succeed.
*******************************************************************************
*/
static U8 SFWrite(U16 DLen,U16 SAddr,U32 DAddr)
{
	U16 wPageLen;

	if(g_bySFProgModeSP&SF_SP_PAGE_PROG)
	{
		// page program
		while(DLen)
		{
			// actual write page length is smaller between length and distance to next page
			wPageLen = SF_PAGE_LEN_32- (DAddr&SF_PAGE_MASK_32); // distance to next page
			if(DLen < wPageLen)
			{
				wPageLen = DLen;
			}
			// start page program
			if(!SPI_CMD(SF_WREN))
			{
				return FALSE;
			}
			if(!SF_PageProg(wPageLen, SAddr, DAddr))
			{
				return FALSE;
			}
			// update address and length for next page program
			DAddr   += wPageLen;
			SAddr   += wPageLen;
			DLen     -= wPageLen;
		}
	}
	else if(g_bySFProgModeSP&SF_SP_AAI_PROG)
	{
		// AAI program
		// start sequential program
		if(!SPI_CMD(SF_WREN))
		{
			return FALSE;
		}
		if(!SF_AAIProg(DLen,SAddr,DAddr))
		{
			return FALSE;
		}
	}
	else  // byte program
	{
		// byte program
		while(DLen)
		{
			// start byte program
			if(!SPI_CMD(SF_WREN))
			{
				return FALSE;
			}
			if(!SF_ByteProg(SAddr, DAddr))
			{
				return FALSE;
			}
			// update address and length for next byte program
			DAddr++;
			SAddr++;
			DLen--;
		}
	}
	if(!SPI_CMD(SF_WRDI))
	{
		return FALSE;
	}
	return TRUE;
}

U8 SPI_EraceSFChip(void )
{
	U8 byTmp1;
#ifdef SP_ATMEL_SPI_FLASH
	U8 byTmp2;
#endif
	if(SFReadFlashID())//have read flash ID before, just return
	{
		if(!SPI_RDSR( &byTmp1))
		{
			return FALSE;
		}

		if(byTmp1&0X8C)// SPRL is locked
		{
			if(!SPI_CMD(SF_WREN)) // enable write
			{
				return FALSE;
			}

			if(g_bySFManuID == SST_MANUFACT_ID)
			{
				if(!SPI_CMD(SST_SF_EWSR)) // enable write
				{
					return FALSE;
				}
			}

			if(!SPI_WRSR( 0))
			{
				return FALSE;
			}
		}
#ifdef SP_ATMEL_SPI_FLASH
		if((g_bySFManuID==ATMEL_MANU_ID)&&(g_wSFDeviceID==0x0400))
		{
			if(byTmp1&0x0C) // setctor protect bits set , need unprotect, before erase chip
			{
				dwAddr=0;
				while(dwAddr<g_wSFCapacity)
				{
					if(!SPI_ReadSectorProtectReg(0X36, (dwAddr<<10), &byTmp2)) // read sector protect register
					{
						return FALSE;
					}

					if(byTmp2 == 0XFF )// sector protected, unprotected it.
					{
						if(!SPI_CMD(SF_WREN))
						{
							return FALSE;
						}

						if(!SPI_UnProtectSector(0x39, (dwAddr<<10)))
						{
							return FALSE;
						}
					}
					dwAddr += 8;
					//DBG(("dwAddr=%x\n",(U16)dwAddr ));
				}
			}

		}
#endif	//SP_ATMEL_SPI_FLASH	
		if(!SPI_CMD(SF_WREN))
		{
			return FALSE;
		}
		return SFErase();
	}

	return FALSE;
}

#ifdef SP_SPI_FLASH_ERASE_SECTOR
U8 SPI_EraceSFSector(U32 DAddr)
{
	U8 byTmp1;
	U8 byTmp2;

	if(SFReadFlashID())//have read flash ID before, just return
	{
		if((g_bySFManuID==ATMEL_MANU_ID)&&(g_wSFDeviceID==0x0400))
		{
			if(!SPI_RDSR( &byTmp1))
			{
				return FALSE;
			}

			if(byTmp1&0X80)// SPRL is locked
			{
				if(!SPI_CMD(SF_WREN)) // enable write
				{
					return FALSE;
				}

				if(!SPI_WRSR( 0))
				{
					return FALSE;
				}
			}

			if(byTmp1&0x0C) // setctor protect bits set , need unprotect, before erase chip
			{
				if(!SPI_ReadSectorProtectReg(0X36, DAddr, &byTmp2)) // read sector protect register
				{
					return FALSE;
				}

				if(byTmp2== 0XFF )// sector protected, unprotected it.
				{
					if(!SPI_CMD(SF_WREN))
					{
						return FALSE;
					}

					if(!SPI_UnProtectSector(0x39, DAddr))
					{
						return FALSE;
					}
				}

			}

		}
		return SFErase(g_bySFSectEROpCode, 3, DAddr);
	}
}
#endif //SP_ATMEL_SPI_FLASH

U8  SFReadFlashID(void )
{

	if(g_bySFManuID)//have read flash ID before, just return
	{
		return TRUE;
	}

#ifdef  SP_OTHER_FLASH_BESIDE_MXIC_EON
	if(ReadSFID(SF_READID0,2,0))//0X90  as opcode,  add len=0, ID len=2
	{
		//unknown manufactor, just use default op code.
		return TRUE;
	}
#endif

	if(ReadSFID(SF_READID0,2,3))//0X90  as opcode, add len=3, ID len=2
	{
		switch(g_bySFManuID)
		{
		case  SST_MANUFACT_ID:
			{
				g_bySFChipEROpCode=SST_SF_CHIP_ERASE;
				g_bySFProgModeSP= SF_SP_BYTE_PROG|SF_SP_AAI_PROG;

				switch(g_wSFDeviceID)
				{
				case 0x8E:
					g_wSFCapacity = (1024) ;
					break;
				case 0x43:
					g_wSFCapacity=(256) ;
					break;
				case 0x44:
					g_wSFCapacity=(512) ;
					break;
				case 0x48:
					g_wSFCapacity = 64;
					break;
				default:
					break;
				}
				return TRUE;
			}
			//break;
		case GD_MANUFACT_ID:
			{
				g_bySFChipEROpCode= GD_SF_CHIP_ERASE;
				g_bySFProgModeSP= SF_SP_PAGE_PROG;

				switch(g_wSFDeviceID)
				{
				case 0x10:
					g_wSFCapacity = (128) ;
					break;
				case 0x05:
					g_wSFCapacity = (64) ;
					break;
				default:
					break;
				}
				return TRUE;
			}
			//break;
		case MXIC_MANUFACT_ID://mxic  manufactor ID
		case EON_MANUFACT_ID:
		case AMIC_MANUFACT_ID:
		case AMIC_MANUFACT_ID2:
			{
				g_bySFProgModeSP= SF_SP_PAGE_PROG|SF_SP_BYTE_PROG;
				switch(g_wSFDeviceID)
				{
				case 0x05:
					g_wSFCapacity = (64) ;
					break;
				default:
					break;
				}
				return TRUE;
			}
		default:
			return TRUE;
		}
	}

	if(ReadSFID(SF_READID1,2,0))//0x9F as opcode , addlen=0,IDlen=2;
	{
		switch (g_bySFManuID)
		{
		case ST_MANUFACT_ID://ST  ID =3
			{
				g_bySFManuID=0;
				if(!ReadSFID(SF_READID1,3,0))//0x9F as opcode , addlen=0,IDlen=3;
				{
					return FALSE;// error
				}
				if(g_bySFManuID==ST_MANUFACT_ID)
				{
					g_bySFProgModeSP=SF_SP_PAGE_PROG;
					switch(g_wSFDeviceID)
					{
					case 0x10:
					case 0x11:
						g_wSFCapacity=(128) ;
						break;
					default:
						g_wSFCapacity=0;
						break;
					}
				}
				return TRUE;
			}
#ifdef 	SP_ATMEL_SPI_FLASH
		case ATMEL_MANU_ID:
			{
				g_bySFManuID=0;
				if(!ReadSFID(SF_READID1,3,0))//0x9F as opcode , addlen=0,IDlen=3;
				{
					return FALSE;// error
				}
				if(g_bySFManuID==ATMEL_MANU_ID)
				{
					g_bySFProgModeSP = SF_SP_PAGE_PROG;
					g_bySFProgModeSP= SF_SP_BYTE_PROG;
					switch(g_wSFDeviceID)
					{
					case 0x1000:
						g_wSFCapacity=(128) ;
						break;
					case 0x4400:
					case 0x0400:
						g_wSFCapacity=(512) ;
						break;
					default:
						g_wSFCapacity=0;
						break;
					}
				}
				return TRUE;
			}
#endif //SP_ATMEL_SPI_FLASH
		case 0x7F: //zouxiaozhi 2012-4-10: PM25LV010 will wrongly respond 0x9F RDID instruction,so correct the ID value at here
			{
				g_bySFManuID= 0x9D;
				g_wSFDeviceID= 0x007C;
				g_bySFProgModeSP=SF_SP_BYTE_PROG|SF_SP_PAGE_PROG;
				g_wSFCapacity=128;
				return TRUE;
			}
		default:
			{
				return TRUE;
			}
		}
	}

	//for AT25F512
	if(ReadSFID(SF_READID3,2,0))
	{
		if(g_bySFManuID==ATMEL_MANU_ID)
		{
			g_bySFProgModeSP=SF_SP_BYTE_PROG|SF_SP_PAGE_PROG;
			g_bySFChipEROpCode=AT25_SF_CHIP_ERASE;

			switch(g_wSFDeviceID)
			{
				case 0x60:
				default:
					g_wSFCapacity = (64) ;
					break;
			}
			return TRUE;
		}

	}

	if(ReadSFID(SF_READID2,2,3)) //PM25Lv010
	{
		if(g_bySFManuID==PMC_MANU_ID)
		{
			g_bySFProgModeSP=SF_SP_BYTE_PROG|SF_SP_PAGE_PROG;

			switch(g_wSFDeviceID)
			{
			case 0x7B:
				g_wSFCapacity=64;
				break;
			case 0x7C:
				g_wSFCapacity=128;
				break;
			case 0x7D:
				g_wSFCapacity=256;
				break;
			case 0x7E:
				g_wSFCapacity=512;
				break;
			default:
				g_wSFCapacity=0;
				break;
			}
			return TRUE;
		}
	}

	return TRUE;
}

U8 SPI_ReadFlashID(U16 const DAddr)
{
	if(SFReadFlashID())
	{
		XBYTE[DAddr]   =  NV_TYPE_SPI_SF;
		XBYTE[DAddr+1] = g_bySFManuID;
		XBYTE[DAddr+2] = INT2CHAR(g_wSFDeviceID,0);
		XBYTE[DAddr+3] = INT2CHAR(g_wSFDeviceID,1);
		XBYTE[DAddr+4] = INT2CHAR(g_wSFCapacity,0);
		XBYTE[DAddr+5] = INT2CHAR(g_wSFCapacity,1);

		return TRUE;
	}

	return FALSE;
}

U8 SPI_ReadFlash(U8 DLen,U16 DAddr,U32 SAddr,U8 byRMode)
{
	return SFRead((U16)DLen,DAddr,SAddr,byRMode);
}

U8 SPI_WriteFlash(U8 DLen,U16 SAddr,U32 DAddr)
{
	U8 byTmp;

	if(SFReadFlashID())//have read flash ID before, just return
	{
		if(!SPI_RDSR( &byTmp))
		{
			return FALSE;
		}

		if(byTmp&0X8C)// SPRL is locked
		{
			if(!SPI_CMD(SF_WREN)) // enable write
			{
				return FALSE;
			}

			if(g_bySFManuID == SST_MANUFACT_ID)
			{
				if(!SPI_CMD(SST_SF_EWSR)) // enable write
				{
					return FALSE;
				}
			}

			if(!SPI_WRSR( 0))
			{
				return FALSE;
			}
		}
		return SFWrite((U16)DLen, SAddr, DAddr);
	}
	return FALSE;
}

U8 SPI_WriteFlashTag(void )
{
	FlushVdCfgBuf();
	XBYTE[SPI_I2C_BUFFER_BASE+0]=SF_DOWNLOAD_TAG0;
	XBYTE[SPI_I2C_BUFFER_BASE+1]=SF_DOWNLOAD_TAG1;
	return SPI_WriteFlash(2,SPI_I2C_BUFFER_BASE, (U32)CACHE_TAG_ADDR0);
}

U8 SPI_RDSRSFlash(U16 const DAddr)
{
	U8 byTmp;
	U8 ret;
	ret =SPI_RDSR( &byTmp);
	XBYTE[DAddr] =byTmp;
	return ret;
}

U8 SPI_WRSRSFlash(U16 const SAddr)
{
	return SPI_WRSR( XBYTE[SAddr]);
}



// hemonel 2009-12-15: delete SPI EEPROM and 3-wire EEPROM
#if 0
/*
*******************************************************************************
*						SPI Read ID instruction processing
* FUNCTION SPI_Write
*******************************************************************************
*/
/**
  SPI controller set parameter of card.

  \param pParameter, the pointer of parameter.

  \retval 1 , if operation succeed.
*******************************************************************************
*/
U8 SpiEEPProg(U8 OpCode, U8 AddrLen, U16 length,U16 SAddr, U32 DAddr)
{
	U8 ret=FALSE;
	//SPI_AUTO_START();
	XBYTE[SPI_COMMAND] = OpCode;
	if(AddrLen==3)
	{
		XBYTE[SPI_CA_NUMBER] = SPI_COMMAND_BIT_8 | SPI_ADDRESS_BIT_24;
	}
	else //Addr len=2
	{
		XBYTE[SPI_CA_NUMBER] = SPI_COMMAND_BIT_8 | SPI_ADDRESS_BIT_16;
	}

	XBYTE[SPI_LENGTH0] = INT2CHAR(length, 0);
	XBYTE[SPI_LENGTH1] = INT2CHAR(length, 1);

	XBYTE[SPI_XCHGM_ADDR0] = INT2CHAR(SAddr, 0);

	if(AddrLen)// AddrLen>0
	{
		XBYTE[SPI_ADDR0] = LONG2CHAR(DAddr, 0);	// LSB
		XBYTE[SPI_ADDR1] = LONG2CHAR(DAddr, 1);
		XBYTE[SPI_ADDR2] = LONG2CHAR(DAddr, 2);	// MSB
		XBYTE[SPI_TRANSFER] = SPI_TRANSFER_START | SPI_CADO_MODE0;
	}
	else
	{
		XBYTE[SPI_TRANSFER] = SPI_TRANSFER_START | SPI_CDO_MODE0;
	}

	if(WaitTimeOut(SPI_TRANSFER, SPI_TRANSFER_END, 1, 10))
	{
		// write succeed, then polling status
		//WaitTimeOut(0,0,0,1);
		//Delay(1);
		ret = SPI_PollingStaus(6);
		if( !ret )
		{
			ret=SPI_PollingStaus(40);
		}
	}
	else
	{
		SPI_RESET_MODULE();
		ret=FALSE;
	}
//	SPI_AUTO_END();
	return ret;
}

U8 SPI_ReadEEP(U8 DLen,U16 DAddr,U32 SAddr)
{
	U8 ret;
//	SPI_AUTO_START();
	XBYTE[SPI_COMMAND] = SPIEEP_READ;
	// Slow read
	XBYTE[SPI_ADDR2] =LONG2CHAR(SAddr,2) ;
	XBYTE[SPI_ADDR1] =LONG2CHAR(SAddr,1) ;
	XBYTE[SPI_ADDR0] =LONG2CHAR(SAddr,0) ;

	XBYTE[SPI_CA_NUMBER] = SPI_COMMAND_BIT_8 | SPI_ADDRESS_BIT_16;

	XBYTE[SPI_LENGTH1] = 0;
	XBYTE[SPI_LENGTH0] = DLen;

	XBYTE[SPI_XCHGM_ADDR0]=INT2CHAR(DAddr,0);

	XBYTE[SPI_TRANSFER] = SPI_TRANSFER_START | SPI_CADI_MODE0;

	if(WaitTimeOut(SPI_TRANSFER, SPI_TRANSFER_END, 1, 10))
	{
		ret=TRUE;
		//DBG(("@2 ret true\n"));
	}
	else
	{
		SPI_RESET_MODULE();
		ret=FALSE;
		//DBG(("@2 ret false\n"));

	}
//	SPI_AUTO_END();
	return ret;
}



U8 SPI_WriteEEP(U8 DLen,U16 SAddr,U32 DAddr)
{
	U16 wTmp=0;
	U8 byTmp;

	if(!SPI_RDSR(SF_RDSR, &byTmp))
	{
		return FALSE;
	}

	if(byTmp&0x0C) // setctor protect bits set , need unprotect, before erase chip
	{
		if(!SPI_CMD(SPIEEP_WREN))
		{
			return FALSE;
		}

		// clear eep status register to write
		if(!SPI_WRSR(SPIEEP_WRSR, 0))
		{
			//	DBG(("write eep status register failed\n"));
			return FALSE;
		}
	}

	while(DLen)
	{
		// actual write page length is smaller between length and distance to next eight byte.
		wTmp   = 32- (DAddr%32);	// distance to next page,avoid page overlap.
		if(DLen < wTmp)
		{
			wTmp = DLen;
		}

		// enable write
		if(!SPI_CMD(SPIEEP_WREN))
		{
			return  FALSE;
		}

		// start page program
		if(!SpiEEPProg(SPIEEP_WRITE,2,wTmp, SAddr, DAddr))
		{
			return FALSE;
		}
		// update address and length for next page program
		DAddr   += wTmp;
		SAddr   += wTmp;
		DLen     -= wTmp;
	}

	return TRUE;
}


U8 SPI_EraseEEP( )
{
	U8 bytmp;
	//only need clear tag .

	for(bytmp=0; bytmp<4; bytmp++)
	{
		XBYTE[SPI_SCRATCH_BUFFER_BASE+4+bytmp]=0xFF;
	}

	if(!SPI_WriteEEP(4,SPI_SCRATCH_BUFFER_BASE+4 , 0))
	{
		return FALSE;
	}
	return TRUE;

}

U8 SPI_ReadEEPID(U8 DLen,U16 DAddr)
{
	return SPI_ReadEEP(DLen,DAddr,0);
}

U8 SPI_RDSREEP(U16 DAddr)
{
	U8 byTmp;
	U8 ret;
	ret=SPI_RDSR(SPIEEP_RDSR, &byTmp);
	XBYTE[DAddr] = byTmp;
	return ret;
}
U8 SPI_WRSREEP(U16 SAddr)
{
	return SPI_WRSR(SPIEEP_WRSR, XBYTE[SAddr]);
}
#endif // 0

#else

U8 SPI_ReadFlashID(U16 const DAddr)
{
	XBYTE[DAddr]   =  NV_TYPE_SPI_SF;
	XBYTE[DAddr+1] = g_bySFManuID;
	XBYTE[DAddr+2] = INT2CHAR(g_wSFDeviceID,0);
	XBYTE[DAddr+3] = INT2CHAR(g_wSFDeviceID,1);
	XBYTE[DAddr+4] = 0;
	XBYTE[DAddr+5] = 0;

	return TRUE;
}

#ifdef _ENABLE_OLT_
bit SPI_CheckTransferEnd(void)
{	
	U16 i,j;

	for (i=0;i<2000;i++)
	{
		for (j=0;j<2000;j++)	
		{
			if ((XBYTE[SPI_COM_TRANSFER] & SPI_COM_TRANSFER_END )== SPI_COM_TRANSFER_END)
			{
				if((XBYTE[SPI_COM_TRANSFER] & SPI_COM_TRANSFER_TIMEOUT) == 0x00)
				{
					// succeed
					return TRUE;
				}				
			}
		}
	}

	// error occured, reset
	XBYTE[SPI_COM_TRANSFER] = SPI_COM_TRANSFER_RESET;

	return FALSE;
}
#else
bit SPI_CheckTransferEnd(void)
{
	if(WaitTimeOut(SPI_COM_TRANSFER, SPI_COM_TRANSFER_END, 1, 250))
	{
		if((XBYTE[SPI_COM_TRANSFER] & SPI_COM_TRANSFER_TIMEOUT) == 0x00)
		{
			// succeed
			return TRUE;
		}
	}

	// error occured, reset
	XBYTE[SPI_COM_TRANSFER] = SPI_COM_TRANSFER_RESET;

	return FALSE;
}
#endif
bit SPI_WB_EraceSFSector(U32 dwDAddr)
{
	// write enable
	XBYTE[SPI_SUB1_COMMAND] = SF_WREN;
	XBYTE[SPI_SUB1_MODE] = SPI_C_MODE0;

	// sector erase :PM25LV512(0x7B), PM25LV010 (0x7C)
	if((g_bySFManuID == 0x9D) && ((g_wSFDeviceID == 0x007B)||(g_wSFDeviceID == 0x007C)))
	{
		// hemonel 2010-11-10: PM25LV512 sector erase command is 0xD7
		XBYTE[SPI_SUB2_COMMAND] = 0xD7;
	}
	else if ((g_bySFManuID == 0x7F) && (g_wSFDeviceID == 0x009D))
	{
		// zouxiaozhi 2012-4-10: just fix the bug when PM25LV010 reaction 0x9F(which should be 0xAB) RDID instruction
		XBYTE[SPI_SUB2_COMMAND] = 0xD7;
	}		
	else
	{
		XBYTE[SPI_SUB2_COMMAND] = 0x20;
	}
	XBYTE[SPI_SUB2_ADDR0] =  LONG2CHAR(dwDAddr, 0);
	XBYTE[SPI_SUB2_ADDR1] =  LONG2CHAR(dwDAddr, 1);
	XBYTE[SPI_SUB2_ADDR2] =  LONG2CHAR(dwDAddr, 2);
	XBYTE[SPI_SUB2_MODE] = SPI_CA_MODE0;

	// polling status ACK
	XBYTE[SPI_SUB3_COMMAND] = SF_RDSR;
	XBYTE[SPI_SUB3_MODE] = SPI_POLLING_MODE0;

	// WB start
	XBYTE[SPI_COM_CRAM_ADDR] = 0x00;
	XBYTE[SPI_TOP_CTL] = 0xFF;
	XBYTE[SPI_COM_TRANSFER] = SPI_COM_TRANSFER_START | SPI_COM_TRANSFER_NUM_3;

	return SPI_CheckTransferEnd();
}

bit SPI_WB_PageProgram(U32 dwDAddr, U8 bySAddr, U16 wLength)
{
	// write enable
	XBYTE[SPI_SUB1_COMMAND] = SF_WREN;
	XBYTE[SPI_SUB1_MODE] = SPI_C_MODE0;

	// page program
	XBYTE[SPI_SUB2_COMMAND] = 0x02;
	XBYTE[SPI_SUB2_ADDR0] =  LONG2CHAR(dwDAddr, 0);
	XBYTE[SPI_SUB2_ADDR1] =  LONG2CHAR(dwDAddr, 1);
	XBYTE[SPI_SUB2_ADDR2] =  LONG2CHAR(dwDAddr, 2);
	XBYTE[SPI_SUB2_LENGTH] = wLength;
	XBYTE[SPI_SUB2_MODE] = SPI_CADO_MODE0;

	// polling status ACK
	XBYTE[SPI_SUB3_COMMAND] = SF_RDSR;
	XBYTE[SPI_SUB3_MODE] = SPI_POLLING_MODE0;

	// WB start
	XBYTE[SPI_COM_CRAM_ADDR] = bySAddr;
	XBYTE[SPI_TOP_CTL] = 0xFF;
	XBYTE[SPI_COM_TRANSFER] = SPI_COM_TRANSFER_START | SPI_COM_TRANSFER_NUM_3;

	return SPI_CheckTransferEnd();
}

bit SPI_WB_Program(U32 dwDAddr, U8 bySAddr, U8 byLength)
{
	U8 data byPageLen;

	// page program
	while(byLength)
	{
		if( g_bySFManuID == 0xBF)
		{
			// actual write page length is smaller between length and distance to next page
			byPageLen = 1;
		}
		else
		{
			// actual write page length is smaller between length and distance to next page
			byPageLen = 32 - (dwDAddr&0x1F);	// distance to next page
			if(byLength < byPageLen)
			{
				byPageLen = byLength;
			}		
		}

		// start page program
		if(!SPI_WB_PageProgram(dwDAddr, bySAddr, byPageLen))
		{
			return FALSE;
		}
		
		// update address and length for next page program
		dwDAddr += byPageLen;
		bySAddr += byPageLen;
		byLength -= byPageLen;
	}

	return TRUE;
}

bit SPI_WB_WriteStatus(U8 byValue)
{
	// write enable
	XBYTE[SPI_SUB1_COMMAND] = SF_WREN;
	XBYTE[SPI_SUB1_MODE] = SPI_C_MODE0;

	//write status
	XBYTE[SPI_BUFFER_BASE_ADDR] = byValue;	// write data
	if( g_bySFManuID == 0xBF)	//PCT25VF010 SST
	{
		// write Status enable
		XBYTE[SPI_SUB2_COMMAND] = SST_SF_EWSR;
		XBYTE[SPI_SUB2_MODE] = SPI_C_MODE0;	

		// write Status
		XBYTE[SPI_SUB3_COMMAND] = 0x01;
		XBYTE[SPI_SUB3_LENGTH] = 1;
		XBYTE[SPI_SUB3_MODE] = SPI_CDO_MODE0;

		// polling status ACK
		XBYTE[SPI_SUB4_COMMAND] = SF_RDSR;
		XBYTE[SPI_SUB4_MODE] = SPI_POLLING_MODE0;

		// WB start
		XBYTE[SPI_COM_CRAM_ADDR] = 0;
		XBYTE[SPI_TOP_CTL] = 0xFF;
		XBYTE[SPI_COM_TRANSFER] = SPI_COM_TRANSFER_START | SPI_COM_TRANSFER_NUM_4;		
	}
	else
	{
		XBYTE[SPI_SUB2_COMMAND] = SF_WRSR;
		XBYTE[SPI_SUB2_LENGTH] = 1;
		XBYTE[SPI_SUB2_MODE] = SPI_CDO_MODE0;

		// polling status ACK
		XBYTE[SPI_SUB3_COMMAND] = SF_RDSR;
		XBYTE[SPI_SUB3_MODE] = SPI_POLLING_MODE0;

		// WB start
		XBYTE[SPI_COM_CRAM_ADDR] = 0x00;
		XBYTE[SPI_TOP_CTL] = 0xFF;
		XBYTE[SPI_COM_TRANSFER] = SPI_COM_TRANSFER_START | SPI_COM_TRANSFER_NUM_3;
	}

	return SPI_CheckTransferEnd();
}

bit SPI_WB_ReadStatus(void)
{
	// read status
	XBYTE[SPI_SUB1_COMMAND] = SF_RDSR;
	XBYTE[SPI_SUB1_LENGTH] = 1;
	XBYTE[SPI_SUB1_MODE] = SPI_CDI_MODE0;

	// WB start
	XBYTE[SPI_COM_CRAM_ADDR] = 0x00;
	XBYTE[SPI_TOP_CTL] = 0xFF;
	XBYTE[SPI_COM_TRANSFER] = SPI_COM_TRANSFER_START | SPI_COM_TRANSFER_NUM_1;

	return SPI_CheckTransferEnd();
}
#if ((defined _PPWB_ADDR_0X10000_)||(defined _SPI_FAST_READ_)||(defined _IQ_TABLE_CALIBRATION_))
bit SPI_WB_Real_Read(U32 dwDAddr, U8 byLength, U8 byMode)
{
	U8 byCommand;
	switch (byMode)
	{
		case SPI_FAST_READ_MODE:
			byCommand = SF_FAST_READ;
			break;
		case SPI_FAST_READ_DUAL_OUT_MODE:
			byCommand = SF_FAST_READ_DUAL_OUT;
			break;
		case SPI_FAST_READ_DUAL_INOUT_MODE:
			byCommand = SF_FAST_READ_DUAL_INOUT;
			break;			
		case SPI_CADI_MODE0:
		default:
			byCommand = SF_READ;
			break;
	}

	XBYTE[SPI_SUB1_COMMAND] = byCommand;	
	XBYTE[SPI_SUB1_MODE] = byMode;

	XBYTE[SPI_SUB1_ADDR0] =  LONG2CHAR(dwDAddr, 0);
	XBYTE[SPI_SUB1_ADDR1] =  LONG2CHAR(dwDAddr, 1);
	XBYTE[SPI_SUB1_ADDR2] =  LONG2CHAR(dwDAddr, 2);
	XBYTE[SPI_SUB1_LENGTH] = byLength;

	// WB start
	XBYTE[SPI_COM_CRAM_ADDR] = 0x00;
	XBYTE[SPI_TOP_CTL] = 0xFF;
	XBYTE[SPI_COM_TRANSFER] = SPI_COM_TRANSFER_START | SPI_COM_TRANSFER_NUM_1;

	if(WaitTimeOut(SPI_COM_TRANSFER, SPI_COM_TRANSFER_END, 1, 250))
	{
		if((XBYTE[SPI_COM_TRANSFER] & SPI_COM_TRANSFER_TIMEOUT) == 0x00)
		{
			// succeed
			return TRUE;
		}
	}

	// error occured, reset
	XBYTE[SPI_COM_TRANSFER] = SPI_COM_TRANSFER_RESET;

	return FALSE;
}
#endif
bit SPI_WB_Read(U32 dwDAddr, U8 byLength, U8 bySubCmd)
{
	U8 i;
	bySubCmd = bySubCmd;

	for(i=0; i<byLength; i++)
	{
		XBYTE[SPI_BUFFER_BASE_ADDR + i] = CBYTE[dwDAddr+i];
	}

	 //succeed
	return TRUE;
}

#endif
