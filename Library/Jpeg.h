#ifndef	_JPEG_H_
#define	_JPEG_H_

void StartJpegEncoder(U8 byMode);
void StopJpegEncoder(void);
void InitJpegTable();
#endif
