#include "pc_cam.h"
#include "camutil.h"
#include "CamReg.h"
#include "CamReset.h"

//close watchdog and power fail interrupt
void PatchMCUReset(void)
{
	EPFI=0;
	PFI = 0;	
	HW_TM_INT_DIS();
	CLR_TM_INT_STS();	
	EA = 0;				//Except PowerFail	
}

void MCUReset(void )
{

#pragma asm
	//XBYTE[AL_CACHE_CFG] = MCU_RESET_ONLY;	
	//6Bytes
	mov DPTR,#0xFFB7
	mov A,#0x40
	movx @DPTR,A
	
	NOP
	NOP
	NOP
	NOP
	NOP

	NOP
	NOP
	NOP
	NOP
	NOP

	NOP
	NOP
	NOP
	NOP
	NOP

	NOP
	NOP
	NOP
	NOP
	NOP

	NOP
	NOP
	NOP
	NOP
	NOP	
#pragma endasm

}
