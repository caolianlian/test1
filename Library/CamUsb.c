/*
*********************************************************************************************************
*                                       Realtek's CardReader Project
*
*                                           Layer: CONTROLLER
*                                       Module: USB CONTROL MODULE
*                                   (c) Copyright 2003-2004, Realtek Corp.
*                                           All Rights Reserved
*
*                                                  V1.0
*
* File : USBCTL.C
*********************************************************************************************************
*/
/**
*******************************************************************************
  \file usbctl.c
  \brief Provide the interface between USB SIE on chip and host.

GENERAL DESCRIPTION:

   This file contains the responses to host USB enumeration process and related operations. It is mainly
   about the setup packets which are used during the setup stage of control transfer. The most important
   function is USBProSetupPKT() which deal with many kinds of host requests, such as standard requests,
   class requests, vendor requests and etc. Within these requests, the standard requests reflect the
   enumeration process of USB driver. The class requests is device specific requests and we use mass
   storage device here. The vendor requests is defined by the vendor for particular purpose, which is,
   for example, updating the firmware thru USB channel.

EXTERNALIZED FUNCTIONS:

  READ_VENDOR_DATA --
    Read vendor data from EEPROM or Nor flash to SRAM2.

  USB_LED_CHECK --
    Check USB speed and turn on relevant LED.

  USBProReset --
    Change USB protocol state to CBW state.

  USBProSetupPKT --
    Respond to host's requests.

Copyright (c) 2003-2004 by Realtek Incorporated.  All Rights Reserved.


*  \version 1.0
*  \date    2003-2004

*******************************************************************************/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

when        who     what, where, why
--------    -----   ----------------------------------------------------------
TBD         Joey    Create this file
09/22/04    Jin     Add comments

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/




#include "pc_cam.h"
#include "camutil.h"
#include "CamReg.h"
#include "CamUvc.h"
#include "CamUsb.h"
#include "CamCtl.h"
#include "CamProcess.h"
#include "Global_vars.h" //jqg080708
#include "camvdcfg.h"
#include "camvdcmd.h"
#ifdef _UAC_EXIST_
#include "CamUac.h"
#endif
//#include "camspi.h"
#include "camisp.h"
#include "ISP_Vars.h"
#include "CamUvcRtkExtDesc.h"
#include "CamReset.h"

void VideoClsReqProc();	 
extern U8 CopyVdCfgByte2SRam(U16 wSAddr,U8* pBuf,U8 byLen);
#ifdef _UBIST_TEST_
extern  void SoftInterruptOpenAftConnect(U8 intType);
#endif


U32 GetMaxFrameSize(U8 IsVideo,U8 FMT, U8 Idx,U8 IsHighSpeed);
U8 GetFormatNum(U8 IsHighSpeed);

extern U8	code 	iProduct[];
extern U8 code VideoDevDesc[18];

#ifdef EP0_LPM_L1_TEST
static U8 code Qualification[8]="REALSIL";
static U8 code g_cVdFwVer[] = 
{
	0x00,//0x00,
	0xF0,//_BACKENDIC_ID_,// backend IC ID
	0x01,//CUSTOMER_PROJECT_NO,  //customer project No.
	0x58,//_CUSTOMER_ID_,
	0x00,//_LIBRARY_VER_H_,
	0x01,//_LIBRARY_VER_L_,
	0x00,//_CUSTOM_FW_VER_H_,
	0x01//_CUSTOM_FW_VER_L_
};
#endif
// hemonel 2009-09-16: decrease usb bandwidth for usb if test gold tree full speed preview fail
//code U16 g_waMaxPacketSizeFS[8]={0,128,256,384,512,768,896,1020};
#ifndef _BULK_ENDPOINT_
code U16 g_waMaxPacketSizeFS[9]= {0,128,256,384,512,768,780,800,800};

code U16 g_waMaxPacketSizeHS_Def[9]= {0,
                                      128,
                                      512,
                                      1024,
                                      768 |(1<<11),
                                      1024|(1<<11),
                                      896 |(2<<11),
                                      1024|(2<<11),
                                      1024|(2<<11)
                                     };
#endif
#ifdef _UAC_EXIST_
//code U8 g_byUACBitResolutionList[8]={2, 2, 2, 2,  2, 3, 3, 3};//pcm24
//xiaozhizou 2014-1-15:fine tune all packet size to multiple of 12
code U16 g_waUACMaxPktSize[8]=		{	
										0x78,		//22.05k, 4byte 0x58
										0xA8,		//32, 4byte	 0x80
										//0xC8,		//44.1k , 4byte 0xb0/0xb4
										0xD8,		//48K,MONO 
										0xD8,		//48k, 4byte	0xc0
										0X1B0,		//96,4byte 0x180 
										0x138,		//44.1k , 6byte 0x108
										0x150,		//48k, 6byte	0x120
										0X294,		//96K,6byte 0x240 
									};

//	#endif
//#endif
//code U32 g_dwaUACSampleRateList[8]=	{	
//										SAMPLING_FREQUENCY_22K05HZ,
//										SAMPLING_FREQUENCY_32KHZ,
//										SAMPLING_FREQUENCY_48KHZ,
//										SAMPLING_FREQUENCY_48KHZ,
//										SAMPLING_FREQUENCY_96KHZ,
//										SAMPLING_FREQUENCY_44K1HZ,
//										SAMPLING_FREQUENCY_48KHZ,
//										SAMPLING_FREQUENCY_96KHZ,
//									};




#endif

//#define  D_IDX_JPEG_STLIMG  		0x01
//#define  D_IDX_YUY2_STLIMG  	0x02
//#define  D_IDX_VDFMT_STLIMG  	0x03
//#define  D_IDX_RAWFMT_STLIMG  	0x04

#ifdef _UBIST_TEST_
extern void SoftInterruptOpenAftConnect(U8 intType);
#endif

Desc_t  UVC_Descs[UVC_DESC_TOTAL_NUM];

#define NF_CALABRITION_INTERVAL 20 	//unit:10ms


//===jqg_20100906_move frome camusb.h===
U8	code iLangID[]=
{
	//-----------------		// String Descriptor: 0x4, 0x3, 0x9, 0x4
	0x04,						// bLength
	USB_STRING_DESCRIPTOR_TYPE,			// bDescriptorType
	0x09,
	0x04
};

U8	code iManufacturer[] =
{
	// MID = 1111 "Generic"
	9, USB_STRING_DESCRIPTOR_TYPE,
	'G',
	'e',
	'n',
	'e',
	'r',
	'i',
	'c'
};

#ifdef _UAC_EXIST_
U8	code 	iProduct_MIC[] =
{
	// MID = 1111 "RTS5101-01"
	20,  3,
	'R',
	'e',
	'a',
	'l',
	't',
	'e',
	'k',
	' ',
	'U',
	'S',
	'B',
	'2',
	'.',
	'0',
	' ',
	'M',
	'I',
	'C'
};
#endif

U8	code 	iSerialNumber[] =
{
	14, USB_STRING_DESCRIPTOR_TYPE,
	'2',
	'0',
	'0',
	'9',
	'0',
	'1',
	'0',
	'1',
	'0',
	'0',
	'0',
	'1'

};
U8 code iDummy[]=
{
	0x4, USB_STRING_DESCRIPTOR_TYPE,
	'0',
	0x00
};



U8	code	Device_Qualifier[]=
{
	// Device Qualifier
	0x0A, // bLength
	0x06, // bDescriptorType
#ifdef _USB2_LPM_ // JQG_add_2010_0415_
	0x01, // bcdUSB (LSB)
#else
	0x00, // bcdUSB (LSB)
#endif
	0x02, // bcdUSB (MSB)
	DEV_CLS_MULTI, //bDeviceClass:                  -> This is a Multi-interface Function Code Device
	DEV_SUBCLS_COMMON, //bDeviceSubClass:               -> This is the Common Class Sub Class
	DEV_PROTOCOL_IAD, //bDeviceProtocol:               -> This is the Interface Association Descriptor protocol
	0x40, // bMaxPacketSixe0
	0x01, // bNumberConfigurations
	0x00  // bReserved
};

code U8 USBTestPacket[]=    //53 bytes (for USB 2.0 Test Mode)
{
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,
	0xAA,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,
	0xEE,0xFE,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
	0xFF,0xFF,0xFF,0xFF,0xFF,0x7F,0xBF,0xDF,
	0xEF,0xF7,0xFB,0xFD,0xFC,0x7E,0xBF,0xDF,
	0xEF,0xF7,0xFB,0xFD,0x7E
};

#ifdef _UAC_EXIST_
U8 code Video_AudioCfgDesc[]=
{
	DESC_SIZE_CONF,      //  bLength:
	USB_CONFIGURATION_DESCRIPTOR_TYPE,      //  bDescriptorType:
	0x45,      //  wTotalLength:                   0x0245
	0x02,
#ifdef _HID_BTN_
	0x05,
#else
	0x04,      //  bNumInterfaces:     
#endif
	0x01,      //  bConfigurationValue:
	I_CONFIGURATION,      //  iConfiguration:
	//0xA0,//enable remote wakeup in bulk mode for LPM 
#ifdef _REMOTE_WAKEUP_SUPPORT_ 
	0xA0,      //  bmAttributes:                     -> Bus Powered 
#else
	0x80,	
#endif

	0xFA       //= 500 mA          MaxPower:
};

#else

// ===>Configuration Descriptor<===//
U8 code VideoCfgDesc[]=
{
	DESC_SIZE_CONF,      //  bLength:
	USB_CONFIGURATION_DESCRIPTOR_TYPE,      //  bDescriptorType:
	0x45,      //  wTotalLength:                   0x0245
	0x02,
#ifdef _HID_BTN_
	0x03,
#else
	0x02,      //  bNumInterfaces:   
#endif
	0x01,      //  bConfigurationValue:
	I_CONFIGURATION,      //  iConfiguration:
	//0xA0,//enable remote wakeup in bulk mode for LPM 
#ifdef _REMOTE_WAKEUP_SUPPORT_ 
	0xA0,      //  bmAttributes:                     -> Bus Powered 
#else
	0x80,	
#endif
	0xFA       //= 500 mA          MaxPower:
};
#endif

//          ===>IAD Descriptor<===
U8 code VideoIADDesc[]=
{
	DESC_SIZE_IAD,           //bLength:
	USB_IAD_DESCRIPTOR_TYPE,           //bDescriptorType:
	IF_IDX_VIDEOCONTROL,           //bFirstInterface:
	0x02,           //bInterfaceCount:
	CC_VIDEO,           //bFunctionClass:                      -> Video Interface Class
	SC_VIDEO_INTERFACE_COLLECTION,           //bFunctionSubClass:               -> Video Interface Collection
	PC_PROTOCOL_UNDEFINED,           //bFunctionProtocol:                 -> PC_PROTOCOL_UNDEFINED protocol
	I_VIDEOFUNCTION            //iFunction:
};

//      ===>Interface Descriptor<===
U8 code VideoControlIfDesc[]=
{
	DESC_SIZE_INTF,      //bLength:
	0x04,      //bDescriptorType:
	IF_IDX_VIDEOCONTROL,      //bInterfaceNumber:
	0x00,      //bAlternateSetting:
	0x01,      //bNumEndpoints:
	CC_VIDEO,      //bInterfaceClass:                     -> Video Interface Class
	SC_VIDEOCONTROL,      //bInterfaceSubClass:                  -> Video Control Interface SubClass
	PC_PROTOCOL_UNDEFINED,      //bInterfaceProtocol:
	I_VIDEOFUNCTION      //iInterface: must be the same as the iFunction
}  ;


//          ===>Class-Specific Video Control Interface Header Descriptor<===
U8 code VideoClsSpcVCIfDesc[]=
{
	DESC_SIZE_CLS_VC_IF,                   //bLength:
	CS_INTERFACE,                   //bDescriptorType:
	VC_HEADER,                   //bDescriptorSubtype:
#ifdef _UVC_1V1_
	(_UVC_VER_1V1%256),
	(_UVC_VER_1V1/256  ),                   //bcdVDC:
#else
	(_UVC_VER_1V0%256),
	(_UVC_VER_1V0/256  ),                   //bcdVDC:
#endif
	0x00,
	0x00,                   //wTotalLength:                 -> Validated
	CONSTLONG2BYTE(DEV_CLOCK_FRQ,0),
	CONSTLONG2BYTE(DEV_CLOCK_FRQ,1),
	CONSTLONG2BYTE(DEV_CLOCK_FRQ,2),
	CONSTLONG2BYTE(DEV_CLOCK_FRQ,3),                   //dwClockFrequency:            = (24000000) Hz
	0x01,                   //bInCollection:              number of VideoStreaming interface
	0x01                    //baInterfaceNr[1]:
};
//          ===>Video Control Input Terminal Descriptor<===
U8 code VideoCameraTrmDesc[]=
{
	DESC_SIZE_CAMERA_TRM,                    //bLength:
	CS_INTERFACE,                    //bDescriptorType:
	VC_INPUT_TERMINAL,                    //bDescriptorSubtype:
	ENT_ID_CAMERA_IT,                    //bTerminalID:
	ITT_CAMERA%256,                    //wTerminalType:                   = (ITT_CAMERA)
	ITT_CAMERA/256,
	0x00,                    //bAssocTerminal:
	I_CAMERA_TERM,                    //iTerminal:  //===>Camera Input Terminal Data
	OBJECT_FOCAL_LEN_MIN%256,                    //wObjectiveFocalLengthMin:
	OBJECT_FOCAL_LEN_MIN/256,
	OBJECT_FOCAL_LEN_MAX%256,                    //wObjectiveFocalLengthMax:
	OBJECT_FOCAL_LEN_MAX/256,
	OCULAR_FOCAL_LEN%256,                    //wOcularFocalLength:
	OCULAR_FOCAL_LEN/256,
	0x03,                    //bControlSize:
	0x0a,                     // no controls for debug first.
	0x00,
	0x00                     //bmControls : 0x0A 0x00 0x04
//     D00 = 0   no -  Scanning Mode
//     D01 = 1  yes -  Auto-Exposure Mode
//     D02 = 0   no -  Auto-Exposure Priority
//     D03 = 1  yes -  Exposure Time (Absolute)
//     D04 = 0   no -  Exposure Time (Relative)
//     D05 = 0   no -  Focus (Absolute)
//     D06 = 0   no -  Focus (Relative)
//     D07 = 0   no -  Iris (Absolute)
//     D08 = 0   no -  Iris (Relative)
//     D09 = 0   no -  Zoom (Absolute)
//     D10 = 0   no -  Zoom (Relative)
//     D11 = 0   no -  Pan (Absolute)
//     D12 = 0   no -  Pan (Relative)
//     D13 = 0   no -  Roll (Absolute)
//     D14 = 0   no -  Roll (Relative)
//     D15 = 0   no -  Tilt (Absolute)
//     D16 = 0   no -  Tilt (Relative)
//     D17 = 0   no -  Focus Auto
//     D18 = 1  yes -  Reserved
//     D19 = 0   no -  Reserved
//     D20 = 0   no -  Reserved
//     D21 = 0   no -  Reserved
//     D22 = 0   no -  Reserved
//     D23 = 0   no -  Reserved
};

//          ===>Video Control Processing Unit Descriptor<===
U8 code VideoProcUnitDesc[]=
{
#ifdef _UVC_1V1_
	DESC_SIZE_PROC_UNIT_1V1,        //bLength:
#else
	DESC_SIZE_PROC_UNIT_1V0,        //bLength:
#endif
	CS_INTERFACE,        //bDescriptorType:
	VC_PROCESSING_UNIT,        //bDescriptorSubtype:
	ENT_ID_PROCESSING_UNIT,        //bUnitID:
	SRC_ID_PROCESSING_UNIT,        //bSourceID:
	0x00,        //wMaxMultiplier low byte
	0x00,	 // wMaxMultiplier high byte
	0x02,        //bControlSize:
	0x7F,	 // bmControls low byte
	0x17,	// bmcontrols high bytes
//bmControls : 0x7F 0x17
//     D00 = 1  yes -  Brightness
//     D01 = 1  yes -  Contrast
//     D02 = 1  yes -  Hue
//     D03 = 1  yes -  Saturation
//     D04 = 1  yes -  Sharpness
//     D05 = 1  yes -  Gamma
//     D06 = 1  yes -  White Balance Temperature
//     D07 = 0   no -  White Balance Component
//     D08 = 1  yes -  Backlight Compensation
//     D09 = 1  yes -  Gain
//     D10 = 1  yes -  Power Line Frequency
//     D11 = 0   no -  Hue, Auto
//     D12 = 1  yes -  White Balance Temperature, Auto
//     D13 = 0   no -  White Balance Component, Auto
//     D14 = 0   no -  Digital Multiplier
//     D15 = 0   no -  Digital Multiplier Limit
	I_PROCESSING_UNIT, //iProcessing:
	VIDEO_STANDARDS	// uvc 1.1 specific
};

//         ===>Video Control Output Terminal Descriptor<===
U8 code VideoOutTrmDesc[]=
{
	DESC_SIZE_OUT_TRM,     //bLength:
	CS_INTERFACE,     //bDescriptorType:
	VC_OUTPUT_TERMINAL,     //bDescriptorSubtype:
	ENT_ID_OUTPUT_TRM,     //bTerminalID:
	TT_STREAMING%256,
	TT_STREAMING/256,     //wTerminalType:                   0x0101 = (TT_STREAMING)
	0x00,     //bAssocTerminal:
	SRC_ID_OUTPUT_TRM,     //bSourceID:
	I_OUTPUT_TRM     //iTerminal:
};
//          ===>Video Control Extension Unit Descriptor<===
U8 code VideoExtDbgUnitDesc[]=
{
	DESC_SIZE_EXT_DBG_UNIT, //0x1b,   //bLength:
	CS_INTERFACE,   //bDescriptorType:
	VC_EXTENSION_UNIT,   //bDescriptorSubtype:
	ENT_ID_EXTENSION_UNIT_DBG,   //bUnitID:
	//{
	//GUID {1229A78C-47B4-4094-B0CE-DB07386FB938}
	0x8C,
	0xA7,
	0x29,
	0x12,

	0xB4,
	0x47,

	0x94,
	0x40,

	0xB0,
	0xCE,

	0xDB,
	0x07,
	0x38,
	0x6F,
	0xB9,
	0x38,
	//}
	0x02,    // control number
	0x01,    //bNrInPins:
//===>List of Connected Units and Terminal ID's===
	SRC_ID_EXTENSION_UNIT_DBG, //baSourceID[1]:
	0x02,//bControlSize:
	//bmControls : 0xF9 0x1F 0x18
	0x00, //bitmap
	0x06,
//     D00 = 1  yes - MS required.
//     D01 = 0   no -  Vendor-Specific (Optional)
//     D02 = 0   no -  Vendor-Specific (Optional)
//     D03 = 1  yes -  Vendor-Specific (Optional)
//     D04 = 1  yes -  Vendor-Specific (Optional)
//     D05 = 1  yes -  Vendor-Specific (Optional)
//     D06 = 1  yes -  Vendor-Specific (Optional)
//     D07 = 1  yes -  Vendor-Specific (Optional)
//     D08 = 1  yes -  Vendor-Specific (Optional)
//     D09 = 1  yes -  Vendor-Specific (Optional)
//     D10 = 1  yes -  Vendor-Specific (Optional)
//     D11 = 1  yes -  Vendor-Specific (Optional)
//     D12 = 1  yes -  Vendor-Specific (Optional)
//     D13 = 0   no -  Vendor-Specific (Optional)
//     D14 = 0   no -  Vendor-Specific (Optional)
//     D15 = 0   no -  Vendor-Specific (Optional)
	0x00 //iExtension:
};

#ifdef _FACIALAEWINSET_XU_
U8 code FacialAEExtUnitDesc[]=
{
    DESC_SIZE_FACIAL_AE_XU, //0x1b,   //bLength:                           
    CS_INTERFACE,   //bDescriptorType:                   
    VC_EXTENSION_UNIT,   //bDescriptorSubtype:                
    ENT_ID_EXTENSION_UNIT_FACEAE,   //bUnitID:  
    //{
       //GUID  {B126EA97-4436-41df-9DE4-2DD4647EC193}                                                                  
    0x97,
    0xea, 
    0x26, 
    0xb1,
  
    0x36, 
    0x44,

    0xdf,
    0x41,

    0x9d,
    0xe4,

    0x2d,
    0xd4, 
    0x64, 
    0x7e,
    0xc1,  
    0x93,    
    //}  
    0x01,    // bNumControls 
    0x01,    //bNrInPins:                         
//===>List of Connected Units and Terminal ID's===
    SRC_ID_EXTENSION_UNIT_FACEAE, //baSourceID[1]:                    
    0x01,//bControlSize:
    0x01, //bitmap
//     D00 = 1  yes - MS required.
//     D01 = 0   no -  Vendor-Specific (Optional)
//     D02 = 0   no -  Vendor-Specific (Optional)
//     D03 = 1  yes -  Vendor-Specific (Optional)
//     D04 = 1  yes -  Vendor-Specific (Optional)
//     D05 = 1  yes -  Vendor-Specific (Optional)
//     D06 = 1  yes -  Vendor-Specific (Optional)
//     D07 = 1  yes -  Vendor-Specific (Optional)
    0x00 //iExtension:
};
#endif
//          ===>Status Interrupt Endpoint Descriptor<===
U8 code VideoStsIntEpDesc[]=
{
	DESC_SIZE_EP,        //bLength:
	USB_ENDPOINT_DESCRIPTOR_TYPE,        //bDescriptorType:
	0x80|EP_IDX_VIDEO_INT,        //bEndpointAddress:                  -> Direction: IN - EndpointID: 1
	0x03,        //bmAttributes:                      -> Interrupt Transfer Type
	MAX_VCINTEP_PKT_SIZE%256,        //wMaxPacketSize:                   = 1 transactions per microframe, 0x0A max bytes
	MAX_VCINTEP_PKT_SIZE/256,
	0x06         //bInterval:
};
//          ===>Class-specific VC Interrupt Endpoint Descriptor<===
U8 code  VideoClsSpcStsEpDesc[]=
{
	DESC_SIZE_CLS_VCI_EP, //  bLength:
	CS_ENDPOINT, //  bDescriptorType:
	EP_INTERRUPT, //  bDescriptorSubType:
	MAX_VCINTEP_PKT_SIZE%256, //  wMaxTransferSize:                = (10) Bytes
	MAX_VCINTEP_PKT_SIZE/256  //
};
//          ===>Interface Descriptor<===
U8 code VideoStreamIfDesc[]=
{
	DESC_SIZE_INTF,     // bLength:
	USB_INTERFACE_DESCRIPTOR_TYPE,     // bDescriptorType:
	IF_IDX_VIDEOSTREAMING,     // bInterfaceNumber:
	0x00,     // bAlternateSetting:
#ifdef _BULK_ENDPOINT_
	0x01,     // bNumEndpoints:
#else
	0x00,     // bNumEndpoints:
#endif
	CC_VIDEO,     // bInterfaceClass:                     -> Video Interface Class
	SC_VIDEOSTREAMING,     // bInterfaceSubClass:                  -> Video Streaming Interface SubClass
	PC_PROTOCOL_UNDEFINED,     // bInterfaceProtocol:
	I_VIDEO_STREAMING      // iInterface:
};


//          ===>Video Streaming Uncompressed Format Type Descriptor<===
U8 code VideoStreamUncompFmtDesc[]=
{
	0x1B,    //bLength:
	0x24,    //bDescriptorType:
	0x04,    //bDescriptorSubtype:
	0x02,    //bFormatIndex:
	0x0c,    //bNumFrameDescriptors:
	//guidFormat: {32595559-0000-0010-8000-00AA00389B71} = YUY2 Format
	//{
	0x59,
	0x55,
	0x59,
	0x32,
	0x00,
	0x00,
	0x10,
	0x00,
	0x80,
	0x00,
	0x00,
	0xAA,
	0x00,
	0x38,
	0x9B,
	0x71,
	//}
	0x10,    //bBitsPerPixel:
	0x01,    //bDefaultFrameIndex:
	0x00,    //bAspectRatioX:
	0x00,    //bAspectRatioY:
	0x00,    //bmInterlaceFlags:
	//D0    = 0x00 Interlaced stream or variable: No
	//D1    = 0x00 Fields per frame: 2 fields
	//D2    = 0x00 Field 1 first: No
	//D3    = 0x00 Reserved
	//D4..5 = 0x00 Field patterns  -> Field 1 only
	//D6..7 = 0x00 Display Mode  -> Bob only
	0x00 //bCopyProtect:                        -> Duplication Unrestricted
};

U8 VideoDescBuf[26+4*11];
#ifndef _BULK_ENDPOINT_
//          ===>Interface Descriptor<===
U8 code VideoStreamAltIfDesc[]=
{
	DESC_SIZE_INTF,		//bLength:                           0x09
	USB_INTERFACE_DESCRIPTOR_TYPE,//bDescriptorType:                   0x04
	IF_IDX_VIDEOSTREAMING,	//bInterfaceNumber:                  0x01
	0X01,	//bAlternateSetting:                 0x01
	0X01,	//bNumEndpoints:                     0x01
	CC_VIDEO,//bInterfaceClass:                   0x0E  -> Video Interface Class
	SC_VIDEOSTREAMING,//bInterfaceSubClass:                0x02  -> Video Streaming Interface SubClass
	PC_PROTOCOL_UNDEFINED,//bInterfaceProtocol:                0x00
	I_VIDEO_STREAMING//iInterface:                        0x00
};
#endif
//          ===>Endpoint Descriptor<===
#ifdef _BULK_ENDPOINT_
U8 code VideoStreamEpDesc[]=
{
	DESC_SIZE_EP,//bLength:                           0x07
	USB_ENDPOINT_DESCRIPTOR_TYPE,//bDescriptorType:                   0x05
	0X80|EP_IDX_VIDEO_STREAM,	//bEndpointAddress:                  0x82  -> Direction: IN - EndpointID: 2
	0X02,	//bmAttributes:                      0x02  -> Bulk Transfer Type
	0X00,	//wMaxPacketSize:                  0x0200 = 1 transactions per microframe, 0x200 max bytes
	0X02,
	0X01	//bInterval:                         0x01    1 per (micro)frame.
};

#else
U8 code VideoStreamEpDesc[]=
{
	DESC_SIZE_EP,//bLength:                           0x07
	USB_ENDPOINT_DESCRIPTOR_TYPE,//bDescriptorType:                   0x05
	0X80|EP_IDX_VIDEO_STREAM,	//bEndpointAddress:                  0x82  -> Direction: IN - EndpointID: 2
	0X05,	//bmAttributes:                      0x05  -> Isochronous Transfer Type
	//                                              Synchronization Type = Asynchronous
	0X80,	//wMaxPacketSize:                  0x0080 = 1 transactions per microframe, 0x80 max bytes
	0X00,
	0X01	//bInterval:                         0x01    1 per (micro)frame.
};
#endif
//          ===>Video Streaming MJPEG Format Type Descriptor<===//
U8 code VideoStreamMjpegFmt_Desc[] =
{
	0x0B,               //bLength:
	0x24,               //bDescriptorType:
	0x06,               //bDescriptorSubtype:
	0x01,               //bFormatIndex:
	0x0c,               //bNumFrameDescriptors:
	0x01,               //bmFlags:                             -> Sample Size is Fixed
	0x01,               //bDefaultFrameIndex:
	0x00,               //bAspectRatioX:
	0x00,               //bAspectRatioY:
	0x00,               //bmInterlaceFlags:
	//     D00   = 0  non-Interlaced stream or variable
	//     D01   = 0  2 fields per frame
	//     D02   = 0  Field 1 not first
	//     D03   = 0  Reserved
	//     D4..5 = 0  Field patterns  -> Field 1 only
	//     D6..7 = 0  Display Mode  -> Bob only
	0x00               //bCopyProtect:                       -> Duplication Unrestricted
};


U8 code VideoStreamVendorFmt_Desc1V0[] =
{
	56,               //bLength:
	0x24,               //bDescriptorType:
	0x0E,               //bDescriptorSubtype:
	0x01,               //bFormatIndex:
	0x0c,               //bNumFrameDescriptors:
	//{
	'0',
	'2',
	'4',
	'Y',
	0x00,
	0x00,
	0x10,
	0x00,
	0x80,
	0x00,
	0x00,
	0xAA,
	0x00,
	0x38,
	0x9B,
	0x71,
	//}
	//{
	'2',
	'V',
	'U',
	'Y',
	0x00,
	0x00,
	0x10,
	0x00,
	0x80,
	0x00,
	0x00,
	0xAA,
	0x00,
	0x38,
	0x9B,
	0x71,
	//}
	//{
	'3',
	'V',
	'U',
	'Y',
	0x00,
	0x00,
	0x10,
	0x00,
	0x80,
	0x00,
	0x00,
	0xAA,
	0x00,
	0x38,
	0x9B,
	0x71,
	//}



	//bmInterlaceFlags:
	//     D00   = 0  non-Interlaced stream or variable
	//     D01   = 0  2 fields per frame
	//     D02   = 0  Field 1 not first
	//     D03   = 0  Reserved
	//     D4..5 = 0  Field patterns  -> Field 1 only
	//     D6..7 = 0  Display Mode  -> Bob only
	0x00, //bPayload Class  0:Frame based.
	0x01,
	0x00               //bCopyProtect:                       -> Duplication Unrestricted
};

U8 code VideoStreamVendorFmt_M420_Desc1V0[] =
{
	56,               //bLength:
	0x24,               //bDescriptorType:
	0x0E,               //bDescriptorSubtype:
	0x01,               //bFormatIndex:
	0x0c,               //bNumFrameDescriptors:
	//{
	'M',
	'4',
	'2',
	'0',
	0x00,
	0x00,
	0x10,
	0x00,
	0x80,
	0x00,
	0x00,
	0xAA,
	0x00,
	0x38,
	0x9B,
	0x71,
	//}
	//{
	'2',
	'V',
	'U',
	'Y',
	0x00,
	0x00,
	0x10,
	0x00,
	0x80,
	0x00,
	0x00,
	0xAA,
	0x00,
	0x38,
	0x9B,
	0x71,
	//}
	//{
	'3',
	'V',
	'U',
	'Y',
	0x00,
	0x00,
	0x10,
	0x00,
	0x80,
	0x00,
	0x00,
	0xAA,
	0x00,
	0x38,
	0x9B,
	0x71,
	//}



	//bmInterlaceFlags:
	//     D00   = 0  non-Interlaced stream or variable
	//     D01   = 0  2 fields per frame
	//     D02   = 0  Field 1 not first
	//     D03   = 0  Reserved
	//     D4..5 = 0  Field patterns  -> Field 1 only
	//     D6..7 = 0  Display Mode  -> Bob only
	0x00, //bPayload Class  0:Frame based.
	0x01,
	0x00               //bCopyProtect:                       -> Duplication Unrestricted
};

U8 code VideoStreamRAWFmt_Desc1V0[] =
{
	56,               //bLength:
	0x24,               //bDescriptorType:
	0x0E,               //bDescriptorSubtype:
	0x01,               //bFormatIndex:
	0x0c,               //bNumFrameDescriptors:
	//{
	'1',
	'W',
	'A',
	'R',
	0x00,
	0x00,
	0x10,
	0x00,
	0x80,
	0x00,
	0x00,
	0xAA,
	0x00,
	0x38,
	0x9B,
	0x71,
	//}
	//{
	'2',
	'W',
	'A',
	'R',
	0x00,
	0x00,
	0x10,
	0x00,
	0x80,
	0x00,
	0x00,
	0xAA,
	0x00,
	0x38,
	0x9B,
	0x71,
	//}
	//{
	'3',
	'W',
	'A',
	'R',
	0x00,
	0x00,
	0x10,
	0x00,
	0x80,
	0x00,
	0x00,
	0xAA,
	0x00,
	0x38,
	0x9B,
	0x71,
	//}



	//bmInterlaceFlags:
	//     D00   = 0  non-Interlaced stream or variable
	//     D01   = 0  2 fields per frame
	//     D02   = 0  Field 1 not first
	//     D03   = 0  Reserved
	//     D4..5 = 0  Field patterns  -> Field 1 only
	//     D6..7 = 0  Display Mode  -> Bob only
	0x00, //bPayload Class  0:Frame based.
	0x01,
	0x00               //bCopyProtect:                       -> Duplication Unrestricted
};

//          ===>Color Matching Descriptor<===
U8 code VideoColorMatchDesc[]=
{
	0x06,   //bLength:                           0x06
	0x24,   //bDescriptorType:                   0x24
	0x0D,   //bDescriptorSubtype:                0x0D
	0x01,   //bColorPrimaries:                   0x00
	0x01,   //bTransferCharacteristics:          0x00
	0x04    //bMatrixCoefficients:               0x00
};


#ifdef _UAC_EXIST_
U8 code AudioStaticDesc_1[] = 
{
	0x08, 0x0B, 0x02, 0x02, 0x01, 0x02, 0x00, 0x06, 0x09, 0x04, 0x02, 0x00, 0x00, 0x01, 0x01, 0x00, 
	0x06, 0x09, 0x24, 0x01, 0x00, 0x01, 0x27, 0x00, 0x01, 0x03, 0x0C, 0x24, 0x02, 0x01, 0x01, 0x02, 
	0x00, 0x01, 0x03, 0x00, 0x00, 0x00, 0x09, 0x24, 0x03, 0x02, 0x01, 0x01, 0x01, 0x03, 0x00, 0x09, 
	0x24, 0x06, 0x03, 0x01, 0x02, 0x03, 0x02, 0x00, 0x09, 0x04, 0x03, 0x00, 0x00, 0x01, 0x02, 0x00, 
	0x00, 0x09, 0x04, 0x03, 0x01, 0x01, 0x01, 0x02, 0x00, 0x00, 0x07, 0x24, 0x01, 0x02, 0x01, 0x01, 
	0x00, 0x0B, 0x24, 0x02, 0x01, 0x02, 0x02, 0x10, 0x01, 0x22, 0x56, 0x00, 0x09, 0x05, 0x82, 0x05, 
	0x78, 0x00, 0x04, 0x00, 0x00, 0x07, 0x25, 0x01, 0x01, 0x00, 0x00, 0x00, 0x09, 0x04, 0x03, 0x02, 
	0x01, 0x01, 0x02, 0x00, 0x00, 0x07, 0x24, 0x01, 0x02, 0x01, 0x01, 0x00, 0x0B, 0x24, 0x02, 0x01, 
	0x02, 0x02, 0x10, 0x01, 0x00, 0x7D, 0x00, 0x09, 0x05, 0x82, 0x05, 0xA0, 0x00, 0x04, 0x00, 0x00, 
	0x07, 0x25, 0x01, 0x01, 0x00, 0x00, 0x00, 0x09, 0x04, 0x03, 0x03, 0x01, 0x01, 0x02, 0x00, 0x00, 
	0x07, 0x24, 0x01, 0x02, 0x01, 0x01, 0x00, 0x0B, 0x24, 0x02, 0x01, 0x01, 0x02, 0x10, 0x01, 0x80, 
	0xBB, 0x00, 0x09, 0x05, 0x82, 0x05, 0xD8, 0x00, 0x04, 0x00, 0x00, 0x07, 0x25, 0x01, 0x01, 0x00, 
	0x00, 0x00, 0x09, 0x04, 0x03, 0x04, 0x01, 0x01, 0x02, 0x00, 0x00, 0x07, 0x24, 0x01, 0x02, 0x01, 
	0x01, 0x00, 0x0B, 0x24, 0x02, 0x01, 0x02, 0x02, 0x10, 0x01, 0x80, 0xBB, 0x00, 0x09, 0x05, 0x82, 
	0x05, 0xD8, 0x00, 0x04, 0x00, 0x00, 0x07, 0x25, 0x01, 0x01, 0x00, 0x00, 0x00
};

U8 code AudioStaticDesc_2[] = 	
{
	0x09, 0x04, 0x03, 0x05, 0x01, 0x01, 0x02, 0x00, 0x00, 0x07, 0x24, 0x01, 0x02, 0x01, 0x01, 0x00, 
	0x0B, 0x24, 0x02, 0x01, 0x02, 0x02, 0x10, 0x01, 0x00, 0x77, 0x01, 0x09, 0x05, 0x82, 0x05, 0xB0, 
	0x01, 0x04, 0x00, 0x00, 0x07, 0x25, 0x01, 0x01, 0x00, 0x00, 0x00, 0x09, 0x04, 0x03, 0x06, 0x01, 
	0x01, 0x02, 0x00, 0x00, 0x07, 0x24, 0x01, 0x02, 0x01, 0x01, 0x00, 0x0B, 0x24, 0x02, 0x01, 0x02, 
	0x03, 0x18, 0x01, 0x44, 0xAC, 0x00, 0x09, 0x05, 0x82, 0x05, 0x30, 0x01, 0x04, 0x00, 0x00, 0x07, 
	0x25, 0x01, 0x01, 0x00, 0x00, 0x00, 0x09, 0x04, 0x03, 0x07, 0x01, 0x01, 0x02, 0x00, 0x00, 0x07, 
	0x24, 0x01, 0x02, 0x01, 0x01, 0x00, 0x0B, 0x24, 0x02, 0x01, 0x02, 0x03, 0x18, 0x01, 0x80, 0xBB, 
	0x00, 0x09, 0x05, 0x82, 0x05, 0x50, 0x01, 0x04, 0x00, 0x00, 0x07, 0x25, 0x01, 0x01, 0x00, 0x00, 
	0x00, 0x09, 0x04, 0x03, 0x08, 0x01, 0x01, 0x02, 0x00, 0x00, 0x07, 0x24, 0x01, 0x02, 0x01, 0x01, 
	0x00, 0x0B, 0x24, 0x02, 0x01, 0x02, 0x03, 0x18, 0x01, 0x00, 0x77, 0x01, 0x09, 0x05, 0x82, 0x05, 
	0x90, 0x02, 0x04, 0x00, 0x00, 0x07, 0x25, 0x01, 0x01, 0x00, 0x00, 0x00
};
/*
// UAC Descriptor . Rabi 2009-06-15
//          ===>IAD Descriptor<===
U8 code AudioIADDesc[]=
{
	DESC_SIZE_IAD, //bLength
	USB_IAD_DESCRIPTOR_TYPE,//bDescriptorType
	IF_IDX_AUDIOCONTROL, //bFirstInterface
	0x02, //bInterfaceCount
	CC_AUDIO, //bFunctionClass;
	SC_AUDIOSTREAMING,//bFunctionSubClass
	PR_PROTOCAL_UNDEFINED, //bFunctionProtocol
	I_PRODUCT_MIC//iFunction
};

//          ===>Audio Control Interface Descriptor<===
U8 code AudioControlIfDesc[]=
{

	0x09,//bLength
	USB_INTERFACE_DESCRIPTOR_TYPE,//bDescriptorType
	IF_IDX_AUDIOCONTROL, //bInterfaceNumber
	0x00, //bAlternateSetting;
	0x00,//bNumEndpoints
	CC_AUDIO,//bInterfaceClass
	SC_AUDIOCONTROL,//bInterfaceSubClass
	PR_PROTOCAL_UNDEFINED,//bInterfaceProtocal
	I_PRODUCT_MIC // iInterface:must be the same as the iFunction
};

//          ===>Audio Control Interface Header Descriptor<===
U8 code AudioControlIfHeaderDesc[]=
{
	0x09, //bLength
	CS_INTERFACE, //bDescriptorType
	AC_HEADER, //bDescriptorSubType
	_UAC_VER_1V0%256, //bcdADC, UAC spec1.0
	_UAC_VER_1V0/256,
	0x27,
	0x00,// wTotalLength=0x0027, Header 0x9 + IT 0xc +OT 0x9 +FU length 0x9
	0x01,//bInCollection, Number of streaming interfaces.
	IF_IDX_AUDIOSTREAMING//baInterfaceNr[1], AS IF 3 belongs to this AC IF
};

//          ===>Audio Control Interface Input Terminal Descriptor<===
U8 code AudioControlITDesc[]=
{
	0x0C,//bLength
	CS_INTERFACE,//bDescriptor Type
	AC_INPUT_TERMINAL,//bDescriptorSub Type
	ID_TERMINAL_IT, //bTerminalID
	TT_IT_MICROPHONE%256,
	TT_IT_MICROPHONE/256, //wTermialType
	0x00, //bAssocTerminal
	0x01,// bNrChannels, 2 channel
	0x03,
	0x00, //wChannelsConfig, Left front/Right front
	0x00, //iChannelNames
	0x00	//iTerminal
};

//          ===>Audio Control Interface Input Terminal Descriptor<===
U8 code AudioControlITArrayDesc[]=
{
	0x0C,//bLength
	CS_INTERFACE,//bDescriptor Type
	AC_INPUT_TERMINAL,//bDescriptorSub Type
	ID_TERMINAL_IT, //bTerminalID
	TT_IT_MICROPHONE_ARRAY%256,
	TT_IT_MICROPHONE_ARRAY/256, //wTermialType
	0x00, //bAssocTerminal
	0x02,// bNrChannels, 2 channel
	0x03,
	0x00, //wChannelsConfig, Left front/Right front
	0x00, //iChannelNames
	0x00	//iTerminal
};

//          ===>Audio Control Interface Output Terminal Descriptor<===
U8 code AudioControlOTDesc[]=
{
	0x09, //bLength
	CS_INTERFACE, //bDescriptor Type
	AC_OUTPUT_TERMINAL, //bDescriptorSubtype
	ID_TERMINAL_OT, //bTerminalID
	TT_USB_STREAMING%256,
	TT_USB_STREAMING/256,//wTerminalType
	ID_TERMINAL_IT, //bAssocTermial, identifying the Input Terminal to which this Output Terminal is associated.
	ID_FU, //bSourceID
	0x00//iTerminal
};

//          ===>Audio Control Interface Feature Unit Descriptor<===
U8 code AudioControlFUDesc[]=
{
	0x09, //bLength
	CS_INTERFACE, //bDescriptorType
	AC_FEATURE_UNIT, //bDescriptorSubtype
	ID_FU, //bUnitID
	ID_TERMINAL_IT,//bSourceID
	0x02, //bControlSize, in bytes of an element of the bmaControls array
	0x3, //bmaControls[0], master channel 0
	//D0 : Mute
	//D1: Volume
	//D2: Bass
	//D3: Mid
	//D4: Treble
	//D5: Graphic Equalizer
	//D6: Automatic Gain
	//D7: Delay
	0x02,
	//D8: Bass Boost
	//D9: Loudness
	0x00
};

//          ===>Audio Streaming Interface Descriptor<===
U8 code AudioStreamingIfDesc_AltSet0[]=
{
	0x09, //bLength
	USB_INTERFACE_DESCRIPTOR_TYPE, //bDescriptorType
	IF_IDX_AUDIOSTREAMING, //bInterfaceNumber
	0x00, //bAlternateSetting
	0x00,//bNumEndpoints
	CC_AUDIO, //bInterfaceClass
	SC_AUDIOSTREAMING, //bInterfaceSubClass
	PR_PROTOCAL_UNDEFINED, //bInterfaceProtocol
	0x00//iInterface
};

//          ===>Audio Streaming Class Specific Audio data EP Descriptor<===
U8 code AudioStreamingCsEpDesc[]=
{
	0x07, //bLength
	CS_ENDPOINT,//bDescriptorType
	AS_GENERAL,//bDescriptorSubType
	0x01,//bmAttributes, Samping Freq Supported?
	0x00,//bLockDelayUnits
	0x00,
	0x00//wLockDelay
};
//          ===>Audio Streaming Class Specific Interface Descriptor<===
U8 code AudioStreamingCsIfDesc[]=
{
	0x07, //bLength
	CS_INTERFACE, //bDescriptorType
	AS_GENERAL,//bDescriptorSubtype
	ID_TERMINAL_OT,//bTerminalLink, Unit ID of the Output Terminal
	0x01,//bDelay, Inter Channel Synchroniztion, number of frames
	TYPE_I_PCM%256,
	TYPE_I_PCM/256//wForamtTag
};
*/
#endif // _UAC_EXIST_

#ifdef _UBIST_TEST_

// Sample UBIST descriptor.
U8 code UBistDesc[] =
{
	0x28,                   // bLength               - Total length of the descriptor.  Includes all test descriptors.
	0xFF,                   // bDescriptorType       - Vendor descriptor type 0xFF.
	0x42,        // dSignature            - "BIST" Signature.0x54534942
	0x49,        // dSignature            - "BIST" Signature.0x54534942
	0x53,        // dSignature            - "BIST" Signature.0x54534942
	0x54,        // dSignature            - "BIST" Signature.0x54534942
	0x00,                  // bcdVersion            - Revision of the specification in BCD format. 0x100
	0x01,                  // bcdVersion            - Revision of the specification in BCD format. 0x100
	6,                      // bNumOfTsts            - Total number of tests.
	I_UBIST_DEVICENAME,                      // bDeviceNameIndex      - Device name index.
	USB_VDREQUEST_BIST_CMD,                   // bBistCmdID            - UBIST command ID.
	0,0,0,0,0,              // reserved[5]           - Reserved.


	// 1st Test.
	UBIST_TEST1_ID,  // bTestID               - Test ID.
	I_UBIST_TESTNAME1,               // bTestNameIndex        - Test name index.
	0xF4,             // wTimeout              - Timeout with no activity. Set to 500ms.
	0x01,             // wTimeout              - Timeout with no activity. Set to 500ms.

	// 2nd Test.
	UBIST_TEST2_ID,  // bTestID               - Test ID.
	I_UBIST_TESTNAME2,               // bTestNameIndex        - Test name index.
	0xF4,             // wTimeout              - Timeout with no activity. Set to 500ms.
	0x01,             // wTimeout              - Timeout with no activity. Set to 500ms.

	// 3rd Test.
	UBIST_TEST3_ID,  // bTestID               - Test ID.
	I_UBIST_TESTNAME3,               // bTestNameIndex        - Test name index.
	0xF4,             // wTimeout              - Timeout with no activity. Set to 500ms.
	0x01,             // wTimeout              - Timeout with no activity. Set to 500ms.

	// 4th Test.
	UBIST_TEST4_ID,  // bTestID               - Test ID.
	I_UBIST_TESTNAME4,               // bTestNameIndex        - Test name index.
	0xF4,             // wTimeout              - Timeout with no activity. Set to 500ms.
	0x01,             // wTimeout              - Timeout with no activity. Set to 500ms.

	// 5th Test.
	UBIST_TEST5_ID,  // bTestID               - Test ID.
	I_UBIST_TESTNAME5,               // bTestNameIndex        - Test name index.
	0xF4,             // wTimeout              - Timeout with no activity. Set to 500ms.
	0x01,             // wTimeout              - Timeout with no activity. Set to 500ms.

	// 6th Test.
	UBIST_TEST6_ID,  // bTestID               - Test ID.
	I_UBIST_TESTNAME6,               // bTestNameIndex        - Test name index.
	0xE8,             // wTimeout              - Timeout with no activity. Set to 1000ms.
	0x03,             // wTimeout              - Timeout with no activity. Set to 1000ms.
};

U8	code iUbistTestName1[] =
{
	27,	 USB_STRING_DESCRIPTOR_TYPE,
	'S',
	'e',
	'n',
	's',
	'o',
	'r',
	'-',
	'C',
	'o',
	'm',
	'm',
	'u',
	'n',
	'i',
	'c',
	'a',
	't',
	'i',
	'o',
	'n',
	'-',
	'T',
	'e',
	's',
	't'
};

U8	code iUbistTestName2[] =
{
	// MID = 1111 "RTS5101-01"
	28,	 USB_STRING_DESCRIPTOR_TYPE,
	'S',
	'e',
	'r',
	'i',
	'a',
	'l',
	'-',
	'F',
	'l',
	'a',
	's',
	'h',
	'-',
	'C',
	'h',
	'e',
	'c',
	'k',
	'i',
	'n',
	'g',
	'-',
	'T',
	'e',
	's',
	't'
};

U8	code iUbistTestName3[] =
{
	// MID = 1111 "RTS5101-01"
	15,	 USB_STRING_DESCRIPTOR_TYPE,
	'R',
	'e',
	'g',
	'i',
	's',
	't',
	'e',
	'r',
	'-',
	'T',
	'e',
	's',
	't'
};


U8	code iUbistTestName4[] =
{
	// MID = 1111 "RTS5101-01"
	17,	 USB_STRING_DESCRIPTOR_TYPE,
	'C',
	'o',
	'n',
	't',
	'r',
	'o',
	'l',
	'l',
	'e',
	'r',
	'-',
	'T',
	'e',
	's',
	't'
};

U8	code iUbistTestName5[] =
{
	// MID = 1111 "RTS5101-01"
	19,	 USB_STRING_DESCRIPTOR_TYPE,
	'L',
	'u',
	'm',
	'i',
	'n',
	'a',
	'c',
	'e',
	'-',
	'0',
	'0',
	'0',
	'-',
	'T',
	'e',
	's',
	't'
};

U8	code iUbistTestName6[] =
{
	// MID = 1111 "RTS5101-01"
	16,	 USB_STRING_DESCRIPTOR_TYPE,
	'B',
	'l',
	'i',
	'n',
	'k',
	'-',
	'L',
	'E',
	'D',
	'-',
	'T',
	'e',
	's',
	't'
};

#endif // _UAC_EXIST_


#ifdef _USB2_LPM_ // JQG_add_2010_0415_
USB3_BOS_DEV_CAPABILITY_DESCRIPTOR code sBOSDeviceCapabilityDescriptor =
{
	//	BOS Descriptor
	{
		5, //DESC_SIZE_BOS,											// bLength
		0x0F, //USB_BOS_DESCRIPTOR_TYPE, 								// bDescriptorType
		12,0, // wTotalLength, (LSB,MSB)
		0x01											// bNumDeviceCaps
	},
	//  Device Capability Descriptor - USB2.0 Extension
	{
		7,//DESC_SIZE_USB2_EXTENSION,				// bLength
		0x10,//USB_DEVICE_CAPABILITY_DESCRIPTOR_TYPE, 	// bDescriptorType
		0x02,//USB2_EXTENSION_TYPE,					// bDevCapabilityType
		0x06, 0x00, 0x00, 0x00					// bmAttributes, SS device shall support LPM (LSB~MSB)
	},

	//  Device Capability Descriptor - SuperSpeed USB       
   /* {
        10,             // bLength
        0x10,//USB_DEVICE_CAPABILITY_DESCRIPTOR_TYPE,  // bDescriptorType
        0x03,                   // bDevCapabilityType
        0x00,                                   // bmAttributes, Do not support LTM
        0x0E, 0x00,                             // wSpeedsSupported, Support SS/HS/FS, (LSB,MSB)
        0x01,                                   // bFunctionalitySupport, Lowest Speed of function is FS
        0x0A,                                   // bU1DevExitLat, Less than 10us for U1->U0
        0xFF, 0x03                              // wU2DevExitLat, less than 1024us for U2->U0, (LSB,MSB)    
    }   
*/
		
};

#endif //_USB2_LPM_

#ifdef _DELL_EXU_
//          ===>Video Control Extension Unit Descriptor<===
U8 code VideoXU1ExtUnitDesc[]=
{
	DESC_SIZE_DELLXU1_UNIT,   //bLength:
	CS_INTERFACE,   //bDescriptorType:
	VC_EXTENSION_UNIT,   //bDescriptorSubtype:
	ENT_ID_EXTENSION_UNIT_XU1,   //bUnitID:
	//ENT_ID_EXTENSION_UNIT_DBG,   //bUnitID:
	//{

	//M10 GUID {0FB885C3-68C2-4547-90F7-8F47579D95FC}
	0xC3,
	0x85,
	0xB8,
	0x0F,

	0xC2,
	0x68,

	0x47,
	0x45,

	0x90,
	0xF7,

	0x8F,
	0x47,
	0x57,
	0x9D,
	0x95,
	0xFC,

	/*	    //M09 GUID {7780106A-0232-11DC-8314-0800200C9A66}
	    0x6A,
	    0x10,
	    0x80,
	    0x77,

	    0x32,
	    0x02,

	    0xDC,
	    0x11,

	    0x83,
	    0x14,

	    0x08,
	    0x00,
	    0x20,
	    0x0C,
	    0x9A,
	    0x66,
	*/
	//}
	0x00,
	0x01,    //bNrInPins:
	SRC_ID_EXTENSION_UNIT_XU1,//0x03,
	0x04,
	0x0F,
	0x00,
	0x00,
	0x00,
//     D00 = 1  yes - MS required.
//     D01 = 0   no -  Vendor-Specific (Optional)
//     D02 = 0   no -  Vendor-Specific (Optional)
//     D03 = 1  yes -  Vendor-Specific (Optional)
//     D04 = 1  yes -  Vendor-Specific (Optional)
//     D05 = 1  yes -  Vendor-Specific (Optional)
//     D06 = 1  yes -  Vendor-Specific (Optional)
//     D07 = 1  yes -  Vendor-Specific (Optional)
//     D08 = 1  yes -  Vendor-Specific (Optional)
//     D09 = 1  yes -  Vendor-Specific (Optional)
//     D10 = 1  yes -  Vendor-Specific (Optional)
//     D11 = 1  yes -  Vendor-Specific (Optional)
//     D12 = 1  yes -  Vendor-Specific (Optional)
//     D13 = 0   no -  Vendor-Specific (Optional)
//     D14 = 0   no -  Vendor-Specific (Optional)
//     D15 = 0   no -  Vendor-Specific (Optional)
	0x00 //iExtension:
};
#endif//_DELL_EXU_
#ifdef _HID_BTN_
//          ===>HID Interface Descriptor<===
U8 code cHIDDesc[] = 
{
	DESC_SIZE_INTF,						//bLength
	USB_INTERFACE_DESCRIPTOR_TYPE,		//bDescriptorType
	IF_IDX_HID,							//bInterfaceNumber
	0x00,								//bAlternateSetting
	0x01,								//bNumEndpoints
	0x03,                           				// bInterfaceClass, HID Class = 0X03
	0x00,                           				// bInterfaceSubClass,0=No Sublcass;1=Boot Interface subclass
	0x00,                           				// bInterfaceProtocol, 0=none,1=Keyboard,2=Mouse
	0x00,                        					// ilInterface (Idx of this intf str desc.)	
};

//          ===>HID Class Descriptor<===
U8 code cHIDClsDesc[] = 
{
	DESC_SIZE_HID,						// bLength
	USB_HID_CLASS_DESCRIPTOR,			// bDescriptorType, HID Class
	0x11, 0x01,							// HID Version V1.11				
	0x00,								// bCountryCode
	0x01,								// bNumDescriptors = 1 Report Descriptor
	USB_HID_REPORT_DESCRIPTOR,						// bDescriptorType = Report Descriptor
	DESC_SIZE_REPORT, 0x00,				// wDescriptorLength = bytes for Report Descriptor	
};

//          ===>Class-specific VD Interrupt Endpoint Descriptor<===
U8 code  cHIDClsSpcEpDesc[]=
{
	//-----------------		// Endpoint D
    0x07,                  	// bLength
    USB_ENDPOINT_DESCRIPTOR_TYPE,   	// bDescriptorType
    0x84,                           	// bEndpointAddress (EP 3, IN)
    0x03,                           	// bmAttributes  (0011 = Interrupt)
    0x01, 0x00,				// wMaxPacketSize = 1 (LSB, MSB)
    0x10                           		// bInterval = 8ms
};

//          ===>Class-specific HID Report Descriptor<===
U8 code cHIDClsReportDesc[] = 
{
/*	//cardreader
	// Report Descriptor, Report the data format is 1 byte " X X X X X X X X BTN"
	0x06,0x1B,0xFF, 	// Usage Page, 0xFF1B defined by HP
	0x09, 0x1B,		// Usage, BTN usage = 0x1B, defined by HP
	0xA1, 0x01,		// Collection(Application)	
	0x09, 0x1B,		// Usage ID: 0x1B , defined by HP
	0x19, 0x01,		// Usage Min(1)
	0x29, 0x01,		// Usage Max(1)
	0x15, 0x00,		// Logical Min(0)
	0x25, 0x01,		// Logical Max(1)
	0x75, 0x01,		// Report Size(1)
	0x95, 0x01,		// Report Count(1)
	0x81, 0x02,		// Input(Data, Varible, Absolute)
	0x75, 0x07,		// Report Size(7)
	0x95, 0x01,		// Report Count(1)
	0x81, 0x01,		// Input(Constant)	
	0xC0			// End Collection
*/
/*	//pccam
 	0x05, 0x01,                    // USAGE_PAGE (Generic Desktop)	
	0x15, 0x00,                    // LOGICAL_MINIMUM (0) 	
	0x09, 0x04,                    // USAGE (Joystick)	
	0xa1, 0x01,                    // COLLECTION (Application)
	0x05, 0x09,                    //   USAGE_PAGE (Button)
	0x19, 0x01,                    //   USAGE_MINIMUM (Button 1)
	0x29, 0x08,                    //   USAGE_MAXIMUM (Button 8)
	0x15, 0x00,                    //   LOGICAL_MINIMUM (0)
	0x25, 0x01,                    //   LOGICAL_MAXIMUM (1)
	0x75, 0x01,                    //   REPORT_SIZE (1)
	0x95, 0x08,                    //   REPORT_COUNT (8)	
	0x55, 0x00,                    //   UNIT_EXPONENT (0)
	0x65, 0x00,                    //   UNIT (None)
	0x81, 0x02,                    //   INPUT (Data,Var,Abs)
	0xc0                             // END_COLLECTION
*/
	0x06,0xDA,0xFF, 	// Usage Page, 0xFF1B defined by HP
	0x09, 0xDA,		// Usage, BTN usage = 0x1B, defined by HP
	0xA1, 0x01,		// Collection(Application)	
	0x09, 0xDA,		// Usage ID: 0x1B , defined by HP
	0x19, 0x01,		// Usage Min(1)
	0x29, 0x08,		// Usage Max(3)
	0x15, 0x00,		// Logical Min(0)
	0x25, 0x01,		// Logical Max(1)
	0x75, 0x01,		// Report Size(1)
	0x95, 0x08,		// Report Count(1)
	0x81, 0x02,		// Input(Data, Varible, Absolute)
	0xC0			// End Collection
};
#endif//_HID_BTN_

#ifndef _BULK_ENDPOINT_
U16 GetMaxPacketSize(U8 IfIdx,U8 isHighSpeed)
{
	// hemonel 2010-05-25 code optimize: not support bulk endpoint
//	if(g_byIsBulkEP)
//	{
//		if(isHighSpeed)
//			//return MAX_BULK_SIZE_HS;
//			return g_waBulkMaxPacketSizeHS[IfIdx];
//		else
//			return g_waBulkMaxPacketSizeFS[IfIdx];
//			//return MAX_BULK_SIZE_FS;
//	}
//	else
	{
		if(isHighSpeed)
		{
			// hemonel 2010-05-25 code optimize: not support 512B configure
			//	if(g_byEnable512BWconfig)
			//	{
			//		return g_waMaxPacketSizeHS_512BW[IfIdx];
			//	}
			//	else
			{
				return g_waMaxPacketSizeHS_Def[IfIdx];
			}
		}
		else
		{
			return g_waMaxPacketSizeFS[IfIdx];
		}
	}



}
#endif
//search map index
//example:
//    wBitMap = 0x0011
//    Idx = 1
//    return 0
static U8 SearchMapIdxU16(U8 Idx,U16 wBitMap)
{
	U8 i;
	U8 j=0;

	for(i=0; i<16; i++)
	{
		if((((U16)0x0001)<<i)&wBitMap)//
		{
			j++;
			if(j==Idx)
			{
				return i;
			}
		}
	}

	return 0xff; //invalid map, never come here;
}

static U8 GetNoneZeroBitNumU16(U16 wBitMap)
{
//	U8 i;
	U8 j=0;

//	for(i=0; i<16; i++)
//	{
//		if(((U16)0x0001<<i)&wBitMap)
//		{
//			j++;
//		}
//	}
	for (j = 0; wBitMap; ++j)
	{
		//clear the LSB "1"  when wBitMap is none-zero; he bit "1" number can be get when wBitMap is zero t
		wBitMap &= (wBitMap -1);
	}

	return j;
}

U8 GetFormatFrameNum(U8 IsVideo,U8 FmtIdx,U8 IsHighSpeed)
{
	U8 byTmp;

	// hemonel 2010-08-26: avoid array overbound
	if((FmtIdx == 0)||(FmtIdx > MAX_FORMAT_TYPE))
	{
		return 0;
	}

	if(IsVideo)
	{
		if(IsHighSpeed)
		{
			//	byTmp=g_paHSFormatTable[FmtIdx]->byaVideoFrameTbl[0];
			byTmp=g_aVideoFormat[FmtIdx-1].byaVideoFrameTbl[0];
		}
		else
		{
			//	byTmp=g_paFSFormatTable[FmtIdx]->byaVideoFrameTbl[0];
			byTmp=g_VideoFormatFSYUY2.byaVideoFrameTbl[0];
		}
	}
	else
	{
		if(IsHighSpeed)
		{
			//	byTmp=g_paHSFormatTable[FmtIdx]->byaStillFrameTbl[0];
			byTmp=g_aVideoFormat[FmtIdx-1].byaStillFrameTbl[0];
		}
		else
		{
			//	byTmp=g_paFSFormatTable[FmtIdx]->byaStillFrameTbl[0];
			byTmp=g_VideoFormatFSYUY2.byaStillFrameTbl[0];
		}
	}

	return byTmp;
}

U8 GetFormatNum(U8 IsHighSpeed)
{
	U8 i;
//	U8 bystate;
	U8 byCnt;
	if(IsHighSpeed)
	{
		/*
		for(i=1;i<MAX_FORMAT_TYPE+1;i++)
		{
			if(g_paHSFormatTable[i]!=NULL)
			{
				byCnt++;
				bystate=1;  //not-NULL
			}
			else
			{
				if(bystate==1)  //from not-NULL to NULL break
				{
					break;
				}
			}
		}
		*/
		byCnt = 0;
		for(i=0; i<MAX_FORMAT_TYPE; i++)
		{
			if(g_aVideoFormat[i].byFormatType!=FORMAT_TYPE_NC)
			{
				byCnt++;
			}
		}
		return byCnt;
	}
	else
	{
		/*
		for(i=1;i<MAX_FORMAT_TYPE+1;i++)
		{
			if(g_paFSFormatTable[i]!=NULL)
			{
				byCnt++;
				bystate=1; //not-NULL
			}
			else
			{
				if(bystate==1)  //from not-NULL to NULL break
				{
					break;
				}
			}
		}
		*/
		return 1;
	}
}

VideoFormat_T* GetFormatPointer(U8 FmtIdx,U8 IsHighSpeed)
{
	// hemonel 2010-08-26: avoid array overbound
	if((FmtIdx == 0)||(FmtIdx > MAX_FORMAT_TYPE))
	{
		return NULL;
	}

	if(IsHighSpeed)
	{
		//	return g_paHSFormatTable[FmtIdx];
		return &g_aVideoFormat[FmtIdx-1];
	}
	else
	{
		//	return g_paFSFormatTable[FmtIdx];
		return &g_VideoFormatFSYUY2;
	}
}

U8 GetFomatType(U8 FmtIdx,U8 IsHighSpeed)
{
	VideoFormat_T* pVideoFormat = NULL;
	pVideoFormat =  GetFormatPointer( FmtIdx, IsHighSpeed);
	if(pVideoFormat == NULL)
	{
		return 0xff;
	}
	else
	{
		return pVideoFormat->byFormatType;
	}
}

static U8 GetResolutionIdx(U8 IsVideo,U8 FmtIdx,U8 FrmIdx,U8 IsHighSpeed)
{
	// hemonel 2010-08-26: avoid array overbound
	if((FmtIdx == 0)||(FmtIdx > MAX_FORMAT_TYPE) ||(FrmIdx > MAX_VIDEO_FRAME_NUM))
	{
		return 0;
	}

	if(IsVideo)
	{
		if(IsHighSpeed)
		{
			//	return g_paHSFormatTable[FmtIdx]->byaVideoFrameTbl[FrmIdx];
			return g_aVideoFormat[FmtIdx-1].byaVideoFrameTbl[FrmIdx];
		}
		else
		{
			//	return g_paFSFormatTable[FmtIdx]->byaVideoFrameTbl[FrmIdx];
			return g_VideoFormatFSYUY2.byaVideoFrameTbl[FrmIdx];
		}
	}
	else
	{
		if(IsHighSpeed)
		{
			//	return g_paHSFormatTable[FmtIdx]->byaStillFrameTbl[FrmIdx];
			return g_aVideoFormat[FmtIdx-1].byaStillFrameTbl[FrmIdx];
		}
		else
		{
			//	return g_paFSFormatTable[FmtIdx]->byaStillFrameTbl[FrmIdx];
			return g_VideoFormatFSYUY2.byaStillFrameTbl[FrmIdx];
		}
	}
}

U16 GetFrameHeight(U8 IsVideo,U8 FmtIdx, U8 FrmIdx,U8 IsHighSpeed)
{
	return g_cwaResolutionTable[GetResolutionIdx(IsVideo,FmtIdx,FrmIdx,IsHighSpeed)].wHeight;
}


U16 GetFrameWidth(U8 IsVideo,U8 FmtIdx, U8 FrmIdx,U8 IsHighSpeed)
{
	return g_cwaResolutionTable[GetResolutionIdx(IsVideo,FmtIdx,FrmIdx,IsHighSpeed)].wWidth;
}


U32 GetMaxFrameSize(U8 IsVideo,U8 FmtIdx, U8 FrmIdx,U8 IsHighSpeed)
{
	/*
	U32 dwTmp;
	dwTmp=GetFrameHeight( IsVideo, FmtIdx,  FrmIdx, IsHighSpeed);
	dwTmp *=GetFrameWidth( IsVideo, FmtIdx,  FrmIdx, IsHighSpeed);
	dwTmp <<=1;
	return dwTmp;
	*/
	// hemonel 2010-06-02 optimize code
	return (U32)(GetFrameHeight( IsVideo, FmtIdx,  FrmIdx, IsHighSpeed)<<1)*(U32)GetFrameWidth( IsVideo, FmtIdx,  FrmIdx, IsHighSpeed);
}

U8 VideoDescBufFillINT(U8 Idx,U16 wData)
{
	VideoDescBuf[Idx++] = INT2CHAR(wData, 0);
	VideoDescBuf[Idx++] = INT2CHAR(wData, 1);
	return Idx;
}

void StillImgDescInit(U8 type_Idx)
{
	U8  byFmtIdx;
	U8 data byOffet;
	U8  i;
	U8 byResolutionNum;
	U16 data wTmp;

	byFmtIdx = (type_Idx&0x0f);
	byResolutionNum = GetFormatFrameNum(0,byFmtIdx,g_byIsHighSpeedDesc);

	VideoDescBuf[0] = (byResolutionNum<<2)+5+1;
	VideoDescBuf[1]=0x24;
	VideoDescBuf[2]=0x03;
	VideoDescBuf[3]=0x00;
	VideoDescBuf[4]=byResolutionNum;

	byOffet = 5;
	for(i=1; i<=byResolutionNum; i++)
	{
		wTmp = GetFrameWidth(0, byFmtIdx,  i,g_byIsHighSpeedDesc);
		//	VideoDescBuf[1+(i<<2)]= INT2CHAR(wTmp,0);
		//	VideoDescBuf[2+(i<<2)]= INT2CHAR(wTmp,1);
//		VideoDescBuf[byOffet++]= INT2CHAR(wTmp,0);
//		VideoDescBuf[byOffet++]= INT2CHAR(wTmp,1);
		byOffet = VideoDescBufFillINT(byOffet, wTmp);

		wTmp = GetFrameHeight(0, byFmtIdx,  i,g_byIsHighSpeedDesc);
		//	VideoDescBuf[3+(i<<2)]= INT2CHAR(wTmp,0);
		//	VideoDescBuf[4+(i<<2)]= INT2CHAR(wTmp,1);
//		VideoDescBuf[byOffet++]= INT2CHAR(wTmp,0);
//		VideoDescBuf[byOffet++]= INT2CHAR(wTmp,1);
		byOffet = VideoDescBufFillINT(byOffet, wTmp);
	}

	VideoDescBuf[byOffet]=0;

	return;
}

U16 GetFpsBitMap(U8 IsVideo,U8 FmtIdx, U8 FrmIdx,U8 IsHighSpeed)
{
	if((FmtIdx == 0)||(FmtIdx > MAX_FORMAT_TYPE))
	{
		return FPS_5;	// return default bitmap support 5fps
	}

	if(IsHighSpeed)
	{
//		return g_paHSFormatTable[FmtIdx]->waFrameFpsBitmap[GetResolutionIdx(IsVideo,FmtIdx,FrmIdx,IsHighSpeed)];
		return g_aVideoFormat[FmtIdx-1].waFrameFpsBitmap[GetResolutionIdx(IsVideo,FmtIdx,FrmIdx,IsHighSpeed)];
	}
	else
	{
		//	return g_paFSFormatTable[FmtIdx]->waFrameFpsBitmap[GetResolutionIdx(IsVideo,FmtIdx,FrmIdx,IsHighSpeed)];
		return g_VideoFormatFSYUY2.waFrameFpsBitmap[GetResolutionIdx(IsVideo,FmtIdx,FrmIdx,IsHighSpeed)];
	}
}

U8 GetFPSNum(U8 IsVideo,U8 FmtIdx,U8 FrmIdx,U8 IsHighSpeed)
{
	U16 wTmp;

	wTmp = GetFpsBitMap(IsVideo,FmtIdx, FrmIdx,IsHighSpeed);
	return GetNoneZeroBitNumU16(wTmp);
}

U8 GetFPS(U8 IsVideo,U8 FmtIdx,U8 FrmIdx,U8 IsHighSpeed,U8 FpsIdx)
{
	U8 byFpsIdx;
	U16 wFpsBitMap;

	wFpsBitMap = GetFpsBitMap(IsVideo,FmtIdx, FrmIdx,IsHighSpeed);
	byFpsIdx = SearchMapIdxU16(FpsIdx,wFpsBitMap);

	//for fix AMcap video capture bug.
	if(byFpsIdx==0xff)
	{
		return SENSOR_FPS_5;	// hemonel 2009-06-25: when fps is not in table, select 5fps
	}
	else
	{
		return g_byaFPSTable[byFpsIdx];
	}
}

U8 GetMinFPS(U8 IsVideo,U8 FMT,U8 FRM,U8 IsHighSpeed)
{
	return GetFPS( IsVideo,FMT,FRM,IsHighSpeed, (GetFPSNum(IsVideo, FMT,FRM,IsHighSpeed)));
}

U8 GetMaxFPS(U8 IsVideo,U8 FMT,U8 FRM,U8 IsHighSpeed)
{
	return GetFPS(IsVideo,FMT,FRM,IsHighSpeed,1);
}

U8 GetDefaultFPS(U8 IsVideo, U8 FMT,U8 FRM,U8 IsHighSpeed)
{
	return GetFPS(IsVideo,FMT,FRM,IsHighSpeed,1);
}

U32 GetFrameInterval(U8 IsVideo,U8 FMT,U8 FRM,U8 IsHighSpeed,U8 FpsIdx)
{
	U8 const byTmp=GetFPS(IsVideo,FMT,FRM,IsHighSpeed,FpsIdx) ;
	//DBG(("fps=%bx\n", dwTmp));
	return ((U32)10000000)/byTmp;
}

U32 GetDefaultFrameInterval(U8 IsVideo,U8 FMT,U8 FRM,U8 IsHighSpeed)
{
	U8 const byTmp=GetDefaultFPS(IsVideo,FMT,FRM,IsHighSpeed) ;
	return ((U32)10000000)/byTmp;
}

U32 GetMaxFrameInterval(U8 IsVideo,U8 FMT,U8 FRM,U8 IsHighSpeed)
{
	U8 const byTmp=GetMinFPS(IsVideo,FMT,FRM,IsHighSpeed) ;
	return ((U32)10000000)/byTmp;
}

U32 GetMinFrameInterval(U8 IsVideo,U8 FMT,U8 FRM,U8 IsHighSpeed)
{
	U8 const byTmp=GetMaxFPS(IsVideo,FMT,FRM,IsHighSpeed) ;
	return ((U32)10000000)/byTmp;
}

U32 GetMinBitRate(U8 IsVideo,U8 FMT, U8 Idx,U8 IsHighSpeed)
{
//	U32 dwTmp;
//	dwTmp=GetMaxFrameSize(IsVideo,FMT, Idx,IsHighSpeed) ;
//	dwTmp *=GetMinFPS(IsVideo,FMT, Idx,IsHighSpeed);
//	dwTmp <<=3;
//	return dwTmp;
	// hemonel 2010-06-02 optimize code
	return GetMaxFrameSize(IsVideo,FMT, Idx,IsHighSpeed)*(U32)GetMinFPS(IsVideo,FMT, Idx,IsHighSpeed)*(U32)8;
}

U32 GetMaxBitRate(U8 IsVideo,U8 FMT, U8 Idx,U8 IsHighSpeed)
{
//	U32 dwTmp;
//	dwTmp=GetMaxFrameSize(IsVideo,FMT, Idx,IsHighSpeed) ;
//	dwTmp *=GetMaxFPS(IsVideo,FMT, Idx,IsHighSpeed);
//	dwTmp <<=3;
//	return dwTmp;
	// hemonel 2010-06-02 optimize code
	return GetMaxFrameSize(IsVideo,FMT, Idx,IsHighSpeed)*(U32)GetMaxFPS(IsVideo,FMT, Idx,IsHighSpeed)*(U32)8;
}


U8 VideoDescBufFillLONG(U8 Idx,U32 dwData)
{
//	VideoDescBuf[Idx] = LONG2CHAR(dwData, 0);
//	VideoDescBuf[Idx+1] = LONG2CHAR(dwData, 1);
//	VideoDescBuf[Idx+2] = LONG2CHAR(dwData, 2);
//	VideoDescBuf[Idx+3] = LONG2CHAR(dwData, 3);
//	return (Idx+4);
	VideoDescBuf[Idx++] = LONG2CHAR(dwData, 0);
	VideoDescBuf[Idx++] = LONG2CHAR(dwData, 1);
	VideoDescBuf[Idx++] = LONG2CHAR(dwData, 2);
	VideoDescBuf[Idx++] = LONG2CHAR(dwData, 3);
	return Idx;
}



void VideoFrameDescInit(U8 byTypeIdx)
{
//	U8 Type;
	U8 byFrameIdx;
	U8 idata i,j;
	U8 byFmtIdx;
	U8 byFomatType;
	U8 data byTmp;
	U16 wTmp;
//	U32 dwTmp ;


//	Type = (byTypeIdx&0xf0);
	byFrameIdx    = (byTypeIdx&0x0f);

	switch(byTypeIdx&0xf0) // descriptor type
	{
	case D_TYPE_FRM1:
		byFmtIdx= 1;
		break;
	case D_TYPE_FRM2:
		byFmtIdx= 2;
		break;
	case D_TYPE_FRM3:
		byFmtIdx= 3;
		break;
	case D_TYPE_FRM4:
		byFmtIdx= 4;
		break;// hemonel 2010-08-26: max support 3 format
	default:
		byFmtIdx= 1;
		break;	// hemonel 2010-08-26: max support 3 format
	}
	byFomatType=GetFomatType(byFmtIdx, g_byIsHighSpeedDesc);
	switch(byFomatType)
	{
	case FORMAT_TYPE_YUY2:
		byTmp= VS_FRAME_UNCOMPRESSED;
		break;
	case FORMAT_TYPE_MJPG:
		byTmp= VS_FRAME_MJPEG;
		break;
	case FORMAT_TYPE_YUV420:
	case FORMAT_TYPE_M420:
	case FORMAT_TYPE_RSDH:
	case FORMAT_TYPE_RAW:
	default:
		byTmp= VS_FRAME_VENDOR;
		break;
	}
	VideoDescBuf[2]=byTmp;


	byTmp = GetFPSNum(UVC_IS_VIEDO,byFmtIdx,  byFrameIdx, g_byIsHighSpeedDesc);

	VideoDescBuf[0] = (byTmp<<2)+26;
	VideoDescBuf[1]=CS_INTERFACE;

	//VideoDescBuf[2] refer to upper statements

	VideoDescBuf[3]=byFrameIdx;
#ifdef _STILL_IMAGE_METHOD_1
	VideoDescBuf[4]=1; //bmCapabilities,if still image method 1
#else
	VideoDescBuf[4]=0; //bmCapabilities
#endif
	wTmp = GetFrameWidth(UVC_IS_VIEDO,byFmtIdx,  byFrameIdx, g_byIsHighSpeedDesc);
//	VideoDescBuf[5]	= INT2CHAR(wTmp, 0);
//	VideoDescBuf[6]	= INT2CHAR(wTmp, 1);
	VideoDescBufFillINT(5, wTmp);

	wTmp = GetFrameHeight(UVC_IS_VIEDO,byFmtIdx,  byFrameIdx, g_byIsHighSpeedDesc);
//	VideoDescBuf[7]	= INT2CHAR(wTmp, 0);
//	VideoDescBuf[8]	= INT2CHAR(wTmp, 1);
	VideoDescBufFillINT(7, wTmp);

//	dwTmp = GetMinBitRate(UVC_IS_VIEDO,byFmtIdx,byFrameIdx,g_byIsHighSpeedDesc);
	VideoDescBufFillLONG(9,GetMinBitRate(UVC_IS_VIEDO,byFmtIdx,byFrameIdx,g_byIsHighSpeedDesc));

//	dwTmp=GetMaxBitRate(UVC_IS_VIEDO,byFmtIdx, byFrameIdx, g_byIsHighSpeedDesc);
	VideoDescBufFillLONG(13,GetMaxBitRate(UVC_IS_VIEDO,byFmtIdx, byFrameIdx, g_byIsHighSpeedDesc));

//	dwTmp = GetMaxFrameSize(UVC_IS_VIEDO,byFmtIdx, byFrameIdx, g_byIsHighSpeedDesc);
	VideoDescBufFillLONG(17,GetMaxFrameSize(UVC_IS_VIEDO,byFmtIdx, byFrameIdx, g_byIsHighSpeedDesc));

//	dwTmp = GetDefaultFrameInterval(UVC_IS_VIEDO,byFmtIdx, byFrameIdx, g_byIsHighSpeedDesc);
	VideoDescBufFillLONG(21,GetDefaultFrameInterval(UVC_IS_VIEDO,byFmtIdx, byFrameIdx, g_byIsHighSpeedDesc));

	VideoDescBuf[25] = byTmp;
	j=26;
	for(i=1; i<=byTmp; i++)
	{
		//	dwTmp = GetFrameInterval(UVC_IS_VIEDO,byFmtIdx, byFrameIdx, g_byIsHighSpeedDesc, i);
		j = VideoDescBufFillLONG(j,GetFrameInterval(UVC_IS_VIEDO,byFmtIdx, byFrameIdx, g_byIsHighSpeedDesc, i));
	}

	return;
}

void VideoStreamingInHdrDescInit()
{
	U8 const byFormatNum = GetFormatNum(g_byIsHighSpeedDesc);

	VideoDescBuf[0] = 13 +byFormatNum;
	VideoDescBuf[1] = 0x24; //bDescriptorType
	VideoDescBuf[2] = 0x01;//bDescriptorSubtype
	VideoDescBuf[3] = byFormatNum;
//	VideoDescBuf[4] = INT2CHAR(g_wVSCSdescSize, 0); //g_wVSCSdescSize%256; //total length
//	VideoDescBuf[5] = INT2CHAR(g_wVSCSdescSize, 1); //g_wVSCSdescSize/256;
	VideoDescBufFillINT(4, g_wVSCSdescSize);
	VideoDescBuf[6]	=  0x80|EP_IDX_VIDEO_STREAM;  //bEndpointAddress:             -> Direction: IN - EndpointID: 2
	VideoDescBuf[7]	= 0x00,  //bmInfo:                       -> Dynamic Format Change not Supported
	                  VideoDescBuf[8]	=  ENT_ID_OUTPUT_TRM,  //bTerminalLink:
#ifdef _STILL_IMAGE_METHOD_1
	VideoDescBuf[9]	=  0x01,  //bStillCaptureMethod:          -> Still Capture Method 1
#else
	VideoDescBuf[9]	=  0x02,  //bStillCaptureMethod:          -> Still Capture Method 2
#endif
	VideoDescBuf[10] = 1;	//g_byHwButtonTriggerSp
	VideoDescBuf[11] = 0;	//g_byHwButtonTriggerUsage
	VideoDescBuf[12] =  0x01;//,  //bControlSize:
	VideoDescBuf[13] =  0x00;//Video Payload Format 1
	//D00 = 0   no -  Key Frame Rate
	//D01 = 0   no -  P Frame Rate
	//D02 = 0   no -  Compression Quality
	//D03 = 0   no -  Compression Window Size
	//D04 = 0   no -  Generate Key Frame
	//D05 = 0   no -  Update Frame Segment
	//D06 = 0   no -  Reserved
	//D07 = 0   no -  Reserved
	VideoDescBuf[14] =0x00;//Video Payload Format 2
	VideoDescBuf[15] =0x00;//Video Payload Format 3
	VideoDescBuf[16] =0x00;//Video Payload Format 4
}

/*
#ifdef _UAC_EXIST_
void UACAltSettingDescInit(U8 type_Idx)
{
	U8  byAltIdx;

	byAltIdx = (type_Idx&0x0f) + 1;

	VideoDescBuf[0] = 0x09;
	VideoDescBuf[1]=USB_INTERFACE_DESCRIPTOR_TYPE;
	VideoDescBuf[2]=IF_IDX_AUDIOSTREAMING;
	VideoDescBuf[3]=byAltIdx;
	VideoDescBuf[4]=0x01;
	VideoDescBuf[5]=CC_AUDIO;
	VideoDescBuf[6]=SC_AUDIOSTREAMING;
	VideoDescBuf[7]=PR_PROTOCAL_UNDEFINED;
	VideoDescBuf[8]=0x00;
	return;
}

void UACFormatDescInit(U8 type_Idx)
{
	U8  byFmtIdx;

	byFmtIdx = (type_Idx&0x0f) ;

	VideoDescBuf[0] = 0x0B;
	VideoDescBuf[1]=CS_INTERFACE;
	VideoDescBuf[2]=AS_FORMAT_TYPE;
	VideoDescBuf[3]=AD_FORMAT_TYPE_I;
	if(byFmtIdx==2)
	{
		VideoDescBuf[4]=0x01; //alerante setting 3 is mono channel 
	}
	else
	{
		VideoDescBuf[4]=0x02;// other setting is two channel 
	}
	VideoDescBuf[5]=g_byUACBitResolutionList[byFmtIdx];
	VideoDescBuf[6]=g_byUACBitResolutionList[byFmtIdx] * 8;
	VideoDescBuf[7]=0x01;
	VideoDescBuf[8]=g_dwaUACSampleRateList[byFmtIdx]%256;
	VideoDescBuf[9]=(g_dwaUACSampleRateList[byFmtIdx]/256)%256;
	VideoDescBuf[10]=(g_dwaUACSampleRateList[byFmtIdx]/256)/256;	
	return;
}

void UACStreamingEPDescInit(U8 type_Idx)
{
	U8  byFmtIdx;
	byFmtIdx = (type_Idx&0x0f) ;

	VideoDescBuf[0] = 0x09;
	VideoDescBuf[1]=USB_ENDPOINT_DESCRIPTOR_TYPE;
	VideoDescBuf[2]=0x80|EP_IDX_AUDIO_STREAM;
	VideoDescBuf[3]=0x05;
//	if (	g_byIsHighSpeedDesc == 1)
	{
//		VideoDescBuf[4]= g_waUACMaxPktSize[byFmtIdx] & 0xFF;
//		VideoDescBuf[5]=  g_waUACMaxPktSize[byFmtIdx] >> 8;
		VideoDescBufFillINT(4, g_waUACMaxPktSize[byFmtIdx]);
	}
//	else
//	{
//		VideoDescBuf[4] = 0x40;
//		VideoDescBuf[5] = 0x00;		// 8k 16bit , maxpacket should more than 32byte
//	}
	VideoDescBuf[6]=0x04;
	VideoDescBuf[7]=0x00;
	VideoDescBuf[8]=0x00;
}

#endif
*/
void USBDescInit( )
{
	U8 data i,j,k;
	U8 byFormatNum;
	U8 byFrameNum;
//	VideoFormat_T* pVideoFomat;

	byFormatNum = GetFormatNum(g_byIsHighSpeedDesc);

	for(i=0; i<UVC_DESC_TOTAL_NUM; i++)
	{
		UVC_Descs[i].type_idx = D_TYPE_NORMAL;
	}

	//===== configure=========
#ifdef _UAC_EXIST_
//	if (g_byMICMode == ADC2CODEC_EN)
//	{
//		UVC_Descs[0].Desc_Str =VideoCfgDesc;
//	}
//	else
	{
		UVC_Descs[0].Desc_Str =Video_AudioCfgDesc;
	}
#else
	UVC_Descs[0].Desc_Str =VideoCfgDesc;
#endif
	UVC_Descs[0].type_idx = D_TYPE_SPECIAL;

	j = 0;
#ifdef _UBIST_TEST_
	if (g_byUBISTEnble ==1)
	{ 
		j++;
		UVC_Descs[1].Desc_Str = UBistDesc;
	}
#endif

	//===== interface association=========
	j++;
	UVC_Descs[j].Desc_Str =VideoIADDesc;	
	
	//===== video control interface=========
	//------- standard video control interface--------
	j++;
	UVC_Descs[j].Desc_Str =VideoControlIfDesc;	
	
	// ----- class-specific video control interface-------
	j++;
	UVC_Descs[j].Desc_Str =VideoClsSpcVCIfDesc;	
	UVC_Descs[j].type_idx =D_TYPE_CLSSPCVCIF;	
	
	j++;
	UVC_Descs[j].Desc_Str =VideoCameraTrmDesc;
	UVC_Descs[j].type_idx =(D_TYPE_CAMTRM);

	j++;
	UVC_Descs[j].Desc_Str =VideoProcUnitDesc;	// hemonel 2010-05-25 code optimize: modify to constant string
	UVC_Descs[j].type_idx =(D_TYPE_PROCUNIT);	
#ifdef _FACIALAEWINSET_XU_
	j++;
	UVC_Descs[j].Desc_Str =FacialAEExtUnitDesc;  
#endif
	j++;
	UVC_Descs[j].Desc_Str =VideoOutTrmDesc;

	j++;
	UVC_Descs[j].Desc_Str =VideoExtDbgUnitDesc;
	UVC_Descs[j].type_idx  = D_TYPE_EXU;
#ifdef _DELL_EXU_
	j++;
	UVC_Descs[j].Desc_Str =VideoXU1ExtUnitDesc;

	//class-specific video control total length
	g_wVCCSdescSize = DESC_SIZE_CLS_VC_IF 	// hemonel 2010-06-03 optimize code: 
					+ DESC_SIZE_CAMERA_TRM
#ifdef _UVC_1V1_
					+DESC_SIZE_PROC_UNIT_1V1
#else
					+DESC_SIZE_PROC_UNIT_1V0
#endif
					+DESC_SIZE_OUT_TRM 
					+DESC_SIZE_EXT_DBG_UNIT
					+DESC_SIZE_DELLXU1_UNIT;
#else
	//class-specific video control total length
	g_wVCCSdescSize = DESC_SIZE_CLS_VC_IF 	// hemonel 2010-06-03 optimize code: 
					+ DESC_SIZE_CAMERA_TRM
#ifdef _UVC_1V1_
					+DESC_SIZE_PROC_UNIT_1V1
#else
					+DESC_SIZE_PROC_UNIT_1V0
#endif
					+DESC_SIZE_OUT_TRM 
					+DESC_SIZE_EXT_DBG_UNIT;
#endif

#ifdef _FACIALAEWINSET_XU_
	g_wVCCSdescSize +=DESC_SIZE_FACIAL_AE_XU;
#endif

#if defined _RTK_EXTENDED_CTL_ || defined _LENOVO_IDEA_EYE_EXTENDEDUNIT_
	j++;
	UvcRealtekExtendedUnitDes(j);
#endif

	//===== video control endpoint=========
	j++;
	UVC_Descs[j].Desc_Str =VideoStsIntEpDesc;

	j++;
	UVC_Descs[j].Desc_Str =VideoClsSpcStsEpDesc;

	//===== video streaming interface=========
	//------- standard video streaming interface--------
	j++;
	UVC_Descs[j].Desc_Str =VideoStreamIfDesc;
#ifdef _BULK_ENDPOINT_
	//------------- bulk endpoint ---------------
	j++;
	UVC_Descs[j].type_idx = D_TYPE_SPECIAL;
	UVC_Descs[j].Desc_Str = VideoStreamEpDesc;
	// ----- class-specific video streaming interface-------
	j++;
	UVC_Descs[j].Desc_Str = VideoDescBuf;
	UVC_Descs[j].type_idx = D_TYPE_VSINHDR;
	//class-specific video streaming total length
	g_wVSCSdescSize = 13 +byFormatNum;	// VS input header descriptor
#else
	// ----- class-specific video streaming interface-------
	j++;
	UVC_Descs[j].Desc_Str = VideoDescBuf;
	UVC_Descs[j].type_idx = D_TYPE_VSINHDR;
	//class-specific video streaming total length
	g_wVSCSdescSize = 13 +byFormatNum;	// VS input header descriptor
#endif//_BULK_ENDPOINT_

//	byTmp=0;
	for(k=1; k<=byFormatNum; k++) //k= format index
	{
//		byTmp++;
		j++;
		UVC_Descs[j].type_idx = D_TYPE_FMT|k;
		//	pVideoFomat=GetFormatPointer(k, g_byIsHighSpeedDesc);
		//	switch(pVideoFomat->byFormatType)
		// hemonel 2010-06-03 optimize code
		switch(GetFomatType(k, g_byIsHighSpeedDesc))
		{
		case FORMAT_TYPE_MJPG:
			UVC_Descs[j].Desc_Str = VideoStreamMjpegFmt_Desc;
			g_wVSCSdescSize += 0x0B;	// format descriptor
			break;
		case FORMAT_TYPE_YUV420:
			UVC_Descs[j].Desc_Str = VideoStreamVendorFmt_Desc1V0;
			g_wVSCSdescSize += 56;		// format descriptor
			break;
		case FORMAT_TYPE_M420:
			UVC_Descs[j].Desc_Str = VideoStreamVendorFmt_M420_Desc1V0;
			g_wVSCSdescSize += 56;		// format descriptor
			break;
		case FORMAT_TYPE_RAW:
			UVC_Descs[j].Desc_Str = VideoStreamRAWFmt_Desc1V0;
			g_wVSCSdescSize += 56;		// format descriptor
			break;
		case FORMAT_TYPE_YUY2:
		default:
			UVC_Descs[j].Desc_Str = VideoStreamUncompFmtDesc;
			g_wVSCSdescSize += 0x1B;	// format descriptor
			break;
		}

		byFrameNum = GetFormatFrameNum(UVC_IS_VIEDO,k, g_byIsHighSpeedDesc);
		for(i=1; i<=byFrameNum; i++)
		{
			j++;
			if(k==1)	//(byTmp ==1)
			{
				UVC_Descs[j].type_idx = D_TYPE_FRM1|i;
			}
			else if(k ==2)
			{
				UVC_Descs[j].type_idx = D_TYPE_FRM2|i;
			}
			else if(k ==3)	// hemonel 2010-08-26: max support 3 format
			{
				UVC_Descs[j].type_idx = D_TYPE_FRM3|i;
			}
			else
			{
				UVC_Descs[j].type_idx = D_TYPE_FRM4|i;
			}
			//	}
			UVC_Descs[j].Desc_Str = VideoDescBuf;
			g_wVSCSdescSize += ((GetFPSNum(UVC_IS_VIEDO, k,  i, g_byIsHighSpeedDesc)<<2)+26); // frame descriptor
		}

#ifndef _STILL_IMAGE_METHOD_1
		j++;
		UVC_Descs[j].type_idx = D_TYPE_STLIMG|k;
		UVC_Descs[j].Desc_Str = VideoDescBuf;
		g_wVSCSdescSize += (GetFormatFrameNum(UVC_IS_STILL,k,g_byIsHighSpeedDesc)<<2)+5+1; // still image frame descriptor
#endif

		j++;
		UVC_Descs[j].Desc_Str = VideoColorMatchDesc;	// color match descriptor
		g_wVSCSdescSize += 0x06;
	}
	// hemonel 2010-05-25 code optimize: not support bulk endpoint
//	if(g_byIsBulkEP)
//	{
//		for(i=1;i<=VS_IF_MAX_SETTING;i++)
//		{
//			j++;
//			UVC_Descs[j].type_idx = D_TYPE_IF|(i);
//			UVC_Descs[j].Desc_Str = VideoStreamAltIfDesc;
//			j++;
//			UVC_Descs[j].type_idx = D_TYPE_BULK_EP|(i);
//			UVC_Descs[j].Desc_Str = VideoStreamBulkEpDesc;
//		}
//	}
//	else
	{
#ifndef _BULK_ENDPOINT_
		for(i=1; i<=VS_IF_MAX_SETTING-1; i++)
		{
			j++;

			UVC_Descs[j].type_idx = D_TYPE_SPECIAL|(i);
			UVC_Descs[j].Desc_Str = VideoStreamAltIfDesc;

			j++;

			UVC_Descs[j].type_idx = D_TYPE_SPECIAL|(i);
			UVC_Descs[j].Desc_Str = VideoStreamEpDesc;
		}
#endif


	}

	g_wTotalCfgDescSize = DESC_SIZE_CONF					
						+DESC_SIZE_IAD
						+DESC_SIZE_INTF		// video control interface
						+g_wVCCSdescSize	// video control class specific
	                      +DESC_SIZE_EP		// interrupt endpoint
	                      +DESC_SIZE_CLS_VCI_EP	// interrupt endpoint class specific
	                      +DESC_SIZE_INTF		// video streaming interface
	                      +g_wVSCSdescSize	// video streaming class specific
#ifdef _BULK_ENDPOINT_
	                      +DESC_SIZE_EP;
#else
	                      +(DESC_SIZE_INTF+DESC_SIZE_EP)*(VS_IF_MAX_SETTING-1);
#endif


#ifdef _UBIST_TEST_		
	if (g_byUBISTEnble ==1)
	{ 
		g_wTotalCfgDescSize += 0x28;
	}
#endif	

#ifdef _UAC_EXIST_
		j++;
		UVC_Descs[j].Desc_Str = AudioStaticDesc_1;
		UVC_Descs[j].type_idx = D_TYPE_UAC|0x01;
		g_wTotalCfgDescSize += 237;

		j++;
		UVC_Descs[j].Desc_Str = AudioStaticDesc_2;
		UVC_Descs[j].type_idx =  D_TYPE_UAC|0x02;
		g_wTotalCfgDescSize += 172;
#endif	
#ifdef _HID_BTN_
	{
		j++;
		UVC_Descs[j].type_idx = D_TYPE_NORMAL;
		UVC_Descs[j].Desc_Str = cHIDDesc;
		j++;
		UVC_Descs[j].type_idx = D_TYPE_NORMAL;
		UVC_Descs[j].Desc_Str = cHIDClsDesc;
		j++;
		UVC_Descs[j].type_idx = D_TYPE_NORMAL;
		UVC_Descs[j].Desc_Str = cHIDClsSpcEpDesc;
		g_wTotalCfgDescSize += DESC_SIZE_INTF + DESC_SIZE_HID + DESC_SIZE_EP;        //Not include report
	}
#endif

}

void InitDescByTypeIdx(U8 byTypeIdx)
{
	U8 byType;
	byType = (byTypeIdx&0xf0);
	switch(byType)
	{
#ifndef _STILL_IMAGE_METHOD_1
	case D_TYPE_STLIMG:
		StillImgDescInit(byTypeIdx);
		break;
#endif
	case D_TYPE_FRM1:
	case D_TYPE_FRM2:
	case D_TYPE_FRM3:
	case D_TYPE_FRM4:	// hemonel 2010-08-26: max support 3 format
		VideoFrameDescInit(byTypeIdx);
		break;
//	case D_TYPE_FRM4:	// hemonel 2010-08-26: max support 3 format
//		VideoFrameDescInit(byTypeIdx);
//		break;
		// hemonel 2010-05-25 code optimize: modify to constant string
//		case D_TYPE_PROCUNIT:
//			ProcUnitDescInit();
//			break;
	case D_TYPE_VSINHDR:
		VideoStreamingInHdrDescInit();
		break;
#ifdef _UAC_EXIST_
//	case D_TYPE_UAC_ALT:
//		UACAltSettingDescInit(byTypeIdx);
//		break;
//	case D_TYPE_UAC_FMTD:
//		UACFormatDescInit(byTypeIdx);
//		break;
//	case D_TYPE_UAC_EP:
//		UACStreamingEPDescInit(byTypeIdx);
//		break;
#endif
	default:
		break;
	}
}

/*
*********************************************************************************************************
*                                           descs  iterator reset
* FUNCTION desc_iterator_reset
*********************************************************************************************************
*/
/**
   reset the desc iterator
                              clear g_byDescIdx and g_byDescOffset

  \param --none

  \retval --none
*********************************************************************************************************
*/
static void  desc_iterator_reset()
{
	g_byDescIdx=0;
	g_byDescOffset=0;
}

/*
*********************************************************************************************************
*                                           descs  iterator get operatioin
* FUNCTION desc_get
*********************************************************************************************************
*/
/**
     get the byte on current position (indicated by  g_byDescIdx and g_byDescOffset)
     of the configuration descriptor.



  \param
  	pDesc --  pointor to descs array

  \retval -- the byte on current position.
*********************************************************************************************************
*/

static U8 desc_get()
{
	U8 data type;
	U8 data idx;
#ifndef _BULK_ENDPOINT_
	U16 wTmp;
#endif

	if(UVC_Descs[g_byDescIdx].type_idx !=0)
	{
		idx=UVC_Descs[g_byDescIdx].type_idx&0x0f;
		type=UVC_Descs[g_byDescIdx].type_idx&0xf0;

		/*configureation desc*/
		switch (type)
		{
		case D_TYPE_SPECIAL:
			{
				if (UVC_Descs[g_byDescIdx].Desc_Str[1]
				        == USB_CONFIGURATION_DESCRIPTOR_TYPE)
				{

					switch(g_byDescOffset)
					{
						// hemonel 2010-05-25 code optimize: not support remote wakeup
						//	case 7:
						//		if(idx)
						//		{
						//			return USBD_ATTR_RWU;// support remote wake up
						//		}
						//		else
						//		{
						//			return USBD_ATTR_NO_RWU;
						//		}
						//		break;
					case 2:
						return g_wTotalCfgDescSize%256;
						//break;
					case 3:
						return g_wTotalCfgDescSize/256;
						//break;
					default:
						break;
					}
					break;
				}
				else if (UVC_Descs[g_byDescIdx].Desc_Str[1]
				         == USB_ENDPOINT_DESCRIPTOR_TYPE)
				{
#ifdef _BULK_ENDPOINT_
					if(!g_byIsHighSpeedDesc)
					{
						switch(g_byDescOffset)
						{
						case 4:
							return  0x40;// return 64 for full speed ,bulk endpoint
							//break;
						case 5:
							return  0x00;
							//break;
						default:
							break;
						}
					}
#else
					switch(g_byDescOffset)
					{
					case 4:
						wTmp = GetMaxPacketSize(idx,g_byIsHighSpeedDesc);
						return  INT2CHAR(wTmp, 0);
						//break;
					case 5:
						wTmp = GetMaxPacketSize(idx,g_byIsHighSpeedDesc);
						return  INT2CHAR(wTmp, 1);
						//break;
					default:
						break;
					}
#endif
					break;
				}
				else if (UVC_Descs[g_byDescIdx].Desc_Str[1]
				         == USB_INTERFACE_DESCRIPTOR_TYPE)
				{
					switch(g_byDescOffset)
					{
					case 3:
						return idx;
						//break;
					default:
						break;
					}
					break;
				}
				break;
			}

		case D_TYPE_FMT:
			{
				switch(g_byDescOffset)
				{
				case 3: //format index
					return idx;
					//break;
				case 4: //frame numbers
					return GetFormatFrameNum(1,idx,g_byIsHighSpeedDesc);
					//break;
				default:
					break;
				}
				break;
			}
			// hemonel 2010-05-25 code optimize: add for modify to constant string
		case D_TYPE_PROCUNIT:// awb type, none, temperature or component
			{
				switch(g_byDescOffset)
				{
				case 8:
					return INT2CHAR(g_wProcUnitCtlSel, 0);
					//break;
				case 9:
					return INT2CHAR(g_wProcUnitCtlSel, 1);
					//break;
				default:
					break;
				}
				break;
			}



		case D_TYPE_EXU:
			{
				switch(g_byDescOffset)
				{
				case 20:
					if ((g_wExtUIspCtl&EXU1_CTL_SEL_TCORRECTION)==EXU1_CTL_SEL_TCORRECTION)
					{
						return 0x03;
					}
					else
					{
						return 0x02;
					}
					//break;
				case 24:
					if ((g_wExtUIspCtl&EXU1_CTL_SEL_TCORRECTION)==EXU1_CTL_SEL_TCORRECTION)
					{
						return 0x01;
					}
					else
					{
						return 0x00;
					}
					//break;
				default:
					break;
				}
				break;
			}
		case D_TYPE_CLSSPCVCIF://UVC version ID
			{
				switch(g_byDescOffset)
				{
					// hemonel 2010-05-25 code optimize: not support uvc1.1
					//	case 3:
					//		{
					//			if(g_byIsUVC1V1)
					//			{
					//				return (_UVC_VER_1V1%256);
					//			}
					//			else
					//			{
					//				return (_UVC_VER_1V0%256);
					//			}
					//			break;
					//		}
				case 5:
					return g_wVCCSdescSize%256;
					//break;
				case 6:
					return g_wVCCSdescSize/256;
					//break;
				default:
					break;
				}
				break;
			}
		case D_TYPE_CAMTRM:
			{
				switch(g_byDescOffset)
				{
				case 15:
					return INT2CHAR(g_wCamTrmCtlSel, 0);	// hemonel 2009-12-18: add PTZ control
				case 16:
					return INT2CHAR(g_wCamTrmCtlSel, 1);	// hemonel 2009-12-18: add PTZ control
#ifdef _AF_ENABLE_
				case 17:
					return g_byCamTrmCtlSel_Ext;	// hemonel 2009-12-18: add PTZ control
#endif
				default:
					break;
				}
				break;
			}
#if defined _RTK_EXTENDED_CTL_ || defined _LENOVO_IDEA_EYE_EXTENDEDUNIT_
		case D_TYPE_EXTU_RTK:
			{
				return UvcRealtekExtendedUnitDesPro(g_byDescIdx,g_byDescOffset);
			}
#endif
//		case D_TYPE_UAC_ALT:
//			if (g_byDescOffset == 2)
//			{
//				UVC_Descs[g_byDescIdx].Desc_Str[0] = 172;
//			}
//			return UVC_Descs[g_byDescIdx].Desc_Str[g_byDescOffset];
			
//		case D_TYPE_UAC_EP:
//			if (g_byDescOffset == 2)
//			{
//				UVC_Descs[g_byDescIdx].Desc_Str[0] = 237;
//			}
//			return UVC_Descs[g_byDescIdx].Desc_Str[g_byDescOffset];		
		default:
			break;
		}
	}

	return UVC_Descs[g_byDescIdx].Desc_Str[g_byDescOffset];
}
/*
*********************************************************************************************************
*                                           descs  iterator move pointer
* FUNCTION desc_next
*********************************************************************************************************
*/
/**
     move the position (indicated by  g_byDescIdx and g_byDescOffset) of the configuration descriptor.



  \param
  	pDesc --  pointor to descs array
  	DescCount-- descs count.

  \retval -- true , move operation succeed
                     false,  move operation failed.  It indicates the pointor point to  the end of the descs.
*********************************************************************************************************
*/
static void desc_next(void)
{
	U8 byTypeIdx;
	U8 bynum;
#ifdef _UAC_EXIST_
	if (UVC_Descs[g_byDescIdx].type_idx == (D_TYPE_UAC|0x01))
	{
		bynum = 236;
	}
	else 	if (UVC_Descs[g_byDescIdx].type_idx == (D_TYPE_UAC|0x02))
	{
		bynum = 171;
	}
	else
#endif		
	{
		bynum = (UVC_Descs[g_byDescIdx].Desc_Str[0]-1);
	}

	if(g_byDescOffset==bynum)
	{
		g_byDescIdx++;
		byTypeIdx = UVC_Descs[g_byDescIdx].type_idx;
		InitDescByTypeIdx( byTypeIdx);
		g_byDescOffset=0;
	}
	else
	{
		g_byDescOffset++;
	}
	// hemonel 2010-06-03 optimize code
//	if(g_byDescIdx>=(DescCount)) //overflow
//	{
//		return FALSE;
//	}
//	return  TRUE;

}


/*
*********************************************************************************************************
*                                          Deal With Root Reset
* FUNCTION USBProReset
*********************************************************************************************************
*/
/**
  Reset DMA and bulk endpoints when port reset.

  \param None

  \retval None
*********************************************************************************************************
*/
void	USBProReset(void )
{
	XBYTE[USBSYS_IRQSTAT] = PORT_RESET_INT;
	XBYTE[EPC_CTL]=EPC_FIFO_FLUSH;
	XBYTE[EPA_CTL]=EPA_FIFO_FLUSH;
#ifdef _HID_BTN_
	XBYTE[EPD_CTL]=EPD_FIFO_FLUSH;
#endif
}



/*
*********************************************************************************************************
*                                          STALL control pipe
* FUNCTION STALL_EPCTL
*********************************************************************************************************
*/
/**
    stall control pipe

  \param None

  \retval None
*********************************************************************************************************
*/


void  STALL_EPCTL(void )
{
//	FUNC_IN_MSG("STALL_EPCTL");
	MSG(("\n stall"));
	EP0_STALL();

}

/*
*********************************************************************************************************
*                                                   print error message.
* FUNCTION VCErrCodeMsg
*********************************************************************************************************
*/
/**
  parse video control interface error code and print error message.

  \param None

  \retval None
*********************************************************************************************************
*/
#ifdef  MSG_ENABLE
void VCErrCodeMsg(U8 ErrCode)
{
	MSG(("\nVC stall Err code = "));
	switch(ErrCode)
	{
	case VC_ERR_NOERROR:
		MSG(("no Error"));
		break;
	case VC_ERR_NOTRDY:
		MSG(("Not Ready"));
		break;
	case VC_ERR_WRNGSTAT:
		MSG(("Wrong State"));
		break;
	case VC_ERR_PWR:
		MSG(("Power"));
		break;
	case VC_ERR_OUTOFRANGE:
		MSG(("Out of Range"));
		break;
	case VC_ERR_INVDUNIT:
		MSG(("Invalid Unit"));
		break;
	case VC_ERR_INVDCTL:
		MSG(("Invalid Control"));
		break;
	case VC_ERR_INVDREQ:
		MSG(("Invalid Request"));
		break;
	default:
		MSG(("Unknown Error"));
		break;
	}
	return;
}
#endif
/*
*********************************************************************************************************
*                                          parse video control interface error code and print error message.
* FUNCTION VCSTALL_EPCTL
*********************************************************************************************************
*/
/**
	  print error message,  save Errcode and stall control pipe.

  \param None

  \retval None
*********************************************************************************************************
*/

void VCSTALL_EPCTL(U8 ErrCode)
{
#ifdef  MSG_ENABLE
	VCErrCodeMsg(ErrCode);
#endif
	g_byVCLastError=ErrCode;
	STALL_EPCTL();
}

U8 SendData_byEP0FIFO(void)
{
	EP0_FFV();
	if(!WaitTimeOut(EP0_CTL, EP0_FIFO_VALID, 0, 250))
	{
		USBProReset();

		// hemonel 2010-06-17: at toshiba NB BIOS self-detect, the time of get configure descriptor is short so that device don't send any data.
		// add handshake for SIE state restore
		EP0_HSK() ;
		return 0;
	}
	return 1;
}

#ifdef _REMOTE_WAKEUP_SUPPORT_
void RemoteWakeupFunc(void)
{

	XBYTE[USBCTL] |= USB_RMTWKUP_EN; 
//		MIPI_MSG(("HS %bx \n", 	XBYTE[0xFA43] ));
}
#endif
/*
*********************************************************************************************************
*                                          USB Get Descriptor Requests  Process
* FUNCTION USBGetDevDescReqProc
*********************************************************************************************************
*/
void USBGetDevDescReqProc()
{
	U16 count;
	U8 i;
	U8 byCount;

	ASSIGN_INT(count,XBYTE[SETUP_PKT_wLEN_H],XBYTE[SETUP_PKT_wLEN_L]);
	// hemonel 2010-05-25: optimize code
//	if(sizeof(VideoDevDesc) < count)
//	{
//		count = sizeof(VideoDevDesc); // wLength = valid device descriptor bLength
//	}
	byCount = ClipWord(count, 0, sizeof(VideoDevDesc));
	for (i = 0; i < byCount; i++)
	{
		if(i==8)//_PID_L
		{
			EP0_DATA_IN(g_byVendorIDL);
			continue;
		}
		if(i==9)//_PID_H
		{
			EP0_DATA_IN(g_byVendorIDH);
			continue;
		}
		if(i==10)//_VID_L
		{
			EP0_DATA_IN(g_byProductIDL);
			continue;
		}
		if(i==11)//_VID_H
		{
			EP0_DATA_IN(g_byProductIDH);
			continue;
		}
		if(i==12) //FW version ID low byte
		{
			EP0_DATA_IN(g_byDevVerID_L);
			continue;
		}
		if(i==13) //FW version ID High byte
		{
			EP0_DATA_IN(g_byDevVerID_H);
			continue;
		}
		EP0_DATA_IN(VideoDevDesc[i]);
	}
	EP0_FFV_HSK();
	return ;
}
/*
*********************************************************************************************************
*                                          USB Get Descriptor Requests  Process
* FUNCTION USBGetCfgescReqProc
*********************************************************************************************************
*/
/**

  \param configtype    0: standard configuration descriptor type
  				    1: other speed configuration descriptor type

  \retval None
*********************************************************************************************************
*/


void USBGetCfgDescReqProc(U8  type)
{
	U16 data i;
	U8 data value;
	U16 count, count_2;
	// this device has only one configuration, otherwise,
	// descriptor index (wValue Low byte) must be checked
	ASSERT((type==USB_CONFIGURATION_DESCRIPTOR_TYPE)
	       ||(type==USB_OTHER_SPEED_CONFIGURATION_TYPE));
	switch (type)
	{
	case USB_CONFIGURATION_DESCRIPTOR_TYPE:
		if(XBYTE[USBSTAT] & HIGH_SPEED)
		{
			g_byIsHighSpeedDesc=1;
		}
		else
		{
			g_byIsHighSpeedDesc=0;
		}
		break;

	default: //case USB_OTHER_SPEED_CONFIGURATION_TYPE:
		if(XBYTE[USBSTAT] & HIGH_SPEED)
		{
			g_byIsHighSpeedDesc=0;
		}
		else
		{
			g_byIsHighSpeedDesc=1;
		}
		break;
	}

	ASSIGN_INT(count,XBYTE[SETUP_PKT_wLEN_H],XBYTE[SETUP_PKT_wLEN_L]);
	count_2 = count;

	{
		USBDescInit();

		if( g_wTotalCfgDescSize< count)
		{
			count = g_wTotalCfgDescSize;
		}
		desc_iterator_reset();

		EP0_DATA_IN(desc_get());//config length

		for (i = 1; i < count; i++)
		{
			if((i%64)==0)
			{
				if(!SendData_byEP0FIFO())
				{
					return;
				}
			}

			desc_next();
			if(i==1)//desc type
			{
				value = (type);
			}
			else
			{
				value=desc_get();
			}
			EP0_DATA_IN(value);
		}

		if(!SendData_byEP0FIFO())
		{
			return;
		}

		if (((g_wTotalCfgDescSize%64)==0)&&(count_2 > g_wTotalCfgDescSize))
		{
			if(!SendData_byEP0FIFO())
			{
				return;
			}
		}
		EP0_HSK() ;
	}
	return;
}
/*
*********************************************************************************************************
*                                  USB String Descriptor Requests  Process
* FUNCTION StrescProc
*parameters
*********************************************************************************************************
*/
/**

  \param
  	ExtZero - if 1, the string should be extended with '\0' following every char. if 0 , the string should be sent as the original string.
  	size        -size of the string pointer by pStr .
  	pStr        -the string pointer.

  \retval None
*********************************************************************************************************
*/
void StrDescProc(U8 ExtZero, U8 const * const pStr )
{
	U8 i;
	U16 count;
	U8 size;
	U8 byCount;

	size = pStr[0];
	ASSIGN_INT(count,XBYTE[SETUP_PKT_wLEN_H],XBYTE[SETUP_PKT_wLEN_L]);
	if(0==ExtZero)
	{
		//	if(count > (U16) size)
		//	{
		//		count=(U16)size;
		//	}
		byCount = ClipWord(count, 0, size);
		for (i = 0; i <byCount ; i++)
		{
			//	if(i==0)
			//	{
			//		EP0_DATA_IN(size);
			//	}
			//	else
			//	{
			//		EP0_DATA_IN(pStr[i]);
			//	}
			if(i==0)
			{
				EP0_DATA_IN(pStr[0]);
			}
			//else if(i==1)
			//{
			//	EP0_DATA_IN(pStr[1]);
			//}
			else
			{
				if((i%64)==0)
				{
					if(!SendData_byEP0FIFO())
					{
						return;
					}
				}
				EP0_DATA_IN(pStr[i]);
			}
		}
	}
	else
	{
		size = (size<<1)-2;
		//	if(count>(U16)size)
		//	{
		//		count= (U16)size;
		//	}
		byCount = ClipWord(count, 0, size);
		for(i=0; i<byCount; i++)
		{
			if(i==0)
			{
				EP0_DATA_IN(size);//left 2 byte need not extended with '\0'.
			}
			else if(i==1)
			{
				EP0_DATA_IN(pStr[1]);
			}
			else
			{
				if((i%64)==0)
				{
					if(!SendData_byEP0FIFO())
					{
						return;
					}
				}

				if((i&0x01)==0)    //even offset ansi char
				{
					EP0_DATA_IN(pStr[(i>>1)+1]);
				}
				else   //odd offset fill '\0'
				{
					EP0_DATA_IN(0);
				}
			}
		}
	}

#ifdef EP0_LPM_L1_TEST
	Delay(100);
#endif
	// hemonel 2012-01-12: fix string length is 64bytes bug
//	EP0_FFV_HSK();
	if(!SendData_byEP0FIFO())
	{
		return;
	}
	
	if(((i%64)==0) && (count>i)) // send zero byte data packet if host send in token
	{
		if(!SendData_byEP0FIFO())
		{
			return;
		}
	}
#ifdef EP0_LPM_L1_TEST
	Delay(100);
#endif	
	EP0_HSK();
	// hemonel 2012-01-12 end
	return;
}

/*
*********************************************************************************************************
*                                          USB Get Descriptor Requests  Process
* FUNCTION USBGetStrescReqProc
*********************************************************************************************************
*/
void USBGetStrDescReqProc()
{
	U8 const * ucPtr;

	switch(XBYTE[SETUP_PKT_wVALUE_L])   // descriptor index
	{
	case I_LANG_ID:
		StrDescProc(0,iLangID);
		break;
	case  I_MANUFACTURER:
		{

			if(g_wVdStrManuFactorAddr)//g_wVdStrManuFactorAddr!=0
			{
				if(CopyVdCfgByte2SRam(g_wVdStrManuFactorAddr,(U8 *)&XBYTE[SPI_I2C_BUFFER_BASE],50))
				{

					{
						ucPtr=(U8 *)&XBYTE[SPI_I2C_BUFFER_BASE];
						StrDescProc(1,ucPtr);
					}
				}
				else
				{
					StrDescProc(1,iDummy);
				}
				break;
			}
			else

			{
				StrDescProc(1,iManufacturer);
				break;
			}
		}

	case I_PRODUCT:
		{

			if(g_wVdStrProductAddr)//g_wVdStrProductAddr!=0
			{
				if(CopyVdCfgByte2SRam(g_wVdStrProductAddr,(U8 *)&XBYTE[SPI_I2C_BUFFER_BASE],50))		
				{

					{
						ucPtr=(U8 *)&XBYTE[SPI_I2C_BUFFER_BASE];
						StrDescProc(1,ucPtr);
					}
				}
				else
				{
					StrDescProc(1,iDummy);
				}
				break;
			}
			else

			{
				//	StrDescProc(0,sizeof(iProduct),iProduct);
				// hemonel 2009-12-02: open device name to customer code
				StrDescProc(1,iProduct);
				break;
			}
		}
#ifdef _UAC_EXIST_
	case I_PRODUCT_MIC:
		StrDescProc(1,iProduct_MIC);
		break;
#endif

	case I_SERIALNUMBER:
		{

			if(g_wVdStrSerialNumAddr)//g_wVdStrSerialNumAddr!=0
			{
				if(CopyVdCfgByte2SRam(g_wVdStrSerialNumAddr,(U8 *)&XBYTE[SPI_I2C_BUFFER_BASE],50))
				{

					{
						ucPtr=(U8 *)&XBYTE[SPI_I2C_BUFFER_BASE];
						StrDescProc(1,ucPtr);
					}
				}
				else
				{
					StrDescProc(1,iDummy);
				}

				break;
			}
			else
			{
				StrDescProc(1,iSerialNumber);
				break;
			}
		}
	case I_CONFIGURATION:
		//	StrDescProc(0,sizeof(iProduct),iProduct);
		// hemonel 2009-12-02: open device name to customer code
		StrDescProc(1,iProduct);
		break;
	case I_VIDEOFUNCTION:
		if(g_wVdIADStrAddr)//g_wVdIADStrAddr!=0
		{
			if(CopyVdCfgByte2SRam(g_wVdIADStrAddr,(U8 *)&XBYTE[SPI_I2C_BUFFER_BASE],50))				
			{

				{
					ucPtr=(U8 *)&XBYTE[SPI_I2C_BUFFER_BASE];
					StrDescProc(1,ucPtr);
				}
			}
			else
			{
				StrDescProc(1,iDummy);
			}
		}
		else
		{
			//	StrDescProc(0,sizeof(iProduct),iProduct);
			// hemonel 2009-12-02: open device name to customer code
			StrDescProc(1,iProduct);
		}
		break;
#if defined _RTK_EXTENDED_CTL_ || defined _LENOVO_IDEA_EYE_EXTENDEDUNIT_
	case I_RTK_EXTENDED_CTL_UNIT:
                StrDescProc(1,iRtkExtUnitStr);
		break; 
#endif
#ifdef _UBIST_TEST_
	case I_UBIST_DEVICENAME: // hemonel 2009-12-01: add UBIST test for DELL
		if(g_wVdUBISTStrAddr)//g_wVdIADStrAddr!=0
		{
			if(CopyVdCfgByte2SRam(g_wVdUBISTStrAddr,(U8 *)&XBYTE[SPI_I2C_BUFFER_BASE],50))					
			{

				{
					ucPtr=(U8 *)&XBYTE[SPI_I2C_BUFFER_BASE];
					StrDescProc(1,ucPtr);
				}
			}
			else
			{
				StrDescProc(1,iDummy);
			}
		}
		else
		{
			// hemonel 2009-12-02: open device name to customer code
			StrDescProc(1,iProduct);
		}
		break;
		// hemonel 2009-12-01: add UBIST test for DELL
	case I_UBIST_TESTNAME1:
		StrDescProc(1,iUbistTestName1);
		break;
	case I_UBIST_TESTNAME2:
		StrDescProc(1,iUbistTestName2);
		break;
	case I_UBIST_TESTNAME3:
		StrDescProc(1,iUbistTestName3);
		break;
	case I_UBIST_TESTNAME4:
		StrDescProc(1,iUbistTestName4);
		break;
	case I_UBIST_TESTNAME5:
		StrDescProc(1,iUbistTestName5);
		break;
	case I_UBIST_TESTNAME6:
		StrDescProc(1,iUbistTestName6);
		break;
#endif
	default:        // undefined string index
		STALL_EPCTL(); // will return a STALL handshake
		break;
	}
	return;

}
/*
*********************************************************************************************************
*                                          USB Get Descriptor Requests  Process
* FUNCTION USBGetStrescReqProc
*********************************************************************************************************
*/
void USBGetDevQualifierDescReqProc()
{
	U16 count;
	U8 i;
	ASSIGN_INT(count,XBYTE[SETUP_PKT_wLEN_H],XBYTE[SETUP_PKT_wLEN_L]);
	if(sizeof(Device_Qualifier) < count)
	{
		count = sizeof(Device_Qualifier);
	}
	for (i = 0; i < count; i++)
	{
		EP0_DATA_IN(Device_Qualifier[i]);
	}
	EP0_FFV_HSK();
	return;
}

#ifdef _USB2_LPM_ // JQG_add_2010_0415_
void USBGetBOSDescReqProc(void)
{
	U16 count;
	U8 code * pData;
	U8 i;

	ASSIGN_INT(count,XBYTE[SETUP_PKT_wLEN_H],XBYTE[SETUP_PKT_wLEN_L]);
	if( count > sizeof(sBOSDeviceCapabilityDescriptor))
	{
		count = sizeof(sBOSDeviceCapabilityDescriptor);
	}

	pData = &(sBOSDeviceCapabilityDescriptor.sBOSDescriptor.byLength);

	for (i = 0; i < count; i++)
	{
		EP0_DATA_IN(pData[i]);
	}

	EP0_FFV_HSK();
	return ;
}
#endif
#ifdef _HID_BTN_
void USBGetHIDClsDescProc(void )
{
	U16 wCount;
	U8 i;
	U8 byCount;

	ASSIGN_INT(wCount,XBYTE[SETUP_PKT_wLEN_H],XBYTE[SETUP_PKT_wLEN_L]);
	byCount = ClipWord(wCount, 0, sizeof(cHIDClsDesc));

	for (i = 0; i < byCount; i++)
	{
	     EP0_DATA_IN(cHIDClsDesc[i]);
	}
	EP0_FFV_HSK();
	return ;	
}

void USBGetHIDReportDescProc(void )
{
	U16 wCount;
	U8 i;
	U8 byCount;

	ASSIGN_INT(wCount,XBYTE[SETUP_PKT_wLEN_H],XBYTE[SETUP_PKT_wLEN_L]);
	byCount = ClipWord(wCount, 0, sizeof(cHIDClsReportDesc));

	for (i = 0; i < byCount; i++)
	{
	     EP0_DATA_IN(cHIDClsReportDesc[i]);
	}
	EP0_FFV_HSK();
	return ;	
}
#endif

/*
*********************************************************************************************************
*                                          USB Get Descriptor Requests  Process
* FUNCTION USBGetDescReqProc
*********************************************************************************************************
*/
/**
  Respond to the Get Descriptors  requests from host and complete the related
  operations. The response is send to host mainly thru endpoint 0. If encountering the requests not
  supported the corresponding endpoint would be stalled as well as the operations when an error occurs.
  It is called by main module ISR while a setup packet interrupt occurs.

  \param None

  \retval None
 *********************************************************************************************************
*/
void USBGetDescReqProc()
{
	switch (XBYTE[SETUP_PKT_wVALUE_H]) /* descriptor type in the high byte*/
	{
	case USB_DEVICE_DESCRIPTOR_TYPE:
		USBGetDevDescReqProc();
		break;
	case USB_CONFIGURATION_DESCRIPTOR_TYPE:
		USBGetCfgDescReqProc(USB_CONFIGURATION_DESCRIPTOR_TYPE);
		break;
	case USB_STRING_DESCRIPTOR_TYPE:
		USBGetStrDescReqProc();
		break;
	case USB_DEVICE_QUALIFIER_DESCRIPTOR_TYPE :
		USBGetDevQualifierDescReqProc();
		break;
#ifdef _USB2_LPM_ // JQG_add_2010_0415_
	case USB_BOS_DESCRIPTOR_TYPE :
		USBGetBOSDescReqProc();
		break;
#endif
	case USB_OTHER_SPEED_CONFIGURATION_TYPE :
		USBGetCfgDescReqProc(USB_OTHER_SPEED_CONFIGURATION_TYPE);
		break;
#ifdef _HID_BTN_
		case USB_HID_CLASS_DESCRIPTOR:
			USBGetHIDClsDescProc();
			break;
		case USB_HID_REPORT_DESCRIPTOR:
			USBGetHIDReportDescProc();        	
			break;
#endif
	default:/* invalid command, Request Error*/
		STALL_EPCTL(); /* return a STALL handshake*/
		break;
	}
}
/*
*********************************************************************************************************
*                                          USB Set Address Requests  Process
* FUNCTION USBSetAdrReqProc
*********************************************************************************************************
*/
/**
  Respond to the Get Descriptors  requests from host and complete the related
  operations. The response is send to host mainly thru endpoint 0. If encountering the requests not
  supported the corresponding endpoint would be stalled as well as the operations when an error occurs.
  It is called by main module ISR while a setup packet interrupt occurs.

  \param None

  \retval None
 *********************************************************************************************************
*/
void USBSetAdrReqProc()
{
	U8 const byAddr = XBYTE[SETUP_PKT_wVALUE_L];

	if(XBYTE[USBSTAT] & HIGH_SPEED)
	{
		g_bIsHighSpeed=1;
	}
	else
	{
		g_bIsHighSpeed=0;
	}
//	XBYTE[EP0_IRQSTAT] = EP0_IRQ_IN_TOKEN;	/ hemonel 2010-05-31: delete it

	// hemonel 2009-09-16: usb if chapter 9 test (default) fail
	// After host set address(1), host will set address(0). At this time, device is not sent control status handshake.
	// When device is in address state(host has already set device address), device must send control status handshake at first if change device address.
	if(XBYTE[DEVADDR]==0)
	{
		// USB spec:the device doesn't change its device address until after the Status stage
		// Before addressed state, hardware will not change its device address until after the Status stage
		XBYTE[DEVADDR] = byAddr;
		EP0_HSK();
	}
	else
	{
		EP0_HSK();
		//joey 2004-01-14
		if(!WaitTimeOut(EP0_STAT,EP0_STS_END,1,15))
		{
			return;
		}
		else
		{
			XBYTE[DEVADDR] = byAddr;
		}
	}
	return;
}

// hemonel 2009-07-20: only for singal rate test debug at usb if test packet
#ifdef _TEST_PHY_GAIN_
void PHYDebug(void)
{
	//enable Hclk output when enter usb if test.
	XBYTE[BLK_CLK_EN] |= ISP_CCS_CLK_EN;
	XBYTE[CCS_CLK_SWITCH] = CCS_CLK_SEL_96M|CCS_CLK_DIVIDER_4;
	XBYTE[CCS_CLK_SWITCH] |= CCS_CLK_ENABLE;

	POWER_ON_SV18();
	POWER_ON_SV28();
}
#endif

/*
*********************************************************************************************************
*                                          USB Set Config Requests  Process
* FUNCTION USBSetCfgReqProc
*********************************************************************************************************
*/
/**
  Respond to the Get Descriptors  requests from host and complete the related
  operations. The response is send to host mainly thru endpoint 0. If encountering the requests not
  supported the corresponding endpoint would be stalled as well as the operations when an error occurs.
  It is called by main module ISR while a setup packet interrupt occurs.

  \param None

  \retval None
 *********************************************************************************************************
*/

void USBSetCfgReqProc()
{
	switch (XBYTE[SETUP_PKT_wVALUE_L])  // lower byte of wValue specifies the desired configuration
	{
	case 0x00 : // Un-configured
		{
			g_byConfigurationValue = 0x00;
			//===Configure Endpoint A================
			//	XBYTE[EPA_MAXPKT0] = 0x00;	// hemonel 2010-05-31 optimize: needn't configure endpoint max packet size before endpoint enable
			//	XBYTE[EPA_MAXPKT1] = 0x02;
			XBYTE[EPA_CFG] = EPA_NUMBER1; // not enabled
			XBYTE[EPA_IRQEN] = 0x00;

			//===Configure Endpoint C================
			//	XBYTE[EPC_MAXPKT] = MAX_VCINTEP_PKT_SIZE;	// hemonel 2010-05-31 optimize: needn't configure endpoint max packet size before endpoint enable
			XBYTE[EPC_CFG] = EPC_NUMBER3; // not enabled
			//	XBYTE[EPC_IRQEN] = 0x00;		// hemonel 2010-05-31 optimize: not use EPC and default is 0
			//	XBYTE[EPC_STAT]=0XFF;
			XBYTE[EPC_CTL] = EPC_FIFO_FLUSH;

#ifdef _UAC_EXIST_
			//===Configure Endpoint B================
			//	XBYTE[EPB_MAXPKT0] = 0x00;	// hemonel 2010-05-31 optimize: needn't configure endpoint max packet size before endpoint enable
			//	XBYTE[EPB_MAXPKT1] = 0x02; //512 bytes
			XBYTE[EPB_CFG] = EP_IDX_AUDIO_STREAM; // not enabled
			XBYTE[EPB_IRQEN] = 0x00;
//				XBYTE[EPB_STAT]=0XFF;
#endif
#ifdef _HID_BTN_
			XBYTE[EPD_CFG] = EPD_NUMBER4;
			XBYTE[EPD_CTL] = EPD_FIFO_FLUSH;
			//XBYTE[EPD_IRQEN] = 0x00;
			//XBYTE[EPD_STAT]=0XFF;
#endif
			EP0_HSK();
			//joey 2004-02-26  for ICH4 TOGGLE BIT PROBLEM WHEN DISABLE/ENABLE DEVICE
			//XBYTE[EPA_CTL] = STALL_EP;
			XBYTE[EPC_CTL] = EP0_STALL_EP;
#ifdef _HID_BTN_
			XBYTE[EPD_CTL] = EP0_STALL_EP;
#endif
			//WaitTimeOut(0,0,0,1);   //at least delay 1ms (1 Frame)
			uDelay(15);
			XBYTE[EPA_CTL] = 0;

			XBYTE[EPC_CTL] = 0;
#ifdef _UAC_EXIST_
			XBYTE[EPB_CTL] = 0;
			XBYTE[EPB_FLOW_CTL] = 0;
#endif
#ifdef _HID_BTN_
			XBYTE[EPD_CTL] = 0;
#endif

		}
		break; // case 0x00

	case 0x01 : // using current configuration, do nothing
		{
			g_byConfigurationValue = 0x01;

			//===Configure Endpoint A================
#ifdef _BULK_ENDPOINT_
			if(g_bIsHighSpeed)
			{
				XBYTE[EPA_MAXPKT0] = 0x00;//high speed ,maxpacketsize set to 512
				XBYTE[EPA_MAXPKT1] = 0x02;
			}
			else
			{
				XBYTE[EPA_MAXPKT0] = 0x40; //full speed ,maxpacketsize set to 64
				XBYTE[EPA_MAXPKT1] = 0x00;
			}

			XBYTE[EPA_CTL] = EPA_STALL_EP;	// initialize data sequence 0
			XBYTE[EPA_CTL] = EPA_PACKET_EN|EPA_FIFO_FLUSH;
			XBYTE[FORCE_TOGGLE] = FORCE_EPA_DATA0;			//Force EPA data sequence data0	
			XBYTE[EPA_CFG] = EPA_EN|EPA_TYPE_BULK|EPA_NUMBER1|EPA_BULK_MODE_BURST;
			XBYTE[EPA_IRQEN] = EPA_IRQ_IN_TOKEN;	// Enable EPA in-token interrupt
			XBYTE[EPA_IRQSTAT] = 0xff;
#ifndef _USB2_LPM_
			XBYTE[RF_EPA_TRANSFER_SIZE_0] = INT2CHAR(g_wTransferSize, 0);
			XBYTE[RF_EPA_TRANSFER_SIZE_1] = INT2CHAR(g_wTransferSize, 1);
			XBYTE[RF_HWM_0]=INT2CHAR(g_wHWM, 0);
			XBYTE[RF_HWM_1]=INT2CHAR(g_wHWM, 1);
#endif
#else//isochronous
			XBYTE[EPA_CFG] = EPA_NUMBER1;	// not enabled
			XBYTE[EPA_CTL] = EPA_FIFO_FLUSH;
			XBYTE[EPA_IRQEN] = 0x00;
			XBYTE[EPA_IRQSTAT] = 0xff;
#endif//_BULK_ENDPOINT_

			//===Configure Endpoint C===============
			XBYTE[EPC_MAXPKT] = MAX_VCINTEP_PKT_SIZE;
			XBYTE[EPC_CFG] = EPC_NUMBER3;
			XBYTE[EPC_CTL] = EPC_FIFO_FLUSH;
			//	XBYTE[EPC_IRQEN] = 0;	// hemonel 2010-05-31 optimize: not use EPC and default is 0
			//	XBYTE[EPC_IRQSTAT] = 0xff;
			XBYTE[EPC_CFG] |= EPC_EN;
#ifdef _HID_BTN_
			XBYTE[EPD_MAXPKT] = 0x01;	
			XBYTE[EPD_CTL] = EPD_FIFO_FLUSH;
			XBYTE[EPD_IRQSTAT] = 0xff;
			XBYTE[EPD_CFG] = EPD_EN|EPD_NUMBER4;
#endif

			// hemonel 2009-06-18: initilize probe control because liunix os will send get probe control before set probe control.
			g_VsProbe.bmHint=0;
			g_VsProbe.bFormatIndex=1;
			g_VsProbe.bFrameIndex=1;
			g_VsProbe.dwFrameInterval= GetDefaultFrameInterval(UVC_IS_VIEDO,1,1,(U8)g_bIsHighSpeed);
			g_VsProbe.dwMaxVideoFrameSize=GetMaxFrameSize(UVC_IS_VIEDO,1,1,(U8)g_bIsHighSpeed);
			g_VsProbe.dwMaxPayloadTransferSize=GetMaxPayloadTransferSize(g_VsProbe.dwMaxVideoFrameSize, 40, (U8)g_bIsHighSpeed);

			g_VsStlProbe.bFormatIndex= 1;
			g_VsStlProbe.bFrameIndex= 1;
			g_VsStlProbe.bCompressionIndex = 1;
			g_VsStlProbe.dwMaxVideoFrameSize=GetMaxFrameSize(UVC_IS_STILL,1,1,(U8)g_bIsHighSpeed);
			g_VsStlProbe.dwMaxPayloadTransferSize=g_VsProbe.dwMaxPayloadTransferSize;
			//2009-03-09 cheney.cai:  for fix USB_IF UVCTest bug.
			// At set configure, we change white balance to manual. And after set interface, we change white balance back to the default value that we are set.
			// hemonel 2009-08-12:after USBCV 1.3, usb.org fix this bug
			// and use this methold will test fail
			// mark these code, test pass at USBCV1.3.2 and 1.3.5.5
			/*	g_byWhiteBalanceTempAutoDef= 0;
				g_byWhiteBalanceTempAutoLast = g_byWhiteBalanceTempAutoDef;
				if(g_byWhiteBalanceTempAutoLast ==0)
				{
					Ctl_WhiteBalanceTemp.Info &=(~(CONTROL_INFO_DIS_BY_AUTO|CONTROL_INFO_SP_AUTOUPDATA));
				}
			*/
			//2010-03-25 hemonel: when disable gain function, manual white balance USB_IF UVCTest fail.
/*				WhiteBalanceTempAutoItem.Last = 0;
				if(WhiteBalanceTempAutoItem.Last ==0)
				{		
					Ctl_WhiteBalanceTemp.Info &=(~(CONTROL_INFO_DIS_BY_AUTO|CONTROL_INFO_SP_AUTOUPDATA));
				}
				*/
			// hemonel 2011-04-26: delete these code because NF calibration use thermal sensor
			/*
			{
				if (g_wEnumerateNFValue == 0 )
				{
					EnterCritical();
					byCal_N = PHYRegisterRead(0xB7);
					byCal_F = PHYRegisterRead(0xC7);

					ASSIGN_INT (g_wEnumerateNFValue, byCal_N, byCal_F);
					ExitCritical();
				}
			}
			*/
#ifdef EP0_LPM_L1_TEST
		Delay(100);
#endif			
			EP0_HSK();
		}
		break;  // case 0x01
	default: // return a Request Error
		STALL_EPCTL(); // return a STALL handshake
		break;
	}
}

#ifndef _BULK_ENDPOINT_
/*
*********************************************************************************************************
*                                          USB video streaming interface setting process
* FUNCTION VideoStreamIfSettingProc
*********************************************************************************************************
*/
/**
  Set the video streaming interface. Enable endpoint, config isp and sensor.

  \param None

  \retval None
 *********************************************************************************************************
*/

void VideoStreamIfSettingProc(U8 IFSetting)
{
	U16 wTmp;

	//2010-03-25 hemonel: when disable gain function, manual white balance USB_IF UVCTest fail.
	WhiteBalanceTempAutoItem.Last = g_byWhiteBalanceAutoLast_BackForUVCtest;
	if(WhiteBalanceTempAutoItem.Last ==0)
	{
		Ctl_WhiteBalanceTemp.Info &=(~(CONTROL_INFO_DIS_BY_AUTO|CONTROL_INFO_SP_AUTOUPDATA));
	}
	else
	{
		Ctl_WhiteBalanceTemp.Info |= (CONTROL_INFO_DIS_BY_AUTO|CONTROL_INFO_SP_AUTOUPDATA);
	}

	if(0==IFSetting)
	{
#if (defined _UVC_PPWB_ || defined _UVC_COMMITWB_)
		ParaWriteBackToFlash();
#endif

		//altera setting 0 , should stop sensor and ISP module.
		if(g_bySensorIsOpen == SENSOR_OPEN)
		{
			StopImageTransfer(EN_DELAYTIME);
		}
		XBYTE[EPA_CTL] &= (~EPA_PACKET_EN);
		XBYTE[EPA_CTL] |= EPA_FIFO_FLUSH;
		XBYTE[EPA_IRQEN] = 0x00;
		XBYTE[EPA_IRQSTAT] = 0xff;
		XBYTE[EPA_CFG] &= (~EPA_EN);
#ifdef _LENOVO_IDEA_EYE_
		g_byUSBStreaming=0;
		if (g_byMTDDetectBackend)
		{		
			StartVideoBackend();		
			g_wMTDTimeOutCnt = 500;
		}	
#endif			
	}
	else
	{
		wTmp= GetMaxPacketSize(IFSetting, (U8)g_bIsHighSpeed);
		XBYTE[EPA_MAXPKT0] = INT2CHAR(wTmp, 0);
		XBYTE[EPA_MAXPKT1] = INT2CHAR(wTmp, 1);

		XBYTE[EPA_CTL] &= (~EPA_PACKET_EN);
		XBYTE[EPA_CTL] |= EPA_FIFO_FLUSH;
		XBYTE[EPA_IRQEN] = EPA_IRQ_IN_TOKEN;	// Enable EPA in-token interrupt
		XBYTE[EPA_IRQSTAT] = 0xff;
		XBYTE[EPA_CTL] = EPA_PACKET_EN|EPA_FIFO_FLUSH;
		XBYTE[EPA_CFG] |= (EPA_EN);

#ifdef _WIN81METRO_BALCKSCREEN_
		StartVideoImage(EN_USB_STREAMING);
#endif

		// hemonel 2009-11-23: add for download firmware to indicate stage
		g_wLEDToggleInterval =0;	// not toggle

		// hemonel 2009-11-25: move LED on to StartImageTransfer for fix Chapter9 test LED on bug
//		TURN_LED_ON();
	}

//	EP0_HSK();	// hemonel 2010-05-31: move to call function
	return;
}
#endif

#ifdef _UAC_EXIST_
/*
*********************************************************************************************************
*                                          USB audio streaming interface setting process
* FUNCTION AudioStreamIfSettingProc
*********************************************************************************************************
*/
/**
  Set the audio streaming interface. Enable endpoint, config isp and sensor.

  \param None

  \retval None
 *********************************************************************************************************
*/

void AudioStreamIfSettingProc(U8 IFSetting)
{

	if(0==IFSetting)
	{
		//stop audio data trasfer.
		XBYTE[DMIC_CLK_DETECT_CTL] = 0x10;

		//only support Dmic to UAC mode	20110223, darcy_lu
//		if (g_byMICMode == ADC2UAC_EN)
//		{
//			XBYTE[MIC_ADC_CTL_L] = 0x25;
// 			XBYTE[PG_GPIO_AL_CTRL0] &= ~GPIO_AL0_DRIVING_HIGH;
//		}
		XBYTE[ADF_CFG] &= ADF_CHN_SWITCH;		//clear ADF_CFG setting
		XBYTE[EPB_FLOW_CTL] &= (~EPB_TRANSFER_EN);
		XBYTE[EPB_CTL] |= EPB_FIFO_FLUSH;
		XBYTE[EPB_IRQEN] = 0x00;
		XBYTE[EPB_IRQSTAT] = 0xff;
		XBYTE[EPB_CFG] &= (~EPB_EN);
	}
	else
	{
		//start audio data transfer, must init some
//				wTmp= GetMaxPacketSize(IFSetting, (U8)g_bIsHighSpeed);
		XBYTE[DMIC_CLK_DETECT_CTL] = 0x11;
		//only support Dmic to UAC mode	20110223, darcy_lu
//		if (g_byMICMode == ADC2UAC_EN)
//		{
//			XBYTE[MIC_ADC_CTL_L] = 0xA5;
//			XBYTE[PG_GPIO_AL_CTRL0] |= GPIO_AL0_DRIVING_HIGH;
//		}
//		if (g_bIsHighSpeed == 1)
		{
			XBYTE[EPB_MAXPKT0] = ( g_waUACMaxPktSize[IFSetting-1] ) & 0xFF;
			XBYTE[EPB_MAXPKT1] = ( g_waUACMaxPktSize[IFSetting-1] ) >> 8;

			XBYTE[EPB_FLOW_CTL] &= (~EPB_TRANSFER_EN);
			XBYTE[ADF_CFG] &= ADF_CHN_SWITCH;		//clear ADF_CFG setting
			XBYTE[ADF_CTRL0] &= 0xFC;	//clear channel select bits

			switch (IFSetting)
			{	
			case ALT_SETTING_IDX_22K05_16BIT:
				XBYTE[ADF_CFG] |= ADF_CLK_EN|AD_DATA_WIDTH_16|ADF_22K05_CLK;
				XBYTE[ADF_CTRL0] |= BOTH_CHANNEL_SEL;
				break;
				
			case ALT_SETTING_IDX_32K_16BIT:
				XBYTE[ADF_CFG] |= ADF_CLK_EN|AD_DATA_WIDTH_16|ADF_32K_CLK;
				XBYTE[ADF_CTRL0] |= BOTH_CHANNEL_SEL;
				break;
				
			case ALT_SETTING_IDX_48K_16BIT_MONO:
				XBYTE[ADF_CFG] |= ADF_CLK_EN|AD_DATA_WIDTH_16|ADF_48K_CLK;
				XBYTE[ADF_CTRL0] |= MONO_CHANNEL_SEL;
				break;
				
			case ALT_SETTING_IDX_48K_16BIT:
				XBYTE[ADF_CFG] |= ADF_CLK_EN|AD_DATA_WIDTH_16|ADF_48K_CLK;
				XBYTE[ADF_CTRL0] |= BOTH_CHANNEL_SEL;
				break;

			case ALT_SETTING_IDX_96K_16BIT:
				XBYTE[ADF_CFG] |= ADF_CLK_EN|AD_DATA_WIDTH_16|ADF_96K_CLK;
				XBYTE[ADF_CTRL0] |= BOTH_CHANNEL_SEL;
				break;
				
			case ALT_SETTING_IDX_44K1_24BIT:
				XBYTE[ADF_CTRL0] |= BOTH_CHANNEL_SEL;
				XBYTE[ADF_CFG] |= ADF_CLK_EN|AD_DATA_WIDTH_24|ADF_44K1_CLK;
				break;

			case ALT_SETTING_IDX_48K_24BIT:
				XBYTE[ADF_CTRL0] |= BOTH_CHANNEL_SEL;
				XBYTE[ADF_CFG] |= ADF_CLK_EN|AD_DATA_WIDTH_24|ADF_48K_CLK;
				break;
				
			case ALT_SETTING_IDX_96K_24BIT:
				XBYTE[ADF_CTRL0] |= BOTH_CHANNEL_SEL;
				XBYTE[ADF_CFG] |= ADF_CLK_EN|AD_DATA_WIDTH_24|ADF_96K_CLK;
				break;
				
			default:
				XBYTE[ADF_CTRL0] |= BOTH_CHANNEL_SEL;
				XBYTE[ADF_CFG] |= ADF_CLK_EN|AD_DATA_WIDTH_24|ADF_48K_CLK;
				break;
			}
		}
//		else
//		{
//			XBYTE[EPB_MAXPKT0] = 0x50;
//			XBYTE[EPB_MAXPKT1] = 0x00;
//			XBYTE[EPB_CTL] &= (~EPB_PACKET_EN);
//			XBYTE[ADF_CFG] |= ADF_CLK_EN|AD_DATA_WIDTH_16|ADF_8K_CLK;
//		}

		Delay(g_wEPB_ADF_delay);				//delay 50ms for ADF initialization
		
		XBYTE[EPB_CTL] |= EPB_FIFO_FLUSH;

		if (g_bIsHighSpeed == 1)
		{
			XBYTE[ADF_CFG] &=(~ADF_FULL_SPEED);
		}
		else
		{
			XBYTE[ADF_CFG] |=ADF_FULL_SPEED;
		}
		XBYTE[EPB_IRQEN] = EPB_IRQ_IN_TOKEN;
		XBYTE[EPB_IRQSTAT] = 0xff;
		XBYTE[EPB_CFG] = EPB_EN |EP_IDX_AUDIO_STREAM;
	}

//	EP0_HSK();	// hemonel 2010-05-31: move to call function
	return;
}
#endif

/*
*********************************************************************************************************
*                                          USB Set Interface Requests  Process
* FUNCTION USBSetIfReqProc
*********************************************************************************************************
*/
/**
  Respond to the Get Descriptors  requests from host and complete the related
  operations. The response is send to host mainly thru endpoint 0. If encountering the requests not
  supported the corresponding endpoint would be stalled as well as the operations when an error occurs.
  It is called by main module ISR while a setup packet interrupt occurs.

  \param None

  \retval None
 *********************************************************************************************************
*/
void USBSetIfReqProc()
{
	U16 data setting;
	U16 data If_idx;

	ASSIGN_INT(setting,XBYTE[SETUP_PKT_wVALUE_H],XBYTE[SETUP_PKT_wVALUE_L]);
	ASSIGN_INT(If_idx,XBYTE[SETUP_PKT_wINDEX_H],XBYTE[SETUP_PKT_wINDEX_L]);

	switch (If_idx)
	{
	case IF_IDX_VIDEOCONTROL:
#ifdef _UAC_EXIST_
	case IF_IDX_AUDIOCONTROL:
#endif
#ifdef _HID_BTN_
		case IF_IDX_HID:
#endif
		if(setting==0)  //valid  interface setting 0, handshake
		{
			EP0_HSK();
			return;
		}
		break;
	case IF_IDX_VIDEOSTREAMING:
#ifdef _BULK_ENDPOINT_
		// hemonel 2012-04-13: fix linux preview bug
		//					linux bulk driver will use set interface 0 to stop preview
		//					windows bulk driver will use clear feature to stop preview
		//					so copy clear feature code to set interface 0 for linux bulk camera preview
		if (setting == 0)
		{
#if (defined _UVC_PPWB_ || defined _UVC_COMMITWB_)
			ParaWriteBackToFlash();
#endif
			if(g_bySensorIsOpen == SENSOR_OPEN)
			{
				StopImageTransfer(EN_DELAYTIME);
			}

			XBYTE[EPA_CTL] = EPA_PACKET_EN|EPA_FIFO_FLUSH;
			XBYTE[EPA_IRQEN] = EPA_IRQ_IN_TOKEN;	// Enable EPA in-token interrupt
			XBYTE[EPA_IRQSTAT] = 0xff;
			
			g_byVideoStrmIFSetting=setting;	// for USBGetIfReqProc
			EP0_HSK();	// hemonel 2010-05-31: move to call function
			return;
		}
#else
		if (setting<=VS_IF_MAX_SETTING-1)  //valid  interface setting, handshake
		{
			// hemonel 2010-05-31 optimize: delete
			//	if((g_bDLFWFlag)&&(setting!=0))// download status, only setting 0 valid.
			//	{
			//		break;
			//	}
			VideoStreamIfSettingProc(setting);
			g_byVideoStrmIFSetting=setting;	// for USBGetIfReqProc
			EP0_HSK();	// hemonel 2010-05-31: move to call function
			return;
		}
#endif
		break;
#ifdef _UAC_EXIST_
		// hemonel 2010-05-31 optimize: merge code with video
//		case IF_IDX_AUDIOCONTROL:
//			if(setting==0)  //valid  interface setting 0, handshake
//			{
//				EP0_HSK();
//				return;
//			}
//			break;
	case IF_IDX_AUDIOSTREAMING:
		DBG_UAC(("Set Audio Streaming Interface %d Requeset. \n", If_idx));
		DBG_UAC(("Altr setting  %d. \n", setting));
		// do some init here
		AudioStreamIfSettingProc(setting);
		g_byAudioStrmIFSetting=setting;	// for USBGetIfReqProc
		EP0_HSK();	// hemonel 2010-05-31: move to call function
		return;
		//break;
#endif
	default:
		break;
	}


	STALL_EPCTL(); // return a STALL handshake
	return;

}
/*
*********************************************************************************************************
*                                          USB Get Interface Requests  Process
* FUNCTION USBGetIfReqProc
*********************************************************************************************************
*/
/**
  Respond to the Get Descriptors  requests from host and complete the related
  operations. The response is send to host mainly thru endpoint 0. If encountering the requests not
  supported the corresponding endpoint would be stalled as well as the operations when an error occurs.
  It is called by main module ISR while a setup packet interrupt occurs.

  \param None

  \retval None
 *********************************************************************************************************
*/
void USBGetIfReqProc()
{
	U16 If_idx;
	ASSIGN_INT(If_idx,XBYTE[SETUP_PKT_wINDEX_H],XBYTE[SETUP_PKT_wINDEX_L]);
	if(If_idx==IF_IDX_VIDEOCONTROL)
	{
		EP0_DATA_IN(0x00);
		EP0_FFV_HSK();
	}
	else if(If_idx==IF_IDX_VIDEOSTREAMING)
	{
		EP0_DATA_IN(g_byVideoStrmIFSetting);
		EP0_FFV_HSK();
	}
#ifdef _UAC_EXIST_
	else if(If_idx==IF_IDX_AUDIOCONTROL)
	{
		EP0_DATA_IN(0x00);
		EP0_FFV_HSK();
	}
	else if(If_idx==IF_IDX_AUDIOSTREAMING)
	{
		EP0_DATA_IN(g_byAudioStrmIFSetting);
		EP0_FFV_HSK();
	}
#endif
#ifdef _HID_BTN_
	else if(If_idx==IF_IDX_HID)
	{
		EP0_DATA_IN(0x00);
		EP0_FFV_HSK();		
	}
#endif
	else
	{
		STALL_EPCTL();
	}
}
/*
*********************************************************************************************************
*                                          USB Get Interface Requests  Process
* FUNCTION USBGetIfReqProc
*********************************************************************************************************
*/
/**
  Respond to the Get Descriptors  requests from host and complete the related
  operations. The response is send to host mainly thru endpoint 0. If encountering the requests not
  supported the corresponding endpoint would be stalled as well as the operations when an error occurs.
  It is called by main module ISR while a setup packet interrupt occurs.

  \param None

  \retval None
 *********************************************************************************************************
*/
void USBGetStsReqProc()
{
	U16 count;
	U8 byTemp;
	switch (XBYTE[SETUP_PKT_bmREQUST_TYPE])
	{
			//=========================================
		case 0x80 : //Device
			ASSIGN_INT(count,XBYTE[SETUP_PKT_wINDEX_H],XBYTE[SETUP_PKT_wINDEX_L]);
			if (!count)  // wINDEX must be 0
			{
				//  Support remote wakeup
				if ((XBYTE[USBCTL] & USB_RMTWKUP_EN) /*&& (g_bySpRemoteWakeUp== 1)*/)
				{
				// for CV Test
					EP0_DATA_IN(0X03);; // LSB, Self Powerd
					//EP0_DATA_IN(0X02);	// LSB, BUS Powerd
				}
				else
				{
					if( ( 1 == g_byHostEnableRemoteWakeup ) && (g_byRemoteWakeupSupport == 1))
					{
						byTemp = DEV_STATUS_REMOTE_WAKEUP; // "No Self-Power" and "Remote Wakeup"
					}
					else
					{
						byTemp = 0x00; // "No Self-Power" and "No Remote Wakeup"
					}				
					// for CV Test
					//EP0_DATA_IN(0X01);; // LSB, Self Powerd                   
					EP0_DATA_IN(byTemp);  // LSB, BUS Powerd
				}
				EP0_DATA_IN(0X00);
				EP0_FFV_HSK();
			}
			else
			{
				STALL_EPCTL();
			}
			break;
			//=========================================
		case 0x81 : //Interface
			//?? check if configured
			// only one interface
			EP0_DATA_IN(0X00);
			EP0_DATA_IN(0X00);
			EP0_FFV_HSK();
			break;
			//=========================================
		case 0x82 : //Endpoint
			switch (XBYTE[SETUP_PKT_wINDEX_L])
			{
			case 0x80 : //Endpoint 0
				//?? check if configured
				if (EP0_TST_STALL()) // endpoint Halt
				{
					EP0_DATA_IN(0X01);
				}
				else
				{
					EP0_DATA_IN(0X00);
				}
				EP0_DATA_IN(0X00);
				EP0_FFV_HSK();
				break;
			case (0x80|EP_IDX_VIDEO_INT) : //video int in ep
				//?? check if configured
				if (EPC_TST_STALL()) // endpoint Halt
				{
					EP0_DATA_IN(0X01);
				}
				else
				{
					EP0_DATA_IN(0X00);
				}
				EP0_DATA_IN(0X01);
				EP0_FFV_HSK();
				break;
#ifdef _BULK_ENDPOINT_
			case (0x80|EP_IDX_VIDEO_STREAM) : //video int in ep
				//?? check if configured
				if (EPA_TST_STALL()) // endpoint Halt
				{
					EP0_DATA_IN(0X01);
				}
				else
				{
					EP0_DATA_IN(0X00);
				}
				EP0_DATA_IN(0X01);
				EP0_FFV_HSK();
				break;
#endif
#ifdef _HID_BTN_
				case (0x80|EP_IDX_HID_INT) : //video int in ep 
					if (EPD_TST_STALL()) // endpoint Halt
					{
						EP0_DATA_IN(0X01);  
					}
					else
					{
						EP0_DATA_IN(0X00);      
					}
					EP0_DATA_IN(0X01);  
					EP0_FFV_HSK();
					break;
#endif
			default :
				STALL_EPCTL();// return a STALL handshake
				break;
			}
			break;// end of switch for endpoint
			//=========================================
		default :   // invalid request type
			STALL_EPCTL();// return a STALL handshake
			break;
	}
	return;
}






/*
*********************************************************************************************************
*                                          USB Get Configuration Requests  Process
* FUNCTION USBGetCfgReqProc
*********************************************************************************************************
*/
void USBGetCfgReqProc()
{
	// only two possible configuration values: 0 and 1
	EP0_DATA_IN(g_byConfigurationValue);
	EP0_FFV_HSK();
	return;
}

/*
*********************************************************************************************************
*                                          USB Test mode  Requests  Process
* FUNCTION USBTestReqProc
*********************************************************************************************************
*/
void USBTestReqProc()
{
	U8 data i;
	U16 data wWaitTime = 0;
	U16 wNFCalValue;
	U8 byCal_N;
	U8 byCal_F;
	float fpll_enumer;
	float fpll_tmp;
	U8 by_thermal_tmp;
	float fb_tmp;
	U16 wEnumerateNFValue;
	U8 byEnumerateThermalValue;

	if(!(XBYTE[SETUP_PKT_wINDEX_L]))  //lower byte of wIndex is 0
	{
		if((XBYTE[SETUP_PKT_wINDEX_H]<5)&&(XBYTE[SETUP_PKT_wINDEX_H]>0))//valid test.
		{

		}
		switch (XBYTE[SETUP_PKT_wINDEX_H])
		{

		case 0x01 : //Test_J
		case 0x02 : //Test_K

			EP0_HSK();
			//wait Hand shake complete.
			WaitTimeOut(EP0_STAT,EP0_STS_END,1,30);
			PHYRegisterWrite(0x00, 0xC3); //Disable SIE low power mode.

			uDelay(5);
			if(0x01== XBYTE[SETUP_PKT_wINDEX_H])
			{
				XBYTE[USBTEST] = 0x02; // Test_J
			}
			else //Test_K
			{
				XBYTE[USBTEST] = 0x03; // Test_K
			}
			while (1) {}
			//break;
		case 0x03 : //Test_SE0_NAK
			EP0_HSK();
			WaitTimeOut(EP0_STAT,EP0_STS_END,1,30);
			PHYRegisterWrite(0x00, 0xC3); //Disable SIE low power mode.
			uDelay(5);
			XBYTE[USBTEST] = 0x01; // Test_SE0_NAK
			while (1) {}
			//break;
		case 0x04 : //Test_Packet
#ifdef _TEST_PHY_GAIN_
			PHYDebug();	// hemonel 2009-07-20: only for singal rate test debug at usb if test packet
#endif
#ifdef _OSC_RC_MODE_
			CacheSpeed(MCU_CLOCK_30M);
 			// RC->LC
			// 1) adjust LC frequency 2.0M
			// 2) switch RC to LC
			// 3) Delay 5~10ms		
			//PHYRegisterWrite(0xE3, 0xC0);	
			PHYRegisterWrite(225, 0xD0);
			g_fThermalCofe = (g_fThermalCofe*214)/225;

			//TURN_LED_ON();
			
			// RC switch to LC
			PHYRegisterWrite(0x0B, 0xC1);		// LC enable
			XBYTE[RC_OSC_CFG] =0x21;		// RC disselect
			XBYTE[RC_OSC_CFG] =0x20;		// RC disable

			XBYTE[FREQ_TOP_CONFIG_12] = 0x01;   //Reset Frequency top 
			XBYTE[FREQ_TOP_CONFIG_12] = 0x00;
			for (i=0;i<50;i++)
			{
				byCal_N = PHYRegisterRead(0xB7);
				byCal_F = PHYRegisterRead(0xC7);
				ASSIGN_INT (wEnumerateNFValue, byCal_N, byCal_F);
#ifdef _RS232_DEBUG_
				printf("%bu n = %bx, f=%bx\n",i,byCal_N,byCal_F);
#endif
				Delay(1);
			}                		
#endif		


			// save enumeration stage NF value and temperature for non-xtal LC calibration
			XBYTE[THERMAL_SENSOR_CTL] = 0x07;
			XBYTE[THERMAL_SENSOR_CFG] = 0x00;
			Delay(100);
			EnterCritical();
			byCal_N = PHYRegisterRead(0xB7);
			byCal_F = PHYRegisterRead(0xC7);
			ASSIGN_INT (wEnumerateNFValue, byCal_N, byCal_F);
			ExitCritical();
			byEnumerateThermalValue = XBYTE[THERMAL_SENSOR_TEMPE];
			XBYTE[THERMAL_SENSOR_CTL] = 0x00;
			// calculate the calibration equation coefficient b
			fpll_enumer = 480000000/((float)wEnumerateNFValue);
			fb_tmp = fpll_enumer - g_fThermalCofe * ((float)byEnumerateThermalValue);

			// hemonel 2009-04-17: handshake before calibrate
			EP0_HSK(); // Status stage of the control transfer
			WaitTimeOut(EP0_STAT,EP0_STS_END,1,30);

			EP0_FFFLUSH();
			XBYTE[EP0_CTL] = 0;
			PHYRegisterWrite(0x00, 0xC3); //Disable SIE low power mode.

			if((0x20&PHYRegisterRead(0x80))==0x20)   // non-crystal mode enable offset calibration. otherwise ,disable offset.
			{
				XBYTE[FREQ_TOP_CONFIG_8] = 0x08;
			}
			else
			{
				XBYTE[FREQ_TOP_CONFIG_8] = 0x00;
			}

			//	Add for eye diagram gauge	2006-7-18 9:22
			//	Delay for hold off time
			PHYRegisterWrite(0xF4, 0xF0);
			XBYTE[UTMI_TUNE] |= 0x06;

			for ( i = 0; i < sizeof(USBTestPacket); i++)
			{
				EP0_DATA_IN(USBTestPacket[i]);
			}

			_nop_();
			_nop_();
			_nop_();
			XBYTE[USBTEST] = 0x04; // Test_Packet

			// While Sent Test_Packet, There will be calibration N.F Value
			// N.F calibration with the thermal sensor calibration equation 1/NF=k*Temperature+b + global offset
			EnterCritical();
			g_wTimerCounterForUSBIF = 0;
			ExitCritical();
			XBYTE[THERMAL_SENSOR_CTL] = 0x07;
			Delay(10);

			while (1)
			{
				wWaitTime = g_wTimerCounterForUSBIF;
				if((wWaitTime % NF_CALABRITION_INTERVAL) == 0)
				{
					by_thermal_tmp = XBYTE[THERMAL_SENSOR_TEMPE];
					fpll_tmp= g_fThermalCofe*((float)by_thermal_tmp) + fb_tmp + g_byThermalCalibrationOffset;

					wNFCalValue = (U16)(480000000/fpll_tmp);
					XBYTE[FREQ_TOP_CONFIG_10] 	= INT2CHAR(wNFCalValue,0);//g_byCal_F;
					XBYTE[FREQ_TOP_CONFIG_9] 	= INT2CHAR(wNFCalValue, 1);//g_byCal_N;
				}
			}
			//break;
		default :// invalid test mode selector
			STALL_EPCTL(); // return a STALL handshake
			break;
		} // end of switch SETUP_PKT_wINDEX_H
	} // end of if
	else
	{
		STALL_EPCTL(); // return a STALL handshake
	}

	return;
}



/*
*********************************************************************************************************
*                                          USB set feature Requests  Process
* FUNCTION USBSetFeatureReqProc
*********************************************************************************************************
*/
void USBSetFeatureReqProc()
{
	switch (XBYTE[SETUP_PKT_bmREQUST_TYPE])
	{
	case 0x00 : //Device
		switch (XBYTE[SETUP_PKT_wVALUE_L])
		{
		case 0x01 : //Remote Wakeup
			//	HIGH remote wakeup
			if(g_byRemoteWakeupSupport == 1)
			{
				g_byHostEnableRemoteWakeup = 1;
			}
			EP0_HSK();				
			break;
		case 0x02 : //Test Mode
			USBTestReqProc();
			break;
		default :
			STALL_EPCTL();// return a STALL handshake
			break;
		} // end of switch SETUP_PKT_wVALUE_L
		break;  //  Break of case 0x00
	case 0x01 : //Interface
		// do nothing
		STALL_EPCTL(); // return a STALL handshake
		break;
	case 0x02 : //Endpoints
		switch (XBYTE[SETUP_PKT_wINDEX_L])
		{
		case (0x80|EP_IDX_VIDEO_INT): //video int in ep
			EPC_STALL();
			EP0_HSK();
			break;
			// hemonel 2010-05-31 optimize: video iso in endpoint do nothing
#ifdef _BULK_ENDPOINT_
		case (0x80|EP_IDX_VIDEO_STREAM): // video stream in ep
			EPA_STALL();
			EP0_HSK();
			break;
#endif
#ifdef _HID_BTN_
		case (0x80|EP_IDX_HID_INT): //video int in ep             
			EPD_STALL();
			EP0_HSK();
			break;
 #endif
		default : // invalid command
			STALL_EPCTL();// return a STALL handshake
			break;
		} // end of switch SETUP_PKT_wINDEX_L
		break;
	default : // invalid command
		STALL_EPCTL();
		break;
	}
}

/*
*********************************************************************************************************
*                                          USB clear feature Requests  Process
* FUNCTION USBClrFeatureReqProc
*********************************************************************************************************
*/
void USBClrFeatureReqProc()
{
	switch (XBYTE[SETUP_PKT_bmREQUST_TYPE])
	{
	case 0 : // Device
		{
			switch (XBYTE[SETUP_PKT_wVALUE_L])
			{
			case 0x01 : //Remote Wakeup
				//  Support remote wakeup
				XBYTE[USBCTL] &= ~USB_RMTWKUP_EN;   //  Disable remote wakeup
				EP0_HSK();
				break;
			default:
				STALL_EPCTL();
				break;
			}
			break;
		}
	case 0x01 : // Interface
		STALL_EPCTL();// return a STALL handshake
		break;
	case 0x02 : // Endpoint
		if (XBYTE[SETUP_PKT_wVALUE_L] == ENDPOINT_HALT)
		{
			switch (XBYTE[SETUP_PKT_wINDEX_L])
			{
			case (EP_IDX_VIDEO_INT|0x80) : // video int in ep
				//	XBYTE[EP_IDX] =(0x0f&XBYTE[SETUP_PKT_wINDEX_L]);
				//	XBYTE[EP_CTL] = FIFO_FLUSH | NAK_OUT;
				EPC_FFFLUSH();	// clear stall
				EP0_HSK();
				break;
#ifdef _HID_BTN_
			case (EP_IDX_HID_INT|0x80) : // video int in ep
				EPD_FFFLUSH();  // clear stall                  
				EP0_HSK();
				break;
#endif
				// hemonel 2010-05-31 optimize: video iso in endpoint do nothing
#ifdef _BULK_ENDPOINT_
			case (EP_IDX_VIDEO_STREAM|0x80) : // video iso in ep
#ifdef _EP0_LPM_L1_
				XBYTE[ISP_CONTROL0] = ISP_STOP;
				XBYTE[ISP_SIE_CTRL] = SIE_STOP;
				XBYTE[EPA_CTL] = EPA_FIFO_FLUSH;					//Jimmy.20120906.If epa fifo have data to transfer,,device will NYET LPM L1
				if (g_byCurFormat == FORMAT_TYPE_MJPG)
				{
					XBYTE[EPA_CFG] &= ~EPA_JPEG_MODE_BURST;	//Jimmy.20120906.When preview MJPEG,HW detect ffd9 will set a status which need to send a new transfer
				}												//Then devie will always NYET LPM L1, this bit to stop ffd9 replace and clear this status
#endif

#if (defined _UVC_PPWB_ || defined _UVC_COMMITWB_)
				ParaWriteBackToFlash();
#endif
				if(g_bySensorIsOpen == SENSOR_OPEN)
				{
					StopImageTransfer(EN_DELAYTIME);
				}
					
				// hemonel 2011-12-01: not need reconfigure EPA MAX PACKET SIZE
				//	XBYTE[EPA_MAXPKT0] = 0x00;
				//	XBYTE[EPA_MAXPKT1] = 0x02;				
				XBYTE[EPA_CTL] = EPA_STALL_EP; // init data sequence data0 and disable packet enable
				XBYTE[EPA_CTL] = EPA_PACKET_EN|EPA_FIFO_FLUSH;	
				XBYTE[FORCE_TOGGLE] = FORCE_EPA_DATA0;			//Force EPA data sequence data0			
				XBYTE[EPA_IRQEN] = EPA_IRQ_IN_TOKEN;	// Enable EPA in-token interrupt
				XBYTE[EPA_IRQSTAT] = 0xff;

#ifdef EP0_LPM_L1_TEST
				Delay(1000);
#endif
				EP0_HSK();
				break;
#endif	//_BULK_ENDPOINT_	

			default :
				STALL_EPCTL();
				break;
			} // end of switch SETUP_PKT_wINDEX_L

		} // end of if
		else
		{
			STALL_EPCTL();
		}
		break;
	default :
		STALL_EPCTL();
		break;
	}
}

/*
*********************************************************************************************************
*                                          USB Standard Requests  Process
* FUNCTION USBStdReqProc
*********************************************************************************************************
*/
/**
  Respond to the standard requests from host and complete the related
  operations. The response is send to host mainly thru endpoint 0. If encountering the requests not
  supported the corresponding endpoint would be stalled as well as the operations when an error occurs.
  It is called by main module ISR while a setup packet interrupt occurs.

  \param None

  \retval None
 *********************************************************************************************************
*/
void USBStdReqProc()
{
	switch (XBYTE[SETUP_PKT_bREQUEST])
	{
	case USB_REQUEST_GET_DESCRIPTOR:
		USBGetDescReqProc();
		break;
	case USB_REQUEST_SET_ADDRESS:
		USBSetAdrReqProc();
		break;
	case USB_REQUEST_SET_CONFIGURATION:
		USBSetCfgReqProc();
		break;
	case USB_REQUEST_GET_INTERFACE :
		USBGetIfReqProc();
		break;
	case USB_REQUEST_SET_INTERFACE :
		USBSetIfReqProc();
		break;
	case USB_REQUEST_GET_STATUS :
		USBGetStsReqProc();
		break;
	case USB_REQUEST_GET_CONFIGURATION :
		USBGetCfgReqProc();
		break;
	case USB_REQUEST_SET_FEATURE :
		USBSetFeatureReqProc();
		break;
	case USB_REQUEST_CLEAR_FEATURE:
		USBClrFeatureReqProc()	;
		break;
		//************************************************************
		// case Synch_Frame : not in CV

		//************************************************************
		// case Set_Descriptor : optional
	default:
		STALL_EPCTL();
		break;
	}
	return;
}

/*
*********************************************************************************************************
*                                          USB Class-specific Requests Process
* FUNCTION USBClsReqProc
*********************************************************************************************************
*/
/**
Respond to the  class requests from host and complete the related
operations. The response is send to host mainly thru endpoint 0. If encountering the requests not
supported the corresponding endpoint would be stalled as well as the operations when an error occurs.
It is called by main module ISR while a setup packet interrupt occurs.

\param None

\retval None
*********************************************************************************************************
*/
void USBClsReqProc()
{
	switch(XBYTE[SETUP_PKT_bREQUEST])
	{
	case SET_CUR :
	case GET_CUR :
	case GET_MIN :
	case GET_MAX :
	case GET_RES :
	case GET_LEN :
	case GET_INFO:
	case GET_DEF :
		VideoClsReqProc();
		break;
	default:
		STALL_EPCTL();
		break;
	}
	return;
}

/*
*********************************************************************************************************
*                                          USB Class-specific Requests Process
* FUNCTION USBClsReqProc
*********************************************************************************************************
*/
/**
Respond to the  vendor requests from host and complete the related
operations. The response is send to host mainly thru endpoint 0. If encountering the requests not
supported the corresponding endpoint would be stalled as well as the operations when an error occurs.
It is called by main module ISR while a setup packet interrupt occurs.

\param None

\retval None
*********************************************************************************************************
*/




void USBVendorReqProc()
{
#ifdef EP0_LPM_L1_TEST
		U16 wAddr,wLen;
		U8 byTmp;
		U8 data i;

		U8 bySubPara;
		U8 byRequestCmd;
		//U16 wValue;
#endif
	switch (XBYTE[SETUP_PKT_bREQUEST])
	{
	case USB_VDREQUEST_RESET:
		if(XBYTE[SETUP_PKT_wINDEX_L]==VSCMD_RST_2_UPDATEFW)
		{
			EP0_HSK();	
			switch(XBYTE[SETUP_PKT_wVALUE_L])
			{
				case 4:
					XBYTE[SCRATCH0]=SF_FAST_DWN_SPICS_DELINK_FLAG0;
					XBYTE[SCRATCH1]=SF_FAST_DWN_SPICS_DELINK_FLAG1;
#ifdef _PG_EN_	
					XBYTE[PG_FSM_RST] |= PG_STATE_RST;	//mantis[0002204]
#endif	
#ifdef _USB2_LPM_
					XBYTE[USB_LPM] = (LPM_SUPPORT | LPM_NAK);
#endif					
					//Jimmy.20131106.Fix mantis[0004470] fastdownload spi controller abnormal
					//XBYTE[AL_CACHE_CFG] = MCU_RESET_ONLY;
					PatchMCUReset();
					MCUReset();
					break;
				case 2:
					XBYTE[SCRATCH0]=SF_FAST_DOWNLOAD_FLAG0;
					XBYTE[SCRATCH1]=SF_FAST_DOWNLOAD_FLAG1;
#ifdef _PG_EN_	
					XBYTE[PG_FSM_RST] |= PG_STATE_RST;	//mantis[0002204]
#endif
#ifdef _USB2_LPM_
					XBYTE[USB_LPM] = (LPM_SUPPORT | LPM_NAK);
#endif	
					//Jimmy.20131106.Fix mantis[0004470] fastdownload spi controller abnormal
					//XBYTE[AL_CACHE_CFG] = MCU_RESET_ONLY;
					PatchMCUReset();					
					MCUReset();					
					break;
				case 0:
					XBYTE[SCRATCH0] = SF_DOWNLOAD_FLAG0;
					XBYTE[SCRATCH1] = SF_DOWNLOAD_FLAG1;
					WaitTimeOut_Delay(5);
					XBYTE[USBCTL] = USB_DISCONNECT;
					WaitTimeOut_Delay(10);
#ifdef _PG_EN_	
					XBYTE[PG_FSM_RST] |= PG_STATE_RST;	//mantis[0002204]
#endif					
					//Jimmy.20121205.Fix mantis[0002412]
					//mode_hs always 1, When DP go high,fw can not receive suspend interrupt
					XBYTE[UTMI_TST] = 0x42;	//Force PHY into normal and full speed mode
					XBYTE[UTMI_TST] = 0x00;	//Then Clear	
#ifdef _USB2_LPM_
					XBYTE[USB_LPM] = 0x00;	//Jimmy.20130326.Rom Code not support LPM L1
#endif							
					XBYTE[AL_BOOTCTL] = BOOT_FROM_ROM;  
					break;
				case 3:
					XBYTE[SCRATCH0] = SF_DWN_SPICI_DELINK_FLAG0;
					XBYTE[SCRATCH1] = SF_DWN_SPICI_DELINK_FLAG1;
					WaitTimeOut_Delay(5);
					XBYTE[USBCTL] = USB_DISCONNECT;
					WaitTimeOut_Delay(10);
#ifdef _PG_EN_	
					XBYTE[PG_FSM_RST] |= PG_STATE_RST;	//mantis[0002204]
#endif					
					//Jimmy.20121205.Fix mantis[0002412]
					//mode_hs always 1, When DP go high,fw can not receive suspend interrupt
					XBYTE[UTMI_TST] = 0x42;	//Force PHY into normal and full speed mode
					XBYTE[UTMI_TST] = 0x00;	//Then Clear		
#ifdef _USB2_LPM_
					XBYTE[USB_LPM] = 0x00;	//Jimmy.20130326.Rom Code not support LPM L1
#endif						
					XBYTE[AL_BOOTCTL] = BOOT_FROM_ROM;  
					break;
				default:
					break;
				}
		}
		else if(XBYTE[SETUP_PKT_wINDEX_L]==VSCMD_RST_2_NEWCODE)	//only valid for DL code .
		{
			EP0_HSK();
		}
		else // VSCMD_RST_2_ROM:
		{
			EP0_HSK();	
			WaitTimeOut_Delay(5);
			XBYTE[USBCTL] = USB_DISCONNECT;
			WaitTimeOut_Delay(5);
#ifdef _PG_EN_	
			XBYTE[PG_FSM_RST] |= PG_STATE_RST;	//mantis[0002204]
#endif				
			//Jimmy.20121205.Fix mantis[0002412]
			//mode_hs always 1, When DP go high,fw can not receive suspend interrupt
			XBYTE[UTMI_TST] = 0x42;	//Force PHY into normal and full speed mode
			XBYTE[UTMI_TST] = 0x00;	//Then Clear
#ifdef _USB2_LPM_
			XBYTE[USB_LPM] = 0x00;	//Jimmy.20130326.Rom Code not support LPM L1
#endif				
			XBYTE[AL_BOOTCTL] = BOOT_FROM_ROM;
		}
		return;
		//break;
	case USB_VDREQUEST_GET_STATUS:
		EP0_DATA_IN(g_byVendorReqStatus);
		EP0_FFV_HSK();
		break;
#ifdef EP0_LPM_L1_TEST
		case 0x25:
		byRequestCmd =0x00;
		bySubPara = XBYTE[SETUP_PKT_wVALUE_L];

		ASSIGN_INT(wAddr,XBYTE[SETUP_PKT_wINDEX_H],XBYTE[SETUP_PKT_wINDEX_L]);
		ASSIGN_INT(wLen,XBYTE[SETUP_PKT_wLEN_H], XBYTE[SETUP_PKT_wLEN_L]);

	    // wLen >0 -->Please add command here    
		if(wLen > 0)
		{
			while(wLen) //Here wLen!=0
			{
				if(wLen>0x40) //EP0 max packet size 
				{
					byTmp= 0x40;
				}
				else
				{
					byTmp =  (U8)wLen;
				}

				for(i=0;i<byTmp;i++)
				{
					{
						EP0_DATA_IN(i+byRequestCmd);
					}
				}
				wAddr+=byTmp;
				wLen -= byTmp;
				Delay(1000);
				EP0_FFV();

				if(!WaitTimeOut(EP0_CTL, EP0_FIFO_VALID, 0, 250))
				{
					USBProReset();

					// hemonel 2010-06-17: at toshiba NB BIOS self-detect, the time of get configure descriptor is short so that device don't send any data.
					// add handshake for SIE state restore
					EP0_HSK() ;
				}
				byRequestCmd = 0x40;
			
			}
			Delay(1000);
			EP0_HSK();
			return;   
		}		




			
			break;
				
		case 0x07:
			byRequestCmd = XBYTE[SETUP_PKT_bREQUEST];
		bySubPara = XBYTE[SETUP_PKT_wVALUE_L];

		ASSIGN_INT(wAddr,XBYTE[SETUP_PKT_wINDEX_H],XBYTE[SETUP_PKT_wINDEX_L]);
		ASSIGN_INT(wLen,XBYTE[SETUP_PKT_wLEN_H], XBYTE[SETUP_PKT_wLEN_L]);
		
			switch(bySubPara)
			{
				case 0:
					EP0_DATA_IN(_RT_VID_L_);    //VID
					EP0_DATA_IN(_RT_VID_H_);
					EP0_DATA_IN(_5840_PID_L_);       //PID
					EP0_DATA_IN(_5840_PID_H_);
					
					EP0_DATA_IN(_FW_SUB_VER_);  //FW Version
					EP0_DATA_IN(_FW_MAIN_VER_);
					EP0_DATA_IN(XBYTE[HW_VER_ID1]); //HW Version
					EP0_DATA_IN(XBYTE[HW_VER_ID2]);						
					break;
				case 3:
					for(i=0;i<8;i++)
					{
						EP0_DATA_IN(Qualification[i]);
					} 	
					break;
				case 8:
					for(i=0;i<8;i++)
					{
						EP0_DATA_IN(g_cVdFwVer[i]);//g_cVdFwVer
					} 	
					break;
				default:
					for(i=0;i<8;i++)
					{
						EP0_DATA_IN(0);
					} 	
					break;
			}	
			EP0_FFV_HSK();
			break;
	case 0x24:
		
		byRequestCmd = XBYTE[SETUP_PKT_bREQUEST];
		bySubPara = XBYTE[SETUP_PKT_wVALUE_L];

		ASSIGN_INT(wAddr,XBYTE[SETUP_PKT_wINDEX_H],XBYTE[SETUP_PKT_wINDEX_L]);
		ASSIGN_INT(wLen,XBYTE[SETUP_PKT_wLEN_H], XBYTE[SETUP_PKT_wLEN_L]);

	    // wLen >0 -->Please add command here    
		if(wLen > 0)
		{
			while(wLen) //Here wLen!=0
			{
				if(wLen>0x40) //EP0 max packet size 
				{
					byTmp= 0x40;
				}
				else
				{
					byTmp =  (U8)wLen;
				}

				if(!WaitTimeOut(EP0_STAT, EP0_FIFO_EMPTY, 0, 20)) //
				{
					STALL_EPCTL();
					g_byVendorReqStatus = VDREQ_STS_NO_DOUT;
					return ;	
				}

				Delay(100);

				//XBYTE[LPM_CFG] |= 0x80;	
				for(i=0;i<byTmp;i++)
				{
					EP0_DATA_OUT();
				}
				wAddr+=byTmp;
				wLen -= byTmp;
				
				XBYTE[LPM_CFG] |= 0x08;


				
			}
			Delay(100);
			EP0_HSK();
			return;   
		}
		break;
#endif		
#ifdef _UBIST_TEST_
	case USB_VDREQUEST_BIST_CMD:
		switch(XBYTE[SETUP_PKT_wVALUE_L])
		{
		case UBIST_CMD_START_TEST:
			g_byUbistTestProgress = UBIST_STAT_RUNNING | 0x00;
			g_byUbistTestReturnCode = 0x00;
			g_byUbistTestID = XBYTE[SETUP_PKT_wINDEX_L];
			EP0_HSK();
			break;
		case UBIST_CMD_ABORT:
			g_byUbistTestAbort = 1;
			EP0_HSK();
			break;
		case UBIST_CMD_ENABLE_BIST:
			g_byUbistTestEnable = 1;
			EP0_HSK();
			break;
		case UBIST_CMD_DISABLE_BIST:
			g_byUbistTestEnable = 0;
			EP0_HSK();
			break;
		case UBIST_CMD_GET_STATUS:
			EP0_DATA_IN(g_byUbistTestProgress);
			EP0_DATA_IN(g_byUbistTestReturnCode);
			EP0_FFV_HSK();
			break;
		default:
			break;
		}
		break;
#endif
	default:
		STALL_EPCTL();
		break;
	}
}

#ifdef _UBIST_TEST_
void UbistTest_SensorCommunication(void)
{

	U16  sensor_idx = 0x0000;
	sensor_idx = g_wSensor;

	//read sensor ID
	SoftInterruptOpenAftConnect(SF_IRQ_READ_SENSORID);

	if (sensor_idx != g_wSensor)
	{
		g_byUbistTestProgress &= ~UBIST_STAT_RUNNING;	// test finished
		g_byUbistTestReturnCode =UBIST_RC_ERROR;
		return;
	}

	g_byUbistTestProgress = 100;

	if((g_byUbistTestProgress & UBIST_STAT_PROGRESS_MASK)==100)
	{
		g_byUbistTestProgress &= ~UBIST_STAT_RUNNING;	// test finished
		g_byUbistTestReturnCode = UBIST_RC_PASS;	// pass
	}
}

void UbistTest_FlashChecking(void)
{

//	if ((  SF_DOWNLOAD_TAG0 != CBYTE[ (U32)CACHE_TAG_ADDR0]) ||(SF_DOWNLOAD_TAG1 != CBYTE[ (U32)CACHE_TAG_ADDR0 + 1]))
//	{
//		g_byUbistTestProgress &= ~UBIST_STAT_RUNNING;	// test finished
//		g_byUbistTestReturnCode =UBIST_RC_ERROR;
//		return;
//	}

	g_byUbistTestProgress = 100;

	if((g_byUbistTestProgress & UBIST_STAT_PROGRESS_MASK)==100)
	{
		g_byUbistTestProgress &= ~UBIST_STAT_RUNNING;	// test finished
		g_byUbistTestReturnCode = UBIST_RC_PASS;	// pass
	}
}

void UbistTest_Register(void)
{
	//U8	UbistTestRegisterValue;
	//UbistTestRegisterValue = XBYTE[HW_TIMER0_CNT0];
	//XBYTE[HW_TIMER0_CNT0] = 0x12;
	//if ( XBYTE[HW_TIMER0_CNT0] !=  0x12)
	//{
	//	g_byUbistTestProgress &= ~UBIST_STAT_RUNNING;	// test finished
	//	g_byUbistTestReturnCode =UBIST_RC_ERROR;
	//	return;
	//}
	//XBYTE[HW_TIMER0_CNT0] = UbistTestRegisterValue;


	g_byUbistTestProgress = 100;

	if((g_byUbistTestProgress & UBIST_STAT_PROGRESS_MASK)==100)
	{
		g_byUbistTestProgress &= ~UBIST_STAT_RUNNING;	// test finished
		g_byUbistTestReturnCode = UBIST_RC_PASS;	// pass
	}
}

void UbistTest_Controller(void)
{
	// test progress increase
	g_byUbistTestProgress = 100;

	if((g_byUbistTestProgress & UBIST_STAT_PROGRESS_MASK)==100)
	{
		g_byUbistTestProgress &= ~UBIST_STAT_RUNNING;	// test finished
		g_byUbistTestReturnCode = UBIST_RC_PASS;	// pass
	}
}

void UbistTest_Luminance(void)
{
	// test progress increase
	g_byUbistTestProgress = 100;

	if((g_byUbistTestProgress & UBIST_STAT_PROGRESS_MASK)==100)
	{
		g_byUbistTestProgress &= ~UBIST_STAT_RUNNING;	// test finished
		g_byUbistTestReturnCode = UBIST_RC_PASS;	// pass
	}
}

void UbistTest_BlinkLED(void)
{
	U8 i=0;
	for  (i = 0; i<16; i++)
	{
		LED_TOGGLE();
		Delay(60);
	}

	TURN_LED_OFF();

	g_byUbistTestProgress = 100;

	if((g_byUbistTestProgress & UBIST_STAT_PROGRESS_MASK)==100)
	{
		g_byUbistTestProgress &= ~UBIST_STAT_RUNNING;	// test finished
		g_byUbistTestReturnCode = UBIST_RC_PASS;	// pass
	}
}
#endif //_UBIST_TEST_

/*
*********************************************************************************************************
*                                          USB Setup Packets Process
* FUNCTION USBProSetupPKT
*********************************************************************************************************
*/
/**
  Respond to the standard requests, class requests and vendor requests from host and complete the related
  operations. The response is send to host mainly thru endpoint 0. If encountering the requests not
  supported the corresponding endpoint would be stalled as well as the operations when an error occurs.
  It is called by main module ISR while a setup packet interrupt occurs.

  \param None

  \retval None
*********************************************************************************************************
*/
void    USBProSetupPKT(void)
{
	switch (XBYTE[SETUP_PKT_bmREQUST_TYPE] & REQUEST_TYPE)
	{
	case STANDARD_REQUEST:
		USBStdReqProc();
		break;
	case CLASS_REQUEST:
		USBClsReqProc();
		break;
	case VENDOR_REQUEST:
		USBVendorReqProc();
		break;
	default: //unkown request
		STALL_EPCTL();
		break;
	}
	return;
}

