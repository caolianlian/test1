#include "Pc_cam.h"
#include "Camutil.h"
#include "CamReg.h"
#include "CamCtl.h"
#include "Global_vars.h"
#include "isp_vars.h"
#include "CamRtkExtIsp.h"
#include "CamAECPro.h"
#include "ISP_vars.h"
#ifdef _AF_ENABLE_
#include "CamAFPro.h"
#endif

#ifdef _RTK_EXTENDED_CTL_

#ifdef _USE_BK_SPECIAL_EFFECT_
void SetISPSpecailEffect(U16 wEffect)
{
	U8 byspecialeffect2 = 0x00;
	U8 data byUvalue = 0x80, byVvalue = 0x80;

	XBYTE[ISP_CONTROL2] &= ~ISP_SPECIAL_EFFECT_MASK;
	switch (wEffect)
	{
	case ISP_SPECIAL_EFFECT_MONOCHROME:
		byspecialeffect2 = ISP_MONO_EN;
		break;	
	case ISP_SPECIAL_EFFECT_GRAY:
		byspecialeffect2 =ISP_GRAY_EN;
		break;		
	case ISP_SPECIAL_EFFECT_NEGATIVE:
		byspecialeffect2 = ISP_NEGATIVE_EN;
		break;
	case ISP_SPECIAL_EFFECT_SEPIA:
		byspecialeffect2 = ISP_SPECIAL_EN;
		byUvalue = 0x40;
		byVvalue = 0xa0;
		break;
	case ISP_SPECIAL_EFFECT_GREENISH:
		byspecialeffect2 = ISP_SPECIAL_EN;
		byUvalue = 0x60;
		byVvalue = 0x60;
		break;
	case ISP_SPECIAL_EFFECT_REDDISH:
		byspecialeffect2 = ISP_SPECIAL_EN;
		byUvalue = 0x80;
		byVvalue = 0xc0;
		break;
	case ISP_SPECIAL_EFFECT_BLUISH:
		byspecialeffect2 = ISP_SPECIAL_EN;
		byUvalue = 0xa0;
		byVvalue = 0x40;
		break;
	default: //no effect
		break;
	}

	XBYTE[ISP_SPECIAL_TUNE] =0x20;
	XBYTE[ISP_SPECIAL_TARGET_U] = byUvalue;
	XBYTE[ISP_SPECIAL_TARGET_V] = byVvalue;

	XBYTE[ISP_CONTROL2] |=byspecialeffect2;
}
#endif	// _USE_BK_SPECIAL_EFFECT_

void BackupAETarget()
{
	g_byAEMeanTarget_Backup = g_byAEC_Mean_Target;
	g_byAEMeanTargetL_Backup = g_byAEC_Mean_Target_L;
	g_byAEMeanTargetH_Backup = g_byAEC_Mean_Target_H;
	g_byAEHistPosThH_Backup = g_byAEC_HistPos_Th_H;
	g_byAEHistPosThL_Backup = g_byAEC_HistPos_Th_L;
}

void SetEVCompensation(S16 wEVCompValue, S16 wEVCompRes)
{
	U8 bySteps;
	float fEVComp;

	if(wEVCompValue != RtkExtEVCompensationItem.Last)
	{
		switch(wEVCompRes)
		{
		case EVCOMP_SIXTHSTEP:
			bySteps = 6;
			break;
			
		case EVCOMP_QUARTERSTEP:
			bySteps = 4;
			break;
			
		case EVCOMP_THIRDSTEP:
			bySteps = 3;
			break;
			
		case EVCOMP_HALFSTEP:
			bySteps = 2;
			break;
			
		default:
			bySteps = 1;
			break;
		}

		fEVComp = pow(1.414214, ((float)wEVCompValue)/bySteps);

		g_byAEC_Mean_Target = (U8)ClipWord((U16)(g_byAEMeanTarget_Backup*fEVComp), 0, 255);
		g_byAEC_Mean_Target_L = (U8)ClipWord((U16)(g_byAEMeanTargetL_Backup*fEVComp), 0, 255);
		g_byAEC_Mean_Target_H = (U8)ClipWord((U16)(g_byAEMeanTargetH_Backup*fEVComp), 0, 255);
		g_byAEC_HistPos_Th_L = (U8)ClipWord((U16)(g_byAEHistPosThL_Backup*fEVComp), 0, 255);
		g_byAEC_HistPos_Th_H = (U8)ClipWord((U16)(g_byAEHistPosThH_Backup*fEVComp), 0, 255);
		g_fAEC_HistPos_LH_Target = pow(g_byAEC_HistPos_Th_L, g_fAEC_HighContrast_Exp_L) * pow(g_byAEC_HistPos_Th_H, 1.0f-g_fAEC_HighContrast_Exp_L);

		g_byAEForce_Adjust = 1;
	}
}

#ifdef _RTK_EXTENDED_CTL_
void SetISO(U16 wISOValue)
{
	switch(wISOValue)
	{
		case ISO_AUTO:
			g_fAEC_ISO_gain_min = g_fAEC_ISO_gain_max = 0;
			break;
		case ISO_50:
			g_fAEC_ISO_gain_min = 1;
			g_fAEC_ISO_gain_max = 2;
			break;
		case ISO_80:
			g_fAEC_ISO_gain_min = 1.6;
			g_fAEC_ISO_gain_max = 3.2;
			break;
		case ISO_100:
			g_fAEC_ISO_gain_min = 2;
			g_fAEC_ISO_gain_max = 4;
			break;
		case ISO_200:
			g_fAEC_ISO_gain_min = 4;
			g_fAEC_ISO_gain_max = 8;
			break;
		case ISO_400:
			g_fAEC_ISO_gain_min = 8;
			g_fAEC_ISO_gain_max = 16;
			break;
		case ISO_800:
			g_fAEC_ISO_gain_min = 16;
			g_fAEC_ISO_gain_max = 32;
			break;
		case ISO_1600:
			g_fAEC_ISO_gain_min = 32;
			g_fAEC_ISO_gain_max = 64;
			break;
		case ISO_3200:
			g_fAEC_ISO_gain_min = 64;
			g_fAEC_ISO_gain_max = 128;
			break;
		case ISO_6400:
			g_fAEC_ISO_gain_min = 128;
			g_fAEC_ISO_gain_max = 256;
			break;
		case ISO_12800:
			g_fAEC_ISO_gain_min = 256;
			g_fAEC_ISO_gain_max = 512;
			break;
		case ISO_25600:
			g_fAEC_ISO_gain_min = 512;
			g_fAEC_ISO_gain_max = 1024;
			break;
		default:
			g_fAEC_ISO_gain_min = g_fAEC_ISO_gain_max = 0;
			break;
	}

	ISO_MSG(("\nSetISO:  gain_min = %f, gain_max = %f\n",g_fAEC_ISO_gain_min, g_fAEC_ISO_gain_max));
	
	//ISO_MSG(("SetISO: g_VsPropCommit.dwExposureTimeAbsolut = %lu\n",g_VsPropCommit.dwExposureTimeAbsolut));
	//ISO_MSG(("SetISO: Ctl_ExposureTimeAuto.pAttr->CtlItemU8.Last  = %bx\n",Ctl_ExposureTimeAuto.pAttr->CtlItemU8.Last));
	//if(g_VsPropCommit.dwExposureTimeAbsolut == EXPOSURE_TIME_AUTO_MOD_MANUAL)
	if(ExposureTimeAutoItem.Last == EXPOSURE_TIME_AUTO_MOD_MANUAL)
	{
		//ISO_MSG(("SetISO: AE MANUAL, SetSensorGain\n"));
		SetSensorGain(g_fAEC_ISO_gain_min);
	}
	else
	{
		g_byAEForce_Adjust = 1;
	}
}
#endif//_RTK_EXTENDED_CTL_

U16 ColorTemperature(void)
{
	return g_wColorTemperature;
}

static void SaveROIStableColor(void)
{
	memcpy(g_byROIStableColorR, g_byAWB_WinMeanR, 25);
	memcpy(g_byROIStableColorG, g_byAWB_WinMeanG, 25);
	memcpy(g_byROIStableColorB, g_byAWB_WinMeanB, 25);
	g_byROIColorDiffStatTimes = 0;
}

static bit JudgeROISceneChange()
{
	U8 i,j;
	U8 byBlockDiff = 0;
	
	for(i=0; i<5; i++)
	{
		for(j=0; j<5; j++)
		{
			if(((U8)abs((S16)g_byROIStableColorR[i][j]-(S16)g_byAWB_WinMeanR[i][j]) > g_byROIColorDiffTh)
				|| ((U8)abs((S16)g_byROIStableColorG[i][j]-(S16)g_byAWB_WinMeanG[i][j]) > g_byROIColorDiffTh)
				|| ((U8)abs((S16)g_byROIStableColorB[i][j]-(S16)g_byAWB_WinMeanB[i][j]) > g_byROIColorDiffTh))
			{
				byBlockDiff++;
			}
		}
	}

	if(byBlockDiff > g_byROIColorChangeBlockNumTh)
	{
		g_byROIColorDiffStatTimes++;
	}
	else
	{
		g_byROIColorDiffStatTimes=0;
	}
	
	if(g_byROIColorDiffStatTimes > g_byROIDiffTimesTh)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

void RtkExtROIAE(void)
{	
	switch(g_byROIAEStatus)
	{
		case ROI_AE_INIT:
			SetROIAEWindow(g_wROITop, g_wROILeft, g_wROIBottom, g_wROIRight);
			g_byROIAEStatus = ROI_AE_START_ADJUST;
			break;

		case ROI_AE_START_ADJUST:
			g_byROIAEStatus = ROI_AE_ADJUST;
			break;

		case ROI_AE_BACK_TO_NORMAL:
			memcpy(g_byAE_WinWeight, &(ct_IQ_Table.ae.weight), 25);
			g_byROIAEStatus = ROI_AE_QUIT;
			break;

		default:
			break;
	}
}

#ifdef _AF_ENABLE_
void RtkExtROIAF(void)
{
	switch(g_byROIAFStatus)
	{
		case ROI_AF_INIT:
			SetROIAFWindow(g_wROITop, g_wROILeft, g_wROIBottom, g_wROIRight);
			RestartAF();
			g_byROIAFStatus = ROI_AF_START_ADJUST;
			break;

		case ROI_AF_START_ADJUST:
			g_byROIAFStatus = ROI_AF_ADJUST;
			break;

		case ROI_AF_BACK_TO_NORMAL:
			InitAFSetting(g_wSensorCurFormatWidth, g_wSensorCurFormatHeight);
			RestartAF();
			g_byROIAFStatus = ROI_AF_QUIT;
			break;

		default:
			break;
	}
}
#endif

void RtkExtROIProcess(void)
{
	U16 bmROIStableStatus = 0;
	static U8 byROIStableDelay = 0;

	switch(g_byROIStatus)
	{
	case ROI_ADJUST:
		if(((g_bmROIAutoControls&ROI_AE_SUPPORT) == ROI_AE_SUPPORT) && (g_byROIAEStatus == ROI_AE_ADJUST)
			&& ((g_byAECStatus == AE_STABLE)  || (g_byAECStatus == AE_MINEPTG) || (g_byAECStatus == AE_MAXEPTG)))
		{
			bmROIStableStatus |= ROI_AE_SUPPORT;
		}

#ifdef _AF_ENABLE_
		if(((g_bmROIAutoControls&ROI_AF_SUPPORT) == ROI_AF_SUPPORT) && (g_byROIAFStatus == ROI_AF_ADJUST)
			&& (g_byAFStatus == AF_STABLE))
		{
			bmROIStableStatus |= ROI_AF_SUPPORT;
		}
#endif

		if(bmROIStableStatus == g_bmROIAutoControls)
		{
			if(++byROIStableDelay >= 3)
			{
				SaveROIStableColor();
				g_byROIStatus = ROI_STABLE;
				byROIStableDelay = 0;
			}
		}
		else
		{
			byROIStableDelay = 0;
		}
		
		break;

	case ROI_STABLE:
		if(JudgeROISceneChange())
		{
			if((g_bmROIAutoControls&ROI_AE_SUPPORT) == ROI_AE_SUPPORT)
			{
				g_byROIAEStatus = ROI_AE_BACK_TO_NORMAL;
			}

#ifdef _AF_ENABLE_
			if((g_bmROIAutoControls&ROI_AF_SUPPORT) == ROI_AF_SUPPORT)
			{
				g_byROIAFStatus = ROI_AF_BACK_TO_NORMAL;
			}
#endif

			g_byROIStatus = ROI_QUIT;
		}
		
		break;

	default:
		break;
	}
}

void SetROI(U16 wWinTop, U16 wWinLeft, U16 wWinBottom, U16 wWinRight, U16 bmWinControls)
{
	if((bmWinControls&ROI_AE_SUPPORT) == ROI_AE_SUPPORT)
	{
		g_byROIAEStatus = ROI_AE_INIT;
	}
	else if((g_bmROIAutoControls&ROI_AE_SUPPORT) == ROI_AE_SUPPORT && g_byROIAEStatus != ROI_AE_QUIT)
	{
		g_byROIAEStatus = ROI_AE_BACK_TO_NORMAL;
	}

#ifdef _AF_ENABLE_
	if((bmWinControls&ROI_AF_SUPPORT) == ROI_AF_SUPPORT)
	{
		g_byROIAFStatus = ROI_AF_INIT;
	}
	else if((g_bmROIAutoControls&ROI_AF_SUPPORT) == ROI_AF_SUPPORT && g_byROIAFStatus != ROI_AF_QUIT)
	{
		g_byROIAFStatus = ROI_AF_BACK_TO_NORMAL;
	}
#endif

	if(bmWinControls != 0)
	{
		g_byROIStatus = ROI_ADJUST;
	}
	else
	{
		g_byROIStatus = ROI_QUIT;
	}

	g_wROITop = wWinTop;
	g_wROILeft = wWinLeft;
	g_wROIBottom = wWinBottom;
	g_wROIRight = wWinRight;
	g_bmROIAutoControls = bmWinControls;
}

U8 GetROIAEStatus()
{
	if((g_byAECStatus == AE_STABLE) || (g_byAECStatus == AE_MINEPTG) || (g_byAECStatus == AE_MAXEPTG))
	{
		return ROI_COMPLETE_OK;
	}
	else
	{
		return ROI_GOING;
	}
}

#ifdef _AF_ENABLE_
U8 GetROIAFStatus()
{
	if(g_byAFStatus == AF_STABLE)
	{
		return ROI_COMPLETE_OK;
	}
	else
	{
		return ROI_GOING;
	}
}
#endif

void SetPreviewLEDOff(U8 byLEDOff)
{
#ifdef _UVC_PPWB_
	if (g_VsPropCommit.byRtkExtPreviewLEDOff != byLEDOff)
	{
		g_dwRtkExtCtlUnitChange_Flag |= RTK_EXT_CTL_SEL_PREVIEW_LED_OFF;
	}
	else
	{
		g_dwRtkExtCtlUnitChange_Flag &= ~RTK_EXT_CTL_SEL_PREVIEW_LED_OFF;
	}
#endif

	g_byPreviewLEDOff = byLEDOff;
}

#endif	// _RTK_EXTENDED_CTL_
