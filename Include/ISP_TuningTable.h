#ifndef _ISP_Tuning_IQ_H_
#define _ISP_Tuning_IQ_H_
typedef struct IQ_HEADER
{
	U8 byAPVersion;
	U16 wIQTableLength;
	U8 byIQVersionH;
	U8 byIQVersionL;
	U8 byReserved0;
	U8 byReserved1;
	U8 byReserved2;
} IQ_HEADER_t;

typedef struct IQ_BLC_Offset
{
	// BLC
	U8 byOffsetR;
	U8 byOffsetG1;
	U8 byOffsetG2;
	U8 byOffsetB;
} IQ_BLC_Offset_t;

typedef struct IQ_BLC
{
	IQ_BLC_Offset_t normal;
	IQ_BLC_Offset_t lowlux;
} IQ_BLC_t;

typedef struct IQ_LSC_Circle
{
	// Circle LSC
	U8 abyCurve[3][48];
	U16 awCenter[6];
} IQ_LSC_Circle_t;

typedef struct IQ_LSC_Micro
{
	// Micro LSC
	U8 byGridmode;
	U8 abyMatrix[3][320];
} IQ_LSC_Micro_t;

typedef struct IQ_LSC_Dynamic
{
	U8 byGainThL;
	U8 byGainThH;
	U8 byAdjustRateL;
	U8 byAdjustRateH;
	U8 abyGainR[6];
	U8 abyGainB[6];
	U8 byStartThd;
	U8 byStopThd;
	U16 wColorTempBuffer;
	U16 awColorTempTh[5];
	U8 abyRatebyCT[6][3];	
}IQ_LSC_Dynamic_t;

typedef struct IQ_LSC
{
	IQ_LSC_Circle_t circle;
	IQ_LSC_Micro_t micro;
	IQ_LSC_Dynamic_t dynamic;
} IQ_LSC_t;

typedef struct IQ_CCM
{
	// CCM
	S16 abyD65[9];
	S16 abyA[9];
	S16 abyLowlux[9];
	U8 byDynamicLumaGainThL;
	U8 byDynamicLumaGainThH;
	U16 wDynamicCCMA_Th;
	U16 wDynamicCCMD65_th;
} IQ_CCM_t;

typedef struct IQ_Gamma
{
	// Gamma
	U8 abyNor[28];
	U8 abyLowlux[28];
	U8 byDynamicGainThL;
	U8 byDynamicGainThH;
} IQ_Gamma_t;

typedef struct IQ_AE_Target
{
	// jack_hu 2012-08-16: AE4.0 parameters
	U16 wHistRatio_L;
	U16 wHistRatio_H;
	U8 byMeanTarget_L;
	U8 byMeanTarget;
	U8 byMeanTarget_H;
	U8 byHistPosTh_L;
	U8 byHistPosTh_H;
	U8 byDynamicMeanDecExtent;
} IQ_AE_Target_t;

typedef struct IQ_AE_Limit
{
	U8 byMaxStep50Hz;
	U8 byMaxStep60Hz;
	U16 wGlobalgainmax;		// hemonel 2011-12-05: IQ version 2 only modify these field
	U16 wGainTh_ContinousFps;
	U16 wGainTh_15fps;
	U16 wGainTh_30fps;
	U8 byHighLightModeTh;
} IQ_AE_Limit_t;

typedef struct IQ_AE_Weight
{
	U8 byWeight[25];
} IQ_AE_Weight_t;

typedef struct IQ_AE_Sensitivity
{
	float fAdjustTh; // jack_hu 2012-08-16: AE4.0 parameters
	U8 byLatency;
	U8 bySameBlockDiffTh;
	U8 bySameBlockNumTh;
} IQ_AE_Sensitivity_t;

typedef struct IQ_AE
{
	IQ_AE_Target_t target;
	IQ_AE_Limit_t limit;
	IQ_AE_Weight_t weight;
	IQ_AE_Sensitivity_t sensitivity;
} IQ_AE_t;

typedef struct IQ_AWB_Simple
{
	U8 abyGainR[6];
	U8 abyGainB[6];
	U8 byK1;
	U16 wB1;
	U16 wB2;
	S8 sbyK3;
	S16 swB3;
	S8 sbyK4;
	S16 swB4;
	S8 sbyK5;
	S16 swB5;
	S8 sbyK6;
	S16 swB6;
	U16 wB_Up;
	U16 wB_Down;
	S16 swB_Left;
	S16 swB_Right;
	U8 byBright_Max;
	U8 byBright_Min;
	U16 wRGBSumMinLimit;
} IQ_AWB_Simple_t;

typedef struct IQ_AWB_Advanced
{
	U16 wPixelNumTh;
	U8 byRGMax;
	U8 byRGMin;
	U8 byBGMax;
	U8 byBGMin;
	U8 byYMax;
	U8 byYMin;
	U8 byFtGainTh;
} IQ_AWB_Advanced_t;

typedef struct IQ_AWB_Sensitivity
{
	U16 wGainDiffTh;
	U8 byGainStep;
	U8 byYmeanMinLimitTh;
	U8 byColorDiffTh;
	U8 byDiffWindowsTh;
} IQ_AWB_Sensitivity_t;

typedef struct IQ_AWB
{
	IQ_AWB_Simple_t simple;
	IQ_AWB_Advanced_t advanced;
	IQ_AWB_Sensitivity_t sensitivity;
} IQ_AWB_t;

typedef struct IQ_Texture_Sharpness
{
	U8 byCIF;
	U8 byVGA;
	U8 byHD;
	U8 byLowlux;
	U8 byDynamicGainThL;
	U8 byDynamicGainThH;
} IQ_Texture_Sharpness_t;

typedef struct IQ_Texture_Denoise
{
	U8 abyVaris_CIF[4][28];
	U8 abyVaris_VGA[4][28];
	U8 abyVaris_HD[4][28];
	U8 byDynamicGainTh0;
	U8 byDynamicGainTh1;
	U8 byDynamicGainTh2;
} IQ_Texture_Denoise_t;

typedef struct IQ_Texture_Denoise_Th
{
	U8 byDynamicGainTh0;
	U8 byDynamicGainTh1;
	U8 byDynamicGainTh2;
}IQ_Texture_Denoise_Th_t;

typedef struct IQ_Texture
{
	IQ_Texture_Sharpness_t sharpness;
	IQ_Texture_Denoise_t denoise;
} IQ_Texture_t;

typedef struct IQ_UV_Offset
{
	S8 byA_U;
	S8 byA_V;
	S8 byD65_U;
	S8 byD65_V;
	U16 wDynamicUV_A_Th;
	U16 wDynamicUV_D65_Th;
} IQ_UV_Offset_t;

typedef struct IQ_Gamma2
{
	S8 abyNor[2][28];
	S8 abyLowlux[2][28];
}IQ_Gamma2_t;

typedef struct IQ_Texture2_Corner
{
	U16 wStart_RGB;
	U16 wEnd_RGB;
	U16 wStart_YUV;
	U16 wEnd_YUV;
	U8 abyRate[4][8];
}IQ_Texture2_Corner_t;

typedef struct IQ_Texture2_UVDenoise
{
	U8 abyUvd[4][5];
}IQ_Texture2_UVDenoise_t;

typedef struct IQ_Texture2_Nr
{
	U8 abyEehSm3CrcRate[4];
	U8 abyRdMm2Rate[4];
	U8 abyNrMmm[4][5];
}IQ_Texture2_Nr_t;

typedef struct IQ_Texture2
{
	IQ_Texture2_Corner_t corner;
	IQ_Texture2_UVDenoise_t uvdenoise;	
	IQ_Texture2_Nr_t nr;
}IQ_Texture2_t;

typedef struct IQ_EdgeEnhance
{
	U8 byEdgeEhance[33];
}IQ_EdgeEnhance_t;

typedef struct IQ_DeadPixelC
{
	U8 bydpc[9];
}IQ_DeadPixelC_t;

typedef struct IQ_UV_Color_Tune
{
	U8 byUvColorTune_A[4];
	U8 byUvColorTune_D65[4];
	U16 wDynamicUV_A_Th;
	U16 wDynamicUV_D65_Th;	
}IQ_UV_Color_Tune_t;

typedef struct IQ_FalseColor
{
	U8 byFalseColor[4][6];
}IQ_FalseColor_t;

typedef struct IQ_NLC
{
	U8 byG[16];
	S8 byRDiff[16];
	S8 byBDiff[16];
}IQ_NLC_t;

typedef struct IQ_Hdr_Th
{
	U8 byHistBlkPxlTh;
	U8 byHistBrtPxlTh;
	U8 byHistBlkPxlMax;
	U8 byHDRMaxTuneVaule;
}IQ_Hdr_Th_t;

typedef struct 
{
	U8 abyHDRGamma[28];
} IQ_HDR_FW_t;

typedef struct 
{
	U8 bytgamma_th;
	U8 bytgamma_rate;
	U8 byhdr_lpf_coef[9];
	U8 byhdr_halo_thd;
	U8 byhdr_curver[24];
	U8 byhdr_max_curver[9];
	U8 byhdr_step;
	U8 bylocal_constrast_curver[16];
	U8 bylocal_constrast_rate_min;
	U8 bylocal_constrast_rate_max;
	U8 bylocal_constrast_step;
} IQ_HDR_HW_t;

typedef struct IQ_HDR
{
	IQ_Hdr_Th_t hdrth;
	IQ_HDR_FW_t hdrfw;
	IQ_HDR_HW_t hdrhw;
} IQ_HDR_t;

typedef struct IQTABLE
{
	IQ_HEADER_t header;
	IQ_BLC_t blc;
	IQ_LSC_t lsc;
	IQ_CCM_t ccm;
	IQ_Gamma_t gamma;
	IQ_AE_t ae;
	IQ_AWB_t awb;
	IQ_Texture_t texture;
	IQ_UV_Offset_t uvoffset;
	IQ_Gamma2_t gamma2;
	IQ_Texture2_t texture2;
	IQ_EdgeEnhance_t edgeenhance;
	IQ_FalseColor_t falsecolor;
	IQ_DeadPixelC_t dpc;
	IQ_UV_Color_Tune_t uvcolortune;
	IQ_NLC_t nlc;
	IQ_HDR_t hdr;
}IQTABLE_t;

#define IQ_TABLE_AP_VERSION	0x14	// this version sync FW IQ table with IQ tuning AP

#endif
