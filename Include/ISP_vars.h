#ifndef _GLOBAL_ISP_VARS_H_
#define _GLOBAL_ISP_VARS_H_


#include "camutil.h"


extern U8 g_byIsAEAWBFixed;

extern U8 g_byISPABStaticsEn;
extern U8 g_byABCurrentStep;
extern U8 g_byABRatioThreshold;
extern U8 g_byABRatioMaxThreshold;
extern U8 g_byABRatioThirdThreshold;
extern U8 g_byABRatioSecThreshold;
extern U8 g_byABRatioMinThreshold;
extern U8 g_byABBypassThreshold;
extern U8 g_byABDummylines;
extern U8 g_byABDetectMinFps;
extern U8 g_byABDetectMaxGain;
extern U8 g_byABDctCntThreshold;
extern U8 g_byABDynamicDctCntSetting;
extern U8 g_byABStaticDctCntSetting;
extern U8 g_byABStableBlockDiffTh;

extern U8 g_byABCurPowerLine;
extern U8 g_byABDetectStatis;
extern U8 g_byABDetectType;
extern U8 g_byABIFExceedFpsThreshold;
extern U8 g_byABDetectCount;
extern U8 g_byABStart;
extern U8 g_byABLastAETime;
extern U8 g_byABIfFlickExist;

extern U32 g_byABRatioSum;
extern U32 g_byABLastRatioSum;
extern U8 g_byABLastWinYMean[25];
extern U8 g_byABPresentWinYMean[25];

#ifdef _ENABLE_OUTDOOR_DETECT_
extern Dyanmic_AWB_Boundary_t code gc_AWBDynmaicBoundary;
extern U8 g_bySceneChangeDelay;
extern U8 g_byOutdoorET;
extern U8 g_byIndoorET;
extern U8 g_byOutDoorScene;
extern U8 g_byOutdoorModeEnter_Thd;
extern U8 g_byOutdoorModeExit_Thd;
extern U8 g_byExistFlicker;
extern U8 g_byFlickerUnDetectCnt;
extern U8 g_byFlickerDetectCnt;
#endif

//AEC
extern float g_fSensorRowTimes;
extern U8 g_byISPAEStaticsEn;
extern float g_fAEC_EtGain;
extern U16 g_wAECGlobalgainmax;
extern float g_fAEC_Gain;
extern U16 g_wAFRInsertDummylines;
extern float g_fCurExpTime;
extern U8 g_byAEC_Window_Weight[5][5];
extern U8 g_byAEC_HighLight_Thread;
extern U8 g_byAEC_Mode;
extern U8 g_byAECStatus;
extern U8 g_byAEC_Backlight_Ratio;/* param to adjust AE  Stable Region uppper*/
extern U16 g_wAEC_Fps_Coef;
extern float g_fAEC_SmoothExpectYmeanCmp_all;
extern U8 g_byAE_StableOut_DelayCnt;
extern U8 g_byAE_Delay_Cfg;
extern float g_fAEC_SetEtgain;
extern U8 g_byAEC_AdjustStatus;
extern float g_fAEC_Min_ExpTime;
extern U8 g_byAEForce_Adjust;
extern U16 g_wAEC_LineNumber ;
extern U16 g_wAECExposureRowMax;
extern U8 g_byAEMaxStep50Hz;
extern U8 g_byAEMaxStep60Hz;
extern U16 g_wAEC_Gain_Threshold_15fps;
extern U16 g_wAEC_Gain_Threshold_30fps;
#ifdef _RTK_EXTENDED_CTL_
extern float g_fAEC_ISO_gain_min;
#endif//#ifdef _RTK_EXTENDED_CTL_
extern U8 g_byAE_Hist_MeanTargetDec_Extent;
extern U8 g_byAE_OverExpTargetDecVal;
extern U8 g_byAEC_Control;
extern U8 g_byAE_WinWeight[25];
extern U8 g_byAEC_Stable_BlockDiffTh;
extern U8 g_byAEC_Stable_BlockNumTh;


extern U16 g_wAE_Hist_StNum[64];
extern U8 g_byAE_Stable_WinYMean[25];
extern U8 g_byAE_WinYMean[25];


//AE
extern U16 g_wYpercentage_Th_L;
extern U16 g_wYpercentage_Th_H;
extern U8 g_byAEMeanValue;

extern float g_fAEC_HighContrast_Exp_L;
extern float g_fAEC_HistPos_LH_Target;
extern float g_fAEC_HistPos_LH_Target_Th;

#ifdef _FACIAL_EXPOSURE_
extern U16 g_wZoomStartX;	
extern U16 g_wZoomStartY;	
extern U16 g_wZoomScale_h;
extern U16 g_wZoomScale_v;	
extern U16 g_wAE_WinStartX;
extern U16 g_wAE_WinStartY;
extern U16 g_wAE_WinWidth; 
extern U16 g_wAE_WinHeight;	 
extern U8 g_byAEFaceWindowSet;
extern U8 g_bySimilar;							
extern U8 g_bySimilar_last;
extern U8 g_byFaceExposure_En;
#endif

extern U8 g_byAEC_Mean_Target;
extern U8 g_byAEC_Mean_Target_L;
extern U8 g_byAEC_Mean_Target_H;
extern U8 g_byAEC_HistPos_Th_L;
extern U8 g_byAEC_HistPos_Th_H;
extern U8 g_byAEC_HistPos_L;
extern U8 g_byAEC_HistPos_H;
extern float g_fAEC_Adjust_Th;
extern U16 g_wAEC_CurGain_Threshold_15fps;
extern U16 g_wAEC_CurGain_Threshold_30fps;

#ifdef _AE_NEW_SPEED_ENABLE_
extern U8 g_byAE_SpeedMode;
extern U8 g_byAE_LastStatus;
extern U8 g_byAE_StaticsDelay;
#else
extern U8 g_byAEC_AdjustMode;
#endif

#ifdef _RTK_EXTENDED_CTL_
extern float g_fAEC_ISO_gain_min;
extern float g_fAEC_ISO_gain_max;//bruce_young 2013-11-4: Add for using ISO manual control
#endif

//AWB
extern U8 g_byAWB_K1;
extern U16 g_wAWB_B1;
extern U16 g_wAWB_B2;
extern S8 g_byAWB_K3;
extern S16 g_wAWB_B3;
extern S8 g_byAWB_K4;
extern S16 g_wAWB_B4;
extern S8 g_byAWB_K5;
extern S16 g_wAWB_B5;
extern S8 g_byAWB_K6;
extern S16 g_wAWB_B6;
extern U8 g_byAWBFineMax_Bright;
extern U8 g_byAWBFineMin_Bright;
extern U16 g_wAWBFineGain_R;
extern U16 g_wAWBFineGain_G;
extern U16 g_wAWBFineGain_B;
extern U8  g_byNearestTempIndex   ;
extern U8 g_byISPAWBStaticsEn;
extern U8 g_byAWBGainStep;
extern U16 g_wAWBGainDiffTh;
extern U8 g_aAWBRoughGain_R[6];
extern U8 g_aAWBRoughGain_B[6];
extern U32 g_dwDistance;
extern 	U16 g_wColorTempPixnum[6];
extern U16 g_wTemPixelNumThread_L;
extern U16  g_wProjectGB;
extern U16  g_wProjectGR;
extern U16 g_wGWLastGB;
extern U16 g_wGWLastGR;
extern U8 g_byAWBFineMax_RG;
extern U8 g_byAWBFineMin_RG;
extern U8 g_byAWBFineMax_BG;
extern U8 g_byAWBFineMin_BG;
extern U16 g_wFtGB;
extern U16 g_wFtGR;
extern U8 g_byFtGainTh;
extern U16 g_wAWB_B_Up;
extern U16 g_wAWB_B_Down;
extern S16 g_swAWB_B_Left;
extern S16 g_swAWB_B_Right;
extern U16 g_wAWB_RGB_SumTh;

extern U16 g_wAWBFinalGainR;
extern U16 g_wAWBFinalGainB;
extern U8  g_byNearestTempIndex_Last;
extern U8	g_byAWBGainStep_Max;
extern U8 	g_byAWBStepNumbers;
//AWB
extern U8 g_byAWB_Win_Bright_Max;
extern U8 g_byAESensorGainMax;
extern U16 g_wAWBRGain_Last;
extern U16 g_wAWBGGain_Last;
extern U16 g_wAWBBGain_Last;
extern U8 g_byAWB_State;

extern U8 g_byAWBWin_Rgain_FirstProject[5][5];
extern U8 g_byAWBWin_Bgain_FirstProject[5][5];
extern U8 g_byAWBWinInitRGain[5][5];
extern U8 g_byAWBWinInitBGain[5][5];
extern U8 g_byAWBWin_Brightness[5][5];
extern U8 g_byAWBFixed_YmeanTh;
extern U8 g_byAWB_Win_Bright_Min;
extern U8 g_byAWB_Window_Position;
extern U16 g_wAWB_RGB_SumMin;
extern U8 g_byAWB_Hold;
extern U8 g_byAWB_SameBlock_Hold;
extern U8 g_byAWBEnterStableDelay;
extern 	U8 g_byAWBDiffWindows;
extern U8 g_byAWBDiffWindowsTh;
extern U8 g_byAWBColorDiff_Th;
extern U8 g_byAWBWinLastGR[5][5];
extern U8 g_byAWBWinLastGB[5][5];
extern U8 	g_aCTC_Gamma[28];
extern U8 	g_byAWBExitStableDelay;
extern U8 	g_byAWBStableDelay_Cfg;
extern U8 g_byAWB_AdjustMode;




//LSC & MLSC
extern U8  (*g_pLensShadingCurve)[48];
extern U16 g_awLensShadingCenter[6];
extern U8 g_bymLSCGridmode;
extern U8  (*g_pmLensShading)[320] ;
//CCM
extern S16 g_aCCM[9];
extern S16 g_aCCM_Normal[9];

//BLC
extern U8 g_byoffsetR;
extern U8 g_byoffsetG1;
extern U8 g_byoffsetG2;
extern U8 g_byoffsetB;
extern U16 g_wSensorWidthBefBLC;
extern U16 g_wSensorHeightBefBLC;

//Gamma
extern U8 code g_byGammaCtlAdjDef_RAW[28];
extern U8 g_aGamma_Def[28];
extern U8 g_aGamma_Lowlux[28] ;
extern U8 g_aGamma_Normal[28];

//ygamma
extern U8 g_Ygamma_nor[16];
extern U8 g_Ygamma_lowlux[16];
extern U8 g_Ygamma[16];


//Sharpness
extern U8 g_bySharpness_Normal;
extern U8 g_bySharpParamIndex_Last;
//AF
extern U8 g_byAFStatus ;
extern U8 g_byAFControl;
extern U16 g_wAF_CurPosition ;
extern U32 g_dwAFCurrentSharp;
extern U8 g_bySearchingDir;
extern U8 g_byAF_RoughStep;
extern U8 g_byAF_FineStep;
extern U8 g_byAFSearchStep;
extern U32 g_dwAFSharpLast1;
extern U32 g_dwAFSharpLast2;
extern U32 g_dwAFSharpLast3;
extern U8 g_byAFPosIndex;
extern U8 g_byAFPosIndexLast1;
extern U8 g_byAFPosIndexLast2;
extern U8 g_byAFSerachDirectionChangeTimes;
extern U8 g_byAFSearchTimes;
extern U8 g_byAFSharpPeakTh2	;
extern U8 g_byAFSharpPeakTh1;
extern U8 g_byAFMaxSharpPos;
extern U32 g_dwAFMaxSharpness;
extern U8 g_byAF_Stable_Position ;
extern U32 g_dwAF_Stable_Sharpness;
extern U8  g_byAFStatTime;
extern U8  g_byAFStatTime2;
extern float g_fAF_SharpDiff_High_Th;
extern float g_fAF_SharpDiff_Low_Th;
extern U8 g_byAF_ColorDiff_Th;
extern U8  g_byAFStable_RMean[5][5]	;
extern U8  g_byAFStable_GMean[5][5];
extern U8  g_byAFStable_BMean[5][5];
extern U8 g_byAWB_WinMeanR[5][5];
extern U8 g_byAWB_WinMeanG[5][5];
extern U8 g_byAWB_WinMeanB[5][5];
extern U8 g_byAFEnterRoughSearch;
extern U8 g_byAFDiffTimes_Th;
extern U16 g_wAFRoughEntrance_Th;
extern U8 g_byAFTotalSteps;
extern U8 g_byAFDirChangeTimes_Th;
extern float g_fAF_SharpDiff_High_Th2;
extern float g_fAF_SharpDiff_Low_Th2;
extern float g_fAFSharpChange;
extern U16 g_wAFColorChange;
extern U8 g_byAFSharpPeakTh1_Fine;
extern U8 g_byAFSharpPeakTh2_Fine;
extern U8 g_byAFSharpPeakTh1_Rough;
extern U8 g_byAFSharpPeakTh2_Rough;
extern U16 g_wAFCurrentEdgeNum;
extern U8 g_byIsMoving;
extern U8 g_byAFMoveBlockNum_Thd;				
extern U8 g_byAFMoveColor_Thd;				
extern U8 g_byAFMoveCenterBlockNum_Thd;		


//AS
extern U8 g_byWhiteScene;
extern U8 g_byYellowScene;
extern U8 g_byWhite_Edge_Dark_TH;
extern U8 g_byWhite_Edge_TH;
extern U8 g_byWhite_Edge_HD_TH;
extern U8 g_byWhite_Edge_Block_TH;
extern U8 g_byWhite_Edge_Block_Dark_TH;
extern U8 g_byAddAE_U;
extern U16 g_wASWinSharpLast[5][5];

extern U8 g_byCIFscaleParam_En;
// Interpolation params

// 1/2 subsample
extern U8 g_byHalfSubSample_En;

extern U16 g_wZoomRemainderWidth;
extern U16 g_wZoomRemainderHeight;


extern U8 g_bySaturation_Def;
extern U8 g_byContrast_Def;




extern U8 g_bySharpness_Def;
extern U8 g_bySetHalfSubSample_En;




#ifdef _ENABLE_MJPEG_
extern U8 g_byJpeg_ACRA_En;
extern U8    g_byQtableScale;
extern U8 g_byQtableCurScale;
#endif //_ENABLE_MJPEG_
extern U8 g_abyISParamVaris[4][28];
extern U8 g_bySharp_VIIR_Coef	;
extern U8 g_bySharp_HIIR_Coef;
extern U8 g_bySharp_EdgeDct_Thd1;

//moire
extern U8 g_byMoireThreshold;

extern U16 g_wDynamicISPEn;
extern U8 g_bySharpness_Lowlux;
extern U8 g_byFirstStatic;

extern U8 g_byDynISP_FrameInterval;
extern U8 g_abyColorTemperatureTable_gain[6];
extern U8 g_byCCMState;
extern S8 g_byU_Offset_Normal;
extern S8 g_byV_Offset_Normal;
extern U16 g_wSTnum;
extern U8 g_byFirstPreview;
extern U8 g_bySubResolution_For_LSC;

extern U8 g_byDynamicLSCSet;

extern U16 g_aIlluminantCTth_last[5];
extern U8 g_byLastIllumCT;

extern U16 g_wAWBSTnum;	

extern U8 g_byHistEqLB;
extern U8 g_byHistEqHB;
extern U8 g_bBacklightFlag;
extern IQ_Hdr_Th_t g_tHdrTh;						
extern U8 g_byHDRGamma[28];	
extern U8 g_byFX;

extern U8 g_byFpsLast;
extern IQTABLE_t code ct_IQ_Table;
extern IQ_Gamma_t g_byIQGamma;
extern IQ_Texture_Sharpness_t g_byIQSharpness;
extern IQ_Texture_Denoise_Th_t g_byIQDenoiseTh;
extern S8 g_asbyGamma2Cur[2][28];
extern IQ_UV_Color_Tune_t g_byIQUVColorTune;
extern IQ_NLC_t g_byIQNLC;	

extern U32 r_dwISP_AWB_WIN_SUM_R;
extern U32 r_dwISP_AWB_WIN_SUM_G;
extern U32 r_dwISP_AWB_WIN_SUM_B;
extern U16 r_wISP_MICRO_LSC_ADDR;
extern U16 r_wISP_AF_START0_X;
extern U16 r_wISP_AF_START0_Y;
extern U16 r_wISP_AF_END0_X;
extern U16 r_wISP_AF_END0_Y;
extern U16 r_wISP_AF_START1_X;
extern U16 r_wISP_AF_START1_Y;
extern U16 r_wISP_AF_END1_X;
extern U16 r_wISP_AF_END1_Y;
extern U32 r_dwISP_AF_SUM0;
extern U32 r_dwISP_AF_SUM1;
extern U32 r_dwISP_AF_NUM0;
extern U32 r_dwISP_AF_NUM1;

extern U8 g_byAEDecreaseRate;
extern U8 g_byFaceLostTime;
extern U8 g_byXU_FacialAEWin_Set;
extern U8 g_byaFacial_AE_Parameters[7];
extern U8 g_byTgamma_rate_max;
extern U8 g_byTgamma_rate_min ;

extern U8 code gc_byAE_WinWeight[25];
extern U8 g_byFaceAEmode;

// Realtek Extended Unit
#ifdef _RTK_EXTENDED_CTL_
// EV Compensation
extern U8 g_byAEMeanTarget_Backup;
extern U8 g_byAEMeanTargetL_Backup;
extern U8 g_byAEMeanTargetH_Backup;
extern U8 g_byAEHistPosThH_Backup;
extern U8 g_byAEHistPosThL_Backup;

// ROI
extern U16 g_wROITop;
extern U16 g_wROILeft;
extern U16 g_wROIBottom;
extern U16 g_wROIRight;
extern U16 g_bmROIAutoControls;

extern U8 g_byROIStableColorR[5][5];
extern U8 g_byROIStableColorG[5][5];
extern U8 g_byROIStableColorB[5][5];

extern U8 g_byROIColorDiffStatTimes;
extern U8 g_byROIColorDiffTh;
extern U8 g_byROIColorChangeBlockNumTh;
extern U8 g_byROIDiffTimesTh;

extern U8 g_byROIAEStatus;
extern U8 g_byROIAFStatus;
extern U8 g_byROIStatus;

// Preview LED Off
extern U8 g_byPreviewLEDOff;

// Color Temperature Estimation
extern U16 g_wColorTemperature;
#endif

#endif //_GLOBAL_VARS_H_
