#ifndef _CAMI2C_H_
#define _CAMI2C_H_
#define I2C_NO_ERR		0x00
#define I2C_HW_ERR		0x01
#define I2C_TIME_OUT	0x02
#define I2C_WRITE_ERR	0x03
#define I2C_READ_ERR	0x04

#define I2C_READ_OP		0x01
#define I2C_WRITE_OP	0x00

#define I2C_DATA_LEN_1	0x01
#define I2C_DATA_LEN_2	0x02
#define I2C_DATA_LEN_3	0x03
#define I2C_DATA_LEN_4	0x04

#define I2CEEP_ADDR       0xA0


typedef struct
{
	U8 byI2CID;
	U8 byMode_DataWidth;
	U8 byI2CPageSelAddr;

} SnrRegAccessProp_t;

typedef struct
{
	U8 byI2CID;			//I2C Slave ID
	U8 byMode;			//0->W, 1->SetAddress+Read, 2->Random read
	U8 byAddrLen;
	U8 byAddr1;
	U8 byAddr2;
	U8 byAddr3;
	U8 byAddr4;
	U8 byAddr5;
}I2CSuperAccess_t;

#define I2C_ADDR_MODE_BA     (0<<4)
#define I2C_ADDR_MODE_WA       (1<<4)
#define I2C_ADDR_MODE_PA         (2<<4)
#define I2C_ADDR_MODE_MASK         (0xF<<4)

#define I2C_ACCESS_DATAWIDTH_1            1
#define I2C_ACCESS_DATAWIDTH_2             2
#define I2C_ACCESS_DATAWIDTH_MASK     0xF

#define I2CSuper_Mode_W	0x00		//just write
#define I2CSuper_Mode_R		0x01		//setaddress + read
#define I2CSuper_Mode_RR	0x02		//Random read

void Init_I2C(void);
bit Read_SenReg(U16 address, U16 * pwData);
bit Write_SenReg(U16 address, U16 wData);
bit I2C_WriteWord(U8 byID_Addr, U16 wAddr, U16 wData, U8 byLen);

#ifdef _MIPI_EXIST_
U8 Read_MIPIAPHYReg(U16 address, U16 * pwData);
U8 Write_MIPIAPHYReg(U16 address, U16 wData);
#endif

#ifdef _I2C_NORMAL_
bit I2C_NORMAL_Read(U8 const byID_Addr, U32 * const pdwData, U8 const byLen);
bit I2C_NORMAL_Write(U8 const byID_Addr, U32 const dwData, U8 const byLen );
#endif
bit I2C_Super_Write(I2CSuperAccess_t * ptI2CSuperAccess, U8 *pData , U8 byDataLen);
bit I2C_Super_Read(I2CSuperAccess_t * ptI2CSuperAccess, U8 *pData , U8 byDataLen);

#endif //_CAMI2C_H_
