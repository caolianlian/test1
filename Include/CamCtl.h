#ifndef _CAMCTL_H_
#define _CAMCTL_H_

//#define SP_WHITE_BALANCE_COMPONET
#include "camuvc.h"
//modify this macro according HW
#define MAX_GPIO_NUM  (12)   // (10)   5820X _ 12 gpio

//default transfer_size/HWM/LWM
#define TRANSFER_SIZE	0x0010// 16k
#define LPM_LWM			0x1600 // 16k
#define LPM_HWM		0x2100  // 16.5K

#define LED_WORKMODE_SWITCH   0
#define LED_WORKMODE_TOGGLE   1
#define LED_WORKMODE_ALWAYSOFF 2
#define LED_WORKMODE_TURN_ON     3
#define LED_WORKMODE_TURN_OFF  4

#define LED_TOGGLE_CYCLE  40

//RT FLAG
//#define RT_FLAG_3    (0X55)
//#define RT_FLAG_2    (0XAA)
//#define RT_FLAG_1    (0XAA)
//#define RT_FLAG_0    (0X55)
//#define RT_FLAG_LEN (4)

//OP MASKS
#define CONTROL_OP_GET_CUR	(1 << 0)
#define CONTROL_OP_SET_CUR	(1 << 1)
#define CONTROL_OP_GET_MIN	(1 << 2)
#define CONTROL_OP_GET_MAX	(1 << 3)
#define CONTROL_OP_GET_RES		(1 << 4)
#define CONTROL_OP_GET_DEF	(1 << 5)
//
#define CONTROL_OP_GET_LEN      (1 << 6)
//
////bit7  if 1 signed control, if 0 unsigned control
#define CONTROL_ATTR_SIGNED        (1<<7)
#define CONTROL_ATTR_UNSIGNED   (0<<7)

//control len
#define CONTROL_LEN_1             (1)
#define CONTROL_LEN_2             (2)
#define CONTROL_LEN_4             (4)
#define CONTROL_LEN_8             (8)

//INFO MASKS
#define CONTROL_INFO_SP_GET  		        (1 << 0)
#define CONTROL_INFO_SP_SET    		        (1 << 1)
#define CONTROL_INFO_DIS_BY_AUTO         (1 << 2)
#define CONTROL_INFO_SP_AUTOUPDATA    (1 << 3)
#define CONTROL_INFO_SP_ASYNC    	        (1 << 4)

//////Control change flags
/*
	ready: stable state  , do not need notify.
	not ready : unstable state, control may change.
	need notify: ready state, but need notify.
*/
//
#define CTL_CHNGFLG_RDY                                          0x00
#define CTL_CHNGFLG_NOTRDY                                   0x01
#define CTL_CHNGFLG_ASYN_NEED_NOTIFY            0X02
#define CTL_CHNGFLG_FAILED_NEED_NOTIFY         0X03
#define CTL_CHNGFLG_AUTO_NEED_NOTIFY             0X04
#define CTL_CHNGFLG_INFOCHNG_NEED_NOTIFY   0X05
//
#define STS_PKT_ATTR_VALUE_CHANGE   (0x00)
#define STS_PKT_ATTR_INFO_CHANGE      (0x01)
#define STS_PKT_ATTR_FAILED_CHANGE  (0x02)


/*EXTENSION UNIT DBG CONTROLS*/
#define EXU1_TRAPEZIUM_CORRECTION	0x01
#define EXU1_CTL_SEL_TCORRECTION		(0x0001)

#define EXU1_ADDR 0X01
//debug support.
#define EXU1_RIMEM    0x02
#define EXU1_RWXMEM   0x03

#define EXU1_RCODE    0x04

//firmware download support.
#define EXU1_ERASEFLASH             0x05
#define EXU1_RWFLASH               0x06
#define EXU1_WRITEFLASH_TAG    0x07

#define EXU1_RST_OP   0x08

#define RST_OP_2DLFW       0x01
#define RST_OP_2DLCODE   0X02
#define RST_OP_2ROM         0X03

#define EXU1_I2C_REG   	 0x09  // used to access sensor registers
//#define EXU1_SNR_ID         	 0X0A  // used to get sensor vendor id and product id
//#define EXU1_RWEEPROM    	 0X0B  // used to access eeprom.
//#define EXU1_RWPHYREG        0x0C  // used to access USB PHY registers.
//#define EXU1_DEVSTAT            0x0D
#define EXU1_CMD_STS          0x0A
#define EXU1_DATA_INOUT    0X0B
#define EXU1_CMD_KEYSTONE    0X0C




#define  EXU1_GPIO_NOTITY    0X0C



//#define EXU1_GRAY_MONO      0X0E
//#define EXU1_GPIO_NOTITY    0X0F
//#define EXU1_NEGATIVE           0X10

//

#define CMD_DATA_CONTROL_SIZE 0X08



/*EXTENSION UNIT ISP controls*/

#define  EXU_ISP_GRAY_MONO       0x01
#define  EXU_ISP_NEGTIVE               0x02
#define  EXU_ISP_AUTOCONTRAST  0x03
//consts for EXU_ISP_GRAY_MONO
#define  GM_NORMAL 0x00
#define  GM_GRAY 0x01
#define  GM_MONO 0x02
//consts for EXU_ISP_NEGTIVE
#define  NEG_NORMAL 0x00
#define  NEG_NEGTIVE  0X01

/**/

#define MAX_RWEEPROM_SIZE       8

#define MAX_RWFLASH_SIZE  8

/*EXTENSION UNIT 2 CONTROLS*/

 
//PWR_LINE_FRQ
#define PWR_LINE_FRQ_DIS     0x00
#define  PWR_LINE_FRQ_50      0x01
#define  PWR_LINE_FRQ_60      0x02
#ifdef _LENOVO_JAPAN_PROPERTY_PAGE_
#define PWR_LINE_FRQ_OUTDOOR 	0x03
#endif

// hemonel 2009-11-26: define new control struct for min, max, res and default to be all changed.
typedef struct
{
	U8 Min;
	U8 Max;
	U8 Res;
	U8 Def;
	U8 Des;
	U8 Last;
} CtlItemU8_t;

typedef struct
{
	S8 Min;
	S8 Max;
	U8 Res;
	S8 Def;
	S8 Des;
	S8 Last;
} CtlItemS8_t;

typedef struct
{
	U16 Min;
	U16 Max;
	U16 Res;
	U16 Def;
	U16 Des;
	U16 Last;
} CtlItemU16_t;

typedef struct
{
	S16 Min;
	S16 Max;
	U16 Res;
	S16  Def;
	S16  Des;
	S16  Last;
} CtlItemS16_t;

typedef struct
{
	U32 Min;
	U32 Max;
	U32 Res;
	U32 Def;
	U32 Des;
	U32 Last;
} CtlItemU32_t;

typedef struct
{
	S32 Min;
	S32 Max;
	U32 Res;
	S32 Def;
	S32 Des;
	S32 Last;
} CtlItemS32_t;

typedef struct RtkExtROICtlParams
{
	U16 wTop;
	U16 wLeft;
	U16 wBottom;
	U16 wRight;
	U16 bmAutoControls;
}RtkExtROICtlParams_t;

typedef struct RtkExtROICtlItem
{
	//RtkExtROICtlParams_t Min;
	RtkExtROICtlParams_t Max;
	//RtkExtROICtlParams_t Res;
	//RtkExtROICtlParams_t Def;
	RtkExtROICtlParams_t Des;
	RtkExtROICtlParams_t Last;
}RtkExtROICtlItem_t;

typedef union
{
	CtlItemU8_t  CtlItemU8;
	CtlItemS8_t  CtlItemS8;
	CtlItemU16_t CtlItemU16;
	CtlItemS16_t CtlItemS16;
	CtlItemU32_t  CtlItemU32;
	CtlItemS32_t  CtlItemS32;

} CtlAttr_t;

typedef struct Control
{
	U8 Info;                           //GET_INFO response value. would be changed by auto controls
	U8  ChangeFlag;            // change flag, FW use this to determine control change status.
	U8 OpMask;           // support operations
	U8 Len;                  //control size
	CtlAttr_t* pAttr;  //control attribute. MIN, MAX, RES ,DEF and change destination value and current value of the control.
} Ctl_t;

typedef struct
{
	U16 bmHint;
	U8  bFormatIndex;
	U8  bFrameIndex;
	U32 dwFrameInterval;
	//U16 wKeyFrameRate;
	//U16 wPFrameRate;
	//U16 wCompQuality;
	//U16 wCompWindowSize;
	//U16 wDelay;
	U32 dwMaxVideoFrameSize;
	U32 dwMaxPayloadTransferSize;
	//U32 dwClockFrequency;
	//U8  bmFramingInfo;
	//U8  bPreferedVersion;
	//U8  bMinVersion;
	//U8  bMaxVersion;
} VSProbCommCtl_t;

typedef struct
{
	U8 bFormatIndex;
	U8 bFrameIndex;
	U8 bCompressionIndex;
	U32 dwMaxVideoFrameSize;
	U32 dwMaxPayloadTransferSize;
} VSStlProbCommCtl_t ;

typedef struct
{
	U8 byHeader;		// version
	U8 byLen;			// include byHeader
	U32 dwMagictag;		// fix 0x12345678
	U16 wICName;		// exa: 0x5847
	U16 wVID;			// exa: 0x0BDA
	U16 wPID;			// exa: 0x0001
	U32 dwFWVer;			
	U32 dwCusVer;
	// Reserved for customer
	U8  Reserved[12];
} FWVersion_t;

/*
struct VideoFrameMjpeg
{
	U16 wWidth;
	U16 wHeight;
	U32 dwFrameInt;
	U32 dwMinBitRate;
	U32 dwMaxBitRate;
};

struct VideoFrameYUY2
{
	U16 wWidth;
	U16 wHeight;
	U32 dwFrameInt;
	U32 dwMinBitRate;
	U32 dwMaxBitRate;
};
*/
typedef struct        // frame diamension size.
{
	U16 wWidth; // frame width
	U16 wHeight; //frame height
} VideoDimension_t;

typedef struct
{
	U8 byFormatType;
	U8 byaVideoFrameTbl[MAX_VIDEO_FRAME_NUM+1];//the first byte is frame number.
	U8 byaStillFrameTbl[MAX_STILL_FRAME_NUM+1];//the first byte is frame number.
	U16 waFrameFpsBitmap[MAX_RES_TABLE_SIZE];
} VideoFormat_T;

#if (defined _UVC_PPWB_ || defined _UVC_COMMITWB_)
typedef struct
{
	// process unit control
	S16  wBrightness;
	U16  wContrast;
	S16  wHue;
	U16  wSaturation;
	U16  wSharpness;
	U16  wGamma;
	U16  wWhiteBalanceTemp;
	U8  	byWhiteBalanceTempAuto;
	U16  wBackLightComp;
	U16  wGain;
	U8  	byPwrLineFreq;

	// camera terminal control
	U32 	dwExposureTimeAbsolut;
	U8  	byExposureTimeAuto;
	U8  	byLowLightComp;
	S16  wPan;
	S16  wTilt;
	U16 wZoom;
	S16  wRoll;
	U16 wFocusAbsolut;
	U8 byFocusAuto;
#ifdef _RTK_EXTENDED_CTL_
	U16 wRtkExtISPSpecialEffect;
	S16 wRtkExtEVCompensation;
	U8 byRtkExtPreviewLEDOff;
	U16 wRtkExtISO;
#endif
#ifdef _LENOVO_JAPAN_PROPERTY_PAGE_
	U8 g_byAWBRGain_Lenovo;
	U8 g_byAWBGGain_Lenovo;
	U8 g_byAWBBGain_Lenovo;
#endif
} VSPropertyCommCtl_t;	//37Bytes

typedef struct
{
	// commit control
	U8 byCommitFormatIndex;
	U8 byCommitFrameIndex;
	U8 byCommitFPS;
	U16 wTransferSize;
	U16 wLWM;
	U16 wHWM;
} VSBulkCommit_t;	//9Byte
#endif

//global routines
void ControlInitial();
void VCStatusIntPacket_Int0(U8 Originator, U8 CS, U8 Attribute, U8 const * pValue, U8 size);
//id VCStatusIntPacket_main(U8 Originator,U8 CS,U8 Attribute,U8* pValue,U8 size);
U32 GetMaxPayloadTransferSize(U32 FrameSize, U8 Fps, U8 IsHighSpeed);
extern void FacialAEWinSet_XU();
void ControlGetReqProc(const Ctl_t* pCtl, U8 req);
bit ControlSetReqCheckProc(Ctl_t* pCtl);
void VideoCtlRtkExtReq(void);
bit WaitEP0_DATA_Out();
#endif
