#ifndef _CAMSENSORUTIL_H_
#define _CAMSENSORUTIL_H_

#define CTT_INDEX_MAX 6
#define COLOR_R			0x00
#define COLOR_G			0x01
#define COLOR_B			0x02
#define VDINS_BUF_LEN         8

typedef struct
{
	U16 wModeAddr_RegAddr;
	U16 wDataInOut_RegAddr;
} SnrVarAccessProp_t;


void FT2ModelSetFormatFps(U8 Fps);
U8 Read_MI_Vairable ( U16 dWidth_driverID_offset,U16* pwData) ;
U8 Write_MI_Vairable ( U16 dWidth_driverID_offset,U16 wData) ;
void Write_SenReg_Mask(U16 address, U16 wData, U16 wMask);
void WriteSensorSettingBB(U16 length, t_RegSettingBB const  pSetting[]);
void WriteSensorSettingWB(U16 length, t_RegSettingWB const pSetting[]);
void WriteSensorSettingWW(U16 length, t_RegSettingWW const pSetting[]);
void WriteMIVarSettingWW(U16 length, t_RegSettingWW pSetting[]);
void WriteSamVarSettingWW(U16 length, t_RegSettingWW pSetting[]);
void CfgDefaultControlAttr(void);
void CfgFT2ControlAttr(void);

#ifdef _MIPI_EXIST_
#else
void Init_CCS(void);
#endif
void Sensor_POR_Common(void);
void Sensor_POR(void);
//void SetSnrpinPolarity(bySensorSignalPolarity);
#ifndef _MIPI_EXIST_
void SetBkWindowStart(U16 wHorStart, U16 wVerStart);
#endif
U8 MI_PollWaitRefreshDone(U8 byTimeout);
void LoadVdSensorSetting(U8* pbyaSetting,U8 bySettingLength);
void Write_MI_Vairable_Mask(U16 address, U16 wData, U16 wMask);
U8 MapWBTemp2MicronSensorWBPositon(U16 wSetValue);
U16 MapMicronSensorWBPositon2WBTemp(U8 byPosition);
OV_CTT_t OV_SetColorTemperature(U16 wSetValue);
//U16 OV_GetColorTemperature(OV_CTT_t ctt, U8 byColor);
U16 GetOVSensorAEGain(void);
U16 CalAntiflickerStep(U8 byFps, U8 byFrq, U16 wRowNumber);
OV_BandingFilter_t  OV_SetBandingFilter(U8 byFps);
void ENTER_SENSOR_RESET(void );
void LEAVE_SENSOR_RESET(void );
#ifdef _MIPI_EXIST_
void Init_MIPI(void);
void SetBLCWindowStart(U16 wHorStart, U16 wVerStart);
void SetMipiDphy(U8 byLaneSel, U8 byFormat, U8 byDataType, U8 byHSTerm);
void StartMipiAphy(void );
void StopMipiAphy(void );
#endif
void WaitFrameSync(U8 byISPINTNum, U8 byWaitSignal);
void InitSensor(void);
#endif
