#ifndef _CAMPROCESS_H_
#define _CAMPROCESS_H_

#define JPEG_DATA_TYPE_VSTREAM   0
#define JPEG_DATA_TYPE_STILLIMG   1

#define JPEG_COMPRESS_RATIO	6

#define USE_SIE_DROP	0
#define USE_ISP_DROP	1

#define EN_USB_STREAMING 1
#define DIS_USB_STREAMING 0

void StartVideoImage(U8 byUSBStreaming);
void StartStillImage(void );
void StopImageTransfer(U8 byDelayCtrl);

#ifdef _UAC_EXIST_
void StartAudioTransfer(void);
#endif

#ifdef _SENSOR_ESD_ISSUE_
void ResetTransfer(void);
#endif
void StartBackgroundStreaming(void);
void StopBackgroundStreaming(void);
void StartVideoBackend(void);
void NotifyHostMotionDetected(void);
void NotifyHostNoMotionDetected(void);

#endif
