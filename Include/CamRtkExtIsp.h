#ifndef _CAM_RTK_EXT_ISP_H_
#define _CAM_RTK_EXT_ISP_H_

#include "Pc_cam.h"

#ifdef _RTK_EXTENDED_CTL_

//isp special effect
#define ISP_SPECIAL_EFFECT_NORMAL			0x0001
#define ISP_SPECIAL_EFFECT_MONOCHROME 	0x0002
#define ISP_SPECIAL_EFFECT_GRAY			0x0004
#define ISP_SPECIAL_EFFECT_NEGATIVE		0x0008
#define ISP_SPECIAL_EFFECT_SEPIA			0x0010
#define ISP_SPECIAL_EFFECT_GREENISH		0x0020
#define ISP_SPECIAL_EFFECT_REDDISH		0x0040
#define ISP_SPECIAL_EFFECT_BLUISH			0x0080

// EV compensation resolution
#define EVCOMP_SIXTHSTEP		0x0001		// 1/6
#define EVCOMP_QUARTERSTEP	0x0002		// 1/4
#define EVCOMP_THIRDSTEP		0x0004		// 1/3
#define EVCOMP_HALFSTEP		0x0008		// 1/2
#define EVCOMP_FULLSTEP		0x0010		// 1

// ROI Auto Controls
#define ROI_AE_SUPPORT			0x0001
#define ROI_AF_SUPPORT			0x0008

// ROI AE Status
#define ROI_AE_QUIT			0x00
#define ROI_AE_INIT			0x01
#define ROI_AE_START_ADJUST	0x02
#define ROI_AE_ADJUST			0x03
#define ROI_AE_BACK_TO_NORMAL	0x04

// ROI AF Status
#define ROI_AF_QUIT			0x00
#define ROI_AF_INIT			0x01
#define ROI_AF_START_ADJUST	0x02
#define ROI_AF_ADJUST			0x03
#define ROI_AF_BACK_TO_NORMAL	0x04

// ROI Status
#define ROI_QUIT				0x00
#define ROI_INIT				0x01
#define ROI_ADJUST				0x02
#define ROI_STABLE				0x03

// ROI_ST_CONTROL Status
#define ROI_GOING			0x00
#define ROI_COMPLETE_OK	0x01
#define ROI_COMPLETE_FAIL	0x02

// ISO
#define ISO_AUTO 	0x0001
#define ISO_50 		0x0002
#define ISO_80 		0x0004
#define ISO_100 		0x0008
#define ISO_200 		0x0010
#define ISO_400 		0x0020
#define ISO_800 		0x0040
#define ISO_1600 	0x0080
#define ISO_3200 	0x0100
#define ISO_6400 	0x0200
#define ISO_12800 	0x0400
#define ISO_25600 	0x0800

#ifdef _USE_BK_SPECIAL_EFFECT_
void SetISPSpecailEffect(U16 wEffect);
#endif

void BackupAETarget();
void SetEVCompensation(S16 wEVCompValue, S16 wEVCompRes);

U16 ColorTemperature(void);

void RtkExtROIAE(void);
void RtkExtROIAF(void);
void RtkExtROIProcess(void);
void SetROI(U16 wWinTop, U16 wWinLeft, U16 wWinBottom, U16 wWinRight, U16 bmWinControls);

U8 GetROIAEStatus();
U8 GetROIAFStatus();

void SetISO(U16 wISOValue);

void SetPreviewLEDOff(U8 byLEDOff);

#endif	// _RTK_EXTENDED_CTL_

#endif

