/*
*******************************************************************************
*                           Cardreader RTS5151 project
*
*                           Layer:
*                           Module:
*
*
*
*
* File : Define.h
*******************************************************************************
*/
/**
*******************************************************************************
  \file Define.h
  \brief Constant and macro definintion.



* \version 0.1
* \date 2006

*******************************************************************************/

/*=============================================================================

                            EDIT HISTORY FOR MODULE

 when           who         what, where, why
 -----------    ---------   ---------------------------------------------------
 2006/1/13      JinZhong    Create this file

==============================================================================*/


#ifndef	_CAMREG2_H_
#define	_CAMREG2_H_

// hardware register paritial definition
#define  ISP_NLSC_R_ADJ_RATE			0x8109
#define  ISP_NLSC_G_ADJ_RATE			0x810A
#define  ISP_NLSC_B_ADJ_RATE			0x810B

#define ISP_RGB_AE_GAIN				0x833a
#define ISP_RGB_AE_GAIN_NOW			0x833c
#define ISP_RGB_AE_GAIN_CONTROL		0x833e

#define	ISP_HUE_SIN_X					0x841C
#define	ISP_HUE_COS_X					0x841D
#define	ISP_HUE_SIN_Y					0x841E
#define	ISP_HUE_COS_Y					0x841F
#define	ISP_CONTRAST					0x8425
#define	ISP_Y_OFFSET					0x8427
#define	ISP_Y_GAIN						0x8428
#define	ISP_U_GAIN						0x842A
#define	ISP_V_GAIN						0x842C
#define	ISP_YGAMMA_P0					0x8430
#define	ISP_YGAMMA_P1					0x8431
#define	ISP_YGAMMA_P2					0x8432
#define	ISP_YGAMMA_P3					0x8433
#define	ISP_YGAMMA_P4					0x8434
#define	ISP_YGAMMA_P5					0x8435
#define	ISP_YGAMMA_P6					0x8436
#define	ISP_YGAMMA_P7					0x8437
#define	ISP_YGAMMA_P8					0x8438
#define	ISP_YGAMMA_P9					0x8439
#define	ISP_YGAMMA_PA					0x843A
#define	ISP_YGAMMA_PB					0x843B
#define	ISP_YGAMMA_PC					0x843C
#define	ISP_YGAMMA_PD					0x843D
#define	ISP_YGAMMA_PE					0x843E
#define	ISP_YGAMMA_PF					0x843F
#define	ISP_YGAMMA_SYNC				0x8440

#define ISP_AWB_STATIS_CTRL			0x825F

#define ISP_AWB_WIN_ADDR				0x826C

#define ISP_AF_CTRL 					0x8576

#define	ISP_AE_CTRL					0x8280

#define	ISP_AE_ADDR					0x828C
#define	ISP_AE_SUM_L					0x828D
#define	ISP_AE_SUM_H					0x828E
#define ISP_AF_CTRL 					0x8576


#define CCS_CLK_SWITCH		0xFEC0
#define CCS_CONTROL			0xFECD
#define CCS_INT_STS			0xFED8

#define REG_TUNESVA		0xFFA4
#define REG_TUNESVIO		0xFFAA


// hardware register bit definition
// CCS_CLK_SWITCH	0xFEC0
#define CCS_CLK_CHANGE	0x80
#define CCS_CLK_ENABLE		0x40
#define CCS_CLK_DISABLE		0x00
#define CCS_CLK_MASK		0x3F
#define CCS_CLK_SEL_MASK	0x30
#define CCS_CLK_SEL_60M	0x00
#define CCS_CLK_SEL_80M	0x10
#define CCS_CLK_SEL_96M	0x20
#define SSC_CLK_SEL_96M	0x08
#define SSC_CLK_SEL			0x30
#define CCS_CLK_SEL_SSC_48M_DIV2		0x31
#define CCS_CLK_SEL_SSC_24M			0x30
#define SSC_CLK_SEL_DIV4		0x02

#define CCS_CLK_DIVIDER_MASK	0x0F
#define CCS_CLK_DIVIDER_1	0x00
#define CCS_CLK_DIVIDER_2	0x01
#define CCS_CLK_DIVIDER_4	0x02
#define CCS_CLK_DIVIDER_8	0x03
#define CCS_CLK_DIVIDER_16	0x04
#define CCS_CLK_DIVIDER_32	0x05
#define CCS_CLK_DIVIDER_64	0x06
#define CCS_CLK_DIVIDER_128	0x07
#define SSC_FPGA_CLK_SEL_24M       0x08


//CCS_INT_STATS CLEAR	0xFED8
#define CCS_INT_DATA_START_FLAG		0x08
#define CCS_INT_FRM_END_FLAG			0X04
#define CCS_INT_FRM_START_FLAG		0x02
#define CCS_INT_BFR_ABORT_FLAG		0X01

#define DYNAMIC_GAMMA_EN		0x0001
#define DYNAMIC_SHARPNESS_EN	0x0002
#define DYNAMIC_BLC_EN			0x0004
#define DYNAMIC_CCM_BRIGHT_EN			0x0008
#define DYNAMIC_SHARPPARAM_EN	0x0010
#define DYNAMIC_STA_EN				0x0020
#define DYNAMIC_LSC_EN				0x0040
#define DYNAMIC_LSC_CT_EN				0x0080
//#define DYNAMIC_YGAMMA_EN			0x0080	// hemonel 2011-06-01: delete dynamic Ygamma
#define DYNAMIC_UVOFFSET_EN		0x0100
#define DYNAMIC_CCM_CT_EN			0x0200
#define DYNAMIC_HDR_EN				0x0400
#define DYNAMIC_UVCOLORTUNE_EN	0x0800


#define MIPI_DATA_LANE1_EN  0x02
#define MIPI_DATA_LANE0_EN  0x01

#define MIPI_DATA_FORMAT_RAW8   0x00
#define MIPI_DATA_FORMAT_RAW10  0x01
#define MIPI_DATA_FORMAT_YUV    0x02
#define MIPI_DATA_FORMAT_JPEG   0x03
#define MIPI_YUV_TYPE_NULL      0x00
#define MIPI_YUV_TYPE_UYVY      0x00
#define MIPI_YUV_TYPE_VYUY      0x10
#define MIPI_YUV_TYPE_YVYU      0x20
#define MIPI_YUV_TYPE_YUYV      0x30


#define MIPI_DATA_TYPE_YUYV8    0x1E
#define MIPI_DATA_TYPE_YUYV10   0x1F
#define MIPI_DATA_TYPE_RAW8     0x2A
#define MIPI_DATA_TYPE_RAW10    0x2B

#define SetMipiClkNonContinuousMode() {XBYTE[0x86D3] |= 0x08;XBYTE[0x86EC] |= 0x04;}

#define ISP_DATA_END_INT    0x08
#define ISP_DATA_START_INT  0x04
#define ISP_FRM_END_INT     0x02
#define ISP_INT1            0x01
#define ISP_INT0            0x00


#define HSTERM_EN_TIME_00	0x00
#define HSTERM_EN_TIME_11	0x11
#define HSTERM_EN_TIME_22	0x22
#define HSTERM_EN_TIME_33	0x33
#define HSTERM_EN_TIME_44	0x44
#define HSTERM_EN_TIME_55	0x55
#define HSTERM_EN_TIME_66	0x66
#define HSTERM_EN_TIME_77	0x77
#define HSTERM_EN_TIME_88	0x88
#define HSTERM_EN_TIME_99	0x99
#define HSTERM_EN_TIME_AA	0xAA
#define HSTERM_EN_TIME_BB	0xBB
#define HSTERM_EN_TIME_CC	0xCC
#define HSTERM_EN_TIME_DD	0xDD
#define HSTERM_EN_TIME_EE	0xEE
#define HSTERM_EN_TIME_FF	0xFF


#define EN_ANTI_FLICK 0x1
#define EN_RSDH_FMT 0x2
#define EN_TWO_ENDPOINT_TRANSFER 0x4
#define EN_TRAPEZIUM_CORRECTION 0x8

//0x833c ISP_RGB_AE_GAIN_CONTROL
#define ISP_RGB_AE_GAIN_LOAD 0x80
#define ISP_RGB_AE_GAIN_0_FRAME 0x0
#define ISP_RGB_AE_GAIN_1_FRAME  0x1
#define ISP_RGB_AE_GAIN_2_FRAME  0x2
#define ISP_RGB_AE_GAIN_3_FRAME  0x3

//ISP_YOFFSET	8427
#define ISP_BRIGHTNESS_INCREASE	0x00
#define ISP_BRIGHTNESS_DECREASE	0x80

//ISP_AWB_STATIS_CTRL	825F
#define ISP_AWB_STAT_START	0x80
#define ISP_AWB_STAT_STOP		0x40
#define ISP_AWB_STAT_GOING	0x20
#define ISP_AWB_ROUGH_EN		0x08
#define ISP_AWB_FINE_EN		0x04

//ISP_AF_CTRL
#define ISP_AF_STAT_START	0x80
#define ISP_AF_STAT_STOP	0x40
#define ISP_AF_STAT_GOING	0x01

//ISP_AE_CTRL	8280
#define ISP_AE_STAT_START		0x80
#define ISP_AE_STAT_STOP		0x40
#define ISP_AE_ISGOING			0x20
#endif
