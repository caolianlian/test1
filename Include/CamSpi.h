/*
*******************************************************************************
* File : SPI.h
*******************************************************************************
*/
/**
*******************************************************************************
  \file SPI.h
  \brief Serial flash and serial eeprom operations constant and macro definition.


* \version 0.1
* \date 2006

*******************************************************************************/

/*=============================================================================

                            EDIT HISTORY FOR MODULE

 when           who         what, where, why
 -----------    ---------   ---------------------------------------------------
 2008/4/10     cheney	 copy from 5161 project


==============================================================================*/

//	Vendor sub SCSI command for SPI
#ifndef   _CAMSPI_H_
#define  _CAMSPI_H_

//#define SP_SPI_FLASH_ERASE_SECTOR

//#define SP_ATMEL_SPI_FLASH

//#define SP_OTHER_FLASH_BESIDE_MXIC_EON


//	Serial flash OP instruction
//common op code
#define	SF_WREN			0x06
#define	SF_WRDI			0x04
#define 	SF_RDSR			0x05
#define   SF_WRSR                   0X01
#define 	SF_READ 			0x03
#define	SF_FAST_READ		0x0B
#define	SF_FAST_READ_DUAL_OUT	0x3B
#define	SF_FAST_READ_DUAL_INOUT	0xBB

#define    SF_PAGE_PROG        0x02
#define    SF_CHIP_ERASE      0XC7
#define    SF_SECT_ERASE      0XD8

#define    SF_READID0              0x90

#define    SF_READID1              0x9F

#define    SF_READID2              0xAB

//for AT25F512
#define    SF_READID3             0x15

#define    SF_PWR_DOWN       0xB9
#define    SF_RELS_PDWN       0xAB


#define  SST_SF_CHIP_ERASE   0X60
#define  SST_SF_SECT_ERASE   0X20
#define  SST_SF_AAI_PROG       0XAF
#define  SST_SF_EWSR               0x50

#define  GD_SF_CHIP_ERASE   0X60
#define  GD_SECT_ERASE      0X20


//Manufacture ID
#define ST_MANUFACT_ID         0x20
#define MXIC_MANUFACT_ID     0xC2
#define EON_MANUFACT_ID    0x1C
#define AMIC_MANUFACT_ID   0X37
#define AMIC_MANUFACT_ID2   0X42
#define WINBOND_MANU_ID     0xEF
#define PMC_MANU_ID                  0x9D
#define ATMEL_MANU_ID           0x1F
#define  SST_MANUFACT_ID        0xBF
#define GD_MANUFACT_ID 0xC8
#define ADESTO_MANUFACT_ID	0x1F
#define FM_MANUFACT_ID		0xA1
#define PUYA_MANUFACT_ID	0x85


#define PM_SF_SECT_ERASE     0xD7
//ATMEL specified OP code
#define AT26_SF_SECT_ERASE     0x20
//for AT25F512
#define AT25_SF_CHIP_ERASE     0x62

#define SF_SP_NON_PROG   0x00
#define SF_SP_BYTE_PROG  0x01
#define SF_SP_AAI_PROG   0x02
#define SF_SP_PAGE_PROG  0x04

#define SF_CS_SEL_DELINK	0x02


#define    SF_PAGE_LEN		 256
#define    SF_PAGE_LEN_32	 32
#define    SF_PAGE_MASK_32	 0x1F

//M25P20,SST25LF020, STM95128_W,PM25LV512
#define SF_SRWD                       0x80
#define SF_BP1                          0x08
#define SF_BP0                          0x04
#define SF_WEL 			 0x02
#define SF_WIP                          0x01





#define	SPIEEP_WREN			0x06
#define	SPIEEP_WRDI			0x04
#define 	SPIEEP_RDSR			0x05
#define    SPIEEP_WRSR                    0X01
#define 	SPIEEP_READ 			0x03
#define    SPIEEP_WRITE                  0x02



#define SPIEEP_PAGE_LEN     32

#define   NV_TYPE_I2C_NON      0x0

#define   NV_TYPE_I2C_EEP_1BYTE_ADDR      0x11
#define   NV_TYPE_I2C_EEP_2BYTE_ADDR      0x12

#define   NV_TYPE_SPI_EEP      0x20
#define   NV_TYPE_SPI_SF         0x30




#define   NV_TYPE_L3L_EEP8    0x40
#define   NV_TYPE_L3L_EEP16  0x80

//(data bit len>>1)<<4 + (addr bit len)

#define  L3L_EEP_ADDR_LN_MASK 0x0F


#define SPI_RESET_MODULE()  { XBYTE[SPI_CONTROL] |= SPI_RESET; }

#define SPI_AUTO_END()  {XBYTE[SPI_OE_CTL] &= ~(SPI_CS_SEL_GPIO4|SPI_CS_SEL|SPI_SCK_SEL|SPI_MISO_SEL|SPI_MOSI_SEL);\
							XBYTE[SPI_CONTROL] &= (~SPI_PAD_SPI_SEL);}
#define SPI_AUTO_START()   {XBYTE[SPI_CONTROL] = CS_POLARITY_LOW|DTO_MSB_FIRST|SPI_MODE0|SPI_PAD_SPI_SEL|SPI_AUTO_MODE;\
							XBYTE[SPI_OE_CTL] = SPI_CS_SEL_GPIO4|SPI_CS_SEL|SPI_MISO_SEL|SPI_MOSI_SEL|SPI_SCK_SEL;}
							
#define CHECK_SPI_CRC_CLK()   (XBYTE[BLK_CLK_EN]&SPI_CRC_CLK_EN)
#define START_SPI_CRC_CLK()   {XBYTE[BLK_CLK_EN] |= SPI_CRC_CLK_EN;}
#define STOP_SPI_CRC_CLK()    {XBYTE[BLK_CLK_EN] &= (~SPI_CRC_CLK_EN);}


void ChangeSPIClockDivider();
void ChangeSPIClockSink(U8 byType);// hemonel 2009-12-29: delete spi eeprom support
bit SPI_WB_EraceSFSector(U32 DAddr);
bit SPI_WB_Program(U32 dwDAddr, U8 bySAddr, U8 byLength);
bit SPI_WB_WriteStatus(U8 byValue);
bit SPI_WB_ReadStatus(void);
#if (defined _PPWB_ADDR_0X10000_ || (defined _SPI_FAST_READ_) || (defined _IQ_TABLE_CALIBRATION_))
bit SPI_WB_Real_Read(U32 DAddr, U8 byLength, U8 byMode);
#endif
bit SPI_WB_Read(U32 DAddr, U8 byLength, U8 bySubCmd);
U8 SPI_ReadFlash(U8 DLen,U16 DAddr,U32 SAddr,U8 byRMode);
U8  SFReadFlashID(void );
void SPI_InitPara(void);
U8 SPI_WriteFlashTag(void );
U8 SPI_WriteFlash(U8 DLen, U16 SAddr, U32 DAddr);
U8 SPI_EraceSFChip(void );
U8 SPI_ReadFlashID(U16 DAddr);
U8 SPI_RDSRSFlash(U16 DAddr);
U8 SPI_WRSRSFlash(U16 SAddr);
#endif
