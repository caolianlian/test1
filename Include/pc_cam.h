#ifndef _PC_CAM_H_
#define _PC_CAM_H_

/**********************
******Mode option ******
**********************/
#define _IC_CODE_		// ASIC only
#define _CACHE_MODE_ 	//Cache mode only


/**********************
******System options ****
**********************/
#define _WATCH_DOG_ENABLE_
#define _MCU_FAST_
//#define _SENSOR_ESD_ISSUE_
#define _SAVE_POWER_   //Default open it.Jimmy.2011.11.21.add this macro for save power
//#define _OSC_RC_MODE_ //Default is LC mode
#define _I2C_NORMAL_	//Default open it.it used for normal i2c read and write
//#define _SPI_CLK_FIX_15MHZ_

/**********************
******IC option	 ******
**********************/
#define _MIPI_EXIST_
//#define _REAR_CAMERA_	// this macro define camera location. rear camera need open this macro, fornt camera need disable this macro
#define _RTS5840_		0x01
#define _RTS5832_		0x02
#define _RTS5829B_		0x04
#define _CHIP_ID_		_RTS5840_
#define _PACKAGE_QFN40_	//Open this macro,package is QFN40, otherwise is QFN32;default open it.RTS5832 same with RTS5840

/*************************
******USB option ***********
**************************/
//#define _STILL_IMAGE_METHOD_1
//#define _UAC_EXIST_ 
//#define _UBIST_TEST_   // hemonel 2009-12-01: add UBIST test for DELL
//#define _DELL_EXU_
//#define _AUTO_UVC_BANDWIDTH_
#define _PH_PTS_EN_
#define _PG_EN_		// power gating enable	// hemonel 2010-11-08:enable power gating for test
#define _UVC_PPWB_	// property page write back enable
//#define _HID_BTN_		//Default.close it.Use GPIO 0 as Test GPIO.External should pull up.when short to GND,button Pressed
//#define _UVC_1V1_		//UVC1.1 Support
//#define _PPWB_ADDR_0X10000_	//Default.close it.property page and uvc commit save serialflash address at 0x10000,need 128K serialflash
//#define _IQ_TABLE_CALIBRATION_		//Default close it.it used at 128KByte serialflash to patch IQ table setting.
//#define _REMOTE_WAKEUP_SUPPORT_


/**************************
**** ISP options***********
**************************/
//#define _CUSTOMER_ISP_
#ifdef _CUSTOMER_ISP_
#define _AF_ENABLE_
#endif

//#define _JPEG_RATE_ADJ_		// Default close it. used for variable mjpg qtable rate for fhd/hd/vga size

//#define _IQ_TABLE_CALIBRATION_		//Default close it.it used at 128KByte serialflash to patch IQ table setting.
#ifdef _IQ_TABLE_CALIBRATION_		//Default close it.it used at 128KByte serialflash to patch IQ table setting.
//offset4
#define IQ_CAL_CLSC_FHD		0x01
#define IQ_CAL_MLSC_FHD		0x02
//#define IQ_CAL_CLSC_5M		0x04
//#define IQ_CAL_MLSC_5M		0x08
#define IQ_CAL_DYN_LSC		0x10		//dynamic lsc rate 6 color temperature rate
//#define IQ_CAL_CLSC_8M		0x20
//#define IQ_CAL_MLSC_8M		0x40
#endif

#define _ENABLE_ANTI_FLICK_
//#define _TCORRECTION_EN_
#define _DECOLOR_TONE_ENABLE_
//#define _NEW_AUTO_LSC_FUNCTION_
//#define _AWB_SAMEBLOCK_
//#define _FACIAL_EXPOSURE_	// enable face detection for Lenovo
//#define _FACIALAEWINSET_XU_
//#define _AUTOBANDING_FFT_TEST_
//#define _LENOVO_JAPAN_PROPERTY_PAGE_
//#define _IMX076_320x240_60FPS_ //Open _IMX076_320x240_60FPS_(in Pc_cam.h) to return 1280*480 for IMX076's VGA_FRM.

#ifdef _ENABLE_ANTI_FLICK_
#define _STATIC_FLICK_DETECT_
//#define _FLICK_DETECT_FPS_THRESHOLD_
#define _DEFENT_OBJECT_MOVEMENT_
//#define _FLICK_DETECT_GAIN_THRESHOLD_
#endif
#ifdef _TCORRECTION_EN_
//#define _TCORRECTION_TEST_BY_BRIGHTNESS_
#endif
//#define _SCENE_DETECTION_
#ifdef _SCENE_DETECTION_
#define _SCENE_CHANGE_AE_BOUNDARY_
#endif
#define _Discrate_Frame_Rate_ 	// use discrate frame rate control, like 7.5, 15, 30
//#define _MSOC_TEST_
#define _ENABLE_MJPEG_
#define _ENABLE_M420_FMT_
//#define _USE_BK_SPECIAL_EFFECT_
#define _USE_BK_HSBC_ADJ_
//#define _ENABLE_OUTDOOR_DETECT_

//#define _BLACK_SCREEN_	// Black Screen code option
//#define _STILL_IMG_BACKUP_SETTING_

#define _AE_NEW_SPEED_ENABLE_

//#define _AF_ENABLE_
#ifdef _AF_ENABLE_
//#define _AF_TEST_
#endif
//#define _AE_ST_WIN_CROP_
//#define _LENOVO_IDEA_EYE_
#ifdef _LENOVO_IDEA_EYE_
#define _LENOVO_IDEA_EYE_EXTENDEDUNIT_
#define _LENOVO_IDEA_EYE_20_DEGREE
//#define _IDEA_EYE_QVGA_RESOLUTION_
#endif

#ifdef _CACHE_MODE_
#if (_CHIP_ID_ & _RTS5832_)
#define _BULK_ENDPOINT_
#define _USB2_LPM_
#define _EP0_LPM_L1_	//Default open it.Used for support EP0 LPM L1
#define _NF_RESTORE_	//Default open it.FIX suspend->resume LPM L1 not response issue.
#ifdef _BULK_ENDPOINT_
#define _UVC_COMMITWB_ //Default Open it. save commit result in ppwb ,in bulk mode
#define _NOLPML1_HANG_	//Open LPM L1,but insert to nomal bulk, bandwith not enough,preview hang issue
#define _HASWELL_LPM_LAG_	//HASWELL YUYV preview set ClearFeature Issue,maybe Sleep time at least 1mS
#define _S3_RESUMKEK_		//metro preview can not go to suspend issue,device will resumeK when go to suspend
#endif
#endif

#if (_CHIP_ID_ & _RTS5829B_)
#define _BULK_ENDPOINT_
#define _USB2_LPM_
#define _EP0_LPM_L1_	//Default open it.Used for support EP0 LPM L1
#define _NF_RESTORE_	//Default close it.Wait for Verify.FIX suspend->resume LPM L1 not response issue.
#define _HW_HDR_ENABLE_
#ifdef _BULK_ENDPOINT_
#define _UVC_COMMITWB_ //Default Open it. save commit result in ppwb ,in bulk mode
#define _NOLPML1_HANG_	//Open LPM L1,but insert to nomal bulk, bandwith not enough,preview hang issue
#define _HASWELL_LPM_LAG_	//HASWELL YUYV preview set ClearFeature Issue,maybe Sleep time at least 1mS
#define _S3_RESUMKEK_		//metro preview can not go to suspend issue,device will resumeK when go to suspend
#endif
#ifdef _USB2_LPM_
//#define _L1_SAFE_TIME_	//Default close
#endif
#endif

#ifndef _BULK_ENDPOINT_	// must put after _BULK_ENDPOINT_
#define _WIN81METRO_BALCKSCREEN_		//Jimmy.20140410.Note>at some platform,win8.1&MetroUI,swtich maybe black screen.this macro move StartVideoImage to alt setting.
										// In Token do nothing for preview. have not found root cause. but do this have effective. other isp vendor also change like this.
#endif

/**************************
**** Test options***********
**************************/
//#define _ENABLE_OLT_
//#define _SLB_TEST_
//#define EP0_LPM_L1_TEST		//Default close it.it used to test 1ms go to lpm l1
//#define _FT2_SENSOR_TEST_EN_		// open this macro only when use FT2 sensor test, otherwise close this macro
//#define _NEW_FT2_MODEL_
//#define _FT2_RS_MODEL_
#ifdef _FT2_RS_MODEL_
//#define _RS0509_PREVIEW_ISSUE_	
#endif
//#define _SFR_ACCESS_					//Default colse it. only used for test SFR Read and Write
//#define _RTK_EXTENDED_CTL_
#endif
//#define _USE_SENSOR_HI165_

/**************************
**** debug options***********
**************************/
//#define _EN_PHY_DBG_MODE_
//#define _SP_VENDOR_INS_CMD_
//#define _GPIO_STS_INT_TEST_
//#define _TEST_PHY_GAIN_	//only enable this macro when tuning non-crystal PHY gain table.
//#define _SPI_FAST_READ_		//default close it.it only used to test serial flash fast read mode.

/****************************
**** VID/PID, FW_Version options*
****************************/
#define _RT_VID_L_       0xda
#define _RT_VID_H_      0x0b

#if (_CHIP_ID_ & _RTS5840_)	//RTS5840
#define _5840_PID_L_   0x40
#define _5840_PID_H_   0x58
#endif	//#ifdef _RTS5832_

#if (_CHIP_ID_ & _RTS5832_)
#define _5840_PID_L_   0x32
#define _5840_PID_H_   0x58
#endif

#if (_CHIP_ID_ & _RTS5829B_)
#define _5840_PID_L_   0x29
#define _5840_PID_H_   0x58
#endif	//#ifdef _RTS5832_

/*
#ifdef _IMX076_320x240_60FPS_
#define	_FW_MAIN_VER_	0x01
#define	_FW_SUB_VER_	0x61
#else
#define	_FW_MAIN_VER_	0x41	// Firmware version is used in ordering part number
#define	_FW_SUB_VER_	0x20 	  // cache code map modify, 0x0000~0xE7FF: code space, 0xE800~0xEFFD: configure,0xEFFE~0xEFFF:cache code tag,0xF000~0xFFFF:pp write back
#endif
*/

#define	FW_TRK_VER	    0x0007	// release version
#define	FW_CUS_LIB_VER	0x0000	// revision version 
#define	FW_TRUNK_VERSION	(((U32)FW_TRK_VER<<16)|FW_CUS_LIB_VER)

#define AP_MAIN_VERSION 	0x10
#define AP_SUB_VERSION		0x01

#define    _FWDL_PID_H_      0x58
#define    _FWDL_PID_L_      0xFF
/**************************
**** UART print debug options*******
**************************/
/*debug support*/
//#define _RS232_DEBUG_
//#define _ASSERT_ENABLE_
//#define _DEBUG_3A_USE_GPIO_

#ifdef _RS232_DEBUG_
#define RS232_TEST
//#define FUNC_IN_OUT_MSG_ENABLE
//#define LINE_RUN_MSG
//#define DBG_MSG_ENABLE
//#define DBG_sCache_MSG_ENABLE
//#define CFG_MSG_ENABLE
//#define DBG2_MSG_ENABLE
//#define MSG_ENABLE
//#define WARN_MSG_ENABLE
//#define ERR_MSG_ENABLE
//#define ISP_MSG_ENABLE
//#define DBG_VD_MSG_ENABLE
//#define FCALL_DBG_MSG_ENABLE
//#define AE_MSG_ENABLE
//#define AF_MSG_ENABLE
//#define AWB_MSG_ENABLE
//#define AB_MSG_ENABLE
//#define EPE_MSG_ENABLE
//#define DBG_SENSOR_MSG_ENABLE
//#define DBG_OV7670_MSG_ENABLE
//#define DBG_OV7690_MSG_ENABLE
//#define MIPI_MSG_ENABLE
//#define DBG_BOOT_ENABLE
//#define DBG_MCU_SPDUP_ENABLE
//#define ISO_MSG_ENABLE
//#define IDEAEYE_MSG_ENALBE
#endif
#endif
