#ifndef _CAMISP_H_
#define _CAMISP_H_

#define NO_CCM	0
#define DAY_CCM	1
#define A_CCM	2

#define NO_NR 0
#define LOW_LIGHT_NR 1
#define NORMAL_LIGHT_NR 2

//chip feature bitmap definition
#define  FEATURE_BK_ISP_INTERPOLATION                     (0x0001<<0)

//#define CCM_ENABLE 	0x01 // hemonel 2009-12-10: RTS5803 no CCM
//#define NR_ENABLE	0x02	// hemonel 2009-12-10: RTS5803 no NR
#define BLACK_PATTERN_ENABLE	0x04

#define RAW_MODE			0
#define YUV422_MODE		1
#define MJPG_MODE			2

void ExposureTimeSet(U32 dwSetValue);
void ExposureTimeAutoSet(U8 bySetValue);
void LowLightCompSet(U8 bySetValue);
void WhiteBalanceTempSet(U16 wSetValue);
void BrightnessSet(S8 sbySetValue);
void ContrastSet(U8 bySetValue);
void SaturationSet(U8 bySetValue);
void HueSet(S16 swSetValue);
void SharpnessSet(U8 bySetValue);
void GammaSet(U16 wSetValue);
void BackLightCompSet(U8 bySetValue);
void GainSet(U8 bySetValue);
void PwrLinFreqSet(U8 bySetValue);
void WhiteBalanceTempAutoSet(U8 setValue);
void WhiteBalanceComponentSet(U32 dwSetValue);
void WhiteBalanceComponentAutoSet(U8 bySetValue);
void PanTiltSet(S8 sbyPanSetValue, S8 sbyTiltSetValue);
void ZoomSet(U8 bySetValue);
void RollSet(U8 bySetValue);
void TrapeziumCorrectionSet(S8 sbySetValue);
#ifdef _USE_BK_SPECIAL_EFFECT_
void SetBKSpecailEffect(U8 byEffect);
#endif
//void SetBKNoiseReduction(U16 wWidth, U8 byFlag); // hemonel 2009-12-10: delete for RTS5803 no NR module
void IspSwitchPro(void);
void ParaWriteBackToFlash(void );
void PropertyPageWritecackToEeprom(void);
void LoadUVCPPParam(void);
void LoadUVCPPParam_Last(void);
void	SetBkSaturation(U8 bySetValue);
void SetBkBrightness(S8 sbySetValue);
void	SetBkContrast(U8 bySetValue);
void SetBkSharpness (U8 bySetValue);
void	SetBkHue (S16 swSetValue);
void SetGainParam(U8 byGain);
void SetGammaParam(U16 wSetValue);
void FocusSet(U16 wSetValue);
void FocusAutoSet(U8 bySetValue);
void SetFocusAuto(U8 bySetValue);
void SetFocusAbsolute(U16 wSetValue);
void	SetBkHueRT(S16 swSetValue);
void SetISPAEGain(float fTotalGain, float fSnrGain, U8 byDelayFrm);
U8 MappingSharpness(U8 bySetValue);
#ifdef _IQ_TABLE_CALIBRATION_
void LoadIQTablePatch(void );
#endif

void CusISPInit(void);
void CusAWBStaticsStart(void);
void CusAEStaticsStart(void);
void CusAFStaticsStart(void);
void CusAWBAlgorithm(void);
void CusAEAlgorithm(void);
void CusAFAlgorithm(void);
void CusCalledPerFrame(void);

#endif
