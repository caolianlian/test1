#ifndef _CAMUSB_H_
#define _CAMUSB_H_
#include "pc_cam.h"
#include "camutil.h"
#include "CamUVC.h"
#ifdef _UAC_EXIST_
#include "CamUAC.h"
#endif


#define	USBD_ATTR_NO_RWU		0x80
#define	USBD_ATTR_RWU	  (0x80|0x20)
#define	USBD_MAXPOWER	250
#define VS_IF_MAX_SETTING 0x8



#define	REQUEST_TYPE		0x60
#define	STANDARD_REQUEST	0x00
#define	CLASS_REQUEST		0x20
#define	VENDOR_REQUEST		0x40

// class request based on the Bulk-Only Mass Storage
#define	CLASS_RESET	0xff


// vendor request for download mode
#define	RT_DOWNLOAD_IMAGE		0x1
#define	RT_VERIFY_IMAGE			0x2
#define	RT_ERASE_ENTIRE_IMAGE	0x3
#define	RT_GET_LASTOP_RESULT	0x4
#define	RT_RESTART_PROCESS		0x5
#define	RT_RESTART_WITHOUT_TAG	0x6

// standard request based on the USB 1.0 specification
#define USB_REQUEST_GET_STATUS			0x00
#define USB_REQUEST_CLEAR_FEATURE		0x01
#define USB_REQUEST_SET_FEATURE			0x03
#define USB_REQUEST_SET_ADDRESS			0x05
#define USB_REQUEST_GET_DESCRIPTOR		0x06
#define USB_REQUEST_SET_DESCRIPTOR		0x07
#define USB_REQUEST_GET_CONFIGURATION	0x08
#define USB_REQUEST_SET_CONFIGURATION	0x09
#define USB_REQUEST_GET_INTERFACE		0x0A
#define USB_REQUEST_SET_INTERFACE		0x0B
#define USB_REQUEST_SYNC_FRAME			0x0C

// standard descriptor (table 9-5)
#define USB_DEVICE_DESCRIPTOR_TYPE                0x01
#define USB_CONFIGURATION_DESCRIPTOR_TYPE         0x02
#define USB_STRING_DESCRIPTOR_TYPE                0x03
#define USB_INTERFACE_DESCRIPTOR_TYPE             0x04
#define USB_ENDPOINT_DESCRIPTOR_TYPE              0x05
/**********Modified by Alan 12/19/02**************/
#define USB_DEVICE_QUALIFIER_DESCRIPTOR_TYPE	  0x06
#define USB_OTHER_SPEED_CONFIGURATION_TYPE	  0x07
#define USB_POWER_DESCRIPTOR_TYPE		  0x08
#ifdef _USB2_LPM_ // JQG_add_2010_0415_
#define	USB_BOS_DESCRIPTOR_TYPE				0x0F
#endif
#ifdef _HID_BTN_
#define	USB_HID_CLASS_DESCRIPTOR				0x21				
#define	USB_HID_REPORT_DESCRIPTOR			0x22	
#endif

/*USB vendor request*/
#define USB_VDREQUEST_RESET                    (0x01)
#define USB_VDREQUEST_ERASE_IMAGE     (0x02)
#define USB_VDREQUEST_WRITE_TAG          (0x03)
#define USB_VDREQUEST_DOWNLOAD_IMAGE (0x04)
#define USB_VDREQUEST_READ_IMAGE        (0x05)
#define USB_VDREQUEST_GET_STATUS         (0x06)

#define   DEV_STATUS_REMOTE_WAKEUP		0x02



// string descriptor index
#define I_LANG_ID        	0x0
#define I_PRODUCT        	0x1
#define I_SERIALNUMBER   	0x2
#define I_MANUFACTURER      0x3
#define I_CONFIGURATION  	0x4
//#define I_VIDEOFUNCTION     0x05		// in camuvc.h
#define I_STDFWCFG               0x10
#define I_STDFWIF                   0x11
#ifdef _UAC_EXIST_
#define I_PRODUCT_MIC       	0x06	//darcy_lu 2010-04-23: change descriptor from 0x55 to 0x06 , for on Win7 os, device management warning
#endif
#define I_RTK_EXTENDED_CTL_UNIT	0x07

// feature selector
#define	ENDPOINT_HALT		0x0
#define	DEVICE_REMOTE_WAKEUP	0x1


#define DESC_SIZE_DEV       18
#define DESC_SIZE_CONF      9
#define DESC_SIZE_INTF      9
#define DESC_SIZE_EP       7
#define DESC_SIZE_LANGID	    4
#ifdef _HID_BTN_
#define DESC_SIZE_HID	9
#define DESC_SIZE_REPORT	24
#endif  



#define MAX_BULK_SIZE_HS	    512
#define MAX_BULK_SIZE_FS	    64



/************************/

#define USB_VDREQUES_SET_THANSFER_MODE_RTS5827	0x70
#define USB_VDREQUES_THANSFER_MODE_ORIGINAL 0
#define USB_VDREQUES_THANSFER_MODE_60FPS    1

#define USB_VDREQUES_FT2_GET_STATISTICS	0x72

#ifdef _UBIST_TEST_
// hemonel 2009-12-01: add ubit test for DELL
// UBIST return codes
#define UBIST_RC_PASS           5
#define UBIST_RC_ABORT          1
#define UBIST_RC_INVALID_PARAM  2
#define UBIST_RC_HOST_ERR       3
#define UBIST_RC_NO_START       4
#define UBIST_RC_ERROR		0x10

// Test IDs.  Valid values are 0x1 to 0xFF.
#define UBIST_TEST1_ID 0x1
#define UBIST_TEST2_ID 0x2
#define UBIST_TEST3_ID 0x3
#define UBIST_TEST4_ID 0x4
#define UBIST_TEST5_ID 0x5
#define UBIST_TEST6_ID 0x6

// UBIST status packet defines.
#define UBIST_STAT_RUNNING 0x80
#define UBIST_STAT_PROGRESS_MASK 0x7F

// USB BIST commands.
#define UBIST_CMD_START_TEST    1
#define UBIST_CMD_ABORT         2
#define UBIST_CMD_ENABLE_BIST   3
#define UBIST_CMD_DISABLE_BIST  4
#define UBIST_CMD_GET_STATUS    5

// UBIST Command ID
#define USB_VDREQUEST_BIST_CMD	0x10

#ifdef _UAC_EXIST_
// UBIST string descriptor index
#define I_UBIST_DEVICENAME		0x07
#define I_UBIST_TESTNAME1		0x08
#define I_UBIST_TESTNAME2		0x09
#define I_UBIST_TESTNAME3		0x0a
#define I_UBIST_TESTNAME4		0x0b
#define I_UBIST_TESTNAME5		0x0c
#define I_UBIST_TESTNAME6		0x0d
#else
// UBIST string descriptor index
#define I_UBIST_DEVICENAME		0x06
#define I_UBIST_TESTNAME1		0x07
#define I_UBIST_TESTNAME2		0x08
#define I_UBIST_TESTNAME3		0x09
#define I_UBIST_TESTNAME4		0x0a
#define I_UBIST_TESTNAME5		0x0b
#define I_UBIST_TESTNAME6		0x0c
#endif
#endif // _UBIST_TEST_

#ifdef _USB2_LPM_ // JQG_add_2010_0415_

typedef struct
{
	unsigned char	byLength;					// bLength
	unsigned char	byDescriptorType;  			// bDescriptorType
	unsigned char	byTotalLength_Lo;			// byTotalLength_Lo
	unsigned char	byTotalLength_Hi;			// byTotalLength_Hi
	unsigned char	byNumDeviceCaps;			// bNumDeviceCaps

} USB_BOS_DESCRIPTOR;

typedef struct
{
	unsigned char	byLength;					// bLength
	unsigned char	byDescriptorType;  			// bDescriptorType
	unsigned char	byDevCapabilityType;		// bDevCapabilityType
	unsigned char	byAttributes_0;				// bmAttributes_0, LSB
	unsigned char	byAttributes_1;				// bmAttributes_1
	unsigned char	byAttributes_2;				// bmAttributes_2
	unsigned char	byAttributes_3;				// bmAttributes_3, MSB

} USB_DEVCAP_USB2_EXTENSION_DESCRIPTOR;

typedef struct
{
	unsigned char	byLength;					// bLength
    unsigned char	byDescriptorType;  			// bDescriptorType
	unsigned char	byDevCapabilityType;		// bDevCapabilityType
	unsigned char	byAttributes;				// bmAttributes
	unsigned char	bySpeedsSupported_Lo;		// wSpeedsSupported_Lo, LSB
	unsigned char	bySpeedsSupported_Hi;		// wSpeedsSupported_Hi, MSB
	unsigned char	byFunctionalitySupport;		// bFunctionalitySupport
	unsigned char	byU1DevExitLat;				// bU1DevExitLat
	unsigned char	byU2DevExitLat_Lo;			// wU2DevExitLat_Lo, LSB
	unsigned char	byU2DevExitLat_Hi;			// wU2DevExitLat_Hi, MSB		
} USB_DEVCAP_SUPERSPEED_USB_DESCRIPTOR;

typedef struct
{
	USB_BOS_DESCRIPTOR						sBOSDescriptor;
	USB_DEVCAP_USB2_EXTENSION_DESCRIPTOR	sUSB2ExtensionDescriptor;
}USB3_BOS_DEV_CAPABILITY_DESCRIPTOR;


#endif

#ifdef _DELL_EXU_
//#define XU_CONTROL_UNDEFINED 0x00
#define XU_DEVICE_INFO 0x01
#define XU_LED_CONTROL 0x02
#define XU_AE_MAX_AUTO_GAIN_CONTROL 0x03
#define XU_AE_MAX_EXPOSURE_TIME_CONTROL 0x04
#endif //_DELL_EXU_
#endif
