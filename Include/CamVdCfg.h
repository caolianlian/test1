#ifndef _CAMVDCFG_H_
#define _CAMVDCFG_H_
#include "pc_cam.h"



//#define CACHE_CODE_CFG_BASE  0xE000 _move to camreg.h


#define VD_CFG_INFO_BUF_LEN      64
#define VD_CFG_INFO_BUF_LEN_EXPO  6
#define VD_INVALID_PAGE_IDX  0xFF

//configuration info type code .
#define VD_INFO_TYPE_CFG_ONLY                  0x01
//#define VD_INFO_TYPE_CFG_AND_CODE        0x02
//#define VD_INFO_TYPE_CODE_ONLY               0x03

//#define VD_INFO_TYPE_FT_TEST                      0x04
//#define VD_INFO_TYPE_FT2_TEST                    0x05
#define VD_INFO_TYPE_OLT_TEST                    0x06
#define VD_INFO_TYPE_SLB_TEST                    0x07

// system operation mode definition
#define SYS_OPERATION_MODE_NORMAL         0X00
//#define SYS_OPERATION_MODE_FT                    0X01
//#define SYS_OPERATION_MODE_FT2                  0X02
#define SYS_OPERATION_MODE_OLT                  0X03
#define SYS_OPERATION_MODE_SLB                  0X04

//----------------------------------------
//--vendor config descriptor type code definition.
//----------------------------------------
#define VDCFG_DESC_TYPE_STR_MANUFACTOR   	0x01
#define VDCFG_DESC_TYPE_STR_PRODUCT           	0x02
#define VDCFG_DESC_TYPE_STR_SERIALNUM       	0x03
#define VDCFG_DESC_TYPE_VID_PID                      	0x04
#define VDCFG_DESC_TYPE_SYS                              	0x05
//#define VDCFG_DESC_TYPE_FRAME                         	0x06
#define VDCFG_DESC_TYPE_STR_UBISTNAME            	0x06
#define VDCFG_DESC_TYPE_STR_IADSTR               	0x07
#define VDCFG_DESC_TYPE_APSPEC               0x08
#define VDCFG_DESC_TYPE_UVCPPWB               0x09
#define VDCFG_DESC_TYPE_VIDEOFORMAT          0X0A
#define VDCFG_DESC_TYPE_FRAMERATETABLE    0X0B
#define VDCFG_DESC_TYPE_FRAMERESOLUTION	0x0C
//#define VDCFG_DESC_SUBTYPE_AP_BAR       0x02
#define VDCFG_DESC_TYPE_SLB                        	0x0e
//#define VDCFG_DESC_TYPE_CODE                           	0x0f
#define VDCFG_DESC_TYPE_USBIFTEST                  	0x10
//#define VDCFG_DESC_TYPE_NONCRYSTAL_GAIN_TBL 0x12
//#define VDCFG_DESC_TYPE_SNRISP_SETTING       	0x11
//#define VDCFG_DESC_TYPE_VDREG_SETTING        	0x13
#define VDCFG_DESC_TYPE_SENSOR_INFO			0x14
#define VDCFG_DESC_TYPE_SNR_POWERSWITCHSEQ	0x15
#define VDCFG_DESC_TYPE_CACHE_SNRISP_SETTING       	0x16
#define VDCFG_DESC_TYPE_NFCALIBRATION_GAIN_TBL 		0x17
#define VDCFG_DESC_TYPE_SSC				0x18
#define VDCFG_DESC_TYPE_MIC_CFG				0x19
#define VDCFG_DESC_TYPE_CFG_DESC				0x1A
#define VDCFG_DESC_TYPE_MIC_ARRAY			0x1B


#define VDCFG_DESC_TYPE_DMIC                             0x20
//#define VDCFG_DESC_TYPE_FRAMERATE                 0x21
#define VDCFG_DESC_TYPE_CONTROL_ATTR           0x22
//#define VDCFG_DESC_TYPE_SOC_LSC                      0x23
#define VDCFG_DESC_TYPE_QTABLE                         0X24
#define VDCFG_DESC_TYPE_PREVIEW_DELAY		0x25
//#define VDCFG_DESC_TYPE_EXTENSION_CONTROL	0x26
#define VDCFG_DESC_TYPE_SMALL_CACHE			0x27
#define VDCFG_DESC_TYPE_ISP_BLKPAT			0x28
#define VDCFG_DESC_TYPE_ISP_FUNCTRL		0x2A
#define VDCFG_DESC_TYPE_ISP_AWB			0x2B
#define VDCFG_DESC_TYPE_ISP_MLSC			0x2C
#define VDCFG_DESC_TYPE_ISP_SHARPNESS	0x2D
//#define VDCFG_DESC_TYPE_ISP_INTER			0x2E
#define VDCFG_DEST_TYPE_HWAP_VERSION       0x2F
//#define VDCFG_DESC_TYPE_ISPCTL			0x31
#define VDCFG_DESC_TYPE_ISP_LSC			0x32
#define VDCFG_DESC_TYPE_ISP_CCM			0x33
#define VDCFG_DESC_TYPE_ISP_BLC			0x34
//#define VDCFG_DESC_TYPE_ISP_G1G2F		0x34
//#define VDCFG_DESC_TYPE_ISP_WB			0x35
#define VDCFG_DESC_TYPE_ISP_ZOOMFILTER	0x35
#define VDCFG_DESC_TYPE_ISP_AEC			0x36
#define VDCFG_DESC_TYPE_ISP_WDELAY		0x37
//#define VDCFG_DESC_TYPE_ISP_EDGE			0x37
//#define VDCFG_DESC_TYPE_ISP_NR			0x38
#define VDCFG_DESC_TYPE_ISP_FWVARIABLE	0x38
#define VDCFG_DESC_TYPE_ISP_UVOFFSET		0x39
#define VDCFG_DESC_TYPE_ISP_AE_WEIGHT		0x3A
//#define VDCFG_DESC_TYPE_ISP_DP			0x39
//#define VDCFG_DESC_TYPE_ISP_SUBSAM		0x3A
#define VDCFG_DESC_TYPE_ISP_DARK			0x3B
#define VDCFG_DESC_TYPE_ISP_WBTABLE		0x3C
#define VDCFG_DESC_TYPE_ISP_GAMMA		0x3D
#define VDCFG_ISP_PARAMVARIS				0x3E
//#define VDCFG_DESC_TYPE_ISP_AESPEED		0x3E
#define VDCFG_DESC_TYPE_ISP_YUV2RGB		0x3F

#define ISP_HALFSAMPLECFG_EN  0x01
#define ISP_CIF_SPE_SCALE  0x02
#define ISP_MLSC_EN 0x04
#define ISP_HALFISPCLK_EN 0x08

#define VDCFG_ISP_LSC_EXIST 0x01
#define VDCFG_ISP_MLSC_EXIST 0x04

//#define VDCFG_DESC_TYPE_APSPEC
#define VDCFG_DESC_SUBTYPE_AP_EEPINFO       0x01
#define VDCFG_DESC_SUBTYPE_AP_BARINFO       0x02
//VDCFG_DESC_TYPE_CACHE_SNRISP_SETTING
#define VDCFG_SNRSETTING_PAIR_MODE_REG_BB        0x00
//#define VDCFG_SNRSETTING_PAIR_MODE_REG_BW       0x01
#define VDCFG_SNRSETTING_PAIR_MODE_REG_WB       0x02
#define VDCFG_SNRSETTING_PAIR_MODE_REG_WW      0x03

//#define VDCFG_SNRSETTING_PAIR_MODE_MIVAR_BB        0x10
//#define VDCFG_SNRSETTING_PAIR_MODE_MIVAR_BW       0x11
//#define VDCFG_SNRSETTING_PAIR_MODE_MIVAR_WB       0x12
#define VDCFG_SNRSETTING_PAIR_MODE_MIVAR_WW      0x13

//#define VDCFG_SNRSETTING_PAIR_MODE_SAMVAR_BB        0x20
//#define VDCFG_SNRSETTING_PAIR_MODE_SAMVAR_BW       0x21
//#define VDCFG_SNRSETTING_PAIR_MODE_SAMVAR_WB       0x22
#define VDCFG_SNRSETTING_PAIR_MODE_SAMVAR_WW      0x23

//----------------------------------------
//--vendor config descriptor sub type definition.
//----------------------------------------
//#define VD_SNRISP_REGSETTING_LSC          (0X01<<0)
//#define VD_SNRISP_REGSETTING_CCM         (0X01<<1)
//#define VD_SNRISP_REGSETTING_AWB        (0X01<<2)
//#define VD_SNRISP_REGSETTING_AE            (0X01<<3)
//#define VD_SNRISP_REGSETTING_GAMMA   (0X01<<4)
//#define VD_SNRISP_REGSETTING_MISC        (0X01<<5)
//#define VD_SNRISP_REGSETTING_NRC          (0X01<<6)
//VDCFG_DESC_TYPE_VDREG_SETTING
#define VD_REGSETTING_1STFORMAT                (0x00)
#define VD_REGSETTING_2NDFORMAT                (0x01)
//#define VD_REGSETTING_SETCONFIG1               (0x02)
//#define VD_REGSETTING_SETSTREAMIFNOTZERO       (0x03)
//#define VD_REGSETTING_SETSTREAMIFZERO          (0x04)
//#define VD_REGSETTING_USBIFTEST                (0x05)
//#define VD_REGSETTING_INITSYS                  (0x06)
//#define VD_REGSETTING_SUSPEND                  (0x07)
//#define VD_REGSETTING_RESUME                   (0x08)
//#define VD_REGSETTING_BKISP                    (0x09)
#define VD_REGSETTING_SENSORISP         (0x0A)
// VDCFG_DESC_TYPE_FRAMERATE
#define VCCFG_FRAMERATE_FORMAT_SEL_YUY2       (0x01<<0)
#define VCCFG_FRAMERATE_FORMAT_SEL_MJPG       (0x01<<1)
#define VCCFG_FRAMERATE_FORMAT_SEL_YUV420  (0x01<<2)
#define VCCFG_FRAMERATE_FORMAT_SEL_RAW        (0x01<<3)

//VDCFG_DESC_TYPE_FRAME
#define VDCFG_FRAMESIZE_FORMAT_SEL_YUY2       (0x01<<0)
#define VDCFG_FRAMESIZE_FORMAT_SEL_MJPG       (0x01<<1)
#define VDCFG_FRAMESIZE_FORMAT_SEL_YUV420  (0x01<<2)
#define VDCFG_FRAMESIZE_FORMAT_SEL_RAW        (0x01<<3)



//g_byVendorCfgExistFlag
//#define VDCFG_ISP_LSC_EXIST		0x01
//#define VDCFG_ISP_CCM_SWITCH_EXIST		0x02
#define VDCFG_ISP_AWB_EXIST		0x01
#define VDCFG_ISP_CCM_EXIST		0x02
#define VDCFG_ISP_WBT_EXIST		0x04
#define VDCFG_ISP_GAMMA_EXIST		0x08
#define VDCFG_ISP_EDGE_EXIST		0x10
#define VDCFG_ISP_AEC_EXIST		0x20
#define VDCFG_ISP_BLC_EXIST		0x40
#define VDCFG_ISP_PARAMVARIS_EXIST	0x80
//#define VDCFG_ISP_FILTER_EXIST		0x40
//#define VDCFG_ISP_NR_EXIST		0x80

//g_byVendorCfgExistFlag2
//#define VDCFG_ISP_INTER_EXIST		0x01
//#define VDCFG_ISP_ZOOMFILTER_EXIST 0x02
#define VDCFG_ISP_FUNCTRL 0x04

//info item offset definition
#define  CFG_NV_TYPE_OFFSET              0
#define  CFG_INFO_TAG_OFFSET            1
#define  CFG_INFO_TYPE_OFFSET          2
#define  CFG_INFO_SIZE_OFFSET0        3
#define  CFG_INFO_SIZE_OFFSET1        4
#define  CFG_INFO_DESCNUM_OFFSET 5


#define VD_CFG_INFO_EXIST_FLAG  0XA5



#define USE_INTERNAL_DV18 0x20
#define SP_REMOTE_WAKEUP 0x10


// Back end power supply voltage.
#define BK_PWR_VOLT_MSK    0x07

// block clock auto switch off enable
#define SP_USB_HSRX_PWRDWN_EN  	0x80
#define SP_USB_HSTX_PWRDWN_EN  	0x40
#define SP_SPI_CRC_AUTO_DOWN 		0x20
#define SP_I2C_AUTO_DOWN  			0x10
#define SP_SIECLK_AUTO_DOWN  		0x08
#define SP_USB_HSRCV_PWRDWN_EN	0x04
#define SP_USB_HSDLL_PWRDWN_EN	0x02

#define HD_HEIGHT_800  0x00
#define HD_HEIGHT_720  0x10
#define HD_HEIGHT_MSK  0x10



#define SENSOR_REGSETTING_MODE_BB   0x00
#define SENSOR_REGSETTING_MODE_BW  0x01
#define SENSOR_REGSETTING_MODE_WB  0x02
#define SENSOR_REGSETTING_MODE_WW 0x03




/***E2prom configutraion commond for RTS58XX***/
// upper 5 bits as  instruction
#define VDCFG_INS_SHIFT		2
#define VDCFG_INS_MASK		0xFC
#define VDCFG_OPMODE_MASK	0x03
//vender config commond_delay, unit: 10ms
#define VDCFG_DELAY        			(0x1F<<VDCFG_INS_SHIFT)
//write sensor registers via i2c
#define VDCFG_REG_WR   			(0x00<<VDCFG_INS_SHIFT)
#define VDCFG_REG_WR_MASK		(0x01<<VDCFG_INS_SHIFT)
#define VDCFG_REG_CLRB			(0x02<<VDCFG_INS_SHIFT)
#define VDCFG_REG_STB  			(0x03<<VDCFG_INS_SHIFT)

//write micron sensor variables
#define VDCFG_MIVAR_WR   			(0x04<<VDCFG_INS_SHIFT)
#define VDCFG_MIVAR_WR_MASK		(0x05<<VDCFG_INS_SHIFT)
//clear bit
#define VDCFG_MIVAR_CLRB			(0x06<<VDCFG_INS_SHIFT)
//set bit
#define VDCFG_MIVAR_STB  			(0x07<<VDCFG_INS_SHIFT)

#define VDCFG_MI_REFRESH                    (0x08<<VDCFG_INS_SHIFT)
#define VDCFG_MI_REFRESH_MODE       (0x09<<VDCFG_INS_SHIFT)
#define VDCFG_MI_WAIT_REFRESH_DONE (0X0a<<VDCFG_INS_SHIFT)

#define VDCFG_BKREG_WR                      (0x0b<<VDCFG_INS_SHIFT)
#define VDCFG_BKREG_WR_MASK          (0x0c<<VDCFG_INS_SHIFT)
#define VDCFG_BKREG_CLRB                   (0x0d<<VDCFG_INS_SHIFT)
#define VDCFG_BKREG_STB                      (0x0e<<VDCFG_INS_SHIFT)

#define VDCFG_PHYREG_WR                      (0x0f<<VDCFG_INS_SHIFT)
#define VDCFG_PHYREG_WR_MASK          (0x10<<VDCFG_INS_SHIFT)

#define VDCFG_POLL_REG                         (0x11<<VDCFG_INS_SHIFT)
#define VDCFG_POLL_MIVAR                    (0x12<<VDCFG_INS_SHIFT)
#define VDCFG_POLL_BKREG                    (0x13<<VDCFG_INS_SHIFT)
#define VDCFG_POLL_PHYREG                   (0x14<<VDCFG_INS_SHIFT)



#define SNR_PWRCTL_SEQ_SV18   0x00
#define SNR_PWRCTL_SEQ_GPIO8 0x01
#define SNR_PWRCTL_SEQ_SV28  0x02

#define SNF_PWRCTL_SEQ_MSK_1ST   0x03
#define SNF_PWRCTL_SEQ_MSK_2ND  0x0c
#define SNF_PWRCTL_SEQ_MSK_3RD  0x30


void LoadNVMCfg();
void LoadVendorCfg(void );

U8 GetVdCfgByte(U16 wSAddr);
#endif
