#ifndef _CAMUVC_H_
#define _CAMUVC_H_

#define _UVC_VER_1V1         0x0110
#define _UVC_VER_1V0         0x0100

#define UVC_IS_VIEDO 0x01
#define UVC_IS_STILL 0x00

#define UVC_QQVGA_SIZE 		38400		//160*120*2
#define UVC_QCIF_SIZE 		50688		//176*144*2
#define UVC_QVGA_SIZE		153600		//320*240*2
#define UVC_CIF_SIZE		202752		//352*288*2
#define UVC_VGA_SIZE		614400		//640*480*2

#ifdef _UVC_1V1_
#define DEV_CLOCK_FRQ 15000000	//unused in uvc1.1
#else
#define DEV_CLOCK_FRQ  15000000
#endif

//#define DEV_INTERNAL_HS_DELAY    32
//#define DEV_INTERNAL_FS_DELAY     4

typedef struct Desc_type
{
	U8  type_idx;//type_idx =0 normal, type_idx!=0 VideoStreaming Interface , endpoint desc or other specific desc.
	//high nibble=0xe  interface,
	//high nibble=0xf  endpoint,
	//high nibble= 0x01 , mjpeg frame desc.
	//high nibble = 0x02 , uncompressed frame desc.
	//high nibble = 0x03 , format desc.
	//high nibble = 0x04 , video header desc.
	//others reserved.
	U8* Desc_Str;
} Desc_t;

#define UVC_DESC_NUM_1 (12 + (15+3)*3)

#ifdef _BULK_ENDPOINT_
#define UVC_DESC_NUM_2  (2)  //bulk endpoint and interface
#else
#define UVC_DESC_NUM_2 (7*2)  // (resolution_num+ format_head+stillimage+colormatch)*format_num
#endif

#ifdef _UAC_EXIST_
#define UVC_DESC_NUM_3 2
#else
#define UVC_DESC_NUM_3 0
#endif

#ifdef _DELL_EXU_
#define UVC_DESC_NUM_4 1
#else
#define UVC_DESC_NUM_4 0
#endif

#ifdef _UBIST_TEST_
#define UVC_DESC_NUM_5 1
#else
#define UVC_DESC_NUM_5 0
#endif
#ifdef _FACIALAEWINSET_XU_
#define UVC_DESC_NUM_FACEAE 1 
#else
#define UVC_DESC_NUM_FACEAE 0
#endif

#if defined _RTK_EXTENDED_CTL_ || defined _LENOVO_IDEA_EYE_EXTENDEDUNIT_
#define UVC_DESC_NUM_6 1 // add a new extension unit
#else
#define UVC_DESC_NUM_6 0
#endif

#define UVC_DESC_TOTAL_NUM (UVC_DESC_NUM_1 + UVC_DESC_NUM_2 + UVC_DESC_NUM_3 + UVC_DESC_NUM_4 + UVC_DESC_NUM_5 + UVC_DESC_NUM_FACEAE + UVC_DESC_NUM_6) 


#define MAX_VIDEO_FRAME_NUM 12
#define MAX_STILL_FRAME_NUM   12
#ifdef _IMX076_320x240_60FPS_
#define MAX_RES_TABLE_SIZE      24//23
#else
#define MAX_RES_TABLE_SIZE      23
#endif
#define MAX_FORMAT_TYPE    4

#define FORMAT_TYPE_NC		0x00
#define FORMAT_TYPE_YUY2        0x11
#define FORMAT_TYPE_MJPG        0x22
#define FORMAT_TYPE_YUV420   0x33
#define FORMAT_TYPE_RAW         0x44
#define FORMAT_TYPE_M420   0x55
#define FORMAT_TYPE_RSDH   0x66

#define JPEG_QT_HIGHQUALITY 0
#define JPEG_QT_HIGHCOMPRATE 1


#define HS_JPEG_ENABLE    0x01
#define HS_YUY2_ENABLE   0x02
#define HS_VDFMT_ENABLE   0x04
#define HS_RAWFMT_ENABLE   0x08

#define HS_FORMAT_MSK    0x0f

#define FS_JPEG_ENABLE     0x10
#define FS_YUY2_ENABLE    0x20
#define FS_VDFMT_ENABLE  0x40
#define FS_RAWFMT_ENABLE  0x80

#define FS_FORMAT_MSK      0xf0



// White balance type
#define D_IDX_PUAWB_NONE      0X00
#define D_IDX_PUAWB_TEMP      0X01
#define D_IDX_PUAWB_COMP      0X02



#define VIDEO_STANDARDS 0x00
/*D0: None
D1: NTSC 每 525/60
D2: PAL 每 625/50
D3: SECAM 每 625/50
D4: NTSC 每 625/50
D5: PAL 每 525/60
D6-D7: Reserved. Set to zero
*/

//#define MAX_FORMAT_IDX   2
//#define MAX_FRAME_IDX    12
//#define MAX_CMPRESSPATTERN_IDX    8


//#define VIDEO_FORMAT_IDX_HS_YUY2    0x02
//#define VIDEO_FORMAT_IDX_HS_MJPG    0x01

//#define VIDEO_FORMAT_IDX_FS_YUY2    0x01
//#define VIDEO_FORMAT_IDX_FS_MJPG    0x02
#define AUTOCONTROL_UPDATE_TIME   20




#define  MAX_VCINTEP_PKT_SIZE    0x10
#define  MAX_EP0_PKT_SIZE            0x40

// Interface idx
#define  IF_IDX_VIDEOCONTROL     0x00
#define  IF_IDX_VIDEOSTREAMING 0X01
//Endpoint idx
#define EP_IDX_VIDEO_INT             0x03
#define EP_IDX_VIDEO_STREAM     0x01
#ifdef _HID_BTN_
#ifdef _UAC_EXIST_
#define IF_IDX_HID		0x04
#else
#define IF_IDX_HID		0x02
#endif
#define EP_IDX_HID_INT	0x04
#endif

#ifndef _FACIALAEWINSET_XU_
//Entity Identify
#define  ENT_ID_CAMERA_IT               0x01
#define  ENT_ID_PROCESSING_UNIT  0X02
#define  ENT_ID_OUTPUT_TRM             0X03
//Jimmy.20130604.Extension Unit Dbg must keep at 0x04,the same to Rom Code;
//Otherwise, fast reset to rom and use extension unit will fail
#define  ENT_ID_EXTENSION_UNIT_DBG  0x04	
#define  ENT_ID_EXTENSION_UNIT_UTIL  0x05
#define  ENT_ID_RTK_EXTENDED_CTL_UNIT	 0x06
#define ENT_ID_EXTENSION_UNIT_XU1	0x07

//entity sourceID
#define SRC_ID_PROCESSING_UNIT   ENT_ID_CAMERA_IT
#define SRC_ID_EXTENSION_UNIT_XU1	ENT_ID_PROCESSING_UNIT
#ifdef _DELL_EXU_
#define SRC_ID_EXTENSION_UNIT_DBG   ENT_ID_EXTENSION_UNIT_XU1 //ENT_ID_CAMERA_IT
#else
#define SRC_ID_EXTENSION_UNIT_DBG   ENT_ID_PROCESSING_UNIT //ENT_ID_CAMERA_IT  
#endif
#define SRC_ID_RTK_EXTENDED_CTL_UNIT	ENT_ID_EXTENSION_UNIT_DBG
#if defined _RTK_EXTENDED_CTL_ || defined _LENOVO_IDEA_EYE_EXTENDEDUNIT_
#define SRC_ID_OUTPUT_TRM				ENT_ID_RTK_EXTENDED_CTL_UNIT 
#else
#define SRC_ID_OUTPUT_TRM				ENT_ID_EXTENSION_UNIT_DBG 
#endif
#else

#define  ENT_ID_CAMERA_IT               0x01
#define ENT_ID_EXTENSION_UNIT_FACEAE 0x02
#define  ENT_ID_PROCESSING_UNIT  0X03
//Jimmy.20130604.Extension Unit Dbg must keep at 0x04,the same to Rom Code;
//Otherwise, fast reset to rom and use extension unit will fail
#define  ENT_ID_EXTENSION_UNIT_DBG  0x04	
#define  ENT_ID_OUTPUT_TRM             0X05
#define  ENT_ID_EXTENSION_UNIT_UTIL  0x06
#define  ENT_ID_RTK_EXTENDED_CTL_UNIT	 0x07
#define ENT_ID_EXTENSION_UNIT_XU1	0x08

//entity sourceID 
#define SRC_ID_PROCESSING_UNIT   ENT_ID_CAMERA_IT
#define SRC_ID_EXTENSION_UNIT_FACEAE	ENT_ID_PROCESSING_UNIT
#define SRC_ID_EXTENSION_UNIT_XU1	 ENT_ID_EXTENSION_UNIT_FACEAE
#ifdef _DELL_EXU_
#define SRC_ID_EXTENSION_UNIT_DBG   ENT_ID_EXTENSION_UNIT_XU1 //ENT_ID_CAMERA_IT
#else
#define SRC_ID_EXTENSION_UNIT_DBG   ENT_ID_EXTENSION_UNIT_FACEAE //ENT_ID_CAMERA_IT  
#endif
#define SRC_ID_RTK_EXTENDED_CTL_UNIT	ENT_ID_EXTENSION_UNIT_DBG
#if defined _RTK_EXTENDED_CTL_ || defined _LENOVO_IDEA_EYE_EXTENDEDUNIT_
#define SRC_ID_OUTPUT_TRM				ENT_ID_RTK_EXTENDED_CTL_UNIT 
#else
#define SRC_ID_OUTPUT_TRM				ENT_ID_EXTENSION_UNIT_DBG 
#endif
#endif

//VC error codes
#define  VC_ERR_NOERROR                  0x00
#define  VC_ERR_NOTRDY                     0x01
#define  VC_ERR_WRNGSTAT                0x02
#define  VC_ERR_PWR                            0x03
#define  VC_ERR_OUTOFRANGE           0x04
#define  VC_ERR_INVDUNIT                  0x05
#define  VC_ERR_INVDCTL                    0x06
#define  VC_ERR_INVDREQ                   0x07
//#define  VC_ERR_RESERVED                 0x08  -0XFE
#define  VC_ERR_UNKNOWN                 0xFF

//VS error codes
#define VS_ERR_NOERROR                        (0)
#define VS_ERR_CNTPROTECT                  (1)
#define VS_ERR_IBUFUNDERRUN             (2)
#define VS_ERR_DATDISCONT                  (3)
#define VS_ERR_OBUFUNDERRUN            (4)
#define VS_ERR_OBUFOVERRUN              (5)
#define VS_ERR_FMTCHANGE                   (6)
#define VS_ERR_STLIMGCAPERR              (7)
#define VS_ERR_UNKNOWN                         (8)
//define exposure time auto mode
#define EXPOSURE_TIME_AUTO_MOD_MANUAL     0X01
#define EXPOSURE_TIME_AUTO_MOD_AUTO          0X02
#define EXPOSURE_TIME_AUTO_MOD_SHUTPRO   0X04
#define EXPOSURE_TIME_AUTO_MOD_APERTPRO 0X08

// Focus Auto
#define CTL_MIN_CT_FOCUS_AUTO   (0)
#define CTL_MAX_CT_FOCUS_AUTO  (1)
#define CTL_RES_CT_FOCUS_AUTO   (1)
#define CTL_DEF_CT_FOCUS_AUTO   (1)

//VS event type
#define VS_STSINT_EVENT_BTNPRESS  0x00
#define VS_STSINT_EVENT_STRMERR    0x01      //0x01- 0xFF stream error.
//VS event value
#define VS_STSINT_VALUE_BTNRELEASE 0x00
#define VS_STSINT_VALUE_BTNPRESS      0x01

//power mode
#define PWR_MODE_FULL          0x00
#define PWR_MODE_DEV_DEP  0x01

/* FRAME INTERVAL CONSTANTS*/
#define VIDEO_FRM_INTVAL_30     0x051615
#define VIDEO_FRM_INTVAL_25     0x061a80
#define VIDEO_FRM_INTVAL_20     0x07a120
#define VIDEO_FRM_INTVAL_15     0x0a2c2b
#define VIDEO_FRM_INTVAL_10     0x0f4240
#define VIDEO_FRM_INTVAL_9       0x10F447
//#define VIDEO_FRM_INTVAL_7_5   0x145855
#define VIDEO_FRM_INTVAL_5	 0x1e8480
#define VIDEO_FRM_INTVAL_1	 0x989680


/*video frame diamension*/
#define VIDEO_WIDTH_2592    2592
#define VIDEO_WIDTH_2048    2048
#define VIDEO_WIDTH_1920    1920
#define VIDEO_WIDTH_1600    1600
#define VIDEO_WIDTH_1280    1280
#define VIDEO_WIDTH_1024    1024
#define VIDEO_WIDTH_960	960
#define VIDEO_WIDTH_848	848
#define VIDEO_WIDTH_800      800
#define VIDEO_WIDTH_640    	640
#define VIDEO_WIDTH_424	424
#define VIDEO_WIDTH_352    	352
#define VIDEO_WIDTH_320    	320
#define VIDEO_WIDTH_176    	176
#define VIDEO_WIDTH_160    	160


#define VIDEO_HEIGHT_1944   1944
#define VIDEO_HEIGHT_1536   1536
#define VIDEO_HEIGHT_1200   1200
#define VIDEO_HEIGHT_1080   1080
#define VIDEO_HEIGHT_1024   1024
#define VIDEO_HEIGHT_960     960
#define VIDEO_HEIGHT_900	900
#define VIDEO_HEIGHT_800     800
#define VIDEO_HEIGHT_768     768
#define VIDEO_HEIGHT_720     720
#define VIDEO_HEIGHT_600     600
#define VIDEO_HEIGHT_540	540
#define VIDEO_HEIGHT_480   	480
#define VIDEO_HEIGHT_400   	400
#define VIDEO_HEIGHT_360	360
#define VIDEO_HEIGHT_288   	288
#define VIDEO_HEIGHT_240   	240
#define VIDEO_HEIGHT_200   	200
#define VIDEO_HEIGHT_192   	192
#define VIDEO_HEIGHT_180 	180
#define VIDEO_HEIGHT_144   	144
#define VIDEO_HEIGHT_120   	120
#ifdef _IMX076_320x240_60FPS_
#define VIDEO_HEIGHT_90   	90
#endif

/*max video frame size*/
/*
#define VIDEOFRAMSIZE_160_120         38400
#define VIDEOFRAMSIZE_176_144         50688
#define VIDEOFRAMSIZE_320_240 	153600
#define VIDEOFRAMSIZE_320_200 	128000

#define VIDEOFRAMSIZE_352_288 	202752
#define VIDEOFRAMSIZE_640_480 	614400
#define VIDEOFRAMSIZE_640_400 	512000
#define VIDEOFRAMSIZE_800_600 	960000

#define VIDEOFRAMSIZE_960_720 	1382400
#define VIDEOFRAMSIZE_1024_768       1572864
#define VIDEOFRAMSIZE_1280_720       1843200
#define VIDEOFRAMSIZE_1280_800       2048000

#define VIDEOFRAMSIZE_1280_960       2457600
#define VIDEOFRAMSIZE_1280_1024     2621440

#define VIDEOFRAMSIZE_1600_1200 	3840000\
*/
/*max payload transfer size*/
//high speed
#define MAX_PAYLOADTRFERSIZE_HS_128    (128)
#define MAX_PAYLOADTRFERSIZE_HS_512    (512)
#define MAX_PAYLOADTRFERSIZE_HS_1024  (1024)
#define MAX_PAYLOADTRFERSIZE_HS_1536  (1536)
#define MAX_PAYLOADTRFERSIZE_HS_2048  (2048)
#define MAX_PAYLOADTRFERSIZE_HS_2688  (2688)
#define MAX_PAYLOADTRFERSIZE_HS_3072  (3072)
//full speed
#define MAX_PAYLOADTRFERSIZE_FS_128  (128)
#define MAX_PAYLOADTRFERSIZE_FS_256  (256)
#define MAX_PAYLOADTRFERSIZE_FS_384  (384)
#define MAX_PAYLOADTRFERSIZE_FS_512  (512)
#define MAX_PAYLOADTRFERSIZE_FS_768  (768)
#define MAX_PAYLOADTRFERSIZE_FS_896  (896)
#define MAX_PAYLOADTRFERSIZE_FS_1020  (1020)




#define CTL_SIZE_VS_PROBE_1V1                 0x22
#define CTL_SIZE_VS_PROBE_1V0                 0x1a
#define CTL_SIZE_VS_COMMIT_1V1              CTL_SIZE_VS_PROBE_1V1
#define CTL_SIZE_VS_COMMIT_1V0              CTL_SIZE_VS_PROBE_1V0


#define CTL_SIZE_VS_STILL_PROBE     11
#define CTL_SIZE_VS_STILL_COMMIT  CTL_SIZE_VS_STILL_PROBE

#define CTL_INFO_VS_PROBE                ( CONTROL_INFO_SP_GET|CONTROL_INFO_SP_SET)
#define CTL_INFO_VS_COMMIT             ( CONTROL_INFO_SP_GET|CONTROL_INFO_SP_SET)
#define CTL_INFO_VS_STILL_PROBE    ( CONTROL_INFO_SP_GET|CONTROL_INFO_SP_SET)
#define CTL_INFO_VS_STILL_COMMIT  ( CONTROL_INFO_SP_GET|CONTROL_INFO_SP_SET)
#define CTL_INFO_VS_STILL_IMAGE_TRIGGER  ( CONTROL_INFO_SP_GET|CONTROL_INFO_SP_SET)
#define CTL_INFO_VS_ERROR_CODE     (CONTROL_INFO_SP_GET)

#define CTL_TRIGGER_STS_NORMAL                           0
#define CTL_TRIGGER_STS_TRNS_STILL                     1
#define CTL_TRIGGER_STS_TRNS_STILL_VIABULK   2
#define CTL_TRIGGER_STS_ABORT_STILL                  3



#define F_SEL_640_480    (0)
#define F_SEL_160_120    (1)
#define F_SEL_176_144    (2)
#define F_SEL_320_180	(3)
#define F_SEL_320_200    (4)
#define F_SEL_320_240    (5)
#define F_SEL_352_288    (6)
#define F_SEL_424_240	(7)
#define F_SEL_640_360	(8)
#define F_SEL_640_400    (9)
#define F_SEL_800_600    (10)
#define F_SEL_848_480	(11)
#define F_SEL_960_540	 (12)	//output resolution not a multiple of 8 for LINK TEST
//#define F_SEL_960_720    (8)
#define F_SEL_1024_768   (13)
#define F_SEL_1280_720   (14)
#define F_SEL_1280_800   (15)
#define F_SEL_1280_960   (16)
#ifdef _NEW_FT2_MODEL_
#define F_SEL_1280_192  (17)
#define F_SEL_1600_240  (18)
#else
#define F_SEL_1280_1024  (17)
#define F_SEL_1600_1200  (18)
#endif
#define F_SEL_1920_1080  (19)
#define F_SEL_1600_900	 (20)
#define F_SEL_2048_1536  (21)
#define F_SEL_2592_1944  (22)
#ifdef _IMX076_320x240_60FPS_
#define F_SEL_160_90    (23)
#endif

#define F_SEL_NONE           0xff

#define FRM_EXIST(BitSel)  (0x00000001<<(BitSel))






// hemonel 2010-01-11: add fps 120, 60 ,24, delete fps 1
#define FPS_120    (0x0001<<0)
#define FPS_60   (0x0001<<1)
#define FPS_30    (0x0001<<2)
#define FPS_25    (0x0001<<3)
#define FPS_24    (0x0001<<4)
#define FPS_23    (0x0001<<5)
#define FPS_20    (0x0001<<6)
#define FPS_15    (0x0001<<7)
#define FPS_12    (0x0001<<8)
#define FPS_11    (0x0001<<9)
#define FPS_10    (0x0001<<10)
#define FPS_9      (0x0001<<11)
#define FPS_8      (0x0001<<12)
#define FPS_7      (0x0001<<13)
#define FPS_5      (0x0001<<14)
#define FPS_3      (0x0001<<15)
//#define FPS_1      (0x0001<<15)

#define SENSOR_FPS_120   120
#define SENSOR_FPS_60   60
#define SENSOR_FPS_40   40
#define SENSOR_FPS_30   30
#define SENSOR_FPS_25   25
#define SENSOR_FPS_24   24
#define SENSOR_FPS_23   23
#define SENSOR_FPS_20   20
#define SENSOR_FPS_15   15
#define SENSOR_FPS_12   12
#define SENSOR_FPS_11   11
#define SENSOR_FPS_10   10
#define SENSOR_FPS_9      9
#define SENSOR_FPS_8      8
#define SENSOR_FPS_7      7
#define SENSOR_FPS_5      5
#define SENSOR_FPS_3      3
#define SENSOR_FPS_1      1





/*
*--------------------------------------------------------------------------
* UVC constants
*----------------------------------------------------------------------------
*/



//UVC constants
#define DEV_CLS_MULTI              0XEF
#define DEV_SUBCLS_COMMON 0X02
#define DEV_PROTOCOL_IAD      0X01

//
#define  USB_IAD_DESCRIPTOR_TYPE 0X0b

/*
A.1. Video Interface Class Code
*/
#define CC_VIDEO  0x0E
/*
A.2. Video Interface Subclass Codes
*/
#define SC_UNDEFINED  0x00
#define SC_VIDEOCONTROL  0x01
#define SC_VIDEOSTREAMING  0x02
#define SC_VIDEO_INTERFACE_COLLECTION  0x03

/*
A.3. Video Interface Protocol Codes
*/
#define PC_PROTOCOL_UNDEFINED  0x00

/*
A.4. Video Class-Specific Descriptor Types
*/

#define CS_UNDEFINED 0x20
#define CS_DEVICE 0x21
#define CS_CONFIGURATION 0x22
#define CS_STRING 0x23
#define CS_INTERFACE 0x24
#define CS_ENDPOINT 0x25

/*
A.5. Video Class-Specific VC Interface Descriptor Subtypes
*/
#define VC_DESCRIPTOR_UNDEFINED 0x00
#define VC_HEADER 0x01
#define VC_INPUT_TERMINAL 0x02
#define VC_OUTPUT_TERMINAL 0x03
#define VC_SELECTOR_UNIT 0x04
#define VC_PROCESSING_UNIT 0x05
#define VC_EXTENSION_UNIT 0x06


/*
A.6. Video Class-Specific VS Interface Descriptor Subtypes
*/
#define VS_UNDEFINED  0x00
#define VS_INPUT_HEADER 0x01
#define VS_OUTPUT_HEADER 0x02
#define VS_STILL_IMAGE_FRAME 0x03
#define VS_FORMAT_UNCOMPRESSED 0x04
#define VS_FRAME_UNCOMPRESSED 0x05
#define VS_FORMAT_MJPEG 0x06
#define VS_FRAME_MJPEG 0x07


/* 0x08
Reserved
0x09
Reserved
*/
#define VS_FORMAT_MPEG2TS 0x0A
/*0x0B
Reserved */
#define VS_FORMAT_DV 0x0C
#define VS_COLORFORMAT 0x0D

#define VS_FORMAT_VENDOR  0x0E
#define VS_FRAME_VENDOR     0x0F

/*0x0E
Reserved
0x0F
Reserved */
#define VS_FORMAT_FRAME_BASED 0x10
#define VS_FRAME_FRAME_BASED 0x11
#define VS_FORMAT_STREAM_BASED 0x12

/*
A.7. Video Class-Specific Endpoint Descriptor Subtypes
*/
#define EP_UNDEFINED 0x00
#define EP_GENERAL 0x01
#define EP_ENDPOINT 0x02
#define EP_INTERRUPT 0x03

/*
A.8. Video Class-Specific Request Codes
*/
#define RC_UNDEFINED 0x00
#define SET_CUR 0x01
#define GET_CUR 0x81
#define GET_MIN 0x82
#define GET_MAX 0x83
#define GET_RES 0x84
#define GET_LEN 0x85
#define GET_INFO 0x86
#define GET_DEF 0x87

/*
A.9. Control Selector Codes
*/
/*
A.9.1. VideoControl Interface Control Selectors
*/
#define VC_CONTROL_UNDEFINED  0x00
#define VC_VIDEO_POWER_MODE_CONTROL 0x01
#define VC_REQUEST_ERROR_CODE_CONTROL 0x02
/*0x03
Reserved
*/
/*
A.9.2. Terminal Control Selectors
*/
#define TE_CONTROL_UNDEFINED 0x00

/*
A.9.3. Selector Unit Control Selectors
*/
#define SU_CONTROL_UNDEFINED 0x00
#define SU_INPUT_SELECT_CONTROL 0x01


/*
 A.9.4. Camera Terminal Control Selectors
*/
#define CT_CONTROL_UNDEFINED 0x00
#define CT_SCANNING_MODE_CONTROL 0x01
#define CT_AE_MODE_CONTROL 0x02
#define CT_AE_PRIORITY_CONTROL 0x03
#define CT_EXPOSURE_TIME_ABSOLUTE_CONTROL 0x04
#define CT_EXPOSURE_TIME_RELATIVE_CONTROL 0x05
#define CT_FOCUS_ABSOLUTE_CONTROL 0x06
#define CT_FOCUS_RELATIVE_CONTROL 0x07
#define CT_FOCUS_AUTO_CONTROL 0x08
#define CT_IRIS_ABSOLUTE_CONTROL 0x09
#define CT_IRIS_RELATIVE_CONTROL 0x0A
#define CT_ZOOM_ABSOLUTE_CONTROL 0x0B
#define CT_ZOOM_RELATIVE_CONTROL 0x0C
#define CT_PANTILT_ABSOLUTE_CONTROL 0x0D
#define CT_PANTILT_RELATIVE_CONTROL 0x0E
#define CT_ROLL_ABSOLUTE_CONTROL 0x0F
#define CT_ROLL_RELATIVE_CONTROL 0x10
#define CT_PRIVACY_CONTROL 0x11

/*
A.9.5. Processing Unit Control Selectors
*/
#define PU_CONTROL_UNDEFINED 0x00
#define PU_BACKLIGHT_COMPENSATION_CONTROL 0x01
#define PU_BRIGHTNESS_CONTROL 0x02
#define PU_CONTRAST_CONTROL 0x03
#define PU_GAIN_CONTROL 0x04
#define PU_POWER_LINE_FREQUENCY_CONTROL 0x05
#define PU_HUE_CONTROL 0x06
#define PU_SATURATION_CONTROL 0x07
#define PU_SHARPNESS_CONTROL 0x08
#define PU_GAMMA_CONTROL 0x09
#define PU_WHITE_BALANCE_TEMPERATURE_CONTROL 0x0A
#define PU_WHITE_BALANCE_TEMPERATURE_AUTO_CONTROL 0x0B
#define PU_WHITE_BALANCE_COMPONENT_CONTROL 0x0C
#define PU_WHITE_BALANCE_COMPONENT_AUTO_CONTROL 0x0D
#define PU_DIGITAL_MULTIPLIER_CONTROL 0x0E
#define PU_DIGITAL_MULTIPLIER_LIMIT_CONTROL 0x0F
#define PU_HUE_AUTO_CONTROL 0x10
#define PU_ANALOG_VIDEO_STANDARD_CONTROL 0x11
#define PU_ANALOG_LOCK_STATUS_CONTROL 0x12

/*
A.9.6. Extension Unit Control Selectors
*/
#define XU_CONTROL_UNDEFINED 0x00

/*
A.9.7. VideoStreaming Interface Control Selectors
*/
#define VS_CONTROL_UNDEFINED 0x00
#define VS_PROBE_CONTROL 0x01
#define VS_COMMIT_CONTROL 0x02
#define VS_STILL_PROBE_CONTROL 0x03
#define VS_STILL_COMMIT_CONTROL 0x04
#define VS_STILL_IMAGE_TRIGGER_CONTROL 0x05
#define VS_STREAM_ERROR_CODE_CONTROL 0x06
#define VS_GENERATE_KEY_FRAME_CONTROL 0x07
#define VS_UPDATE_FRAME_SEGMENT_CONTROL 0x08
#define VS_SYNCH_DELAY_CONTROL 0x09

/*
Terminal types
*/
#define TT_VENDOR_SPECIFIC  0x0100
#define TT_STREAMING  0x0101
/*
Input Terminal Type
*/
#define ITT_VENDOR_SPECIFIC  0x0200
#define ITT_CAMERA  0x0201
#define ITT_MEDIA_TRANSPORT_INPUT  0x0202
/*
Output Terminal Type
*/
#define OTT_VENDOR_SPECIFIC  0x0300
#define OTT_DISPLAY  0x0301
#define OTT_MEDIA_TRANSPORT_OUTPUT  0x0302
/*
External Terminal Type
*/
#define EXTERNAL_VENDOR_SPECIFIC  0x0400
#define COMPOSITE_CONNECTOR 0x0401
#define SVIDEO_CONNECTOR 0x0402
#define COMPONENT_CONNECTOR 0x0403



#define I_VIDEOFUNCTION     0x05
#define I_CAMERA_TERM       0x00
#define I_PROCESSING_UNIT   0x00
#define I_OUTPUT_TRM        0x00
#define I_VIDEO_STREAMING   0x00

//
#define DESC_SIZE_CLS_VC_IF 0x0D
#define DESC_SIZE_IAD              0x08
#define DESC_SIZE_CAMERA_TRM      0x12

#ifdef _UVC_1V1_
#define DESC_SIZE_PROC_UNIT_1V1   0X0C
#else
#define DESC_SIZE_PROC_UNIT_1V0   0X0B
#endif

#define DESC_SIZE_OUT_TRM      0X09
#define DESC_SIZE_CLS_VCI_EP  0X05

#define DESC_SIZE_EXT_DBG_UNIT 0x1B
#define DESC_SIZE_DELLXU1_UNIT		0x1D
#define DESC_SIZE_FACIAL_AE_XU   0x1A
#define DESC_SIZE_RTK_EXT_UNIT		0x1D 

//
#define OCULAR_FOCAL_LEN           0x0000
#define OBJECT_FOCAL_LEN_MAX  0X0000
#define OBJECT_FOCAL_LEN_MIN  0X0000


#define  D_TYPE_NORMAL			0x00
#define  D_TYPE_FMT				0x10
#ifdef _UAC_EXIST_
//#define  D_TYPE_UAC_ALT		0x20
#define  D_TYPE_UAC			    	0x20
#endif
// hemonel 2010-08-26: max support 3 format
#define  D_TYPE_FRM1			0x30
#define  D_TYPE_FRM2			0x40
#define  D_TYPE_FRM3          		0x50
//#ifdef _UAC_EXIST_
//#define  D_TYPE_UAC_FMTD	    	0x60
//#endif
#define  D_TYPE_STLIMG            	0x70
#define  D_TYPE_CLSSPCVCIF   	0x80
#define  D_TYPE_SPECIAL         	0x90
#define  D_TYPE_CAMTRM           	0xa0
#define  D_TYPE_PROCUNIT        	0xb0
#define  D_TYPE_VSINHDR           	0xc0
#define  D_TYPE_FRM4          		0xd0
#define  D_TYPE_EXTU_RTK		0xe0
#define  D_TYPE_EXU			    	0xf0

/*
#define TBL_TYPE_YUY2    1
#define TBL_TYPE_MJPG    2

#define TBL_TYPE_YUY2STL  3
#define TBL_TYPE_MJPGSTL  4

#define TBL_TYPE_YUV420        5
#define TBL_TYPE_YUV420STL  6

#define TBL_TYPE_RAWFMT    7
#define TBL_TYPE_RAWFMTSTL     8
*/

//camera terminal control select bit map

//     D00 = 0   no -  Scanning Mode
//     D01 = 1  yes -  Auto-Exposure Mode
//     D02 = 0   no -  Auto-Exposure Priority
//     D03 = 1  yes -  Exposure Time (Absolute)
//     D04 = 0   no -  Exposure Time (Relative)
//     D05 = 0   no -  Focus (Absolute)
//     D06 = 0   no -  Focus (Relative)
//     D07 = 0   no -  Iris (Absolute)
//     D08 = 0   no -  Iris (Relative)
//     D09 = 0   no -  Zoom (Absolute)
//     D10 = 0   no -  Zoom (Relative)
//     D11 = 0   no -  PanTilt (Absolute)
//     D12 = 0   no -  PanTilt (Relative)
//     D13 = 0   no -  Roll (Absolute)
//     D14 = 0   no -  Roll (Relative)
//     D15 = 0   no -  Reserved
#define CT_CTL_SEL_SCAN_MODE              	(0x0001)
#define CT_CTL_SEL_AUTO_EXP_MODE       	(0x0002)
#define CT_CTL_SEL_AUTO_EXP_PRY       	(0x0004)
#define CT_CTL_SEL_EXPOSURE_TIME_ABS	(0x0008)
#define CT_CTL_SEL_EXPOSURE_TIME_REL	(0x0010)
#define CT_CTL_SEL_FOCUS_ABS			(0x0020)
#define CT_CTL_SEL_FOCUS_REL			(0x0040)
#define CT_CTL_SEL_IRIS_ABS				(0x0080)
#define CT_CTL_SEL_IRIS_REL				(0x0100)
#define CT_CTL_SEL_ZOOM_ABS			(0x0200)
#define CT_CTL_SEL_ZOOM_REL			(0x0400)
#define CT_CTL_SEL_PANTILT_ABS			(0x0800)
#define CT_CTL_SEL_PANTILT_REL			(0x1000)
#define CT_CTL_SEL_ROLL_ABS			(0x2000)
#define CT_CTL_SEL_ROLL_REL			(0x4000)
//     D16 = 0   no -  Reserved
//     D17 = 0   no -  Focus,Auto
//     D18 = 0   no -  Privacy
// Camera Termal control select bit D16~D31 definition
#define CT_CTL_SEL_FOCUS_AUTO			(0x0002)

#define CT_CTL_SEL_FOCUS_AUTO_FOR_PPWB	0x8000	// hemonel 2011-05-05: camera control select bit map D15 is reserved, so use D15 as FocusAuto PPWB change bit

//PROCESSING UNIT CONTROL SELECT BIT MAP
//bmControls : 0x7F 0x17
//     D00 = 1  yes -  Brightness
//     D01 = 1  yes -  Contrast
//     D02 = 1  yes -  Hue
//     D03 = 1  yes -  Saturation
//     D04 = 1  yes -  Sharpness
//     D05 = 1  yes -  Gamma
//     D06 = 1  yes -  White Balance Temperature
//     D07 = 0   no -  White Balance Component
//     D08 = 1  yes -  Backlight Compensation
//     D09 = 0  yes -  Gain
//     D10 = 1  yes -  Power Line Frequency
//     D11 = 0   no -  Hue, Auto
//     D12 = 1  yes -  White Balance Temperature, Auto
//     D13 = 0   no -  White Balance Component, Auto
//     D14 = 0   no -  Digital Multiplier
//     D15 = 0   no -  Digital Multiplier Limit

#define PU_CTL_SEL_BRIGHTNESS                        (0x0001)
#define PU_CTL_SEL_CONTRAST                            (0x0002 )
#define PU_CTL_SEL_HUE                                       (0x0004 )
#define PU_CTL_SEL_SATURATION                        (0x0008 )
#define PU_CTL_SEL_SHARPNESS                         (0x0010 )
#define PU_CTL_SEL_GAMMA                                (0x0020 )
#define PU_CTL_SEL_WHITE_BALANCE_TEMPERATURE        (0x0040 )
#define PU_CTL_SEL_WHITE_BALANCE_COMPONENT           (0x0080 )
#define PU_CTL_SEL_BACKLIGHT_COMPENSATION               (0x0100 )
#define PU_CTL_SEL_GAIN                                                         (0x0200 )
#define PU_CTL_SEL_POWER_LINE_FREQUENCY                   (0x0400)
#define PU_CTL_SEL_HUE_AUTO                                               (0x0800)
#define PU_CTL_SEL_WHITE_BALANCE_TEMPERATURE_AUTO   (0x1000)
#define PU_CTL_SEL_WHITE_BALANCE_COMPONENT_AUTO      (0x2000)
#define PU_CTL_SEL_DIGITAL_MULTIPLIER                                    (0x4000)
#define PU_CTL_SEL_DIGITAL_MULTIPLIER_LIMIT                        (0x8000)

#define RTK_EXT_CTL_SEL_ISP_SPECIAL_EFFECT	0x00000001
#define RTK_EXT_CTL_SEL_EVCOM					0x00000002
#define RTK_EXT_CTL_SEL_CTE					0x00000004
#define RTK_EXT_CTL_SEL_ROI					0x00080000
#define RTK_EXT_CTL_SEL_ROISTS				0x00100000
#define RTK_EXT_CTL_SEL_PREVIEW_LED_OFF		0x00200000
#define RTK_EXT_CTL_SEL_ISO					0x00000100

#define RTK_EXT_CTL_SEL_IDEAEYE_SENSITIVITY	0x01000000
#define RTK_EXT_CTL_SEL_IDEAEYE_STATUS		0x02000000
#define RTK_EXT_CTL_SEL_IDEAEYE_MODE			0x04000000

#define VDREQ_STS_NO_ERROR   0x00
#define VDREQ_STS_NO_DOUT    0x01
#define VDREQ_STS_HW_ERROR   0x02

#endif
