#ifndef _GLOBAL_VARS_H_
#define _GLOBAL_VARS_H_


#include "camutil.h"
#include "camctl.h"
#include "cami2c.h"
#include "camsensor.h"
#include "CamSensorUtil.h"
#include "ISP_TuningTable.h"

extern Ctl_t Ctl_ExposureTimeAbsolut;
extern Ctl_t Ctl_ExposureTimeAuto;
extern Ctl_t Ctl_LowLightComp;
extern Ctl_t Ctl_Brightness;
extern Ctl_t Ctl_Contrast;
extern Ctl_t Ctl_Hue;
extern Ctl_t Ctl_Saturation;
extern Ctl_t Ctl_Sharpness;
extern Ctl_t Ctl_Gamma;
extern Ctl_t Ctl_WhiteBalanceTemp;
extern Ctl_t Ctl_BackLightComp;
extern Ctl_t Ctl_Gain;
extern Ctl_t Ctl_PwrLineFreq;
extern Ctl_t Ctl_WhiteBalanceTempAuto;
extern Ctl_t Ctl_PanTilt;
extern Ctl_t Ctl_Zoom;
extern Ctl_t Ctl_Roll;
#ifdef _RTK_EXTENDED_CTL_
extern Ctl_t Ctl_RtkExtISPSpecialEffect;
extern Ctl_t Ctl_RtkExtEVCompensation;
extern Ctl_t Ctl_RtkExtROI;
extern Ctl_t Ctl_RtkExtPreviewLEDOff;
extern Ctl_t Ctl_RtkExtISO;
#endif
#ifdef _LENOVO_IDEA_EYE_EXTENDEDUNIT_
extern Ctl_t Ctl_IdeaEye_Sens;
extern Ctl_t Ctl_IdeaEye_Stat;
extern Ctl_t Ctl_IdeaEye_Mode;
#endif

extern Ctl_t Ctl_TrapeziumCorrection;
#ifdef _S3_RESUMKEK_
extern U16 data g_wPollTimeOutCnt;
#else
extern U8 data g_byPollTimeOutCnt ;
#endif
extern U8  g_bIsHighSpeed;
extern U8 data g_byDescIdx;
extern U8 data g_byDescOffset;
extern U8	 g_byCpuClkIdx;
extern U8	g_byReadMode;

extern U8 g_bySnrType;
extern U16 g_wSensorCurFormat;
extern U8 g_bySensorIsOpen ;
extern U16 g_wSensor;
extern U8 g_bySensorSize ;
extern U16 g_wSensorSPFormat ;
extern U16 g_wSensorCurFormatWidth ;
extern U16 g_wSensorCurFormatHeight ;

extern U16 g_wVdStrManuFactorAddr  ;
extern U16 g_wVdStrProductAddr ;
extern U16 g_wVdStrSerialNumAddr ;
extern U16 g_wVdIADStrAddr ;
#ifdef _UBIST_TEST_
extern U16 g_wVdUBISTStrAddr;
#endif

#if (defined (_SLB_TEST_))
extern U8    g_bySLB_FSTstCnt;
extern U32  g_dwSLB_HSTstCnt;
extern U16  g_wSLB_HSFailTh;
extern U8    g_bySLB_FixPattern ;
extern U8    g_bySLB_Seed;
extern U8    g_bySLB_FSFailTh;
extern U16  g_wSLB_HSDlyTime;
#endif

extern U16     	g_wTimerCounterForUSBIF ;
extern U8   g_byIsHighSpeedDesc;
extern U8 g_bybTrigger;
extern U8    g_byCommitFPS;
extern U16 g_wCamTrmCtlSel;
extern U16 g_wProcUnitCtlSel;
#if defined _RTK_EXTENDED_CTL_ || defined _LENOVO_IDEA_EYE_EXTENDEDUNIT_
extern U32 g_dwRtkExtUnitCtlSel;
#endif
extern U8 g_bySnrEffect;

extern CtlItemU32_t  ExposureTimeAbsolutItem;
extern CtlItemU8_t  ExposureTimeAutoItem;
extern CtlItemU8_t  LowLightCompItem;
extern CtlItemS16_t  BrightnessItem;
extern CtlItemU16_t  ContrastItem;
extern CtlItemS16_t  HueItem;
extern CtlItemU16_t  SaturationItem;
extern CtlItemU16_t  SharpnessItem;
extern CtlItemU16_t GammaItem;
extern CtlItemU16_t  WhiteBalanceTempItem;
extern CtlItemU16_t  BackLightCompItem;
extern CtlItemU16_t GainItem;
extern CtlItemU8_t  PwrLineFreqItem;
extern CtlItemU8_t  WhiteBalanceTempAutoItem;
extern CtlItemS16_t  PanItem;
extern CtlItemS16_t  TiltItem;
extern CtlItemU16_t  ZoomItem;
extern CtlItemS16_t  RollItem;
extern CtlItemS8_t TCorrectionItem;
#ifdef _RTK_EXTENDED_CTL_
extern CtlItemU16_t RtkExtISPSpecialEffectItem;
extern CtlItemS16_t RtkExtEVCompensationItem;
extern RtkExtROICtlItem_t RtkExtROIItem;
extern CtlItemU8_t RtkExtPreviewLEDOffItem;
extern CtlItemU16_t RtkExtISOItem; 
#endif
#ifdef _LENOVO_IDEA_EYE_EXTENDEDUNIT_
extern CtlItemU16_t RtkExtIdeaEyeSensItem;
extern CtlItemU8_t RtkExtIdeaEyeStatItem;
extern CtlItemU8_t	RtkExtIdeaEyeModeItem;
#endif
#ifdef _AF_ENABLE_
extern CtlItemU16_t  FocusAbsolutItem		;
extern CtlItemU8_t  FocusAutoItem		;
#endif
#ifdef _AF_ENABLE_
extern Ctl_t Ctl_FocusAbsolut		;
extern Ctl_t Ctl_FocusAuto		;
#endif
extern U8 g_byVendorIDL;
extern U8 g_byVendorIDH;
extern U8 g_byProductIDL;
extern U8 g_byProductIDH;

extern U8 g_byVCLastError;
extern U8 g_byVSLastError;

extern U8 g_byCamTrmCtlSel_Ext;
extern U8   g_byDataInOutCtlSize;
extern U8 g_byPHYHSRxTxPwrDownMode ;

extern OV_CTT_t g_asOvCTT[CTT_INDEX_MAX];
extern U8 g_byOVAEW_BLC;
extern U8 g_byOVAEB_BLC;
extern U8 g_byOVAEW_Normal;
extern U8 g_byOVAEB_Normal;

extern U8 g_byBackendIspSwitchPoint;
extern U8 ga_byNR_Coefient_NormalLight_3x5[];
extern U8 ga_byNR_Coefient_LowLight_3x5[];
extern U8 ga_byNR_Coefient_NormalLight_5x5[];
extern U8 ga_byNR_Coefient_LowLight_5x5[];

extern U8    g_bySFManuID;
extern U16  g_wSFDeviceID;

extern U16 g_wCurFrameWidth;
extern U16 g_wCurFrameHeight;
#if(defined _ENABLE_MJPEG_ || defined _ENABLE_M420_FMT_)
extern U16 g_wCurMJPEGOutputWidth;
extern U16 g_wCurMJPEGOutputHeight;
extern U16 g_wSensorMJPGByPass;
#endif
extern U8 g_byCurFps;
extern U8 g_byCurFormat;
extern SnrRegAccessProp_t g_SnrRegAccessProp;


#ifdef _EN_PHY_DBG_MODE_
extern U8 g_byPhyAddrDbg ;
#endif //_EN_PHY_DBG_MODE_

extern U8    g_byDevVerID_L  ;
extern U8    g_byDevVerID_H  ;


extern U8 g_bySnrPowerOnSeq;
extern U8 g_bySnrPowerOffSeq;



extern U8 g_byPreviewDropFrameNumber;
extern U8 g_byPreviewDelayTimeMax;
extern U8 g_byStillimgDropFrameNumber;
extern U8 g_byStillimgDelayTimeMax;

extern U8 g_byIspCtl;
extern U8 g_byIsBlack;
extern U16 g_wBarInfoOffset;

extern VSProbCommCtl_t  g_VsProbe;
extern VSProbCommCtl_t  g_VsCommit;
extern VSStlProbCommCtl_t  g_VsStlProbe;
extern VSStlProbCommCtl_t  g_VsStlCommit;

extern VideoFormat_T g_aVideoFormat[MAX_FORMAT_TYPE];
extern VideoFormat_T g_VideoFormatFSYUY2;
extern U8  g_byaFPSTable[16] ;
extern  U32 g_dwPclk;
extern U16 g_wSensorHsyncWidth;
extern FWVersion_t code g_cVdFwVerSection0;


extern  U16 g_wLEDToggleInterval;
extern U16 g_wExecuteNFValue;
extern U16 g_wBlackPatternGain_TH;
extern U16 g_wBlackPatternExposuretime_TH;
extern U8 g_byBlackPatternYavg_TH;
#ifdef _UBIST_TEST_
extern U8 g_byUBISTEnble;
extern U8 g_byUbistTestProgress;
extern U8 g_byUbistTestReturnCode;
extern U8 g_byUbistTestID;
extern U16 g_wUbistTestMemory;
extern U8 g_byUbistTestAbort;
extern U8 g_byUbistTestEnable;
#endif // _UBIST_TEST_
#ifdef _STILL_IMG_BACKUP_SETTING_
extern U16 g_wPreviewBackupExposure;
extern U16 g_wPreviewBackupGain;
#endif
extern U8 g_byStillFPS;
extern U32 g_dwPanFalseValue_ForXPbug;

extern U8 g_bySensor_YuvMode;
extern U8 g_byStartVideo;
#ifdef _UAC_EXIST_
extern U8 g_byAudioStrmIFSetting;
extern U16 g_wAudioVolumeCurr;
extern U8 g_byAudioAttrCurr; ////bit0: mute, bit1: boost, bit2: AGC
extern U8  g_byConfigMicArray;
extern U16 g_wConfigMicArrayCfgAddr;
#endif
extern U8 g_bySnrImgDir;
extern U8 g_byWhiteBalanceAutoLast_BackForUVCtest;	//2010-03-25 hemonel: when disable gain function, manual white balance USB_IF UVCTest fail.

#ifdef _UVC_PPWB_
extern VSPropertyCommCtl_t  g_VsPropCommit;
#endif
#ifdef _UVC_COMMITWB_
extern VSBulkCommit_t g_VsBulkCommit;
#endif
extern U16 g_wProcessUnitChange_Flag;
extern U16 g_wCameraTerminalChange_Flag;
extern U8 g_byCommitChange_Flag;
#ifdef _RTK_EXTENDED_CTL_
extern U32 g_dwRtkExtCtlUnitChange_Flag;
#endif

extern U8 g_byPG_USBSYS_IRQEN;
extern U8 g_byPG_EPA_CFG;
extern U8 g_byPG_EPA_CTL;
extern U8 g_byPG_EPA_IRQEN;
extern U16 g_wPG_EPA_TRANSFER_SIZE;
extern U16 g_wPG_LWM;
extern U16 g_wPG_HWM;


extern U8 g_bySSCEnable;

extern U8 g_byThermalEnumerDelay;
extern float g_fThermalCofe;
extern S8 g_byThermalCalibrationOffset;
#ifdef _DELL_EXU_
extern U8 g_byMaxAutoGain;
extern U16 g_wMaxAutoExpTime;
extern U8 g_byLEDStatus;
extern U8 g_byLEDBlinkCnt;
extern U8 g_byStreamActive;
#endif

extern VideoDimension_t g_cwaResolutionTable[MAX_RES_TABLE_SIZE];
#ifndef _CACHE_MODE_
extern U8 g_bySPIClockSinkSel ;
extern U16 g_wSFCapacity;
extern U8   g_bySFProgModeSP;
extern U8   g_bySFChipEROpCode;
#endif
extern U8 g_bySLBTestMode;
extern U8 g_byVendorReqStatus;
extern U16 g_wVCCSdescSize;
extern U16 g_wVSCSdescSize;
extern U16 g_wTotalCfgDescSize;
extern U8 g_byConfigurationValue;
extern U8 g_byVideoStrmIFSetting;

#ifdef _UAC_EXIST_
extern U8 g_wEPB_ADF_delay;
extern U8 g_byADF_Chn_Switch;
extern U8 g_byALC_Cfg;
extern U8 g_byALC_FT_Boost;
extern U8 g_byALC_NRGate_Cfg0;
#endif

extern U16 g_wExtUIspCtl;
extern U16 g_wZoomXCorrectValue;
#ifdef _MIPI_EXIST_
extern U8 g_byMipiDphyCtrl;
#endif
#ifdef _RS0509_PREVIEW_ISSUE_
extern U16 g_wSOFCount;
#endif

#ifdef _HID_BTN_
extern U8 g_byHIDBtnLast;
#endif
extern IQ_LSC_Dynamic_t g_sLscDyn;
#ifdef _BULK_ENDPOINT_
extern U8 g_byLPMSetbyAP;
extern U16 g_wHWMbyAp;
extern U16 g_wHWM;
extern U16 g_wLWM;
extern U16 g_wTransferSize;
#endif

#ifdef _SENSOR_ESD_ISSUE_
extern U8 g_byESDExist;
extern U8 g_byStreamOn;
extern U16 g_wESDTimerCounter;
#endif
#ifdef _NF_RESTORE_
extern U8 g_bySuspendFW_N;
extern U8 g_bySuspendFW_F;
extern U8 g_byBypassFirstSOF;
#endif

#ifdef _LENOVO_JAPAN_PROPERTY_PAGE_
extern U8 g_byAWBRGain_Lenovo;
extern U8 g_byAWBGGain_Lenovo;
extern U8 g_byAWBBGain_Lenovo;
extern U8 g_byVendorDriver_Lenovo;
#endif

#ifdef _JPEG_RATE_ADJ_
extern U8 g_byQtableScaleGTHD;
extern U8 g_byQtableScaleGTVGA;
extern U8 g_byQtableScaleLEQVGA;
#endif

extern I2CSuperAccess_t g_tI2CSuperAccess;

extern U8 g_byPre_ind;							
extern U8 g_byind_count;							
extern U8 g_byind; 								
extern U16 g_wRGain_CT;							
extern U16 g_wBGain_CT;							
extern U8 g_byWinMeanR_BeforeLSC[5][5];	
extern U8 g_byWinMeanG_BeforeLSC[5][5];		
extern U8 g_byWinMeanB_BeforeLSC[5][5];			

#ifdef _IQ_TABLE_CALIBRATION_
extern  U8 g_byIQPatchGrpOnePatchOne;
#endif

extern U8 g_byRemoteWakeupSupport;
extern U8 g_byHostEnableRemoteWakeup;
extern U8 g_byRemoteWakeupNotify;

extern U16 g_wDigitalGain;

extern U8 g_byVDCMDPVLEDOff;

extern U8 g_byMTDDetectBackend;
extern U8 g_byMTDStartGetFrame;
extern U8 g_byUSBStreaming;
extern U8 g_byMTDWinDetected;
extern U8 g_byMTDSensCent;
extern U8 g_byMTDWinNCent;
extern U16 data g_wMTDTimeOutCnt;

extern U8 g_bySSCEnable;

#endif //_GLOBAL_VARS_H_
