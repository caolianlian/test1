#ifndef _CAMSENSOR_H_
#define _CAMSENSOR_H_




#define SNR_OP_MODE_NORMAL 0
#define SNR_OP_MODE_TEST_OV9710   1
#define SNR_OP_MODE_TEST_MI2020 2

#define QSXGA_FRM		0x0400		//2592*1944
#define QXGA_FRM	       0x0200		//2048*1536
#define HD1080P_FRM	0x0100		//1920*1080
#define UXGA_FRM		0x0080		// 1600*1200
#define SXGA_FRM		0x0040		// 1280*1024
#define XVGA_FRM		0x0020		// 1280*960
#define HD800P_FRM		0x0010		// 1280*800
#define HD720P_FRM		0x0008		//  1280*720 
#define XGA_FRM			0x0004		// 1024*768
#define SVGA_FRM		0x0002		// 800*600
#define VGA_FRM			0x0001		// 640*480

#define SENSOR_SIZE_MASK		0x0F
#define SENSOR_SIZE_UNKNOWN	0x00
#define SENSOR_SIZE_VGA		0x01	// 640*480
#define SENSOR_SIZE_HD720P		0x02	// 1280*720
#define SENSOR_SIZE_HD800P		0x03	// 1280*800
#define SENSOR_SIZE_XVGA		0x04	// 1280*960
#define SENSOR_SIZE_SXGA		0x05	// 1280*1024
#define SENSOR_SIZE_UXGA		0x06	// 1600*1200
#define SENSOR_SIZE_FHD		0x07	// 1920*1080
#define SENSOR_SIZE_QXGA		0x08	// 2048*1536
#define SENSOR_SIZE_QSXGA		0x09	// 2592*1944

#define SENSOR_DEF_ET_STEPS	0x03

#define EnableSensorHCLK() (XBYTE[CCS_CLK_SWITCH] |= CCS_CLK_ENABLE)
#define DisableSensorHCLK() (XBYTE[CCS_CLK_SWITCH] &= ~CCS_CLK_ENABLE)


// hemonel 2010-04-07: RTS5820X delete this pin
//#define SENSOR_POWER_ON()	{GPIO_H_DRIVE_HIGH(2)}
//#define SENSOR_POWER_OFF() {GPIO_H_DRIVE_LOW(2)}

//special effect
#define SNR_EFFECT_NORMAL             0x00
#define SNR_EFFECT_MONOCHROME 0x01
#define SNR_EFFECT_NEGATIVE          0x02
#define SNR_EFFECT_SEPIA                 0x04
#define SNR_EFFECT_AQUA                  0x08
#define SNR_EFFECT_GREEN                0x10
#define SNR_EFFECT_GREENISH		    0x20
#define SNR_EFFECT_REDDISH			0x40
#define SNR_EFFECT_BLUISH			0x80
#define SNR_EFFECT_GRAY		0x81
#define SNR_EFFECT_EDGEDRAW	0x82
#define SNR_EFFECT_RELIEVO		0x83

// image direction set
#define SNR_IMG_DIR_NORMAL              0x0
#define SNR_IMG_DIR_FLIP                 0x2
#define SNR_IMG_DIR_MIRROR               0x1
#define SNR_IMG_DIR_FLIP_MIRROR     0x3

typedef struct
{
	U8 byAddress;
	U8 byData;
} t_RegSettingBB;

typedef struct
{
	U16 wAddress;
	U8 byData;
} t_RegSettingWB;

typedef struct
{
	U16 wAddress;
	U16 wData;
} t_RegSettingWW;
typedef struct
{
	U16 wAddress;
	U16 wData;
	U8 byDataWidth;
} t_VarSettingWW;
/*
typedef struct
{
	U8 op;
	U16 wAddress;
	U16 wData;
}t_SocRegSettingWW;
*/

/***********MI2020*********/
#define  MI2020_ADDR_MODE_REG   0x338c
#define  MI2020_DATA_INOUT_REG  0x3390
/***********MI1330*********/
#define  MI1330_ADDR_MODE_REG   0x098c
#define  MI1330_DATA_INOUT_REG  0x0990
/////////////////////////////////////////////////////////////////////
#define  MIVAR_LOGIC_ACCESS    0X2000

#define  MIVAR_DATA_WIDTH_16   0x0000
#define  MIVAR_DATA_WIDTH_8     0x8000

#define  MIVAR_DATA_WIDTH_16_PHY  (MIVAR_DATA_WIDTH_16)
#define  MIVAR_DATA_WIDTH_8_PHY     (MIVAR_DATA_WIDTH_8)


//always logic access
#define  MIVAR_DATA_WIDTH_16_LOG   (MIVAR_DATA_WIDTH_16|MIVAR_LOGIC_ACCESS)
#define  MIVAR_DATA_WIDTH_8_LOG     (MIVAR_DATA_WIDTH_8|MIVAR_LOGIC_ACCESS)

#define  MIVAR_ACCESS_READ   0
#define  MIVAR_ACCESS_WRITE  1


#define MI_DRV_ID_MON    (0 <<8)
#define MI_DRV_ID_SEQ     (1 <<8)
#define MI_DRV_ID_AE     (2 <<8)
#define MI_DRV_ID_AWB    (3 <<8)
#define MI_DRV_ID_ANTF   (4 <<8)
#define MI_DRV_ID_AF       (5 <<8)
#define MI_DRV_ID_AFM     (6 <<8)
#define MI_DRV_ID_MODE   (7 <<8)
#define MI_DRV_ID_HSTG   (11 <<8)

#define HCLK_INDEX_0M_9375		0
#define HCLK_INDEX_1M_125	 	1
#define HCLK_INDEX_1M_25	 	2
#define HCLK_INDEX_1M_5	 	3
#define HCLK_INDEX_1M_875		4
#define HCLK_INDEX_2M_25	 	5
#define HCLK_INDEX_2M_5	 	6
#define HCLK_INDEX_3M	 		7
#define HCLK_INDEX_3M_75		8
#define HCLK_INDEX_4M_5	 	9
#define HCLK_INDEX_5M	 		10
#define HCLK_INDEX_6M	 		11
#define HCLK_INDEX_7M_5		12
#define HCLK_INDEX_9M	 		13
#define HCLK_INDEX_10M	 		14
#define HCLK_INDEX_12M	 		15
#define HCLK_INDEX_15M			16
#define HCLK_INDEX_18M	 		17
#define HCLK_INDEX_20M	 		18
#define HCLK_INDEX_24M	 		19
#define HCLK_INDEX_30M			20
#define HCLK_INDEX_36M	 		21
#define HCLK_INDEX_40M	 		22
#define HCLK_INDEX_48M	 		23
#define HCLK_INDEX_60M			24
#define HCLK_INDEX_72M	 		25
#define HCLK_INDEX_80M	 		26
#define HCLK_INDEX_96M	 		27

#define SENSOR_OPEN		1
#define SENSOR_CLOSED		0

//SV28 voltage    Phy register 0xE0 bit[4:2]
//#define SV28_VOL_2V5  0
//#define SV28_VOL_2V6  1
//#define SV28_VOL_2V7  2
//#define SV28_VOL_2V8  3
//#define SV28_VOL_2V9  4
//#define SV28_VOL_3V0  5
//#define SV28_VOL_3V1  6
//#define SV28_VOL_3V3  7


//#define SV18_VOL_1V5 0
//#define SV18_VOL_1V6 1
//#define SV18_VOL_1V8 2
//#define SV18_VOL_1V9 3
#if (_CHIP_ID_ & _RTS5840_)
// REG_TUNESVA 	0xFFA4
#define SVA_VOL_1V16  0x00
#define SVA_VOL_1V45  0x01
#define SVA_VOL_1V53  0x02
#define SVA_VOL_2V20  0x03
#define SVA_VOL_2V39  0x04
#define SVA_VOL_2V54  0x05
#define SVA_VOL_2V71  0x06
//#define SVA_VOL_3V40  0x07

#define SVA_VOL_1V28  0x10
#define SVA_VOL_1V60  0x11
#define SVA_VOL_1V69  0x12
#define SVA_VOL_2V42  0x13
#define SVA_VOL_2V63  0x14	// 2.59V
#define SVA_VOL_2V79  0x15
#define SVA_VOL_2V98 0x16
//#define SVA_VOL_3V40  0x17

#define SVA_VOL_1V39  0x20
#define SVA_VOL_1V74  0x21
#define SVA_VOL_1V84  0x22
#define SVA_VOL_2V630  0x23
#define SVA_VOL_2V87  0x24
#define SVA_VOL_3V04  0x25
#define SVA_VOL_3V25  0x26
//#define SVA_VOL_3V40  0x27

#define SVA_VOL_1V51  0x30
#define SVA_VOL_1V89  0x31
#define SVA_VOL_1V99  0x32
#define SVA_VOL_2V85  0x33
#define SVA_VOL_3V11  0x34
#define SVA_VOL_3V30  0x35
#define SVA_VOL_3V40  0x36
//#define SVA_VOL_3V40  0x37
#elif (_CHIP_ID_ & _RTS5832_)
// REG_TUNESVA 	0xFFA4
#define SVA_VOL_1V20  0x00
#define SVA_VOL_1V49  0x01
#define SVA_VOL_1V58  0x02
#define SVA_VOL_2V25  0x03
#define SVA_VOL_2V45  0x04
#define SVA_VOL_2V61  0x05
#define SVA_VOL_2V79  0x06
#define SVA_VOL_3V400  0x07

#define SVA_VOL_1V31  0x10
#define SVA_VOL_1V64  0x11
#define SVA_VOL_1V73  0x12
#define SVA_VOL_2V48  0x13
#define SVA_VOL_2V70  0x14	// 2.59V
#define SVA_VOL_2V87  0x15
#define SVA_VOL_3V00  0x16
//#define SVA_VOL_3V40  0x17

#define SVA_VOL_1V43  0x20
#define SVA_VOL_1V79  0x21
#define SVA_VOL_1V89  0x22
#define SVA_VOL_2V700  0x23
#define SVA_VOL_2V94  0x24
#define SVA_VOL_3V13  0x25
#define SVA_VOL_3V33  0x26
//#define SVA_VOL_3V40  0x27

#define SVA_VOL_1V56  0x30
#define SVA_VOL_1V94  0x31
#define SVA_VOL_2V05  0x32
#define SVA_VOL_2V93  0x33
#define SVA_VOL_3V19  0x34
#define SVA_VOL_3V37  0x35
#define SVA_VOL_3V40  0x36
//#define SVA_VOL_3V40  0x37
#elif (_CHIP_ID_ & _RTS5829B_)
// REG_TUNESVA 	0xFFA4
#define SVA_VOL_1V17  0x00
#define SVA_VOL_1V47  0x01
#define SVA_VOL_1V54  0x02
#define SVA_VOL_2V22  0x03
#define SVA_VOL_2V41  0x04
#define SVA_VOL_2V56  0x05
#define SVA_VOL_2V73  0x06
//#define SVA_VOL_3V32   0x07

#define SVA_VOL_1V29  0x10
#define SVA_VOL_1V61  0x11
#define SVA_VOL_1V69  0x12
#define SVA_VOL_2V44  0x13
#define SVA_VOL_2V65  0x14	// 2.59V
#define SVA_VOL_2V82  0x15
#define SVA_VOL_3V00  0x16
//#define SVA_VOL_3V32  0x17

#define SVA_VOL_1V41  0x20
#define SVA_VOL_1V79  0x21
#define SVA_VOL_1V84  0x22
#define SVA_VOL_2V66  0x23
#define SVA_VOL_2V89  0x24
#define SVA_VOL_3V08  0x25
#define SVA_VOL_3V28  0x26
//#define SVA_VOL_3V32  0x27

#define SVA_VOL_1V53  0x30
#define SVA_VOL_1V94  0x31
#define SVA_VOL_1V99  0x32
#define SVA_VOL_2V88  0x33
#define SVA_VOL_3V13  0x34
#define SVA_VOL_3V31  0x35
#define SVA_VOL_3V32  0x36
//#define SVA_VOL_3V32  0x37
#endif

#if (_CHIP_ID_ & _RTS5840_)
// REG_TUNESVIO		0xFFAA
#define SVIO_VOL_1V13  0x00
#define SVIO_VOL_1V42  0x01
#define SVIO_VOL_1V50  0x02
#define SVIO_VOL_2V14  0x03
#define SVIO_VOL_2V33  0x04
#define SVIO_VOL_2V47  0x05
#define SVIO_VOL_2V64  0x06
//#define SVIO_VOL_3V40  0x07

#define SVIO_VOL_1V25  0x10
#define SVIO_VOL_1V56  0x11
#define SVIO_VOL_1V64  0x12
#define SVIO_VOL_2V35  0x13
#define SVIO_VOL_2V56  0x14	// 2.59V
#define SVIO_VOL_2V72  0x15
#define SVIO_VOL_2V90  0x16
//#define SVIO_VOL_3V40  0x17

#define SVIO_VOL_1V36  0x20
#define SVIO_VOL_1V70  0x21
#define SVIO_VOL_1V79  0x22
#define SVIO_VOL_2V560  0x23
#define SVIO_VOL_2V79  0x24
#define SVIO_VOL_2V97  0x25
#define SVIO_VOL_3V17  0x26
//#define SVIO_VOL_3V40  0x27

#define SVIO_VOL_1V47  0x30
#define SVIO_VOL_1V84  0x31
#define SVIO_VOL_1V94  0x32
#define SVIO_VOL_2V78  0x33
#define SVIO_VOL_3V02  0x34
#define SVIO_VOL_3V21  0x35
#define SVIO_VOL_3V40  0x36
//#define SVIO_VOL_3V40  0x37
#elif (_CHIP_ID_ & _RTS5832_)
// REG_TUNESVIO		0xFFAA
#define SVIO_VOL_1V17  0x00
#define SVIO_VOL_1V46  0x01
#define SVIO_VOL_1V54  0x02
#define SVIO_VOL_2V21  0x03
#define SVIO_VOL_2V40  0x04
#define SVIO_VOL_2V55  0x05
#define SVIO_VOL_2V72  0x06
//#define SVIO_VOL_3V40  0x07

#define SVIO_VOL_1V29  0x10
#define SVIO_VOL_1V61  0x11
#define SVIO_VOL_1V70  0x12
#define SVIO_VOL_2V43  0x13
#define SVIO_VOL_2V64  0x14
#define SVIO_VOL_2V81  0x15
#define SVIO_VOL_3V00  0x16
//#define SVIO_VOL_3V40  0x17

#define SVIO_VOL_1V40  0x20
#define SVIO_VOL_1V75  0x21
#define SVIO_VOL_1V85  0x22
#define SVIO_VOL_2V65  0x23
#define SVIO_VOL_2V88  0x24
#define SVIO_VOL_3V06  0x25
#define SVIO_VOL_3V27  0x26
//#define SVIO_VOL_3V40  0x27

#define SVIO_VOL_1V52  0x30
#define SVIO_VOL_1V90  0x31
#define SVIO_VOL_2V01  0x32
#define SVIO_VOL_2V87  0x33
#define SVIO_VOL_3V12  0x34
#define SVIO_VOL_3V32  0x35
#define SVIO_VOL_3V40  0x36
//#define SVIO_VOL_3V40  0x37
#elif (_CHIP_ID_ & _RTS5829B_)
// REG_TUNESVIO		0xFFAA
#define SVIO_VOL_1V14  0x00
#define SVIO_VOL_1V42  0x01
#define SVIO_VOL_1V50  0x02
#define SVIO_VOL_2V15  0x03
#define SVIO_VOL_2V34  0x04
#define SVIO_VOL_2V49  0x05
#define SVIO_VOL_2V65  0x06
//#define SVIO_VOL_3V27  0x07

#define SVIO_VOL_1V25  0x10
#define SVIO_VOL_1V56  0x11
#define SVIO_VOL_1V65  0x12
#define SVIO_VOL_2V37  0x13
#define SVIO_VOL_2V58  0x14
#define SVIO_VOL_2V74  0x15
#define SVIO_VOL_2V92  0x16
//#define SVIO_VOL_3V27  0x17

#define SVIO_VOL_1V37  0x20
#define SVIO_VOL_1V71  0x21
#define SVIO_VOL_1V80  0x22
#define SVIO_VOL_2V580  0x23
#define SVIO_VOL_2V81  0x24
#define SVIO_VOL_2V98  0x25
#define SVIO_VOL_3V18  0x26
//#define SVIO_VOL_3V27  0x27

#define SVIO_VOL_1V48  0x30
#define SVIO_VOL_1V85  0x31
#define SVIO_VOL_1V95  0x32
#define SVIO_VOL_2V80  0x33
#define SVIO_VOL_3V04  0x34
#define SVIO_VOL_3V22  0x35
#define SVIO_VOL_3V27  0x36
//#define SVIO_VOL_3V27  0x37
#endif

// CCS_CONTROL		0xFECD
#define HSYNC_ACTIVE_LOW		0x00
#define HSYNC_ACTIVE_HIGH		0x01
#define VSYNC_ACTIVE_LOW		0x00
#define VSYNC_ACTIVE_HIGH		0x02
#define PIXEL_SAMPLE_FALLING	0x00
#define PIXEL_SAMPLE_RISING	0x04

void ReadSensorID(void);
//void InitSensorParam(void);
void SetSensorFormatFps(U16 SetFormat,U8 Fps);
void Reset_Sensor(void);
void	SetSensorIntegrationTime(U32 dwSetValue);
void SetSensorIntegrationTimeAuto(U8 bySetValue);
void SetSensorLowLightComp(U8 bySetValue);
void	SetSensorSharpness(U8 bySetValue);
void 	SetSensorPwrLineFreq(U8 byFps, U8 bySetValue);
void SetSensorBackLightComp(U8 bySetValue);

void	SetSensorWBTemp(U16 wSetValue);

void	SetSensorWBTempAuto(U8 bySetValue);
void	SetSensorImgDir(U8 bySnrImgDir);
void CfgSensorControlAttr(void );
void SensorOutputTestPattern(U8 byMode);
void SetSensorScaleCrop(U16 wX,U16 wY,U16 wSnrX,U16 wSnrY);
void SetSensorOutputDim(U16 wWidth,U16 wHeight);
OV_CTT_t ReadSensorAwbGain();
U16 GetSensorAEGain();
U8 GetSensorYavg(void);
U16 GetSensorExposuretime(void);
void PreviewToCapture(U16 wPreviewWidth, U16 wStillWidth);
void CaptureToPreview();
//void InitIspParams();
void SetSensorDynamicISP(U8 byAEC_Gain);
void SetSensorDynamicISP_AWB(U16 wColorTempature);
void SetSensorExposuretime_Gain(float fExpTime, float fTotalGain);
U16 GetSensorExposureTime(void);
void SetSensorGain(float fGain);

//void GetBLCoffset();
//void SetBackendEN(U16 width);
#endif //_CAMSENSOR_H_
