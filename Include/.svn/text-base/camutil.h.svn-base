
#ifndef _CAMUTIL_H_
#define _CAMUTIL_H_

///////////////////////////////////////////
#include <reg52.h>
#include <absacc.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include<intrins.h>

typedef  unsigned char			U8;		//  8bit
typedef  unsigned short int		U16;		// 16bit
typedef  unsigned long	int		U32;		// 32bit

typedef  signed char			S8;
typedef  signed short int		S16;
typedef  signed long  int		S32;
typedef  unsigned char            BOOL;

typedef struct OV_FpsSetting
{
	U8 byFps;
	U16 wExtraDummyPixel;   // register 0x2a,0x2b: dummy pixel adjustment
	//U8  byExtraDummyLine;
	U8  byClkrc; // register 0x11: clock pre-scalar
	U32 dwPixelClk;
} OV_FpsSetting_t;

typedef struct GC_FpsSetting
{
	U8 byFps;
	U16 wExtraDummyPixel;   // register 0x2a,0x2b: dummy pixel adjustment
	U8  byExtraDummyLine;
	U8  byClkrc; // register 0x11: clock pre-scalar
	U32 dwPixelClk;
} GC_FpsSetting_t;

typedef struct
{
	U8 byFps;
	U8 byHB;   // register 0x2a,0x2b: dummy pixel adjustment
	U8 byVB;
	U8  byClkrc; // register 0x11: clock pre-scalar
	U32 dwPixelClk;
} GC0307FpsSetting_t;

typedef struct OV_CTT
{
	U16 wCT;	// color temperature
	U16 wRgain;	// r gain
	U16 wGgain;	// g gain
	U16 wBgain;	// b gain
} OV_CTT_t;

typedef struct OV_BandingFilter
{
	U16 wD50Base; // 50Hz, banding filter base value, Minimum AEC value
	U8  byD50Step; // 50Hz, banding filter Maximun step
	U16 wD60Base; // 60Hz, banding filter base value, Minimum AEC value
	U8  byD60Step; // 60Hz, banding filter Maximun step
} OV_BandingFilter_t;

#ifdef _ENABLE_OUTDOOR_DETECT_
typedef struct Dyanmic_AWB_Boundary
{
		S8 byAWB_K3;
		S16 wAWB_B3;
		S8 byAWB_K4;
		S16 wAWB_B4;
		S8 byAWB_K5;
		S16 wAWB_B5;
		S8 byAWB_K6;
		S16 wAWB_B6;
} Dyanmic_AWB_Boundary_t;
#endif

/* boolean definition*/
#ifdef TRUE
#undef TRUE
#endif

#ifdef FALSE
#undef FALSE
#endif

#define TRUE  1
#define FALSE 0

#define EN_DELAYTIME 1
#define DIS_DELAYTIME 0

/*********************
***GPIO configuration****
*********************/
#define SPI_WP_GPIO            0
#define SPI_WP_GPIO10		(10-8)
//#define OLT_TEST_GPIO                   1
//#define SLB_TEST_CFG_GPIO         2
//#define SLB_TEST_GPIO                   3
#define SENSOR_RST_GPIO_IDX    5
//for fix GPIO6 power bug , replace GPIO_6 by GPIO_9
#define BLINK_GPIO_IDX                 9
#define EEP_PWRCTL_GPIO_IDX    7

#define BACK_DOOR_PIN                 7
#ifdef _HID_BTN_
#define HID_BTN_IDX				0	//GPIO 0 used as HID BUTTON TEST
#define HID_BTN_1			0x01
#define HID_BTN_2			0x02
#define HID_BTN_3			0x04
#define HID_BTN_4			0x08
#define HID_BTN_5			0x10
#define HID_BTN_6			0x20
#define HID_BTN_7			0x40
#define HID_BTN_8			0x80
#endif

#define DUMMY_BYTE (0XBB)

#define VALID_BTN_PULS   (0x03)

#define SWITCH_ON 1
#define SWITCH_OFF 0

#define CLOCK_DOWN_DELAY_CNT  300		// hemonel 2010-06-09: delay 300x10ms = 3s
#define I2CBUS_POWEROFF_DELAY 100
#define EEPROM_POWEROFF_DELAY 50


sfr CKCON  = 0x8E;

#define LONG2CHAR(num,i)		(*( (U8 *)&num + (3-i) ))
#define INT2CHAR(num,i)			(*( (U8 *)&num + (1-i) ))

#define ASSIGN_INT(num,hbyte,lbyte)			{*( (U8 *)&num  ) = hbyte; \
											*( (U8 *)&num+1 ) = lbyte;}

#define ASSIGN_LONG(num,byte3,byte2,byte1,byte0)	{*( (U8 *)&num  )  = byte3; \
													*( (U8 *)&num+1 ) = byte2; \
													*( (U8 *)&num+2 ) = byte1; \
 											    	*( (U8 *)&num+3 ) = byte0;  }
#define ASSIGN_U32BYTE(num, i,byte)    {*( (U8 *)&num+(3-i) ) = byte;}
#define ASSIGN_U16BYTE(num, i,byte)    {*( (U8 *)&num+(1-i) ) = byte;}



#define CONSTLONG2BYTE(cLong,i)           ((cLong)&(0xff000000>>(24-(i)*8)))>>(8*(i))

#define CONSTSHORT2BYTE(cShort,i)        ((cShort)&(0xff00>>(8-(i)*8)))>>(8*(i))


#define EnterCritical()  {EA=0;EPFI=0;}
#define ExitCritical()     {EA=1;EPFI=1;}

extern void rlprintf(const char *, ...);

#define printf rlprintf
#define CONFIG_PRINT_SIZE		64

#ifdef FUNC_IN_OUT_MSG_ENABLE
//FUNC IN/OUT Message,  the parameter should be the function's name
//#define FUNC_IN_MSG(x)   printf("\n====> %s\tLine:%4d",__FILE__,__LINE__); printf("\tFunc:    "); printf x;
//printf("\n") //printf("\t%s\tLine:%4d\n\n",__FILE__,__LINE__);
//#define FUNC_OUT_MSG(x)  printf("\n<==== %s\tLine:%4d",__FILE__,__LINE__); printf("\tFunc:    "); printf x;
//printf("\n") //printf("\t%s\tLine:%4d\n\n",__FILE__,__LINE__);
#define FUNC_IN_MSG(x) \
	do{	\
		printf("\n==> %s \t Line:%d",__FILE__,__LINE__);\
		printf("\tFunc:    ");\
		printf (x);\
	} while(0)

#define FUNC_OUT_MSG(x) \
	do{	\
		printf("\n<== %s\ t Line:%d",__FILE__,__LINE__);\
		printf("\tFunc:    ");\
		printf (x);\
	} while(0)

#else
#define FUNC_IN_MSG(x)
#define FUNC_OUT_MSG(x)
#endif

#ifdef LINE_RUN_MSG
#define LINE_EXEC_MSG(x) \
	do{	\
		printf("\n==>%s\t Line:%d\t  -%bd-",__FILE__,__LINE__,(x));\
	} while(0)
#else
#define LINE_EXEC_MSG(x)
#endif


#ifdef MSG_ENABLE
#define MSG(x)  printf x
#else
#define MSG(x)
#endif

#ifdef DBG_MSG_ENABLE
#define DBG(x)      printf x
#else
#define DBG(x)
#endif

#ifdef MIPI_MSG_ENABLE
#define MIPI_MSG(x) printf x
#else
#define MIPI_MSG(x)
#endif

#ifdef DBG_sCache_MSG_ENABLE
#define DBG_sCache(x)      printf x
#else
#define DBG_sCache(x)
#endif

#ifdef CFG_MSG_ENABLE
#define DBG_CFG(x)      printf x
#else
#define DBG_CFG(x)
#endif

#ifdef DBG_BOOT_ENABLE
#define DBG_BOOT(x)      printf x
#else
#define DBG_BOOT(x)
#endif

#ifdef DBG_MCU_SPDUP_ENABLE
#define DBG_SPDUP(x)      printf x
#else
#define DBG_SPDUP(x)
#endif

#ifdef DBG2_MSG_ENABLE
#define DBG2(x)      printf x
#else
#define DBG2(x)
#endif

#ifdef DBG_SENSOR_MSG_ENABLE
#define DBG_SENSOR(x)      printf x
#else
#define DBG_SENSOR(x)
#endif

#ifdef DBG_OV7670_MSG_ENABLE
#define DBG_OV7670(x)      printf x
#else
#define DBG_OV7670(x)
#endif

#ifdef DBG_OV7690_MSG_ENABLE
#define DBG_OV7690(x)      printf x
#else
#define DBG_OV7690(x)
#endif

#ifdef FCALL_DBG_MSG_ENABLE
#define FCALL_DBG(x)      printf x
#else
#define FCALL_DBG(x)
#endif

#ifdef DBG_VD_MSG_ENABLE
#define DBG_VD(x)      printf x
#else
#define DBG_VD(x)
#endif

#ifdef GLOBALVAR_INIT_MSG_ENABLE
#define VARINTITSTMSG(x)      printf x
#else
#define VARINTITSTMSG(x)    
#endif

#ifdef WARN_MSG_ENABLE
#define WARN(x)  printf x
#else
#define WARN(x)
#endif

#ifdef ERR_MSG_ENABLE
#define ERR(x)  printf x
#else
#define ERR(x)
#endif

#ifdef ISP_MSG_ENABLE
#define ISP_MSG(x) printf x
#else
#define ISP_MSG(x)
#endif

#ifdef UAC_DBG_ENABLE
#define DBG_UAC(x) 	printf x
#else
#define DBG_UAC(x)
#endif

#ifdef AE_MSG_ENABLE
#define AE_MSG(x) printf x
#else
#define AE_MSG(x)
#endif

#ifdef AF_MSG_ENABLE
#define AF_MSG(x) printf x
#else
#define AF_MSG(x)
#endif

#ifdef USB_DBG_ENABLE
#define USB_MSG(x) printf x
#else
#define USB_MSG(x)
#endif

#ifdef AWB_MSG_ENABLE
#define AWB_MSG(x) printf x
#else
#define AWB_MSG(x)
#endif

#ifdef AB_MSG_ENABLE
#define AB_MSG(x) printf x
#else
#define AB_MSG(x)
#endif

#ifdef BURNIN_MSG_ENABLE
#define BURNIN_MSG(x) printf x
#else
#define BURNIN_MSG(x)
#endif

#ifdef EPE_MSG_ENABLE
#define EPE_MSG(x) printf x
#else
#define EPE_MSG(x)
#endif

#ifdef ISO_MSG_ENABLE
#define ISO_MSG(x) printf x
#else
#define ISO_MSG(x)
#endif

#ifdef IDEAEYE_MSG_ENALBE
#define IDEAEYE_MSG(x) printf x
#else
#define IDEAEYE_MSG(x)
#endif

#ifdef _ASSERT_ENABLE_
#define ASSERT(expr) \
  if (expr) { ; } \
  else  {\
    ERR(("Assert failed: " #expr " (file %s line %d)\n", __FILE__, (int) __LINE__ ));\
    while (1);\
  }
#else
#define ASSERT(expr)
#endif

////
typedef  enum
{
    DEV_STS_IDLE=0,
    DEV_STS_STREAMING=1,
    DEV_STS_STILL=2,
    DEV_STS_PWRON_FAIL=3,
    DEV_STS_I2CACCESS_FAIL=4,
    DEV_STS_SNR_NOTSP=5,
    DEV_STS_NO_SENSOR=6,
    DEV_STS_ISP_OVERFLOW =7,
    DEV_STS_JPG_OVERFLOW =8,
    DEV_STS_SIE_OVERFLOW=9
} VDevSts_t;



//utility functions
void Delay(U16 ms);
void uDelay(U16 w100us);
void VideoDelay(U8 byDelayMax100Ms, U8 byFrameCnt, U8 byFPS);
void STALL_EPCTL(void );
void VCSTALL_EPCTL(U8 ErrCode);
void usDelay(U8 byus);
bit	WaitTimeOut(U16 wReg, U8 byVal, U8 byPolarity, U8 byMSEC) ;
void WaitTimeOut_Delay(U8 by10MSec);
#ifdef _NOLPML1_HANG_
bit	WaitTimeOutTwoReg(U16 wReg1, U8 byVal1, U8 byPolarity1, U16 wReg2, U8 byVal2, U8 byPolarity2, U8 byMSEC);
#endif
U8   DelayWait(U16 wReg, U8 byVal, U8 byPolarity, U8 byMSEC);
U8   DelayWait_main(U16 wReg, U8 byVal, U8 byPolarity, U8 byMSEC);
void CHANGE_MCU_CLK(U8 MCU_CLK);
void CHANGE_ISP_CLK(U8 ISP_CLK);
void CHANGE_JPEG_CLK(U8 JPEG_CLK);
U8 CHANGE_CCS_CLK(U8 CCS_CLK);
void ISP_Clock_Switch(U8 bySwitch);
void JPEG_Clock_Switch(U8 bySwitch);
U8 PHYRegisterRead(U8 byAddr);
void PHYRegisterWrite(U8 setvalue, U8 byAddr);
void PHYRegisterWrite_Mask(U8 setvalue, U8 byAddr,U8 byMask);


#define SPI_CLK_SINK_SF    0
#define SPI_CLK_SINK_EEP  1

#define I2C_BUS_SNR_SEL 0x01
#define I2C_BUS_EEP_SEL 0x02

#define EEPROM_POWER_STS_OFF 0
#define EEPROM_POWER_STS_ON  1

#define NON_CRYSTAL_OSC_SEL_LC              0X00
#define NON_CRYSTAL_OSC_SEL_RC             0X80
#define NON_CRYSTAL_USBIF_SEL_LC_OSC  0X01
#define NON_CRYSTAL_TUNE_METHOD2        0X02
#define NON_CRYSTAL_TUNE_NF_GAIN		0x01
#define NON_CRYSTAL_TUNE_THERMAL_LC	0x02

#define SLBTESTMODE_HSONLY   0X01
#define SLBTESTMODE_FSONLY   0X02
#define SLBTESTMODE_HS_FS      0X03

void SensorPowerControl(U8 bySwitch,U8 byDelayCtrl );
U16 GetGPIOValueW();
bit  GetGPIOValueB(U8 io) ;// test if GPIO 1 or 0
void SetGpioValueB(U8 io,U8 value);
void SetGPIOPullCtlW(U16 pullEn,U16 pullDir);
void SetGPIODirB(U8 io,U8 byValue); //set GPIO direction 1: output 0: input
U16 TestGPIOIntEnW(U8 edge);
bit TestGPIOIntEnB(U8 io,U8 edge);
void SetGPIOIntEnB(U8 io, U8 edge);
U16 TestGpioIntFlagW(U8 edge);
void ClearGpioIntFlagW(U16 wValue,U8 edge);
bit TestGpioIntFlagB(U8 io, U8 edge);
void ClearGpioIntFlagB(U8 io, U8 edge);
U16 ClipWord(U16 wCur, U16 wMin, U16 wMax);
float ClipFloat(float fCur, float fMin, float fMax);
U8 LinearIntp_Byte(U8 byX1,U8 byY1, U8 byX2, U8 byY2, U8 byX);
U8 LinearIntp_Byte_Bound(U8 byX1,U8 byY1, U8 byX2, U8 byY2, U8 byX);
S8 LinearIntp_Byte_Signed(S8 byX1,S8 byY1, S8 byX2, S8 byY2, S8 byX);
U16 LinearIntp_Word(U16 wX1,U16 wY1, U16 wX2, U16 wY2, U16 wX);
S16 LinearIntp_Word_Signed(S16 wX1,S16 wY1, S16 wX2, S16 wY2, S16 wX);
S16 LinearIntp_Word_Bound_Signed(S16 wX1,S16 wY1, S16 wX2, S16 wY2, S16 wX);
U8 MaxByte(U8 byX1, U8 byX2);
U8 MinByte(U8 byX1, U8 byX2);

#define START_I2C_CLK()   		{XBYTE[BLK_CLK_EN] |= I2C_CLK_EN;}
#define STOP_I2C_CLK()    		{XBYTE[BLK_CLK_EN] &= (~I2C_CLK_EN);}

float MaxFloat(float fX1, float fX2);
float MinFloat(float fX1, float fX2);
U16 MinWord(U16 wX1, U16 wX2);
#endif
